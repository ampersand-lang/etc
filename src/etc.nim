import os
import osproc
import streams
import macros
import options
import sets
import deques
import strformat
import strutils
import sequtils
import parseopt
import random
import tables
import sugar

import etcpkg/lib
import etcpkg/sema/type_analysis
import etcpkg/sema/name_mangler
import etcpkg/cg/cg
import etcpkg/llvm/llvm

when isMainModule:
  type Emit = enum
    emitIr
    emitBc
    emitAsm
    emitObj
    emitBin

  type Relocation = enum
    relDefault
    relPie
    relStatic
    relDynamic

  proc printHelp(arg0: string) =
    echo fmt"Usage: {arg0} COMMAND"
    echo ""
    echo "Commands"
    echo "  compile         Compiles a single unit (which might consist of multiple files)."
    echo "  build           Builds the current project."
    echo "  check           Checks the current project for errors."
    echo "  expand          Expands macros and statements in a single file."
    echo "  test            Runs tests."
    echo "  install         Installs the package."
    echo "  help            Prints this message."
    echo "  version         Prints the version."
  
  proc printVersion(arg0: string) =
    echo fmt"{arg0}: {lib.version_str}"

  proc emit_to_ir(module: llvm.ModuleRef, output: string, reloc: Relocation) =
    var error: cstring
    let e = llvm.printModuleToFile(module, output.cstring, addr error)
    if e != 0:
      writeLine stderr, error
      llvm.disposeMessage(error)
      quit(1)

  proc emit_to_bc(module: llvm.ModuleRef, output: string, reloc: Relocation) =
    var error: cstring
    let e = llvm.writeBitcodeToFile(module, output.cstring)
    if e != 0:
      quit(1)

  proc emit_to_asm_or_obj(module: llvm.ModuleRef, output: string, reloc: Relocation, opt_level: int, gen: llvm.CodeGenFileType) =
    llvm.initNative()

    let triple = llvm.getDefaultTargetTriple()
    var target: llvm.TargetRef
    discard llvm.getTargetFromTriple(triple, addr target, nil)
    if target == nil:
      writeLine stderr, "no default target available"
      quit(1)

    # Less and above cause bugs in the produced binary
    var level = llvm.codeGenLevelNone
    case opt_level
    of 0: level = llvm.codeGenLevelNone
    of 1: level = llvm.codeGenLevelLess
    of 2: level = llvm.codeGenLevelDefault
    else: level = llvm.codeGenLevelAggressive

    var reloc_model = llvm.relocDefault
    case reloc
    of relDefault: reloc_model = llvm.relocDefault
    of relPie: reloc_model = llvm.relocPIC
    of relStatic: reloc_model = llvm.relocStatic
    of relDynamic: reloc_model = llvm.relocDynamicNoPic

    let
      cpu = "generic".cstring
      features = "".cstring

    let target_machine = llvm.createTargetMachine(
      target,
      triple,
      cpu,
      features,
      level,
      reloc_model,
      llvm.codeModelDefault,
    )

    let target_data = llvm.createTargetDataLayout(target_machine)

    var error: cstring

    let e = llvm.targetMachineEmitToFile(
      target_machine,
      module,
      output.cstring,
      gen,
      addr error,
    )

    if e != 0:
      writeLine stderr, error
      llvm.disposeMessage(error)
      quit(1)

    llvm.disposeTargetData(target_data)
    llvm.disposeTargetMachine(target_machine)

  proc emit_to_asm(module: llvm.ModuleRef, output: string, reloc: Relocation, opt_level: int) =
    emit_to_asm_or_obj(module, output, reloc, opt_level, llvm.assemblyFile)

  proc emit_to_obj(module: llvm.ModuleRef, output: string, reloc: Relocation, opt_level: int) =
    emit_to_asm_or_obj(module, output, reloc, opt_level, llvm.objectFile)

  proc temp_file(): string = "./tmp." & $random.rand(65536)

  proc emit_to_bin(module: llvm.ModuleRef, output: string, reloc: Relocation, opt_level: int, system_c: bool, link: seq[string], link_dir: seq[string], shared_lib: bool) =
    let
      temp = temp_file()
      arg0 = os.getAppFilename()
    emit_to_asm_or_obj(module, temp, reloc, opt_level, llvm.objectFile)
    if system_c:
      var args = @[temp, "-o", output, "-lc", "-lm"]
      args.add(link)
      args.add(link_dir)
      if reloc == relPie:
        args.add("-fPIC")
      if shared_lib:
        args.add("-shared")
      echo fmt"{arg0}: clang ", args.join(" ")
      let output = execProcess("clang", args = args, options = {poStdErrToStdOut, poUsePath})
      write stderr, output
    else:
      raise newIce("unimplemented")
    os.removeFile(temp)

  proc ext_of(emit: Emit): string =
    case emit
    of emitBin:
      return ""
    of emitIr:
      return "ll"
    of emitBc:
      return "bc"
    of emitAsm:
      return "s"
    of emitObj:
      return "o"

  random.randomize()
  
  proc executeCE(command: string) =
    proc printHelp(arg0: string) =
      case command
      of "compile": echo fmt"Usage: {arg0} compile [options] IN.."
      of "expand": echo fmt"Usage: {arg0} expand [options] IN"
      else: discard # unreachable
      echo ""
      echo "Input Options"
      echo "  -I | --include DIR                        Include DIR when searching for modules."
      echo "  -C | --c-include DIR                      Include DIR when searching for C headers."
      echo "  -L | --libdir DIR                         Include DIR when searching for libraries."
      echo "  -D | --c-define DEF[=VALUE]               Include definition when compiling C headers."
      echo "       --system-c                           Use the system-provided libc instead of the one provided by etc."
      echo "       --no-system-c                        Use the default libc."
      echo ""
      echo "Output options"
      echo "  -o | --out OUT                            Place the output into OUT."
      echo "  -c                                        Don't run the linker. (Implies --emit obj)"
      echo "  -g | --debuginfo                          Generate debug metadata."
      echo "  -O0 | -O1 | -O2 | -O3                     Set optimization level."
      echo "  --emit llvm-ir|llvm-bc|asm|obj|bin        Emit to llvm-ir, llvm-bytecode, assembly or object. (Default: binary)"
      echo "  --relocation pie|default|static|dynamic   Set relocation model."
      echo "  --shared                                  Link a shared library. (Implies --relocation pie, --emit bin)"
      echo "  -lLIB                                     Link to a library"
      echo ""
      echo "Compiler directives"
      echo "  -N | --package NAME                       Compile with NAME set as the package name."
      echo ""
      echo "Misc"
      echo "  -h | --help                               Print this message and exit."
      echo "  -V | --version                            Print version and exit."

    var
      opts = initOptParser(
        os.commandLineParams()[1 ..^ 1].join(" "),
        {'h', 'V', 'c', 'g'},
        @["help", "version", "shared", "system-c", "debuginfo"],
      )
      amp_include = newSeq[string]()
      c_include = newSeq[string]()
      c_define = newSeq[string]()
      system_c = true
      output = none[string]()
      no_linker = false
      debug_info = false
      opt_level = 0
      emit = emitBin
      reloc = relDefault
      shared_lib = false
      package_name = none[string]()
      paths = newSeq[string]()
      link = newSeq[string]()
      link_dir = newSeq[string]()
  
    for opt in getopt(opts):
      case opt.kind
      of cmdEnd: break
      of cmdShortOption, cmdLongOption:
        case opt.key
        of "I", "include":
          amp_include.add(opt.val)
        of "C", "c-include":
          c_include.add(opt.val)
        of "D", "define":
          c_define.add(opt.val)
        of "L", "libdir":
          link_dir.add(fmt"-L{opt.val}")
        of "system-c":
          system_c = true
        of "no-system-c":
          system_c = false
        of "o", "out":
          output = some(opt.val)
        of "c":
          no_linker = true
        of "g", "debuginfo":
          debug_info = true
        of "O":
          opt_level = opt.val.parseInt
        of "emit":
          case opt.val
          of "llvm-ir":
            emit = emitIr
          of "llvm-bc":
            emit = emitBc
          of "asm":
            emit = emitAsm
          of "obj":
            emit = emitObj
          else:
            emit = emitBin
        of "relocation":
          case opt.val
          of "pie", "pic":
            reloc = relPie
          of "default":
            reloc = relDefault
          of "static":
            reloc = relStatic
          of "dynamic":
            reloc = relDynamic
          else:
            reloc = relDefault
        of "shared":
          shared_lib = true
        of "l":
          link.add(fmt"-l{opt.val}")
        of "N", "package":
          package_name = some(opt.val)
        of "h", "help":
          printHelp(os.getAppFilename())
          return
        of "V", "version":
          printVersion(os.getAppFilename())
          return
        else: discard
      of cmdArgument:
        paths.add(opt.key)
  
    if paths.len == 0:
      printHelp(os.getAppFilename())
      return
  
    try:
      let
        target = newNativeTarget()
      var
        units = newSeq[ref IndexUnit]()
        compile = initOrderedSet[string]()
        index = newIndex()
        driver = newDriver(target, opt_level, debug_info, false, false, false)

      for i in amp_include:
        index.add_include(i)
  
      for i in c_include:
        index.add_c_include(i)
  
      for i in c_define:
        index.add_c_define(i)
  
      for path in paths:
        let 
          directory = path.parentDir
          file = path.lastPathPart
        
        let unit = index.load(path)
        if unit.isNil:
          echo fmt"""
error({errkFileNotFound}): file not found: {path}
"""
          quit(1)
        units.add(unit)
        compile.incl(unit.path)
      let modules = init_modules(driver.type_ctx)
      load_module(modules, ["__lang", "macros"].toPath, driver.type_ctx, driver.macro_ctx, driver.pass_controllers[tsCodeDecl].CgPassController)
      startIndex(index, package_name, compile)
      driver.init(index)
      case command
      of "compile":
        let
          module = driver.run(index, units[0])
          output = output.get(paths[0].changeFileExt(ext_of(emit)))
  
        var err: cstring
        if llvm.verifyModule(module, llvm.ReturnStatusAction, addr err) != 0:
          let module = llvm.printModuleToString(module)
          let msg = fmt"{'\n'}{err}{'\n'}{module}"
          llvm.disposeMessage(module)
          llvm.disposeMessage(err)
          raise newIce(msg)
  
        case emit
        of emitIr:
          emit_to_ir(module, output, reloc)
        of emitBc:
          emit_to_bc(module, output, reloc)
        of emitAsm:
          emit_to_asm(module, output, reloc, opt_level)
        of emitObj:
          emit_to_obj(module, output, reloc, opt_level)
        of emitBin:
          emit_to_bin(module, output, reloc, opt_level, system_c, link, link_dir, shared_lib)
      of "expand":
        driver.run_until(index, units[0], tsFlowAnalysis)
        let
          output = output.get(paths[0].changeFileExt("ampe"))
          outfile = newFileStream(output, fmWrite)
        write outfile, $units[0].root.get
    except Error:
      let e = (ref Error)(getCurrentException())
      write stderr, $e[]
      quit(1)
    except Errors:
      let e = (ref Errors)(getCurrentException())
      write stderr, $e[]
      quit(1)

  proc executeBCT(command: string) =
    proc printHelp(arg0: string) =
      case command
      of "build": echo fmt"Usage: {arg0} build [options]"
      of "check": echo fmt"Usage: {arg0} check [options]"
      of "test": echo fmt"Usage: {arg0} test [options]"
      else: discard # unreachable
      echo ""
      echo "Output options"
      echo "       --release                            Build in release mode (with optimizations, without debug info)."
      echo "       --debug                              Build in debug mode (without optimizations, with debug info)."
      echo ""
      echo "Misc"
      echo "       --run                                Run the binary after building."
      echo "  -h | --help                               Print this message and exit."
      echo "  -V | --version                            Print version and exit."

    var
      opts = initOptParser(
        os.commandLineParams()[1 ..^ 1].join(" "),
        {'h', 'V'},
        @["help", "version", "release"],
      )
      release = false
      run = false

    for opt in getopt(opts):
      case opt.kind
      of cmdEnd: break
      of cmdShortOption, cmdLongOption:
        case opt.key
        of "release":
          release = true
        of "debug":
          release = false
        of "run":
          run = true
        of "h", "help":
          printHelp(os.getAppFilename())
          return
        of "V", "version":
          printVersion(os.getAppFilename())
          return
        else: discard
      of cmdArgument:
        discard

    try:
      let
        target = newNativeTarget()
      var
        units = newSeq[ref IndexUnit]()
        index = newIndex()
        driver = newDriver(target, 0, false, true, false, release)
  
      for path in ["./build.amp"]:
        let 
          directory = path.parentDir
          file = path.lastPathPart
        
        let unit = index.load(path)
        if unit.isNil:
          echo fmt"""
error({errkFileNotFound}): {path} not found
"""
          quit(1)
        units.add(unit)
      let modules = init_modules(driver.type_ctx)
      load_module(modules, ["__lang", "macros"].toPath, driver.type_ctx, driver.macro_ctx, driver.pass_controllers[tsCodeDecl].CgPassController)
      load_module(modules, ["__lang", "macros", "build"].toPath, driver.type_ctx, driver.macro_ctx, driver.pass_controllers[tsCodeDecl].CgPassController)
      load_module(modules, ["__lang", "macros", "pkg"].toPath, driver.type_ctx, driver.macro_ctx, driver.pass_controllers[tsCodeDecl].CgPassController)

      startIndex(index, none[string](), initOrderedSet[string]())
      driver.init(index)
      driver.run_until(index, units[0], tsTypeAnalysis)

      units.setLen(0)

      var
        amp_include = newSeq[string]()
        c_include = newSeq[string]()
        c_define = newSeq[string]()
        system_c = true
        output = none[string]()
        no_linker = false
        debug_info = false
        opt_level = 0
        emit = emitBin
        reloc = relDefault
        shared_lib = false
        package_name = index.pkg_name
        paths = index.compile.toSeq
        link = newSeq[string]()
        link_dir = newSeq[string]()
        compile = index.compile
  
      for (key, val) in index.opts.pairs:
        if val.len == 0:
          continue
        case key
        of "I", "include":
          amp_include.add(val)
        of "C", "c-include":
          c_include.add(val)
        of "D", "define":
          c_define.add(val)
        of "L", "libdir":
          for val in val:
            link_dir.add(fmt"-L{val}")
        of "o", "out":
          output = some(val[^1])
        of "O":
          opt_level = val[^1].parseInt
        of "emit":
          case val[^1]
          of "llvm-ir":
            emit = emitIr
          of "llvm-bc":
            emit = emitBc
          of "asm":
            emit = emitAsm
          of "obj":
            emit = emitObj
          else:
            emit = emitBin
        of "relocation":
          case val[^1]
          of "pie", "pic":
            reloc = relPie
          of "default":
            reloc = relDefault
          of "static":
            reloc = relStatic
          of "dynamic":
            reloc = relDynamic
          else:
            reloc = relDefault
        of "l":
          for val in val:
            link.add(fmt"-l{val}")
        of "N", "package":
          package_name = some(val[^1])
        else: discard
  
      for (key, val) in index.flags.pairs:
        case key
        of "system-c":
          system_c = val != 0
        of "no-system-c":
          system_c = val == 0
        of "c":
          no_linker = val != 0
        of "g", "debuginfo":
          debug_info = val != 0
        of "shared":
          shared_lib = val != 0
        else: discard

      for i in amp_include:
        index.add_include(i)
  
      for i in c_include:
        index.add_c_include(i)
  
      for i in c_define:
        index.add_c_define(i)
  
      for path in paths:
        let 
          directory = path.parentDir
          file = path.lastPathPart
        
        let unit = index.load(path)
        if unit.isNil:
          echo fmt"""
error({errkFileNotFound}): file not found: {path}
"""
          quit(1)
        units.add(unit)
        compile.incl(unit.path)
      driver.update(opt_level, debuginfo, false, false)
      startIndex(index, package_name, compile)
      driver.init(index)

      case command
      of "build":
        let
          module = driver.run(index, units[0])
          output = output.get(package_name.map((pkg_name) => index.target/pkg_name).get(index.target/"bin")).changeFileExt(ext_of(emit))
  
        var err: cstring
        if llvm.verifyModule(module, llvm.ReturnStatusAction, addr err) != 0:
          let module = llvm.printModuleToString(module)
          let msg = fmt"{'\n'}{err}{'\n'}{module}"
          llvm.disposeMessage(module)
          llvm.disposeMessage(err)
          raise newIce(msg)
  
        os.createDir(index.target)
        case emit
        of emitIr:
          emit_to_ir(module, output, reloc)
        of emitBc:
          emit_to_bc(module, output, reloc)
        of emitAsm:
          emit_to_asm(module, output, reloc, opt_level)
        of emitObj:
          emit_to_obj(module, output, reloc, opt_level)
        of emitBin:
          emit_to_bin(module, output, reloc, opt_level, system_c, link, link_dir, shared_lib)
        if run:
          let output = execProcess(output, options = {})
          write stdout, output
      of "check":
        driver.run_until(index, units[0], tsFlowAnalysis)
      of "test":
        driver.test_mode = true
        let
          module = driver.run(index, units[0])
          output = index.target/"tests"/"test"
  
        var err: cstring
        if llvm.verifyModule(module, llvm.ReturnStatusAction, addr err) != 0:
          let module = llvm.printModuleToString(module)
          let msg = fmt"{'\n'}{err}{'\n'}{module}"
          llvm.disposeMessage(module)
          llvm.disposeMessage(err)
          raise newIce(msg)
  
        os.createDir(index.target)
        os.createDir(index.target/"tests")
        emit_to_bin(module, output, relDefault, opt_level, system_c, link, link_dir, false)
        let (outp, test) = execCmdEx("."/output, options = {poStdErrToStdOut})
        if test != 0:
          echo "tests failed!"
          echo "test output:"
          echo outp
          quit(1)
    except Error:
      let e = (ref Error)(getCurrentException())
      write stderr, $e[]
      quit(1)
    except Errors:
      let e = (ref Errors)(getCurrentException())
      write stderr, $e[]
      quit(1)

  proc executeInstall(command: string) =
    proc printHelp(arg0: string) =
      echo fmt"Usage: {arg0} install [options]"
      echo ""
      echo "Output options"
      echo "       --release                            Build in release mode (with optimizations, without debug info)."
      echo "       --debug                              Build in debug mode (without optimizations, with debug info)."
      echo ""
      echo "Misc"
      echo "  -h | --help                               Print this message and exit."
      echo "  -V | --version                            Print version and exit."

    var
      opts = initOptParser(
        os.commandLineParams()[1 ..^ 1].join(" "),
        {'h', 'V'},
        @["help", "version", "release"],
      )
      release = true

    for opt in getopt(opts):
      case opt.kind
      of cmdEnd: break
      of cmdShortOption, cmdLongOption:
        case opt.key
        of "release":
          release = true
        of "debug":
          release = false
        of "h", "help":
          printHelp(os.getAppFilename())
          return
        of "V", "version":
          printVersion(os.getAppFilename())
          return
        else: discard
      of cmdArgument:
        discard

    try:
      let
        target = newNativeTarget()
      var
        units = newSeq[ref IndexUnit]()
        index = newIndex()
        driver = newDriver(target, 0, false, true, true, release)
  
      for path in ["./build.amp"]:
        let 
          directory = path.parentDir
          file = path.lastPathPart
        
        let unit = index.load(path)
        if unit.isNil:
          echo fmt"""
error({errkFileNotFound}): {path} not found
"""
          quit(1)
        units.add(unit)
      let modules = init_modules(driver.type_ctx)
      load_module(modules, ["__lang", "macros"].toPath, driver.type_ctx, driver.macro_ctx, driver.pass_controllers[tsCodeDecl].CgPassController)
      load_module(modules, ["__lang", "macros", "build"].toPath, driver.type_ctx, driver.macro_ctx, driver.pass_controllers[tsCodeDecl].CgPassController)
      load_module(modules, ["__lang", "macros", "pkg"].toPath, driver.type_ctx, driver.macro_ctx, driver.pass_controllers[tsCodeDecl].CgPassController)

      startIndex(index, none[string](), initOrderedSet[string]())
      driver.init(index)
      driver.run_until(index, units[0], tsTypeAnalysis)

      units.setLen(0)

      var
        amp_include = newSeq[string]()
        c_include = newSeq[string]()
        c_define = newSeq[string]()
        system_c = true
        output = none[string]()
        no_linker = false
        debug_info = false
        opt_level = 0
        emit = emitBin
        reloc = relDefault
        shared_lib = false
        package_name = index.pkg_name
        paths = index.compile.toSeq
        link = newSeq[string]()
        link_dir = newSeq[string]()
        compile = index.compile
  
      for (key, val) in index.opts.pairs:
        if val.len == 0:
          continue
        case key
        of "I", "include":
          amp_include.add(val)
        of "C", "c-include":
          c_include.add(val)
        of "D", "define":
          c_define.add(val)
        of "L", "libdir":
          for val in val:
            link_dir.add(fmt"-L{val}")
        of "o", "out":
          output = some(val[^1])
        of "O":
          opt_level = val[^1].parseInt
        of "emit":
          case val[^1]
          of "llvm-ir":
            emit = emitIr
          of "llvm-bc":
            emit = emitBc
          of "asm":
            emit = emitAsm
          of "obj":
            emit = emitObj
          else:
            emit = emitBin
        of "relocation":
          case val[^1]
          of "pie", "pic":
            reloc = relPie
          of "default":
            reloc = relDefault
          of "static":
            reloc = relStatic
          of "dynamic":
            reloc = relDynamic
          else:
            reloc = relDefault
        of "l":
          for val in val:
            link.add(fmt"-l{val}")
        of "N", "package":
          package_name = some(val[^1])
        else: discard
  
      for (key, val) in index.flags.pairs:
        case key
        of "system-c":
          system_c = val != 0
        of "no-system-c":
          system_c = val == 0
        of "c":
          no_linker = val != 0
        of "g", "debuginfo":
          debug_info = val != 0
        of "shared":
          shared_lib = val != 0
        else: discard

      for i in amp_include:
        index.add_include(i)
  
      for i in c_include:
        index.add_c_include(i)
  
      for i in c_define:
        index.add_c_define(i)
  
      for path in paths:
        let 
          directory = path.parentDir
          file = path.lastPathPart
        
        let unit = index.load(path)
        if unit.isNil:
          echo fmt"""
error({errkFileNotFound}): file not found: {path}
"""
          quit(1)
        units.add(unit)
        compile.incl(unit.path)
      driver.update(opt_level, debuginfo, false, false)
      startIndex(index, package_name, compile)
      driver.init(index)

      let
        module = driver.run(index, units[0])
        outp = output.get(package_name.map((pkg_name) => index.target/pkg_name).get(index.target/"bin")).changeFileExt(ext_of(emit))
  
      var err: cstring
      if llvm.verifyModule(module, llvm.ReturnStatusAction, addr err) != 0:
        let module = llvm.printModuleToString(module)
        let msg = fmt"{'\n'}{err}{'\n'}{module}"
        llvm.disposeMessage(module)
        llvm.disposeMessage(err)
        raise newIce(msg)
  
      os.createDir(index.target)
      case emit
      of emitIr:
        emit_to_ir(module, outp, reloc)
      of emitBc:
        emit_to_bc(module, outp, reloc)
      of emitAsm:
        emit_to_asm(module, outp, reloc, opt_level)
      of emitObj:
        emit_to_obj(module, outp, reloc, opt_level)
      of emitBin:
        emit_to_bin(module, outp, reloc, opt_level, system_c, link, link_dir, shared_lib)

      os.createDir("~/.local/lib/etc/pkgs".expandTilde)
      os.createDir("~/.local/lib/etc/lib".expandTilde)
      os.createDir("~/.local/lib/etc/bin".expandTilde)

      if index.meta.name.isSome:
        let bindir = "~/.local/lib/etc/bin".expandTilde
        let libdir = "~/.local/lib/etc/lib".expandTilde
        let pkgdir = if index.meta.version.isSome:
          let pkgdir = "~/.local/lib/etc/pkgs"/(index.meta.name.get & "-" & index.meta.version.get)
          os.copyDir(".", pkgdir.expandTilde)
          pkgdir.expandTilde
        else:
          let pkgdir = "~/.local/lib/etc/pkgs"/index.meta.name.get
          os.copyDir(".", pkgdir.expandTilde)
          pkgdir.expandTilde
        if index.meta.kind.isSome:
          if index.meta.kind.get == "bin" or index.meta.kind.get == "hybrid":
            if os.symlinkExists(bindir/index.meta.name.get):
              os.removeFile(bindir/index.meta.name.get)
            os.createSymlink(pkgdir/outp, bindir/index.meta.name.get)
          if index.meta.kind.get == "lib" or index.meta.kind.get == "hybrid":
            if os.symlinkExists(libdir/index.meta.name.get):
              os.removeFile(libdir/index.meta.name.get)
            os.createSymlink(pkgdir/"src", libdir/index.meta.name.get)
    except Error:
      let e = (ref Error)(getCurrentException())
      write stderr, $e[]
      quit(1)
    except Errors:
      let e = (ref Errors)(getCurrentException())
      write stderr, $e[]
      quit(1)

  if os.commandLineParams().len == 0:
    printHelp(os.getAppFilename())
    quit(1)

  let command = os.commandLineParams()[0]
  case command
  of "compile": executeCE(command)
  of "build": executeBCT(command)
  of "check": executeBCT(command)
  of "expand": executeCE(command)
  of "test": executeBCT(command)
  of "install": executeInstall(command)
  of "help": printHelp(os.getAppFilename())
  of "version": printVersion(os.getAppFilename())
  else:
    echo fmt"""
error({errkUnrecognizedCommand}): unrecognized command: {command}
"""
    quit(1)
