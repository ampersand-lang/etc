import options
import tables
import sets
import sugar
import sequtils
import strutils
import strformat
import os
import hashes
import ../ice
import ../error
import ../path
import ../names
import ../bits
import ../lazy
import ../index
import ../target
import ../print
import ../driver/driver_typedefs
import ../driver/driver
import ../types/types
import ../hlir/hlir
import ../hlir/visitor
import ../macros/typedefs
import ../macros/macros
import ../llvm/clang

proc enter*(type_analysis: TaPassController) =
  type_analysis.polymorphic_stack.add(initTable[(Path, ref QualifiedType), ref Node]())

proc leave(type_analysis: TaPassController) =
  discard type_analysis.polymorphic_stack.pop()

proc is_monomorphized_or_add(type_analysis: TaPassController, path: Path, fn: ref QualifiedType, node: ref Node): bool =
  if (path, fn) in type_analysis.monomorphized:
    return true
  else:
    type_analysis.monomorphized.add((path, fn), node)
    return false

proc get_monomorphized(type_analysis: TaPassController, path: Path, fn: ref QualifiedType): ref Node =
  return type_analysis.monomorphized[(path, fn)]

proc enter_parametric*(type_analysis: TaPassController) =
  type_analysis.parameter_stack.add(initHashSet[string]())

proc leave_parametric(type_analysis: TaPassController) =
  discard type_analysis.parameter_stack.pop()

proc is_parameter(type_analysis: TaPassController, name: string): bool =
  return type_analysis.parameter_stack[^1].anyIt(it.contains(name))

proc add_parameter(type_analysis: TaPassController, name: string) =
  type_analysis.parameter_stack[^1].incl(name)

proc add_polymorphic(type_analysis: TaPassController, path: Path, ty: ref QualifiedType, node: ref Node) =
  type_analysis.polymorphic_stack[^1].add((path, ty), node)

proc get_polymorphic(type_analysis: TaPassController, path: Path, ty: ref QualifiedType): Option[ref Node] =
  for i in 0 ..< type_analysis.polymorphic_stack.len:
    if type_analysis.polymorphic_stack[type_analysis.polymorphic_stack.len - i - 1].contains((path, ty)):
      return some(type_analysis.polymorphic_stack[type_analysis.polymorphic_stack.len - i - 1][(path, ty)].deepCopy)

proc match(type_analysis: TaPassController): ref TypeSum = type_analysis.match_stack[^1]
proc add_match(type_analysis: TaPassController, ty: ref TypeSum) = type_analysis.match_stack.add(ty)
proc leave_match(type_analysis: TaPassController) = discard type_analysis.match_stack.pop()

proc function(type_analysis: TaPassController): Option[Path] =
  if type_analysis.function_stack.len > 0:
    return some(type_analysis.function_stack[^1])
  else:
    return none[Path]()

proc in_macro(type_analysis: TaPassController): bool =
  if type_analysis.in_macro_stack.len > 0:
    return type_analysis.in_macro_stack[^1]
  else:
    return false

proc result_type(type_analysis: TaPassController): Option[ref TypeSum] =
  if type_analysis.result_stack.len > 0:
    return some(type_analysis.result_stack[^1])
  else:
    return none[ref TypeSUm]()

proc enter_function(type_analysis: TaPassController, fn: Path, result_type: ref TypeSum, is_macro: bool) =
  type_analysis.in_macro_stack.add(is_macro)
  type_analysis.result_stack.add(result_type)
  type_analysis.function_stack.add(fn)

proc leave_fn(type_analysis: TaPassController) =
  discard type_analysis.function_stack.pop()
  discard type_analysis.result_stack.pop()
  discard type_analysis.in_macro_stack.pop()

proc module_name(type_analysis: TaPassController): Path =
  type_analysis.function.get(type_analysis.module)

proc replace_params(node: ref Node, type_args: seq[tuple[param: string, arg: ref QualifiedType]])
proc quasiquote(index: ref Index, driver: ref Driver, node: ref Node)
proc mark_type(node: ref Node)
proc replace_capture(node: ref Node, capture: ref seq[tuple[name: string, by_ref: bool]])

proc type_reg*(index: ref Index, driver: ref Driver, root: ref Node, scope: bool = true) =
  let type_analysis = driver.pass_controllers[tsTypeDecl].TaPassController
  let
    pre_visitor = HlirVisitor(
      visit_mod: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        type_analysis.module = self.node.module.path
        type_analysis.module.full = true
      ),
      visit_use_extern: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        proc visitor(cursor: CXCursor, parent: CXCursor, client_data: CXClientData): CXChildVisitResult {.cdecl.} =
          let driver = cast[ptr (ref Driver, ref QualifiedType)](client_data)[][0]
          if clang.isDeclaration(clang.getCursorKind(cursor)) != 0:
            let
              args = clang.cursor_getNumArguments(cursor)
              storage = clang.cursor_getStorageClass(cursor)
              is_extern = storage == CX_SC_Extern
              is_static = storage == CX_SC_Static
              cx_type = clang.getCursorType(cursor)
              canonical = clang.getCanonicalType(cx_type)

            if args >= 0:
              # function declaration
              if not is_static:
                let
                  cs = clang.getCursorSpelling(cursor)
                  name = clang.getString(cs)
                  path = name.toNonFullPath
                var params = newSeqOfCap[Argument](args)
                
                for i in 0 ..< args:
                  params.add(Argument(ty: clang.getArgType(canonical, cuint(i)).to_type(driver.target).toTypeSumRef))
                let is_variadic = clang.isFunctionTypeVariadic(canonical) != 0

                let impl = newFnImpl(path, some(name), false, false, true, clang.getResultType(canonical).to_type(driver.target).toTypeSumRef, params, is_variadic, false, false)
                driver.type_ctx.add_impl(path, impl, 0)
            elif clang.getCursorKind(cursor) == CXCursor_StructDecl:
              var full_name = "__struct_"
              let
                ty = canonical.to_type(driver.target)
                cs = clang.getCursorSpelling(cursor)
                name = clang.getString(cs)
              if name.len > 0:
                full_name.add(name)
                let
                  path = full_name.toNonFullPath
                  impl = newConsImpl(path, some(full_name), false, true, true, ty.toTypeSumRef)
                driver.type_ctx.add(path, ty)
                driver.type_ctx.add_impl(path, impl, 0)
              clang.disposeString(cs)
            elif clang.getCursorKind(cursor) == CXCursor_UnionDecl:
              var full_name = "__union_"
              let
                ty = canonical.to_type(driver.target)
                cs = clang.getCursorSpelling(cursor)
                name = clang.getString(cs)
              if name.len > 0:
                full_name.add(name)
                let
                  path = full_name.toNonFullPath
                  impl = newConsImpl(path, some(full_name), false, true, true, ty.toTypeSumRef)
                driver.type_ctx.add(path, ty)
                driver.type_ctx.add_impl(path, impl, 0)
              clang.disposeString(cs)
            elif clang.getCursorKind(cursor) == CXCursor_EnumDecl:
              var full_name = "__enum_"
              let
                ty = canonical.to_type(driver.target)
                cs = clang.getCursorSpelling(cursor)
                name = clang.getString(cs)
              if name.len > 0:
                full_name.add(name)
                let
                  path = full_name.toNonFullPath
                  impl = newConsImpl(path, some(full_name), false, true, true, ty.toTypeSumRef)
                driver.type_ctx.add(path, ty)
                driver.type_ctx.add_impl(path, impl, 0)
              clang.disposeString(cs)
              cast[ptr (ref Driver, ref QualifiedType)](client_data)[][1] = ty
              return CXChildVisit_Recurse
            elif clang.getCursorKind(cursor) == CXCursor_EnumConstantDecl:
              let
                ty = cast[ptr (ref Driver, ref QualifiedType)](client_data)[][1]
                cs = clang.getCursorSpelling(cursor)
                name = clang.getString(cs)
                path = name.toNonFullPath
                impl = newVarImpl(path, some(path.mangled), false, true, true, ty.toTypeSumRef)
              driver.type_ctx.add_impl(path, impl, 0)
            elif clang.getCursorKind(cursor) == CXCursor_TypedefDecl:
              var
                ty = canonical.to_type(driver.target)
              let
                cs = clang.getCursorSpelling(cursor)
                name = clang.getString(cs)
                path = name.toNonFullPath
                impl = newConsImpl(path, some(name), false, true, true, ty.toTypeSumRef)
              case ty.inner.kind
              of tykStruct:
                if ty.inner.struct.name.isNone:
                  ty.inner.struct.name = some(name.toNonFullPath)
              of tykUnion:
                if ty.inner.union.name.isNone:
                  ty.inner.union.name = some(name.toNonFullPath)
              else:
                discard
              clang.disposeString(cs)
              driver.type_ctx.add(path, ty)
              driver.type_ctx.add_impl(path, impl, 0)
            elif not is_static:
              let
                cs = clang.getCursorSpelling(cursor)
                name = clang.getString(cs)
                path = name.toNonFullPath

              let impl = newVarImpl(path, some(name), false, true, true, canonical.to_type(driver.target).toTypeSumRef)
              driver.type_ctx.add_impl(path, impl, 0)
          return CXChildVisit_Continue

        let
          lang = self.node.use_extern.lang
          file = self.node.use_extern.file
        if lang != "C":
          raise newError(errkUnrecognizedLanguage, self.meta.span)

        var data = (driver, (ref QualifiedType)(nil))
        let
          (ts, cxtu) = index.load_c_with_span(file, self.meta.span)
          root = clang.getTranslationUnitCursor(cxtu)
        if ts < tsTypeDecl:
          clang.visitChildren(root, visitor, cast[ptr cchar](addr data))
          index.update_c_tu(file, tsTypeDecl)
      ),
      visit_trait: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.trait.name.isNone:
          self.node.trait.name = some(Names(short: self.node.trait.name_node.node.expr_named.name.short))

        self.node.trait.name.get.qualify(type_analysis.module_name)
        return vrkRecurse),
      visit_struct: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.struct.name.isNone:
          self.node.struct.name = some(Names(short: self.node.struct.name_node.node.expr_named.name.short))

        self.node.struct.name.get.qualify(type_analysis.module_name)
        return vrkRecurse),
      visit_union: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.struct.name.isNone:
          self.node.struct.name = some(Names(short: self.node.struct.name_node.node.expr_named.name.short))

        self.node.struct.name.get.qualify(type_analysis.module_name)
        return vrkRecurse),
      visit_enum: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.enumeration.name.isNone:
          self.node.enumeration.name = some(Names(short: self.node.enumeration.name_node.node.expr_named.name.short))

        self.node.enumeration.name.get.qualify(type_analysis.module_name)
        return vrkRecurse),
      visit_tagged: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.tagged.name.isNone:
          self.node.tagged.name = some(Names(short: self.node.tagged.name_node.node.expr_named.name.short))

        self.node.tagged.name.get.qualify(type_analysis.module_name)
        return vrkRecurse),
      visit_enum_variant: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.enum_variant.name.isNone:
          self.node.enum_variant.name = some(Names(short: self.node.enum_variant.name_node.node.expr_named.name.short))

        self.node.enum_variant.name.get.qualify(type_analysis.module_name)
        self.node.enum_variant.name.get.mangle()
        return vrkRecurse),
      visit_typedef: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.typedef.name.isNone:
          self.node.typedef.name = some(Names(short: self.node.typedef.name_node.node.expr_named.name.short))

        self.node.typedef.name.get.qualify(type_analysis.module_name)
        self.node.typedef.name.get.mangle_as_type
        return vrkRecurse),
      # pre-find an impl, if it's a __lang::macros::quasiquote or quote, return vrkContinue
      visit_expr_application: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let fn = self.node.expr_application.fn
        case fn.node.kind
        of hrkExprNamed:
          var args = newSeq[Argument]()
          for arg in self.node.expr_application.args:
            case arg.node.kind
            of hrkDecl:
              args.add(Argument(name: arg.node.decl.name.map((name) => name.node.expr_named.name.short.path[^1]), ty: arg.meta.type_sum))
            else:
              args.add(Argument(name: none[string](), ty: arg.meta.type_sum))

          let name = fn.node.expr_named.name.short

          var instance: Instance
          if fn.node.expr_named.type_args.isSome:
            var type_args = newSeq[tuple[name: Option[string], ty: ref QualifiedType]]()
            for arg in fn.node.expr_named.type_args.get:
              case arg.node.kind
              of hrkDecl:
                type_args.add((some(arg.node.decl.name.get.node.expr_named.name.short.path[^1]), arg.meta.own_type.get))
              else:
                type_args.add((none[string](), arg.meta.own_type.get))
            instance = type_analysis.type_ctx.find_impl_with_args(name, none[ref TypeSum](), some(args), type_args, false)
          else:
            instance = type_analysis.type_ctx.find_impl(name, none[ref TypeSum](), some(args), false)
          if instance.impls.len > 0:
            let impl = instance.impls.toSeq[0]

            if impl.kind == ikFn and impl.is_macro:
              if impl.name == ["__lang", "macros", "quote"].toPath or impl.name == ["__lang", "macros", "quasiquote"].toPath:
                return vrkNoRecurse
        else:
          discard
        return vrkRecurse),
      visit_expr_command: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let fn = self.node.expr_command.fn
        case fn.node.kind
        of hrkExprNamed:
          var args = newSeq[Argument]()
          for arg in self.node.expr_command.args:
            case arg.node.kind
            of hrkDecl:
              args.add(Argument(name: arg.node.decl.name.map((name) => name.node.expr_named.name.short.path[^1]), ty: arg.meta.type_sum))
            else:
              args.add(Argument(name: none[string](), ty: arg.meta.type_sum))

          let name = fn.node.expr_named.name.short

          var instance: Instance
          if fn.node.expr_named.type_args.isSome:
            var type_args = newSeq[tuple[name: Option[string], ty: ref QualifiedType]]()
            for arg in fn.node.expr_named.type_args.get:
              case arg.node.kind
              of hrkDecl:
                type_args.add((some(arg.node.decl.name.get.node.expr_named.name.short.path[^1]), arg.meta.own_type.get))
              else:
                type_args.add((none[string](), arg.meta.own_type.get))
            instance = type_analysis.type_ctx.find_impl_with_args_untyped(name, none[ref TypeSum](), some(args), type_args, false)
          else:
            instance = type_analysis.type_ctx.find_impl_untyped(name, none[ref TypeSum](), some(args), false)
          if instance.impls.len > 0:
            let impl = instance.impls.toSeq[0]

            if impl.kind == ikFn and impl.is_macro:
              if impl.name == ["__lang", "macros", "quote"].toPath or impl.name == ["__lang", "macros", "quasiquote"].toPath:
                return vrkNoRecurse
        else: raise newIce("unimplemented")
        return vrkRecurse),
    )
    post_visitor = HlirVisitor(
      visit_trait: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        var trait_type = TypeTrait(
          name: self.node.trait.name.get.full.get,
          fns: newSeqOfCap[StructField](self.node.trait.fns.len),
        )
        let
          ty = newType(Type(kind: tykTrait, trait: trait_type))
          qty = ty.toQualifiedType

        self.meta.own_type = some(qty)

        type_analysis.type_ctx.add(self.node.trait.name.get.full.get, qty)
        
        let impl = newConsImpl(self.node.trait.name.get.full.get, none[string](), false, true, true, self.meta.own_type.get.toTypeSumRef)
        self.meta.impl = some(impl)
        type_analysis.type_ctx.add_impl(self.node.trait.name.get.short, impl)
        type_analysis.type_ctx.add_impl(self.node.trait.name.get.full.get, impl, 0)

        return vrkRecurse),
      visit_struct: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        var struct_type = TypeStruct(
          name: some(self.node.struct.name.get.full.get),
          fields: newSeqOfCap[StructField](self.node.struct.fields.len),
        )
        let
          ty = newType(Type(kind: tykStruct, struct: struct_type))
          qty = ty.toQualifiedType

        self.meta.own_type = some(qty)

        type_analysis.type_ctx.add(self.node.struct.name.get.full.get, qty)
        
        let impl = newConsImpl(self.node.struct.name.get.full.get, none[string](), false, true, true, self.meta.own_type.get.toTypeSumRef)
        self.meta.impl = some(impl)
        type_analysis.type_ctx.add_impl(self.node.struct.name.get.short, impl)
        type_analysis.type_ctx.add_impl(self.node.struct.name.get.full.get, impl, 0)

        var type_params = newSeq[ref Node]()
        while self.node.struct.fields.len > 0:
          if self.node.struct.fields[0].is_type_decl:
            type_params.add(self.node.struct.fields[0])
            self.node.struct.fields.delete(0)
          else:
            break

        if type_params.len > 0:
          self.node.struct.type_params = some(type_params)
          for param in type_params:
            let name = param.node.decl.name.get.node.expr_named.name.short.path[^1]
            let ty = newType(Type(kind: tykParameter, param: TypeParameter(kind: pkType, name: param.node.decl.name.get.node.expr_named.name.short.path[^1]))).toQualifiedType
            self.meta.own_type.get.args.add(name, ty)

        return vrkRecurse),
      visit_union: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        var struct_type = TypeStruct(
          name: some(self.node.struct.name.get.full.get),
          fields: newSeqOfCap[StructField](self.node.struct.fields.len),
        )
        let
          ty = newType(Type(kind: tykUnion, union: struct_type))
          qty = ty.toQualifiedType

        self.meta.own_type = some(qty)

        type_analysis.type_ctx.add(self.node.struct.name.get.full.get, qty)

        let impl = newConsImpl(self.node.struct.name.get.full.get, none[string](), false, true, true, self.meta.own_type.get.toTypeSumRef)
        self.meta.impl = some(impl)
        type_analysis.type_ctx.add_impl(self.node.struct.name.get.short, impl)
        type_analysis.type_ctx.add_impl(self.node.struct.name.get.full.get, impl, 0)

        var type_params = newSeq[ref Node]()
        while self.node.struct.fields.len > 0:
          if self.node.struct.fields[0].is_type_decl:
            type_params.add(self.node.struct.fields[0])
            self.node.struct.fields.delete(0)
          else:
            break

        if type_params.len > 0:
          self.node.struct.type_params = some(type_params)
          for param in type_params:
            let name = param.node.decl.name.get.node.expr_named.name.short.path[^1]
            let ty = newType(Type(kind: tykParameter, param: TypeParameter(kind: pkType, name: param.node.decl.name.get.node.expr_named.name.short.path[^1]))).toQualifiedType
            self.meta.own_type.get.args.add(name, ty)

        return vrkRecurse),
      visit_enum: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        var bits: uint8
        case self.node.enumeration.variants[^1].node.enum_variant.value.get.node.expr_int.value
        of 0 .. 255: bits = 8
        of 256 .. 65535: bits = 16
        of 65536 .. 4_294_967_295: bits = 32
        else: bits = 64
        let inner = newType(Type(kind: tykUnsigned, unsigned: TypeUnsigned(bits: Bits(kind: bitsCustom, bits: bits)))).toQualifiedType
        var enum_type = TypeEnum(
          name: self.node.enumeration.name.get.full.get,
          inner: inner,
        )
        let
          ty = newType(Type(kind: tykEnum, enumeration: enum_type))
          qty = ty.toQualifiedType
        self.node.enumeration.name.get.mangled = some(qty.mangled)
        self.meta.own_type = some(qty)
        type_analysis.type_ctx.add(self.node.enumeration.name.get.full.get, qty)

        for enumeration in self.node.enumeration.variants:
          let impl = newVarImpl(enumeration.node.enum_variant.name.get.full.get, none[string](), false, true, true, self.meta.own_type.get.toTypeSumRef)
          enumeration.meta.impl = some(impl)
          type_analysis.type_ctx.add_impl(enumeration.node.enum_variant.name.get.short, impl)
          type_analysis.type_ctx.add_impl(enumeration.node.enum_variant.name.get.full.get, impl, 0)

        let impl = newConsImpl(self.node.enumeration.name.get.full.get, none[string](), false, true, true, self.meta.own_type.get.toTypeSumRef)
        self.meta.impl = some(impl)
        type_analysis.type_ctx.add_impl(self.node.enumeration.name.get.short, impl)
        type_analysis.type_ctx.add_impl(self.node.enumeration.name.get.full.get, impl, 0)

        return vrkRecurse),
      visit_tagged: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        var tagged_type = TypeTagged(
          name: self.node.tagged.name.get.full.get,
          variants: newSeqOfCap[TaggedVariant](self.node.tagged.variants.len),
          fields: newSeqOfCap[StructField](self.node.tagged.fields.len),
        )
        let
          ty = newType(Type(kind: tykTagged, tagged: tagged_type))
          qty = ty.toQualifiedType
        self.meta.own_type = some(qty)

        for i, variant in self.node.tagged.variants.mpairs:
          variant.node.tagged_variant.name.get.qualify(type_analysis.module_name)

          let
            struct_type = TypeStruct(
              name: some(variant.node.tagged_variant.name.get.full.get),
              fields: @[],
            )
            sty = newType(Type(kind: tykStruct, struct: struct_type))
            qty = sty.toQualifiedType

          variant.meta.own_type = some(qty)
          type_analysis.type_ctx.add(variant.node.tagged_variant.name.get.full.get, qty)

          let impl = newConsImpl(variant.node.tagged_variant.name.get.full.get, none[string](), false, true, true, self.meta.own_type.get.toTypeSumRef)
          variant.meta.impl = some(impl)
          type_analysis.type_ctx.add_impl(variant.node.tagged_variant.name.get.short, impl)
          type_analysis.type_ctx.add_impl(variant.node.tagged_variant.name.get.full.get, impl, 0)

        type_analysis.type_ctx.add(self.node.tagged.name.get.full.get, qty)

        let impl = newConsImpl(self.node.tagged.name.get.full.get, none[string](), false, true, true, self.meta.own_type.get.toTypeSumRef)
        self.meta.impl = some(impl)
        type_analysis.type_ctx.add_impl(self.node.tagged.name.get.short, impl)
        type_analysis.type_ctx.add_impl(self.node.tagged.name.get.full.get, impl, 0)

        var type_params = newSeq[ref Node]()
        while self.node.tagged.fields.len > 0:
          if self.node.tagged.fields[0].is_type_decl:
            type_params.add(self.node.tagged.fields[0])
            self.node.tagged.fields.delete(0)
          else:
            break

        if type_params.len > 0:
          self.node.tagged.type_params = some(type_params)
          for param in type_params:
            let name = param.node.decl.name.get.node.expr_named.name.short.path[^1]
            let ty = newType(Type(kind: tykParameter, param: TypeParameter(kind: pkType, name: param.node.decl.name.get.node.expr_named.name.short.path[^1]))).toQualifiedType
            self.meta.own_type.get.args.add(name, ty)
            for i, variant in self.node.tagged.variants.mpairs:
              variant.meta.own_type.get.args.add(name, ty)

        return vrkRecurse),
      visit_typedef: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        # placeholder
        let ty = newType(Type()).toQualifiedType
        self.meta.own_type = some(ty)
        type_analysis.type_ctx.add(self.node.typedef.name.get.full.get, ty)

        case ty.inner.kind
        of tykStruct:
          let impl = newConsImpl(self.node.typedef.name.get.full.get, none[string](), false, true, true, ty.toTypeSumRef)
          self.meta.impl = some(impl)
          type_analysis.type_ctx.add_impl(self.node.typedef.name.get.short, impl)
          type_analysis.type_ctx.add_impl(self.node.typedef.name.get.full.get, impl, ^2)
        of tykUnion:
          let impl = newConsImpl(self.node.typedef.name.get.full.get, none[string](), false, true, true, ty.toTypeSumRef)
          self.meta.impl = some(impl)
          type_analysis.type_ctx.add_impl(self.node.typedef.name.get.short, impl)
          type_analysis.type_ctx.add_impl(self.node.typedef.name.get.full.get, impl, ^2)
        of tykEnum:
          let impl = newConsImpl(self.node.typedef.name.get.full.get, none[string](), false, true, true, ty.toTypeSumRef)
          self.meta.impl = some(impl)
          type_analysis.type_ctx.add_impl(self.node.typedef.name.get.short, impl)
          type_analysis.type_ctx.add_impl(self.node.typedef.name.get.full.get, impl, ^2)
          raise newError(errkUnimplemented, self.meta.span, "enum typedefs")
        of tykTagged:
          for variant in ty.inner.tagged.variants:
            let short = self.node.typedef.name.get.short & variant.name.path[^1]
            let long = self.node.typedef.name.get.full.get & variant.name.path[^1]

            let impl = newConsImpl(long, none[string](), false, true, true, ty.toTypeSumRef)
            self.meta.impl = some(impl)
            type_analysis.type_ctx.add_impl(short, impl)
            type_analysis.type_ctx.add_impl(long, impl, ^2)
        else:
          let impl = newConsImpl(self.node.typedef.name.get.full.get, some(ty.mangled), false, true, true, ty.toTypeSumRef)
          self.meta.impl = some(impl)
          type_analysis.type_ctx.add_impl(self.node.typedef.name.get.short, impl)
          type_analysis.type_ctx.add_impl(self.node.typedef.name.get.full.get, impl, ^2)

        return vrkRecurse),
    )
  discard accept(root, pre_visitor, post_visitor)

proc type_reg*(index: ref Index, driver: ref Driver, node: ref Node, root: ref Node, scope: bool = true) =
  let type_analysis = driver.pass_controllers[tsTypeDecl].TaPassController
  if scope and root.meta.impl_scope.isSome:
    type_analysis.type_ctx.enter_this_bounded(root.meta.impl_scope.get)
  elif scope:
    type_analysis.type_ctx.enter_bounded
  type_reg(index, driver, node, scope)
  if scope:
    root.meta.impl_scope = some(type_analysis.type_ctx.leave_this_bounded)

proc type_gen*(index: ref Index, driver: ref Driver, root: ref Node, scope: bool = true) =
  let type_analysis = driver.pass_controllers[tsTypeDecl].TaPassController
  let
    pre_visitor = HlirVisitor(
      visit_mod: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        type_analysis.module = self.node.module.path
        type_analysis.module.full = true
      ),
      visit_use: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let
          module = self.node.use.path
          unit = index.load_with_span(module, "amp", self.meta.span)
          self_module = type_analysis.module
        driver.run_until(index, unit, tsTypeDecl)
        type_analysis.module = self_module
      ),
      visit_with: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let module = self.node.with_single.path
        type_analysis.type_ctx.import_with(module)
      ),
      visit_function: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.function.name.isNone and self.node.function.name_node.isSome:
          self.node.function.name = some(Names(short: self.node.function.name_node.get.node.expr_named.name.short))

        type_analysis.type_ctx.enter_bounded
        type_analysis.enter_parametric

        case self.node.function.ty.node.kind
        of hrkTypeFunction:
          let params = self.node.function.ty.node.type_function.params
          for param in params:
            if param.is_type_decl:
              type_analysis.add_parameter(param.node.decl.name.get.node.expr_named.name.short.path[^1])
        of hrkExprBinary:
          let params = self.node.function.ty.node.expr_binary.lhs
          case params.node.kind
          of hrkExprUnit:
            discard
          of hrkExprParenth:
            for param in params.node.expr_parenth.elems:
              if param.is_type_decl:
                type_analysis.add_parameter(param.node.decl.name.get.node.expr_named.name.short.path[^1])
          of hrkDecl:
            let param = params
            if param.is_type_decl:
              type_analysis.add_parameter(param.node.decl.name.get.node.expr_named.name.short.path[^1])
          else:
            raise newError(errkMalformedHlir, self.meta.span)
        else:
          raise newError(errkMalformedHlir, self.meta.span)

        return vrkRecurse),
      visit_macro: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.macrofn.name.isNone and self.node.macrofn.name_node.isSome:
          self.node.macrofn.name = some(Names(short: self.node.macrofn.name_node.get.node.expr_named.name.short))

        type_analysis.type_ctx.enter_bounded
        type_analysis.enter_parametric

        case self.node.macrofn.ty.node.kind
        of hrkTypeFunction:
          let params = self.node.macrofn.ty.node.type_function.params
          for param in params:
            if param.is_type_decl:
              type_analysis.add_parameter(param.node.decl.name.get.node.expr_named.name.short.path[^1])
        of hrkExprBinary:
          let params = self.node.macrofn.ty.node.expr_binary.lhs
          case params.node.kind
          of hrkExprUnit:
            discard
          of hrkExprParenth:
            for param in params.node.expr_parenth.elems:
              if param.is_type_decl:
                type_analysis.add_parameter(param.node.decl.name.get.node.expr_named.name.short.path[^1])
          of hrkDecl:
            let param = params
            if param.is_type_decl:
              type_analysis.add_parameter(param.node.decl.name.get.node.expr_named.name.short.path[^1])
          else:
            raise newError(errkMalformedHlir, self.meta.span)
        else:
          raise newError(errkMalformedHlir, self.meta.span)

        return vrkRecurse),
      visit_trait: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        type_analysis.enter_parametric

        if self.node.trait.type_params.isSome:
          for field in self.node.trait.type_params.get:
            type_analysis.add_parameter(field.node.decl.name.get.node.expr_named.name.short.path[^1])

        return vrkRecurse),
      visit_struct: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        type_analysis.enter_parametric

        if self.node.struct.type_params.isSome:
          for field in self.node.struct.type_params.get:
            type_analysis.add_parameter(field.node.decl.name.get.node.expr_named.name.short.path[^1])

        return vrkRecurse),
      visit_union: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        type_analysis.enter_parametric

        if self.node.struct.type_params.isSome:
          for field in self.node.struct.type_params.get:
            type_analysis.add_parameter(field.node.decl.name.get.node.expr_named.name.short.path[^1])
  
        return vrkRecurse),
      visit_tagged: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        type_analysis.enter_parametric

        if self.node.tagged.type_params.isSome:
          for field in self.node.tagged.type_params.get:
            type_analysis.add_parameter(field.node.decl.name.get.node.expr_named.name.short.path[^1])
  
        return vrkRecurse),
      visit_expr_sizeof: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        mark_type(self.node.expr_sizeof.value)
        return vrkRecurse),
      visit_expr_alignof: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        mark_type(self.node.expr_alignof.value)
        return vrkRecurse),
      # pre-find an impl, if it's a __lang::macros::quasiquote or quote, return vrkContinue
      visit_expr_application: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let fn = self.node.expr_application.fn
        case fn.node.kind
        of hrkExprNamed:
          var args = newSeq[Argument]()
          for arg in self.node.expr_application.args:
            case arg.node.kind
            of hrkDecl:
              args.add(Argument(name: arg.node.decl.name.map((name) => name.node.expr_named.name.short.path[^1]), ty: arg.meta.type_sum))
            else:
              args.add(Argument(name: none[string](), ty: arg.meta.type_sum))

          let name = fn.node.expr_named.name.short

          var instance: Instance
          if fn.node.expr_named.type_args.isSome:
            var type_args = newSeq[tuple[name: Option[string], ty: ref QualifiedType]]()
            for arg in fn.node.expr_named.type_args.get:
              case arg.node.kind
              of hrkDecl:
                type_args.add((some(arg.node.decl.name.get.node.expr_named.name.short.path[^1]), arg.meta.own_type.get))
              else:
                type_args.add((none[string](), arg.meta.own_type.get))
            instance = type_analysis.type_ctx.find_impl_with_args(name, none[ref TypeSum](), some(args), type_args, false)
          else:
            instance = type_analysis.type_ctx.find_impl(name, none[ref TypeSum](), some(args), false)
          if instance.impls.len > 0:
            let impl = instance.impls.toSeq[0]

            if impl.kind == ikFn and impl.is_macro:
              if impl.name == ["__lang", "macros", "quote"].toPath or impl.name == ["__lang", "macros", "quasiquote"].toPath:
                return vrkNoRecurse
        else:
          discard
        return vrkRecurse),
      visit_expr_command: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let fn = self.node.expr_command.fn
        case fn.node.kind
        of hrkExprNamed:
          var args = newSeq[Argument]()
          for arg in self.node.expr_command.args:
            case arg.node.kind
            of hrkDecl:
              args.add(Argument(name: arg.node.decl.name.map((name) => name.node.expr_named.name.short.path[^1]), ty: arg.meta.type_sum))
            else:
              args.add(Argument(name: none[string](), ty: arg.meta.type_sum))

          let name = fn.node.expr_named.name.short

          var instance: Instance
          if fn.node.expr_named.type_args.isSome:
            var type_args = newSeq[tuple[name: Option[string], ty: ref QualifiedType]]()
            for arg in fn.node.expr_named.type_args.get:
              case arg.node.kind
              of hrkDecl:
                type_args.add((some(arg.node.decl.name.get.node.expr_named.name.short.path[^1]), arg.meta.own_type.get))
              else:
                type_args.add((none[string](), arg.meta.own_type.get))
            instance = type_analysis.type_ctx.find_impl_with_args_untyped(name, none[ref TypeSum](), some(args), type_args, false)
          else:
            instance = type_analysis.type_ctx.find_impl_untyped(name, none[ref TypeSum](), some(args), false)
          if instance.impls.len > 0:
            let impl = instance.impls.toSeq[0]

            if impl.kind == ikFn and impl.is_macro:
              if impl.name == ["__lang", "macros", "quote"].toPath or impl.name == ["__lang", "macros", "quasiquote"].toPath:
                return vrkNoRecurse
        else: raise newIce("unimplemented")
        return vrkRecurse),
    )
    post_visitor = HlirVisitor(
      visit_function: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.function.ty.node.kind != hrkTypeFunction:
          raise newError(errkInvalidType, self.meta.span, "function type is not a type")

        let ty = self.node.function.ty
        var type_params = newSeq[ref Node]()
        while ty.node.type_function.params.len > 0:
          if ty.node.type_function.params[0].is_type_decl:
            type_params.add(ty.node.type_function.params[0])
            ty.node.type_function.params.delete(0)
          else:
            break

        if type_params.len > 0:
          self.node.function.type_params = some(type_params)

        type_analysis.leave_parametric
        self.meta.impl_scope = some(type_analysis.type_ctx.leave_this_bounded)
        return vrkRecurse),
      visit_macro: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.macrofn.ty.node.kind != hrkTypeFunction:
          raise newError(errkInvalidType, self.meta.span, "function type is not a type")

        let ty = self.node.macrofn.ty
        var type_params = newSeq[ref Node]()
        while ty.node.type_function.params.len > 0:
          if ty.node.type_function.params[0].is_type_decl:
            type_params.add(ty.node.type_function.params[0])
            ty.node.type_function.params.delete(0)
          else:
            break

        if type_params.len > 0:
          self.node.macrofn.type_params = some(type_params)

        type_analysis.leave_parametric
        self.meta.impl_scope = some(type_analysis.type_ctx.leave_this_bounded)
        return vrkRecurse),
      visit_trait: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        var type_params = newSeq[ref Node]()
        while self.node.trait.fns.len > 0:
          if self.node.trait.fns[0].is_type_decl:
            type_params.add(self.node.trait.fns[0])
            self.node.trait.fns.delete(0)
          else:
            break

        if type_params.len > 0:
          self.node.trait.type_params = some(type_params)
          for param in type_params:
            let name = param.node.decl.name.get.node.expr_named.name.short.path[^1]
            let ty = newType(Type(kind: tykParameter, param: TypeParameter(kind: pkType, name: param.node.decl.name.get.node.expr_named.name.short.path[^1]))).toQualifiedType
            self.meta.own_type.get.args.add(name, ty)

        type_analysis.leave_parametric

        for i, field in self.node.trait.fns.pairs:
          mark_type(field)
          if field.node.decl.value.meta.own_type.isNone or field.node.decl.value.meta.own_type.get.inner.kind != tykFunction:
            raise newError(errkInvalidType, self.meta.span, "function type is not a type")
          let field = field.node.decl
          let ty = field.value.meta.own_type.get

          self.meta.own_type
            .get
            .inner
            .trait
            .fns
            .add(StructField(
              name: field.name.map((name) => name.node.expr_named.name.short.path[^1]),
              ty: ty.toTypeSumRef,
            ))

        self.meta.own_type.get.regenerate
        
        if self.node.trait.type_params.isNone:
          self.node.trait.name.get.mangled = some(self.meta.own_type.get.mangled)

        return vrkRecurse),
      visit_struct: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        type_analysis.leave_parametric

        for i, field in self.node.struct.fields.pairs:
          mark_type(field)
          if field.node.decl.value.meta.own_type.isNone:
            raise newError(errkInvalidType, field.node.decl.value.meta.span, "field type is not a type")
          let field = field.node.decl
          let ty = field.value.meta.own_type.get

          self.meta.own_type
            .get
            .inner
            .struct
            .fields
            .add(StructField(
              name: field.name.map((name) => name.node.expr_named.name.short.path[^1]),
              ty: ty.toTypeSumRef,
            ))

        self.meta.own_type.get.regenerate
        
        if self.node.struct.type_params.isNone:
          self.node.struct.name.get.mangled = some(self.meta.own_type.get.mangled)

        return vrkRecurse),
      visit_union: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        type_analysis.leave_parametric

        for i, field in self.node.struct.fields.pairs:
          mark_type(field)
          if field.node.decl.value.meta.own_type.isNone:
            raise newError(errkInvalidType, field.node.decl.value.meta.span, "field type is not a type")
          let field = field.node.decl
          let ty = field.value.meta.own_type.get

          self.meta.own_type
            .get
            .inner
            .union
            .fields
            .add(StructField(
              name: field.name.map((name) => name.node.expr_named.name.short.path[^1]),
              ty: ty.toTypeSumRef,
            ))

        self.meta.own_type.get.regenerate

        if self.node.struct.type_params.isNone:
          self.node.struct.name.get.mangled = some(self.meta.own_type.get.mangled)
        
        return vrkRecurse),
      visit_tagged: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        type_analysis.leave_parametric

        for i, field in self.node.tagged.fields.pairs:
          mark_type(field)
          if field.node.decl.value.meta.own_type.isNone:
            raise newError(errkInvalidType, field.node.decl.value.meta.span, "field type is not a type")
          let field = field.node.decl
          let ty = field.value.meta.own_type.get

          self.meta.own_type
            .get
            .inner
            .tagged
            .fields
            .add(StructField(
              name: field.name.map((name) => name.node.expr_named.name.short.path[^1]),
              ty: ty.toTypeSumRef,
            ))

        for i, variant in self.node.tagged.variants.mpairs:
          variant.node.tagged_variant.name.get.qualify(type_analysis.module_name)

          let
            qty = variant.meta.own_type.get

          for j, field in variant.node.tagged_variant.fields.pairs:
            mark_type(field)
            if field.node.decl.value.meta.own_type.isNone:
              raise newError(errkInvalidType, field.node.decl.value.meta.span, "field type is not a type")
            let field = field.node.decl
            let ty = field.value.meta.own_type.get
            qty.inner.struct.fields.add(StructField(name: field.name.map((name) => name.node.expr_named.name.short.path[^1]), ty: ty.toTypeSumRef))

          if self.node.tagged.type_params.isNone:
            variant.node.tagged_variant.name.get.mangled = some(qty.mangled)

          variant.meta.own_type.get.regenerate

          self.meta.own_type.get.inner.tagged.variants.add(TaggedVariant(name: variant.node.tagged_variant.name.get.full.get, fields: qty.inner.struct.fields))

        self.meta.own_type.get.regenerate

        if self.node.tagged.type_params.isNone:
          self.node.tagged.name.get.mangled = some(self.meta.own_type.get.mangled)

        return vrkRecurse),
      visit_typedef: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.typedef.ty.meta.own_type.isNone:
          raise newError(errkInvalidType, self.node.typedef.ty.meta.span, "type expression is not a type")
        let ty = self.node.typedef.ty.meta.own_type.get
        self.meta.own_type.get[] = ty[]
        type_analysis.type_ctx.add(self.node.typedef.name.get.full.get, ty)

        case ty.inner.kind
        of tykStruct:
          let impl = newConsImpl(self.node.typedef.name.get.full.get, some(ty.mangled), false, true, true, ty.toTypeSumRef)
          self.meta.impl = some(impl)
          type_analysis.type_ctx.add_impl(self.node.typedef.name.get.short, impl)
          type_analysis.type_ctx.add_impl(self.node.typedef.name.get.full.get, impl, ^2)
        of tykUnion:
          let impl = newConsImpl(self.node.typedef.name.get.full.get, some(ty.mangled), false, true, true, ty.toTypeSumRef)
          self.meta.impl = some(impl)
          type_analysis.type_ctx.add_impl(self.node.typedef.name.get.short, impl)
          type_analysis.type_ctx.add_impl(self.node.typedef.name.get.full.get, impl, ^2)
        of tykEnum:
          let impl = newConsImpl(self.node.typedef.name.get.full.get, some(ty.mangled), false, true, true, ty.toTypeSumRef)
          self.meta.impl = some(impl)
          type_analysis.type_ctx.add_impl(self.node.typedef.name.get.short, impl)
          type_analysis.type_ctx.add_impl(self.node.typedef.name.get.full.get, impl, ^2)
          raise newError(errkUnimplemented, self.meta.span, "enum typedefs")
        of tykTagged:
          for variant in ty.inner.tagged.variants:
            let short = self.node.typedef.name.get.short & variant.name.path[^1]
            let long = self.node.typedef.name.get.full.get & variant.name.path[^1]

            let impl = newConsImpl(long, some(ty.mangled), false, true, true, ty.toTypeSumRef)
            self.meta.impl = some(impl)
            type_analysis.type_ctx.add_impl(short, impl)
            type_analysis.type_ctx.add_impl(long, impl, ^2)
        else:
          let impl = newConsImpl(self.node.typedef.name.get.full.get, some(ty.mangled), false, true, true, ty.toTypeSumRef)
          self.meta.impl = some(impl)
          type_analysis.type_ctx.add_impl(self.node.typedef.name.get.short, impl)
          type_analysis.type_ctx.add_impl(self.node.typedef.name.get.full.get, impl, ^2)

        return vrkRecurse),
      visit_type_type: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.own_type = some(self.to_type(type_analysis.type_ctx))
        return vrkRecurse),
      visit_type_pointer: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.own_type = some(newType(Type(kind: tykPointer, point: TypePointer(pointee: self.node.type_pointer.pointee.meta.own_type.get.toTypeSumRef))).toQualifiedType)
        mark_type(self)
        return vrkRecurse),
      visit_type_array: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.own_type = some(newType(Type(kind: tykArray, arr: TypeArray(elem: self.node.type_array.elem.meta.own_type.get.toTypeSumRef, size: self.node.type_array.size.meta.own_type.get.toTypeSumRef))).toQualifiedType)
        mark_type(self)
        return vrkRecurse),
      visit_type_slice: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.own_type = some(newType(Type(kind: tykSlice, slice: TypeSlice(elem: self.node.type_slice.elem.meta.own_type.get.toTypeSumRef))).toQualifiedType)
        mark_type(self)
        return vrkRecurse),
      visit_type_var: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.own_type = some(newType(Type(kind: tykVar, vararr: TypeVar(elem: self.node.type_var.elem.meta.own_type.get.toTypeSumRef))).toQualifiedType)
        mark_type(self)
        return vrkRecurse),
      visit_type_function: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.own_type = some(newType(Type(
          kind: tykFunction,
          function: TypeFunction(
            result_type: self.node.type_function.result_type.meta.own_type.get.toTypeSumRef,
            params: self.node.type_function.params.mapIt(Argument(name: it.node.decl.name.map((name) => name.node.expr_named.name.short.path[^1]), ty: it.node.decl.value.meta.own_type.get.toTypeSumRef)),
          )
        )).toQualifiedType)
        mark_type(self)
        return vrkRecurse),
      visit_type_never: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.own_type = some(self.to_type(type_analysis.type_ctx))
        return vrkRecurse),
      visit_type_unit: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.own_type = some(self.to_type(type_analysis.type_ctx))
        return vrkRecurse),
      visit_type_bool: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.own_type = some(self.to_type(type_analysis.type_ctx))
        return vrkRecurse),
      visit_type_signed: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.own_type = some(self.to_type(type_analysis.type_ctx))
        return vrkRecurse),
      visit_type_unsigned: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.own_type = some(self.to_type(type_analysis.type_ctx))
        return vrkRecurse),
      visit_type_floating: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.own_type = some(self.to_type(type_analysis.type_ctx))
        return vrkRecurse),
      visit_expr_parenth: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.expr_parenth.elems.allIt(it.is_type):
          # TODO: tuple type
          mark_type(self)
          discard
        return vrkRecurse),
      visit_expr_unary: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        case self.node.expr_unary.op
        of hukRef:
          if self.node.expr_unary.value.meta.own_type.isSome:
            self[] = newNode(newMeta(self.meta.span), Hlir(kind: hrkTypePointer, type_pointer: HlirTypePointer(pointee: self.node.expr_unary.value)))[]
            return vrkRepeat
        of hukRangeTo:
          if self.node.expr_unary.value.meta.own_type.isSome:
            self[] = newNode(newMeta(self.meta.span), Hlir(kind: hrkTypeVar, type_var: HlirTypeVar(elem: self.node.expr_unary.value)))[]
            return vrkRepeat
        else:
          discard
        return vrkRecurse),
      visit_expr_binary: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        case self.node.expr_binary.op
        of hbkArrow:
          var params = newSeq[ref Node]()
          let args = self.node.expr_binary.lhs
          case args.node.kind
          of hrkExprUnit:
            discard
          of hrkExprParenth:
            if args.node.expr_parenth.elems.allIt(it.meta.own_type.isSome):
              params.add(args.node.expr_parenth.elems)
            else:
              return vrkRecurse
          else:
            if args.meta.own_type.isSome:
              params.add(args)
            else:
              return vrkRecurse
          let result_type = self.node.expr_binary.rhs
          self[] = newNode(
            newMeta(self.meta.span),
            Hlir(
              kind: hrkTypeFunction,
              type_function: HlirTypeFunction(params: params, result_type: result_type)
            )
          )[]
          return vrkRepeat
        else:
          return vrkRecurse),
      visit_expr_array: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.expr_array.elems.len != 0 and
            self.node.expr_array.elems[0].meta.own_type.isSome:
          if self.node.expr_array.elems.len == 2:
            self[] = newNode(newMeta(self.meta.span), Hlir(kind: hrkTypeArray, type_array: HlirTypeArray(elem: self.node.expr_array.elems[0], size: self.node.expr_array.elems[1])))[]
            return vrkRepeat
          elif self.node.expr_array.elems.len == 1:
            self[] = newNode(newMeta(self.meta.span), Hlir(kind: hrkTypeSlice, type_slice: HlirTypeSlice(elem: self.node.expr_array.elems[0])))[]
            return vrkRepeat
          else:
            raise newError(errkInvalidType, self.meta.span)
        return vrkRecurse),
      visit_expr_application: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        case self.node.expr_application.fn.node.kind
        of hrkExprNamed:
          var type_args = newSeq[ref Node]()
          while self.node.expr_application.args.len > 0:
            if self.node.expr_application.args[0].meta.own_type.isSome:
              type_args.add(self.node.expr_application.args[0])
              self.node.expr_application.args.delete(0)
            else:
              break

          if type_args.len > 0:
            self.node.expr_application.fn.node.expr_named.type_args = some(type_args)

          if self.node.expr_application.fn.node.expr_named.type_args.isSome:
            type_args = self.node.expr_application.fn.node.expr_named.type_args.get

          let name = self.node.expr_application.fn.node.expr_named.name.short

          var
            instance: Instance
            args = newSeq[tuple[name: Option[string], ty: ref QualifiedType]]()
          if type_args.len > 0:
            for arg in type_args:
              case arg.node.kind
              of hrkDecl:
                args.add((some(arg.node.decl.name.get.node.expr_named.name.short.path[^1]), arg.meta.own_type.get))
              else:
                args.add((none[string](), arg.meta.own_type.get))
          instance = type_analysis.type_ctx.find_impl(name, none[ref TypeSum](), none[seq[Argument]](), false)

          if instance.impls.len == 0:
            discard
          elif instance.impls.len == 1:
            let impl = instance.impls.toSeq[0]
            case impl.kind
            of ikCons:
              let type_args = args.mapIt((it[0].get, it[1]))
              var ty = impl.ty[].as_qualified_in_span(self.meta.span)
              if type_args.len > 0:
                self.meta.prepare_args = some(type_args)
              if self.node.expr_application.args.len == 0:
                var own_type: ref QualifiedType
                new own_type
                own_type[] = ty[]
                self.meta.own_type = some(own_type)
              var sty = type_analysis.type_ctx.get(impl.name)
              var own_stype: ref QualifiedType
              new own_stype
              own_stype[] = sty[]
              if type_args.len > 0:
                self.node.expr_application.fn.meta.prepare_args = some(type_args)
              self.node.expr_application.fn.meta.own_type = some(own_stype)
              self.node.expr_application.fn.node.expr_named.name.full = some(impl.name)
              mark_type(self.node.expr_application.fn)
            else:
              discard
          else:
            discard
        else:
          discard

        return vrkRecurse),
      visit_expr_let: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.expr_let.ty.isSome:
          mark_type(self.node.expr_let.ty.get)
        return vrkRecurse),
      visit_expr_cast: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        mark_type(self.node.expr_cast.ty)
        return vrkRecurse),
      visit_expr_named: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if (parent.node.kind != hrkDecl or parent.node.decl.name.isNone or (addr self[]) != (addr parent.node.decl.name.get[])) and
            not self.node.expr_named.quiet:
          if type_analysis.is_parameter(self.node.expr_named.name.short.path[^1]):
            self.meta.own_type = some(newType(Type(kind: tykParameter, param: TypeParameter(kind: pkType, name: self.node.expr_named.name.short.path[^1]))).toQualifiedType)
          elif (parent.node.kind != hrkExprApplication or (addr self[]) != (addr parent.node.expr_application.fn[])) and
              (parent.node.kind != hrkExprCommand or (addr self[]) != (addr parent.node.expr_command.fn[])):
            let name = self.node.expr_named.name.short
            var type_args: seq[ref Node]
            if self.node.expr_named.type_args.isSome:
              type_args = self.node.expr_named.type_args.get

            var
              instance: Instance
              args = newSeq[tuple[name: Option[string], ty: ref QualifiedType]]()
            if type_args.len > 0:
              for arg in type_args:
                case arg.node.kind
                of hrkDecl:
                  args.add((some(arg.node.decl.name.get.node.expr_named.name.short.path[^1]), arg.meta.own_type.get))
                else:
                  args.add((none[string](), arg.meta.own_type.get))
            instance = type_analysis.type_ctx.find_impl(name, none[ref TypeSum](), none[seq[Argument]](), false)

            if instance.impls.len == 0:
              discard
            elif instance.impls.len == 1:
              let impl = instance.impls.toSeq[0]
              case impl.kind
              of ikCons:
                let type_args = args.mapIt((it[0].get, it[1]))
                var ty = impl.ty[].as_qualified_in_span(self.meta.span)
                if type_args.len > 0:
                  self.meta.prepare_args = some(type_args)
                var own_type: ref QualifiedType
                new own_type
                own_type[] = ty[]
                self.meta.own_type = some(own_type)
                self.node.expr_named.name.full = some(impl.name)
              else:
                discard
            else:
              discard

        return vrkRecurse),
      visit_decl: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.decl.value.meta.own_type.isSome:
          self.meta.own_type = self.node.decl.value.meta.own_type
        return vrkRecurse),
      visit_pat_bind: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.pat_bind.pat.isSome and self.node.pat_bind.pat.get.meta.own_type.isSome:
          self.meta.own_type = some(self.node.pat_bind.pat.get.meta.own_type.get)
        return vrkRecurse),
    )
  discard accept(root, pre_visitor, post_visitor)

proc type_gen*(index: ref Index, driver: ref Driver, node: ref Node, root: ref Node, scope: bool = true) =
  let type_analysis = driver.pass_controllers[tsTypeDecl].TaPassController
  if scope and root.meta.impl_scope.isSome:
    type_analysis.type_ctx.enter_this_bounded(root.meta.impl_scope.get)
  elif scope:
    type_analysis.type_ctx.enter_bounded
  type_gen(index, driver, node, scope)
  if scope:
    root.meta.impl_scope = some(type_analysis.type_ctx.leave_this_bounded)

proc type_args*(index: ref Index, driver: ref Driver, root: ref Node, scope: bool = true) =
  proc generic_visitor(self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
    if self.meta.own_type.isSome and self.meta.prepare_args.isSome:
      let type_args = self.meta.prepare_args.get
      var ty = self.meta.own_type.get
      if type_args.len > 0:
        ty = ty.deepCopy
        ty.replace_params(type_args)
        ty.args = type_args.toOrderedTable
      self.meta.own_type.get[] = ty[]
      self.meta.prepare_args = none[seq[(string, ref QualifiedType)]]()
    return vrkRecurse

  let type_analysis = driver.pass_controllers[tsTypeDecl].TaPassController
  let
    pre_visitor = HlirVisitor()
    post_visitor = HlirVisitor(
      visit_type_type: some(generic_visitor),
      visit_type_pointer: some(generic_visitor),
      visit_type_array: some(generic_visitor),
      visit_type_slice: some(generic_visitor),
      visit_type_var: some(generic_visitor),
      visit_type_function: some(generic_visitor),
      visit_type_never: some(generic_visitor),
      visit_type_unit: some(generic_visitor),
      visit_type_bool: some(generic_visitor),
      visit_type_signed: some(generic_visitor),
      visit_type_unsigned: some(generic_visitor),
      visit_type_floating: some(generic_visitor),
      visit_expr_array: some(generic_visitor),
      visit_expr_application: some(generic_visitor),
      visit_expr_named: some(generic_visitor),
      visit_decl: some(generic_visitor),
      visit_pat_bind: some(generic_visitor),
    )
  discard accept(root, pre_visitor, post_visitor)

proc type_args*(index: ref Index, driver: ref Driver, node: ref Node, root: ref Node, scope: bool = true) =
  let type_analysis = driver.pass_controllers[tsTypeDecl].TaPassController
  if scope and root.meta.impl_scope.isSome:
    type_analysis.type_ctx.enter_this_bounded(root.meta.impl_scope.get)
  elif scope:
    type_analysis.type_ctx.enter_bounded
  type_args(index, driver, node, scope)
  if scope:
    root.meta.impl_scope = some(type_analysis.type_ctx.leave_this_bounded)

proc type_helper*(driver: ref Driver, root: ref Node, scope: bool = true) =
  let type_analysis = driver.pass_controllers[tsTypeDecl].TaPassController
  let
    pre_visitor = HlirVisitor()
    post_visitor = HlirVisitor(
      visit_type_array: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.node.type_array.size.meta.type_sum[].add(type_analysis.type_ctx.get("uint".toPath))
        return vrkRecurse),
    )
  discard accept(root, pre_visitor, post_visitor)

proc type_helper*(driver: ref Driver, node: ref Node, root: ref Node, scope: bool = true) =
  let type_analysis = driver.pass_controllers[tsTypeDecl].TaPassController
  if scope and root.meta.impl_scope.isSome:
    type_analysis.type_ctx.enter_this_bounded(root.meta.impl_scope.get)
  elif scope:
    type_analysis.type_ctx.enter_bounded
  type_helper(driver, node, scope)
  if scope:
    root.meta.impl_scope = some(type_analysis.type_ctx.leave_this_bounded)

proc decl*(index: ref Index, driver: ref Driver, root: ref Node, scope: bool = true) =
  let type_analysis = driver.pass_controllers[tsTypeDecl].TaPassController
  let macro_ctx = driver.macro_ctx
  let lazy = driver.lazy
  let
    pre_visitor = HlirVisitor(
      visit_function: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        var type_params = initTable[string, ref QualifiedType]()
        if self.node.function.type_params.isSome:
          for param in self.node.function.type_params.get:
            case param.node.kind
            of hrkDecl:
              case param.node.decl.value.node.kind
              of hrkTypeType:
                type_params.add(
                  param.node.decl.name.get.node.expr_named.name.short.path[^1],
                  newType(
                    Type(
                      kind: tykParameter,
                      param: TypeParameter(
                        name: param.node.decl.name.get.node.expr_named.name.short.path[^1],
                        kind: pkType,
                      )
                    )
                  ).toQualifiedType,
                )
              of hrkTypeSigned, hrkTypeUnsigned:
                type_params.add(
                  param.node.decl.name.get.node.expr_named.name.short.path[^1],
                  newType(
                    Type(
                      kind: tykParameter,
                      param: TypeParameter(
                        name: param.node.decl.name.get.node.expr_named.name.short.path[^1],
                        kind: pkInt,
                        ty: param.node.decl.value.to_type_sum(type_analysis.type_ctx),
                      ),
                    )
                  ).toQualifiedType,
                )
              else:
                raise newError(errkMalformedHlir, param.node.decl.value.meta.span, "expected one of: `type`, <signed type>, <unsigned type>")
            else:
              raise newError(errkMalformedHlir, param.meta.span, "expected a declaration")

        let function_type = self.node.function.ty.to_type(type_analysis.type_ctx, type_params)
        self.meta.type_sum[] = function_type.toTypeSum

        if self.node.function.name_node.isSome:
          self.node.function.name.get.qualify(type_analysis.module_name)
          let type_function = self.node.function.ty.node.type_function
          if self.node.function.type_params.isNone:
            if self.meta.attributes.isSome and self.meta.attributes.get.any((attr: Attribute) => attr.kind == akNoMangle):
              self.node.function.name.get.mangled = some[string](self.node.function.name.get.short.path[^1])
            else:
              var params = newSeqOfCap[ref QualifiedType](type_function.params.len)
              for i, param in function_type.params.pairs:
                params.add(param.ty[].as_qualified_in_span(self.node.function.ty.node.type_function.params[i].meta.span))
              self.node.function.name.get.mangle_with_params(params)

          # add test name + test linkage name to test list
          if self.meta.attributes.isSome and self.meta.attributes.get.any((attr: Attribute) => attr.kind == akTest):
            type_analysis.tests.add(self.node.function.name.get)

          if self.meta.attributes.isSome and self.meta.attributes.get.any((attr: Attribute) => attr.kind == akTestMain):
            type_analysis.test_main_added = true

          var linkage_name: Option[string]
          if self.node.function.type_params.isNone:
            linkage_name = some(self.node.function.name.get.mangled.get)

          if not self.node.function.monomorphized:
            let impl = newFnImpl(self.node.function.name.get.full.get, linkage_name, false, false, true, function_type.result_type, function_type.params, false, false, false)
            self.meta.impl = some(impl)
            if self.node.function.is_closure:
              type_analysis.type_ctx.add_impl(self.node.function.name.get.short, impl, 0)
            else:
              type_analysis.type_ctx.add_impl(self.node.function.name.get.short, impl)
            type_analysis.type_ctx.add_impl(self.node.function.name.get.full.get, impl, 0)
        else:
          return vrkRecurse

        if self.node.function.type_params.isSome:
          type_analysis.add_polymorphic(self.node.function.name.get.full.get, function_type, self)
          return vrkContinue

        type_analysis.type_ctx.enter_this_bounded(self.meta.impl_scope.get)
        type_analysis.enter
        type_analysis.enter_function(self.node.function.name.get.full.get, function_type.result_type, true)

        if self.node.function.body.isSome and self.node.function.name_node.isSome:
          var params = newSeq[tuple[name: string, ty: ref QualifiedType]]()
          for i, param in self.node.function.ty.node.type_function.params:
            let ty = self.meta.type_sum[].as_qualified_in_span(self.meta.span).inner.function.params[i].ty[].as_qualified_in_span(param.meta.span)
            params.add((param.node.decl.name.get.node.expr_named.name.short.path[^1], ty))
          let
            name = self.node.function.name.get.mangled.get
            fn = MacroFunction(name: self.node.function.name.get.full.get, params: params, body: self.node.function.body.get)
            val = MacroValue(kind: mvkFunction, fn: fn)
        
          macro_ctx.bind_var(name, MacroVariable(kind: mvakRval, val: val))

        return vrkRecurse),
      visit_macro: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        var type_params = initTable[string, ref QualifiedType]()
        if self.node.macrofn.type_params.isSome:
          for param in self.node.macrofn.type_params.get:
            case param.node.kind
            of hrkDecl:
              case param.node.decl.value.node.kind
              of hrkTypeType:
                type_params.add(
                  param.node.decl.name.get.node.expr_named.name.short.path[^1],
                  newType(
                    Type(
                      kind: tykParameter,
                      param: TypeParameter(
                        name: param.node.decl.name.get.node.expr_named.name.short.path[^1],
                        kind: pkType,
                      )
                    )
                  ).toQualifiedType,
                )
              of hrkTypeSigned, hrkTypeUnsigned:
                type_params.add(
                  param.node.decl.name.get.node.expr_named.name.short.path[^1],
                  newType(
                    Type(
                      kind: tykParameter,
                      param: TypeParameter(
                        name: param.node.decl.name.get.node.expr_named.name.short.path[^1],
                        kind: pkInt,
                        ty: param.node.decl.value.to_type_sum(type_analysis.type_ctx),
                      ),
                    )
                  ).toQualifiedType,
                )
              else:
                raise newError(errkMalformedHlir, param.node.decl.value.meta.span, "expected one of: `type`, <signed type>, <unsigned type>")
            else:
              raise newError(errkMalformedHlir, param.meta.span, "expected a declaration")

        let function_type = self.node.macrofn.ty.to_type(type_analysis.type_ctx, type_params)

        if self.node.macrofn.name_node.isSome:
          self.node.macrofn.name.get.qualify(type_analysis.module_name)
          let type_function = self.node.macrofn.ty.node.type_function
          if self.node.macrofn.type_params.isNone:
            if self.meta.attributes.isSome and self.meta.attributes.get.any((attr: Attribute) => attr.kind == akNoMangle):
              self.node.macrofn.name.get.mangled = some[string](self.node.macrofn.name.get.short.path[^1])
            else:
              var params = newSeqOfCap[ref QualifiedType](type_function.params.len)
              for i, param in function_type.params.pairs:
                params.add(param.ty[].as_qualified_in_span(self.node.macrofn.ty.node.type_function.params[i].meta.span))
              self.node.macrofn.name.get.mangle_with_params(params)

          let linkage_name = self.node.macrofn.name.get.mangled

          if not self.node.macrofn.monomorphized:
            let impl = newFnImpl(self.node.macrofn.name.get.full.get, linkage_name, false, false, true, function_type.result_type, function_type.params, false, false, true)
            self.meta.impl = some(impl)
            type_analysis.type_ctx.add_impl(self.node.macrofn.name.get.short, impl)
            type_analysis.type_ctx.add_impl(self.node.macrofn.name.get.full.get, impl, 0)

        if self.node.macrofn.type_params.isSome:
          type_analysis.add_polymorphic(self.node.macrofn.name.get.full.get, function_type, self)
          return vrkContinue

        self.meta.type_sum[] = function_type.toTypeSum

        type_analysis.type_ctx.enter_this_bounded(self.meta.impl_scope.get)
        type_analysis.enter
        type_analysis.enter_function(self.node.macrofn.name.get.full.get, function_type.result_type, true)

        if self.node.macrofn.body.isSome and self.node.macrofn.name_node.isSome:
          var params = newSeq[tuple[name: string, ty: ref QualifiedType]]()
          for i, param in self.node.macrofn.ty.node.type_function.params:
            let ty = self.meta.type_sum[].as_qualified_in_span(self.meta.span).inner.function.params[i].ty[].as_qualified_in_span(param.meta.span)
            params.add((param.node.decl.name.get.node.expr_named.name.short.path[^1], ty))
          let
            name = self.node.macrofn.name.get.mangled.get
            fn = MacroFunction(name: self.node.macrofn.name.get.full.get, params: params, body: self.node.macrofn.body.get)
            val = MacroValue(kind: mvkFunction, fn: fn)
        
          macro_ctx.bind_var(name, MacroVariable(kind: mvakRval, val: val))

        return vrkRecurse),
      visit_trait: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.trait.type_params.isSome:
          type_analysis.add_polymorphic(self.node.trait.name.get.full.get, self.meta.own_type.get, self)
          return vrkContinue
        return vrkRecurse),
      visit_struct: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.struct.type_params.isSome:
          type_analysis.add_polymorphic(self.node.struct.name.get.full.get, self.meta.own_type.get, self)
          return vrkContinue
        return vrkRecurse),
      visit_union: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.struct.type_params.isSome:
          type_analysis.add_polymorphic(self.node.struct.name.get.full.get, self.meta.own_type.get, self)
          return vrkContinue
        return vrkRecurse),
      visit_tagged: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.tagged.type_params.isSome:
          type_analysis.add_polymorphic(self.node.tagged.name.get.full.get, self.meta.own_type.get, self)
          return vrkContinue
        return vrkRecurse),
      # pre-find an impl, if it's a __lang::macros::quasiquote or quote, return vrkContinue
      visit_expr_application: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let fn = self.node.expr_application.fn
        case fn.node.kind
        of hrkExprNamed:
          var args = newSeq[Argument]()
          for arg in self.node.expr_application.args:
            case arg.node.kind
            of hrkDecl:
              args.add(Argument(name: arg.node.decl.name.map((name) => name.node.expr_named.name.short.path[^1]), ty: arg.meta.type_sum))
            else:
              args.add(Argument(name: none[string](), ty: arg.meta.type_sum))

          let name = fn.node.expr_named.name.short

          var instance: Instance
          if fn.node.expr_named.type_args.isSome:
            var type_args = newSeq[tuple[name: Option[string], ty: ref QualifiedType]]()
            for arg in fn.node.expr_named.type_args.get:
              case arg.node.kind
              of hrkDecl:
                type_args.add((some(arg.node.decl.name.get.node.expr_named.name.short.path[^1]), arg.meta.own_type.get))
              else:
                type_args.add((none[string](), arg.meta.own_type.get))
            instance = type_analysis.type_ctx.find_impl_with_args(name, none[ref TypeSum](), some(args), type_args, false)
          else:
            instance = type_analysis.type_ctx.find_impl(name, none[ref TypeSum](), some(args), false)
          if instance.impls.len > 0:
            let impl = instance.impls.toSeq[0]

            if impl.kind == ikFn and impl.is_macro:
              if impl.name == ["__lang", "macros", "quote"].toPath or impl.name == ["__lang", "macros", "quasiquote"].toPath:
                return vrkNoRecurse
        else:
          discard
        return vrkRecurse),
      visit_expr_command: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let fn = self.node.expr_command.fn
        case fn.node.kind
        of hrkExprNamed:
          var args = newSeq[Argument]()
          for arg in self.node.expr_command.args:
            case arg.node.kind
            of hrkDecl:
              args.add(Argument(name: arg.node.decl.name.map((name) => name.node.expr_named.name.short.path[^1]), ty: arg.meta.type_sum))
            else:
              args.add(Argument(name: none[string](), ty: arg.meta.type_sum))

          let name = fn.node.expr_named.name.short

          var instance: Instance
          if fn.node.expr_named.type_args.isSome:
            var type_args = newSeq[tuple[name: Option[string], ty: ref QualifiedType]]()
            for arg in fn.node.expr_named.type_args.get:
              case arg.node.kind
              of hrkDecl:
                type_args.add((some(arg.node.decl.name.get.node.expr_named.name.short.path[^1]), arg.meta.own_type.get))
              else:
                type_args.add((none[string](), arg.meta.own_type.get))
            instance = type_analysis.type_ctx.find_impl_with_args_untyped(name, none[ref TypeSum](), some(args), type_args, false)
          else:
            instance = type_analysis.type_ctx.find_impl_untyped(name, none[ref TypeSum](), some(args), false)
          if instance.impls.len > 0:
            let impl = instance.impls.toSeq[0]

            if impl.kind == ikFn and impl.is_macro:
              if impl.name == ["__lang", "macros", "quote"].toPath or impl.name == ["__lang", "macros", "quasiquote"].toPath:
                return vrkNoRecurse
        else: raise newIce("unimplemented")
        return vrkRecurse),
    )
    post_visitor = HlirVisitor(
      visit_mod: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        type_analysis.module = self.node.module.path
        type_analysis.module.full = true
      ),
      visit_function: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.function.name_node.isSome:
          self.meta.impl_scope = some(type_analysis.type_ctx.leave_this_bounded)
          type_analysis.leave_fn()
          type_analysis.leave

        return vrkRecurse),
      visit_macro: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.impl_scope = some(type_analysis.type_ctx.leave_this_bounded)
        type_analysis.leave_fn()
        type_analysis.leave

        return vrkRecurse),
      visit_static: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.static_global.name.isNone:
          self.node.static_global.name = some(Names(short: self.node.static_global.name_node.node.expr_named.name.short))

        if self.node.static_global.ty.meta.own_type.isNone:
          raise newError(errkInvalidType, self.node.static_global.ty.meta.span, "static extern type is not a type")
        self.meta.type_sum[].add(self.node.static_global.ty.to_type(type_analysis.type_ctx))
        if self.node.static_global.value.isSome:
          self.node.static_global.value.get.meta.type_sum[] = type_analysis.type_ctx.restrict_sum_in_span(self.node.static_global.value.get.meta.type_sum, self.meta.type_sum, self.meta.span)
        mark_type(self.node.static_global.ty)

        self.node.static_global.name.get.qualify(type_analysis.module_name)
        self.node.static_global.name.get.mangle()

        let impl = newVarImpl(self.node.static_global.name.get.full.get, none[string](), false, true, true, self.meta.type_sum)
        self.meta.impl = some(impl)
        type_analysis.type_ctx.add_impl(self.node.static_global.name.get.short, impl)
        type_analysis.type_ctx.add_impl(self.node.static_global.name.get.full.get, impl, 0)

        return vrkRecurse),
      visit_static_extern: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.static_extern.name.isNone:
          self.node.static_extern.name = some(Names(short: self.node.static_extern.name_node.node.expr_named.name.short))

        if self.node.static_extern.ty.meta.own_type.isNone:
          raise newError(errkInvalidType, self.node.static_extern.ty.meta.span, "static extern type is not a type")
        self.meta.type_sum[].add(self.node.static_extern.ty.to_type(type_analysis.type_ctx))
        mark_type(self.node.static_extern.ty)

        self.node.static_extern.name.get.qualify(type_analysis.module_name)
        self.node.static_extern.name.get.mangle()

        let impl = newVarImpl(self.node.static_extern.name.get.full.get, none[string](), false, true, true, self.meta.type_sum)
        self.meta.impl = some(impl)
        type_analysis.type_ctx.add_impl(self.node.static_extern.name.get.short, impl)
        type_analysis.type_ctx.add_impl(self.node.static_extern.name.get.full.get, impl, 0)

        return vrkRecurse),
      visit_define: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.define.name.isNone:
          self.node.define.name = some(Names(short: self.node.define.name_node.node.expr_named.name.short))

        if self.node.define.ty.get.meta.own_type.isNone:
          raise newError(errkInvalidType, self.node.define.ty.get.meta.span, "define.ty.getpe is not a type")
        self.meta.type_sum[].add(self.node.define.ty.get.to_type(type_analysis.type_ctx))
        self.node.define.value.meta.type_sum[] = type_analysis.type_ctx.restrict_sum_in_span(self.node.define.value.meta.type_sum, self.meta.type_sum, self.meta.span)
        mark_type(self.node.define.ty.get)

        self.node.define.name.get.qualify(type_analysis.module_name)
        self.node.define.name.get.mangle()

        let impl = newVarImpl(self.node.define.name.get.full.get, none[string](), false, true, true, self.meta.type_sum)
        self.meta.impl = some(impl)
        type_analysis.type_ctx.add_impl(self.node.define.name.get.short, impl)
        type_analysis.type_ctx.add_impl(self.node.define.name.get.full.get, impl, 0)

        return vrkRecurse),
    )
  discard accept(root, pre_visitor, post_visitor)

proc decl*(index: ref Index, driver: ref Driver, node: ref Node, root: ref Node, scope: bool = true) =
  let type_analysis = driver.pass_controllers[tsTypeDecl].TaPassController
  if scope and root.meta.impl_scope.isSome:
    type_analysis.type_ctx.enter_this_bounded(root.meta.impl_scope.get)
  elif scope:
    type_analysis.type_ctx.enter_bounded
  decl(index, driver, node, scope)
  if scope:
    root.meta.impl_scope = some(type_analysis.type_ctx.leave_this_bounded)

proc analyze*(index: ref Index, driver: ref Driver, root: ref Node, scope: bool = true) =
  let type_analysis = driver.pass_controllers[tsTypeDecl].TaPassController
  let macro_ctx = driver.macro_ctx
  let lazy = driver.lazy
  let
    pre_visitor = HlirVisitor(
      visit_use: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let
          module = self.node.use.path
          unit = index.load_with_span(module, "amp", self.meta.span)
          self_module = type_analysis.module
        driver.run_until(index, unit, tsTypeAnalysis)
        type_analysis.module = self_module
      ),
      visit_function: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.function.type_params.isSome:
          return vrkContinue

        if self.node.function.name_node.isNone:
          if self.node.function.body.isSome:
            inc self.node.function.body.get.meta.dont_visit
          inc self.node.function.ty.meta.dont_visit
          return vrkRecurse

        type_analysis.type_ctx.enter_this_bounded(self.meta.impl_scope.get)
        type_analysis.enter
        type_analysis.enter_function(self.node.function.name.get.full.get, self.meta.type_sum[].as_qualified.result_type, true)

        let result_var = newVarImpl(self.node.function.name.get.full.get & "result", some("result"), false, true, false, self.meta.type_sum[].as_qualified.result_type)
        type_analysis.type_ctx.add_impl("result".toNonFullPath, result_var)

        for param in self.node.function.ty.node.type_function.params:
          if param.node.decl.name.isSome:
            param.node.decl.name.get.node.expr_named.name.qualify(type_analysis.module_name)
            let impl = newVarImpl(param.node.decl.name.get.node.expr_named.name.full.get, none[string](), false, true, false, param.meta.type_sum)
            param.meta.impl = some(impl)
            type_analysis.type_ctx.add_impl(param.node.decl.name.get.node.expr_named.name.short, impl)
            type_analysis.type_ctx.add_impl(param.node.decl.name.get.node.expr_named.name.full.get, impl, 0)

        return vrkRecurse),
      visit_macro: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.macrofn.type_params.isSome:
          return vrkContinue

        type_analysis.type_ctx.enter_this_bounded(self.meta.impl_scope.get)
        type_analysis.enter
        type_analysis.enter_function(self.node.macrofn.name.get.full.get, self.meta.type_sum[].as_qualified.result_type, true)

        for param in self.node.macrofn.ty.node.type_function.params:
          if param.node.decl.name.isSome:
            param.node.decl.name.get.node.expr_named.name.qualify(type_analysis.module_name)
            let impl = newVarImpl(param.node.decl.name.get.node.expr_named.name.full.get, none[string](), false, true, false, param.meta.type_sum)
            param.meta.impl = some(impl)
            type_analysis.type_ctx.add_impl(param.node.decl.name.get.node.expr_named.name.short, impl)
            type_analysis.type_ctx.add_impl(param.node.decl.name.get.node.expr_named.name.full.get, impl, 0)

        return vrkRecurse),
      visit_trait: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        type_analysis.type_ctx.add_impl(self.node.trait.name.get.short, self.meta.impl.get)
        if self.node.trait.type_params.isSome:
          type_analysis.add_polymorphic(self.node.trait.name.get.full.get, self.meta.own_type.get, self)
          return vrkContinue
        return vrkRecurse),
      visit_struct: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        type_analysis.type_ctx.add_impl(self.node.struct.name.get.short, self.meta.impl.get)
        if self.node.struct.type_params.isSome:
          type_analysis.add_polymorphic(self.node.struct.name.get.full.get, self.meta.own_type.get, self)
          return vrkContinue
        return vrkRecurse),
      visit_union: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        type_analysis.type_ctx.add_impl(self.node.struct.name.get.short, self.meta.impl.get)
        if self.node.struct.type_params.isSome:
          type_analysis.add_polymorphic(self.node.struct.name.get.full.get, self.meta.own_type.get, self)
          return vrkContinue
        return vrkRecurse),
      visit_tagged: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        for variant in self.node.tagged.variants:
          type_analysis.type_ctx.add_impl(variant.node.tagged_variant.name.get.short, variant.meta.impl.get)
        if self.node.tagged.type_params.isSome:
          type_analysis.add_polymorphic(self.node.tagged.name.get.full.get, self.meta.own_type.get, self)
          return vrkContinue
        return vrkRecurse),
      visit_typedef: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        type_analysis.type_ctx.add_impl(self.node.typedef.name.get.short, self.meta.impl.get)
        return vrkRecurse),
      visit_stmt_match: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let ty = self.node.stmt_match.value.meta.type_sum
        type_analysis.add_match(ty)

        for case_of in self.node.stmt_match.cases.mitems:
          case_of.meta.switch_type = ty
        return vrkRecurse),
      visit_expr_compound: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        type_analysis.type_ctx.enter
        return vrkRecurse),
      visit_case: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let ty = self.meta.switch_type
        self.node.case_of.pat.meta.switch_type = ty
        return vrkRecurse),
      visit_pat_bind: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let ty = self.meta.switch_type
        if self.node.pat_bind.pat.isSome:
          self.node.pat_bind.pat.get.meta.switch_type = ty
        return vrkRecurse),
      visit_pat_destructure: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let ty = self.meta.switch_type
        for pat in self.node.pat_destructure.pats.mitems:
          pat.meta.switch_type = ty

        let fn = self.node.pat_destructure.ty
        case fn.node.kind
        of hrkExprNamed:
          var type_args = newSeq[ref Node]()
          while self.node.pat_destructure.pats.len > 0:
            if self.node.pat_destructure.pats[0].meta.own_type.isSome:
              type_args.add(self.node.pat_destructure.pats[0])
              self.node.pat_destructure.pats.delete(0)
            else:
              break

          if type_args.len > 0:
            self.node.pat_destructure.ty.node.expr_named.type_args = some(type_args)

          if self.node.pat_destructure.ty.node.expr_named.type_args.isSome:
            type_args = self.node.pat_destructure.ty.node.expr_named.type_args.get

          let name = fn.node.expr_named.name.short

          var instance: Instance
          if fn.node.expr_named.type_args.isSome:
            var args = newSeq[tuple[name: Option[string], ty: ref QualifiedType]]()
            for arg in type_args:
              case arg.node.kind
              of hrkDecl:
                args.add((some(arg.node.decl.name.get.node.expr_named.name.short.path[^1]), arg.meta.own_type.get))
              else:
                args.add((none[string](), arg.meta.own_type.get))
            instance = type_analysis.type_ctx.find_impl_with_args(name, none[ref TypeSum](), none[seq[Argument]](), args, false)
          else:
            instance = type_analysis.type_ctx.find_impl(name, none[ref TypeSum](), none[seq[Argument]](), false)
          case instance.impls.len
          of 0:
            var msg = fmt"{name}{'\n'}the following impls were found:{'\n'}"
            for impl in instance.found_impls:
              msg.add("  ")
              msg.add($impl[])
              msg.add("\n")
            raise newError(errkNoImpl, self.meta.span, msg)
          of 1:
            let impl = instance.impls.toSeq[0]
            self.meta.type_sum = self.meta.switch_type[].deref
            self.meta.impl = some(impl)
            case impl.kind
            of ikVar, ikFn:
              raise newError(errkInvalidType, self.meta.span, "destructure patterns have to be types")
            of ikCons:
              let type_args = self.meta.type_sum[].as_qualified_in_span(self.meta.span).type_args_named
              var ty = self.meta.type_sum[].as_qualified_in_span(self.meta.span)
              var sty = type_analysis.type_ctx.get(impl.name)
              if type_args.len > 0:
                sty = sty.deepCopy
                sty.replace_params(type_args)
              self.meta.own_type = some(ty)
              fn.meta.own_type = some(sty)
              fn.node.expr_named.name.full = some(impl.name)
              fn.node.expr_named.name.mangled = some(sty.mangled)
          else:
            var msg = fmt"{'\n'}{name}():{'\n'}"
            for impl in instance.impls:
              msg.add("  ")
              msg.add($impl[])
              msg.add("\n")
            raise newError(errkAmbigousImpl, self.meta.span, msg)
        else: raise newIce("unimplemented")

        return vrkRecurse),
      # pre-find an impl, if it's a __lang::macros::quasiquote or quote, return vrkContinue
      visit_expr_application: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let fn = self.node.expr_application.fn
        case fn.node.kind
        of hrkExprNamed:
          var args = newSeq[Argument]()
          for arg in self.node.expr_application.args:
            case arg.node.kind
            of hrkDecl:
              args.add(Argument(name: arg.node.decl.name.map((name) => name.node.expr_named.name.short.path[^1]), ty: arg.meta.type_sum))
            else:
              args.add(Argument(name: none[string](), ty: arg.meta.type_sum))

          let name = fn.node.expr_named.name.short

          var instance: Instance
          if fn.node.expr_named.type_args.isSome:
            var type_args = newSeq[tuple[name: Option[string], ty: ref QualifiedType]]()
            for arg in fn.node.expr_named.type_args.get:
              case arg.node.kind
              of hrkDecl:
                type_args.add((some(arg.node.decl.name.get.node.expr_named.name.short.path[^1]), arg.meta.own_type.get))
              else:
                type_args.add((none[string](), arg.meta.own_type.get))
            instance = type_analysis.type_ctx.find_impl_with_args(name, none[ref TypeSum](), some(args), type_args, false)
          else:
            instance = type_analysis.type_ctx.find_impl(name, none[ref TypeSum](), some(args), false)
          if instance.impls.len > 0:
            let impl = instance.impls.toSeq[0]

            if impl.kind == ikFn and impl.is_macro:
              if impl.name == ["__lang", "macros", "quote"].toPath or impl.name == ["__lang", "macros", "quasiquote"].toPath:
                if impl.name == ["__lang", "macros", "quasiquote"].toPath:
                  quasiquote(index, driver, self)
                return vrkNoRecurse

              if not impl.builtin:
                for i, param in impl.params:
                  if param.ty[].is_untyped_node:
                    inc self.node.expr_application.args[i].meta.dont_visit
        else:
          discard
        return vrkRecurse),
      visit_expr_command: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let fn = self.node.expr_command.fn
        case fn.node.kind
        of hrkExprNamed:
          var args = newSeq[Argument]()
          for arg in self.node.expr_command.args:
            case arg.node.kind
            of hrkDecl:
              args.add(Argument(name: arg.node.decl.name.map((name) => name.node.expr_named.name.short.path[^1]), ty: arg.meta.type_sum))
            else:
              args.add(Argument(name: none[string](), ty: arg.meta.type_sum))

          let name = fn.node.expr_named.name.short

          var instance: Instance
          if fn.node.expr_named.type_args.isSome:
            var type_args = newSeq[tuple[name: Option[string], ty: ref QualifiedType]]()
            for arg in fn.node.expr_named.type_args.get:
              case arg.node.kind
              of hrkDecl:
                type_args.add((some(arg.node.decl.name.get.node.expr_named.name.short.path[^1]), arg.meta.own_type.get))
              else:
                type_args.add((none[string](), arg.meta.own_type.get))
            instance = type_analysis.type_ctx.find_impl_with_args_untyped(name, none[ref TypeSum](), some(args), type_args, false)
          else:
            instance = type_analysis.type_ctx.find_impl_untyped(name, none[ref TypeSum](), some(args), false)
          if instance.impls.len > 0:
            let impl = instance.impls.toSeq[0]

            if impl.kind == ikFn and impl.is_macro:
              if impl.name == ["__lang", "macros", "quote"].toPath or impl.name == ["__lang", "macros", "quasiquote"].toPath:
                if impl.name == ["__lang", "macros", "quasiquote"].toPath:
                  quasiquote(index, driver, self)
                return vrkNoRecurse

              if not impl.builtin:
                for i, param in impl.params:
                  if param.ty[].is_untyped_node:
                    inc self.node.expr_command.args[i].meta.dont_visit
        else: raise newIce("unimplemented")
        return vrkRecurse),
    )
    post_visitor = HlirVisitor(
      visit_mod: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        type_analysis.module = self.node.module.path
        type_analysis.module.full = true
      ),
      visit_function: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.function.name_node.isNone:
          let function_type = self.node.function.ty.meta.own_type.get.inner.function
          var params = newSeqOfCap[ref QualifiedType](function_type.params.len)
          for i, param in function_type.params.pairs:
            params.add(param.ty[].as_qualified_in_span(self.node.function.ty.node.type_function.params[i].meta.span))

          let
            line = self.meta.span.line
            col = self.meta.span.col

          let name = "__call__"
          var path = (self.meta.span.directory/self.meta.span.file).changeFileExt("").split('/')
          path.add(fmt"at{line}_{col}")
          path.add(name)
          if path[0].len == 0:
            path.delete(0)
          self.node.function.name = some(Names(short: name.toNonFullPath, full: some(Path(full: true, path: path))))
          self.node.function.name.get.qualify(type_analysis.module_name)
          if self.node.function.type_params.isNone:
            self.node.function.name.get.mangle_with_params(params)
          self.node.function.name_node = some(newNode(newMeta(self.meta.span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: self.node.function.name.get, quiet: true))))

          # should be true for any anonymous function
          if self.node.function.is_closure:
            var
              capture_fields = newSeq[ref Node]()
              capture_vars = newSeq[ref Node]()
              capture_names: ref seq[tuple[name: string, by_ref: bool]]
              path = (self.meta.span.directory/self.meta.span.file).changeFileExt("").split('/')
            new capture_names
            capture_names[] = newSeq[tuple[name: string, by_ref: bool]]()
            if self.node.function.capture.isSome:
              for capture in self.node.function.capture.get:
                var
                  name: string
                  name_node: ref Node
                  by_ref = false
                case capture.node.kind
                of hrkExprNamed:
                  name = capture.node.expr_named.name.short.path[^1]
                  name_node = capture
                of hrkExprUnary:
                  case capture.node.expr_unary.op
                  of hukRef:
                    let capture = capture.node.expr_unary.value
                    case capture.node.kind
                    of hrkExprNamed:
                      name = capture.node.expr_named.name.short.path[^1]
                      name_node = capture
                      by_ref = true
                    else:
                      raise newError(errkInvalidCapture, capture.meta.span)
                  else:
                    raise newError(errkInvalidCapture, capture.meta.span)
                else:
                  raise newError(errkInvalidCapture, capture.meta.span)

                let capture_field_type = capture.meta.type_sum[].as_qualified_in_span(capture.meta.span)
                let capture_field_decl = (name, capture_field_type[]).to_hlir(self.meta.span)
                capture_fields.add(capture_field_decl)

                let capture_var_expr = capture
                let capture_var_decl = newNode(newMeta(capture.meta.span), Hlir(kind: hrkDecl, decl: HlirDecl(name: some(name_node.deepCopy), value: capture_var_expr)))
                capture_vars.add(capture_var_decl)

                capture_names[].add((name, by_ref))
            path.add(fmt"at{line}_{col}")
            path.add("Closure")
            let full = Path(path: path)
            let mangled = full.mangled_as_type
            let struct_name_node = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: Names(short: "Closure".toNonFullPath, full: some(full), mangled: some(mangled)), quiet: true)))
            let struct = newNode(newMeta(self.meta.span), Hlir(kind: hrkStruct, struct: HlirStruct(name_node: struct_name_node, name: some(Names(short: "Closure".toNonFullPath, full: some(full), mangled: some(mangled))), fields: capture_fields)))
            let fn = self.deepCopy
            # don't visit me (&y) or my son (y) ever again 
            if fn.node.function.capture.isSome:
              for capture in fn.node.function.capture.get:
                capture.meta.dont_visit = 2147483647 # int32 max, because int might be 32 bits wide
            let struct_name = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: Names(short: "Closure".toNonFullPath, full: some(full), mangled: some(mangled)))))
            let cons = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprApplication, expr_application: HlirExprApplication(fn: struct_name, args: capture_vars)))
            self[] = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprCompound, expr_compound: HlirExprCompound(elems: @[struct, fn], last: some(cons))))[]
            # this is hacky, it should be ran elsewhere, but pi couldn't find a way to do that
            type_reg(index, driver, struct, false)
            type_gen(index, driver, struct, false)
            type_args(index, driver, struct, false)
            type_helper(driver, struct, false)
            decl(index, driver, struct, false)
            analyze(index, driver, struct, false)
            # add the implicit `recur` field to the parameter list of the function
            let closure_type = struct.meta.own_type.get.toTypeSumRef
            let closure_ptr = newType(Type(kind: tykPointer, point: TypePointer(pointee: closure_type))).toQualifiedType
            let closure_decl = ("recur", closure_ptr[]).to_hlir(self.meta.span)
            fn.node.function.ty.node.type_function.params.insert(closure_decl)

            # replace y with *recur.y
            replace_capture(fn, capture_names)

            # still hacky
            type_reg(index, driver, fn)
            type_gen(index, driver, fn)
            type_args(index, driver, fn)
            type_helper(driver, fn)
            decl(index, driver, fn)
            analyze(index, driver, fn)
            # so hacky
            type_reg(index, driver, cons, false)
            type_gen(index, driver, cons, false)
            type_args(index, driver, cons, false)
            type_helper(driver, cons, false)
            decl(index, driver, cons, false)
            analyze(index, driver, cons, false)

            # don't forget to set compound expression type
            self.meta.type_sum = cons.meta.type_sum
            return vrkContinue

        self.meta.impl_scope = some(type_analysis.type_ctx.leave_this_bounded)
        type_analysis.leave_fn()
        type_analysis.leave

        return vrkRecurse),
      visit_macro: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.impl_scope = some(type_analysis.type_ctx.leave_this_bounded)
        type_analysis.leave_fn()
        type_analysis.leave

        if driver.install_mode:
          if self.node.macrofn.body.isSome and self.node.macrofn.name_node.isSome and self.node.macrofn.name.get.short == "package".toNonFullPath:
            macro_ctx.get_var(["__lang", "macros", "ctx"].toPath.mangled).get.val.struct.fields["call_site"] = self.meta.span.to_macro_value
            macro_ctx.get_var(["__lang", "macros", "ctx"].toPath.mangled).get.val.struct.fields["release"] = driver.release.to_macro_value(self.meta.span)
            discard macro_ctx.eval(index, self.node.macrofn.body.get)
        if driver.build_mode:
          if self.node.macrofn.body.isSome and self.node.macrofn.name_node.isSome and self.node.macrofn.name.get.short == "build".toNonFullPath:
            macro_ctx.get_var(["__lang", "macros", "ctx"].toPath.mangled).get.val.struct.fields["call_site"] = self.meta.span.to_macro_value
            macro_ctx.get_var(["__lang", "macros", "ctx"].toPath.mangled).get.val.struct.fields["release"] = driver.release.to_macro_value(self.meta.span)
            discard macro_ctx.eval(index, self.node.macrofn.body.get)
        else:
          if self.node.macrofn.body.isSome and self.node.macrofn.name_node.isSome and self.node.macrofn.name.get.short == "main".toNonFullPath:
            macro_ctx.get_var(["__lang", "macros", "ctx"].toPath.mangled).get.val.struct.fields["call_site"] = self.meta.span.to_macro_value
            macro_ctx.get_var(["__lang", "macros", "ctx"].toPath.mangled).get.val.struct.fields["release"] = driver.release.to_macro_value(self.meta.span)
            discard macro_ctx.eval(index, self.node.macrofn.body.get)

        return vrkRecurse),
      visit_decl: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.meta.own_type.isSome:
          self.meta.type_sum[].add(self.meta.own_type.get)
        else:
          self.meta.type_sum[].add(self.node.decl.value.meta.type_sum[])
        if self.node.decl.name.isSome:
          if self.node.decl.name.get.node.expr_named.name.full.isNone:
            self.node.decl.name.get.node.expr_named.name.qualify(type_analysis.module_name)
        return vrkRecurse),
      visit_stmt_if: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        var bool_type: Option[ref QualifiedType]
        for ty in self.node.stmt_if.cond.meta.type_sum.sum:
          if ty.is_bool:
            bool_type = some(ty)
            break
        if bool_type.isSome:
          self.node.stmt_if.cond.meta.type_sum.sum.clear()
          self.node.stmt_if.cond.meta.type_sum[].add(bool_type.get)
        return vrkRecurse),
      visit_stmt_while: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        var bool_type: Option[ref QualifiedType]
        for ty in self.node.stmt_while.cond.meta.type_sum.sum:
          if ty.is_bool:
            bool_type = some(ty)
            break
        if bool_type.isSome:
          self.node.stmt_while.cond.meta.type_sum.sum.clear()
          self.node.stmt_while.cond.meta.type_sum[].add(bool_type.get)
        return vrkRecurse),
      visit_stmt_dowhile: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        var bool_type: Option[ref QualifiedType]
        for ty in self.node.stmt_dowhile.cond.meta.type_sum.sum:
          if ty.is_bool:
            bool_type = some(ty)
            break
        if bool_type.isSome:
          self.node.stmt_dowhile.cond.meta.type_sum.sum.clear()
          self.node.stmt_dowhile.cond.meta.type_sum[].add(bool_type.get)
        return vrkRecurse),
      visit_stmt_for: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.stmt_for.cond.isSome:
          var bool_type: Option[ref QualifiedType]
          for ty in self.node.stmt_for.cond.get.meta.type_sum.sum:
            if ty.is_bool:
              bool_type = some(ty)
              break
          if bool_type.isSome:
            self.node.stmt_for.cond.get.meta.type_sum.sum.clear()
            self.node.stmt_for.cond.get.meta.type_sum[].add(bool_type.get)
        return vrkRecurse),
      visit_stmt_match: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        type_analysis.leave_match()
        return vrkRecurse),
      visit_expr_compound: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        type_analysis.type_ctx.leave
        if self.node.expr_compound.last.isSome:
          self.meta.type_sum = self.node.expr_compound.last.get.meta.type_sum
        else:
          self.meta.type_sum[].add(type_analysis.type_ctx.get("unit".toPath))
        return vrkRecurse),
      visit_expr_unit: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.type_sum[].add(type_analysis.type_ctx.get("unit".toPath))
        return vrkRecurse),
      visit_expr_bool: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.type_sum[].add(type_analysis.type_ctx.get("bool".toPath))
        return vrkRecurse),
      visit_expr_int: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.expr_int.as_type.isSome:
          case self.node.expr_int.as_type.get.node.kind
          of hrkExprNamed:
            if self.node.expr_int.as_type.get.node.expr_named.name.short == "u".toNonFullPath:
              self.meta.type_sum[].add(type_analysis.type_ctx.get("u8".toPath))
              self.meta.type_sum[].add(type_analysis.type_ctx.get("u16".toPath))
              self.meta.type_sum[].add(type_analysis.type_ctx.get("u32".toPath))
              self.meta.type_sum[].add(type_analysis.type_ctx.get("u64".toPath))
              self.meta.type_sum[].add(type_analysis.type_ctx.get("u128".toPath))
              self.meta.type_sum[].add(type_analysis.type_ctx.get("uint".toPath))
            elif self.node.expr_int.as_type.get.node.expr_named.name.short == "s".toNonFullPath:
              self.meta.type_sum[].add(type_analysis.type_ctx.get("s8".toPath))
              self.meta.type_sum[].add(type_analysis.type_ctx.get("s16".toPath))
              self.meta.type_sum[].add(type_analysis.type_ctx.get("s32".toPath))
              self.meta.type_sum[].add(type_analysis.type_ctx.get("s64".toPath))
              self.meta.type_sum[].add(type_analysis.type_ctx.get("s128".toPath))
              self.meta.type_sum[].add(type_analysis.type_ctx.get("sint".toPath))
            else:
              raise newError(errkInvalidType, self.node.expr_int.as_type.get.meta.span, "integer postfix must be an integer type or one of `u`, `s`")
          else:
            let ty = self.node.expr_int.as_type.get.to_type(type_analysis.type_ctx)
            if ty[].is_signed or ty[].is_unsigned:
              self.meta.type_sum[].add(ty)
            else:
              raise newError(errkInvalidType, self.node.expr_int.as_type.get.meta.span, "integer postfix must be an integer type or one of `u`, `s`")
        else:
          self.meta.type_sum[].add(type_analysis.type_ctx.get("s8".toPath))
          self.meta.type_sum[].add(type_analysis.type_ctx.get("s16".toPath))
          self.meta.type_sum[].add(type_analysis.type_ctx.get("s32".toPath))
          self.meta.type_sum[].add(type_analysis.type_ctx.get("s64".toPath))
          self.meta.type_sum[].add(type_analysis.type_ctx.get("s128".toPath))
          self.meta.type_sum[].add(type_analysis.type_ctx.get("sint".toPath))
          self.meta.type_sum[].add(type_analysis.type_ctx.get("u8".toPath))
          self.meta.type_sum[].add(type_analysis.type_ctx.get("u16".toPath))
          self.meta.type_sum[].add(type_analysis.type_ctx.get("u32".toPath))
          self.meta.type_sum[].add(type_analysis.type_ctx.get("u64".toPath))
          self.meta.type_sum[].add(type_analysis.type_ctx.get("u128".toPath))
          self.meta.type_sum[].add(type_analysis.type_ctx.get("uint".toPath))
        return vrkRecurse),
      visit_expr_float: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.expr_float.as_type.isSome:
          let ty = self.node.expr_float.as_type.get.to_type(type_analysis.type_ctx)
          if ty[].is_floating:
            self.meta.type_sum[].add(ty)
          else:
            raise newError(errkInvalidType, self.node.expr_float.as_type.get.meta.span, "floating postfix must be a floating type")
        else:
          self.meta.type_sum[].add(type_analysis.type_ctx.get("float16".toPath))
          self.meta.type_sum[].add(type_analysis.type_ctx.get("float32".toPath))
          self.meta.type_sum[].add(type_analysis.type_ctx.get("float64".toPath))
          self.meta.type_sum[].add(type_analysis.type_ctx.get("float80".toPath))
          self.meta.type_sum[].add(type_analysis.type_ctx.get("float128".toPath))
          self.meta.type_sum[].add(type_analysis.type_ctx.get("float".toPath))
        return vrkRecurse),
      visit_expr_string: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.type_sum[].add(type_analysis.type_ctx.get(["__lang", "builtin", "str"].toPath))
        return vrkRecurse),
      visit_expr_sizeof: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.type_sum[].add(type_analysis.type_ctx.get("uint".toPath))
        return vrkRecurse),
      visit_expr_alignof: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.type_sum[].add(type_analysis.type_ctx.get("uint".toPath))
        return vrkRecurse),
      visit_expr_interp: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.type_sum = self.node.expr_interp.value.meta.type_sum
        return vrkRecurse),
      visit_expr_unary: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        proc unary(self: ref Node, name: string) {.closure.} =
          let args = @[Argument(name: some("value"), ty: self.node.expr_unary.value.meta.type_sum)]
          var instance = type_analysis.type_ctx.find_impl_no_coerce(name.toNonFullPath, none[ref TypeSum](), some(args), false)

          if instance.impls.len > 0:
            var params = instance.impls.toSeq[0].params.mapIt(newTypeSum().toTypeSumRef)
            for impl in instance.impls:
              for i, param in impl.params:
                params[i][].add(param.ty[])

            block:
              let param = params[0]
              let arg = self.node.expr_unary.value
              arg.meta.type_sum[] = type_analysis.type_ctx.restrict_sum_in_span(arg.meta.type_sum, param, self.meta.span)

              if not type_analysis.type_ctx.eval_sum_in_span(arg.meta.type_sum, arg.meta.span):
                raise newError(errkAmbigousType, arg.meta.span, $arg.meta.type_sum[])

            var remove = newSeq[ref Impl]()
            for i, impl in instance.impls:
              for j, param in impl.params:
                if not param.ty.matches(args[j].ty):
                  remove.add(impl)
                  break

            for i in 0 ..< remove.len:
              instance.impls.excl(remove[i])

          case instance.impls.len
          of 0:
            var msg = fmt"{name}({self.node.expr_unary.value.meta.type_sum[]}){'\n'}the following impls were found:{'\n'}"
            for impl in instance.found_impls:
              msg.add("  ")
              msg.add($impl[])
              msg.add("\n")
            raise newError(errkNoImpl, self.meta.span, msg)
          of 1:
            let impl = instance.impls.toSeq[0]
            self.meta.type_sum[] = impl.monomorphize(impl.result_type, args)[0]
            self.meta.impl = some(impl)
            if not impl.builtin:
              let fn = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: Names(short: name.toNonFullPath, full: some(impl.name)))))
              if impl.linkage_name.isSome:
                fn.node.expr_named.name.mangled = some(impl.linkage_name.get)
              else:
                fn.node.expr_named.name.mangle_with_params(args.mapIt(it.ty[].as_qualified))
              let mono = impl.monomorphize(impl.ty, args, instance.type_args)
              fn.meta.type_sum[] = mono[0]
              fn.meta.impl = some(impl)
              let args = @[self.node.expr_unary.value]
              self[] = newNode(self.meta, Hlir(kind: hrkExprApplication, expr_application: HlirExprApplication(fn: fn, args: args)))[]
              if impl.polymorphic:
                let node = if impl.poly_impl.isSome:
                  type_analysis.get_polymorphic(impl.name, impl.poly_impl.get.ty[].as_qualified).get
                else:
                  type_analysis.get_polymorphic(impl.name, impl.ty[].as_qualified).get
                node.replace_params(mono[1])
                node.node.function.type_params = none[seq[ref Node]]()
                node.node.function.monomorphized = true
                if not type_analysis.is_monomorphized_or_add(impl.name, fn.meta.type_sum[].as_qualified, node):
                  lazy.add(node)
          else:
            var msg = fmt"{'\n'}{name}({self.node.expr_unary.value.meta.type_sum[]}):{'\n'}"
            for impl in instance.impls:
              msg.add("  ")
              msg.add($impl[])
              msg.add("\n")
            raise newError(errkAmbigousImpl, self.meta.span, msg)

        case self.node.expr_unary.op
        of hukPostInc: unary(self, "__inc__")
        of hukPostDec: unary(self, "__dec__")
        of hukPreInc: unary(self, "__inc__")
        of hukPreDec: unary(self, "__dec__")
        of hukRef:
          self.meta.type_sum[] = newType(Type(kind: tykPointer, point: TypePointer(pointee: self.node.expr_unary.value.meta.type_sum))).toQualifiedType.toTypeSum
        of hukDeref:
          self.meta.type_sum[] = self.node.expr_unary.value.meta.type_sum[].pointee[]
        of hukPos: unary(self, "__pos__")
        of hukNeg: unary(self, "__neg__")
        of hukNot: unary(self, "__not__")
        of hukColon: unary(self, "__colon__")
        of hukRangeTo: unary(self, "__range_to__")
        of hukRangeFrom: unary(self, "__range_from__")

        return vrkRecurse),
      visit_expr_binary: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        proc index(self: ref Node, name: string) {.closure.} =
          var
            args = @[Argument(name: some("lhs"), ty: self.node.expr_binary.lhs.meta.type_sum), Argument(name: some("rhs"), ty: self.node.expr_binary.rhs.meta.type_sum)]
            instance = type_analysis.type_ctx.find_impl(name.toNonFullPath, none[ref TypeSum](), some(args), false)

          if instance.impls.len > 0:
            var params = instance.impls.toSeq[0].params.mapIt(newTypeSum().toTypeSumRef)
            for impl in instance.impls:
              for i, param in impl.params:
                params[i][].add(param.ty[])

            block:
              let param = params[0]
              let arg = self.node.expr_binary.lhs
              arg.meta.type_sum[] = type_analysis.type_ctx.restrict_sum_in_span(arg.meta.type_sum, param, self.meta.span)

              if not type_analysis.type_ctx.eval_sum_in_span(arg.meta.type_sum, arg.meta.span):
                raise newError(errkAmbigousType, arg.meta.span, $arg.meta.type_sum[])

            block:
              let param = params[1]
              let arg = self.node.expr_binary.rhs
              arg.meta.type_sum[] = type_analysis.type_ctx.restrict_sum_in_span(arg.meta.type_sum, param, self.meta.span)

              discard type_analysis.type_ctx.eval_sum_in_span(arg.meta.type_sum, arg.meta.span)

            # first try if we can find an impl without coercing
            # only then coerce
            block:
              var
                impls = instance.impls
                scores = instance.scores
                remove = newSeq[(int, ref Impl)]()
              for i, impl in impls:
                for j, param in impl.params:
                  if not param.ty.matches(args[j].ty):
                    remove.add((i, impl))
                    break

              for i in 1 .. remove.len:
                impls.excl(remove[^i][1])
                scores.delete(remove[^i][0])

              if impls.len != 0:
                instance.impls = impls
                instance.scores = scores
              else:
                var remove = newSeq[(int, ref Impl)]()
                for i, impl in instance.impls:
                  for j, param in impl.params:
                    if not param.ty.matches(args[j].ty) and not param.ty.matches_coerce(args[j].ty):
                      remove.add((i, impl))
                      break

                for i in 1 .. remove.len:
                  instance.impls.excl(remove[^i][1])
                  instance.scores.delete(remove[^i][0])

          var impls = newSeq[ref Impl]()
          if instance.scores.len > 0:
            let highest = instance.scores.max
            let impls_seq = instance.impls.toSeq
            for i, score in instance.scores:
              if score == highest:
                impls.add(impls_seq[i])

          case impls.len
          of 0:
            var msg = fmt"{name}({self.node.expr_binary.lhs.meta.type_sum[]}, {self.node.expr_binary.rhs.meta.type_sum[]}){'\n'}the following impls were found:{'\n'}"
            for impl in instance.found_impls:
              msg.add("  ")
              msg.add($impl[])
              msg.add("\n")
            raise newError(errkNoImpl, self.meta.span, msg)
          of 1:
            let impl = impls[0]

            var param = impl.params[0]
            if param.ty.matches_coerce(args[0].ty) and not param.ty.matches(args[0].ty):
              let ty = param.ty[].to_hlir(self.node.expr_binary.lhs.meta.span)
              ty.meta.own_type = some(param.ty[].as_qualified_in_span(self.node.expr_binary.lhs.meta.span))
              self.node.expr_binary.lhs[] = newNode(newMeta(self.node.expr_binary.lhs.meta.span), Hlir(kind: hrkExprCast, expr_cast: HlirExprCast(value: self.node.expr_binary.lhs.deepCopy, ty: ty)))[]
              self.node.expr_binary.lhs.meta.type_sum[] = param.ty[]
              args[0].ty = param.ty

            param = impl.params[1]
            if param.ty.matches_coerce(args[1].ty) and not param.ty.matches(args[1].ty):
              let ty = param.ty[].to_hlir(self.node.expr_binary.rhs.meta.span)
              ty.meta.own_type = some(param.ty[].as_qualified_in_span(self.node.expr_binary.rhs.meta.span))
              self.node.expr_binary.rhs[] = newNode(newMeta(self.node.expr_binary.rhs.meta.span), Hlir(kind: hrkExprCast, expr_cast: HlirExprCast(value: self.node.expr_binary.rhs.deepCopy, ty: ty)))[]
              self.node.expr_binary.rhs.meta.type_sum[] = param.ty[]
              args[1].ty = param.ty

            let ty = impl.monomorphize(impl.result_type, args)[0]
            let pointee = ty.pointee
            let is_deref = if pointee.sum.len != 0:
              true
            else:
              false
            self.meta.type_sum[] = ty
            self.meta.impl = some(impl)
            if not impl.builtin:
              # replace self with HlirExprApplication(HlirExprNamed, args..)
              let fn = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: Names(short: name.toNonFullPath, full: some(impl.name)))))
              if impl.linkage_name.isSome:
                fn.node.expr_named.name.mangled = some(impl.linkage_name.get)
              else:
                fn.node.expr_named.name.mangle_with_params(args.mapIt(it.ty[].as_qualified))
              let mono = impl.monomorphize(impl.ty, args, instance.type_args)
              fn.meta.type_sum[] = mono[0]
              fn.meta.impl = some(impl)
              let args = @[self.node.expr_binary.lhs, self.node.expr_binary.rhs]
              self[] = newNode(self.meta, Hlir(kind: hrkExprApplication, expr_application: HlirExprApplication(fn: fn, args: args)))[]
              # monomorphize
              if impl.polymorphic:
                let node = if impl.poly_impl.isSome:
                  type_analysis.get_polymorphic(impl.name, impl.poly_impl.get.ty[].as_qualified).get
                else:
                  type_analysis.get_polymorphic(impl.name, impl.ty[].as_qualified).get
                node.replace_params(mono[1])
                node.node.function.type_params = none[seq[ref Node]]()
                node.node.function.monomorphized = true
                if not type_analysis.is_monomorphized_or_add(impl.name, fn.meta.type_sum[].as_qualified, node):
                  lazy.add(node)
              if is_deref:
                self[] = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprUnary, expr_unary: HlirExprUnary(op: hukDeref, value: self.deepCopy)))[]
                self.meta.type_sum = pointee
          else:
            var msg = fmt"{'\n'}{name}({self.node.expr_binary.lhs.meta.type_sum[]}, {self.node.expr_binary.rhs.meta.type_sum[]}):{'\n'}"
            for impl in impls:
              msg.add("  ")
              msg.add($impl[])
              msg.add("\n")
            raise newError(errkAmbigousImpl, self.meta.span, msg)

        proc binary(self: ref Node, name: string) {.closure.} =
          var
            args = @[Argument(name: some("lhs"), ty: self.node.expr_binary.lhs.meta.type_sum), Argument(name: some("rhs"), ty: self.node.expr_binary.rhs.meta.type_sum)]
            instance = type_analysis.type_ctx.find_impl(name.toNonFullPath, none[ref TypeSum](), some(args), false)

          if instance.impls.len > 0:
            var params = instance.impls.toSeq[0].params.mapIt(newTypeSum().toTypeSumRef)
            for impl in instance.impls:
              for i, param in impl.params:
                params[i][].add(param.ty[])

            block:
              let param = params[0]
              let arg = self.node.expr_binary.lhs
              arg.meta.type_sum[] = type_analysis.type_ctx.restrict_sum_in_span(arg.meta.type_sum, param, self.meta.span)

              if not type_analysis.type_ctx.eval_sum_in_span(arg.meta.type_sum, arg.meta.span):
                raise newError(errkAmbigousType, arg.meta.span, $arg.meta.type_sum[])

            block:
              let param = params[1]
              let arg = self.node.expr_binary.rhs
              arg.meta.type_sum[] = type_analysis.type_ctx.restrict_sum_in_span(arg.meta.type_sum, param, self.meta.span)

              if not type_analysis.type_ctx.eval_sum_in_span(arg.meta.type_sum, arg.meta.span):
                raise newError(errkAmbigousType, arg.meta.span, $arg.meta.type_sum[])

            # first try if we can find an impl without coercing
            # only then coerce
            block:
              var
                impls = instance.impls
                scores = instance.scores
                remove = newSeq[(int, ref Impl)]()
              for i, impl in impls:
                for j, param in impl.params:
                  if not param.ty.matches(args[j].ty):
                    remove.add((i, impl))
                    break

              for i in 1 .. remove.len:
                impls.excl(remove[^i][1])
                scores.delete(remove[^i][0])

              if impls.len != 0:
                instance.impls = impls
                instance.scores = scores
              else:
                var remove = newSeq[(int, ref Impl)]()
                for i, impl in instance.impls:
                  for j, param in impl.params:
                    if not param.ty.matches(args[j].ty) and not param.ty.matches_coerce(args[j].ty):
                      remove.add((i, impl))
                      break

                for i in 1 .. remove.len:
                  instance.impls.excl(remove[^i][1])
                  instance.scores.delete(remove[^i][0])

          var impls = newSeq[ref Impl]()
          if instance.scores.len > 0:
            let highest = instance.scores.max
            let impls_seq = instance.impls.toSeq
            for i, score in instance.scores:
              if score == highest:
                impls.add(impls_seq[i])

          case impls.len
          of 0:
            var msg = fmt"{name}({self.node.expr_binary.lhs.meta.type_sum[]}, {self.node.expr_binary.rhs.meta.type_sum[]}){'\n'}the following impls were found:{'\n'}"
            for impl in instance.found_impls:
              msg.add("  ")
              msg.add($impl[])
              msg.add("\n")
            raise newError(errkNoImpl, self.meta.span, msg)
          of 1:
            let impl = impls[0]

            var param = impl.params[0]
            if param.ty.matches_coerce(args[0].ty) and not param.ty.matches(args[0].ty):
              let ty = param.ty[].to_hlir(self.node.expr_binary.lhs.meta.span)
              ty.meta.own_type = some(param.ty[].as_qualified_in_span(self.node.expr_binary.lhs.meta.span))
              self.node.expr_binary.lhs[] = newNode(newMeta(self.node.expr_binary.lhs.meta.span), Hlir(kind: hrkExprCast, expr_cast: HlirExprCast(value: self.node.expr_binary.lhs.deepCopy, ty: ty)))[]
              self.node.expr_binary.lhs.meta.type_sum[] = param.ty[]
              args[0].ty = param.ty

            param = impl.params[1]
            if param.ty.matches_coerce(args[1].ty) and not param.ty.matches(args[1].ty):
              let ty = param.ty[].to_hlir(self.node.expr_binary.rhs.meta.span)
              ty.meta.own_type = some(param.ty[].as_qualified_in_span(self.node.expr_binary.rhs.meta.span))
              self.node.expr_binary.rhs[] = newNode(newMeta(self.node.expr_binary.rhs.meta.span), Hlir(kind: hrkExprCast, expr_cast: HlirExprCast(value: self.node.expr_binary.rhs.deepCopy, ty: ty)))[]
              self.node.expr_binary.rhs.meta.type_sum[] = param.ty[]
              args[1].ty = param.ty

            self.meta.type_sum[] = impl.monomorphize(impl.result_type, args)[0]
            self.meta.impl = some(impl)
            if not impl.builtin:
              # replace self with HlirExprApplication(HlirExprNamed, args..)
              let fn = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: Names(short: name.toNonFullPath, full: some(impl.name)))))
              if impl.linkage_name.isSome:
                fn.node.expr_named.name.mangled = some(impl.linkage_name.get)
              else:
                fn.node.expr_named.name.mangle_with_params(args.mapIt(it.ty[].as_qualified))
              let mono = impl.monomorphize(impl.ty, args, instance.type_args)
              fn.meta.type_sum[] = mono[0]
              fn.meta.impl = some(impl)
              let args = @[self.node.expr_binary.lhs, self.node.expr_binary.rhs]
              self[] = newNode(self.meta, Hlir(kind: hrkExprApplication, expr_application: HlirExprApplication(fn: fn, args: args)))[]
              # monomorphize
              if impl.polymorphic:
                let node = if impl.poly_impl.isSome:
                  type_analysis.get_polymorphic(impl.name, impl.poly_impl.get.ty[].as_qualified).get
                else:
                  type_analysis.get_polymorphic(impl.name, impl.ty[].as_qualified).get
                node.replace_params(mono[1])
                node.node.function.type_params = none[seq[ref Node]]()
                node.node.function.monomorphized = true
                if not type_analysis.is_monomorphized_or_add(impl.name, fn.meta.type_sum[].as_qualified, node):
                  lazy.add(node)
          else:
            var msg = fmt"{'\n'}{name}({self.node.expr_binary.lhs.meta.type_sum[]}, {self.node.expr_binary.rhs.meta.type_sum[]}):{'\n'}"
            for impl in impls:
              msg.add("  ")
              msg.add($impl[])
              msg.add("\n")
            raise newError(errkAmbigousImpl, self.meta.span, msg)

        proc assign_op(self: ref Node, op: string): bool {.closure.} =
          var
            args = @[Argument(name: some("lhs"), ty: self.node.expr_binary.rhs.meta.type_sum), Argument(name: some("rhs"), ty: self.node.expr_binary.rhs.meta.type_sum)]
            instance = type_analysis.type_ctx.find_impl(op.toNonFullPath, none[ref TypeSum](), some(args), false)

          if instance.impls.len > 0:
            var params = instance.impls.toSeq[0].params.mapIt(newTypeSum().toTypeSumRef)
            for impl in instance.impls:
              for i, param in impl.params:
                params[i][].add(param.ty[])

            block:
              let param = params[0]
              let arg = self.node.expr_binary.lhs
              arg.meta.type_sum[] = type_analysis.type_ctx.restrict_sum_in_span(arg.meta.type_sum, param, self.meta.span)

              if not type_analysis.type_ctx.eval_sum_in_span(arg.meta.type_sum, arg.meta.span):
                raise newError(errkAmbigousType, arg.meta.span, $arg.meta.type_sum[])

            block:
              let param = params[1]
              let arg = self.node.expr_binary.rhs
              arg.meta.type_sum[] = type_analysis.type_ctx.restrict_sum_in_span(arg.meta.type_sum, param, self.meta.span)

              if not type_analysis.type_ctx.eval_sum_in_span(arg.meta.type_sum, arg.meta.span):
                raise newError(errkAmbigousType, arg.meta.span, $arg.meta.type_sum[])

            var remove = newSeq[(int, ref Impl)]()
            for i, impl in instance.impls:
              let param = impl.params[1]
              if not param.ty.matches(args[1].ty) and not param.ty.matches_coerce(args[1].ty):
                remove.add((i, impl))
                break

            for i in 1 .. remove.len:
              instance.impls.excl(remove[^i][1])
              instance.scores.delete(remove[^i][0])

          var impls = newSeq[ref Impl]()
          if instance.scores.len > 0:
            let highest = instance.scores.max
            let impls_seq = instance.impls.toSeq
            for i, score in instance.scores:
              if score == highest:
                impls.add(impls_seq[i])

          case impls.len
          of 0:
            return false
          of 1:
            let impl = impls[0]

            let param = impl.params[1]
            if param.ty.matches_coerce(args[1].ty) and not param.ty.matches(args[1].ty):
              let ty = param.ty[].to_hlir(self.node.expr_binary.rhs.meta.span)
              ty.meta.own_type = some(param.ty[].as_qualified_in_span(self.node.expr_binary.rhs.meta.span))
              self.node.expr_binary.rhs[] = newNode(newMeta(self.node.expr_binary.rhs.meta.span), Hlir(kind: hrkExprCast, expr_cast: HlirExprCast(value: self.node.expr_binary.rhs.deepCopy, ty: ty)))[]
              self.node.expr_binary.rhs.meta.type_sum[] = param.ty[]

            self.meta.type_sum[] = impl.monomorphize(impl.result_type, args)[0]
            self.meta.impl = some(impl)
            if not impl.builtin:
              # replace self with { let ptr = &lhs; *lhs = __op__(*lhs, rhs); *lhs }
              let fn = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: Names(short: op.toNonFullPath, full: some(impl.name)))))
              if impl.linkage_name.isSome:
                fn.node.expr_named.name.mangled = some(impl.linkage_name.get)
              else:
                fn.node.expr_named.name.mangle_with_params(args.mapIt(it.ty[].as_qualified))
              let mono = impl.monomorphize(impl.ty, args, instance.type_args)
              fn.meta.type_sum[] = mono[0]
              fn.meta.impl = some(impl)

              let
                ty = self.node.expr_binary.lhs.meta.type_sum
                ptr_ty = newType(Type(kind: tykPointer, point: TypePointer(pointee: ty))).toQualifiedType.toTypeSumRef

              if not ty.matches(impl.result_type):
                raise newError(errkInvalidType, self.meta.span, fmt"expected {ty[]} but got {impl.result_type[]}")

              let
                let_name = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: Names(short: "tmp.0".toNonFullPath, full: some("tmp.0".toPath), mangled: some("tmp.0".toPath.mangled)), quiet: true)))
                lhs = self.node.expr_binary.lhs
                ptr_lhs = newNode(newMeta(lhs.meta.span), Hlir(kind: hrkExprUnary, expr_unary: HlirExprUnary(op: hukRef, value: lhs)))
                let_expr = newNode(newMeta(lhs.meta.span), Hlir(kind: hrkExprLet, expr_let: HlirExprLet(name_node: let_name, name: some(Names(short: "tmp.0".toNonFullPath, full: some("tmp.0".toPath), mangled: some("tmp.0".toPath.mangled))), value: some(ptr_lhs))))

                tmpl = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: Names(short: "tmp.0".toNonFullPath, full: some("tmp.0".toPath), mangled: some("tmp.0".toPath.mangled)))))
                derefl = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprUnary, expr_unary: HlirExprUnary(op: hukDeref, value: tmpl)))
                tmpr = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: Names(short: "tmp.0".toNonFullPath, full: some("tmp.0".toPath), mangled: some("tmp.0".toPath.mangled)))))
                derefr = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprUnary, expr_unary: HlirExprUnary(op: hukDeref, value: tmpr)))

                args = @[derefr, self.node.expr_binary.rhs]
                app = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprApplication, expr_application: HlirExprApplication(fn: fn, args: args)))

                assignment = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprBinary, expr_binary: HlirExprBinary(op: hbkAssign, lhs: derefl, rhs: app)))

                res = newNode(newMeta(lhs.meta.span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: Names(short: "tmp.0".toNonFullPath, full: some("tmp.0".toPath), mangled: some("tmp.0".toPath.mangled)))))
                deref_res = newNode(newMeta(lhs.meta.span), Hlir(kind: hrkExprUnary, expr_unary: HlirExprUnary(op: hukDeref, value: tmpr)))

                compound = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprCompound, expr_compound: HlirExprCompound(elems: @[let_expr, assignment], last: some(deref_res))))

              ptr_lhs.meta.type_sum = ptr_ty
              let_expr.meta.type_sum = ptr_ty
              tmpl.meta.type_sum = ptr_ty
              derefl.meta.type_sum = ty
              tmpr.meta.type_sum = ptr_ty
              derefr.meta.type_sum = ty
              app.meta.type_sum = impl.result_type
              assignment.meta.type_sum = ty
              res.meta.type_sum = ptr_ty
              deref_res.meta.type_sum = ty

              self[] = compound[]

              # monomorphize
              if impl.polymorphic:
                let node = if impl.poly_impl.isSome:
                  type_analysis.get_polymorphic(impl.name, impl.poly_impl.get.ty[].as_qualified).get
                else:
                  type_analysis.get_polymorphic(impl.name, impl.ty[].as_qualified).get
                node.replace_params(mono[1])
                node.node.function.type_params = none[seq[ref Node]]()
                node.node.function.monomorphized = true
                if not type_analysis.is_monomorphized_or_add(impl.name, fn.meta.type_sum[].as_qualified, node):
                  lazy.add(node)
              return true
          else:
            return false

        proc assign(self: ref Node, name: string) {.closure.} =
          let lhs = self.node.expr_binary.lhs.meta.type_sum
          if not type_analysis.type_ctx.eval_sum_in_span(self.node.expr_binary.lhs.meta.type_sum, self.node.expr_binary.lhs.meta.span):
            raise newError(errkAmbigousType, self.node.expr_binary.lhs.meta.span, $self.node.expr_binary.lhs.meta.type_sum[])
          let ptr_lhs = newType(Type(kind: tykPointer, point: TypePointer(pointee: lhs))).toQualifiedType.toTypeSumRef
          var
            args = @[Argument(name: some("lhs"), ty: ptr_lhs), Argument(name: some("rhs"), ty: self.node.expr_binary.rhs.meta.type_sum)]
            instance = type_analysis.type_ctx.find_impl(name.toNonFullPath, none[ref TypeSum](), some(args), false)

          if instance.impls.len > 0:
            var params = instance.impls.toSeq[0].params.mapIt(newTypeSum().toTypeSumRef)
            for impl in instance.impls:
              for i, param in impl.params:
                params[i][].add(param.ty[])

            block:
              let param = params[1]
              let arg = self.node.expr_binary.rhs
              arg.meta.type_sum[] = type_analysis.type_ctx.restrict_sum_in_span(arg.meta.type_sum, param, self.meta.span)

              if not type_analysis.type_ctx.eval_sum_in_span(arg.meta.type_sum, arg.meta.span):
                raise newError(errkAmbigousType, arg.meta.span, $arg.meta.type_sum[])

            var remove = newSeq[(int, ref Impl)]()
            for i, impl in instance.impls:
              let param = impl.params[1]
              if not param.ty.matches(args[1].ty) and not param.ty.matches_coerce(args[1].ty):
                remove.add((i, impl))
                break

            for i in 1 .. remove.len:
              instance.impls.excl(remove[^i][1])
              instance.scores.delete(remove[^i][0])

          var impls = newSeq[ref Impl]()
          if instance.scores.len > 0:
            let highest = instance.scores.max
            let impls_seq = instance.impls.toSeq
            for i, score in instance.scores:
              if score == highest:
                impls.add(impls_seq[i])

          case impls.len
          of 0:
            var msg = fmt"{name}({self.node.expr_binary.lhs.meta.type_sum[]}, {self.node.expr_binary.rhs.meta.type_sum[]}){'\n'}the following impls were found:{'\n'}"
            for impl in instance.found_impls:
              msg.add("  ")
              msg.add($impl[])
              msg.add("\n")
            raise newError(errkNoImpl, self.meta.span, msg)
          of 1:
            let impl = impls[0]

            let param = impl.monomorphize(impl.params[1].ty, args[0 ..< 1])[0].toTypeSumRef
            if param.matches_coerce(args[1].ty) and not param.matches(args[1].ty):
              let ty = param[].to_hlir(self.node.expr_binary.rhs.meta.span)
              ty.meta.own_type = some(param[].as_qualified_in_span(self.node.expr_binary.rhs.meta.span))
              self.node.expr_binary.rhs[] = newNode(newMeta(self.node.expr_binary.rhs.meta.span), Hlir(kind: hrkExprCast, expr_cast: HlirExprCast(value: self.node.expr_binary.rhs.deepCopy, ty: ty)))[]
              self.node.expr_binary.rhs.meta.type_sum[] = param[]

            self.meta.type_sum[] = impl.monomorphize(impl.result_type, args)[0]
            self.meta.impl = some(impl)
            if not impl.builtin:
              # replace self with __assign__(&lhs, rhs)
              let fn = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: Names(short: name.toNonFullPath, full: some(impl.name)))))
              if impl.linkage_name.isSome:
                fn.node.expr_named.name.mangled = some(impl.linkage_name.get)
              else:
                fn.node.expr_named.name.mangle_with_params(args.mapIt(it.ty[].as_qualified))
              let mono = impl.monomorphize(impl.ty, args, instance.type_args)
              fn.meta.type_sum[] = mono[0]
              fn.meta.impl = some(impl)
              let
                lhs = self.node.expr_binary.lhs
                ptr_lhs = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprUnary, expr_unary: HlirExprUnary(op: hukRef, value: lhs)))
                args = @[ptr_lhs, self.node.expr_binary.rhs]
              self[] = newNode(self.meta, Hlir(kind: hrkExprApplication, expr_application: HlirExprApplication(fn: fn, args: args)))[]
              # monomorphize
              if impl.polymorphic:
                let node = if impl.poly_impl.isSome:
                  type_analysis.get_polymorphic(impl.name, impl.poly_impl.get.ty[].as_qualified).get
                else:
                  type_analysis.get_polymorphic(impl.name, impl.ty[].as_qualified).get
                node.replace_params(mono[1])
                node.node.function.type_params = none[seq[ref Node]]()
                node.node.function.monomorphized = true
                if not type_analysis.is_monomorphized_or_add(impl.name, fn.meta.type_sum[].as_qualified, node):
                  lazy.add(node)
          else:
            var msg = fmt"{'\n'}{name}({self.node.expr_binary.lhs.meta.type_sum[]}, {self.node.expr_binary.rhs.meta.type_sum[]}):{'\n'}"
            for impl in impls:
              msg.add("  ")
              msg.add($impl[])
              msg.add("\n")
            raise newError(errkAmbigousImpl, self.meta.span, msg)

        proc assign(self: ref Node, name: string, op: string) {.closure.} =
          let lhs = self.node.expr_binary.lhs.meta.type_sum
          if not type_analysis.type_ctx.eval_sum_in_span(self.node.expr_binary.lhs.meta.type_sum, self.node.expr_binary.lhs.meta.span):
            raise newError(errkAmbigousType, self.node.expr_binary.lhs.meta.span, $self.node.expr_binary.lhs.meta.type_sum[])
          let ptr_lhs = newType(Type(kind: tykPointer, point: TypePointer(pointee: lhs))).toQualifiedType.toTypeSumRef
          var
            args = @[Argument(name: some("lhs"), ty: ptr_lhs), Argument(name: some("rhs"), ty: self.node.expr_binary.rhs.meta.type_sum)]
            instance = type_analysis.type_ctx.find_impl(name.toNonFullPath, none[ref TypeSum](), some(args), false)

          if instance.impls.len == 0:
            if assign_op(self, op):
              return

          if instance.impls.len > 0:
            var params = instance.impls.toSeq[0].params.mapIt(newTypeSum().toTypeSumRef)
            for impl in instance.impls:
              for i, param in impl.params:
                params[i][].add(param.ty[])

            block:
              let param = params[1]
              let arg = self.node.expr_binary.rhs
              arg.meta.type_sum[] = type_analysis.type_ctx.restrict_sum_in_span(arg.meta.type_sum, param, self.meta.span)

              if not type_analysis.type_ctx.eval_sum_in_span(arg.meta.type_sum, arg.meta.span):
                raise newError(errkAmbigousType, arg.meta.span, $arg.meta.type_sum[])

            var remove = newSeq[(int, ref Impl)]()
            for i, impl in instance.impls:
              let param = impl.params[1]
              if not param.ty.matches(args[1].ty) and not param.ty.matches_coerce(args[1].ty):
                remove.add((i, impl))
                break

            for i in 1 .. remove.len:
              instance.impls.excl(remove[^i][1])
              instance.scores.delete(remove[^i][0])

          var impls = newSeq[ref Impl]()
          if instance.scores.len > 0:
            let highest = instance.scores.max
            let impls_seq = instance.impls.toSeq
            for i, score in instance.scores:
              if score == highest:
                impls.add(impls_seq[i])

          case impls.len
          of 0:
            var msg = fmt"{name}({self.node.expr_binary.lhs.meta.type_sum[]}, {self.node.expr_binary.rhs.meta.type_sum[]}){'\n'}the following impls were found:{'\n'}"
            for impl in instance.found_impls:
              msg.add("  ")
              msg.add($impl[])
              msg.add("\n")
            raise newError(errkNoImpl, self.meta.span, msg)
          of 1:
            let impl = impls[0]

            let param = impl.params[1]
            if param.ty.matches_coerce(args[1].ty) and not param.ty.matches(args[1].ty):
              let ty = param.ty[].to_hlir(self.node.expr_binary.rhs.meta.span)
              ty.meta.own_type = some(param.ty[].as_qualified_in_span(self.node.expr_binary.rhs.meta.span))
              self.node.expr_binary.rhs[] = newNode(newMeta(self.node.expr_binary.rhs.meta.span), Hlir(kind: hrkExprCast, expr_cast: HlirExprCast(value: self.node.expr_binary.rhs.deepCopy, ty: ty)))[]
              self.node.expr_binary.rhs.meta.type_sum[] = param.ty[]

            self.meta.type_sum[] = impl.monomorphize(impl.result_type, args)[0]
            self.meta.impl = some(impl)
            if not impl.builtin:
              # replace self with __assign_op__(&lhs, rhs)
              let fn = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: Names(short: name.toNonFullPath, full: some(impl.name)))))
              if impl.linkage_name.isSome:
                fn.node.expr_named.name.mangled = some(impl.linkage_name.get)
              else:
                fn.node.expr_named.name.mangle_with_params(args.mapIt(it.ty[].as_qualified))
              let mono = impl.monomorphize(impl.ty, args, instance.type_args)
              fn.meta.type_sum[] = mono[0]
              fn.meta.impl = some(impl)
              let
                lhs = self.node.expr_binary.lhs
                ptr_lhs = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprUnary, expr_unary: HlirExprUnary(op: hukRef, value: lhs)))
                args = @[ptr_lhs, self.node.expr_binary.rhs]
              self[] = newNode(self.meta, Hlir(kind: hrkExprApplication, expr_application: HlirExprApplication(fn: fn, args: args)))[]
              # monomorphize
              if impl.polymorphic:
                let node = if impl.poly_impl.isSome:
                  type_analysis.get_polymorphic(impl.name, impl.poly_impl.get.ty[].as_qualified).get
                else:
                  type_analysis.get_polymorphic(impl.name, impl.ty[].as_qualified).get
                node.replace_params(mono[1])
                node.node.function.type_params = none[seq[ref Node]]()
                node.node.function.monomorphized = true
                if not type_analysis.is_monomorphized_or_add(impl.name, fn.meta.type_sum[].as_qualified, node):
                  lazy.add(node)
          else:
            var msg = fmt"{'\n'}{name}({self.node.expr_binary.lhs.meta.type_sum[]}, {self.node.expr_binary.rhs.meta.type_sum[]}):{'\n'}"
            for impl in impls:
              msg.add("  ")
              msg.add($impl[])
              msg.add("\n")
            raise newError(errkAmbigousImpl, self.meta.span, msg)

        proc logical(self: ref Node) {.closure.} =
          var
            args = @[Argument(name: some("value"), ty: self.node.expr_binary.lhs.meta.type_sum)]
            instance = type_analysis.type_ctx.find_impl("__ask__".toNonFullPath, none[ref TypeSum](), some(args), false)

          if instance.impls.len > 0:
            var params = instance.impls.toSeq[0].params.mapIt(newTypeSum().toTypeSumRef)
            for impl in instance.impls:
              for i, param in impl.params:
                params[i][].add(param.ty[])

            block:
              let param = params[0]
              let arg = self.node.expr_binary.lhs
              arg.meta.type_sum[] = type_analysis.type_ctx.restrict_sum_in_span(arg.meta.type_sum, param, self.meta.span)

              if not type_analysis.type_ctx.eval_sum_in_span(arg.meta.type_sum, arg.meta.span):
                raise newError(errkAmbigousType, arg.meta.span, $arg.meta.type_sum[])

            var remove = newSeq[(int, ref Impl)]()
            for i, impl in instance.impls:
              for j, param in impl.params:
                if not param.ty.matches(args[j].ty) and not param.ty.matches_coerce(args[j].ty):
                  remove.add((i, impl))
                  break

            for i in 1 .. remove.len:
              instance.impls.excl(remove[^i][1])
              instance.scores.delete(remove[^i][0])

          var impls = newSeq[ref Impl]()
          if instance.scores.len > 0:
            let highest = instance.scores.max
            let impls_seq = instance.impls.toSeq
            for i, score in instance.scores:
              if score == highest:
                impls.add(impls_seq[i])

          self.node.expr_binary.rhs.meta.type_sum[] = type_analysis.type_ctx.restrict_sum_in_span(self.node.expr_binary.rhs.meta.type_sum, self.node.expr_binary.lhs.meta.type_sum, self.meta.span)

          if not type_analysis.type_ctx.eval_sum_in_span(self.node.expr_binary.rhs.meta.type_sum, self.node.expr_binary.rhs.meta.span):
            raise newError(errkAmbigousType, self.node.expr_binary.rhs.meta.span, $self.node.expr_binary.rhs.meta.type_sum[])

          if not self.node.expr_binary.lhs.meta.type_sum.matches(self.node.expr_binary.rhs.meta.type_sum):
            raise newError(errkInvalidType, self.node.expr_binary.rhs.meta.span, fmt"expected {self.node.expr_binary.lhs.meta.type_sum[]} but got {self.node.expr_binary.rhs.meta.type_sum[]}")

          case impls.len
          of 0:
            var msg = fmt"__ask__({self.node.expr_binary.lhs.meta.type_sum[]}){'\n'}the following impls were found:{'\n'}"
            for impl in instance.found_impls:
              msg.add("  ")
              msg.add($impl[])
              msg.add("\n")
            raise newError(errkNoImpl, self.meta.span, msg)
          of 1:
            let impl = impls[0]
            self.meta.type_sum = self.node.expr_binary.lhs.meta.type_sum
            self.meta.impl = some(impl)
            if not impl.builtin:
              # replace self with { let tmp.0 = `lhs`; __ask__(_tmp)? tmp.0 : `rhs` }
              let fn = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: Names(short: "__ask__".toNonFullPath, full: some(impl.name)))))
              if impl.linkage_name.isSome:
                fn.node.expr_named.name.mangled = some(impl.linkage_name.get)
              else:
                fn.node.expr_named.name.mangle_with_params(args.mapIt(it.ty[].as_qualified))
              let mono = impl.monomorphize(impl.ty, args, instance.type_args)
              fn.meta.type_sum[] = mono[0]
              fn.meta.impl = some(impl)
              let args = @[self.node.expr_binary.lhs, self.node.expr_binary.rhs]
              let tmp = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: Names(short: "tmp.0".toNonFullPath, full: some("tmp.0".toPath), mangled: some("tmp.0".toPath.mangled)))))
              tmp.meta.type_sum = self.meta.type_sum
              let let_name = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: Names(short: "tmp.0".toNonFullPath, full: some("tmp.0".toPath), mangled: some("tmp.0".toPath.mangled)), quiet: true)))
              let ret_val = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: Names(short: "tmp.0".toNonFullPath, full: some("tmp.0".toPath), mangled: some("tmp.0".toPath.mangled)))))
              ret_val.meta.type_sum = self.meta.type_sum
              let ask = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprApplication, expr_application: HlirExprApplication(fn: fn, args: @[tmp])))
              ask.meta.type_sum = self.meta.type_sum
              let let_expr = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprLet, expr_let: HlirExprLet(name_node: let_name, name: some(Names(short: "tmp.0".toNonFullPath, full: some("tmp.0".toPath), mangled: some("tmp.0".toPath.mangled))), value: some(self.node.expr_binary.lhs))))
              let_expr.meta.type_sum = self.meta.type_sum
              let select = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprTernary, expr_ternary: HlirExprTernary(value: ask, lhs: some(ret_val), rhs: some(self.node.expr_binary.rhs))))
              select.meta.type_sum = self.meta.type_sum
              let compound = newNode(self.meta, Hlir(kind: hrkExprCompound, expr_compound: HlirExprCompound(elems: @[let_expr], last: some(select))))
              self[] = compound[]
              # monomorphize
              if impl.polymorphic:
                let node = if impl.poly_impl.isSome:
                  type_analysis.get_polymorphic(impl.name, impl.poly_impl.get.ty[].as_qualified).get
                else:
                  type_analysis.get_polymorphic(impl.name, impl.ty[].as_qualified).get
                node.replace_params(mono[1])
                node.node.function.type_params = none[seq[ref Node]]()
                node.node.function.monomorphized = true
                if not type_analysis.is_monomorphized_or_add(impl.name, fn.meta.type_sum[].as_qualified, node):
                  lazy.add(node)
          else:
            var msg = fmt"{'\n'}__ask__({self.node.expr_binary.lhs.meta.type_sum[]}):{'\n'}"
            for impl in impls:
              msg.add("  ")
              msg.add($impl[])
              msg.add("\n")
            raise newError(errkAmbigousImpl, self.meta.span, msg)

        case self.node.expr_binary.op
        of hbkIndex: index(self, "__index__")
        of hbkAdd: binary(self, "__add__")
        of hbkSub: binary(self, "__sub__")
        of hbkMul: binary(self, "__mul__")
        of hbkDiv: binary(self, "__div__")
        of hbkRem: binary(self, "__rem__")
        of hbkShl: binary(self, "__shl__")
        of hbkShr: binary(self, "__shr__")
        of hbkLt: binary(self, "__lt__")
        of hbkGt: binary(self, "__gt__")
        of hbkLe: binary(self, "__le__")
        of hbkGe: binary(self, "__ge__")
        of hbkEq: binary(self, "__eq__")
        of hbkNe: binary(self, "__ne__")
        of hbkBitand: binary(self, "__bitand__")
        of hbkBitor: binary(self, "__bitor__")
        of hbkBitxor: binary(self, "__bitxor__")
        of hbkLogicalAnd: logical(self)
        of hbkLogicalOr: logical(self)
        of hbkRange: binary(self, "__range__")
        of hbkAssign: assign(self, "__assign__")
        of hbkAssignMul: assign(self, "__assign_mul__", "__mul__")
        of hbkAssignDiv: assign(self, "__assign_div__", "__div__")
        of hbkAssignRem: assign(self, "__assign_rem__", "__rem__")
        of hbkAssignAdd: assign(self, "__assign_add__", "__add__")
        of hbkAssignSub: assign(self, "__assign_sub__", "__sub__")
        of hbkAssignLshift: assign(self, "__assign_shl__", "__shl__")
        of hbkAssignRshift: assign(self, "__assign_shr__", "__shr__")
        of hbkAssignAnd: assign(self, "__assign_and__", "__and__")
        of hbkAssignXor: assign(self, "__assign_xor__", "__xor__")
        of hbkAssignOr: assign(self, "__assign_or__", "__or__")
        of hbkArrow: binary(self, "__arrow__")

        return vrkRecurse),
      visit_expr_range_full: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.type_sum[].add(type_analysis.type_ctx.get(["__lang", "builtin", "RangeFull"].toPath))
        return vrkRecurse),
      visit_expr_array: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let sizet = type_analysis.type_ctx.get("uint".toPath)
        let size = newType(Type(kind: tykLit, lit: TypeLiteral(kind: lkInt, ty: sizet.toTypeSumRef, intval: self.node.expr_array.elems.len))).toQualifiedType.toTypeSumRef
        if self.node.expr_array.elems.len != 0:
          let elem = self.node.expr_array.elems[0].meta.type_sum
          self.meta.type_sum[].add(newType(Type(kind: tykArray, arr: TypeArray(elem: elem, size: size))).toQualifiedType)
          self.meta.type_sum[].add(newType(Type(kind: tykSlice, slice: TypeSlice(elem: elem))).toQualifiedType)
        else:
          self.meta.type_sum[].add(newType(Type(kind: tykArray, arr: TypeArray(elem: newTypeSum().toTypeSumRef, size: size))).toQualifiedType)
          self.meta.type_sum[].add(newType(Type(kind: tykSlice, slice: TypeSlice(elem: newTypeSum().toTypeSumRef))).toQualifiedType)
        return vrkRecurse),
      visit_expr_parenth: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.is_type:
          return vrkRecurse

        if self.node.expr_parenth.elems.len == 1:
          self.meta.type_sum = self.node.expr_parenth.elems[0].meta.type_sum
        return vrkRecurse),
      visit_expr_cast: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        # TODO: __cast__ operator
        self.meta.type_sum[].add(self.node.expr_cast.ty.to_type(type_analysis.type_ctx))
        return vrkRecurse),
      visit_expr_application: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.is_type:
          return vrkRecurse

        proc overload() =
          if self.node.expr_application.fn.node.kind == hrkExprNamed:
            # resolution or hrkExprNamed is inhibited if parent is hrkExprApplication, so we need to recalculate the
            # impl here
            let fn = self.node.expr_application.fn

            let name = fn.node.expr_named.name.short

            let instance = type_analysis.type_ctx.find_impl(name, none[ref TypeSum](), none[seq[Argument]](), false)
            case instance.impls.len
            of 0:
              raise newError(errkNoImpl, fn.meta.span, $name)
            of 1:
              let impl = instance.impls.toSeq[0]
              fn.meta.type_sum[] = impl.ty[]
              fn.meta.type_sum = impl.ty
              fn.meta.impl = some(impl)
              fn.node.expr_named.name.full = some(impl.name)
              if impl.linkage_name.isSome:
                fn.node.expr_named.name.mangled = some(impl.linkage_name.get)
              else:
                fn.node.expr_named.name.mangle()
            else:
              var msg = fmt"{'\n'}{name}:{'\n'}"
              for impl in instance.impls:
                msg.add("  ")
                msg.add($impl[])
                msg.add("\n")
              raise newError(errkAmbigousImpl, fn.meta.span, msg)
          let type_function = self.node.expr_application.fn.meta.type_sum[].as_qualified_in_span(self.meta.span)
          case type_function.inner.kind
          of tykFunction:
            if type_function.params.len != self.node.expr_application.args.len:
              let msg = fmt"invalid argument count for {type_function[]}"
              raise newError(errkNoImpl, self.meta.span, msg)
            for i, param in type_function.params:
              let arg = self.node.expr_application.args[i]
              if not param.ty.matches(arg.meta.type_sum):
                let msg = fmt"invalid argument at position {i} for {type_function[]}"
                raise newError(errkNoImpl, self.meta.span, msg)
              else:
                arg.meta.type_sum[] = type_analysis.type_ctx.restrict_sum_in_span(arg.meta.type_sum, param.ty, self.meta.span)
            self.meta.type_sum = type_function.result_type
          else:
            # __call__ overload
            var args = newSeq[Argument]()
            let
              fn_type = self.node.expr_application.fn.meta.type_sum
              fn_pointer = newType(Type(kind: tykPointer, point: TypePointer(pointee: fn_type))).toQualifiedType.toTypeSumRef
            args.add(Argument(ty: fn_pointer))
            for arg in self.node.expr_application.args:
              case arg.node.kind
              of hrkDecl:
                args.add(Argument(name: arg.node.decl.name.map((name) => name.node.expr_named.name.short.path[^1]), ty: arg.meta.type_sum))
              else:
                args.add(Argument(name: none[string](), ty: arg.meta.type_sum))

            let name = "__call__".toNonFullPath

            var instance = type_analysis.type_ctx.find_impl(name, none[ref TypeSum](), some(args), false)

            if instance.impls.len > 0 and instance.impls.toSeq[0].kind != ikCons:
              var params = instance.impls.toSeq[0].params.mapIt(newTypeSum().toTypeSumRef)
              for impl in instance.impls:
                for i, param in impl.params:
                  params[i][].add(param.ty[])

              for i in 1 ..< params.len:
                let param = params[i]
                let arg = self.node.expr_application.args[i - 1]
                arg.meta.type_sum[] = type_analysis.type_ctx.restrict_sum_in_span(arg.meta.type_sum, param, self.meta.span)

                discard type_analysis.type_ctx.eval_sum_in_span(arg.meta.type_sum, arg.meta.span)

              var remove = newSeq[(int, ref Impl)]()
              for i, impl in instance.impls:
                for j, param in impl.params:
                  if not param.ty.matches(args[j].ty) and not param.ty.matches_coerce(args[j].ty):
                    remove.add((i, impl))
                    break

              for i in 1 .. remove.len:
                instance.impls.excl(remove[^i][1])
                instance.scores.delete(remove[^i][0])
            elif instance.impls.len > 0 and instance.impls.toSeq[0].kind == ikCons:
              for arg in self.node.expr_application.args:
                if not type_analysis.type_ctx.eval_sum_in_span(arg.meta.type_sum, arg.meta.span):
                  raise newError(errkAmbigousType, arg.meta.span, $arg.meta.type_sum[])

            var impls = newSeq[ref Impl]()
            if instance.scores.len > 0:
              let highest = instance.scores.max
              let impls_seq = instance.impls.toSeq
              for i, score in instance.scores:
                if score == highest:
                  impls.add(impls_seq[i])

            case impls.len
            of 0:
              var msg = fmt"{name}("
              for i, arg in args.pairs:
                if i > 0:
                  msg.add(", ")
                msg.add($arg.ty[])
              msg.add(")\nthe following impls were found:\n")
              for impl in instance.found_impls:
                msg.add("  ")
                msg.add($impl[])
                msg.add("\n")
              raise newError(errkNoImpl, self.meta.span, msg)
            of 1:
              let impl = impls[0]

              var j = 0
              for param in impl.params:
                if param.name.isSome:
                  let arg = find_arg(args, param.name.get)
                  if arg.isSome:
                    let (i, arg) = arg.get
                    if param.ty.matches_coerce(arg.ty) and not param.ty.matches(arg.ty):
                      let ty = param.ty[].to_hlir(self.node.expr_application.args[i].meta.span)
                      ty.meta.own_type = some(param.ty[].as_qualified_in_span(self.node.expr_application.args[i].meta.span))
                      if self.node.expr_application.args[i].node.kind == hrkDecl:
                        self.node.expr_application.args[i].node.decl.value[] = newNode(newMeta(self.node.expr_application.args[i].node.decl.value.meta.span), Hlir(kind: hrkExprCast, expr_cast: HlirExprCast(value: self.node.expr_application.args[i].node.decl.value.deepCopy, ty: ty)))[]
                        self.node.expr_application.args[i].node.decl.value.meta.type_sum[] = param.ty[]
                      else:
                        self.node.expr_application.args[i][] = newNode(newMeta(self.node.expr_application.args[i].meta.span), Hlir(kind: hrkExprCast, expr_cast: HlirExprCast(value: self.node.expr_application.args[i].deepCopy, ty: ty)))[]
                        self.node.expr_application.args[i].meta.type_sum[] = param.ty[]
                    j = i + 1
                    continue
                if j >= args.len:
                  break
                let arg = args[j]
                if param.ty.matches_coerce(arg.ty) and not param.ty.matches(arg.ty):
                  let ty = param.ty[].to_hlir(self.node.expr_application.args[j].meta.span)
                  ty.meta.own_type = some(param.ty[].as_qualified_in_span(self.node.expr_application.args[j].meta.span))
                  if self.node.expr_application.args[j].node.kind == hrkDecl:
                    self.node.expr_application.args[j].node.decl.value[] = newNode(newMeta(self.node.expr_application.args[j].node.decl.value.meta.span), Hlir(kind: hrkExprCast, expr_cast: HlirExprCast(value: self.node.expr_application.args[j].node.decl.value.deepCopy, ty: ty)))[]
                    self.node.expr_application.args[j].node.decl.value.meta.type_sum[] = param.ty[]
                  else:
                    self.node.expr_application.args[j][] = newNode(newMeta(self.node.expr_application.args[j].meta.span), Hlir(kind: hrkExprCast, expr_cast: HlirExprCast(value: self.node.expr_application.args[j].deepCopy, ty: ty)))[]
                    self.node.expr_application.args[j].meta.type_sum[] = param.ty[]
                inc j

              self.meta.type_sum[] = impl.monomorphize(impl.result_type, args, instance.type_args)[0]
              self.meta.impl = some(impl)
              case impl.kind
              of ikFn:
                let mono = impl.monomorphize(impl.ty, args, instance.type_args)
                let name_full = impl.name
                var mangled: string

                let args = args.mapIt(it.ty[].as_qualified)
                if impl.linkage_name.isSome:
                  mangled = impl.linkage_name.get
                else:
                  if impl.kind == ikVar:
                    mangled = name_full.mangled
                  else:
                    mangled = name_full.mangled_with_params(args)

                let fn = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: Names(short: name, full: some(name_full), mangled: some(mangled)))))
                fn.meta.type_sum[] = mono[0]
                fn.meta.impl = some(impl)
                var app_args = newSeq[ref Node]()
                let arg0 = newNode(newMeta(self.node.expr_application.fn.meta.span), Hlir(kind: hrkExprUnary, expr_unary: HlirExprUnary(op: hukRef, value: self.node.expr_application.fn)))
                arg0.node.expr_unary.value.meta.type_sum = impl.params[0].ty[].pointee
                arg0.meta.type_sum = impl.params[0].ty
                app_args.add(arg0)
                app_args.add(self.node.expr_application.args)
                let app = newNode(self.meta, Hlir(kind: hrkExprApplication, expr_application: HlirExprApplication(fn: fn, args: app_args)))
                self[] = app[]

                if impl.polymorphic:
                  let node = if impl.poly_impl.isSome:
                    type_analysis.get_polymorphic(impl.name, impl.poly_impl.get.ty[].as_qualified).get
                  else:
                    type_analysis.get_polymorphic(impl.name, impl.ty[].as_qualified).get
                  node.replace_params(mono[1])
                  node.node.function.type_params = none[seq[ref Node]]()
                  node.node.function.monomorphized = true
                  if not type_analysis.is_monomorphized_or_add(impl.name, fn.meta.type_sum[].as_qualified, node):
                    lazy.add(node)
              of ikVar, ikCons:
                raise newIce("unreachable")
            else:
              var msg = fmt"{'\n'}{name}("
              for i, arg in args.pairs:
                if i > 0:
                  msg.add(", ")
                msg.add($arg.ty[])
              msg.add("):\n")
              for impl in instance.impls:
                msg.add("  ")
                msg.add($impl[])
                msg.add("\n")
              raise newError(errkAmbigousImpl, self.meta.span, msg)

        # FIXME: don't restrict this to the first argument, but to the first trait argument
        proc virtual(name: string, args: seq[Argument]): VisitResultKind {.closure.} =
          let trait = args[0].ty[].as_qualified
          for idx, fn in trait.inner.trait.fns:
            block loop:
              if fn.name.isSome and fn.name.get == name:
                let unit_ptr = newType(Type(kind: tykPointer, point: TypePointer(pointee: newType(Type(kind: tykUnit, unit: TypeUnit())).toQualifiedType.toTypeSumRef))).toQualifiedType.toTypeSumRef
                let fn = fn.ty[].as_qualified.deepCopy
                fn.inner.function.params[0].ty = unit_ptr
                var
                  params = fn.inner.function.params
                  args = newSeqOfCap[ref TypeSum](params.len)

                for i, param in params[1 ..^ 1]:
                  let arg = self.node.expr_application.args[i]

                  if not arg.meta.type_sum.matches(param.ty) and not arg.meta.type_sum.matches_coerce(param.ty):
                    break loop

                  arg.meta.type_sum[] = type_analysis.type_ctx.restrict_sum_in_span(arg.meta.type_sum, param.ty, self.meta.span)
                  discard type_analysis.type_ctx.eval_sum_in_span(arg.meta.type_sum, arg.meta.span)
                  args.add(arg.meta.type_sum)

                let trait_vtable = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprFieldAccess, expr_field_access: HlirExprFieldAccess(value: self.node.expr_application.args[0].deepCopy, name: "vtable")))
                let index = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprInt, expr_int: HlirExprInt(value: idx)))
                let trait_vtable_index = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprBinary, expr_binary: HlirExprBinary(lhs: trait_vtable, rhs: index)))
                let fn_ty = fn[].to_hlir(self.meta.span)
                fn_ty.meta.own_type = some(fn)
                let fn_cast = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprCast, expr_cast: HlirExprCast(value: trait_vtable_index, ty: fn_ty)))

                let trait_data = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprFieldAccess, expr_field_access: HlirExprFieldAccess(value: self.node.expr_application.args[0].deepCopy, name: "data")))
                self.node.expr_application.args[0] = trait_data

                self.node.expr_application.fn[] = fn_cast[]

                return vrkRepeat
          raise newError(errkNoImpl, self.meta.span, fmt"trait {trait.inner.trait.name} doesn't provide this function")

        let fn = self.node.expr_application.fn
        case fn.node.kind
        of hrkExprNamed:
          var args = newSeq[Argument]()
          for arg in self.node.expr_application.args:
            case arg.node.kind
            of hrkDecl:
              args.add(Argument(name: arg.node.decl.name.map((name) => name.node.expr_named.name.short.path[^1]), ty: arg.meta.type_sum))
            else:
              args.add(Argument(name: none[string](), ty: arg.meta.type_sum))

          let name = fn.node.expr_named.name.short

          if args.len > 0 and args[0].ty[].is_qualified and args[0].ty[].as_qualified[].is_trait:
            return virtual(name.path[0], args)

          var instance: Instance
          if fn.node.expr_named.type_args.isSome:
            var type_args = newSeq[tuple[name: Option[string], ty: ref QualifiedType]]()
            for arg in fn.node.expr_named.type_args.get:
              case arg.node.kind
              of hrkDecl:
                type_args.add((some(arg.node.decl.name.get.node.expr_named.name.short.path[^1]), arg.meta.own_type.get))
              else:
                type_args.add((none[string](), arg.meta.own_type.get))
            instance = type_analysis.type_ctx.find_impl_with_args(name, none[ref TypeSum](), some(args), type_args, false)
          else:
            instance = type_analysis.type_ctx.find_impl(name, none[ref TypeSum](), some(args), false)

          if instance.impls.len > 0 and instance.impls.toSeq[0].kind != ikCons:
            var params_tmp = instance.impls.toSeq[0].params.mapIt(newTypeSum().toTypeSumRef)
            for impl in instance.impls:
              if impl.kind != ikFn or not impl.is_macro or impl.builtin:
                for i, param in impl.params:
                  if param.ty[].as_qualified[].is_var:
                    break
                  params_tmp[i][].add(param.ty[])
            # it.sum.len == 0 for varargs
            let params = params_tmp.filterIt(it.sum.len > 0)

            for i, param in params:
              let arg = self.node.expr_application.args[i]
              arg.meta.type_sum[] = type_analysis.type_ctx.restrict_sum_in_span(arg.meta.type_sum, param, self.meta.span)

              discard type_analysis.type_ctx.eval_sum_in_span(arg.meta.type_sum, arg.meta.span)

            # varargs
            for arg in self.node.expr_application.args[params.len ..^ 1]:
              try:
                discard type_analysis.type_ctx.eval_sum_in_span(arg.meta.type_sum, arg.meta.span)
              except Error:
                let e = (ref Error)(getCurrentException())
                if e.kind != errkUnspecifiedType:
                  raise e

            var remove = newSeq[(int, ref Impl)]()
            for i, impl in instance.impls:
              let endidx = if impl.ampvar.isSome: 2 else: 1
              for j, param in impl.params[0 ..^ endidx]:
                if not param.ty.matches(args[j].ty) and not param.ty.matches_coerce(args[j].ty):
                  remove.add((i, impl))
                  break
              if impl.ampvar.isSome:
                let param = impl.ampvar.get
                for j in impl.params.len - endidx + 1 ..< args.len:
                  if not param.matches(args[j].ty) and not param.matches_coerce(args[j].ty):
                    remove.add((i, impl))
                    break

            for i in 1 .. remove.len:
              instance.impls.excl(remove[^i][1])
              instance.scores.delete(remove[^i][0])
          elif instance.impls.len > 0 and instance.impls.toSeq[0].kind == ikCons:
            for arg in self.node.expr_application.args:
              if not type_analysis.type_ctx.eval_sum_in_span(arg.meta.type_sum, arg.meta.span):
                raise newError(errkAmbigousType, arg.meta.span, $arg.meta.type_sum[])

          var impls = newSeq[ref Impl]()
          if instance.scores.len > 0:
            let highest = instance.scores.max
            let impls_seq = instance.impls.toSeq
            for i, score in instance.scores:
              if score == highest:
                impls.add(impls_seq[i])

          case impls.len
          of 0:
            var msg = fmt"{name}("
            for i, arg in args.pairs:
              if i > 0:
                msg.add(", ")
              msg.add($arg.ty[])
            msg.add(")\nthe following impls were found:\n")
            for impl in instance.found_impls:
              msg.add("  ")
              msg.add($impl[])
              msg.add("\n")
            raise newError(errkNoImpl, self.meta.span, msg)
          of 1:
            let impl = impls[0]

            let endidx = if impl.ampvar.isSome: 2 else: 1
            var j = 0
            for param in impl.params[0 ..^ endidx]:
              if param.name.isSome:
                let arg = find_arg(args, param.name.get)
                if arg.isSome:
                  let (i, arg) = arg.get
                  if param.ty.matches_coerce(arg.ty) and not param.ty.matches(arg.ty):
                    let ty = param.ty[].to_hlir(self.node.expr_application.args[i].meta.span)
                    ty.meta.own_type = some(param.ty[].as_qualified_in_span(self.node.expr_application.args[i].meta.span))
                    if self.node.expr_application.args[i].node.kind == hrkDecl:
                      self.node.expr_application.args[i].node.decl.value[] = newNode(newMeta(self.node.expr_application.args[i].node.decl.value.meta.span), Hlir(kind: hrkExprCast, expr_cast: HlirExprCast(value: self.node.expr_application.args[i].node.decl.value.deepCopy, ty: ty)))[]
                      self.node.expr_application.args[i].node.decl.value.meta.type_sum[] = param.ty[]
                    else:
                      self.node.expr_application.args[i][] = newNode(newMeta(self.node.expr_application.args[i].meta.span), Hlir(kind: hrkExprCast, expr_cast: HlirExprCast(value: self.node.expr_application.args[i].deepCopy, ty: ty)))[]
                      self.node.expr_application.args[i].meta.type_sum[] = param.ty[]
                    args[i].ty = param.ty
                  j = i + 1
                  continue

              if j >= args.len:
                break
              let arg = args[j]
              if param.ty.matches_coerce(arg.ty) and not param.ty.matches(arg.ty):
                let ty = param.ty[].to_hlir(self.node.expr_application.args[j].meta.span)
                ty.meta.own_type = some(param.ty[].as_qualified_in_span(self.node.expr_application.args[j].meta.span))
                if self.node.expr_application.args[j].node.kind == hrkDecl:
                  self.node.expr_application.args[j].node.decl.value[] = newNode(newMeta(self.node.expr_application.args[j].node.decl.value.meta.span), Hlir(kind: hrkExprCast, expr_cast: HlirExprCast(value: self.node.expr_application.args[j].node.decl.value.deepCopy, ty: ty)))[]
                  self.node.expr_application.args[j].node.decl.value.meta.type_sum[] = param.ty[]
                else:
                  self.node.expr_application.args[j][] = newNode(newMeta(self.node.expr_application.args[j].meta.span), Hlir(kind: hrkExprCast, expr_cast: HlirExprCast(value: self.node.expr_application.args[j].deepCopy, ty: ty)))[]
                  self.node.expr_application.args[j].meta.type_sum[] = param.ty[]
                args[j].ty = param.ty
              inc j
            if impl.ampvar.isSome:
              let param = impl.ampvar.get
              for j in impl.params.len - endidx + 1 ..< args.len:
                if param.matches_coerce(args[j].ty) and not param.matches(args[j].ty):
                  let ty = param[].to_hlir(self.node.expr_application.args[j].meta.span)
                  ty.meta.own_type = some(param[].as_qualified_in_span(self.node.expr_application.args[j].meta.span))
                  self.node.expr_application.args[j][] = newNode(newMeta(self.node.expr_application.args[j].meta.span), Hlir(kind: hrkExprCast, expr_cast: HlirExprCast(value: self.node.expr_application.args[j].deepCopy, ty: ty)))[]
                  self.node.expr_application.args[j].meta.type_sum[] = param[]
                  args[j].ty = param

            if impl.kind == ikFn and impl.is_macro:
              let mono = impl.monomorphize(impl.ty, args, instance.type_args)
              fn.meta.type_sum[] = mono[0]
              fn.meta.impl = some(impl)
              fn.node.expr_named.name.full = some(impl.name)

              var args = impl.params.mapIt(it.ty[].as_qualified)
              if mono[1].len > 0:
                for arg in args.mitems:
                  arg = arg.deepCopy
                  arg.replace_params(mono[1])
              fn.node.expr_named.name.full = some(impl.name)
              if impl.linkage_name.isSome:
                fn.node.expr_named.name.mangled = some(impl.linkage_name.get)
              else:
                fn.node.expr_named.name.mangle_with_params(args)

              if impl.polymorphic:
                let node = if impl.poly_impl.isSome:
                  type_analysis.get_polymorphic(impl.name, impl.poly_impl.get.ty[].as_qualified).get
                else:
                  type_analysis.get_polymorphic(impl.name, impl.ty[].as_qualified).get
                node.replace_params(mono[1])
                node.node.macrofn.type_params = none[seq[ref Node]]()
                node.node.macrofn.monomorphized = true
                if not type_analysis.is_monomorphized_or_add(impl.name, fn.meta.type_sum[].as_qualified, node):
                  lazy.add_norun(node)
                  # this is hacky, it should be ran elsewhere, but pi couldn't find a way to do that
                  type_gen(index, driver, node)
                  type_args(index, driver, node)
                  type_helper(driver, node)
                  decl(index, driver, node)
                  analyze(index, driver, node)
                let monomorphized = type_analysis.get_monomorphized(impl.name, fn.meta.type_sum[].as_qualified)

                macro_ctx.get_var(["__lang", "macros", "ctx"].toPath.mangled).get.val.struct.fields["call_site"] = self.meta.span.to_macro_value
                macro_ctx.get_var(["__lang", "macros", "ctx"].toPath.mangled).get.val.struct.fields["release"] = driver.release.to_macro_value(self.meta.span)
                var args = newSeq[MacroValue]()
                for i, param in impl.params:
                  let arg = self.node.expr_application.args[i]
                  let is_typed = param.ty[].is_typed_node
                  let value = arg.to_macro_value(is_typed)
                  args.add(value)
                    
                let res = macro_ctx.expand(index, monomorphized, args)
                case res.kind
                of mvkNode:
                  self[] = res.node.node[]
                  return vrkRepeat
                else:
                  raise newIce("unimplemented")
              else:
                let
                  name = fn.node.expr_named.name.mangled.get
                  fn = macro_ctx.get_var(name)
                if fn.isSome:
                  let fn = macro_ctx.load(fn.get)

                  case fn.kind
                  of mvkFunction:
                    macro_ctx.get_var(["__lang", "macros", "ctx"].toPath.mangled).get.val.struct.fields["call_site"] = self.meta.span.to_macro_value
                    macro_ctx.get_var(["__lang", "macros", "ctx"].toPath.mangled).get.val.struct.fields["release"] = driver.release.to_macro_value(self.meta.span)
                    var args = newSeq[MacroValue]()
                    for i, param in impl.params:
                      let arg = self.node.expr_application.args[i]
                      let is_typed = param.ty[].is_typed_node
                      let value = arg.to_macro_value(is_typed)
                      args.add(value)
                        
                    let res = macro_ctx.call(index, fn.fn, args)
                    case res.kind
                    of mvkNode:
                      self[] = res.node.node[]
                      return vrkRepeat
                    else:
                      raise newIce("unimplemented")
                  of mvkFfi:
                    discard
                  else:
                    raise newError(errkInvalidType, self.meta.span, "not a function")

            self.meta.type_sum[] = impl.monomorphize(impl.result_type, args, instance.type_args)[0]
            self.meta.impl = some(impl)
            case impl.kind
            of ikVar:
              overload()
            of ikFn:
              let mono = impl.monomorphize(impl.ty, args, instance.type_args)
              fn.meta.type_sum[] = mono[0]
              fn.meta.impl = some(impl)
              fn.node.expr_named.name.full = some(impl.name)

              let args = args.mapIt(it.ty[].as_qualified)
              if impl.linkage_name.isSome:
                fn.node.expr_named.name.mangled = some(impl.linkage_name.get)
              else:
                fn.node.expr_named.name.mangle_with_params(args)

              if impl.polymorphic:
                let node = if impl.poly_impl.isSome:
                  type_analysis.get_polymorphic(impl.name, impl.poly_impl.get.ty[].as_qualified).get
                else:
                  type_analysis.get_polymorphic(impl.name, impl.ty[].as_qualified).get
                node.replace_params(mono[1])
                node.node.function.type_params = none[seq[ref Node]]()
                node.node.function.monomorphized = true
                if not type_analysis.is_monomorphized_or_add(impl.name, fn.meta.type_sum[].as_qualified, node):
                  lazy.add(node)
            of ikCons:
              let mono = impl.monomorphize(impl.ty, args, instance.type_args)
              fn.meta.type_sum[] = mono[0]
              let variant_name = impl.name
              var variant_type = type_analysis.type_ctx.get(variant_name)
              if mono[1].len > 0:
                variant_type = variant_type.deepCopy
                variant_type.replace_params(mono[1])
              fn.meta.own_type = some(variant_type)
              fn.meta.impl = some(impl)
              fn.node.expr_named.name.full = some(impl.name)

              let args = args.mapIt(it.ty[].as_qualified)
              if instance.type_args.isSome:
                fn.node.expr_named.name.mangle_as_type_with_params(mono[1].mapIt(it[1]))
              else:
                fn.node.expr_named.name.mangle_as_type()

              if impl.polymorphic:
                let name = self.meta.type_sum[].as_qualified[].name
                # those are the four builtin polymorphic types
                # if more are introduced in the future, a `builtin` field should be introduced just for this purpose
                if name != ["__lang", "builtin", "Range"].toPath and
                    name != ["__lang", "builtin", "RangeTo"].toPath and
                    name != ["__lang", "builtin", "RangeFrom"].toPath and
                    name != ["__lang", "macros", "Typed"].toPath:
                  let node = type_analysis.get_polymorphic(impl.ty[].as_qualified[].name, impl.ty[].as_qualified).get
                  node.replace_params(mono[1])
                  case node.node.kind
                  of hrkStruct, hrkUnion:
                    node.node.struct.type_params = none[seq[ref Node]]()
                    node.node.struct.type_args = some(mono[1].mapIt((it.param, it.arg[]).to_hlir(self.meta.span)))
                  of hrkTagged:
                    node.node.tagged.type_params = none[seq[ref Node]]()
                    node.node.tagged.type_args = some(mono[1].mapIt((it.param, it.arg[]).to_hlir(self.meta.span)))
                  else:
                    raise newError(errkMalformedHlir, node.meta.span)
                  if not type_analysis.is_monomorphized_or_add(impl.name, self.meta.type_sum[].as_qualified, node):
                    lazy.add(node)
          else:
            var msg = fmt"{'\n'}{name}("
            for i, arg in args.pairs:
              if i > 0:
                msg.add(", ")
              msg.add($arg.ty[])
            msg.add("):\n")
            for impl in impls:
              msg.add("  ")
              msg.add($impl[])
              msg.add("\n")
            raise newError(errkAmbigousImpl, self.meta.span, msg)
        else:
          overload()
        return vrkRecurse),
      visit_expr_command: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let fn = self.node.expr_command.fn
        case fn.node.kind
        of hrkExprNamed:
          var args = newSeq[Argument]()
          for arg in self.node.expr_command.args:
            case arg.node.kind
            of hrkDecl:
              args.add(Argument(name: arg.node.decl.name.map((name) => name.node.expr_named.name.short.path[^1]), ty: arg.meta.type_sum))
            else:
              args.add(Argument(name: none[string](), ty: arg.meta.type_sum))

          let name = fn.node.expr_named.name.short

          let instance = type_analysis.type_ctx.find_impl(name, none[ref TypeSum](), some(args), false)
          case instance.impls.len
          of 0:
            var msg = fmt"{name}("
            for i, arg in args.pairs:
              if i > 0:
                msg.add(", ")
              msg.add($arg.ty[])
            msg.add(")")
            raise newError(errkNoImpl, self.meta.span, msg)
          of 1:
            let impl = instance.impls.toSeq[0]

            self.meta.type_sum[] = impl.monomorphize(impl.result_type, args, instance.type_args)[0]
            self.meta.impl = some(impl)
            case impl.kind
            of ikVar, ikFn:
              let mono = impl.monomorphize(impl.ty, args, instance.type_args)
              fn.meta.type_sum[] = mono[0]
              fn.meta.impl = some(impl)
              fn.node.expr_named.name.full = some(impl.name)

              var args = impl.params.mapIt(it.ty[].as_qualified)
              if mono[1].len > 0:
                for arg in args.mitems:
                  arg = arg.deepCopy
                  arg.replace_params(mono[1])
              fn.node.expr_named.name.full = some(impl.name)
              if impl.linkage_name.isSome:
                fn.node.expr_named.name.mangled = some(impl.linkage_name.get)
              else:
                fn.node.expr_named.name.mangle_with_params(args)

              if impl.polymorphic:
                let node = if impl.poly_impl.isSome:
                  type_analysis.get_polymorphic(impl.name, impl.poly_impl.get.ty[].as_qualified).get
                else:
                  type_analysis.get_polymorphic(impl.name, impl.ty[].as_qualified).get
                node.replace_params(mono[1])
                node.node.macrofn.type_params = none[seq[ref Node]]()
                node.node.macrofn.monomorphized = true
                if not type_analysis.is_monomorphized_or_add(impl.name, fn.meta.type_sum[].as_qualified, node):
                  lazy.add_norun(node)
                  # this is hacky, it should be ran elsewhere, but pi couldn't find a way to do that
                  type_gen(index, driver, node)
                  type_args(index, driver, node)
                  type_helper(driver, node)
                  decl(index, driver, node)
                  analyze(index, driver, node)
                let monomorphized = type_analysis.get_monomorphized(impl.name, fn.meta.type_sum[].as_qualified)

                macro_ctx.get_var(["__lang", "macros", "ctx"].toPath.mangled).get.val.struct.fields["call_site"] = self.meta.span.to_macro_value
                macro_ctx.get_var(["__lang", "macros", "ctx"].toPath.mangled).get.val.struct.fields["release"] = driver.release.to_macro_value(self.meta.span)
                var args = newSeq[MacroValue]()
                for i, param in impl.params:
                  let arg = self.node.expr_command.args[i]
                  let is_typed = param.ty[].is_typed_node
                  let value = arg.to_macro_value(is_typed)
                  args.add(value)
                    
                let res = macro_ctx.expand(index, monomorphized, args)
                case res.kind
                of mvkNode:
                  self[] = res.node.node[]
                  type_reg(index, driver, self, false)
                  type_gen(index, driver, self, false)
                  type_args(index, driver, self, false)
                  type_helper(driver, self, false)
                  decl(index, driver, self, false)
                  analyze(index, driver, self, false)
                  return vrkContinue
                else:
                  raise newIce("unimplemented")
              else:
                let
                  name = fn.node.expr_named.name.mangled.get
                  fn = macro_ctx.get_var(name)
                if fn.isSome:
                  let fn = macro_ctx.load(fn.get)

                  case fn.kind
                  of mvkFunction:
                    macro_ctx.get_var(["__lang", "macros", "ctx"].toPath.mangled).get.val.struct.fields["call_site"] = self.meta.span.to_macro_value
                    macro_ctx.get_var(["__lang", "macros", "ctx"].toPath.mangled).get.val.struct.fields["release"] = driver.release.to_macro_value(self.meta.span)
                    var args = newSeq[MacroValue]()
                    for i, param in impl.params:
                      let arg = self.node.expr_command.args[i]
                      let is_typed = param.ty[].is_typed_node
                      let value = arg.to_macro_value(is_typed)
                      args.add(value)
                        
                    let res = macro_ctx.call(index, fn.fn, args)
                    case res.kind
                    of mvkNode:
                      self[] = res.node.node[]
                      type_reg(index, driver, self, false)
                      type_gen(index, driver, self, false)
                      type_args(index, driver, self, false)
                      type_helper(driver, self, false)
                      decl(index, driver, self, false)
                      analyze(index, driver, self, false)
                      return vrkContinue
                    else:
                      raise newIce("unimplemented")
                  of mvkFfi:
                    discard
                  else:
                    raise newError(errkInvalidType, self.meta.span, "not a function")
                else:
                  raise newIce("macro function not found")
            else:
              raise newError(errkNoImpl, self.meta.span, "macro not found")
          else:
            var msg = fmt"{'\n'}{name}("
            for i, arg in args.pairs:
              if i > 0:
                msg.add(", ")
              msg.add($arg.ty[])
            msg.add("):\n")
            for impl in instance.impls:
              msg.add("  ")
              msg.add($impl[])
              msg.add("\n")
            raise newError(errkAmbigousImpl, self.meta.span, msg)
        else: raise newIce("unimplemented")
        return vrkRecurse),
      visit_expr_named: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.is_type:
          return vrkRecurse

        if (parent.node.kind != hrkExprApplication or (addr self[]) != (addr parent.node.expr_application.fn[])) and
            (parent.node.kind != hrkExprCommand or (addr self[]) != (addr parent.node.expr_command.fn[])) and
            not self.node.expr_named.quiet and
            (parent.node.kind != hrkDecl or parent.node.decl.name.isNone or (addr self[]) != (addr parent.node.decl.name.get[])):
          let name = self.node.expr_named.name.short

          let instance = type_analysis.type_ctx.find_impl(name, none[ref TypeSum](), none[seq[Argument]](), false)
          case instance.impls.len
          of 0:
            raise newError(errkNoImpl, self.meta.span, $name)
          of 1:
            let impl = instance.impls.toSeq[0]
            self.meta.type_sum[] = impl.ty[]
            self.meta.impl = some(impl)
            self.node.expr_named.name.full = some(impl.name)
            if impl.linkage_name.isSome:
              self.node.expr_named.name.mangled = some(impl.linkage_name.get)
            else:
              self.node.expr_named.name.mangle()
          else:
            var msg = fmt"{'\n'}{name}:{'\n'}"
            for impl in instance.impls:
              msg.add("  ")
              msg.add($impl[])
              msg.add("\n")
            raise newError(errkAmbigousImpl, self.meta.span, msg)
        elif parent.node.kind == hrkDecl and parent.node.decl.name.isSome and (addr self[]) == (addr parent.node.decl.name.get[]):
          self.node.expr_named.name.qualify(type_analysis.module_name)
          self.node.expr_named.name.mangle
        return vrkRecurse),
      visit_expr_field_access: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let field = self.node.expr_field_access.name
        let ty = self.node.expr_field_access.value.meta.type_sum[].deref[].as_qualified_in_span(self.meta.span)
        var idx = none[int]()

        let fields = ty.fields(type_analysis.type_ctx)
        for i, f in fields:
          if f.name.isSome:
            if f.name.get == field:
              idx = some(i)
              break

        if idx.isSome:
          self.meta.type_sum[] = fields[idx.get].ty[]
        else:
          raise newError(errkNotAField, self.meta.span, fmt"`{ty[]}` . `{field}`")
        return vrkRecurse),
      visit_expr_let: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.expr_let.ty.isSome:
          if self.node.expr_let.ty.get.meta.own_type.isNone:
            raise newError(errkInvalidType, self.node.static_global.ty.meta.span, "let expression type is not a type")
          self.meta.type_sum[].add(self.node.expr_let.ty.get.meta.own_type.get)
          if self.node.expr_let.value.isSome:
            self.node.expr_let.value.get.meta.type_sum[] = self.meta.type_sum[]
        elif self.node.expr_let.value.isSome:
          self.meta.type_sum[] = self.node.expr_let.value.get.meta.type_sum[]
          self.node.expr_let.value.get.meta.type_sum = self.meta.type_sum

        if self.node.expr_let.name.isNone:
          self.node.expr_let.name = some(Names(short: self.node.expr_let.name_node.node.expr_named.name.short))

        self.node.expr_let.name.get.qualify(type_analysis.module_name)
        self.node.expr_let.name.get.mangle()

        let impl = newVarImpl(self.node.expr_let.name.get.full.get, none[string](), false, true, false, self.meta.type_sum)
        self.meta.impl = some(impl)
        type_analysis.type_ctx.add_impl(self.node.expr_let.name.get.short, impl)
        type_analysis.type_ctx.add_impl(self.node.expr_let.name.get.full.get, impl, 0)

        return vrkRecurse),
      visit_expr_return: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let result_type = type_analysis.result_type.get
        if result_type[].as_qualified.is_unit:
          if self.node.expr_return.value.isSome:
            if not self.node.expr_return.value.get.meta.type_sum[].as_qualified.is_unit:
              raise newError(errkInvalidType, self.meta.span, fmt"got {self.node.expr_return.value.get.meta.type_sum[]}, but expected unit")
        else:
          if self.node.expr_return.value.isSome:
            if self.node.expr_return.value.get.meta.type_sum.matches(result_type):
              self.node.expr_return.value.get.meta.type_sum[] = type_analysis.type_ctx.restrict_sum_in_span_no_coerce(self.node.expr_return.value.get.meta.type_sum, result_type, self.meta.span)
            elif self.node.expr_return.value.get.meta.type_sum.matches_coerce(result_type):
              let ty = result_type[].to_hlir(self.node.expr_return.value.get.meta.span)
              ty.meta.own_type = some(result_type[].as_qualified_in_span(self.node.expr_return.value.get.meta.span))
              self.node.expr_return.value.get[] = newNode(newMeta(self.node.expr_return.value.get.meta.span), Hlir(kind: hrkExprCast, expr_cast: HlirExprCast(value: self.node.expr_return.value.get.deepCopy, ty: ty)))[]
              self.node.expr_return.value.get.meta.type_sum[] = result_type[]
            else:
              raise newError(errkInvalidType, self.meta.span, fmt"got {self.node.expr_return.value.get.meta.type_sum[]}, but expected {result_type[]}")
          else:
            raise newError(errkInvalidType, self.meta.span, fmt"got unit but expected {result_type[]}")
        return vrkRecurse),
      visit_case: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let switch_type = self.meta.switch_type[].deref
        case self.node.case_of.pat.node.kind
        of hrkPatBind:
          self.node.case_of.pat.meta.type_sum = switch_type
        of hrkPatElse:
          self.node.case_of.pat.meta.type_sum = switch_type
        else:
          if self.node.case_of.pat.meta.type_sum.matches(switch_type):
            self.node.case_of.pat.meta.type_sum[] = type_analysis.type_ctx.restrict_sum_in_span_no_coerce(self.node.case_of.pat.meta.type_sum, switch_type, self.meta.span)
          elif self.node.case_of.pat.meta.type_sum.matches_coerce(switch_type):
            let ty = switch_type[].to_hlir(self.node.case_of.pat.meta.span)
            ty.meta.own_type = some(switch_type[].as_qualified_in_span(self.node.case_of.pat.meta.span))
            self.node.case_of.pat[] = newNode(newMeta(self.node.case_of.pat.meta.span), Hlir(kind: hrkExprCast, expr_cast: HlirExprCast(value: self.node.case_of.pat.deepCopy, ty: ty)))[]
            self.node.case_of.pat.meta.type_sum[] = switch_type[]
          else:
            raise newError(errkInvalidType, self.meta.span, fmt"got {self.node.case_of.pat.meta.type_sum[]}, but expected {switch_type[]}")
        return vrkRecurse),
      visit_pat_bind: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.node.pat_bind.name.get.qualify(type_analysis.module_name)
        self.node.pat_bind.name.get.mangle()

        case parent.node.kind
        of hrkPatDestructure:
          let field = self.node.pat_bind.name.get.short.path[^1]
          var ty = self.meta.switch_type[].as_qualified_in_span(parent.meta.span)

          var ptr_depth = 0
          while ty.inner.kind == tykPointer:
            ty = ty.pointee[].as_qualified_in_span(self.meta.span)
            inc ptr_depth

          case ty.inner.kind
          of tykStruct:
            var found = false
            for i, f in ty.inner.struct.fields.pairs:
              if f.name.isSome:
                if f.name.get == field:
                  found = true
                  if ptr_depth > 0:
                    self.meta.type_sum[] = newType(Type(kind: tykPointer, point: TypePointer(pointee: f.ty))).toQualifiedType.toTypeSum
                  else:
                    self.meta.type_sum[] = f.ty[]
                  break
            if not found:
              raise newError(errkNotAField, self.meta.span)
          of tykTagged:
            let
              variant = parent.node.pat_destructure.ty.node.expr_named.name.full.get
              vt = parent.node.pat_destructure.ty
            var vfound = false
            for v in ty.inner.tagged.variants:
              if v.name == variant:
                vfound = true
                var ffound = false
                for i, f in v.fields.pairs:
                  if f.name.isSome:
                    if f.name.get == field:
                      ffound = true
                      if ptr_depth > 0:
                        self.meta.type_sum[] = newType(Type(kind: tykPointer, point: TypePointer(pointee: f.ty))).toQualifiedType.toTypeSum
                      else:
                        self.meta.type_sum[] = f.ty[]
                      break
                if not ffound:
                  raise newError(errkNotAField, self.meta.span)
                break
            if not vfound:
              raise newError(errkNotAVariant, vt.meta.span)
          else:
            raise newError(errkInvalidType, parent.meta.span)
        of hrkPatBind:
          parent.meta.type_sum = self.meta.type_sum
        of hrkCase:
          self.meta.type_sum[] = type_analysis.match[]
        else:
          raise newError(errkMalformedHlir, self.meta.span)
        if self.node.pat_bind.pat.isNone:
          let impl = newVarImpl(self.node.pat_bind.name.get.full.get, none[string](), false, true, false, self.meta.type_sum)
          self.meta.impl = some(impl)
          type_analysis.type_ctx.add_impl(self.node.pat_bind.name.get.short, impl)
          type_analysis.type_ctx.add_impl(self.node.pat_bind.name.get.full.get, impl, 0)
        return vrkRecurse),
    )
  discard accept(root, pre_visitor, post_visitor)

  if driver.test_mode and not type_analysis.test_main_added:
    type_analysis.test_main_added = true
    let sint = newNode(newMeta(root.meta.span), Hlir(kind: hrkTypeSigned, type_signed: HlirTypeSigned(bits: Bits(kind: bitsCustom, bits: 32))))
    sint.meta.span.len = 0

    let ty = newNode(newMeta(root.meta.span), Hlir(kind: hrkTypeFunction, type_function: HlirTypeFunction(result_type: sint, params: @[])))
    ty.meta.span.len = 0

    let result_var = newNode(newMeta(root.meta.span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: Names(short: "result".toNonFullPath, full: some(["main", "result"].toPath), mangled: some("result")))))
    result_var .meta.span.len = 0

    let return_result = newNode(newMeta(root.meta.span), Hlir(kind: hrkExprReturn, expr_return: HlirExprReturn(value: some(result_var.deepCopy))))
    return_result.meta.span.len = 0

    let zero = newNode(newMeta(root.meta.span), Hlir(kind: hrkExprInt, expr_int: HlirExprInt(value: 0)))
    zero.meta.span.len = 0

    let result_0 = newNode(newMeta(root.meta.span), Hlir(kind: hrkExprBinary, expr_binary: HlirExprBinary(op: hbkAssign, lhs: result_var.deepCopy, rhs: zero)))
    result_0.meta.span.len = 0

    let one = newNode(newMeta(root.meta.span), Hlir(kind: hrkExprInt, expr_int: HlirExprInt(value: 1)))
    one.meta.span.len = 0

    let result_1 = newNode(newMeta(root.meta.span), Hlir(kind: hrkExprBinary, expr_binary: HlirExprBinary(op: hbkAssign, lhs: result_var.deepCopy, rhs: one)))
    result_1.meta.span.len = 0

    var test_elems = newSeq[ref Node]()

    test_elems.add(result_0)

    for names in type_analysis.tests:
      var names = names
      names.short = names.full.get
      let fn = newNode(newMeta(root.meta.span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: names)))
      fn.meta.span.len = 0

      let app = newNode(newMeta(root.meta.span), Hlir(kind: hrkExprApplication, expr_application: HlirExprApplication(fn: fn, args: @[])))
      app.meta.span.len = 0

      let cond = newNode(newMeta(root.meta.span), Hlir(kind: hrkExprUnary, expr_unary: HlirExprUnary(op: hukNot, value: app)))
      cond.meta.span.len = 0

      let fn_printf = newNode(newMeta(root.meta.span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: Names(short: "printf".toNonFullPath, full: some("printf".toPath), mangled: some("printf")))))
      fn_printf.meta.span.len = 0

      let str_test_failure = newNode(newMeta(root.meta.span), Hlir(kind: hrkExprString, expr_string: HlirExprString(value: fmt"[ERR] {names.full.get}{'\n'}")))
      str_test_failure.meta.span.len = 0

      let str_test_failure_ptr = newNode(newMeta(root.meta.span), Hlir(kind: hrkExprFieldAccess, expr_field_access: HlirExprFieldAccess(value: str_test_failure, name: "ptr")))
      str_test_failure_ptr.meta.span.len = 0

      let print_err = newNode(newMeta(root.meta.span), Hlir(kind: hrkExprApplication, expr_application: HlirExprApplication(fn: fn_printf, args: @[str_test_failure_ptr])))
      print_err.meta.span.len = 0

      let then = result_1.deepCopy
      then.meta.span.len = 0

      let then_body = newNode(newMeta(root.meta.span), Hlir(kind: hrkExprCompound, expr_compound: HlirExprCompound(elems: @[print_err], last: some(then))))
      then_body.meta.span.len = 0

      let str_test_success = newNode(newMeta(root.meta.span), Hlir(kind: hrkExprString, expr_string: HlirExprString(value: fmt"[OK]  {names.full.get}{'\n'}")))
      str_test_success.meta.span.len = 0

      let str_test_success_ptr = newNode(newMeta(root.meta.span), Hlir(kind: hrkExprFieldAccess, expr_field_access: HlirExprFieldAccess(value: str_test_success, name: "ptr")))
      str_test_success_ptr.meta.span.len = 0

      let print_ok = newNode(newMeta(root.meta.span), Hlir(kind: hrkExprApplication, expr_application: HlirExprApplication(fn: fn_printf.deepCopy, args: @[str_test_success_ptr])))
      print_ok.meta.span.len = 0

      let el_body = newNode(newMeta(root.meta.span), Hlir(kind: hrkExprCompound, expr_compound: HlirExprCompound(elems: @[print_ok], last: none[ref Node]())))
      el_body.meta.span.len = 0

      let iff = newNode(newMeta(root.meta.span), Hlir(kind: hrkStmtIf, stmt_if: HlirStmtIf(cond: cond, body: then_body, el: some(el_body))))
      iff.meta.span.len = 0

      test_elems.add(iff)

    let body = newNode(newMeta(root.meta.span), Hlir(kind: hrkExprCompound, expr_compound: HlirExprCompound(elems: test_elems, last: some(return_result))))
    body.meta.span.len = 0

    let name = Names(short: "main".toNonFullPath, full: some("main".toPath), mangled: some("main"))
    let name_node = newNode(newMeta(root.meta.span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: name, quiet: true)))
    name_node.meta.span.len = 0
    let test_main = newNode(newMeta(root.meta.span), Hlir(kind: hrkFunction, function: HlirFunction(name_node: some(name_node), name: some(name), ty: ty, body: some(body))))
    test_main.meta.span.len = 0
    test_main.meta.attributes = some(@[Attribute(kind: akNoMangle), Attribute(kind: akTestMain)])

    let use_stdio = newNode(newMeta(root.meta.span), Hlir(kind: hrkUseExtern, use_extern: HlirUseExtern(lang: "C", file: "stdio.h")))
    use_stdio.meta.span.len = 0

    driver.lazy.add(test_main)
    driver.lazy.add(use_stdio)

proc analyze*(index: ref Index, driver: ref Driver, node: ref Node, root: ref Node, scope: bool = true) =
  let type_analysis = driver.pass_controllers[tsTypeDecl].TaPassController
  if scope and root.meta.impl_scope.isSome:
    type_analysis.type_ctx.enter_this_bounded(root.meta.impl_scope.get)
  elif scope:
    type_analysis.type_ctx.enter_bounded
  analyze(index, driver, node, scope)
  if scope:
    root.meta.impl_scope = some(type_analysis.type_ctx.leave_this_bounded)

proc eval*(driver: ref Driver, root: ref Node, scope: bool = true) =
  let type_analysis = driver.pass_controllers[tsTypeDecl].TaPassController
  let
    pre_visitor = HlirVisitor(
      visit_function: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.function.type_params.isSome:
          return vrkContinue
        return vrkRecurse),
      visit_macro: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        return vrkContinue),
      visit_expr_application: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.meta.impl.isSome and
            (self.meta.impl.get.name == ["__lang", "macros", "quote"].toPath or self.meta.impl.get.name  == ["__lang", "macros", "quasiquote"].toPath):
          return vrkNoRecurse
        return vrkRecurse),
      visit_expr_command: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.meta.impl.isSome and
            (self.meta.impl.get.name == ["__lang", "macros", "quote"].toPath or self.meta.impl.get.name  == ["__lang", "macros", "quasiquote"].toPath):
          return vrkNoRecurse
        return vrkRecurse),
    )
    post_visitor = HlirVisitor(
      visit_expr_named: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.meta.own_type.isNone and
            (parent.node.kind != hrkExprApplication or (addr self[]) != (addr parent.node.expr_application.fn[])) and
            (parent.node.kind != hrkExprCommand or (addr self[]) != (addr parent.node.expr_command.fn[])) and
            not self.node.expr_named.quiet and
            (parent.node.kind != hrkDecl or parent.node.decl.name.isNone or (addr self[]) != (addr parent.node.decl.name.get[])):
          discard type_analysis.type_ctx.eval_sum_in_span(self.meta.type_sum, self.meta.span)
        return vrkRecurse),
      visit_expr_int: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        discard type_analysis.type_ctx.eval_sum_in_span(self.meta.type_sum, self.meta.span)
        return vrkRecurse),
      visit_expr_float: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        discard type_analysis.type_ctx.eval_sum_in_span(self.meta.type_sum, self.meta.span)
        return vrkRecurse),
      visit_expr_array: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        discard type_analysis.type_ctx.eval_sum_in_span(self.meta.type_sum, self.meta.span)
        return vrkRecurse),
    )
  discard accept(root, pre_visitor, post_visitor)

proc eval*(driver: ref Driver, node: ref Node, root: ref Node, scope: bool = true) =
  let type_analysis = driver.pass_controllers[tsTypeDecl].TaPassController
  if scope and root.meta.impl_scope.isSome:
    type_analysis.type_ctx.enter_this_bounded(root.meta.impl_scope.get)
  elif scope:
    type_analysis.type_ctx.enter_bounded
  eval(driver, node, scope)
  if scope:
    root.meta.impl_scope = some(type_analysis.type_ctx.leave_this_bounded)

proc replace_params(node: ref Node, type_args: seq[tuple[param: string, arg: ref QualifiedType]]) =
  let
    pre_visitor = HlirVisitor()
    post_visitor = HlirVisitor(
      visit_expr_named: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if parent.node.kind != hrkDecl or parent.node.decl.name.isNone or (addr self[]) != (addr parent.node.decl.name.get[]):
          for arg in type_args:
            if self.node.expr_named.name.short.path[^1] == arg.param:
              self[] = arg.arg[].to_hlir(self.meta.span)[]
              if self.node.kind == hrkExprNamed and self.node.expr_named.name.short.path[^1] == arg.param:
                return vrkContinue
              return vrkRepeat
        return vrkRecurse),
    )
  discard accept(node, pre_visitor, post_visitor)

proc quasiquote(index: ref Index, driver: ref Driver, node: ref Node) =
  let
    pre_visitor = HlirVisitor()
    post_visitor = HlirVisitor(
      visit_expr_interp: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        analyze(index, driver, self.node.expr_interp.value, false)
        return vrkNoRecurse
      )
    )
  discard accept(node, pre_visitor, post_visitor)

proc mark_type(node: ref Node) =
  let
    pre_visitor = HlirVisitor(
      visit_decl: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.decl.name.isSome:
          inc self.node.decl.name.get.meta.dont_visit
        return vrkRecurse
      ),
    )
    post_visitor = HlirVisitor(
      visit_expr_application: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.is_type = true
        return vrkRecurse
      ),
      visit_expr_named: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.is_type = true
        return vrkRecurse
      ),
      visit_expr_parenth: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.is_type = true
        return vrkRecurse
      ),
    )
  discard accept(node, pre_visitor, post_visitor)

proc replace_capture(node: ref Node, capture: ref seq[tuple[name: string, by_ref: bool]]) =
  let
    pre_visitor = HlirVisitor(
      visit_function: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        for param in self.node.function.ty.node.type_function.params:
          if param.node.decl.name.isSome:
            var idx = none[int]()

            let name = param.node.decl.name.get.node.expr_named.name

            for i, capture in capture[]:
              if name.short.path.len == 1 and name.short.path[0] == capture.name:
                idx = some(i)
                break

            if idx.isSome:
              capture[].delete(idx.get)
        return vrkRecurse),
    )
    post_visitor = HlirVisitor(
      visit_expr_let: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.expr_let.name.isSome:
          var idx = none[int]()

          for i, capture in capture[]:
            if self.node.expr_let.name.get.short.path.len == 1 and self.node.expr_let.name.get.short.path[0] == capture.name:
              idx = some(i)
              break

          if idx.isSome:
            capture[].delete(idx.get)
        return vrkRecurse),
      visit_expr_named: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if (parent.node.kind != hrkDecl or parent.node.decl.name.isNone or (addr self[]) != (addr parent.node.decl.name.get[])) and
            not self.node.expr_named.quiet:
          for capture in capture[]:
            if self.node.expr_named.name.short.path[^1] == capture.name:
              var node: ref Node
              let recur = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: Names(short: "recur".toNonFullPath))))
              let field = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprFieldAccess, expr_field_access: HlirExprFieldAccess(value: recur, name: capture.name)))
              if capture.by_ref:
                node = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprUnary, expr_unary: HlirExprUnary(op: hukDeref, value: field)))
              else:
                node = field
              self[] = node[]
              return vrkContinue
        return vrkRecurse),
    )
  discard accept(node, pre_visitor, post_visitor)
