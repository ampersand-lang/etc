import options
import sequtils
import sugar
import ../ice
import ../error
import ../path
import ../names
import ../index
import ../driver/driver_typedefs
import ../hlir/hlir
import ../hlir/visitor

proc analyze*(index: ref Index, driver: ref Driver, root: ref Node) =
  let
    pre_visitor = HlirVisitor(
      visit_define: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.define.name_node.node.kind == hrkExprNamed:
          self.node.define.name_node.node.expr_named.quiet = true
        if self.node.define.ty.isNone and self.node.define.value.node.kind == hrkExprApplication:
          if self.node.define.value.node.expr_application.fn.node.kind == hrkExprTrait:
            let trait = newNode(self.meta, Hlir(kind: hrkTrait, trait: HlirTrait(
              name_node: self.node.define.name_node,
              name: self.node.define.name,
              fns: newSeq[ref Node](),
            )))
            for field in self.node.define.value.node.expr_application.args:
              case field.node.kind
              of hrkDecl:
                trait.node.trait.fns.add(field)
              else:
                raise newError(errkMalformedHlir, field.meta.span, "expected a declaration")
            self[] = trait[]
            return vrkRepeat
          elif self.node.define.value.node.expr_application.fn.node.kind == hrkExprStruct:
            let struct = newNode(self.meta, Hlir(kind: hrkStruct, struct: HlirStruct(
              name_node: self.node.define.name_node,
              name: self.node.define.name,
              fields: newSeq[ref Node](),
            )))
            for field in self.node.define.value.node.expr_application.args:
              case field.node.kind
              of hrkDecl:
                struct.node.struct.fields.add(field)
              else:
                raise newError(errkMalformedHlir, field.meta.span, "expected a declaration")
            self[] = struct[]
            return vrkRepeat
          elif self.node.define.value.node.expr_application.fn.node.kind == hrkExprUnion:
            let struct = newNode(self.meta, Hlir(kind: hrkUnion, struct: HlirStruct(
              name_node: self.node.define.name_node,
              name: self.node.define.name,
              fields: newSeq[ref Node](),
            )))
            for field in self.node.define.value.node.expr_application.args:
              case field.node.kind
              of hrkDecl:
                struct.node.struct.fields.add(field)
              else:
                raise newError(errkMalformedHlir, field.meta.span, "expected a declaration")
            self[] = struct[]
            return vrkRepeat
          elif self.node.define.value.node.expr_application.fn.node.kind == hrkExprEnum:
            let enumeration = newNode(self.meta, Hlir(kind: hrkEnum, enumeration: HlirEnum(
              name_node: self.node.define.name_node,
              name: self.node.define.name,
              variants: newSeq[ref Node](),
            )))
            for variant in self.node.define.value.node.expr_application.args:
              case variant.node.kind
              of hrkDecl:
                let name_node = variant.node.decl.name.get
                let name = if name_node.node.kind == hrkExprNamed:
                  name_node.node.expr_named.quiet = true
                  some(name_node.node.expr_named.name)
                else:
                  none[Names]()
                let variant = newNode(variant.meta, Hlir(kind: hrkEnumVariant, enum_variant: HlirEnumVariant(name_node: name_node, name: name, value: some(variant.node.decl.value))))
                enumeration.node.enumeration.variants.add(variant)
              else:
                let name_node = variant
                let name = if name_node.node.kind == hrkExprNamed:
                  name_node.node.expr_named.quiet = true
                  some(name_node.node.expr_named.name)
                else:
                  none[Names]()
                let variant = newNode(variant.meta, Hlir(kind: hrkEnumVariant, enum_variant: HlirEnumVariant(name_node: name_node, name: name, value: none[ref Node]())))
                enumeration.node.enumeration.variants.add(variant)
            self[] = enumeration[]
            return vrkRepeat
          elif self.node.define.value.node.expr_application.fn.node.kind == hrkExprTagged:
            let tagged = newNode(self.meta, Hlir(kind: hrkTagged, tagged: HlirTagged(
              name_node: self.node.define.name_node,
              name: self.node.define.name,
              fields: newSeq[ref Node](),
              variants: newSeq[ref Node](),
            )))
            for variant in self.node.define.value.node.expr_application.args:
              case variant.node.kind
              of hrkDecl:
                tagged.node.tagged.fields.add(variant)
              of hrkExprApplication:
                let name = if variant.node.expr_application.fn.node.kind == hrkExprNamed:
                  variant.node.expr_application.fn.node.expr_named.quiet = true
                  some(variant.node.expr_application.fn.node.expr_named.name)
                else:
                  none[Names]()
                let vrnt = newNode(self.meta, Hlir(kind: hrkTaggedVariant, tagged_variant: HlirTaggedVariant(
                  name_node: variant.node.expr_application.fn,
                  name: name,
                  fields: newSeq[ref Node](),
                )))
                for field in variant.node.expr_application.args:
                  case field.node.kind
                  of hrkDecl:
                    vrnt.node.tagged_variant.fields.add(field)
                  else:
                    raise newError(errkMalformedHlir, field.meta.span, "expected a declaration")
                tagged.node.tagged.variants.add(vrnt)
              else:
                raise newError(errkMalformedHlir, variant.meta.span, "expected an application")
            self[] = tagged[]
            return vrkRepeat
        return vrkRecurse),
      visit_function: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.function.body.isSome:
          if self.node.function.body.get.node.kind == hrkExprReturn:
            self.node.function.body.get[] = newNode(
              newMeta(self.node.function.body.get.meta.span),
              Hlir(
                kind: hrkExprCompound,
                expr_compound: HlirExprCompound(
                  elems: newSeq[ref Node](),
                  last: some(self.node.function.body.get.deepCopy),
                )
              )
            )[]
          elif self.node.function.body.get.node.kind != hrkExprCompound:
            self.node.function.body.get[] = newNode(
              newMeta(self.node.function.body.get.meta.span),
              Hlir(
                kind: hrkExprCompound,
                expr_compound: HlirExprCompound(
                  elems: newSeq[ref Node](),
                  last: some(newNode(
                    newMeta(self.node.function.body.get.meta.span),
                    Hlir(
                      kind: hrkExprReturn,
                      expr_return: HlirExprReturn(
                        value: some(self.node.function.body.get.deepCopy),
                      )
                    )
                  ))
                )
              )
            )[]
          else:
            if self.node.function.body.get.node.expr_compound.last.isSome and
              self.node.function.body.get.node.expr_compound.last.get.node.kind != hrkExprReturn:
              self.node.function.body.get.node.expr_compound.last.get[] = newNode(
                newMeta(self.node.function.body.get.meta.span),
                Hlir(
                  kind: hrkExprReturn,
                  expr_return: HlirExprReturn(
                    value: some(self.node.function.body.get.node.expr_compound.last.get.deepCopy),
                  )
                )
              )[]
        return vrkRecurse),
      visit_expr_named: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if parent.is_pattern and not self.node.expr_named.quiet:
          self.node.expr_named.quiet = true
          self[] = newNode(
            self.meta,
            Hlir(
              kind: hrkPatBind,
              pat_bind: HlirPatBind(
                name_node: self.deepCopy,
                name: some(self.node.expr_named.name),
                pat: none[ref Node](),
              ),
            ),
          )[]
          return vrkRepeat
        return vrkRecurse),
      visit_expr_unary: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.expr_unary.op == hukColon and parent.is_pattern_or_match:
          case parent.node.kind
          of hrkCase:
            if (addr self[]) == (addr parent.node.case_of.body[]):
              return vrkRecurse
          else: discard
          self[] = newNode(
            self.meta,
            Hlir(
              kind: hrkPatBind,
              pat_bind: HlirPatBind(
                name_node: self.node.expr_unary.value.deepCopy,
                name: some(self.node.expr_unary.value.node.expr_named.name),
                pat: none[ref Node](),
              ),
            ),
          )[]
          return vrkRepeat
        return vrkRecurse),
      visit_expr_application: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if parent.is_pattern_or_match:
          case parent.node.kind
          of hrkCase:
            if (addr self[]) == (addr parent.node.case_of.body[]):
              return vrkRecurse
          else: discard
          self[] = newNode(
            self.meta,
            Hlir(
              kind: hrkPatDestructure,
              pat_destructure: HlirPatDestructure(
                ty: self.node.expr_application.fn,
                pats: self.node.expr_application.args,
              ),
            ),
          )[]
          return vrkRepeat
        return vrkRecurse),
      visit_decl: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if parent.is_pattern_or_match:
          case parent.node.kind
          of hrkCase:
            if (addr self[]) == (addr parent.node.case_of.body[]):
              return vrkRecurse
          else: discard
          self[] = newNode(
            self.meta,
            Hlir(
              kind: hrkPatBind,
              pat_bind: HlirPatBind(
                name_node: self.node.decl.name.get.deepCopy,
                name: some(self.node.decl.name.get.node.expr_named.name),
                pat: some(self.node.decl.value.deepCopy),
              ),
            ),
          )[]
          return vrkRepeat
        return vrkRecurse),
      visit_case: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.case_of.body.node.kind != hrkExprCompound:
          self.node.case_of.body[] = newNode(
            newMeta(self.node.case_of.body.meta.span),
            Hlir(
              kind: hrkExprCompound,
              expr_compound: HlirExprCompound(
                elems: newSeq[ref Node](),
                last: some(self.node.case_of.body.deepCopy),
              )
            )
          )[]
        return vrkRecurse),
    )
    post_visitor = HlirVisitor(
      visit_enum: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        var last = none[int64]()
        for variant in self.node.enumeration.variants.mitems:
          if variant.node.enum_variant.value.isNone:
            variant.node.enum_variant.value = some(newNode(
              newMeta(variant.meta.span),
              Hlir(
                kind: hrkExprInt,
                expr_int: HlirExprInt(value: last.map((value) => value + 1).get(0)),
              ),
            ))
          last = some(variant.node.enum_variant.value.get.node.expr_int.value)
        return vrkRecurse),
      visit_enum_variant: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let
          variant_name = self.node.enum_variant.name_node.node.expr_named.name.short
          enum_name = parent.node.enumeration.name_node.node.expr_named.name.short
        self.node.enum_variant.name_node.node.expr_named.name.short = enum_name & variant_name
        self.node.enum_variant.name.get.short = enum_name & variant_name
        return vrkRecurse),
      visit_tagged_variant: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let
          variant_name = self.node.tagged_variant.name_node.node.expr_named.name.short
          tagged_name = parent.node.tagged.name_node.node.expr_named.name.short
        if not variant_name.starts_with(tagged_name):
          self.node.tagged_variant.name_node.node.expr_named.name.short = tagged_name & variant_name
          self.node.tagged_variant.name.get.short = tagged_name & variant_name
        return vrkRecurse),
      visit_stmt_foreach: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        # transforms a foreach loop into a __iter__! command
        # foreach it in iter body
        # =>
        # __iter__! it, iter, body
        let meta = self.meta
        let fn = newNode(newMeta(self.meta.span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: Names(short: "__iter__".toNonFullPath))))
        let name = self.node.stmt_foreach.name_node
        let iter = self.node.stmt_foreach.iter
        let body = self.node.stmt_foreach.body
        self[] = newNode(
          meta,
          Hlir(
            kind: hrkExprCommand,
            expr_command: HlirExprCommand(fn: fn, args: @[name, iter, body]),
          )
        )[]
        return vrkRepeat),
    )
  case root.node.kind
  of hrkRoot:
    var
      span = root.meta.span
      path = ["__lang", "builtin"].toNonFullPath
    path.glob = true
    span.len = 0
    root.node.root.root.node.sequence.children.insert(newNode(newMeta(span), Hlir(kind: hrkWith, with_single: HlirWith(path: path))))
  else:
    discard
  discard accept(root, pre_visitor, post_visitor)

proc analyze*(index: ref Index, driver: ref Driver, node: ref Node, root: ref Node) =
  analyze(index, driver, node)
