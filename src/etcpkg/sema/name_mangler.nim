import options
import sequtils
import sugar
import ../ice
import ../names
import ../index
import ../driver/driver_typedefs
import ../types/types
import ../hlir/hlir
import ../hlir/visitor

let
  pre_visitor = HlirVisitor()
  post_visitor = HlirVisitor()

proc analyze*(index: ref Index, driver: ref Driver, root: ref Node) =
  discard accept(root, pre_visitor, post_visitor)
