import options
import sequtils
import sugar
import ../ice
import ../error
import ../names
import ../index
import ../driver/driver_typedefs
import ../types/types
import ../hlir/hlir
import ../hlir/visitor

let
  generic_post = proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
    self.meta.returns = fbNo
    self.meta.breaks = fbNo
    self.meta.continues = fbNo
    return vrkRecurse
  pre_visitor = HlirVisitor(
    visit_macro: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
      return vrkContinue),
    visit_function: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
      if self.node.function.type_params.isSome:
        return vrkContinue
      if self.meta.attributes.isSome and self.meta.attributes.get.any((attr: Attribute) => attr.kind == akComptime):
        return vrkContinue
      return vrkRecurse),
  )
  post_visitor = HlirVisitor(
    visit_function: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
      if self.node.function.body.isSome:
        if not self.meta.type_sum[].as_qualified.result_type[].as_qualified.is_unit:
          let body = self.node.function.body.get
          if body.node.kind == hrkExprCompound and body.node.expr_compound.last.isNone:
            if body.meta.returns == fbNo:
              raise newError(errkNoReturn, self.meta.span)
            elif body.meta.returns == fbMaybe:
              raise newError(errkMaybeReturn, self.meta.span)
      return vrkRecurse),
    visit_expr_compound: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
      if self.node.expr_compound.elems.len == 0:
        self.meta.returns = self.node.expr_compound.last.map((node) => node.meta.returns).get(fbNo)
      else:
        self.meta.returns = self.node.expr_compound.last.map((node) => node.meta.returns).get(fbUndefined) or
                            self.node.expr_compound.elems.mapIt(it.meta.returns).foldl(a or b)
      if self.node.expr_compound.elems.len == 0:
        self.meta.breaks = self.node.expr_compound.last.map((node) => node.meta.breaks).get(fbNo)
      else:
        self.meta.breaks = self.node.expr_compound.last.map((node) => node.meta.breaks).get(fbUndefined) or
                           self.node.expr_compound.elems.mapIt(it.meta.breaks).foldl(a or b)
      if self.node.expr_compound.elems.len == 0:
        self.meta.continues = self.node.expr_compound.last.map((node) => node.meta.continues).get(fbNo)
      else:
        self.meta.continues = self.node.expr_compound.last.map((node) => node.meta.continues).get(fbUndefined) or
                              self.node.expr_compound.elems.mapIt(it.meta.continues).foldl(a or b)

      var trunc = none[int]()
      for i, child in self.node.expr_compound.elems.pairs:
        if child.meta.returns == fbYes or child.meta.breaks == fbYes or child.meta.continues == fbYes:
          trunc = some(i + 1)
          break

      if trunc.isSome:
        self.node.expr_compound.elems.setLen(trunc.get)
        self.node.expr_compound.last = none[ref Node]()

      return vrkRecurse),
    visit_stmt_if: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
      if self.node.stmt_if.elifs.len == 0:
        self.meta.returns = self.node.stmt_if.body.meta.returns and self.node.stmt_if.el.map((el) => el.meta.returns).get(fbNo)
      else:
        self.meta.returns = self.node.stmt_if.body.meta.returns and self.node.stmt_if.elifs.mapIt(it.body.meta.returns).foldl(a and b) and self.node.stmt_if.el.map((el) => el.meta.returns).get(fbNo)
      if self.node.stmt_if.elifs.len == 0:
        self.meta.breaks = self.node.stmt_if.body.meta.breaks and self.node.stmt_if.el.map((el) => el.meta.breaks).get(fbNo)
      else:
        self.meta.breaks = self.node.stmt_if.body.meta.breaks and self.node.stmt_if.elifs.mapIt(it.body.meta.breaks).foldl(a and b) and self.node.stmt_if.el.map((el) => el.meta.breaks).get(fbNo)
      if self.node.stmt_if.elifs.len == 0:
        self.meta.continues = self.node.stmt_if.body.meta.continues and self.node.stmt_if.el.map((el) => el.meta.continues).get(fbNo)
      else:
        self.meta.continues = self.node.stmt_if.body.meta.continues and self.node.stmt_if.elifs.mapIt(it.body.meta.continues).foldl(a and b) and self.node.stmt_if.el.map((el) => el.meta.continues).get(fbNo)
      return vrkRecurse),
    visit_stmt_while: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
      self.meta.returns = self.node.stmt_while.body.meta.returns and fbNo
      self.meta.breaks = fbNo
      self.meta.continues = fbNo
      return vrkRecurse),
    visit_stmt_dowhile: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
      self.meta.returns = self.node.stmt_dowhile.body.meta.returns
      self.meta.breaks = fbNo
      self.meta.continues = fbNo
      return vrkRecurse),
    visit_stmt_for: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
      self.meta.returns = self.node.stmt_for.body.meta.returns and fbNo
      self.meta.breaks = fbNo
      self.meta.continues = fbNo
      return vrkRecurse),
    visit_stmt_match: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
      if self.node.stmt_match.cases.len == 0:
        self.meta.returns = fbNo
      else:
        self.meta.returns = self.node.stmt_match.cases.mapIt(it.node.case_of.body.meta.returns).foldl(a and b)
      if self.node.stmt_match.cases.len == 0:
        self.meta.breaks = fbNo
      else:
        self.meta.breaks = self.node.stmt_match.cases.mapIt(it.node.case_of.body.meta.breaks).foldl(a and b)
      if self.node.stmt_match.cases.len == 0:
        self.meta.continues = fbNo
      else:
        self.meta.continues = self.node.stmt_match.cases.mapIt(it.node.case_of.body.meta.continues).foldl(a and b)
      let has_else = self.node.stmt_match.cases.anyIt(it.node.case_of.pat.is_else_pattern)
      if not has_else:
        self.meta.returns = self.meta.returns and fbNo
        self.meta.breaks = self.meta.breaks and fbNo
        self.meta.continues = self.meta.continues and fbNo
      return vrkRecurse),
    visit_expr_fail: some(generic_post),
    visit_expr_return: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
      self.meta.returns = fbYes
      self.meta.breaks = fbNo
      self.meta.continues = fbNo
      return vrkRecurse),
    visit_expr_continue: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
      self.meta.returns = fbNo
      self.meta.breaks = fbYes
      self.meta.continues = fbNo
      return vrkRecurse),
    visit_expr_break: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
      self.meta.returns = fbNo
      self.meta.breaks = fbNo
      self.meta.continues = fbYes
      return vrkRecurse),
    visit_expr_let: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
      self.meta.returns = self.node.expr_let.value.map((value) => value.meta.returns).get(fbNo)
      self.meta.breaks = self.node.expr_let.value.map((value) => value.meta.breaks).get(fbNo)
      self.meta.continues = self.node.expr_let.value.map((value) => value.meta.continues).get(fbNo)
      return vrkRecurse),
    visit_expr_int: some(generic_post),
    visit_expr_float: some(generic_post),
    visit_expr_bool: some(generic_post),
    visit_expr_unit: some(generic_post),
    visit_expr_string: some(generic_post),
    visit_expr_named: some(generic_post),
    visit_expr_array: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
      if self.node.expr_array.elems.len == 0:
        self.meta.returns = fbNo
      else:
        self.meta.returns = self.node.expr_array.elems.mapIt(it.meta.returns).foldl(a or b)
      if self.node.expr_array.elems.len == 0:
        self.meta.breaks = fbNo
      else:
        self.meta.breaks = self.node.expr_array.elems.mapIt(it.meta.breaks).foldl(a or b)
      if self.node.expr_array.elems.len == 0:
        self.meta.continues = fbNo
      else:
        self.meta.continues = self.node.expr_array.elems.mapIt(it.meta.continues).foldl(a or b)
      return vrkRecurse),
    visit_expr_application: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
      if self.node.expr_application.args.len == 0:
        self.meta.returns = self.node.expr_application.fn.meta.returns
      else:
        self.meta.returns = self.node.expr_application.fn.meta.returns or self.node.expr_application.args.mapIt(it.meta.returns).foldl(a or b)
      if self.node.expr_application.args.len == 0:
        self.meta.breaks = self.node.expr_application.fn.meta.breaks
      else:
        self.meta.breaks = self.node.expr_application.fn.meta.breaks or self.node.expr_application.args.mapIt(it.meta.breaks).foldl(a or b)
      if self.node.expr_application.args.len == 0:
        self.meta.continues = self.node.expr_application.fn.meta.continues
      else:
        self.meta.continues = self.node.expr_application.fn.meta.continues or self.node.expr_application.args.mapIt(it.meta.continues).foldl(a or b)
      return vrkRecurse),
    visit_expr_field_access: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
      self.meta.returns = self.node.expr_field_access.value.meta.returns
      self.meta.breaks = self.node.expr_field_access.value.meta.breaks
      self.meta.continues = self.node.expr_field_access.value.meta.continues
      return vrkRecurse),
    visit_expr_cast: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
      self.meta.returns = self.node.expr_cast.value.meta.returns
      self.meta.breaks = self.node.expr_cast.value.meta.breaks
      self.meta.continues = self.node.expr_cast.value.meta.continues
      return vrkRecurse),
    visit_expr_sizeof: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
      self.meta.returns = self.node.expr_sizeof.value.meta.returns
      self.meta.breaks = self.node.expr_sizeof.value.meta.breaks
      self.meta.continues = self.node.expr_sizeof.value.meta.continues
      return vrkRecurse),
    visit_expr_alignof: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
      self.meta.returns = self.node.expr_alignof.value.meta.returns
      self.meta.breaks = self.node.expr_alignof.value.meta.breaks
      self.meta.continues = self.node.expr_alignof.value.meta.continues
      return vrkRecurse),
    visit_expr_unary: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
      self.meta.returns = self.node.expr_unary.value.meta.returns
      self.meta.breaks = self.node.expr_unary.value.meta.breaks
      self.meta.continues = self.node.expr_unary.value.meta.continues
      return vrkRecurse),
    visit_expr_binary: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
      self.meta.returns = self.node.expr_binary.lhs.meta.returns or self.node.expr_binary.rhs.meta.returns
      self.meta.breaks = self.node.expr_binary.lhs.meta.breaks or self.node.expr_binary.rhs.meta.breaks
      self.meta.continues = self.node.expr_binary.lhs.meta.continues or self.node.expr_binary.rhs.meta.continues
      return vrkRecurse),
    visit_expr_ternary: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
      self.meta.returns = self.node.expr_ternary.value.meta.returns
      self.meta.breaks = self.node.expr_ternary.value.meta.breaks
      self.meta.continues = self.node.expr_ternary.value.meta.continues
      var
        returns = fbUndefined
        breaks = fbUndefined
        continues = fbUndefined
      if self.node.expr_ternary.lhs.isSome:
        returns = returns and self.node.expr_ternary.lhs.get.meta.returns
        breaks = breaks and self.node.expr_ternary.lhs.get.meta.breaks
        continues = continues and self.node.expr_ternary.lhs.get.meta.continues
      if self.node.expr_ternary.rhs.isSome:
        returns = returns and self.node.expr_ternary.rhs.get.meta.returns
        breaks = breaks and self.node.expr_ternary.rhs.get.meta.breaks
        continues = continues and self.node.expr_ternary.rhs.get.meta.continues
      self.meta.returns = self.meta.returns or returns
      self.meta.breaks = self.meta.breaks or breaks
      self.meta.continues = self.meta.continues or continues
      return vrkRecurse),
  )

proc analyze*(index: ref Index, driver: ref Driver, root: ref Node) =
  discard accept(root, pre_visitor, post_visitor)
