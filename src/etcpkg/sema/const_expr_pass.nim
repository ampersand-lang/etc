import options
import sequtils
import sugar
import ../path
import ../names
import ../index
import ../driver/driver_typedefs
import ../const_expr/const_expr
import ../types/types
import ../hlir/hlir
import ../hlir/visitor

proc analyze*(index: ref Index, driver: ref Driver, root: ref Node) =
  let
    pre_visitor = HlirVisitor(
      visit_macro: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        return vrkContinue),
      visit_function: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.function.type_params.isSome:
          return vrkContinue
        if self.meta.attributes.isSome and self.meta.attributes.get.any((attr: Attribute) => attr.kind == akComptime):
          return vrkContinue
        return vrkRecurse),
    )
    post_visitor = HlirVisitor(
      visit_expr_unit: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.constant.ty = self.meta.type_sum[].as_qualified_in_span(self.meta.span)
        self.meta.constant.span = self.meta.span
        self.meta.constant.expr = ConstExpr(kind: cekUnit)
        return vrkRecurse),
      visit_expr_bool: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.constant.ty = self.meta.type_sum[].as_qualified_in_span(self.meta.span)
        self.meta.constant.span = self.meta.span
        self.meta.constant.expr = ConstExpr(
          kind: cekBool,
          p: self.node.expr_bool.value,
        )
        return vrkRecurse),
      visit_expr_int: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.constant.ty = self.meta.type_sum[].as_qualified_in_span(self.meta.span)
        self.meta.constant.span = self.meta.span
        self.meta.constant.expr = ConstExpr(
          kind: cekSigned,
          sinteger: self.node.expr_int.value,
        )
        return vrkRecurse),
    )
  discard accept(root, pre_visitor, post_visitor)
