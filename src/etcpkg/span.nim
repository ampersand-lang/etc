import strformat
import ice

type
  Span* = object
    directory*: string
    file*: string
    src*: string
    pos*: int
    len*: int
    col*: Natural
    line*: Natural

proc `+`*(lhs: Span, rhs: Span): Span =
  if lhs.directory != rhs.directory or lhs.file != rhs.file:
    raise newIce("spans are not pointing at the same file")
  if rhs.pos < lhs.pos:
    raise newIce("right-hand side span is not to the right of left-hand side span")
  return Span(directory: lhs.directory, file: lhs.file, src: lhs.src, pos: lhs.pos, len: rhs.pos - lhs.pos + rhs.len, col: lhs.col, line: lhs.line)

proc `+=`*(lhs: var Span, rhs: Span) =
  if lhs.directory != rhs.directory or lhs.file != rhs.file:
    raise newIce("spans are not pointing at the same file")
  if rhs.pos < lhs.pos:
    raise newIce("right-hand side span is not to the right of left-hand side span")
  lhs.len = rhs.pos - lhs.pos + rhs.len

proc get*(span: Span): string =
  return span.src[span.pos ..< span.pos + span.len]

proc `$`*(span: Span): string =
  return fmt"{span.directory}/{span.file}:{span.line}:{span.col}: `{span.get}`"
