import tables
import options
import sequtils
import strformat
import span
import path
import names
import bits
import hlir/hlir
import hlir/visitor

type
  Printer = object
    indent: int
    output: string

proc `**`(str: string, n: int): string =
  for _ in 1 .. n:
    result.add(str)

proc print(printer: var Printer, self: ref Node, parent: ref Node)

proc print*(self: ref Node): string =
  var printer = Printer(indent: 0)
  print(printer, self, self)
  return printer.output

proc `$`*(self: ref Node): string = print(self)

proc print_seq(printer: var Printer, self: ref Node, parent: ref Node) =
  for child in self.children:
    print(printer, child, self)

proc print_null(printer: var Printer, self: ref Node, parent: ref Node) =
  discard

proc print_mod(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add(fmt"mod {self.node.module.path};{'\n'}")

proc print_use(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add(fmt"use {self.node.use.path};{'\n'}")

proc print_useextern(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add(fmt"""use extern "{self.node.use_extern.lang}" "{self.node.use_extern.file}";{'\n'}""")

proc print_trait(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add(fmt"define ")
  print(printer, self.node.trait.name_node, self)
  printer.output.add(fmt" = trait({'\n'}")
  printer.indent += 4
  var i = 0
  for child in self.children:
    inc i
    if i == 1: continue
    printer.output.add(" " ** printer.indent)
    print(printer, child, self)
    printer.output.add(",\n")
  printer.indent -= 4
  printer.output.add(" " ** printer.indent)
  printer.output.add(")\n")

proc print_struct(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add(fmt"define ")
  print(printer, self.node.struct.name_node, self)
  printer.output.add(fmt" = struct({'\n'}")
  printer.indent += 4
  var i = 0
  for child in self.children:
    inc i
    if i == 1: continue
    printer.output.add(" " ** printer.indent)
    print(printer, child, self)
    printer.output.add(",\n")
  printer.indent -= 4
  printer.output.add(" " ** printer.indent)
  printer.output.add(")\n")

proc print_union(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add(fmt"define ")
  print(printer, self.node.struct.name_node, self)
  printer.output.add(fmt" = union({'\n'}")
  printer.indent += 4
  var i = 0
  for child in self.children:
    inc i
    if i == 1: continue
    printer.output.add(" " ** printer.indent)
    print(printer, child, self)
    printer.output.add(",\n")
  printer.indent -= 4
  printer.output.add(" " ** printer.indent)
  printer.output.add(")\n")

proc print_enum(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add(fmt"define ")
  print(printer, self.node.enumeration.name_node, self)
  printer.output.add(fmt" = enum({'\n'}")
  printer.indent += 4
  var i = 0
  for child in self.children:
    inc i
    if i == 1: continue
    printer.output.add(" " ** printer.indent)
    print(printer, child, self)
    printer.output.add(",\n")
  printer.indent -= 4
  printer.output.add(" " ** printer.indent)
  printer.output.add(")\n")

proc print_enumvariant(printer: var Printer, self: ref Node, parent: ref Node) =
  print(printer, self.node.enum_variant.name_node, self)
  if self.node.enum_variant.value.isSome:
    printer.output.add(fmt": ")
    print(printer, self.node.enum_variant.value.get, self)

proc print_tagged(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add(fmt"define ")
  print(printer, self.node.tagged.name_node, self)
  printer.output.add(fmt" = tagged({'\n'}")
  printer.indent += 4
  var i = 0
  for child in self.children:
    inc i
    if i == 1: continue
    printer.output.add(" " ** printer.indent)
    print(printer, child, self)
    printer.output.add(",\n")
  printer.indent -= 4
  printer.output.add(" " ** printer.indent)
  printer.output.add(")\n")

proc print_taggedvariant(printer: var Printer, self: ref Node, parent: ref Node) =
  print(printer, self.node.tagged_variant.name_node, self)
  printer.indent += 4
  var i = 0
  for child in self.children:
    inc i
    if i == 1: continue
    printer.output.add(" " ** printer.indent)
    print(printer, child, self)
    printer.output.add(",\n")
  printer.indent -= 4
  printer.output.add(" " ** printer.indent)
  printer.output.add("}\n")

proc print_typedef(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("define ")
  print(printer, self.node.typedef.name_node, self)
  printer.output.add(fmt" = ")
  print(printer, self.node.typedef.ty, self)
  printer.output.add(";\n")

proc print_static(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("static ")
  print(printer, self.node.static_global.name_node, self)
  printer.output.add(": ")
  print(printer, self.node.static_global.ty, self)
  if self.node.static_global.value.isSome:
    printer.output.add(fmt" = ")
    print(printer, self.node.static_global.value.get, self)
  printer.output.add(";\n")

proc print_static_extern(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("static extern ")
  print(printer, self.node.static_extern.name_node, self)
  printer.output.add(": ")
  print(printer, self.node.static_extern.ty, self)
  printer.output.add(";\n")

proc print_define(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("static ")
  print(printer, self.node.define.name_node, self)
  printer.output.add(": ")
  print(printer, self.node.define.ty.get, self)
  printer.output.add(fmt" = ")
  print(printer, self.node.define.value, self)
  printer.output.add(";\n")

proc print_function(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("fn ");
  if self.node.function.name_node.isSome:
    print(printer, self.node.function.name_node.get, self)
    printer.output.add(": ")
  print(printer, self.node.function.ty, self)
  if self.node.function.body.isSome:
    printer.output.add(" = ")
    print(printer, self.node.function.body.get, self)
    printer.output.add("\n")
  else:
    printer.output.add(";")

proc print_macro(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("macro ");
  if self.node.macrofn.name_node.isSome:
    print(printer, self.node.macrofn.name_node.get, self)
    printer.output.add(": ")
  print(printer, self.node.macrofn.ty, self)
  if self.node.macrofn.body.isSome:
    printer.output.add(" = ")
    print(printer, self.node.macrofn.body.get, self)
  else:
    printer.output.add(";\n")

proc print_with(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add(fmt"with {self.node.with_single.path};\n")

proc print_type_type(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("type")

proc print_type_pointer(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("&")
  print(printer, self.node.type_pointer.pointee, self)

proc print_type_array(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("[")
  print(printer, self.node.type_array.elem, self)
  printer.output.add(", ")
  print(printer, self.node.type_array.size, self)
  printer.output.add("]")

proc print_type_slice(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("[")
  print(printer, self.node.type_slice.elem, self)
  printer.output.add("]")

proc print_type_var(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("..")
  print(printer, self.node.type_var.elem, self)

proc print_type_function(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("(")
  for param in self.node.type_function.params:
    print(printer, param, self)
    printer.output.add(", ")
  printer.output.add(") -> ")
  print(printer, self.node.type_function.result_type, self)

proc print_type_never(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("never")

proc print_type_unit(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("unit")

proc print_type_bool(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("bool")

proc print_type_signed(printer: var Printer, self: ref Node, parent: ref Node) =
  case self.node.type_signed.bits.kind
  of bitsWord:
    printer.output.add("sint")
  of bitsCustom:
    case self.node.type_signed.bits.bits
    of 8: printer.output.add("s8")
    of 16: printer.output.add("s16")
    of 32: printer.output.add("s32")
    of 64: printer.output.add("s64")
    of 128: printer.output.add("s128")
    else: printer.output.add(fmt"s(N: {self.node.type_unsigned.bits.bits})")

proc print_type_unsigned(printer: var Printer, self: ref Node, parent: ref Node) =
  case self.node.type_unsigned.bits.kind
  of bitsWord:
    printer.output.add("uint")
  of bitsCustom:
    case self.node.type_unsigned.bits.bits
    of 8: printer.output.add("u8")
    of 16: printer.output.add("u16")
    of 32: printer.output.add("u32")
    of 64: printer.output.add("u64")
    of 128: printer.output.add("u128")
    else: printer.output.add(fmt"u(N: {self.node.type_unsigned.bits.bits})")

proc print_type_floating(printer: var Printer, self: ref Node, parent: ref Node) =
  case self.node.type_floating.bits.kind
  of bitsWord:
    printer.output.add("float")
  of bitsCustom:
    case self.node.type_floating.bits.bits
    of 16: printer.output.add("float16")
    of 32: printer.output.add("float32")
    of 64: printer.output.add("float64")
    of 80: printer.output.add("float80")
    of 128: printer.output.add("float128")
    else: printer.output.add(fmt"float(N: {self.node.type_floating.bits.bits})")

proc print_stmt_if(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("if ")
  print(printer, self.node.stmt_if.cond, self)
  printer.output.add(" ")
  print(printer, self.node.stmt_if.body, self)
  for stmt in self.node.stmt_if.elifs:
    printer.output.add(" else if ")
    print(printer, stmt.cond, self)
    printer.output.add(" ")
    print(printer, stmt.body, self)
  if self.node.stmt_if.el.isSome:
    printer.output.add(" else ")
    print(printer, self.node.stmt_if.el.get, self)

proc print_stmt_while(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("while ")
  print(printer, self.node.stmt_while.cond, self)
  printer.output.add(" ")
  print(printer, self.node.stmt_while.body, self)

proc print_stmt_dowhile(printer: var Printer, self: ref Node, parent: ref Node) =
  print(printer, self.node.stmt_dowhile.body, self)
  printer.output.add(" do while ")
  print(printer, self.node.stmt_dowhile.cond, self)
  printer.output.add(";")

proc print_stmt_for(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("for ")
  if self.node.stmt_for.init.isSome:
    print(printer, self.node.stmt_for.init.get, self)
  printer.output.add("; ")
  if self.node.stmt_for.cond.isSome:
    print(printer, self.node.stmt_for.cond.get, self)
  printer.output.add("; ")
  if self.node.stmt_for.rep.isSome:
    print(printer, self.node.stmt_for.rep.get, self)
  printer.output.add(" ")
  print(printer, self.node.stmt_for.body, self)

proc print_stmt_foreach(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("foreach ")
  print(printer, self.node.stmt_foreach.name_node, self)
  printer.output.add(" in ")
  print(printer, self.node.stmt_foreach.iter, self)
  printer.output.add(" ")
  print(printer, self.node.stmt_foreach.body, self)

proc print_stmt_match(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("match ")
  print(printer, self.node.stmt_match.value, self)
  printer.output.add(" {")
  printer.indent += 4
  for case_of in self.node.stmt_match.cases:
    printer.output.add(" " ** printer.indent)
    print(printer, case_of, self)
    printer.output.add(",\n")
  printer.indent -= 4
  printer.output.add(" " ** printer.indent)
  printer.output.add("}")

proc print_expr_trait(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("trait")

proc print_expr_struct(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("struct")

proc print_expr_union(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("union")

proc print_expr_enum(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("enum")

proc print_expr_tagged(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("tagged")

proc print_expr_interp(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("~(")
  print(printer, self.node.expr_interp.value, self)
  printer.output.add(")")

proc print_expr_compound(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("{\n")
  printer.indent += 4
  for stmt in self.node.expr_compound.elems:
    printer.output.add(" " ** printer.indent)
    print(printer, stmt, self)
    printer.output.add(";\n")
  if self.node.expr_compound.last.isSome:
    printer.output.add(" " ** printer.indent)
    print(printer, self.node.expr_compound.last.get, self)
    printer.output.add("\n")
  printer.indent -= 4
  printer.output.add(" " ** printer.indent)
  printer.output.add("}")

proc print_expr_parenth(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("(")
  for expr in self.node.expr_parenth.elems:
    print(printer, expr, self)
    printer.output.add(", ")
  printer.output.add(")")

proc print_expr_fail(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("fail")
  if self.node.expr_fail.value.isSome:
    printer.output.add(" ")
    print(printer, self.node.expr_fail.value.get, self)

proc print_expr_defer(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("defer ")
  print(printer, self.node.expr_defer.value, self)

proc print_expr_return(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("return")
  if self.node.expr_return.value.isSome:
    printer.output.add(" ")
    print(printer, self.node.expr_return.value.get, self)

proc print_expr_continue(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("continue")

proc print_expr_break(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("break")

proc print_expr_let(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("let ")
  print(printer, self.node.expr_let.name_node, self)
  if self.node.expr_let.ty.isSome:
    printer.output.add(": ")
    print(printer, self.node.expr_let.ty.get, self)
  if self.node.expr_let.value.isSome:
    printer.output.add(" = ")
    print(printer, self.node.expr_let.value.get, self)

proc print_expr_int(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.addInt(self.node.expr_int.value)

proc print_expr_float(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.addFloat(self.node.expr_float.value)

proc print_expr_bool(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add($self.node.expr_bool.value)

proc print_expr_unit(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("()")

proc print_expr_string(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("\"")
  printer.output.add(self.node.expr_string.value)
  printer.output.add("\"")

proc print_expr_named(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add($self.node.expr_named.name.short)
  if self.node.expr_named.type_args.isSome:
    printer.output.add("(")
    for arg in self.node.expr_named.type_args.get:
      print(printer, arg, self)
      printer.output.add(", ")
    printer.output.add(")")

proc print_expr_array(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("[")
  for expr in self.node.expr_array.elems:
    print(printer, expr, self)
    printer.output.add(", ")
  printer.output.add("]")

proc print_expr_application(printer: var Printer, self: ref Node, parent: ref Node) =
  print(printer, self.node.expr_application.fn, self)
  printer.output.add("(")
  for expr in self.node.expr_application.args:
    print(printer, expr, self)
    printer.output.add(", ")
  printer.output.add(")")

proc print_expr_command(printer: var Printer, self: ref Node, parent: ref Node) =
  print(printer, self.node.expr_command.fn, self)
  printer.output.add("! ")
  for expr in self.node.expr_command.args:
    print(printer, expr, self)
    printer.output.add(", ")

proc print_expr_fieldaccess(printer: var Printer, self: ref Node, parent: ref Node) =
  print(printer, self.node.expr_field_access.value, self)
  printer.output.add(".")
  printer.output.add(self.node.expr_field_access.name)

proc print_expr_cast(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("cast(")
  print(printer, self.node.expr_cast.ty, self)
  printer.output.add(", ")
  print(printer, self.node.expr_cast.value, self)
  printer.output.add(")")

proc print_expr_sizeof(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("sizeof(")
  print(printer, self.node.expr_sizeof.value, self)
  printer.output.add(")")

proc print_expr_alignof(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("alignof(")
  print(printer, self.node.expr_alignof.value, self)
  printer.output.add(")")

proc print_expr_unary(printer: var Printer, self: ref Node, parent: ref Node) =
  case self.node.expr_unary.op
  of hukPostInc:
    print(printer, self.node.expr_unary.value, self)
    printer.output.add("++")
  of hukPostDec:
    print(printer, self.node.expr_unary.value, self)
    printer.output.add("--")
  of hukPreInc:
    printer.output.add("++")
    print(printer, self.node.expr_unary.value, self)
  of hukPreDec:
    printer.output.add("--")
    print(printer, self.node.expr_unary.value, self)
  of hukRef:
    printer.output.add("&")
    print(printer, self.node.expr_unary.value, self)
  of hukDeref:
    printer.output.add("*")
    print(printer, self.node.expr_unary.value, self)
  of hukPos:
    printer.output.add("+")
    print(printer, self.node.expr_unary.value, self)
  of hukNeg:
    printer.output.add("-")
    print(printer, self.node.expr_unary.value, self)
  of hukNot:
    printer.output.add("!")
    print(printer, self.node.expr_unary.value, self)
  of hukColon:
    printer.output.add(":")
    print(printer, self.node.expr_unary.value, self)
  of hukRangeFrom:
    print(printer, self.node.expr_unary.value, self)
    printer.output.add("..")
  of hukRangeTo:
    printer.output.add("..")
    print(printer, self.node.expr_unary.value, self)

proc print_expr_binary(printer: var Printer, self: ref Node, parent: ref Node) =
  case self.node.expr_binary.op
  of hbkIndex:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add("[")
    print(printer, self.node.expr_binary.rhs, self)
    printer.output.add("]")
  of hbkAdd:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add(" + ")
    print(printer, self.node.expr_binary.rhs, self)
  of hbkSub:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add(" - ")
    print(printer, self.node.expr_binary.rhs, self)
  of hbkMul:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add(" * ")
    print(printer, self.node.expr_binary.rhs, self)
  of hbkDiv:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add(" / ")
    print(printer, self.node.expr_binary.rhs, self)
  of hbkRem:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add(" % ")
    print(printer, self.node.expr_binary.rhs, self)
  of hbkShl:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add(" << ")
    print(printer, self.node.expr_binary.rhs, self)
  of hbkShr:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add(" >> ")
    print(printer, self.node.expr_binary.rhs, self)
  of hbkLt:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add(" < ")
    print(printer, self.node.expr_binary.rhs, self)
  of hbkGt:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add(" > ")
    print(printer, self.node.expr_binary.rhs, self)
  of hbkLe:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add(" <= ")
    print(printer, self.node.expr_binary.rhs, self)
  of hbkGe:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add(" >= ")
    print(printer, self.node.expr_binary.rhs, self)
  of hbkEq:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add(" == ")
    print(printer, self.node.expr_binary.rhs, self)
  of hbkNe:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add(" != ")
    print(printer, self.node.expr_binary.rhs, self)
  of hbkBitand:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add(" & ")
    print(printer, self.node.expr_binary.rhs, self)
  of hbkBitor:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add(" | ")
    print(printer, self.node.expr_binary.rhs, self)
  of hbkBitxor:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add(" ^ ")
    print(printer, self.node.expr_binary.rhs, self)
  of hbkLogicalAnd:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add(" and ")
    print(printer, self.node.expr_binary.rhs, self)
  of hbkLogicalOr:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add(" or ")
    print(printer, self.node.expr_binary.rhs, self)
  of hbkRange:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add(" .. ")
    print(printer, self.node.expr_binary.rhs, self)
  of hbkAssign:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add(" = ")
    print(printer, self.node.expr_binary.rhs, self)
  of hbkAssignMul:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add(" *= ")
    print(printer, self.node.expr_binary.rhs, self)
  of hbkAssignDiv:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add(" /= ")
    print(printer, self.node.expr_binary.rhs, self)
  of hbkAssignRem:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add(" %= ")
    print(printer, self.node.expr_binary.rhs, self)
  of hbkAssignAdd:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add(" += ")
    print(printer, self.node.expr_binary.rhs, self)
  of hbkAssignSub:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add(" -= ")
    print(printer, self.node.expr_binary.rhs, self)
  of hbkAssignLshift:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add(" <<= ")
    print(printer, self.node.expr_binary.rhs, self)
  of hbkAssignRshift:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add(" >>= ")
    print(printer, self.node.expr_binary.rhs, self)
  of hbkAssignAnd:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add(" &= ")
    print(printer, self.node.expr_binary.rhs, self)
  of hbkAssignXor:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add(" ^= ")
    print(printer, self.node.expr_binary.rhs, self)
  of hbkAssignOr:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add(" |= ")
    print(printer, self.node.expr_binary.rhs, self)
  of hbkArrow:
    print(printer, self.node.expr_binary.lhs, self)
    printer.output.add(" -> ")
    print(printer, self.node.expr_binary.rhs, self)

proc print_expr_ternary(printer: var Printer, self: ref Node, parent: ref Node) =
  case self.node.expr_ternary.op
  of htkCond:
    print(printer, self.node.expr_ternary.value, self)
    printer.output.add("? ")
    if self.node.expr_ternary.lhs.isSome:
      print(printer, self.node.expr_ternary.lhs.get, self)
      printer.output.add(" ")
    printer.output.add(":")
    if self.node.expr_ternary.rhs.isSome:
      printer.output.add(" ")
      print(printer, self.node.expr_ternary.rhs.get, self)

proc print_expr_range_full(printer: var Printer, self: ref Node, parent: ref Node) =
  printer.output.add("..")

proc print_pat_bind(printer: var Printer, self: ref Node, parent: ref Node) =
  discard

proc print_pat_destructure(printer: var Printer, self: ref Node, parent: ref Node) =
  discard

proc print_pat_else(printer: var Printer, self: ref Node, parent: ref Node) =
  discard

proc print_decl(printer: var Printer, self: ref Node, parent: ref Node) =
  if self.node.decl.name.isSome:
    print(printer, self.node.decl.name.get, self)
    printer.output.add(": ")
  print(printer, self.node.decl.value, self)

proc print_case(printer: var Printer, self: ref Node, parent: ref Node) =
  discard

proc print*(printer: var Printer, self: ref Node) =
  print(printer, self, self)

proc print(printer: var Printer, self: ref Node, parent: ref Node) =
  case self.node.kind
  of hrkRoot: print(printer, self.node.root.root, self)
  of hrkSeq: print_seq(printer, self, parent)
  of hrkNull: print_null(printer, self, parent)
  of hrkMod: print_mod(printer, self, parent)
  of hrkUse: print_use(printer, self, parent)
  of hrkUseExtern: print_useextern(printer, self, parent)
  of hrkTrait: print_trait(printer, self, parent)
  of hrkStruct: print_struct(printer, self, parent)
  of hrkUnion: print_union(printer, self, parent)
  of hrkEnum: print_enum(printer, self, parent)
  of hrkEnumVariant: print_enumvariant(printer, self, parent)
  of hrkTagged: print_tagged(printer, self, parent)
  of hrkTaggedVariant: print_taggedvariant(printer, self, parent)
  of hrkTypedef: print_typedef(printer, self, parent)
  of hrkStatic: print_static(printer, self, parent)
  of hrkStaticExtern: print_static_extern(printer, self, parent)
  of hrkDefine: print_define(printer, self, parent)
  of hrkFunction: print_function(printer, self, parent)
  of hrkMacro: print_macro(printer, self, parent)
  of hrkWith: print_with(printer, self, parent)
  of hrkTypeType: print_type_type(printer, self, parent)
  of hrkTypePointer: print_type_pointer(printer, self, parent)
  of hrkTypeArray: print_type_array(printer, self, parent)
  of hrkTypeSlice: print_type_slice(printer, self, parent)
  of hrkTypeVar: print_type_var(printer, self, parent)
  of hrkTypeFunction: print_type_function(printer, self, parent)
  of hrkTypeNever: print_type_never(printer, self, parent)
  of hrkTypeUnit: print_type_unit(printer, self, parent)
  of hrkTypeBool: print_type_bool(printer, self, parent)
  of hrkTypeSigned: print_type_signed(printer, self, parent)
  of hrkTypeUnsigned: print_type_unsigned(printer, self, parent)
  of hrkTypeFloating: print_type_floating(printer, self, parent)
  of hrkStmtIf: print_stmt_if(printer, self, parent)
  of hrkStmtWhile: print_stmt_while(printer, self, parent)
  of hrkStmtDowhile: print_stmt_dowhile(printer, self, parent)
  of hrkStmtFor: print_stmt_for(printer, self, parent)
  of hrkStmtForeach: print_stmt_foreach(printer, self, parent)
  of hrkStmtMatch: print_stmt_match(printer, self, parent)
  of hrkExprTrait: print_expr_trait(printer, self, parent)
  of hrkExprStruct: print_expr_struct(printer, self, parent)
  of hrkExprUnion: print_expr_union(printer, self, parent)
  of hrkExprEnum: print_expr_enum(printer, self, parent)
  of hrkExprTagged: print_expr_tagged(printer, self, parent)
  of hrkExprInterp: print_expr_interp(printer, self, parent)
  of hrkExprCompound: print_expr_compound(printer, self, parent)
  of hrkExprParenth: print_expr_parenth(printer, self, parent)
  of hrkExprFail: print_expr_fail(printer, self, parent)
  of hrkExprDefer: print_expr_defer(printer, self, parent)
  of hrkExprReturn: print_expr_return(printer, self, parent)
  of hrkExprContinue: print_expr_continue(printer, self, parent)
  of hrkExprBreak: print_expr_break(printer, self, parent)
  of hrkExprLet: print_expr_let(printer, self, parent)
  of hrkExprInt: print_expr_int(printer, self, parent)
  of hrkExprFloat: print_expr_float(printer, self, parent)
  of hrkExprBool: print_expr_bool(printer, self, parent)
  of hrkExprUnit: print_expr_unit(printer, self, parent)
  of hrkExprString: print_expr_string(printer, self, parent)
  of hrkExprNamed: print_expr_named(printer, self, parent)
  of hrkExprArray: print_expr_array(printer, self, parent)
  of hrkExprApplication: print_expr_application(printer, self, parent)
  of hrkExprCommand: print_expr_command(printer, self, parent)
  of hrkExprFieldAccess: print_expr_fieldaccess(printer, self, parent)
  of hrkExprCast: print_expr_cast(printer, self, parent)
  of hrkExprSizeof: print_expr_sizeof(printer, self, parent)
  of hrkExprAlignof: print_expr_alignof(printer, self, parent)
  of hrkExprUnary: print_expr_unary(printer, self, parent)
  of hrkExprBinary: print_expr_binary(printer, self, parent)
  of hrkExprTernary: print_expr_ternary(printer, self, parent)
  of hrkExprRangeFull: print_expr_range_full(printer, self, parent)
  of hrkPatBind: print_pat_bind(printer, self, parent)
  of hrkPatDestructure: print_pat_destructure(printer, self, parent)
  of hrkPatElse: print_pat_else(printer, self, parent)
  of hrkDecl: print_decl(printer, self, parent)
  of hrkCase: print_case(printer, self, parent)
