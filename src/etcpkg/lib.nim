import options
import sequtils
import deques
import tables
import sets
import os

import ice
import error
import path
import names
import bits
import span
import target
import index
import print
import driver/driver_typedefs
import driver/driver
import builtin_modules
import hlir/[hlir, visitor]
import types/types
import const_expr/const_expr
import parse/lex
import parse/grammar
import llvm/llvm
import parse/lex
import parse/grammar
import sema/early_normalization
import sema/type_analysis
import sema/const_expr_pass
import sema/name_mangler
import sema/flow
import cg/cg
import cg/cgvar
import version

export ice
export error
export path
export names
export bits
export span
export target
export index
export print
export driver
export builtin_modules
export driver_typedefs
export hlir
export visitor
export types
export const_expr
export lex
export grammar
export version

# these are declared here, because otherwise they would cause cyclic dependency issues
method init*(pass: LexPassController, index: ref Index, driver: ref Driver) =
  discard

method init*(pass: ParsePassController, index: ref Index, driver: ref Driver) =
  discard

method init*(pass: EnPassController, index: ref Index, driver: ref Driver) =
  discard

method init*(pass: TdPassController, index: ref Index, driver: ref Driver) =
  discard

method init*(pass: TaPassController, index: ref Index, driver: ref Driver) =
  pass.type_ctx = driver.type_ctx
  pass.module = Path(full: true, path: @[])
  pass.function_stack = newSeq[Path]()
  pass.match_stack = newSeq[ref TypeSum]()
  pass.monomorphized = initTAble[(Path, ref QualifiedType), ref Node]()
  pass.polymorphic_stack = newSeq[Table[(Path, ref QualifiedType), ref Node]]()
  pass.parameter_stack = newSeq[HashSet[string]]()
  pass.tests = newSeq[Names]()
  pass.enter
  pass.enter_parametric

method init*(pass: CePassController, index: ref Index, driver: ref Driver) =
  discard

method init*(pass: NmPassController, index: ref Index, driver: ref Driver) =
  discard

method init*(pass: FaPassController, index: ref Index, driver: ref Driver) =
  discard

method init*(pass: CdPassController, index: ref Index, driver: ref Driver) =
  discard

method init*(pass: CgPassController, index: ref Index, driver: ref Driver) =
  let
    ctx = llvm.contextCreate()
    module = llvm.moduleCreateWithNameInContext(cstring("todo"), ctx)
    builder = llvm.createBuilderInContext(ctx)
    di_builder = llvm.createDiBuilder(module)
  pass.type_ctx = driver.type_ctx
  pass.ctx = ctx
  pass.module = module
  pass.builder = builder
  pass.di_builder = di_builder

  if pass.opt_level > 0:
    pass.pr = llvm.getGlobalPassRegistry()
    llvm.initializeTransformUtils(pass.pr)
    llvm.initializeScalarOpts(pass.pr)
    llvm.initializeInstCombine(pass.pr)

    let mp = llvm.createModuleProviderForExistingModule(pass.module)
    let fpm = llvm.createFunctionPassManager(mp)
    llvm.initializeFunctionPassManager(fpm)
    llvm.addInstructionCombiningPass(fpm)
    llvm.addReassociatePass(fpm)
    llvm.addGVNPass(fpm)
    llvm.addCFGSimplificationPass(fpm)
    pass.mp = mp
    pass.fpm = fpm

    let pm = llvm.createPassManager()
    llvm.addEarlyCSEPass(pm)
    pass.pm = pm
  if pass.opt_level > 1:
    let fpm = pass.fpm
    llvm.addTailCallEliminationPass(fpm)
    llvm.addDeadStoreEliminationPass(fpm)
    llvm.addPromoteMemoryToRegisterPass(fpm)
    llvm.addLICMPass(fpm)
    llvm.addLoopIdiomPass(fpm)
    llvm.addLoopRerollPass(fpm)
    llvm.addLoopUnswitchPass(fpm)
    llvm.addLoopVectorizePass(fpm)
    llvm.addLoopUnrollPass(fpm)

  if pass.debug_info:
    let
      dwarf_version = llvm.valueAsMetadata(llvm.constInt(llvm.intTypeInContext(pass.ctx, 32), 4, 0))
      debug_md_version = llvm.valueAsMetadata(llvm.constInt(llvm.intTypeInContext(pass.ctx, 32), llvm.debugMetadataVersion().culong, 0))
      dwarf_version_name = "Dwarf Version"
      debug_md_version_name = "Debug Info Version"
    llvm.addModuleFlag(pass.module, llvm.ModuleFlagBehaviorWarning, dwarf_version_name.cstring, dwarf_version_name.len.uint, dwarf_version);
    llvm.addModuleFlag(pass.module, llvm.ModuleFlagBehaviorWarning, debug_md_version_name.cstring, debug_md_version_name.len.uint, debug_md_version);

    let void_type = llvm.voidTypeInContext(pass.ctx)
    let metadata_type = llvm.metadataTypeInContext(pass.ctx)
    var params = [metadata_type, metadata_type, metadata_type]
    let dbg_addr = llvm.functionType(void_type, addr params[0], params.len.cuint, 0)
    discard llvm.addFunction(pass.module, cstring("llvm.dbg.addr"), dbg_addr)

  pass.bb_stack = newSeq[seq[llvm.BasicBlockRef]]()
  pass.function_stack = newSeq[llvm.ValueRef]()
  pass.pattern_stack = newSeq[tuple[value: ref CgVar, ty: ref QualifiedType, is_switch: bool, variant: int, variant_name: Option[string]]]()
  pass.switch_stack = newSeq[ref CgVar]()

  pass.bindings = newSeq[Table[string, ref CgVar]]()
  pass.types = newSeq[Table[string, llvm.TypeRef]]()

  pass.result_var = newSeq[ref CgVar]()
  pass.return_bb = newSeq[seq[llvm.BasicBlockRef]]()

  pass.types.add(initTable[string, llvm.TypeRef]())
  pass.bindings.add(initTable[string, ref CgVar]())
  pass.return_bb.add(newSeq[seq[llvm.BasicBlockRef]]())

  pass.continue_target = newSeq[seq[llvm.BasicBlockRef]]()
  pass.break_target = newSeq[seq[llvm.BasicBlockRef]]()

  pass.bind_type("unit".toPath.mangled_as_type, llvm.voidTypeInContext(pass.ctx))
  pass.bind_type("never".toPath.mangled_as_type, llvm.voidTypeInContext(pass.ctx))
  pass.bind_type("bool".toPath.mangled_as_type, llvm.intTypeInContext(pass.ctx, 1))
  pass.bind_type("s8".toPath.mangled_as_type, llvm.intTypeInContext(pass.ctx, 8))
  pass.bind_type("s16".toPath.mangled_as_type, llvm.intTypeInContext(pass.ctx, 16))
  pass.bind_type("s32".toPath.mangled_as_type, llvm.intTypeInContext(pass.ctx, 32))
  pass.bind_type("s64".toPath.mangled_as_type, llvm.intTypeInContext(pass.ctx, 64))
  pass.bind_type("s128".toPath.mangled_as_type, llvm.intTypeInContext(pass.ctx, 128))
  pass.bind_type("sint".toPath.mangled_as_type, llvm.intTypeInContext(pass.ctx, driver.target.pointer_width.cuint))
  pass.bind_type("u8".toPath.mangled_as_type, llvm.intTypeInContext(pass.ctx, 8))
  pass.bind_type("u16".toPath.mangled_as_type, llvm.intTypeInContext(pass.ctx, 16))
  pass.bind_type("u32".toPath.mangled_as_type, llvm.intTypeInContext(pass.ctx, 32))
  pass.bind_type("u64".toPath.mangled_as_type, llvm.intTypeInContext(pass.ctx, 64))
  pass.bind_type("u128".toPath.mangled_as_type, llvm.intTypeInContext(pass.ctx, 128))
  pass.bind_type("uint".toPath.mangled_as_type, llvm.intTypeInContext(pass.ctx, driver.target.pointer_width.cuint))

  let string_type = llvm.structCreateNamed(pass.ctx, cstring(["__lang", "builtin", "str"].toPath.mangled_as_type))
  var string_fields = [
    pass.get_type("uint".toPath.mangled_as_type).get,
    llvm.pointerType(pass.get_type("s8".toPath.mangled_as_type).get, 0),
  ]
  llvm.structSetBody(string_type, addr string_fields[0], cuint(string_fields.len), 0)
  pass.bind_type(["__lang", "builtin", "str"].toPath.mangled_as_type, string_type)

  let range_full_type = llvm.structCreateNamed(pass.ctx, cstring(["__lang", "builtin", "RangeFull"].toPath.mangled_as_type))
  llvm.structSetBody(range_full_type, nil, 0, 0)
  pass.bind_type(["__lang", "builtin", "RangeFull"].toPath.mangled_as_type, range_full_type)

method init*(pass: EmptyPassController, index: ref Index, driver: ref Driver) =
  discard

method analyze*(pass: LexPassController, index: ref Index, driver: ref Driver, unit: ref IndexUnit, node: ref Node) =
  unit.tokens = some(lex(unit.path.parentDir, unit.path.lastPathPart, unit.source))

method analyze*(pass: ParsePassController, index: ref Index, driver: ref Driver, unit: ref IndexUnit, node: ref Node) =
  unit.root = some(parse(unit.tokens.get))

method analyze*(pass: EnPassController, index: ref Index, driver: ref Driver, unit: ref IndexUnit, node: ref Node) =
  early_normalization.analyze(index, driver, node)

method analyze*(pass: TdPassController, index: ref Index, driver: ref Driver, unit: ref IndexUnit, node: ref Node) =
  type_analysis.type_reg(index, driver, node)
  type_analysis.type_gen(index, driver, node)
  type_analysis.type_args(index, driver, node)
  type_analysis.type_helper(driver, node)
  type_analysis.decl(index, driver, node)

method analyze*(pass: TaPassController, index: ref Index, driver: ref Driver, unit: ref IndexUnit, node: ref Node) =
  type_analysis.analyze(index, driver, node)
  type_analysis.eval(driver, node)

method analyze*(pass: CePassController, index: ref Index, driver: ref Driver, unit: ref IndexUnit, node: ref Node) =
  const_expr_pass.analyze(index, driver, node)

method analyze*(pass: NmPassController, index: ref Index, driver: ref Driver, unit: ref IndexUnit, node: ref Node) =
  name_mangler.analyze(index, driver, node)

method analyze*(pass: FaPassController, index: ref Index, driver: ref Driver, unit: ref IndexUnit, node: ref Node) =
  flow.analyze(index, driver, node)

method analyze*(pass: CdPassController, index: ref Index, driver: ref Driver, unit: ref IndexUnit, node: ref Node) =
  cg.type_reg(index, driver, node)
  cg.type_gen(index, driver, node)
  cg.const_value_gen(index, driver, node)
  cg.decl(index, driver, node)

method analyze*(pass: CgPassController, index: ref Index, driver: ref Driver, unit: ref IndexUnit, node: ref Node) =
  cg.collect_vars(index, driver, node)
  cg.code_gen(index, driver, node)

method analyze*(pass: EmptyPassController, index: ref Index, driver: ref Driver, unit: ref IndexUnit, node: ref Node) =
  discard

method analyze_with_root*(pass: LexPassController, index: ref Index, driver: ref Driver, unit: ref IndexUnit, node: ref Node, root: ref Node) =
  unit.tokens = some(lex(unit.path.parentDir, unit.path.lastPathPart, unit.source))

method analyze_with_root*(pass: ParsePassController, index: ref Index, driver: ref Driver, unit: ref IndexUnit, node: ref Node, root: ref Node) =
  unit.root = some(parse(unit.tokens.get))

method analyze_with_root*(pass: EnPassController, index: ref Index, driver: ref Driver, unit: ref IndexUnit, node: ref Node, root: ref Node) =
  early_normalization.analyze(index, driver, node, root)

method analyze_with_root*(pass: TdPassController, index: ref Index, driver: ref Driver, unit: ref IndexUnit, node: ref Node, root: ref Node) =
  type_analysis.type_reg(index, driver, node, root)
  type_analysis.type_gen(index, driver, node, root)
  type_analysis.type_args(index, driver, node, root)
  type_analysis.type_helper(driver, node, root)
  type_analysis.decl(index, driver, node, root)

method analyze_with_root*(pass: TaPassController, index: ref Index, driver: ref Driver, unit: ref IndexUnit, node: ref Node, root: ref Node) =
  type_analysis.analyze(index, driver, node, root)
  type_analysis.eval(driver, node, root)

method analyze_with_root*(pass: CePassController, index: ref Index, driver: ref Driver, unit: ref IndexUnit, node: ref Node, root: ref Node) =
  const_expr_pass.analyze(index, driver, node)

method analyze_with_root*(pass: NmPassController, index: ref Index, driver: ref Driver, unit: ref IndexUnit, node: ref Node, root: ref Node) =
  name_mangler.analyze(index, driver, node)

method analyze_with_root*(pass: FaPassController, index: ref Index, driver: ref Driver, unit: ref IndexUnit, node: ref Node, root: ref Node) =
  flow.analyze(index, driver, node)

method analyze_with_root*(pass: CdPassController, index: ref Index, driver: ref Driver, unit: ref IndexUnit, node: ref Node, root: ref Node) =
  cg.type_reg(index, driver, node, root)
  cg.type_gen(index, driver, node, root)
  cg.const_value_gen(index, driver, node, root)
  cg.decl(index, driver, node, root)

method analyze_with_root*(pass: CgPassController, index: ref Index, driver: ref Driver, unit: ref IndexUnit, node: ref Node, root: ref Node) =
  cg.collect_vars(index, driver, node, root)
  cg.code_gen(index, driver, node, root)

method analyze_with_root*(pass: EmptyPassController, index: ref Index, driver: ref Driver, unit: ref IndexUnit, node: ref Node, root: ref Node) =
  discard
