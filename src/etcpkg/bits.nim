import hashes

type
  # the bits of a primitive numerical (signed, unsigned or floating) type
  BitsKind* = enum
    bitsWord
    bitsCustom
  Bits* = object
    case kind*: BitsKind
    of bitsWord: discard
    of bitsCustom: bits*: uint8

proc toBits*(bits: uint8): Bits =
  return Bits(kind: bitsCustom, bits: bits)

proc `==`*(lhs: Bits, rhs: Bits): bool =
  case lhs.kind
  of bitsWord: return rhs.kind == bitsWord
  of bitsCustom: return rhs.kind == bitsCustom and lhs.bits == rhs.bits

proc hash*(bits: Bits): Hash =
  var h: Hash = 0
  h = h !& bits.kind.int
  case bits.kind
  of bitsWord:
    discard
  of bitsCustom:
    h = h !& bits.bits.int
  result = !$h
