import options
import tables
import sets
import ../ice
import ../error
import ../span
import ../path
import ../bits
import ../names
import ../cg/cgvar
import ../types/types
import ../const_expr/const_expr
import ../llvm/llvm

type
  AttributeKind* = enum
    akNoReturn
    akGlobal
    akLocal
    akThread
    akInline
    akNoMangle
    akNoHidden
    akComptime
    akTest
    akTestMain
  Attribute* = object
    kind*: AttributeKind
    args*: seq[ref Node]

  FlowBreak* = enum
    fbUndefined
    fbNo
    fbYes
    fbMaybe

  # the metadata associated with a node
  Meta* = object
    span*: Span
    call_site*: Option[ref Node]
    polymorphic*: Option[ref Node]
    attributes*: Option[seq[Attribute]]

    returns*: FlowBreak
    breaks*: FlowBreak
    continues*: FlowBreak

    impl_scope*: Option[ImplScope]
    cg_bindings_scope*: Option[Table[string, ref CgVar]]
    cg_types_scope*: Option[Table[string, llvm.TypeRef]]
    impl*: Option[ref Impl]
    overload*: Option[string]
    type_sum*: ref TypeSum
    is_type*: bool
    own_type*: Option[ref QualifiedType]
    prepare_args*: Option[seq[(string, ref QualifiedType)]]
    switch_type*: ref TypeSum
    constant*: TypedExpr
    type_ref*: llvm.TypeRef
    value*: ref CgVar
    vararr*: Option[llvm.ValueRef]
    is_const*: bool
    meta_type*: llvm.MetadataRef
    meta_data*: llvm.MetadataRef

    store_at*: Option[ref CgVar]
    saved_block*: Option[llvm.BasicBlockRef]
    in_block*: Option[llvm.BasicBlockRef]
    after_block*: Option[llvm.BasicBlockRef]
    br*: Option[llvm.BasicBlockRef]
    cond_br*: Option[tuple[value: ref CgVar, t: llvm.BasicBlockRef, f: llvm.BasicBlockRef]]
    make_switch*: Option[tuple[switch: ref CgVar, el: llvm.BasicBlockRef, after: llvm.BasicBlockRef, cases: cuint]]

    # a counter of how many visits should be skipped with a vrkNoRecurse
    dont_visit*: int
  # a generic node consisting of a runtime-specified hlir tree and some mutable metadata
  Node* = object
    meta*: Meta
    node*: Hlir

  HlirKind* = enum
    # placeholder for a parent
    hrkRoot
    # generic sequence of nodes
    hrkSeq
    # an empty node
    hrkNull
    # "top-level" items
    hrkMod
    hrkUse
    hrkUseExtern
    hrkTrait
    hrkStruct
    hrkUnion
    hrkEnum
    hrkEnumVariant
    hrkTagged
    hrkTaggedVariant
    hrkTypedef
    hrkStatic
    hrkStaticExtern
    hrkDefine
    hrkFunction
    hrkMacro
    hrkWith
    # types
    hrkTypeType
    hrkTypePointer
    hrkTypeArray
    hrkTypeSlice
    hrkTypeVar
    hrkTypeFunction
    hrkTypeNever
    hrkTypeUnit
    hrkTypeBool
    hrkTypeSigned
    hrkTypeUnsigned
    hrkTypeFloating
    # statements
    hrkStmtIf
    hrkStmtWhile
    hrkStmtDowhile
    hrkStmtFor
    hrkStmtForeach
    hrkStmtMatch
    # expressions
    hrkExprTrait
    hrkExprStruct
    hrkExprUnion
    hrkExprEnum
    hrkExprTagged
    hrkExprInterp
    hrkExprCompound
    hrkExprParenth
    hrkExprFail
    hrkExprDefer
    hrkExprReturn
    hrkExprContinue
    hrkExprBreak
    hrkExprLet
    hrkExprInt
    hrkExprFloat
    hrkExprBool
    hrkExprUnit
    hrkExprString
    hrkExprNamed
    hrkExprArray
    hrkExprApplication
    hrkExprCommand
    hrkExprFieldAccess
    hrkExprCast
    hrkExprSizeof
    hrkExprAlignof
    hrkExprUnary
    hrkExprBinary
    hrkExprTernary
    hrkExprRangeFull
    # patterns
    hrkPatBind
    hrkPatDestructure
    hrkPatElse
    # miscellaneous
    hrkDecl
    hrkCase
  Hlir* = object
    case kind*: HlirKind
    of hrkRoot: root*: HlirRoot
    of hrkSeq: sequence*: HlirSeq
    of hrkNull: null*: HlirNull
    of hrkMod: module*: HlirMod
    of hrkUse: use*: HlirUse
    of hrkUseExtern: use_extern*: HlirUseExtern
    of hrkTrait: trait*: HlirTrait
    of hrkStruct, hrkUnion: struct*: HlirStruct
    of hrkEnum: enumeration*: HlirEnum
    of hrkEnumVariant: enum_variant*: HlirEnumVariant
    of hrkTagged: tagged*: HlirTagged
    of hrkTaggedVariant: tagged_variant*: HlirTaggedVariant
    of hrkTypedef: typedef*: HlirTypedef
    of hrkStatic: static_global*: HlirStatic
    of hrkStaticExtern: static_extern*: HlirStaticExtern
    of hrkDefine: define*: HlirDefine
    of hrkFunction: function*: HlirFunction
    of hrkMacro: macrofn*: HlirMacro
    of hrkWith: with_single*: HlirWith
    of hrkTypeType: type_type*: HlirTypeType
    of hrkTypePointer: type_pointer*: HlirTypePointer
    of hrkTypeArray: type_array*: HlirTypeArray
    of hrkTypeSlice: type_slice*: HlirTypeSlice
    of hrkTypeVar: type_var*: HlirTypeVar
    of hrkTypeFunction: type_function*: HlirTypeFunction
    of hrkTypeNever: type_never*: HlirTypeNever
    of hrkTypeUnit: type_unit*: HlirTypeUnit
    of hrkTypeBool: type_bool*: HlirTypeBool
    of hrkTypeSigned: type_signed*: HlirTypeSigned
    of hrkTypeUnsigned: type_unsigned*: HlirTypeUnsigned
    of hrkTypeFloating: type_floating*: HlirTypeFloating
    of hrkStmtIf: stmt_if*: HlirStmtIf
    of hrkStmtWhile: stmt_while*: HlirStmtWhile
    of hrkStmtDowhile: stmt_dowhile*: HlirStmtDowhile
    of hrkStmtFor: stmt_for*: HlirStmtFor
    of hrkStmtForeach: stmt_foreach*: HlirStmtForeach
    of hrkStmtMatch: stmt_match*: HlirStmtMatch
    of hrkExprTrait: expr_trait*: HlirExprTrait
    of hrkExprStruct: expr_struct*: HlirExprStruct
    of hrkExprUnion: expr_union*: HlirExprUnion
    of hrkExprEnum: expr_enum*: HlirExprEnum
    of hrkExprTagged: expr_tagged*: HlirExprTagged
    of hrkExprInterp: expr_interp*: HlirExprInterp
    of hrkExprCompound: expr_compound*: HlirExprCompound
    of hrkExprParenth: expr_parenth*: HlirExprParenth
    of hrkExprFail: expr_fail*: HlirExprFail
    of hrkExprDefer: expr_defer*: HlirExprDefer
    of hrkExprReturn: expr_return*: HlirExprReturn
    of hrkExprContinue: expr_continue*: HlirExprContinue
    of hrkExprBreak: expr_break*: HlirExprBreak
    of hrkExprLet: expr_let*: HlirExprLet
    of hrkExprInt: expr_int*: HlirExprInt
    of hrkExprFloat: expr_float*: HlirExprFloat
    of hrkExprBool: expr_bool*: HlirExprBool
    of hrkExprUnit: expr_unit*: HlirExprUnit
    of hrkExprString: expr_string*: HlirExprString
    of hrkExprNamed: expr_named*: HlirExprNamed
    of hrkExprArray: expr_array*: HlirExprArray
    of hrkExprApplication: expr_application*: HlirExprApplication
    of hrkExprCommand: expr_command*: HlirExprCommand
    of hrkExprFieldAccess: expr_fieldaccess*: HlirExprFieldAccess
    of hrkExprCast: expr_cast*: HlirExprCast
    of hrkExprSizeof: expr_sizeof*: HlirExprSizeof
    of hrkExprAlignof: expr_alignof*: HlirExprAlignof
    of hrkExprUnary: expr_unary*: HlirExprUnary
    of hrkExprBinary: expr_binary*: HlirExprBinary
    of hrkExprTernary: expr_ternary*: HlirExprTernary
    of hrkExprRangeFull: expr_range_full*: HlirExprRangeFull
    of hrkPatBind: pat_bind*: HlirPatBind
    of hrkPatDestructure: pat_destructure*: HlirPatDestructure
    of hrkPatElse: pat_else*: HlirPatElse
    of hrkDecl: decl*: HlirDecl
    of hrkCase: case_of*: HlirCase

  HlirRoot* = object
    root*: ref Node
  HlirSeq* = object
    children*: seq[ref Node]
  HlirNull* = object
  HlirMod* = object
    path*: Path
  HlirUse* = object
    path*: Path
  HlirUseExtern* = object
    lang*: string
    file*: string
  HlirTrait* = object
    name_node*: ref Node
    name*: Option[Names]
    type_params*: Option[seq[ref Node]]
    type_args*: Option[seq[ref Node]]
    fns*: seq[ref Node]
  HlirStruct* = object
    name_node*: ref Node
    name*: Option[Names]
    type_params*: Option[seq[ref Node]]
    type_args*: Option[seq[ref Node]]
    fields*: seq[ref Node]
  HlirEnum* = object
    name_node*: ref Node
    name*: Option[Names]
    variants*: seq[ref Node]
  HlirEnumVariant* = object
    name_node*: ref Node
    name*: Option[Names]
    value*: Option[ref Node]
  HlirTagged* = object
    name_node*: ref Node
    name*: Option[Names]
    type_params*: Option[seq[ref Node]]
    type_args*: Option[seq[ref Node]]
    fields*: seq[ref Node]
    variants*: seq[ref Node]
  HlirTaggedVariant* = object
    name_node*: ref Node
    name*: Option[Names]
    fields*: seq[ref Node]
  HlirTypedef* = object
    name_node*: ref Node
    name*: Option[Names]
    ty*: ref Node
  HlirStatic* = object
    name_node*: ref Node
    name*: Option[Names]
    ty*: ref Node
    value*: Option[ref Node]
  HlirStaticExtern* = object
    lang*: Option[string]
    name_node*: ref Node
    name*: Option[Names]
    ty*: ref Node
  HlirDefine* = object
    name_node*: ref Node
    name*: Option[Names]
    ty*: Option[ref Node]
    value*: ref Node
  HlirFunction* = object
    name_node*: Option[ref Node]
    name*: Option[Names]
    type_params*: Option[seq[ref Node]]
    monomorphized*: bool
    ty*: ref Node
    capture*: Option[seq[ref Node]]
    is_closure*: bool
    body*: Option[ref Node]
  HlirMacro* = object
    name_node*: Option[ref Node]
    name*: Option[Names]
    type_params*: Option[seq[ref Node]]
    monomorphized*: bool
    ty*: ref Node
    body*: Option[ref Node]
  HlirWith* = object
    path*: Path

  HlirTypeType* = object
  HlirTypePointer* = object
    pointee*: ref Node
  HlirTypeArray* = object
    elem*: ref Node
    size*: ref Node
  HlirTypeSlice* = object
    elem*: ref Node
  HlirTypeVar* = object
    elem*: ref Node
  HlirTypeFunction* = object
    params*: seq[ref Node]
    result_type*: ref Node
  HlirTypeNever* = object
  HlirTypeUnit* = object
  HlirTypeBool* = object
  HlirTypeSigned* = object
    bits*: Bits
  HlirTypeUnsigned* = object
    bits*: Bits
  HlirTypeFloating* = object
    bits*: Bits

  HlirStmtIf* = object
    cond*: ref Node
    body*: ref Node
    elifs*: seq[tuple[cond: ref Node, body: ref Node]]
    el*: Option[ref Node]
  HlirStmtWhile* = object
    cond*: ref Node
    body*: ref Node
  HlirStmtDowhile* = object
    body*: ref Node
    cond*: ref Node
  HlirStmtFor* = object
    init*: Option[ref Node]
    cond*: Option[ref Node]
    rep*: Option[ref Node]
    body*: ref Node
  HlirStmtForeach* = object
    name_node*: ref Node
    name*: Option[Names]
    ty*: Option[ref Node]
    iter*: ref Node
    body*: ref Node
  HlirStmtMatch* = object
    value*: ref Node
    cases*: seq[ref Node]
  HlirExprTrait* = object
  HlirExprStruct* = object
  HlirExprUnion* = object
  HlirExprEnum* = object
  HlirExprTagged* = object
  HlirExprInterp* = object
    value*: ref Node
    quiet*: bool
  HlirExprCompound* = object
    elems*: seq[ref Node]
    last*: Option[ref Node]
  HlirExprParenth* = object
    elems*: seq[ref Node]
  HlirExprFail* = object
    value*: Option[ref Node]
  HlirExprDefer* = object
    value*: ref Node
  HlirExprReturn* = object
    value*: Option[ref Node]
  HlirExprContinue* = object
    label*: Option[ref Node]
  HlirExprBreak* = object
    label*: Option[ref Node]
  HlirExprLet* = object
    name_node*: ref Node
    name*: Option[Names]
    ty*: Option[ref Node]
    value*: Option[ref Node]
  HlirExprInt* = object
    value*: int64
    as_type*: Option[ref Node]
  HlirExprFloat* = object
    value*: float64
    as_type*: Option[ref Node]
  HlirExprBool* = object
    value*: bool
  HlirExprUnit* = object
  HlirExprString* = object
    value*: string
  HlirExprNamed* = object
    name*: Names
    type_args*: Option[seq[ref Node]]
    quiet*: bool
  HlirExprArray* = object
    elems*: seq[ref Node]
  HlirExprApplication* = object
    fn*: ref Node
    args*: seq[ref Node]
  HlirExprCommand* = object
    fn*: ref Node
    args*: seq[ref Node]
  HlirExprFieldAccess* = object
    value*: ref Node
    name*: string
  HlirExprCast* = object
    ty*: ref Node
    value*: ref Node
  HlirExprSizeof* = object
    value*: ref Node
  HlirExprAlignof* = object
    value*: ref Node
  HlirUnaryKind* = enum
    hukPostInc
    hukPostDec
    hukPreInc
    hukPreDec
    hukRef
    hukDeref
    hukPos
    hukNeg
    hukNot
    hukColon
    hukRangeFrom
    hukRangeTo
  HlirExprUnary* = object
    op*: HlirUnaryKind
    value*: ref Node
  HlirBinaryKind* = enum
    hbkIndex
    hbkAdd
    hbkSub
    hbkMul
    hbkDiv
    hbkRem
    hbkShl
    hbkShr
    hbkLt
    hbkGt
    hbkLe
    hbkGe
    hbkEq
    hbkNe
    hbkBitand
    hbkBitor
    hbkBitxor
    hbkLogicalAnd
    hbkLogicalOr
    hbkRange
    hbkAssign
    hbkAssignMul
    hbkAssignDiv
    hbkAssignRem
    hbkAssignAdd
    hbkAssignSub
    hbkAssignLshift
    hbkAssignRshift
    hbkAssignAnd
    hbkAssignXor
    hbkAssignOr
    hbkArrow
  HlirExprBinary* = object
    op*: HlirBinaryKind
    lhs*: ref Node
    rhs*: ref Node
  HlirTernaryKind* = enum
    htkCond
  HlirExprTernary* = object
    op*: HlirTernaryKind
    value*: ref Node
    lhs*: Option[ref Node]
    rhs*: Option[ref Node]
  HlirExprRangeFull* = object

  HlirPatBind* = object
    name_node*: ref Node
    name*: Option[Names]
    pat*: Option[ref Node]
  HlirPatDestructure* = object
    ty*: ref Node
    pats*: seq[ref Node]
  HlirPatElse* = object

  HlirDecl* = object
    name*: Option[ref Node]
    value*: ref Node
  HlirCase* = object
    pat*: ref Node
    body*: ref Node

iterator children*(self: ref Node): ref Node =
  case self.node.kind
  of hrkRoot:
    yield self.node.root.root
  of hrkSeq:
    for child in self.node.sequence.children:
      yield child
  of hrkNull: discard
  of hrkMod: discard
  of hrkUse: discard
  of hrkUseExtern: discard
  of hrkTrait:
    yield self.node.trait.name_node
    if self.node.trait.type_params.isSome:
      for child in self.node.trait.type_params.get():
        yield child
    if self.node.trait.type_args.isSome:
      for child in self.node.trait.type_args.get():
        yield child
    for child in self.node.trait.fns:
      yield child
  of hrkStruct:
    yield self.node.struct.name_node
    if self.node.struct.type_params.isSome:
      for child in self.node.struct.type_params.get():
        yield child
    if self.node.struct.type_args.isSome:
      for child in self.node.struct.type_args.get():
        yield child
    for child in self.node.struct.fields:
      yield child
  of hrkUnion:
    yield self.node.struct.name_node
    if self.node.struct.type_params.isSome:
      for child in self.node.struct.type_params.get():
        yield child
    if self.node.struct.type_args.isSome:
      for child in self.node.struct.type_args.get():
        yield child
    for child in self.node.struct.fields:
      yield child
  of hrkEnum:
    yield self.node.enumeration.name_node
    for child in self.node.enumeration.variants:
      yield child
  of hrkEnumVariant:
    yield self.node.enum_variant.name_node
    if self.node.enum_variant.value.isSome:
      yield self.node.enum_variant.value.get()
  of hrkTagged:
    yield self.node.tagged.name_node
    if self.node.tagged.type_params.isSome:
      for child in self.node.tagged.type_params.get():
        yield child
    if self.node.tagged.type_args.isSome:
      for child in self.node.tagged.type_args.get():
        yield child
    for child in self.node.tagged.fields:
      yield child
    for child in self.node.tagged.variants:
      yield child
  of hrkTaggedVariant:
    yield self.node.tagged_variant.name_node
    for child in self.node.tagged_variant.fields:
      yield child
  of hrkTypedef:
    yield self.node.typedef.name_node
    yield self.node.typedef.ty
  of hrkStatic:
    yield self.node.static_global.name_node
    yield self.node.static_global.ty
    if self.node.static_global.value.isSome:
      yield self.node.static_global.value.get()
  of hrkStaticExtern:
    yield self.node.static_extern.name_node
    yield self.node.static_extern.ty
  of hrkDefine:
    yield self.node.define.name_node
    if self.node.define.ty.isSome:
      yield self.node.define.ty.get
    yield self.node.define.value
  of hrkFunction:
    if self.node.function.name_node.isSome:
      yield self.node.function.name_node.get
    if self.node.function.type_params.isSome:
      for child in self.node.function.type_params.get():
        yield child
    yield self.node.function.ty
    if self.node.function.capture.isSome:
      for child in self.node.function.capture.get():
        yield child
    if self.node.function.body.isSome:
      yield self.node.function.body.get()
  of hrkMacro:
    if self.node.macrofn.name_node.isSome:
      yield self.node.macrofn.name_node.get
    if self.node.macrofn.type_params.isSome:
      for child in self.node.macrofn.type_params.get():
        yield child
    yield self.node.macrofn.ty
    if self.node.macrofn.body.isSome:
      yield self.node.macrofn.body.get()
  of hrkWith: discard
  of hrkTypeType: discard
  of hrkTypePointer:
    yield self.node.type_pointer.pointee
  of hrkTypeArray:
    yield self.node.type_array.elem
    yield self.node.type_array.size
  of hrkTypeSlice:
    yield self.node.type_slice.elem
  of hrkTypeVar:
    yield self.node.type_var.elem
  of hrkTypeFunction:
    for child in self.node.type_function.params:
      yield child
    yield self.node.type_function.result_type
  of hrkTypeNever: discard
  of hrkTypeUnit: discard
  of hrkTypeBool: discard
  of hrkTypeSigned: discard
  of hrkTypeUnsigned: discard
  of hrkTypeFloating: discard
  of hrkStmtIf:
    yield self.node.stmt_if.cond
    yield self.node.stmt_if.body
    for stmt in self.node.stmt_if.elifs:
      yield stmt.cond
      yield stmt.body
    if self.node.stmt_if.el.isSome:
      yield self.node.stmt_if.el.get
  of hrkStmtWhile:
    yield self.node.stmt_while.cond
    yield self.node.stmt_while.body
  of hrkStmtDowhile:
    yield self.node.stmt_dowhile.cond
    yield self.node.stmt_dowhile.body
  of hrkStmtFor:
    if self.node.stmt_for.init.isSome:
      yield self.node.stmt_for.init.get
    if self.node.stmt_for.cond.isSome:
      yield self.node.stmt_for.cond.get
    if self.node.stmt_for.rep.isSome:
      yield self.node.stmt_for.rep.get
    yield self.node.stmt_for.body
  of hrkStmtForeach:
    yield self.node.stmt_foreach.name_node
    if self.node.stmt_foreach.ty.isSome:
      yield self.node.stmt_foreach.ty.get
    yield self.node.stmt_foreach.iter
    yield self.node.stmt_foreach.body
  of hrkStmtMatch:
    yield self.node.stmt_match.value
    for case_of in self.node.stmt_match.cases:
      yield case_of
  of hrkExprTrait:
    discard
  of hrkExprStruct:
    discard
  of hrkExprUnion:
    discard
  of hrkExprEnum:
    discard
  of hrkExprTagged:
    discard
  of hrkExprInterp:
    yield self.node.expr_interp.value
  of hrkExprCompound:
    for elem in self.node.expr_compound.elems:
      yield elem
    if self.node.expr_compound.last.isSome:
      yield self.node.expr_compound.last.get
  of hrkExprParenth:
    for elem in self.node.expr_parenth.elems:
      yield elem
  of hrkExprFail:
    if self.node.expr_fail.value.isSome:
      yield self.node.expr_fail.value.get()
  of hrkExprDefer:
    yield self.node.expr_defer.value
  of hrkExprReturn:
    if self.node.expr_return.value.isSome:
      yield self.node.expr_return.value.get()
  of hrkExprContinue: discard
  of hrkExprBreak: discard
  of hrkExprLet:
    yield self.node.expr_let.name_node
    if self.node.expr_let.ty.isSome:
      yield self.node.expr_let.ty.get()
    if self.node.expr_let.value.isSome:
      yield self.node.expr_let.value.get()
  of hrkExprInt: discard
  of hrkExprFloat: discard
  of hrkExprBool: discard
  of hrkExprUnit: discard
  of hrkExprString: discard
  of hrkExprNamed:
    if self.node.expr_named.type_args.isSome:
      for child in self.node.expr_named.type_args.get:
        yield child
  of hrkExprArray:
    for child in self.node.expr_array.elems:
      yield child
  of hrkExprApplication:
    yield self.node.expr_application.fn
    for child in self.node.expr_application.args:
      yield child
  of hrkExprCommand:
    yield self.node.expr_command.fn
    for child in self.node.expr_command.args:
      yield child
  of hrkExprFieldAccess:
    yield self.node.expr_fieldaccess.value
  of hrkExprCast:
    yield self.node.expr_cast.ty
    yield self.node.expr_cast.value
  of hrkExprSizeof:
    yield self.node.expr_sizeof.value
  of hrkExprAlignof:
    yield self.node.expr_alignof.value
  of hrkExprUnary:
    yield self.node.expr_unary.value
  of hrkExprBinary:
    yield self.node.expr_binary.lhs
    yield self.node.expr_binary.rhs
  of hrkExprTernary:
    yield self.node.expr_ternary.value
    if self.node.expr_ternary.lhs.isSome:
      yield self.node.expr_ternary.lhs.get
    if self.node.expr_ternary.rhs.isSome:
      yield self.node.expr_ternary.rhs.get
  of hrkExprRangeFull:
    discard
  of hrkPatBind:
    yield self.node.pat_bind.name_node
    if self.node.pat_bind.pat.isSome:
      yield self.node.pat_bind.pat.get()
  of hrkPatDestructure:
    for child in self.node.pat_destructure.pats:
      yield child
  of hrkPatElse: discard
  of hrkDecl:
    if self.node.decl.name.isSome:
      yield self.node.decl.name.get
    yield self.node.decl.value
  of hrkCase:
    yield self.node.case_of.pat
    yield self.node.case_of.body

proc `or`*(lhs: FlowBreak, rhs: FlowBreak): FlowBreak =
  case lhs
  of fbUndefined: return rhs
  of fbNo:
    case rhs
    of fbYes: return fbYes
    of fbMaybe: return fbMaybe
    else: return fbNo
  of fbYes: return fbYes
  else:
    case rhs
    of fbUndefined: return lhs
    of fbYes: return fbYes
    of fbMaybe: return fbMaybe
    else: return fbNo

proc `and`*(lhs: FlowBreak, rhs: FlowBreak): FlowBreak =
  case lhs
  of fbUndefined: return rhs
  of fbNo:
    case rhs
    of fbNo: return fbNo
    else: return fbMaybe
  of fbYes:
    case rhs
    of fbYes: return fbYes
    else: return fbMaybe
  else:
    case rhs
    of fbUndefined: return lhs
    else: return fbMaybe

proc newMeta*(span: Span): Meta =
  var type_sum: ref TypeSum
  new type_sum
  type_sum[] = newTypeSum()

  var meta = Meta(
    span: span,
    call_site: none[ref Node](),
    polymorphic: none[ref Node](),
    attributes: none[seq[Attribute]](),
    type_sum: type_sum,
    own_type: none[ref QualifiedType](),
    value: newCgVar(),
  )
  return meta

proc newNode*(meta: Meta, hlir: Hlir): ref Node =
  var node: ref Node
  new node
  node.meta = meta
  node.node = hlir
  return node

proc to_hlir*(decl: tuple[param: string, ty: QualifiedType], span: Span): ref Node
proc to_hlir*(ty: QualifiedType, span: Span): ref Node
proc to_hlir*(sum: TypeSum, span: Span): ref Node =
  return sum.as_qualified_in_span(span)[].to_hlir(span)

proc to_hlir*(ty: QualifiedType, span: Span): ref Node =
  case ty.inner.kind
  of tykUnit:
    result = newNode(newMeta(span), Hlir(kind: hrkTypeUnit, type_unit: HlirTypeUnit()))
  of tykNever:
    result = newNode(newMeta(span), Hlir(kind: hrkTypeNever, type_never: HlirTypeNever()))
  of tykBool:
    result = newNode(newMeta(span), Hlir(kind: hrkTypeBool, type_bool: HlirTypeBool()))
  of tykSigned:
    result = newNode(newMeta(span), Hlir(kind: hrkTypeSigned, type_signed: HlirTypeSigned(bits: ty.inner.signed.bits)))
  of tykUnsigned:
    result = newNode(newMeta(span), Hlir(kind: hrkTypeUnsigned, type_unsigned: HlirTypeUnsigned(bits: ty.inner.unsigned.bits)))
  of tykFloating:
    result = newNode(newMeta(span), Hlir(kind: hrkTypeFloating, type_floating: HlirTypeFloating(bits: ty.inner.floating.bits)))
  of tykTrait:
    var args = newSeq[ref Node]()
    if ty.has_type_args:
      for arg in ty.type_args_named:
        args.add((arg[0], arg[1][]).to_hlir(span))
    let type_args = if args.len > 0: some(args) else: none[seq[ref Node]]()
    result = newNode(newMeta(span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: Names(short: ty.inner.trait.name, full: some(ty.inner.trait.name)), type_args: type_args)))
  of tykStruct:
    var args = newSeq[ref Node]()
    if ty.has_type_args:
      for arg in ty.type_args_named:
        args.add((arg[0], arg[1][]).to_hlir(span))
    let type_args = if args.len > 0: some(args) else: none[seq[ref Node]]()
    result = newNode(newMeta(span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: Names(short: ty.inner.struct.name.get, full: some(ty.inner.struct.name.get)), type_args: type_args)))
  of tykUnion:
    var args = newSeq[ref Node]()
    if ty.has_type_args:
      for arg in ty.type_args_named:
        args.add((arg[0], arg[1][]).to_hlir(span))
    let type_args = if args.len > 0: some(args) else: none[seq[ref Node]]()
    result = newNode(newMeta(span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: Names(short: ty.inner.union.name.get, full: some(ty.inner.union.name.get)), type_args: type_args)))
  of tykEnum:
    result = newNode(newMeta(span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: Names(short: ty.inner.enumeration.name, full: some(ty.inner.enumeration.name)))))
  of tykTagged:
    var args = newSeq[ref Node]()
    if ty.has_type_args:
      for arg in ty.type_args_named:
        args.add((arg[0], arg[1][]).to_hlir(span))
    let type_args = if args.len > 0: some(args) else: none[seq[ref Node]]()
    result = newNode(newMeta(span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: Names(short: ty.inner.tagged.name, full: some(ty.inner.tagged.name)), type_args: type_args)))
  of tykFunction:
    let result_type = ty.inner.function.result_type[].to_hlir(span)
    var params = newSeqOfCap[ref Node](ty.inner.function.params.len)
    for param in ty.inner.function.params:
      var name = none[ref Node]()
      if param.name.isSome:
        name = some(newNode(newMeta(span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: Names(short: param.name.get.toNonFullPath, full: some(param.name.get.toPath))))))
      let param = newNode(newMeta(span), Hlir(kind: hrkDecl, decl: HlirDecl(name: name, value: param.ty[].to_hlir(span))))
      params.add(param)
    result = newNode(newMeta(span), Hlir(kind: hrkTypeFunction, type_function: HlirTypeFunction(result_type: result_type, params: params)))
  of tykPointer:
    let pointee = ty.inner.point.pointee[].to_hlir(span)
    result = newNode(newMeta(span), Hlir(kind: hrkTypePointer, type_pointer: HlirTypePointer(pointee: pointee)))
  of tykSlice:
    let elem = ty.inner.slice.elem[].to_hlir(span)
    result = newNode(newMeta(span), Hlir(kind: hrkTypeSlice, type_slice: HlirTypeSlice(elem: elem)))
  of tykArray:
    let elem = ty.inner.arr.elem[].to_hlir(span)
    result = newNode(newMeta(span), Hlir(kind: hrkTypeArray, type_array: HlirTypeArray(elem: elem)))
  of tykVector:
    raise newIce("unimplemented")
  of tykVar:
    let elem = ty.inner.vararr.elem[].to_hlir(span)
    result = newNode(newMeta(span), Hlir(kind: hrkTypeVar, type_var: HlirTypeVar(elem: elem)))
  of tykParameter:
    result = newNode(newMeta(span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: Names(short: ty.inner.param.name.toNonFullPath, full: some(ty.inner.param.name.toPath)))))
  of tykLit:
    raise newIce("unimplemented")
  of tykType:
    result = newNode(newMeta(span), Hlir(kind: hrkTypeType, type_type: HlirTypeType()))
  result.meta.is_type = true
  var own_type: ref QualifiedType
  new own_type
  own_type[] = ty
  result.meta.own_type = some(own_type)

proc to_hlir*(decl: tuple[param: string, ty: QualifiedType], span: Span): ref Node =
  let name = newNode(newMeta(span), Hlir(kind: hrkExprNamed, expr_named: HlirExprNamed(name: Names(short: decl.param.toNonFullPath, full: some(decl.param.toPath)))))
  return newNode(newMeta(span), Hlir(kind: hrkDecl, decl: HlirDecl(name: some(name), value: decl.ty.to_hlir(span))))

proc to_type*(self: ref Node, ctx: TypeContext, type_params: Table[string, ref QualifiedType]): ref QualifiedType

proc to_type_sum*(self: ref Node, ctx: TypeContext, type_params: Table[string, ref QualifiedType]): ref TypeSum =
  return self.to_type(ctx, type_params).toTypeSumRef

proc to_type*(self: ref Node, ctx: TypeContext, type_params: Table[string, ref QualifiedType]): ref QualifiedType =
  if self.node.kind != hrkExprNamed and
      type_params.len == 0 and self.meta.own_type.isSome:
    return self.meta.own_type.get

  var ty: ref Type
  new ty

  case self.node.kind
  of hrkExprNamed:
    if self.node.expr_named.name.short.path.len == 1 and type_params.contains(self.node.expr_named.name.short.path[0]):
      return type_params[self.node.expr_named.name.short.path[0]]
    return ctx.get(self.node.expr_named.name.full.get)
  of hrkExprApplication:
    if self.meta.own_type.isSome:
      return self.meta.own_type.get
    else:
      raise newError(errkNotAType, self.meta.span, "while trying to convert from node to type")
  of hrkTypePointer:
    let pointee = self.node.type_pointer.pointee.to_type_sum(ctx, type_params)
    ty[] = Type(
      kind: tykPointer,
      point: TypePointer(
        pointee: pointee,
      ),
    )
  of hrkTypeArray:
    let elem = self.node.type_array.elem.to_type_sum(ctx, type_params)
    let size = self.node.type_array.size.to_type_sum(ctx, type_params)
    ty[] = Type(
      kind: tykArray,
      arr: TypeArray(
        elem: elem,
        size: size,
      ),
    )
  of hrkTypeSlice:
    let elem = self.node.type_slice.elem.to_type_sum(ctx, type_params)
    ty[] = Type(
      kind: tykSlice,
      slice: TypeSlice(
        elem: elem,
      ),
    )
  of hrkTypeVar:
    let elem = self.node.type_var.elem.to_type_sum(ctx, type_params)
    ty[] = Type(
      kind: tykVar,
      vararr: TypeVar(
        elem: elem,
      ),
    )
  of hrkTypeFunction:
    var
      result_type: ref TypeSum
      params = newSeq[Argument]()
    result_type = self.node.type_function.result_type.to_type_sum(ctx, type_params)
    for param in self.node.type_function.params:
      case param.node.kind
      of hrkDecl:
        if param.node.decl.name.isSome:
          params.add(Argument(name: some(param.node.decl.name.get.node.expr_named.name.short.path[^1]), ty: param.node.decl.value.to_type_sum(ctx, type_params)))
        else:
          params.add(Argument(ty: param.to_type_sum(ctx, type_params)))
      else:
        params.add(Argument(ty: param.to_type_sum(ctx, type_params)))
    ty[] = Type(
      kind: tykFunction,
      function: TypeFunction(
        result_type: result_type,
        params: params,
      ),
    )
  of hrkTypeType:
    ty[] = Type(kind: tykType, ty: TypeType())
  of hrkTypeNever:
    ty[] = Type(kind: tykNever, never: TypeNever())
  of hrkTypeUnit:
    ty[] = Type(kind: tykUnit, unit: TypeUnit())
  of hrkTypeBool:
    ty[] = Type(kind: tykBool, p: TypeBool())
  of hrkTypeSigned:
    ty[] = Type(kind: tykSigned, signed: TypeSigned(bits: self.node.type_signed.bits))
  of hrkTypeUnsigned:
    ty[] = Type(kind: tykUnsigned, unsigned: TypeUnsigned(bits: self.node.type_unsigned.bits))
  of hrkTypeFloating:
    ty[] = Type(kind: tykFloating, floating: TypeFloating(bits: self.node.type_floating.bits))
  of hrkExprInt:
    ty[] = Type(kind: tykLit, lit: TypeLiteral(kind: lkInt, ty: self.meta.type_sum, intval: self.node.expr_int.value.int))
  else:
    raise newError(errkNotAType, self.meta.span, "while trying to convert from node to type")

  result = newQualifiedType(ty)

proc to_type_sum*(self: ref Node, ctx: TypeContext): ref TypeSum =
  return self.to_type_sum(ctx, initTable[string, ref QualifiedType]())

proc to_type*(self: ref Node, ctx: TypeContext): ref QualifiedType =
  return self.to_type(ctx, initTable[string, ref QualifiedType]())

proc is_type*(node: ref Node): bool =
  return node.meta.is_type or node.node.kind in hrkTypeType .. hrkTypeFloating

proc is_type_decl*(node: ref Node): bool =
  return node.node.kind == hrkDecl and node.node.decl.value.node.kind == hrkTypeType

proc is_pattern*(node: ref Node): bool =
  return node.node.kind == hrkPatElse or
    node.node.kind == hrkPatBind or
    node.node.kind == hrkPatDestructure

proc is_pattern_or_match*(node: ref Node): bool =
  return node.node.kind == hrkCase or
    node.node.kind == hrkPatElse or
    node.node.kind == hrkPatBind or
    node.node.kind == hrkPatDestructure

proc is_else_pattern*(node: ref Node): bool =
  return node.node.kind == hrkPatElse or
    node.node.kind == hrkPatBind
