import options
import hlir
import ../ice
import ../error

type
  VisitResultKind* = enum
    vrkRecurse
    vrkNoRecurse
    vrkContinue
    vrkBreak
    vrkReturn
    vrkRepeat
  HlirVisitor* = object
    visit_seq*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_null*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_mod*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_use*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_use_extern*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_trait*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_struct*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_union*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_enum*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_enum_variant*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_tagged*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_tagged_variant*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_typedef*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_static*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_static_extern*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_define*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_function*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_macro*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_with*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_with_glob*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_type_type*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_type_pointer*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_type_array*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_type_slice*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_type_var*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_type_function*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_type_never*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_type_unit*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_type_bool*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_type_signed*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_type_unsigned*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_type_floating*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_stmt_if*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_stmt_while*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_stmt_dowhile*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_stmt_for*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_stmt_foreach*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_stmt_match*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_expr_trait*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_expr_struct*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_expr_union*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_expr_enum*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_expr_tagged*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_expr_interp*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_expr_compound*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_expr_parenth*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_expr_fail*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_expr_defer*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_expr_return*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_expr_continue*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_expr_break*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_expr_let*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_expr_int*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_expr_float*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_expr_bool*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_expr_unit*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_expr_string*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_expr_named*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_expr_array*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_expr_application*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_expr_command*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_expr_field_access*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_expr_cast*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_expr_sizeof*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_expr_alignof*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_expr_unary*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_expr_binary*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_expr_ternary*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_expr_range_full*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_pat_bind*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_pat_destructure*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_pat_else*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_decl*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_initializer*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]
    visit_case*: Option[proc (self: ref Node, parent: ref Node): VisitResultKind]

proc accept(self: ref Node, parent: ref Node, pre: HlirVisitor, post: HlirVisitor): VisitResultKind

template accept_this(name, visitor: untyped) =
  proc name(self: ref Node, parent: ref Node, pre: HlirVisitor, post: HlirVisitor): VisitResultKind =
    if pre.visitor.isSome:
      case pre.visitor.get()(self, parent)
      of vrkNoRecurse: discard
      of vrkContinue: return vrkContinue
      of vrkBreak: return vrkBreak
      of vrkRecurse:
        for child in self.children:
          case accept(child, self, pre, post)
          of vrkRecurse: discard
          of vrkNoRecurse: discard
          of vrkContinue: continue
          of vrkBreak: break
          of vrkReturn: return vrkReturn
          of vrkRepeat:
            raise newIce("unreachable")
      of vrkReturn: return vrkReturn
      of vrkRepeat: return accept(self, parent, pre, post)
    else:
      for child in self.children:
        case accept(child, self, pre, post)
        of vrkRecurse: discard
        of vrkNoRecurse: discard
        of vrkContinue: continue
        of vrkBreak: break
        of vrkReturn: return vrkReturn
        of vrkRepeat:
          raise newIce("unreachable")
    if post.visitor.isSome:
      case post.visitor.get()(self, parent)
      of vrkRecurse: return vrkRecurse
      of vrkNoRecurse: return vrkRecurse
      of vrkBreak: return vrkBreak
      of vrkContinue: return vrkRecurse
      of vrkReturn: return vrkReturn
      of vrkRepeat: return accept(self, parent, pre, post)
    return vrkRecurse

template accept_multi(name, visitor: untyped) =
  proc name(self: ref Node, parent: ref Node, pre: HlirVisitor, post: HlirVisitor): VisitResultKind =
    if pre.visitor.isSome:
      case pre.visitor.get()(self, parent)
      of vrkNoRecurse: discard
      of vrkContinue: return vrkContinue
      of vrkBreak: return vrkBreak
      of vrkRecurse:
        var errors = newErrors()
        for child in self.children:
          try:
            case accept(child, self, pre, post)
            of vrkRecurse: discard
            of vrkNoRecurse: discard
            of vrkContinue: continue
            of vrkBreak: break
            of vrkReturn: return vrkReturn
            of vrkRepeat:
              raise newIce("unreachable")
          except Error:
            errors.add((ref Error)(getCurrentException()))
          except Errors:
            errors.add((ref Errors)(getCurrentException()))
        if errors.len > 0:
          raise errors
      of vrkReturn: return vrkReturn
      of vrkRepeat: return accept(self, parent, pre, post)
    else:
      var errors = newErrors()
      for child in self.children:
        try:
          case accept(child, self, pre, post)
          of vrkRecurse: discard
          of vrkNoRecurse: discard
          of vrkContinue: continue
          of vrkBreak: break
          of vrkReturn: return vrkReturn
          of vrkRepeat:
            raise newIce("unreachable")
        except Error:
          errors.add((ref Error)(getCurrentException()))
        except Errors:
          errors.add((ref Errors)(getCurrentException()))
      if errors.len > 0:
        raise errors
    if post.visitor.isSome:
      case post.visitor.get()(self, parent)
      of vrkRecurse: return vrkRecurse
      of vrkNoRecurse: return vrkRecurse
      of vrkBreak: return vrkBreak
      of vrkContinue: return vrkContinue
      of vrkReturn: return vrkReturn
      of vrkRepeat: return accept(self, parent, pre, post)
    return vrkRecurse

accept_this(accept_seq, visit_seq)
accept_this(accept_null, visit_null)
accept_this(accept_mod, visit_mod)
accept_this(accept_use, visit_use)
accept_this(accept_useextern, visit_useextern)
accept_this(accept_trait, visit_trait)
accept_this(accept_struct, visit_struct)
accept_this(accept_union, visit_union)
accept_this(accept_enum, visit_enum)
accept_this(accept_enumvariant, visit_enumvariant)
accept_this(accept_tagged, visit_tagged)
accept_this(accept_taggedvariant, visit_taggedvariant)
accept_this(accept_typedef, visit_typedef)
accept_this(accept_static, visit_static)
accept_this(accept_static_extern, visit_static_extern)
accept_this(accept_define, visit_define)
accept_this(accept_function, visit_function)
accept_this(accept_macro, visit_macro)
accept_this(accept_with, visit_with)
accept_this(accept_type_type, visit_type_type)
accept_this(accept_type_pointer, visit_type_pointer)
accept_this(accept_type_array, visit_type_array)
accept_this(accept_type_slice, visit_type_slice)
accept_this(accept_type_var, visit_type_var)
accept_this(accept_type_function, visit_type_function)
accept_this(accept_type_never, visit_type_never)
accept_this(accept_type_unit, visit_type_unit)
accept_this(accept_type_bool, visit_type_bool)
accept_this(accept_type_signed, visit_type_signed)
accept_this(accept_type_unsigned, visit_type_unsigned)
accept_this(accept_type_floating, visit_type_floating)
accept_this(accept_stmt_if, visit_stmt_if)
accept_this(accept_stmt_while, visit_stmt_while)
accept_this(accept_stmt_dowhile, visit_stmt_dowhile)
accept_this(accept_stmt_for, visit_stmt_for)
accept_this(accept_stmt_foreach, visit_stmt_foreach)
accept_this(accept_stmt_match, visit_stmt_match)
accept_this(accept_expr_trait, visit_expr_trait)
accept_this(accept_expr_struct, visit_expr_struct)
accept_this(accept_expr_union, visit_expr_union)
accept_this(accept_expr_enum, visit_expr_enum)
accept_this(accept_expr_tagged, visit_expr_tagged)
accept_this(accept_expr_interp, visit_expr_interp)
accept_multi(accept_expr_compound, visit_expr_compound)
accept_multi(accept_expr_parenth, visit_expr_parenth)
accept_this(accept_expr_fail, visit_expr_fail)
accept_this(accept_expr_defer, visit_expr_defer)
accept_this(accept_expr_return, visit_expr_return)
accept_this(accept_expr_continue, visit_expr_continue)
accept_this(accept_expr_break, visit_expr_break)
accept_this(accept_expr_let, visit_expr_let)
accept_this(accept_expr_int, visit_expr_int)
accept_this(accept_expr_float, visit_expr_float)
accept_this(accept_expr_bool, visit_expr_bool)
accept_this(accept_expr_unit, visit_expr_unit)
accept_this(accept_expr_string, visit_expr_string)
accept_this(accept_expr_named, visit_expr_named)
accept_this(accept_expr_array, visit_expr_array)
accept_this(accept_expr_application, visit_expr_application)
accept_this(accept_expr_command, visit_expr_command)
accept_this(accept_expr_fieldaccess, visit_expr_fieldaccess)
accept_this(accept_expr_cast, visit_expr_cast)
accept_this(accept_expr_sizeof, visit_expr_sizeof)
accept_this(accept_expr_alignof, visit_expr_alignof)
accept_this(accept_expr_unary, visit_expr_unary)
accept_this(accept_expr_binary, visit_expr_binary)
accept_this(accept_expr_ternary, visit_expr_ternary)
accept_this(accept_expr_range_full, visit_expr_range_full)
accept_this(accept_pat_bind, visit_pat_bind)
accept_this(accept_pat_destructure, visit_pat_destructure)
accept_this(accept_pat_else, visit_pat_else)
accept_this(accept_decl, visit_decl)
accept_this(accept_case, visit_case)

proc accept*(root: ref Node, pre: HlirVisitor, post: HlirVisitor): VisitResultKind =
  return accept(root, root, pre, post)

proc accept(self: ref Node, parent: ref Node, pre: HlirVisitor, post: HlirVisitor): VisitResultKind =
  if self.meta.dont_visit > 0:
    dec self.meta.dont_visit
    return vrkNoRecurse
  case self.node.kind
  of hrkRoot: return accept(self.node.root.root, parent, pre, post)
  of hrkSeq: return accept_seq(self, parent, pre, post)
  of hrkNull: return accept_null(self, parent, pre, post)
  of hrkMod: return accept_mod(self, parent, pre, post)
  of hrkUse: return accept_use(self, parent, pre, post)
  of hrkUseExtern: return accept_useextern(self, parent, pre, post)
  of hrkTrait: return accept_trait(self, parent, pre, post)
  of hrkStruct: return accept_struct(self, parent, pre, post)
  of hrkUnion: return accept_union(self, parent, pre, post)
  of hrkEnum: return accept_enum(self, parent, pre, post)
  of hrkEnumVariant: return accept_enumvariant(self, parent, pre, post)
  of hrkTagged: return accept_tagged(self, parent, pre, post)
  of hrkTaggedVariant: return accept_taggedvariant(self, parent, pre, post)
  of hrkTypedef: return accept_typedef(self, parent, pre, post)
  of hrkStatic: return accept_static(self, parent, pre, post)
  of hrkStaticExtern: return accept_static_extern(self, parent, pre, post)
  of hrkDefine: return accept_define(self, parent, pre, post)
  of hrkFunction: return accept_function(self, parent, pre, post)
  of hrkMacro: return accept_macro(self, parent, pre, post)
  of hrkWith: return accept_with(self, parent, pre, post)
  of hrkTypeType: return accept_type_type(self, parent, pre, post)
  of hrkTypePointer: return accept_type_pointer(self, parent, pre, post)
  of hrkTypeArray: return accept_type_array(self, parent, pre, post)
  of hrkTypeSlice: return accept_type_slice(self, parent, pre, post)
  of hrkTypeVar: return accept_type_var(self, parent, pre, post)
  of hrkTypeFunction: return accept_type_function(self, parent, pre, post)
  of hrkTypeNever: return accept_type_never(self, parent, pre, post)
  of hrkTypeUnit: return accept_type_unit(self, parent, pre, post)
  of hrkTypeBool: return accept_type_bool(self, parent, pre, post)
  of hrkTypeSigned: return accept_type_signed(self, parent, pre, post)
  of hrkTypeUnsigned: return accept_type_unsigned(self, parent, pre, post)
  of hrkTypeFloating: return accept_type_floating(self, parent, pre, post)
  of hrkStmtIf: return accept_stmt_if(self, parent, pre, post)
  of hrkStmtWhile: return accept_stmt_while(self, parent, pre, post)
  of hrkStmtDowhile: return accept_stmt_dowhile(self, parent, pre, post)
  of hrkStmtFor: return accept_stmt_for(self, parent, pre, post)
  of hrkStmtForeach: return accept_stmt_foreach(self, parent, pre, post)
  of hrkStmtMatch: return accept_stmt_match(self, parent, pre, post)
  of hrkExprTrait: return accept_expr_trait(self, parent, pre, post)
  of hrkExprStruct: return accept_expr_struct(self, parent, pre, post)
  of hrkExprUnion: return accept_expr_union(self, parent, pre, post)
  of hrkExprEnum: return accept_expr_enum(self, parent, pre, post)
  of hrkExprTagged: return accept_expr_tagged(self, parent, pre, post)
  of hrkExprInterp: return accept_expr_interp(self, parent, pre, post)
  of hrkExprCompound: return accept_expr_compound(self, parent, pre, post)
  of hrkExprParenth: return accept_expr_parenth(self, parent, pre, post)
  of hrkExprFail: return accept_expr_fail(self, parent, pre, post)
  of hrkExprDefer: return accept_expr_defer(self, parent, pre, post)
  of hrkExprReturn: return accept_expr_return(self, parent, pre, post)
  of hrkExprContinue: return accept_expr_continue(self, parent, pre, post)
  of hrkExprBreak: return accept_expr_break(self, parent, pre, post)
  of hrkExprLet: return accept_expr_let(self, parent, pre, post)
  of hrkExprInt: return accept_expr_int(self, parent, pre, post)
  of hrkExprFloat: return accept_expr_float(self, parent, pre, post)
  of hrkExprBool: return accept_expr_bool(self, parent, pre, post)
  of hrkExprUnit: return accept_expr_unit(self, parent, pre, post)
  of hrkExprString: return accept_expr_string(self, parent, pre, post)
  of hrkExprNamed: return accept_expr_named(self, parent, pre, post)
  of hrkExprArray: return accept_expr_array(self, parent, pre, post)
  of hrkExprApplication: return accept_expr_application(self, parent, pre, post)
  of hrkExprCommand: return accept_expr_command(self, parent, pre, post)
  of hrkExprFieldAccess: return accept_expr_fieldaccess(self, parent, pre, post)
  of hrkExprCast: return accept_expr_cast(self, parent, pre, post)
  of hrkExprSizeof: return accept_expr_sizeof(self, parent, pre, post)
  of hrkExprAlignof: return accept_expr_alignof(self, parent, pre, post)
  of hrkExprUnary: return accept_expr_unary(self, parent, pre, post)
  of hrkExprBinary: return accept_expr_binary(self, parent, pre, post)
  of hrkExprTernary: return accept_expr_ternary(self, parent, pre, post)
  of hrkExprRangeFull: return accept_expr_range_full(self, parent, pre, post)
  of hrkPatBind: return accept_pat_bind(self, parent, pre, post)
  of hrkPatDestructure: return accept_pat_destructure(self, parent, pre, post)
  of hrkPatElse: return accept_pat_else(self, parent, pre, post)
  of hrkDecl: return accept_decl(self, parent, pre, post)
  of hrkCase: return accept_case(self, parent, pre, post)
