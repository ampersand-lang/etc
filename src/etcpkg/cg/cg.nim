import options
import tables
import strformat
import sequtils
import sugar
import ../version
import ../ice
import ../error
import ../target
import ../span
import ../path
import ../bits
import ../names
import ../index
import ../print
import ../driver/driver_typedefs
import ../driver/driver
import ../types/types
import ../hlir/hlir
import ../hlir/visitor
import ../llvm/llvm
import ../llvm/clang
import cgvar

proc code_gen*(index: ref Index, driver: ref Driver, root: ref Node)

proc compile_ctype_named(ty: ref QualifiedType, name: string, span: Span, cg: CgPassController, target: ref Target): llvm.TypeRef
proc try_to_first_class_cg_ctype_with_span(ty: ref QualifiedType, span: Span, cg: CgPassController, target: ref Target): llvm.TypeRef
proc di_qualified_type(ty: ref QualifiedType, span: Span, cg: CgPassController, target: ref Target): llvm.MetadataRef
proc di_qualified_type_first_class(ty: ref QualifiedType, span: Span, cg: CgPassController, target: ref Target): llvm.MetadataRef

proc has_function(cg: CgPassController): bool = cg.function_stack.len != 0

proc has_bb(cg: CgPassController): bool = cg.bb_current.isSome
proc bb(cg: CgPassController): llvm.BasicBlockRef = cg.bb_current.get
proc `bb=`(cg: CgPassController, bb: llvm.BasicBlockRef) = cg.bb_current = some(bb)

proc bb_stack_empty(cg: CgPassController): bool = cg.bb_stack.len == 0 or cg.bb_stack[^1].len == 0

proc peek_bb(cg: CgPassController): llvm.BasicBlockRef = cg.bb_stack[^1][^1]

proc push_bb(cg: CgPassController) =
  cg.bb_stack[^1].add(cg.bb)

proc pop_bb(cg: CgPassController) =
  cg.bb_current = some(cg.bb_stack[^1].pop())

proc push_bb_frame(cg: CgPassController) =
  if cg.bb_stack.len > 0:
    cg.push_bb
  cg.bb_stack.add(newSeq[llvm.BasicBlockRef]())

proc pop_bb_frame(cg: CgPassController) =
  discard cg.bb_stack.pop
  if cg.bb_stack.len > 0:
    cg.pop_bb

proc add_loop(cg: CgPassController, continue_target, break_target: llvm.BasicBlockRef) =
  cg.continue_target[^1].add(continue_target)
  cg.break_target[^1].add(break_target)

proc pop_loop(cg: CgPassController) =
  discard cg.continue_target[^1].pop()
  discard cg.break_target[^1].pop()

proc get_continue_target(cg: CgPassController): Option[llvm.BasicBlockRef] =
  if cg.continue_target.len > 0 and cg.continue_target[^1].len > 0:
    return some(cg.continue_target[^1][^1])

proc get_break_target(cg: CgPassController): Option[llvm.BasicBlockRef] =
  if cg.break_target.len > 0 and cg.break_target[^1].len > 0:
    return some(cg.break_target[^1][^1])

proc function(cg: CgPassController): llvm.ValueRef = cg.function_stack[^1]
proc get_di_function(cg: CgPassController): Option[llvm.MetadataRef] = cg.di_function_stack[^1]
proc di_scope(cg: CgPassController): Option[llvm.MetadataRef] =
  if cg.di_function_stack.len > 0 and cg.get_di_function.isSome:
    return cg.get_di_function
  else:
    return cg.di_file

proc get_result_var(cg: CgPassController): ref CgVar = cg.result_var[^1]
proc get_early_var(cg: CgPassController): ref CgVar = cg.early_var[^1]
proc get_return_bb(cg: CgPassController): llvm.BasicBlockRef = cg.return_bb[^1][0]

proc get_defer_bb(cg: CgPassController): llvm.BasicBlockRef = cg.return_bb[^1][^2]
proc get_end_bb(cg: CgPassController): llvm.BasicBlockRef = cg.return_bb[^1][^1]

proc enter_di_function(cg: CgPassController, fn: Option[llvm.MetadataRef]) =
  cg.di_function_stack.add(fn)

proc leave_di_function(cg: CgPassController) =
  discard cg.di_function_stack.pop()

proc enter_declare(cg: CgPassController, fn: llvm.ValueRef) =
  cg.types.add(initTable[string, llvm.TypeRef]())
  cg.bindings.add(initTable[string, ref CgVar]())
  cg.function_stack.add(fn)

proc enter_this_declare(cg: CgPassController, fn: llvm.ValueRef, bindings_scope: Table[string, ref CgVar], types_scope: Table[string, llvm.TypeRef]) =
  cg.types.add(types_scope)
  cg.bindings.add(bindings_scope)
  cg.function_stack.add(fn)

proc leave_declare(cg: CgPassController) =
  discard cg.function_stack.pop()
  discard cg.bindings.pop()
  discard cg.types.pop()

proc leave_this_declare(cg: CgPassController): tuple[bindings_scope: Table[string, ref CgVar], types_scope: Table[string, llvm.TypeRef]] =
  discard cg.function_stack.pop()
  return (cg.bindings.pop(), cg.types.pop())

proc enter_new_fn(cg: CgPassController, fn: llvm.ValueRef, bindings_scope: Table[string, ref CgVar], types_scope: Table[string, llvm.TypeRef]) =
  cg.push_bb_frame
  cg.function_stack.add(fn)
  cg.types.add(types_scope)
  cg.bindings.add(bindings_scope)
  cg.return_bb.add(newSeq[llvm.BasicBlockRef]())
  cg.defer_count.add(newSeq[int]())
  cg.continue_target.add(newSeq[llvm.BasicBlockRef]())
  cg.break_target.add(newSeq[llvm.BasicBlockRef]())

  cg.bb = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring(""))
  llvm.positionBuilderAtEnd(cg.builder, cg.bb)
  llvm.setCurrentDebugLocation(cg.builder, nil)

  var
    result_var: ref CgVar
    early_var: ref CgVar
  new result_var
  new early_var
  # need to call getPointeeType, because typeOf returns something of the form void ()*
  let
    fn_type = llvm.getElementType(llvm.typeOf(fn))
    ret_type = llvm.getReturnType(fn_type)
  if llvm.getTypeKind(ret_type) == llvm.VoidTypeKind:
    result_var[] = initCgUnit()
  else:
    result_var[] = initCgPointer(llvm.buildAlloca(cg.builder, ret_type, cstring("result")))
  early_var[] = initCgPointer(llvm.buildAlloca(cg.builder, llvm.intTypeInContext(cg.ctx, 8), cstring("early")))
  llvm.buildStore(cg.builder, llvm.constInt(llvm.intTypeInContext(cg.ctx, 8), 0, 0), early_var.value)
  cg.result_var.add(result_var)
  cg.early_var.add(early_var)

  let ret = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring("return"))
  cg.return_bb[^1].add(ret)
  llvm.positionBuilderAtEnd(cg.builder, ret)
  if llvm.getTypeKind(ret_type) == llvm.VoidTypeKind:
    discard llvm.buildRetVoid(cg.builder)
  else:
    let result_value = llvm.buildLoad(cg.builder, result_var.value, cstring(""))
    discard llvm.buildRet(cg.builder, result_value)

  llvm.positionBuilderAtEnd(cg.builder, cg.bb)

proc enter_fn(cg: CgPassController, fn: llvm.ValueRef) =
  cg.push_bb_frame
  cg.function_stack.add(fn)
  cg.types.add(initTable[string, llvm.TypeRef]())
  cg.bindings.add(initTable[string, ref CgVar]())
  cg.return_bb.add(newSeq[llvm.BasicBlockRef]())
  cg.defer_count.add(newSeq[int]())
  cg.continue_target.add(newSeq[llvm.BasicBlockRef]())
  cg.break_target.add(newSeq[llvm.BasicBlockRef]())

  llvm.setCurrentDebugLocation(cg.builder, nil)

  cg.bb = llvm.getFirstBasicBlock(cg.function)
  var
    result_var: ref CgVar
    early_var: ref CgVar
  new result_var
  new early_var
  let
    fn_type = llvm.getElementType(llvm.typeOf(fn))
    ret_type = llvm.getReturnType(fn_type)
  if llvm.getTypeKind(ret_type) == llvm.VoidTypeKind:
    result_var[] = initCgUnit()
    early_var[] = initCgPointer(llvm.getFirstInstruction(cg.bb))
  else:
    result_var[] = initCgPointer(llvm.getFirstInstruction(cg.bb))
    early_var[] = initCgPointer(llvm.getNextInstruction(result_var.value))
  cg.result_var.add(result_var)
  cg.early_var.add(early_var)
  cg.return_bb[^1].add(llvm.getNextBasicBlock(cg.bb))
  llvm.positionBuilderAtEnd(cg.builder, cg.bb)

proc enter_this_fn(cg: CgPassController, fn: llvm.ValueRef, bindings_scope: Table[string, ref CgVar], types_scope: Table[string, llvm.TypeRef]) =
  cg.push_bb_frame
  cg.function_stack.add(fn)
  cg.types.add(types_scope)
  cg.bindings.add(bindings_scope)
  cg.return_bb.add(newSeq[llvm.BasicBlockRef]())
  cg.defer_count.add(newSeq[int]())
  cg.continue_target.add(newSeq[llvm.BasicBlockRef]())
  cg.break_target.add(newSeq[llvm.BasicBlockRef]())

  llvm.setCurrentDebugLocation(cg.builder, nil)

  cg.bb = llvm.getFirstBasicBlock(cg.function)
  var
    result_var: ref CgVar
    early_var: ref CgVar
  new result_var
  new early_var
  let
    fn_type = llvm.getElementType(llvm.typeOf(fn))
    ret_type = llvm.getReturnType(fn_type)
  if llvm.getTypeKind(ret_type) == llvm.VoidTypeKind:
    result_var[] = initCgUnit()
    early_var[] = initCgPointer(llvm.getFirstInstruction(cg.bb))
  else:
    result_var[] = initCgPointer(llvm.getFirstInstruction(cg.bb))
    early_var[] = initCgPointer(llvm.getNextInstruction(result_var.value))
  cg.result_var.add(result_var)
  cg.early_var.add(early_var)
  cg.return_bb[^1].add(llvm.getNextBasicBlock(cg.bb))
  llvm.positionBuilderAtEnd(cg.builder, cg.bb)

proc leave_fn(cg: CgPassController) =
  llvm.positionBuilderAtEnd(cg.builder, nil)
  cg.bb_current = none[llvm.BasicBlockRef]()
  cg.pop_bb_frame
  discard cg.function_stack.pop()
  discard cg.result_var.pop()
  discard cg.early_var.pop()
  discard cg.bindings.pop()
  discard cg.types.pop()
  discard cg.return_bb.pop()
  discard cg.defer_count.pop()
  discard cg.continue_target.pop()
  discard cg.break_target.pop()

proc leave_this_fn(cg: CgPassController): tuple[bindings_scope: Table[string, ref CgVar], types_scope: Table[string, llvm.TypeRef]] =
  llvm.positionBuilderAtEnd(cg.builder, nil)
  cg.bb_current = none[llvm.BasicBlockRef]()
  cg.pop_bb_frame
  discard cg.function_stack.pop()
  discard cg.result_var.pop()
  discard cg.early_var.pop()
  let bindings = cg.bindings.pop()
  let types = cg.types.pop()
  discard cg.return_bb.pop()
  discard cg.defer_count.pop()
  discard cg.continue_target.pop()
  discard cg.break_target.pop()
  return (bindings, types)

proc add_pattern(cg: CgPassController, pat: tuple[value: ref CgVar, ty: ref QualifiedType, is_switch: bool, variant: int, variant_name: Option[string]]) = cg.pattern_stack.add(pat)
proc add_switch(cg: CgPassController, switch: ref CgVar) = cg.switch_stack.add(switch)

proc pattern(cg: CgPassController): tuple[value: ref CgVar, ty: ref QualifiedType, is_switch: bool, variant: int, variant_name: Option[string]] = cg.pattern_stack[^1]
proc switch(cg: CgPassController): ref CgVar = cg.switch_stack[^1]

proc pop_pattern(cg: CgPassController) = discard cg.pattern_stack.pop()
proc pop_switch(cg: CgPassController) = discard cg.switch_stack.pop()

proc enter_ctx(cg: CgPassController) =
  cg.types.add(initTable[string, llvm.TypeRef]())
  cg.bindings.add(initTable[string, ref CgVar]())

proc leave_ctx(cg: CgPassController) =
  discard cg.bindings.pop()
  discard cg.types.pop()

proc enter(cg: CgPassController) =
  cg.types.add(initTable[string, llvm.TypeRef]())
  cg.bindings.add(initTable[string, ref CgVar]())
  cg.defer_count[^1].add(0)

  cg.bb = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring(""))
  discard llvm.buildBr(cg.builder, cg.bb)
  llvm.positionBuilderAtEnd(cg.builder, cg.bb)

  let ret = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring(""))
  cg.return_bb[^1].add(ret)

proc add_defer(cg: CgPassController) =
  let def = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring(""))
  llvm.positionBuilderAtEnd(cg.builder, cg.get_end_bb)
  let br = llvm.buildBr(cg.builder, cg.get_defer_bb)
  llvm.positionBuilderAtEnd(cg.builder, def)
  cg.return_bb[^1].add(def)
  cg.bb = def
  inc cg.defer_count[^1][^1]

proc leave(cg: CgPassController) =
  discard cg.bindings.pop()
  discard cg.types.pop()
  for _ in 0 .. cg.defer_count[^1][^1]:
    discard cg.return_bb[^1].pop()
  discard cg.defer_count[^1].pop()

proc bind_var(cg: CgPassController, name: string, variable: ref CgVar) =
  if cg.bindings[^1].contains(name):
    cg.bindings[^1][name] = variable
  else:
    cg.bindings[^1].add(name, variable)

proc bind_var(cg: CgPassController, name: string, variable: ref CgVar, at: int) =
  if cg.bindings[at].contains(name):
    cg.bindings[at][name] = variable
  else:
    cg.bindings[at].add(name, variable)

proc get_var(cg: CgPassController, name: string): Option[ref CgVar] =
  for i in 0 ..< cg.bindings.len:
    let i = cg.bindings.len - i - 1
    if cg.bindings[i].contains(name):
      return some(cg.bindings[i][name])
  return none[ref CgVar]()

proc bind_type*(cg: CgPassController, name: string, ty: llvm.TypeRef) =
  if cg.types[^1].contains(name):
    cg.types[^1][name] = ty
  else:
    cg.types[^1].add(name, ty)

proc get_type*(cg: CgPassController, name: string): Option[llvm.TypeRef] =
  for i in 0 ..< cg.types.len:
    let i = cg.types.len - i - 1
    if cg.types[i].contains(name):
      return some(cg.types[i][name])
  return none[llvm.TypeRef]()

proc to_first_class(ty: llvm.TypeRef): llvm.TypeRef =
  case llvm.getTypeKind(ty)
  of llvm.FunctionTypeKind: return llvm.pointerType(ty, 0)
  else: return ty

proc try_to_first_class_cg_ctype_with_span(ty: ref QualifiedType, span: Span, cg: CgPassController, target: ref Target): llvm.TypeRef =
  case ty.inner.kind
  of tykType:
    return nil
  of tykNever:
    return llvm.voidTypeInContext(cg.ctx)
  of tykUnit:
    return llvm.voidTypeInContext(cg.ctx)
  of tykBool:
    return llvm.intTypeInContext(cg.ctx, 1)
  of tykSigned:
    case ty.inner.signed.bits.kind
    of bitsWord:
      return llvm.intTypeInContext(cg.ctx, cuint(target.pointer_width))
    of bitsCustom:
      return llvm.intTypeInContext(cg.ctx, cuint(ty.inner.signed.bits.bits))
  of tykUnsigned:
    case ty.inner.unsigned.bits.kind
    of bitsWord:
      return llvm.intTypeInContext(cg.ctx, cuint(target.pointer_width))
    of bitsCustom:
      return llvm.intTypeInContext(cg.ctx, cuint(ty.inner.unsigned.bits.bits))
  of tykFloating:
    case ty.inner.floating.bits.kind
    of bitsWord:
      case target.pointer_width
      of 32:
        return llvm.floatTypeInContext(cg.ctx)
      of 64:
        return llvm.doubleTypeInContext(cg.ctx)
      else:
        return nil
    of bitsCustom:
      case ty.inner.floating.bits.bits
      of 16:
        return llvm.halfTypeInContext(cg.ctx)
      of 32:
        return llvm.floatTypeInContext(cg.ctx)
      of 64:
        return llvm.doubleTypeInContext(cg.ctx)
      of 80:
        return llvm.x86fp80TypeInContext(cg.ctx)
      of 128:
        return llvm.fp128TypeInContext(cg.ctx)
      else:
        return nil
  of tykTrait:
    let
      u8 = llvm.intTypeInContext(cg.ctx, 8)
      unit_ptr = llvm.pointerType(u8, 0)
      unitp_ptr = llvm.pointerType(unit_ptr, 0)
    
    var fields = [unit_ptr, unitp_ptr]
    let struct_type = llvm.structTypeInContext(cg.ctx, addr fields[0], cuint(fields.len), 0)
    return struct_type
  of tykStruct:
    let ty = cg.get_type(ty.mangled)
    if ty.isSome:
      return ty.get
    else:
      return nil
  of tykUnion:
    let ty = cg.get_type(ty.mangled)
    if ty.isSome:
      return ty.get
    else:
      return nil
  of tykFunction:
    var params = newSeqOfCap[llvm.TypeRef](ty.inner.function.params.len)
    for param in ty.inner.function.params:
      let t = param.ty[].as_qualified_in_span(span)
      if t.inner.kind != tykUnit:
        var param = t.try_to_first_class_cg_ctype_with_span(span, cg, target)
        if param.isNil:
          param = t.compile_ctype_named(t[].name.mangled_as_type, span, cg, target)
        params.add(param)
    let param_ptr = if params.len == 0: nil else: addr params[0]
    var result_type = ty.inner.function.result_type[].as_qualified_in_span(span).try_to_first_class_cg_ctype_with_span(span, cg, target)
    if result_type.isNil:
      result_type = ty.inner.function.result_type[].as_qualified_in_span(span).compile_ctype_named(ty.inner.function.result_type[].as_qualified[].name.mangled_as_type, span, cg, target)
    let variadic = if ty.inner.function.variadic: 1 else: 0
    return llvm.pointerType(llvm.functionType(result_type, param_ptr, cuint(params.len), variadic.cint), 0)
  of tykPointer:
    let qpointee = ty.inner.point.pointee[].as_qualified_in_span(span)
    var pointee = qpointee.try_to_first_class_cg_ctype_with_span(span, cg, target)
    if pointee.isNil:
      pointee = qpointee.compile_ctype_named(qpointee[].name.path[^1], span, cg, target)
    # llvm has a complicated relationship with void pointers
    if llvm.getTypeKind(pointee) == llvm.VoidTypeKind:
      let u8 = llvm.intTypeInContext(cg.ctx, 8)
      result = llvm.pointerType(u8, 0)
    else:
      result = llvm.pointerType(pointee, 0)
  else:
    return nil

proc to_first_class_cg_type_with_span(ty: ref QualifiedType, span: Span, cg: CgPassController, target: ref Target): llvm.TypeRef =
  case ty.inner.kind
  of tykType:
    raise newError(errkInvalidType, span, "type is a meta-type")
  of tykNever:
    return llvm.voidTypeInContext(cg.ctx)
  of tykUnit:
    return llvm.voidTypeInContext(cg.ctx)
  of tykBool:
    return llvm.intTypeInContext(cg.ctx, 1)
  of tykSigned:
    case ty.inner.signed.bits.kind
    of bitsWord:
      return llvm.intTypeInContext(cg.ctx, cuint(target.pointer_width))
    of bitsCustom:
      return llvm.intTypeInContext(cg.ctx, cuint(ty.inner.signed.bits.bits))
  of tykUnsigned:
    case ty.inner.unsigned.bits.kind
    of bitsWord:
      return llvm.intTypeInContext(cg.ctx, cuint(target.pointer_width))
    of bitsCustom:
      return llvm.intTypeInContext(cg.ctx, cuint(ty.inner.unsigned.bits.bits))
  of tykFloating:
    case ty.inner.floating.bits.kind
    of bitsWord:
      case target.pointer_width
      of 32:
        return llvm.floatTypeInContext(cg.ctx)
      of 64:
        return llvm.doubleTypeInContext(cg.ctx)
      else:
        raise newError(errkInvalidType, span, "invalid floating type width")
    of bitsCustom:
      case ty.inner.floating.bits.bits
      of 16:
        return llvm.halfTypeInContext(cg.ctx)
      of 32:
        return llvm.floatTypeInContext(cg.ctx)
      of 64:
        return llvm.doubleTypeInContext(cg.ctx)
      of 80:
        return llvm.x86fp80TypeInContext(cg.ctx)
      of 128:
        return llvm.fp128TypeInContext(cg.ctx)
      else:
        raise newError(errkInvalidType, span, "invalid floating type width")
  of tykTrait:
    let
      u8 = llvm.intTypeInContext(cg.ctx, 8)
      unit_ptr = llvm.pointerType(u8, 0)
      unitp_ptr = llvm.pointerType(unit_ptr, 0)
    
    var fields = [unit_ptr, unitp_ptr]
    let struct_type = llvm.structTypeInContext(cg.ctx, addr fields[0], cuint(fields.len), 0)
    return struct_type
  of tykStruct:
    let name = ty.mangled
    let lty = cg.get_type(name)
    if lty.isSome:
      return lty.get
    else:
      let struct_type = llvm.structCreateNamed(cg.ctx, cstring(name))
      cg.bind_type(name, struct_type)

      var fields = newSeq[llvm.TypeRef]()
      for field in ty.inner.struct.fields:
        let qfield = field.ty[].as_qualified_in_span(span)
        let field = qfield.to_first_class_cg_type_with_span(span, cg, target)
        fields.add(field)
      let fieldv = if fields.len == 0: nil else: addr fields[0]
      llvm.structSetBody(struct_type, fieldv, cuint(fields.len), 0)
      return struct_type
  of tykUnion:
    let lty = cg.get_type(ty.mangled)
    if lty.isSome:
      return lty.get
    else:
      var
        size: uint
      let
        s = ty.inner.get_size_of(cg.type_ctx)

      case s.kind
      of sokUnknown:
        raise newError(errkInvalidType, span, "unknown size")
      of sokUnsized:
        raise newError(errkInvalidType, span, "unsized")
      of sokSized:
        size = s.size

      let u8 = llvm.intTypeInContext(cg.ctx, 8)
      let union_type = llvm.arrayType(u8, cuint(size))
      cg.bind_type(ty.mangled, union_type)
      return union_type
  of tykEnum:
    let lty = cg.get_type(ty.mangled)
    if lty.isSome:
      return lty.get
    else:
      let inner = ty
        .inner
        .enumeration
        .inner
        .to_first_class_cg_type_with_span(span, cg, target)
      cg.bind_type(ty.mangled, inner)
      return inner
  of tykTagged:
    let lty = cg.get_type(ty.mangled)
    if lty.isSome:
      return lty.get
    else:
      let name = ty.mangled
      let tagged_type = llvm.structCreateNamed(cg.ctx, cstring(name))
      cg.bind_type(name, tagged_type)

      for variant in ty.inner.tagged.variants:
        var name: string
        if ty[].has_type_args:
          name = variant.name.mangled_as_type_with_params(ty.type_args)
        else:
          name = variant.name.mangled_as_type
        let variant_type = llvm.structCreateNamed(cg.ctx, cstring(name))
        cg.bind_type(name, variant_type)

        var fields = newSeq[llvm.TypeRef]()
        for field in variant.fields:
          fields.add(field.ty[].as_qualified_in_span(span).to_first_class_cg_type_with_span(span, cg, target))
        let fieldv = if fields.len == 0: nil else: addr fields[0]
        llvm.structSetBody(variant_type, fieldv, cuint(fields.len), 0)

      var
        size: uint
        tag_size: uint
      let
        s = ty.inner.tagged.get_tagged_union_size(cg.type_ctx)
        ts = ty.inner.tagged.get_tagged_tag_size(cg.type_ctx)

      case s.kind
      of sokUnknown:
        raise newError(errkInvalidType, span, "unknown size")
      of sokUnsized:
        raise newError(errkInvalidType, span, "unsized")
      of sokSized:
        size = s.size

      case ts.kind
      of sokUnknown:
        raise newError(errkInvalidType, span, "unknown tag size")
      of sokUnsized:
        raise newError(errkInvalidType, span, "unsized tag size")
      of sokSized:
        tag_size = ts.size

      let tag_type = llvm.intTypeInContext(cg.ctx, cuint(tag_size * 8))
      let u8 = llvm.intTypeInContext(cg.ctx, 8)
      let union_type = llvm.arrayType(u8, cuint(size))
      
      var fields = newSeq[llvm.TypeRef]()
      for field in ty.inner.tagged.fields:
        fields.add(field.ty[].as_qualified_in_span(span).to_first_class_cg_type_with_span(span, cg, target))
      fields.add(tag_type)
      fields.add(union_type)
      let fieldv = if fields.len == 0: nil else: addr fields[0]
      llvm.structSetBody(tagged_type, fieldv, cuint(fields.len), 0)

      return tagged_type
  of tykFunction:
    var params = newSeqOfCap[llvm.TypeRef](ty.inner.function.params.len)
    for param in ty.inner.function.params:
      let t = param.ty[].as_qualified_in_span(span)
      if t.inner.kind != tykUnit:
        params.add(t.to_first_class_cg_type_with_span(span, cg, target))
    let param_ptr = if params.len == 0: nil else: addr params[0]
    let variadic = if ty.inner.function.variadic: 1 else: 0
    return llvm.pointerType(llvm.functionType(ty.inner.function.result_type[].as_qualified_in_span(span).to_first_class_cg_type_with_span(span, cg, target), param_ptr, cuint(params.len), variadic.cint), 0)
  of tykPointer:
    # llvm has a complicated relationship with void pointers
    let pointee = ty.inner.point.pointee[].as_qualified_in_span(span).to_first_class_cg_type_with_span(span, cg, target)
    if llvm.getTypeKind(pointee) == llvm.VoidTypeKind:
      let u8 = llvm.intTypeInContext(cg.ctx, 8)
      result = llvm.pointerType(u8, 0)
    else:
      result = llvm.pointerType(pointee, 0)
  of tykSlice:
    var fields = newSeqOfCap[llvm.TypeRef](2)
    fields.add(llvm.intTypeInContext(cg.ctx, cuint(target.pointer_width)))
    fields.add(llvm.pointerType(ty.inner.slice.elem[].as_qualified_in_span(span).to_first_class_cg_type_with_span(span, cg, target), 0))
    let field_ptr = addr fields[0]
    return llvm.structTypeInContext(cg.ctx, field_ptr, cuint(fields.len), 0)
  of tykArray:
    let size = ty.inner.arr.size[].as_qualified_in_span(span).inner.lit.intval
    return llvm.arrayType(ty.inner.arr.elem[].as_qualified_in_span(span).to_first_class_cg_type_with_span(span, cg, target), cuint(size))
  of tykVector:
    let size = ty.inner.vec.size[].as_qualified_in_span(span).inner.lit.intval
    return llvm.vectorType(ty.inner.vec.elem[].as_qualified_in_span(span).to_first_class_cg_type_with_span(span, cg, target), cuint(size))
  of tykVar:
    var fields = newSeqOfCap[llvm.TypeRef](2)
    fields.add(llvm.intTypeInContext(cg.ctx, cuint(target.pointer_width)))
    let arr = llvm.arrayType(ty.inner.vararr.elem[].as_qualified_in_span(span).to_first_class_cg_type_with_span(span, cg, target), 0)
    fields.add(arr)
    let field_ptr = addr fields[0]
    let struct_type = llvm.structTypeInContext(cg.ctx, field_ptr, cuint(fields.len), 0)
    return llvm.pointerType(struct_type, 0)
  of tykParameter:
    raise newError(errkUnspecifiedType, span, "unevaluated type parameter")
  of tykLit:
    raise newError(errkFreestandingTypeLiteral, span)

proc to_cg_type_with_span(ty: ref QualifiedType, span: Span, cg: CgPassController, target: ref Target): llvm.TypeRef =
  case ty.inner.kind
  of tykType:
    raise newError(errkInvalidType, span, "type is a meta-type")
  of tykNever:
    return llvm.voidTypeInContext(cg.ctx)
  of tykUnit:
    return llvm.voidTypeInContext(cg.ctx)
  of tykBool:
    return llvm.intTypeInContext(cg.ctx, 1)
  of tykSigned:
    case ty.inner.signed.bits.kind
    of bitsWord:
      return llvm.intTypeInContext(cg.ctx, cuint(target.pointer_width))
    of bitsCustom:
      return llvm.intTypeInContext(cg.ctx, cuint(ty.inner.signed.bits.bits))
  of tykUnsigned:
    case ty.inner.unsigned.bits.kind
    of bitsWord:
      return llvm.intTypeInContext(cg.ctx, cuint(target.pointer_width))
    of bitsCustom:
      return llvm.intTypeInContext(cg.ctx, cuint(ty.inner.unsigned.bits.bits))
  of tykFloating:
    case ty.inner.floating.bits.kind
    of bitsWord:
      case target.pointer_width
      of 32:
        return llvm.floatTypeInContext(cg.ctx)
      of 64:
        return llvm.doubleTypeInContext(cg.ctx)
      else:
        raise newError(errkInvalidType, span, "invalid floating type width")
    of bitsCustom:
      case ty.inner.floating.bits.bits
      of 16:
        return llvm.halfTypeInContext(cg.ctx)
      of 32:
        return llvm.floatTypeInContext(cg.ctx)
      of 64:
        return llvm.doubleTypeInContext(cg.ctx)
      of 80:
        return llvm.x86fp80TypeInContext(cg.ctx)
      of 128:
        return llvm.fp128TypeInContext(cg.ctx)
      else:
        raise newError(errkInvalidType, span, "invalid floating type width")
  of tykTrait:
    let
      u8 = llvm.intTypeInContext(cg.ctx, 8)
      unit_ptr = llvm.pointerType(u8, 0)
      unitp_ptr = llvm.pointerType(unit_ptr, 0)
    
    var fields = [unit_ptr, unitp_ptr]
    let struct_type = llvm.structTypeInContext(cg.ctx, addr fields[0], cuint(fields.len), 0)
    return struct_type
  of tykStruct:
    let name = ty.mangled
    let lty = cg.get_type(name)
    if lty.isSome:
      return lty.get
    else:
      let struct_type = llvm.structCreateNamed(cg.ctx, cstring(name))
      cg.bind_type(name, struct_type)

      var fields = newSeq[llvm.TypeRef]()
      for field in ty.inner.struct.fields:
        let qfield = field.ty[].as_qualified_in_span(span)
        let field = qfield.to_first_class_cg_type_with_span(span, cg, target)
        fields.add(field)
      let fieldv = if fields.len == 0: nil else: addr fields[0]
      llvm.structSetBody(struct_type, fieldv, cuint(fields.len), 0)
      return struct_type
  of tykUnion:
    let lty = cg.get_type(ty.mangled)
    if lty.isSome:
      return lty.get
    else:
      var
        size: uint
      let
        s = ty.inner.get_size_of(cg.type_ctx)

      case s.kind
      of sokUnknown:
        raise newError(errkInvalidType, span, "unknown size")
      of sokUnsized:
        raise newError(errkInvalidType, span, "unsized")
      of sokSized:
        size = s.size

      let u8 = llvm.intTypeInContext(cg.ctx, 8)
      let union_type = llvm.arrayType(u8, cuint(size))
      cg.bind_type(ty.mangled, union_type)
      return union_type
  of tykEnum:
    let lty = cg.get_type(ty.mangled)
    if lty.isSome:
      return lty.get
    else:
      let inner = ty
        .inner
        .enumeration
        .inner
        .to_first_class_cg_type_with_span(span, cg, target)
      cg.bind_type(ty.mangled, inner)
  of tykTagged:
    let lty = cg.get_type(ty.mangled)
    if lty.isSome:
      return lty.get
    else:
      let name = ty.mangled
      let tagged_type = llvm.structCreateNamed(cg.ctx, cstring(name))
      cg.bind_type(name, tagged_type)

      for variant in ty.inner.tagged.variants:
        var name: string
        if ty[].has_type_args:
          name = variant.name.mangled_as_type_with_params(ty.type_args)
        else:
          name = variant.name.mangled_as_type
        let variant_type = llvm.structCreateNamed(cg.ctx, cstring(name))
        cg.bind_type(name, variant_type)

        var fields = newSeq[llvm.TypeRef]()
        for field in variant.fields:
          fields.add(field.ty[].as_qualified_in_span(span).to_first_class_cg_type_with_span(span, cg, target))
        let fieldv = if fields.len == 0: nil else: addr fields[0]
        llvm.structSetBody(variant_type, fieldv, cuint(fields.len), 0)

      var
        size: uint
        tag_size: uint
      let
        s = ty.inner.tagged.get_tagged_union_size(cg.type_ctx)
        ts = ty.inner.tagged.get_tagged_tag_size(cg.type_ctx)

      case s.kind
      of sokUnknown:
        raise newError(errkInvalidType, span, "unknown size")
      of sokUnsized:
        raise newError(errkInvalidType, span, "unsized")
      of sokSized:
        size = s.size

      case ts.kind
      of sokUnknown:
        raise newError(errkInvalidType, span, "unknown tag size")
      of sokUnsized:
        raise newError(errkInvalidType, span, "unsized tag size")
      of sokSized:
        tag_size = ts.size

      let tag_type = llvm.intTypeInContext(cg.ctx, cuint(tag_size * 8))
      let u8 = llvm.intTypeInContext(cg.ctx, 8)
      let union_type = llvm.arrayType(u8, cuint(size))
      
      var fields = newSeq[llvm.TypeRef]()
      for field in ty.inner.tagged.fields:
        fields.add(field.ty[].as_qualified_in_span(span).to_first_class_cg_type_with_span(span, cg, target))
      fields.add(tag_type)
      fields.add(union_type)
      let fieldv = if fields.len == 0: nil else: addr fields[0]
      llvm.structSetBody(tagged_type, fieldv, cuint(fields.len), 0)

      return tagged_type
  of tykFunction:
    var params = newSeqOfCap[llvm.TypeRef](ty.inner.function.params.len)
    for param in ty.inner.function.params:
      let t = param.ty[].as_qualified_in_span(span)
      if t.inner.kind != tykUnit:
        params.add(t.to_first_class_cg_type_with_span(span, cg, target))
    let param_ptr = if params.len == 0: nil else: addr params[0]
    let variadic = if ty.inner.function.variadic: 1 else: 0
    return llvm.functionType(ty.inner.function.result_type[].as_qualified_in_span(span).to_first_class_cg_type_with_span(span, cg, target), param_ptr, cuint(params.len), variadic.cint)
  of tykPointer:
    let pointee = ty.inner.point.pointee[].as_qualified_in_span(span).to_first_class_cg_type_with_span(span, cg, target)
    if llvm.getTypeKind(pointee) == llvm.VoidTypeKind:
      let u8 = llvm.intTypeInContext(cg.ctx, 8)
      result = llvm.pointerType(u8, 0)
    else:
      result = llvm.pointerType(pointee, 0)
  of tykSlice:
    var fields = newSeqOfCap[llvm.TypeRef](2)
    fields.add(llvm.intTypeInContext(cg.ctx, cuint(target.pointer_width)))
    fields.add(llvm.pointerType(ty.inner.slice.elem[].as_qualified_in_span(span).to_first_class_cg_type_with_span(span, cg, target), 0))
    let field_ptr = addr fields[0]
    return llvm.structTypeInContext(cg.ctx, field_ptr, cuint(fields.len), 0)
  of tykArray:
    let size = ty.inner.arr.size[].as_qualified_in_span(span).inner.lit.intval
    return llvm.arrayType(ty.inner.arr.elem[].as_qualified_in_span(span).to_first_class_cg_type_with_span(span, cg, target), cuint(size))
  of tykVector:
    let size = ty.inner.vec.size[].as_qualified_in_span(span).inner.lit.intval
    return llvm.vectorType(ty.inner.vec.elem[].as_qualified_in_span(span).to_first_class_cg_type_with_span(span, cg, target), cuint(size))
  of tykVar:
    var fields = newSeqOfCap[llvm.TypeRef](2)
    fields.add(llvm.intTypeInContext(cg.ctx, cuint(target.pointer_width)))
    let arr = llvm.arrayType(ty.inner.vararr.elem[].as_qualified_in_span(span).to_first_class_cg_type_with_span(span, cg, target), 0)
    fields.add(arr)
    let field_ptr = addr fields[0]
    let struct_type = llvm.structTypeInContext(cg.ctx, field_ptr, cuint(fields.len), 0)
    return llvm.pointerType(struct_type, 0)
  of tykParameter:
    raise newError(errkUnspecifiedType, span, "unevaluated type parameter")
  of tykLit:
    raise newError(errkFreestandingTypeLiteral, span)

proc compile_ctype_named(ty: ref QualifiedType, name: string, span: Span, cg: CgPassController, target: ref Target): llvm.TypeRef =
  let name = name.toNonFullPath.mangled_as_type
  case ty.inner.kind
  of tykType:
    raise newError(errkInvalidType, span, "type is a meta-type")
  of tykNever:
    result = llvm.voidTypeInContext(cg.ctx)
    cg.bind_type(name, result)
  of tykUnit:
    result = llvm.voidTypeInContext(cg.ctx)
    cg.bind_type(name, result)
  of tykBool:
    result = llvm.intTypeInContext(cg.ctx, 1)
    cg.bind_type(name, result)
  of tykSigned:
    case ty.inner.signed.bits.kind
    of bitsWord:
      result = llvm.intTypeInContext(cg.ctx, cuint(target.pointer_width))
      cg.bind_type(name, result)
    of bitsCustom:
      result = llvm.intTypeInContext(cg.ctx, cuint(ty.inner.signed.bits.bits))
      cg.bind_type(name, result)
  of tykUnsigned:
    case ty.inner.unsigned.bits.kind
    of bitsWord:
      result = llvm.intTypeInContext(cg.ctx, cuint(target.pointer_width))
      cg.bind_type(name, result)
    of bitsCustom:
      result = llvm.intTypeInContext(cg.ctx, cuint(ty.inner.unsigned.bits.bits))
      cg.bind_type(name, result)
  of tykFloating:
    case ty.inner.floating.bits.kind
    of bitsWord:
      case target.pointer_width
      of 32:
        result = llvm.floatTypeInContext(cg.ctx)
        cg.bind_type(name, result)
      of 64:
        result = llvm.doubleTypeInContext(cg.ctx)
        cg.bind_type(name, result)
      else:
        raise newError(errkInvalidType, span, "invalid floating type width")
    of bitsCustom:
      case ty.inner.floating.bits.bits
      of 16:
        result = llvm.halfTypeInContext(cg.ctx)
        cg.bind_type(name, result)
      of 32:
        result = llvm.floatTypeInContext(cg.ctx)
        cg.bind_type(name, result)
      of 64:
        result = llvm.doubleTypeInContext(cg.ctx)
        cg.bind_type(name, result)
      of 80:
        result = llvm.x86fp80TypeInContext(cg.ctx)
        cg.bind_type(name, result)
      of 128:
        result = llvm.fp128TypeInContext(cg.ctx)
        cg.bind_type(name, result)
      else:
        raise newError(errkInvalidType, span, "invalid floating type width")
  of tykTrait:
    let
      u8 = llvm.intTypeInContext(cg.ctx, 8)
      unit_ptr = llvm.pointerType(u8, 0)
      unitp_ptr = llvm.pointerType(unit_ptr, 0)
    
    var fields = [unit_ptr, unitp_ptr]
    let struct_type = llvm.structTypeInContext(cg.ctx, addr fields[0], cuint(fields.len), 0)
    return struct_type
  of tykStruct:
    if ty.inner.struct.name.isSome:
      let struct_type = llvm.structCreateNamed(cg.ctx, cstring(name))
      cg.bind_type(name, struct_type)

      var fields = newSeq[llvm.TypeRef]()
      for field in ty.inner.struct.fields:
        let qfield = field.ty[].as_qualified_in_span(span)
        var field = qfield.try_to_first_class_cg_ctype_with_span(span, cg, target)
        if field.isNil:
          field = qfield.compile_ctype_named(qfield[].name.path[^1], span, cg, target)
        fields.add(field)
      let fieldv = if fields.len == 0: nil else: addr fields[0]
      llvm.structSetBody(struct_type, fieldv, cuint(fields.len), 0)
      return struct_type
    else:
      raise newIce("unimplemented")
  of tykUnion:
    var size: uint
    let s = ty.get_size_of(cg.type_ctx)

    case s.kind
    of sokUnknown:
      raise newError(errkInvalidType, span, "unknown size")
    of sokUnsized:
      raise newError(errkInvalidType, span, "unsized")
    of sokSized:
      size = s.size

    let u8 = llvm.intTypeInContext(cg.ctx, 8)
    let union_type = llvm.arrayType(u8, cuint(size))
    cg.bind_type(name, union_type)
    return union_type
  of tykEnum:
    let inner = ty
      .inner
      .enumeration
      .inner
      .to_first_class_cg_type_with_span(span, cg, target)
    cg.bind_type(name, inner)
  of tykTagged:
    let tagged_type = llvm.structCreateNamed(cg.ctx, cstring(name))
    cg.bind_type(name, tagged_type)

    for variant in ty.inner.tagged.variants:
      let variant_type = llvm.structCreateNamed(cg.ctx, cstring(variant.name.mangled_as_type))

      var fields = newSeq[llvm.TypeRef]()
      for field in variant.fields:
        fields.add(field.ty[].as_qualified_in_span(span).to_first_class_cg_type_with_span(span, cg, target))
      let fieldv = if fields.len == 0: nil else: addr fields[0]
      llvm.structSetBody(variant_type, fieldv, cuint(fields.len), 0)

      cg.bind_type(variant.name.mangled_as_type, variant_type)

    var
      size: uint
      tag_size: uint
    let
      s = ty.inner.tagged.get_tagged_union_size(cg.type_ctx)
      ts = ty.inner.tagged.get_tagged_tag_size(cg.type_ctx)

    case s.kind
    of sokUnknown:
      raise newError(errkInvalidType, span, "unknown size")
    of sokUnsized:
      raise newError(errkInvalidType, span, "unsized")
    of sokSized:
      size = s.size

    case ts.kind
    of sokUnknown:
      raise newError(errkInvalidType, span, "unknown tag size")
    of sokUnsized:
      raise newError(errkInvalidType, span, "unsized tag size")
    of sokSized:
      tag_size = ts.size

    let tag_type = llvm.intTypeInContext(cg.ctx, cuint(tag_size * 8))
    let u8 = llvm.intTypeInContext(cg.ctx, 8)
    let union_type = llvm.arrayType(u8, cuint(size))
    
    var fields = newSeq[llvm.TypeRef]()
    for field in ty.inner.tagged.fields:
      fields.add(field.ty[].as_qualified_in_span(span).to_first_class_cg_type_with_span(span, cg, target))
    fields.add(tag_type)
    fields.add(union_type)
    let fieldv = if fields.len == 0: nil else: addr fields[0]
    llvm.structSetBody(tagged_type, fieldv, cuint(fields.len), 0)

    return tagged_type
  of tykFunction:
    var params = newSeqOfCap[llvm.TypeRef](ty.inner.function.params.len)
    for param in ty.inner.function.params:
      let t = param.ty[].as_qualified_in_span(span)
      if t.inner.kind != tykUnit:
        params.add(t.to_first_class_cg_type_with_span(span, cg, target))
    let param_ptr = if params.len == 0: nil else: addr params[0]
    let variadic = if ty.inner.function.variadic: 1 else: 0
    result = llvm.functionType(ty.inner.function.result_type[].as_qualified_in_span(span).to_first_class_cg_type_with_span(span, cg, target), param_ptr, cuint(params.len), variadic.cint)
    cg.bind_type(name, result)
  of tykPointer:
    let qpointee = ty.inner.point.pointee[].as_qualified_in_span(span)
    var pointee = qpointee.try_to_first_class_cg_ctype_with_span(span, cg, target)
    if pointee.isNil:
      pointee = qpointee.compile_ctype_named(qpointee[].name.path[^1], span, cg, target)
    if llvm.getTypeKind(pointee) == llvm.VoidTypeKind:
      let u8 = llvm.intTypeInContext(cg.ctx, 8)
      result = llvm.pointerType(u8, 0)
    else:
      result = llvm.pointerType(pointee, 0)
    cg.bind_type(name, result)
  of tykSlice:
    var fields = newSeqOfCap[llvm.TypeRef](2)
    fields.add(llvm.intTypeInContext(cg.ctx, cuint(target.pointer_width)))
    fields.add(llvm.pointerType(ty.inner.slice.elem[].as_qualified_in_span(span).to_first_class_cg_type_with_span(span, cg, target), 0))
    let field_ptr = addr fields[0]
    result = llvm.structTypeInContext(cg.ctx, field_ptr, cuint(fields.len), 0)
    cg.bind_type(name, result)
  of tykArray:
    let size = ty.inner.arr.size[].as_qualified_in_span(span).inner.lit.intval
    result = llvm.arrayType(ty.inner.arr.elem[].as_qualified_in_span(span).to_first_class_cg_type_with_span(span, cg, target), cuint(size))
    cg.bind_type(name, result)
  of tykVector:
    let size = ty.inner.vec.size[].as_qualified_in_span(span).inner.lit.intval
    result = llvm.vectorType(ty.inner.vec.elem[].as_qualified_in_span(span).to_first_class_cg_type_with_span(span, cg, target), cuint(size))
    cg.bind_type(name, result)
  of tykVar:
    var fields = newSeqOfCap[llvm.TypeRef](2)
    fields.add(llvm.intTypeInContext(cg.ctx, cuint(target.pointer_width)))
    let arr = llvm.arrayType(ty.inner.vararr.elem[].as_qualified_in_span(span).to_first_class_cg_type_with_span(span, cg, target), 0)
    fields.add(arr)
    let field_ptr = addr fields[0]
    let struct_type = llvm.structTypeInContext(cg.ctx, field_ptr, cuint(fields.len), 0)
    result = llvm.pointerType(struct_type, 0)
    cg.bind_type(name, result)
  of tykParameter:
    raise newError(errkUnspecifiedType, span, "unevaluated type parameter")
  of tykLit:
    raise newError(errkFreestandingTypeLiteral, span)

proc type_reg*(index: ref Index, driver: ref Driver, root: ref Node) =
  let cg = driver.pass_controllers[tsCodeDecl].CgPassController
  let target = driver.target
  let
    pre_visitor = HlirVisitor(
      visit_seq: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if parent.node.kind == hrkRoot:
          discard
        return vrkRecurse),
      visit_use: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let module = self.node.use.path
        let di_file = cg.di_file
        let unit = index.load_with_span(module, "amp", self.meta.span)
        driver.run_until(index, unit, tsCodeDecl)
        cg.di_file = di_file
      ),
      visit_use_extern: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        proc visitor(cursor: CXCursor, parent: CXCursor, client_data: CXClientData): CXChildVisitResult {.cdecl.} =
          let
            driver = cast[ptr (ref Driver, ref Node)](client_data)[][0]
            self = cast[ptr (ref Driver, ref Node)](client_data)[][1]
            cg = driver.pass_controllers[tsCodeDecl].CgPassController
          if clang.isDeclaration(clang.getCursorKind(cursor)) != 0:
            let
              cx_type = clang.getCursorType(cursor)
              canonical = clang.getCanonicalType(cx_type)

            if clang.getCursorKind(cursor) == CXCursor_StructDecl:
              var full_name = "__struct_"
              let
                ty = canonical.to_type(driver.target)
                cs = clang.getCursorSpelling(cursor)
                name = clang.getString(cs)
              if name.len > 0:
                full_name.add(name)
                discard ty.compile_ctype_named(full_name, self.meta.span, cg, driver.target)
              clang.disposeString(cs)
            elif clang.getCursorKind(cursor) == CXCursor_UnionDecl:
              var full_name = "__union_"
              let
                ty = canonical.to_type(driver.target)
                cs = clang.getCursorSpelling(cursor)
                name = clang.getString(cs)
              if name.len > 0:
                full_name.add(name)
                discard ty.compile_ctype_named(full_name, self.meta.span, cg, driver.target)
              clang.disposeString(cs)
            elif clang.getCursorKind(cursor) == CXCursor_EnumDecl:
              var full_name = "__enum_"
              let
                ty = canonical.to_type(driver.target)
                cs = clang.getCursorSpelling(cursor)
                name = clang.getString(cs)
              if name.len > 0:
                full_name.add(name)
                discard ty.compile_ctype_named(full_name, self.meta.span, cg, driver.target)
              clang.disposeString(cs)
            elif clang.getCursorKind(cursor) == CXCursor_TypedefDecl:
              let
                ty = canonical.to_type(driver.target)
                cs = clang.getCursorSpelling(cursor)
                name = clang.getString(cs)
              clang.disposeString(cs)
              case ty.inner.kind
              of tykStruct:
                if ty.inner.struct.name.isNone:
                  ty.inner.struct.name = some(name.toNonFullPath)
              of tykUnion:
                if ty.inner.union.name.isNone:
                  ty.inner.union.name = some(name.toNonFullPath)
              else:
                discard
              discard ty.compile_ctype_named(name, self.meta.span, cg, driver.target)
          return CXChildVisit_Continue

        let
          lang = self.node.use_extern.lang
          file = self.node.use_extern.file
        if lang != "C":
          raise newError(errkUnrecognizedLanguage, self.meta.span)

        var data = (driver, self)
        let
          (ts, cxtu) = index.load_c_with_span(file, self.meta.span)
          root = clang.getTranslationUnitCursor(cxtu)
        if ts < tsCodeDecl:
          clang.visitChildren(root, visitor, cast[ptr cchar](addr data))
          index.update_c_tu(file, tsCodeDecl)
      ),
      visit_function: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.function.type_params.isSome:
          return vrkContinue
        if self.meta.attributes.isSome and self.meta.attributes.get.any((attr: Attribute) => attr.kind == akComptime):
          return vrkContinue
        if driver.test_mode and self.node.function.name.get.mangled.get == "main" and not (self.meta.attributes.isSome and self.meta.attributes.get.any((attr: Attribute) => attr.kind == akTestMain)):
          return vrkContinue
        if not driver.test_mode and self.meta.attributes.isSome and self.meta.attributes.get.any((attr: Attribute) => attr.kind == akTest):
          return vrkContinue
        if not driver.test_mode and self.meta.attributes.isSome and self.meta.attributes.get.any((attr: Attribute) => attr.kind == akTestMain):
          return vrkContinue
        return vrkRecurse),
      visit_macro: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        return vrkContinue),
      visit_struct: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.struct.type_params.isSome:
          return vrkContinue

        let struct_type = llvm.structCreateNamed(cg.ctx, cstring(self.node.struct.name.get.mangled.get))
        cg.bind_type(self.node.struct.name.get.mangled.get, struct_type)
        return vrkRecurse),
      visit_union: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.struct.type_params.isSome:
          return vrkContinue

        var
          size: uint
        let
          s = self.meta.own_type.get.get_size_of(cg.type_ctx)

        case s.kind
        of sokUnknown:
          raise newError(errkInvalidType, self.meta.span, "unknown size")
        of sokUnsized:
          raise newError(errkInvalidType, self.meta.span, "unsized")
        of sokSized:
          size = s.size

        let u8 = llvm.intTypeInContext(cg.ctx, 8)
        let union_type = llvm.arrayType(u8, cuint(size))
        cg.bind_type(self.node.struct.name.get.mangled.get, union_type)
        return vrkRecurse),
      visit_enum: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let inner = self.meta.own_type
          .get
          .inner
          .enumeration
          .inner
          .to_first_class_cg_type_with_span(self.meta.span, cg, target)
        self.meta.type_ref = inner
        cg.bind_type(self.node.enumeration.name.get.mangled.get, inner)
        return vrkRecurse),
      visit_enum_variant: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.type_ref = parent.meta.type_ref
        return vrkRecurse),
      visit_tagged: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.tagged.type_params.isSome:
          return vrkContinue

        let tagged_type = llvm.structCreateNamed(cg.ctx, cstring(self.node.tagged.name.get.mangled.get))
        cg.bind_type(self.node.tagged.name.get.mangled.get, tagged_type)
        for variant in self.node.tagged.variants:
          let variant_type = llvm.structCreateNamed(cg.ctx, cstring(variant.node.tagged_variant.name.get.mangled.get))
          cg.bind_type(variant.node.tagged_variant.name.get.mangled.get, variant_type)
        return vrkRecurse),
      visit_typedef: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        cg.bind_type(self.node.typedef.name.get.mangled.get, self.meta.own_type.get.to_first_class_cg_type_with_span(self.meta.span, cg, target))
        return vrkRecurse),
    )
    post_visitor = HlirVisitor()

  if driver.debug_info:
    let
      file = root.meta.span.file
      directory = root.meta.span.directory
    cg.di_file = some(llvm.diBuilderCreateFile(
        cg.di_builder, # Builder
        file.cstring, # Filename
        file.len.uint, # FilenameLen
        directory.cstring, # Directory
        directory.len.uint, # DirectoryLen
    ))
    if cg.di_compile_unit.isNone:
      cg.di_compile_unit = some(llvm.diBuilderCreateCompileUnit(
          cg.di_builder, # Builder
          DWARFSourceLanguageC99, # Lang, C99 is the closest thing to &
          cg.di_file.get, # FileRef
          version_str.cstring, # Producer
          version_str.len, # ProducerLen
          llvm.Bool(cg.opt_level > 0), # IsOptimized
          nil, # Flags
          0, # FlagsLen
          0, # RuntimeVer
          nil, # SplitName
          0, # SplitNameLen
          DWARFEmissionFull, # Kind
          0, # DWOId
          1, # SplitDebugInlining
          1, # DebugInfoForProfiling
      ))

  discard accept(root, pre_visitor, post_visitor)

proc type_reg*(index: ref Index, driver: ref Driver, node: ref Node, root: ref Node) =
  if root.meta.impl_scope.isSome:
    driver.type_ctx.enter_this_bounded(root.meta.impl_scope.get)
  else:
    driver.type_ctx.enter_bounded
  type_reg(index, driver, node)
  root.meta.impl_scope = some(driver.type_ctx.leave_this_bounded)

proc type_gen*(index: ref Index, driver: ref Driver, root: ref Node) =
  let cg = driver.pass_controllers[tsCodeDecl].CgPassController
  let target = driver.target
  let
    pre_visitor = HlirVisitor(
      visit_macro: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        return vrkContinue),
      visit_function: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.function.type_params.isSome:
          return vrkContinue
        if self.meta.attributes.isSome and self.meta.attributes.get.any((attr: Attribute) => attr.kind == akComptime):
          return vrkContinue
        if driver.test_mode and self.node.function.name.get.mangled.get == "main" and not (self.meta.attributes.isSome and self.meta.attributes.get.any((attr: Attribute) => attr.kind == akTestMain)):
          return vrkContinue
        if not driver.test_mode and self.meta.attributes.isSome and self.meta.attributes.get.any((attr: Attribute) => attr.kind == akTest):
          return vrkContinue
        if not driver.test_mode and self.meta.attributes.isSome and self.meta.attributes.get.any((attr: Attribute) => attr.kind == akTestMain):
          return vrkContinue
        return vrkRecurse),
      visit_struct: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.struct.type_params.isSome:
          return vrkContinue
        return vrkRecurse),
      visit_union: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.struct.type_params.isSome:
          return vrkContinue
        return vrkRecurse),
      visit_tagged: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.tagged.type_params.isSome:
          return vrkContinue
        return vrkRecurse),
    )
    post_visitor = HlirVisitor(
      visit_function: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.type_ref = self.node.function.ty.meta.type_ref
        return vrkRecurse),
      visit_static: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.type_ref = self.node.static_global.ty.meta.type_ref.to_first_class
        return vrkRecurse),
      visit_static_extern: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.type_ref = self.node.static_extern.ty.meta.type_ref.to_first_class
        return vrkRecurse),
      visit_define: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.type_ref = self.node.define.ty.get.meta.type_ref.to_first_class
        return vrkRecurse),
      visit_struct: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let t = cg.get_type(self.node.struct.name.get.mangled.get)
        if t.isNone:
          raise newIce("type not found")
        let struct_type = t.get

        var fields = newSeq[llvm.TypeRef]()
        for field in self.node.struct.fields:
          let field = field.node.decl
          fields.add(field.value.meta.type_ref.to_first_class)
        let fieldv = if fields.len == 0: nil else: addr fields[0]
        llvm.structSetBody(struct_type, fieldv, cuint(fields.len), 0)
        return vrkRecurse),
      visit_tagged: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let t = cg.get_type(self.node.tagged.name.get.mangled.get)
        if t.isNone:
          raise newIce("type not found")
        let tagged_type = t.get

        var
          size: uint
          tag_size: uint
        let
          s = self.meta.own_type.get.inner.tagged.get_tagged_union_size(cg.type_ctx)
          ts = self.meta.own_type.get.inner.tagged.get_tagged_tag_size(cg.type_ctx)

        case s.kind
        of sokUnknown:
          raise newError(errkInvalidType, self.meta.span, "unknown size")
        of sokUnsized:
          raise newError(errkInvalidType, self.meta.span, "unsized")
        of sokSized:
          size = s.size

        case ts.kind
        of sokUnknown:
          raise newError(errkInvalidType, self.meta.span, "unknown tag size")
        of sokUnsized:
          raise newError(errkInvalidType, self.meta.span, "unsized tag size")
        of sokSized:
          tag_size = ts.size

        let tag_type = llvm.intTypeInContext(cg.ctx, cuint(tag_size * 8))
        let u8 = llvm.intTypeInContext(cg.ctx, 8)
        let union_type = llvm.arrayType(u8, cuint(size))
        
        var fields = newSeq[llvm.TypeRef]()
        for field in self.node.tagged.fields:
          let field = field.node.decl
          fields.add(field.value.meta.type_ref.to_first_class)
        fields.add(tag_type)
        fields.add(union_type)
        let fieldv = if fields.len == 0: nil else: addr fields[0]
        llvm.structSetBody(tagged_type, fieldv, cuint(fields.len), 0)

        for variant in self.node.tagged.variants:
          let t = cg.get_type(variant.node.tagged_variant.name.get.mangled.get)
          if t.isNone:
            raise newIce("type not found")
          let variant_type = t.get

          var fields = newSeq[llvm.TypeRef]()
          for field in variant.node.tagged_variant.fields:
            let field = field.node.decl
            fields.add(field.value.meta.type_ref.to_first_class)
          let fieldv = if fields.len == 0: nil else: addr fields[0]
          llvm.structSetBody(variant_type, fieldv, cuint(fields.len), 0)
        return vrkRecurse),
      visit_type_unit: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.type_ref = llvm.voidTypeInContext(cg.ctx)
        return vrkRecurse),
      visit_type_bool: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.type_ref = llvm.intTypeInContext(cg.ctx, 1)
        return vrkRecurse),
      visit_type_signed: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        case self.node.type_signed.bits.kind
        of bitsCustom:
          self.meta.type_ref = llvm.intTypeInContext(cg.ctx, cuint(self.node.type_signed.bits.bits))
        of bitsWord:
          self.meta.type_ref = llvm.intTypeInContext(cg.ctx, cuint(target.pointer_width))
        return vrkRecurse),
      visit_type_unsigned: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        case self.node.type_unsigned.bits.kind
        of bitsCustom:
          self.meta.type_ref = llvm.intTypeInContext(cg.ctx, cuint(self.node.type_unsigned.bits.bits))
        of bitsWord:
          self.meta.type_ref = llvm.intTypeInContext(cg.ctx, cuint(target.pointer_width))
        return vrkRecurse),
      visit_type_floating: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        case self.node.type_floating.bits.bits
        of 16:
          self.meta.type_ref = llvm.halfTypeInContext(cg.ctx)
        of 32:
          self.meta.type_ref = llvm.floatTypeInContext(cg.ctx)
        of 64:
          self.meta.type_ref = llvm.doubleTypeInContext(cg.ctx)
        of 80:
          self.meta.type_ref = llvm.x86fp80TypeInContext(cg.ctx)
        of 128:
          self.meta.type_ref = llvm.fp128TypeInContext(cg.ctx)
        else:
          raise newIce("invalid floating type width")
        return vrkRecurse),
      visit_type_pointer: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.type_ref = llvm.pointerType(self.node.type_pointer.pointee.meta.type_ref.to_first_class, 0)
        return vrkRecurse),
      visit_type_slice: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        var fields = newSeqOfCap[llvm.TypeRef](2)
        fields.add(llvm.intTypeInContext(cg.ctx, cuint(target.pointer_width)))
        fields.add(llvm.pointerType(self.node.type_slice.elem.meta.type_ref.to_first_class, 0))
        let field_ptr = addr fields[0]
        self.meta.type_ref = llvm.structTypeInContext(cg.ctx, field_ptr, cuint(fields.len), 0)
        return vrkRecurse),
      visit_type_array: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let
          elem = self.node.type_array.elem.meta.type_ref
          size = self.node.type_array.size.node.expr_int.value
        self.meta.type_ref = llvm.arrayType(elem.to_first_class, cuint(size))
        return vrkRecurse),
      visit_type_var: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let elem = self.node.type_var.elem.meta.type_ref

        var fields = newSeqOfCap[llvm.TypeRef](2)
        fields.add(llvm.intTypeInContext(cg.ctx, cuint(target.pointer_width)))
        let arr = llvm.arrayType(elem, 0)
        fields.add(arr)
        let field_ptr = addr fields[0]
        self.meta.type_ref = llvm.pointerType(llvm.structTypeInContext(cg.ctx, field_ptr, cuint(fields.len), 0), 0)

        return vrkRecurse),
      visit_type_function: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        var params = newSeqOfCap[llvm.TypeRef](self.node.type_function.params.len)
        for param in self.node.type_function.params:
          if llvm.getTypeKind(param.meta.type_ref) != VoidTypeKind:
            params.add(param.meta.type_ref.to_first_class)
        let param_ptr = if params.len == 0: nil else: addr params[0]
        let variadic = 0
        self.meta.type_ref = llvm.functionType(self.node.type_function.result_type.meta.type_ref, param_ptr, cuint(params.len), variadic.cint)
        return vrkRecurse),
      visit_decl: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.type_ref = self.node.decl.value.meta.type_ref
        return vrkRecurse),
      visit_expr_unit: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.type_ref = llvm.voidTypeInContext(cg.ctx)
        return vrkRecurse),
      visit_expr_bool: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.type_ref = self.meta.type_sum[].as_qualified_in_span(self.meta.span).to_first_class_cg_type_with_span(self.meta.span, cg, target)
        return vrkRecurse),
      visit_expr_int: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.type_ref = self.meta.type_sum[].as_qualified_in_span(self.meta.span).to_first_class_cg_type_with_span(self.meta.span, cg, target)
        return vrkRecurse),
      visit_expr_float: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.type_ref = self.meta.type_sum[].as_qualified_in_span(self.meta.span).to_first_class_cg_type_with_span(self.meta.span, cg, target)
        return vrkRecurse),
      visit_expr_string: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.type_ref = self.meta.type_sum[].as_qualified_in_span(self.meta.span).to_first_class_cg_type_with_span(self.meta.span, cg, target)
        return vrkRecurse),
      visit_expr_sizeof: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.type_ref = self.meta.type_sum[].as_qualified_in_span(self.meta.span).to_first_class_cg_type_with_span(self.meta.span, cg, target)
        return vrkRecurse),
      visit_expr_alignof: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.type_ref = self.meta.type_sum[].as_qualified_in_span(self.meta.span).to_first_class_cg_type_with_span(self.meta.span, cg, target)
        return vrkRecurse),
      visit_expr_named: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if not self.node.expr_named.quiet and
            (parent.node.kind != hrkDecl or parent.node.decl.name.isNone or (addr self[]) != (addr parent.node.decl.name.get[])):
          if self.meta.own_type.isNone:
            self.meta.type_ref = self.meta.type_sum[].as_qualified_in_span(self.meta.span).to_first_class_cg_type_with_span(self.meta.span, cg, target)
          elif (parent.node.kind != hrkExprApplication or (addr self[]) != (addr parent.node.expr_application.fn[])) and
             (parent.node.kind != hrkExprCommand or (addr self[]) != (addr parent.node.expr_command.fn[])):
            self.meta.type_ref = self.meta.own_type.get.to_first_class_cg_type_with_span(self.meta.span, cg, target)
        return vrkRecurse),
      visit_expr_unary: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        case self.node.expr_unary.op
        of hukRangeTo:
          let path = ["__lang", "builtin", "RangeTo"].toPath
          let params = @[self.node.expr_unary.value.meta.type_sum[].as_qualified_in_span(self.node.expr_unary.value.meta.span)]
          let name = path.mangled_as_type_with_params(params)
          if cg.get_type(name).isNone:
            let range_type = llvm.structCreateNamed(cg.ctx, cstring(name))
            let elem_type = self.node.expr_unary.value.meta.type_ref
            var fields = [elem_type]
            llvm.structSetBody(range_type, addr fields[0], cuint(fields.len), 0)

            cg.bind_type(name, range_type)
        of hukRangeFrom:
          let path = ["__lang", "builtin", "RangeFrom"].toPath
          let params = @[self.node.expr_unary.value.meta.type_sum[].as_qualified_in_span(self.node.expr_unary.value.meta.span)]
          let name = path.mangled_as_type_with_params(params)
          if cg.get_type(name).isNone:
            let range_type = llvm.structCreateNamed(cg.ctx, cstring(name))
            let elem_type = self.node.expr_unary.value.meta.type_ref
            var fields = [elem_type]
            llvm.structSetBody(range_type, addr fields[0], cuint(fields.len), 0)

            cg.bind_type(name, range_type)
        else:
          discard

        self.meta.type_ref = self.meta.type_sum[].as_qualified_in_span(self.meta.span).to_first_class_cg_type_with_span(self.meta.span, cg, target)
        return vrkRecurse),
      visit_expr_binary: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        case self.node.expr_binary.op
        of hbkRange:
          let path = ["__lang", "builtin", "Range"].toPath
          let params = @[self.node.expr_binary.lhs.meta.type_sum[].as_qualified_in_span(self.node.expr_binary.lhs.meta.span)]
          let name = path.mangled_as_type_with_params(params)
          if cg.get_type(name).isNone:
            let range_type = llvm.structCreateNamed(cg.ctx, cstring(name))
            let elem_type = self.node.expr_binary.lhs.meta.type_ref
            var fields = [elem_type, elem_type]
            llvm.structSetBody(range_type, addr fields[0], cuint(fields.len), 0)

            cg.bind_type(name, range_type)
        else:
            discard

        self.meta.type_ref = self.meta.type_sum[].as_qualified_in_span(self.meta.span).to_first_class_cg_type_with_span(self.meta.span, cg, target)
        return vrkRecurse),
      visit_expr_ternary: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.type_ref = self.meta.type_sum[].as_qualified_in_span(self.meta.span).to_first_class_cg_type_with_span(self.meta.span, cg, target)
        return vrkRecurse),
      visit_expr_compound: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.expr_compound.last.isSome:
          self.meta.type_ref = self.node.expr_compound.last.get.meta.type_ref
        else:
          self.meta.type_ref = llvm.voidTypeInContext(cg.ctx)
        return vrkRecurse),
      visit_expr_cast: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.type_ref = self.meta.type_sum[].as_qualified_in_span(self.meta.span).to_first_class_cg_type_with_span(self.meta.span, cg, target)
        return vrkRecurse),
      visit_expr_application: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.meta.own_type.isNone:
          self.meta.type_ref = self.meta.type_sum[].as_qualified_in_span(self.meta.span).to_first_class_cg_type_with_span(self.meta.span, cg, target)
        else:
          self.meta.type_ref = self.meta.own_type.get.to_first_class_cg_type_with_span(self.meta.span, cg, target)
        return vrkRecurse),
      visit_expr_array: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.type_ref = self.meta.type_sum[].as_qualified_in_span(self.meta.span).to_first_class_cg_type_with_span(self.meta.span, cg, target)
        return vrkRecurse),
      visit_expr_range_full: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.type_ref = self.meta.type_sum[].as_qualified_in_span(self.meta.span).to_first_class_cg_type_with_span(self.meta.span, cg, target)
        return vrkRecurse),
      visit_expr_field_access: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.type_ref = self.meta.type_sum[].as_qualified_in_span(self.meta.span).to_first_class_cg_type_with_span(self.meta.span, cg, target)
        return vrkRecurse),
      visit_expr_let: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.expr_let.ty.isSome:
          self.meta.type_ref = self.node.expr_let.ty.get.meta.type_ref.to_first_class
        elif self.node.expr_let.value.isSome:
          self.meta.type_ref = self.node.expr_let.value.get.meta.type_ref.to_first_class
        else:
          raise newIce("unimplemented")
        return vrkRecurse),
    )
  discard accept(root, pre_visitor, post_visitor)

proc type_gen*(index: ref Index, driver: ref Driver, node: ref Node, root: ref Node) =
  if root.meta.impl_scope.isSome:
    driver.type_ctx.enter_this_bounded(root.meta.impl_scope.get)
  else:
    driver.type_ctx.enter_bounded
  type_gen(index, driver, node)
  root.meta.impl_scope = some(driver.type_ctx.leave_this_bounded)

proc const_value_gen*(index: ref Index, driver: ref Driver, root: ref Node) =
  let cg = driver.pass_controllers[tsCodeDecl].CgPassController
  let target = driver.target
  let
    pre_visitor = HlirVisitor(
      visit_use_extern: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        proc visitor(cursor: CXCursor, parent: CXCursor, client_data: CXClientData): CXChildVisitResult {.cdecl.} =
          let
            driver = cast[ptr (ref Driver, ref Node, llvm.TypeRef)](client_data)[][0]
            self = cast[ptr (ref Driver, ref Node, llvm.TypeRef)](client_data)[][1]
            cg = driver.pass_controllers[tsCodeDecl].CgPassController
          if clang.isDeclaration(clang.getCursorKind(cursor)) != 0:
            if clang.getCursorKind(cursor) == CXCursor_EnumDecl:
              let
                cx_type = clang.getCursorType(cursor)
                canonical = clang.getCanonicalType(cx_type)
                ty = canonical
                  .to_type(driver.target)
                  .to_first_class_cg_type_with_span(self.meta.span, cg, driver.target)
              cast[ptr (ref Driver, ref Node, llvm.TypeRef)](client_data)[][2] = ty
              return CXChildVisit_Recurse
            elif clang.getCursorKind(cursor) == CXCursor_EnumConstantDecl:
              let
                cs = clang.getCursorSpelling(cursor)
                name = clang.getString(cs)
                enum_val = clang.getEnumConstantDeclUnsignedValue(cursor)
                value = llvm.constInt(cast[ptr (ref Driver, ref Node, llvm.TypeRef)](client_data)[][2], culong(enum_val), 0)
              var variable: ref CgVar
              new variable
              variable[] = initCgValue(value)
              cg.bind_var(name.toNonFullPath.mangled, variable)
          return CXChildVisit_Continue

        let
          lang = self.node.use_extern.lang
          file = self.node.use_extern.file
        if lang != "C":
          raise newError(errkUnrecognizedLanguage, self.meta.span)

        var data = (driver, self, llvm.TypeRef(nil))
        let
          (ts, cxtu) = index.load_c_with_span(file, self.meta.span)
          root = clang.getTranslationUnitCursor(cxtu)
        if ts < tsCodeGen:
          clang.visitChildren(root, visitor, cast[ptr cchar](addr data))
          index.update_c_tu(file, tsCodeGen)
      ),
      visit_macro: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        return vrkContinue),
      visit_function: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.function.type_params.isSome:
          return vrkContinue
        if self.meta.attributes.isSome and self.meta.attributes.get.any((attr: Attribute) => attr.kind == akComptime):
          return vrkContinue
        if driver.test_mode and self.node.function.name.get.mangled.get == "main" and not (self.meta.attributes.isSome and self.meta.attributes.get.any((attr: Attribute) => attr.kind == akTestMain)):
          return vrkContinue
        if not driver.test_mode and self.meta.attributes.isSome and self.meta.attributes.get.any((attr: Attribute) => attr.kind == akTest):
          return vrkContinue
        if not driver.test_mode and self.meta.attributes.isSome and self.meta.attributes.get.any((attr: Attribute) => attr.kind == akTestMain):
          return vrkContinue
        return vrkRecurse),
    )
    post_visitor = HlirVisitor(
      visit_enum_variant: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.value[] = initCgValue(llvm.constInt(
          self.meta.type_ref,
          culong(self.node.enum_variant.value.get.node.expr_int.value),
          0,
        ))
        self.meta.is_const = true
        cg.bind_var(self.node.enum_variant.name.get.mangled.get, self.meta.value)
        return vrkRecurse),
      visit_expr_unit: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.value[] = initCgUnit()
        self.meta.is_const = true
        return vrkRecurse),
      visit_expr_bool: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.value[] = initCgValue(llvm.constInt(
          self.meta.type_ref,
          if self.node.expr_bool.value: 1 else: 0,
          0,
        ))
        self.meta.is_const = true
        return vrkRecurse),
      visit_expr_int: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.value[] = initCgValue(llvm.constInt(
          self.meta.type_ref,
          culong(self.node.expr_int.value),
          if self.meta.type_sum[].as_qualified_in_span(self.meta.span)[].inner.kind == tykSigned: 1 else: 0,
        ))
        self.meta.is_const = true
        return vrkRecurse),
      visit_expr_float: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.value[] = initCgValue(llvm.constReal(
          self.meta.type_ref,
          cdouble(self.node.expr_float.value),
        ))
        self.meta.is_const = true
        return vrkRecurse),
      visit_expr_string: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let
          uint = llvm.intTypeInContext(cg.ctx, cuint(driver.target.pointer_width))
          schar = llvm.intTypeInContext(cg.ctx, 8)
          length = self.node.expr_string.value.len + 1

        var elems = newSeqOfCap[llvm.ValueRef](length)
        for ch in self.node.expr_string.value:
          elems.add(llvm.constInt(schar, culong(ch), 0))
        elems.add(llvm.constInt(schar, culong(0), 0))

        let
          elemv = addr elems[0]
          arr = llvm.constArray(schar, elemv, cuint(elems.len))
          global = llvm.addGlobal(cg.module, llvm.arrayType(schar, cuint(length)), cstring("str"))
          len = llvm.constInt(uint, culong(length - 1), 0)
          str = llvm.constPointerCast(global, llvm.pointerType(schar, 0))
        llvm.setLinkage(global, PrivateLinkage)
        llvm.setGlobalConstant(global, 1)
        llvm.setInitializer(global, arr)

        var fields = [len, str]

        self.meta.value[] = initCgValue(llvm.constNamedStruct(self.meta.type_ref, addr fields[0], cuint(fields.len)))
        self.meta.is_const = true
        return vrkRecurse),
      visit_expr_sizeof: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let s = self.node.expr_sizeof.value.meta.own_type.get.get_size_of(cg.type_ctx)
        var size: uint
        case s.kind
        of sokUnknown:
          raise newError(errkUnknownSize, self.meta.span, $self.node.expr_sizeof.value.meta.own_type.get[])
        of sokUnsized:
          raise newError(errkUnknownSize, self.meta.span, $self.node.expr_sizeof.value.meta.own_type.get[])
        of sokSized:
          size = s.size
        self.meta.value[] = initCgValue(llvm.constInt(self.meta.type_ref, culong(size), 0))
        self.meta.is_const = true
        return vrkRecurse),
      visit_expr_alignof: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let s = self.node.expr_alignof.value.meta.own_type.get.get_align_of(cg.type_ctx)
        var align: uint
        case s.kind
        of sokUnknown:
          raise newError(errkUnknownSize, self.meta.span, $self.node.expr_alignof.value.meta.own_type.get[])
        of sokUnsized:
          raise newError(errkUnknownSize, self.meta.span, $self.node.expr_alignof.value.meta.own_type.get[])
        of sokSized:
          align = s.size
        self.meta.value[] = initCgValue(llvm.constInt(self.meta.type_ref, culong(align), 0))
        self.meta.is_const = true
        return vrkRecurse),
      visit_expr_array: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        var elems = newSeq[llvm.ValueRef]()
        for elem in self.node.expr_array.elems:
          if elem.meta.value[].kind == cvkUninit:
            return
          else:
            elems.add(elem.meta.value[].load_with_builder(cg.builder))

        let elemv = if elems.len != 0: addr elems[0] else: nil
        case self.meta.type_sum[].as_qualified_in_span(self.meta.span).inner.kind
        of tykArray:
          self.meta.value[] = initCgValue(llvm.constArray(llvm.getElementType(self.meta.type_ref), elemv, cuint(elems.len)))
          self.meta.is_const = true
        of tykSlice:
          let elem_type = self.meta.type_sum[].as_qualified.elem[].as_qualified.to_first_class_cg_type_with_span(self.meta.span, cg, target)
          let
            arr = llvm.constArray(elem_type, elemv, cuint(elems.len))
            global = llvm.addGlobal(cg.module, self.meta.type_ref, cstring("arr"))
          llvm.setLinkage(global, PrivateLinkage)
          llvm.setInitializer(global, arr)
          let
            uint = cg.get_type("uint".toPath.mangled_as_type).get
            len = llvm.constInt(uint, culong(elems.len), 0)
          var vals = [len, llvm.constPointerCast(global, llvm.pointerType(elem_type, 0))]
          let
            slice = llvm.constStructInContext(cg.ctx, addr vals[0], 2, 0)
          self.meta.value[] = initCgValue(slice)
          self.meta.is_const = true
        else:
          raise newError(errkInvalidType, self.meta.span, "not an array or slice type")
        return vrkRecurse),
      visit_expr_parenth: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.expr_parenth.elems.len == 1:
          self.meta.is_const = self.node.expr_parenth.elems[0].meta.is_const
          self.meta.value = self.node.expr_parenth.elems[0].meta.value
        return vrkRecurse),
      visit_expr_cast: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if not self.node.expr_cast.value.meta.is_const:
          return vrkRecurse

        let value = self.node.expr_cast.value.meta.value[].get(self.meta.span)
        let to = self.node.expr_cast.ty.meta.own_type.get
        let frm = self.node.expr_cast.value.meta.type_sum[].as_qualified_in_span(self.node.expr_cast.value.meta.span)
        case frm.inner.kind
        of tykSigned:
          case to.inner.kind
          of tykSigned:
            let frm_bits = if frm.inner.signed.bits.kind == bitsCustom: frm.inner.signed.bits.bits else: target.pointer_width.uint8
            let to_bits = if to.inner.signed.bits.kind == bitsCustom: to.inner.signed.bits.bits else: target.pointer_width.uint8
            if to_bits > frm_bits:
              self.meta.value[] = initCgValue(llvm.constSExt(value, self.meta.type_ref))
            else:
              self.meta.value[] = initCgValue(llvm.constTrunc(value, self.meta.type_ref))
          of tykUnsigned:
            let frm_bits = if frm.inner.signed.bits.kind == bitsCustom: frm.inner.signed.bits.bits else: target.pointer_width.uint8
            let to_bits = if to.inner.unsigned.bits.kind == bitsCustom: to.inner.unsigned.bits.bits else: target.pointer_width.uint8
            if to_bits > frm_bits:
              self.meta.value[] = initCgValue(llvm.constSExt(value, self.meta.type_ref))
            else:
              self.meta.value[] = initCgValue(llvm.constTrunc(value, self.meta.type_ref))
          of tykFloating:
            self.meta.value[] = initCgValue(llvm.constSIToFp(value, self.meta.type_ref))
          of tykPointer:
            if frm.inner.signed.bits.kind == bitsWord:
              self.meta.value[] = initCgValue(llvm.constPtrToInt(value, self.meta.type_ref))
          else:
            raise newIce("invalid cast")
        of tykUnsigned:
          case to.inner.kind
          of tykSigned:
            let frm_bits = if frm.inner.unsigned.bits.kind == bitsCustom: frm.inner.unsigned.bits.bits else: target.pointer_width.uint8
            let to_bits = if to.inner.signed.bits.kind == bitsCustom: to.inner.signed.bits.bits else: target.pointer_width.uint8
            if to_bits > frm_bits:
              self.meta.value[] = initCgValue(llvm.constZExt(value, self.meta.type_ref))
            else:
              self.meta.value[] = initCgValue(llvm.constTrunc(value, self.meta.type_ref))
          of tykUnsigned:
            let frm_bits = if frm.inner.unsigned.bits.kind == bitsCustom: frm.inner.unsigned.bits.bits else: target.pointer_width.uint8
            let to_bits = if to.inner.unsigned.bits.kind == bitsCustom: to.inner.unsigned.bits.bits else: target.pointer_width.uint8
            if to_bits > frm_bits:
              self.meta.value[] = initCgValue(llvm.constZExt(value, self.meta.type_ref))
            else:
              self.meta.value[] = initCgValue(llvm.constTrunc(value, self.meta.type_ref))
          of tykFloating:
            self.meta.value[] = initCgValue(llvm.constUIToFp(value, self.meta.type_ref))
          of tykPointer:
            if frm.inner.unsigned.bits.kind == bitsWord:
              self.meta.value[] = initCgValue(llvm.constPtrToInt(value, self.meta.type_ref))
          else:
            raise newIce("invalid cast")
        of tykFloating:
          case to.inner.kind
          of tykSigned:
            self.meta.value[] = initCgValue(llvm.constFPToSI(value, self.meta.type_ref))
          of tykUnsigned:
            self.meta.value[] = initCgValue(llvm.constFPToUI(value, self.meta.type_ref))
          of tykFloating:
            self.meta.value[] = initCgValue(llvm.constFPCast(value, self.meta.type_ref))
          else:
            raise newIce("invalid cast")
        of tykPointer:
          case to.inner.kind
          of tykSigned:
            if to.inner.signed.bits.kind == bitsWord:
              self.meta.value[] = initCgValue(llvm.constPtrToInt(value, self.meta.type_ref))
          of tykUnsigned:
            if to.inner.unsigned.bits.kind == bitsWord:
              self.meta.value[] = initCgValue(llvm.constPtrToInt(value, self.meta.type_ref))
          of tykPointer:
            self.meta.value[] = initCgValue(llvm.constPointerCast(value, self.meta.type_ref))
          else:
            raise newIce("invalid cast")
        else:
          raise newIce("invalid cast")

        self.meta.is_const = true

        return vrkRecurse),
      visit_expr_unary: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if not self.node.expr_unary.value.meta.is_const:
          return vrkRecurse

        case self.node.expr_unary.op
        of hukPos:
          self.meta.value[] = initCgValue(self.node.expr_unary.value.meta.value[].load_with_builder(cg.builder))
        of hukNeg:
          if self.node.expr_unary.value.meta.type_sum[].as_qualified_in_span(self.node.expr_unary.value.meta.span)[].is_floating:
            let
              v = self.node.expr_unary.value.meta.value[].load_with_builder(cg.builder)
              vneg = llvm.buildFNeg(cg.builder, v, cstring(""))
            self.meta.value[] = initCgValue(vneg)
          else:
            let
              v = self.node.expr_unary.value.meta.value[].load_with_builder(cg.builder)
              vneg = llvm.buildNeg(cg.builder, v, cstring(""))
            self.meta.value[] = initCgValue(vneg)
        of hukNot:
          let
            v = self.node.expr_unary.value.meta.value[].load_with_builder(cg.builder)
            vneg = llvm.buildNot(cg.builder, v, cstring(""))
          self.meta.value[] = initCgValue(vneg)
        else:
          return vrkRecurse

        self.meta.is_const = true

        return vrkRecurse),
      visit_expr_binary: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if not self.node.expr_binary.lhs.meta.is_const or not self.node.expr_binary.rhs.meta.is_const:
          return vrkRecurse

        proc binary(self: ref Node, const_fn: proc (lhs: llvm.ValueRef, rhs: llvm.ValueRef): llvm.ValueRef) {.closure.} =
          self.meta.value[] = initCgValue(const_fn(self.node.expr_binary.lhs.meta.value[].get(self.meta.span), self.node.expr_binary.rhs.meta.value[].get(self.meta.span)))

        proc icmp(self: ref Node, cmp: llvm.IntPredicate) {.closure.} =
          self.meta.value[] = initCgValue(llvm.constICmp(cmp, self.node.expr_binary.lhs.meta.value[].get(self.meta.span), self.node.expr_binary.rhs.meta.value[].get(self.meta.span)))

        proc fcmp(self: ref Node, cmp: llvm.RealPredicate) {.closure.} =
          self.meta.value[] = initCgValue(llvm.constFCmp(cmp, self.node.expr_binary.lhs.meta.value[].get(self.meta.span), self.node.expr_binary.rhs.meta.value[].get(self.meta.span)))

        case self.node.expr_binary.op
        of hbkAdd:
          if self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_floating:
            binary(self, llvm.constFAdd)
          else:
            binary(self, llvm.constAdd)
        of hbkSub:
          if self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_floating:
            binary(self, llvm.constFSub)
          else:
            binary(self, llvm.constSub)
        of hbkMul:
          if self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_floating:
            binary(self, llvm.constFMul)
          else:
            binary(self, llvm.constMul)
        of hbkDiv:
          if self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_floating:
            binary(self, llvm.constFDiv)
          elif self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_unsigned:
            binary(self, llvm.constUDiv)
          else:
            binary(self, llvm.constSDiv)
        of hbkRem:
          if self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_floating:
            binary(self, llvm.constFRem)
          elif self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_unsigned:
            binary(self, llvm.constURem)
          else:
            binary(self, llvm.constSRem)
        of hbkShl: binary(self, llvm.constShl)
        of hbkShr: binary(self, llvm.constAShr)
        of hbkLt:
          if self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_floating:
            fcmp(self, llvm.RealULT)
          elif self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_unsigned:
            icmp(self, llvm.IntULT)
          else:
            icmp(self, llvm.IntSLT)
        of hbkGt:
          if self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_floating:
            fcmp(self, llvm.RealUGT)
          elif self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_unsigned:
            icmp(self, llvm.IntUGT)
          else:
            icmp(self, llvm.IntSGT)
        of hbkLe:
          if self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_floating:
            fcmp(self, llvm.RealULE)
          elif self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_unsigned:
            icmp(self, llvm.IntULE)
          else:
            icmp(self, llvm.IntSLE)
        of hbkGe:
          if self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_floating:
            fcmp(self, llvm.RealUGE)
          elif self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_unsigned:
            icmp(self, llvm.IntUGE)
          else:
            icmp(self, llvm.IntSGE)
        of hbkEq:
          if self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_floating:
            fcmp(self, llvm.RealUEQ)
          else:
            icmp(self, llvm.IntEQ)
        of hbkNe:
          if self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_floating:
            fcmp(self, llvm.RealUNE)
          else:
            icmp(self, llvm.IntNE)
        of hbkBitand: binary(self, llvm.constAnd)
        of hbkBitor: binary(self, llvm.constOr)
        of hbkBitxor: binary(self, llvm.constXor)
        else:
          return vrkRecurse

        self.meta.is_const = true

        return vrkRecurse),
    )
  discard accept(root, pre_visitor, post_visitor)

proc const_value_gen*(index: ref Index, driver: ref Driver, node: ref Node, root: ref Node) =
  if root.meta.impl_scope.isSome:
    driver.type_ctx.enter_this_bounded(root.meta.impl_scope.get)
  else:
    driver.type_ctx.enter_bounded
  const_value_gen(index, driver, node)
  root.meta.impl_scope = some(driver.type_ctx.leave_this_bounded)

proc decl*(index: ref Index, driver: ref Driver, root: ref Node) =
  let cg = driver.pass_controllers[tsCodeDecl].CgPassController
  let target = driver.target
  let
    pre_visitor = HlirVisitor(
      visit_use_extern: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        proc visitor(cursor: CXCursor, parent: CXCursor, client_data: CXClientData): CXChildVisitResult {.cdecl.} =
          let
            driver = cast[ptr (ref Driver, ref Node)](client_data)[][0]
            self = cast[ptr (ref Driver, ref Node)](client_data)[][1]
            cg = driver.pass_controllers[tsCodeDecl].CgPassController
          if clang.isDeclaration(clang.getCursorKind(cursor)) != 0:
            let
              args = clang.cursor_getNumArguments(cursor)
              storage = clang.cursor_getStorageClass(cursor)
              is_extern = storage == CX_SC_Extern
              is_static = storage == CX_SC_Static
              cx_type = clang.getCursorType(cursor)
              canonical = clang.getCanonicalType(cx_type)

            if args >= 0:
              # function declaration
              if not is_static:
                let
                  cs = clang.getCursorSpelling(cursor)
                  name = clang.getString(cs)

                let
                  function_type = canonical.to_type(driver.target)
                    .to_cg_type_with_span(self.meta.span, cg, driver.target)
                  function = llvm.addFunction(cg.module, cstring(name), function_type)
                var function_var: ref CgVar
                new function_var
                function_var[] = initCgValue(function)
                cg.bind_var(name, function_var)
            elif clang.getCursorKind(cursor) != CXCursor_StructDecl and
                clang.getCursorKind(cursor) != CXCursor_UnionDecl and
                clang.getCursorKind(cursor) != CXCursor_EnumDecl and
                clang.getCursorKind(cursor) != CXCursor_TypedefDecl and
                not is_static:
              let
                cs = clang.getCursorSpelling(cursor)
                name = clang.getString(cs)

              let
                global_type = canonical.to_type(driver.target)
                  .to_first_class_cg_type_with_span(self.meta.span, cg, driver.target)
                global = llvm.addGlobal(cg.module, global_type, cstring(name))
              var global_var: ref CgVar
              new global_var
              global_var[] = initCgPointer(global)
              cg.bind_var(name, global_var)
          return CXChildVisit_Continue

        let
          lang = self.node.use_extern.lang
          file = self.node.use_extern.file
        if lang != "C":
          raise newError(errkUnrecognizedLanguage, self.meta.span)

        var data = (driver, self)
        let
          (ts, cxtu) = index.load_c_with_span(file, self.meta.span)
          root = clang.getTranslationUnitCursor(cxtu)
        if ts < tsComplete:
          clang.visitChildren(root, visitor, cast[ptr cchar](addr data))
          index.update_c_tu(file, tsComplete)
      ),
      visit_function: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.function.type_params.isSome:
          return vrkContinue
        if self.meta.attributes.isSome and self.meta.attributes.get.any((attr: Attribute) => attr.kind == akComptime):
          return vrkContinue
        if driver.test_mode and self.node.function.name.get.mangled.get == "main" and not (self.meta.attributes.isSome and self.meta.attributes.get.any((attr: Attribute) => attr.kind == akTestMain)):
          return vrkContinue
        if not driver.test_mode and self.meta.attributes.isSome and self.meta.attributes.get.any((attr: Attribute) => attr.kind == akTest):
          return vrkContinue
        if not driver.test_mode and self.meta.attributes.isSome and self.meta.attributes.get.any((attr: Attribute) => attr.kind == akTestMain):
          return vrkContinue

        if self.node.function.name.get.mangled.isNone:
          raise newIce("unmangled identifier")

        let fn = llvm.addFunction(cg.module, cstring(self.node.function.name.get.mangled.get), self.node.function.ty.meta.type_ref)
        self.meta.value[] = initCgValue(fn)
        self.meta.type_ref = self.node.function.ty.meta.type_ref.to_first_class
        if driver.debug_info:
          self.meta.meta_type = self.node.function.ty.meta.own_type.get.di_qualified_type(self.node.function.ty.meta.span, cg, target)
        if self.node.function.is_closure:
          cg.bind_var(self.node.function.name.get.mangled.get, self.meta.value, 0)
        else:
          cg.bind_var(self.node.function.name.get.mangled.get, self.meta.value)
        cg.enter_declare(fn)

        return vrkRecurse),
      visit_macro: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        return vrkContinue),
      visit_static: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.static_global.name.get.mangled.isNone:
         raise newIce("unmangled identifier")

        let global = llvm.addGlobal(cg.module, self.meta.type_ref, cstring(""))
        self.meta.value[] = initCgPointer(global)
        cg.bind_var(self.node.static_global.name.get.mangled.get, self.meta.value)
        return vrkRecurse),
      visit_static_extern: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.static_extern.name.get.mangled.isNone:
         raise newIce("unmangled identifier")

        let global = llvm.addGlobal(cg.module, self.meta.type_ref, cstring(""))
        llvm.setLinkage(global, ExternalLinkage)
        self.meta.value[] = initCgPointer(global)
        cg.bind_var(self.node.static_extern.name.get.mangled.get, self.meta.value)
        return vrkRecurse),
      visit_define: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.define.name.get.mangled.isNone:
         raise newIce("unmangled identifier")

        if not self.node.define.value.meta.is_const:
          raise newError(errkNotConst, self.node.define.value.meta.span)

        case self.node.define.ty.get.meta.own_type.get.inner.kind
        of tykSigned:
          self.meta.value[] = initCgValue(llvm.constIntCast(self.node.define.value.meta.value.value, self.node.define.ty.get.meta.type_ref, 1))
          cg.bind_var(self.node.define.name.get.mangled.get, self.meta.value)
        of tykUnsigned:
          self.meta.value[] = initCgValue(llvm.constIntCast(self.node.define.value.meta.value.value, self.node.define.ty.get.meta.type_ref, 0))
          cg.bind_var(self.node.define.name.get.mangled.get, self.meta.value)
        of tykFloating:
          self.meta.value[] = initCgValue(llvm.constFPCast(self.node.define.value.meta.value.value, self.node.define.ty.get.meta.type_ref))
          cg.bind_var(self.node.define.name.get.mangled.get, self.meta.value)
        else:
          self.meta.value[] = self.node.define.value.meta.value[]
          cg.bind_var(self.node.define.name.get.mangled.get, self.meta.value)
        return vrkRecurse),
    )
    post_visitor = HlirVisitor(
      visit_function: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let (bindings, types) = cg.leave_this_declare()
        self.meta.cg_bindings_scope = some(bindings)
        self.meta.cg_types_scope = some(types)
        return vrkRecurse),
    )
  discard accept(root, pre_visitor, post_visitor)

proc decl*(index: ref Index, driver: ref Driver, node: ref Node, root: ref Node) =
  if root.meta.impl_scope.isSome:
    driver.type_ctx.enter_this_bounded(root.meta.impl_scope.get)
  else:
    driver.type_ctx.enter_bounded
  decl(index, driver, node)
  root.meta.impl_scope = some(driver.type_ctx.leave_this_bounded)

proc collect_vars*(index: ref Index, driver: ref Driver, root: ref Node) =
  let cg = driver.pass_controllers[tsCodeDecl].CgPassController
  let target = driver.target
  let
    pre_visitor = HlirVisitor(
      visit_use: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let module = self.node.use.path
        let unit = index.load_with_span(module, "amp", self.meta.span)
        if index.compiling(unit):
          driver.run_until(index, unit, tsCodeGen)
      ),
      visit_function: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.function.type_params.isSome:
          return vrkContinue
        if self.meta.attributes.isSome and self.meta.attributes.get.any((attr: Attribute) => attr.kind == akComptime):
          return vrkContinue
        if driver.test_mode and self.node.function.name.get.mangled.get == "main" and not (self.meta.attributes.isSome and self.meta.attributes.get.any((attr: Attribute) => attr.kind == akTestMain)):
          return vrkContinue
        if not driver.test_mode and self.meta.attributes.isSome and self.meta.attributes.get.any((attr: Attribute) => attr.kind == akTest):
          return vrkContinue
        if not driver.test_mode and self.meta.attributes.isSome and self.meta.attributes.get.any((attr: Attribute) => attr.kind == akTestMain):
          return vrkContinue

        if self.node.function.name.get.mangled.isNone:
          raise newIce("unmangled identifier")

        if self.node.function.body.isSome:
          cg.enter_new_fn(self.meta.value.value, self.meta.cg_bindings_scope.get, self.meta.cg_types_scope.get)

          let
            filename = self.meta.span.file
            directory = self.meta.span.directory
            name = self.node.function.name.get.mangled.get

          if driver.debug_info:
            cg.di_file = some(llvm.diBuilderCreateFile(
              cg.di_builder,
              filename.cstring,
              filename.len.uint,
              directory.cstring,
              directory.len.uint,
            ))
            let di_function = llvm.diBuilderCreateFunction(
                cg.di_builder,
                cg.di_file.get,
                name.cstring,
                name.len.uint,
                name.cstring,
                name.len.uint,
                cg.di_file.get,
                self.meta.span.line.cuint,
                self.meta.meta_type,
                0,
                1,
                self.node.function.body.get.meta.span.line.cuint,
                0,
                0,
            )

            self.meta.meta_data = di_function
            cg.enter_di_function(some(di_function))
            llvm.setSubprogram(cg.function, di_function)
        else:
          cg.enter_this_declare(self.meta.value.value, self.meta.cg_bindings_scope.get, self.meta.cg_types_scope.get)

        return vrkRecurse),
      visit_macro: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        return vrkContinue),
    )
    post_visitor = HlirVisitor(
      visit_function: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.function.body.isSome:
          if driver.debug_info:
            cg.leave_di_function
          let (bindings, types) = cg.leave_this_fn()
          self.meta.cg_bindings_scope = some(bindings)
          self.meta.cg_types_scope = some(types)
          if cg.has_bb:
            llvm.positionBuilderAtEnd(cg.builder, cg.bb)
        else:
          let (bindings, types) = cg.leave_this_declare()
          self.meta.cg_bindings_scope = some(bindings)
          self.meta.cg_types_scope = some(types)
        return vrkRecurse),
      visit_expr_let: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.expr_let.name.get.mangled.isNone:
         raise newIce("unmangled identifier")

        if not self.meta.type_sum[].as_qualified_in_span(self.meta.span).is_unit:
          let lval = llvm.buildAlloca(cg.builder, self.meta.type_ref, cstring(""))
          self.meta.value[] = initCgPointer(lval)
        else:
          self.meta.value[] = initCgUnit()
        return vrkRecurse),
      visit_expr_array: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.meta.is_const:
          return vrkRecurse

        let arrayp = llvm.buildAlloca(cg.builder, self.meta.type_ref, cstring(""))
        self.meta.value[] = initCgPointer(arrayp)
        return vrkRecurse),
      visit_expr_range_full: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let name = self.meta.type_sum[].as_qualified_in_span(self.meta.span).mangled
        var range_type = cg.get_type(name)
        let structp = llvm.buildAlloca(cg.builder, range_type.get, cstring(""))
        self.meta.value[] = initCgPointer(structp)
        return vrkRecurse),
      visit_expr_unary: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        case self.node.expr_unary.op
        of hukRangeTo:
          let name = self.meta.type_sum[].as_qualified_in_span(self.meta.span).mangled
          var range_type = cg.get_type(name)

          let structp = llvm.buildAlloca(cg.builder, range_type.get, cstring(""))
          self.meta.value[] = initCgPointer(structp)
        of hukRangeFrom:
          let name = self.meta.type_sum[].as_qualified_in_span(self.meta.span).mangled
          var range_type = cg.get_type(name)

          let structp = llvm.buildAlloca(cg.builder, range_type.get, cstring(""))
          self.meta.value[] = initCgPointer(structp)
        else:
          discard
        return vrkRecurse),
      visit_expr_binary: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        case self.node.expr_binary.op
        of hbkIndex:
          case self.node.expr_binary.lhs.meta.type_sum[].as_qualified_in_span(self.node.expr_binary.lhs.meta.span).inner.kind
          of tykArray:
            let rhs_ty = self.node.expr_binary.rhs.meta.type_sum[].as_qualified_in_span(self.node.expr_binary.rhs.meta.span)
            case rhs_ty.inner.kind
            of tykStruct:
              if rhs_ty.inner.struct.name.isSome:
                if rhs_ty.inner.struct.name.get == ["__lang", "builtin", "Range"].toPath:
                  let
                    uint = cg.get_type("uint".toPath.mangled_as_type).get
                    pt = llvm.pointerType(self.meta.type_sum[].elem[].as_qualified_in_span(self.meta.span).to_first_class_cg_type_with_span(self.meta.span, cg, target), 0)

                  var fields = [uint, pt]
                  let
                    slice_t = llvm.structTypeInContext(cg.ctx, addr fields[0], cuint(fields.len), 0)
                    slice_p = llvm.buildAlloca(cg.builder, slice_t, cstring(""))
                  self.meta.value[] = initCgPointer(slice_p)
                elif rhs_ty.inner.struct.name.get == ["__lang", "builtin", "RangeFrom"].toPath:
                  let
                    uint = cg.get_type("uint".toPath.mangled_as_type).get
                    pt = llvm.pointerType(self.meta.type_sum[].elem[].as_qualified_in_span(self.meta.span).to_first_class_cg_type_with_span(self.meta.span, cg, target), 0)

                  var fields = [uint, pt]
                  let
                    slice_t = llvm.structTypeInContext(cg.ctx, addr fields[0], cuint(fields.len), 0)
                    slice_p = llvm.buildAlloca(cg.builder, slice_t, cstring(""))
                  self.meta.value[] = initCgPointer(slice_p)
                elif rhs_ty.inner.struct.name.get == ["__lang", "builtin", "RangeTo"].toPath:
                  let
                    uint = cg.get_type("uint".toPath.mangled_as_type).get
                    pt = llvm.pointerType(self.meta.type_sum[].elem[].as_qualified_in_span(self.meta.span).to_first_class_cg_type_with_span(self.meta.span, cg, target), 0)

                  var fields = [uint, pt]
                  let
                    slice_t = llvm.structTypeInContext(cg.ctx, addr fields[0], cuint(fields.len), 0)
                    slice_p = llvm.buildAlloca(cg.builder, slice_t, cstring(""))
                  self.meta.value[] = initCgPointer(slice_p)
                elif rhs_ty.inner.struct.name.get == ["__lang", "builtin", "RangeFull"].toPath:
                  let
                    uint = cg.get_type("uint".toPath.mangled_as_type).get
                    pt = llvm.pointerType(self.meta.type_sum[].elem[].as_qualified_in_span(self.meta.span).to_first_class_cg_type_with_span(self.meta.span, cg, target), 0)

                  var fields = [uint, pt]
                  let
                    slice_t = llvm.structTypeInContext(cg.ctx, addr fields[0], cuint(fields.len), 0)
                    slice_p = llvm.buildAlloca(cg.builder, slice_t, cstring(""))
                  self.meta.value[] = initCgPointer(slice_p)
            else:
              discard
          else:
            discard
        of hbkRange:
          let name = self.meta.type_sum[].as_qualified_in_span(self.meta.span).mangled
          var range_type = cg.get_type(name)
          if range_type.isNone:
            let range_generic = cg.type_ctx.get(["__lang", "builtin", "Range"].toPath).deepCopy
            range_generic.replace_param("T", self.node.expr_binary.lhs.meta.type_sum[].as_qualified_in_span(self.node.expr_binary.lhs.meta.span))
            range_type = some(range_generic.to_first_class_cg_type_with_span(self.meta.span, cg, target))
            cg.bind_type(range_generic.mangled, range_type.get)

          let structp = llvm.buildAlloca(cg.builder, range_type.get, cstring(""))
          self.meta.value[] = initCgPointer(structp)
        of hbkLogicalAnd:
          let t = self.meta.type_ref
          let lval = llvm.buildAlloca(cg.builder, t, cstring(""))
          self.meta.value[] = initCgPointer(lval)
        of hbkLogicalOr:
          let t = self.meta.type_ref
          let lval = llvm.buildAlloca(cg.builder, t, cstring(""))
          self.meta.value[] = initCgPointer(lval)
        else:
          discard
        return vrkRecurse),
      visit_expr_ternary: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        case self.node.expr_ternary.op
        of htkCond:
          let t = self.meta.type_ref
          let lval = llvm.buildAlloca(cg.builder, t, cstring(""))
          self.meta.value[] = initCgPointer(lval)
        return vrkRecurse),
      visit_expr_application: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if not self.is_type and self.meta.impl.isSome:
          case self.meta.impl.get.kind
          of ikCons:
            let lval = llvm.buildAlloca(cg.builder, self.meta.type_ref, cstring(""))
            self.meta.value[] = initCgPointer(lval)
          else:
            discard

        if self.meta.impl.isNone or self.meta.impl.get.kind != ikCons:
          let fn = self.node.expr_application.fn

          var args = newSeq[llvm.ValueRef]()
          if self.meta.impl.isSome and self.meta.impl.get.ampvar.isSome:
            let
              variadic = self.node.expr_application.args.len - self.meta.impl.get.params.len + 1
              var_ty = self.meta.impl.get.ampvar.get[].as_qualified.to_first_class_cg_type_with_span(self.meta.span, cg, target)
              uint = cg.get_type("uint".toPath.mangled_as_type).get
            var fields = @[uint]
            if not self.meta.impl.get.ampvar.get[].as_qualified.is_unit:
              for _ in 0 ..< variadic:
                fields.add(var_ty)

            let
              var_struct = llvm.structTypeInContext(cg.ctx, addr fields[0], cuint(fields.len), 0)
            let vararr = llvm.buildAlloca(cg.builder, var_struct, cstring(""))
            self.meta.vararr = some(vararr)
        return vrkRecurse),
      visit_expr_cast: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let to = self.node.expr_cast.ty.meta.own_type.get
        let frm = self.node.expr_cast.value.meta.type_sum[].as_qualified_in_span(self.node.expr_cast.value.meta.span)
        case frm.inner.kind
        of tykPointer:
          case to.inner.kind
          of tykTrait:
            let p = llvm.buildAlloca(cg.builder, self.meta.type_ref, cstring(""))
            self.meta.value[] = initCgPointer(p)
          else:
            discard
        else:
          discard
        return vrkRecurse),
    )
  discard accept(root, pre_visitor, post_visitor)

proc collect_vars*(index: ref Index, driver: ref Driver, node: ref Node, root: ref Node) =
  if root.meta.impl_scope.isSome:
    driver.type_ctx.enter_this_bounded(root.meta.impl_scope.get)
  else:
    driver.type_ctx.enter_bounded
  collect_vars(index, driver, node)
  root.meta.impl_scope = some(driver.type_ctx.leave_this_bounded)

proc dump_location(cg: CgPassController, self: ref Node)

proc code_gen*(index: ref Index, driver: ref Driver, root: ref Node) =
  let cg = driver.pass_controllers[tsCodeDecl].CgPassController
  let target = driver.target
  proc pre_cg_checks(self: ref Node, parent: ref Node) {.closure.} =
    if driver.debug_info:
      dump_location(cg, self)
    if self.meta.in_block.isSome:
      self.meta.saved_block = some(cg.bb)
      cg.bb = self.meta.in_block.get
      llvm.positionBuilderAtEnd(cg.builder, cg.bb)

  proc post_cg_checks(self: ref Node, parent: ref Node) {.closure.} =
    if driver.debug_info:
      dump_location(cg, self)
    if self.meta.store_at.isSome:
      self.meta.store_at.get[].store_with_builder(self.meta.value[], cg.builder, self.meta.span)
    if self.meta.br.isSome and self.meta.returns != fbYes and self.meta.breaks != fbYes and self.meta.continues != fbYes:
      discard llvm.buildBr(cg.builder, self.meta.br.get)
    if self.meta.cond_br.isSome:
      discard llvm.buildCondBr(cg.builder, self.meta.cond_br.get.value[].load_with_builder(cg.builder), self.meta.cond_br.get.t, self.meta.cond_br.get.f)
    if self.meta.make_switch.isSome:
      let
        ty = self.meta.type_sum[].as_qualified_in_span(self.meta.span)
        el = self.meta.make_switch.get.el

      var deref = self.meta.type_sum[].as_qualified_in_span(self.meta.span)
      var ptr_depth = 0
      while deref.inner.kind == tykPointer:
        deref = deref.pointee[].as_qualified_in_span(self.meta.span)
        inc ptr_depth

      case deref.inner.kind
      of tykSigned, tykUnsigned, tykEnum:
        var
          pvalue: ref CgVar
          value: ref CgVar
          switch: ref CgVar
        new pvalue
        new value
        new switch

        if llvm.getTypeKind(llvm.typeOf(self.meta.value.value)) == llvm.PointerTypeKind:
          var p = self.meta.value[].value
          pvalue[] = initCgValue(p)
          if self.meta.value.kind == cvkPointer:
            for _ in 1 .. ptr_depth:
              p = llvm.buildLoad(cg.builder, p, cstring(""))
            if value.kind != cvkUninit:
              pvalue[] = value[]
            value[] = initCgPointer(p)
          else:
            for _ in 1 ..< ptr_depth:
              p = llvm.buildLoad(cg.builder, p, cstring(""))
            if value.kind != cvkUninit:
              pvalue[] = value[]
            value[] = initCgPointer(p)
        else:
          value[] = initCgValue(self.meta.value.value)
          pvalue[] = initCgValue(self.meta.value.value)

        switch[] = initCgValue(llvm.buildSwitch(cg.builder, value[].load_with_builder(cg.builder), el, self.meta.make_switch.get.cases))
        cg.add_pattern((pvalue, ty, true, 0, none[string]()))
        cg.add_switch(switch)
      of tykStruct:
        raise newIce("unimplemented")
      of tykTagged:
        var
          value: ref CgVar
          switch: ref CgVar
          tag: llvm.ValueRef
        new value
        new switch

        if llvm.getTypeKind(llvm.typeOf(self.meta.value.value)) == llvm.PointerTypeKind:
          var p = self.meta.value[].value
          if self.meta.value.kind == cvkPointer:
            for _ in 1 .. ptr_depth:
              p = llvm.buildLoad(cg.builder, p, cstring(""))
            value[] = initCgPointer(p)
          else:
            for _ in 1 ..< ptr_depth:
              p = llvm.buildLoad(cg.builder, p, cstring(""))
            value[] = initCgPointer(p)
          tag = llvm.buildStructGEP(cg.builder, value[].get_pointer(self.meta.span), cuint(deref.inner.tagged.tag_idx), cstring(""))
          switch[] = initCgValue(llvm.buildSwitch(cg.builder, llvm.buildLoad(cg.builder, tag, cstring("")), el, self.meta.make_switch.get.cases))
        else:
          value[] = initCgValue(self.meta.value.value)
          tag = llvm.buildExtractValue(cg.builder, value[].get(self.meta.span), cuint(deref.inner.tagged.tag_idx), cstring(""))
          switch[] = initCgValue(llvm.buildSwitch(cg.builder, tag, el, self.meta.make_switch.get.cases))

        cg.add_pattern((self.meta.value, ty, true, 0, none[string]()))
        cg.add_switch(switch)
      else:
        raise newError(errkInvalidMatch, self.meta.span, $ty[])
    if self.meta.saved_block.isSome:
      cg.bb = self.meta.saved_block.get
      llvm.positionBuilderAtEnd(cg.builder, cg.bb)
    if self.meta.after_block.isSome:
      cg.bb = self.meta.after_block.get
      llvm.positionBuilderAtEnd(cg.builder, cg.bb)

  proc generic_pre_visitor(self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
    pre_cg_checks(self, parent)
    return vrkRecurse

  proc generic_post_visitor(self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
    post_cg_checks(self, parent)
    return vrkRecurse

  let
    pre_visitor = HlirVisitor(
      visit_macro: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        return vrkContinue),
      visit_function: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.function.type_params.isSome:
          return vrkContinue
        if self.meta.attributes.isSome and self.meta.attributes.get.any((attr: Attribute) => attr.kind == akComptime):
          return vrkContinue
        if driver.test_mode and self.node.function.name.get.mangled.get == "main" and not (self.meta.attributes.isSome and self.meta.attributes.get.any((attr: Attribute) => attr.kind == akTestMain)):
          return vrkContinue
        if not driver.test_mode and self.meta.attributes.isSome and self.meta.attributes.get.any((attr: Attribute) => attr.kind == akTest):
          return vrkContinue
        if not driver.test_mode and self.meta.attributes.isSome and self.meta.attributes.get.any((attr: Attribute) => attr.kind == akTestMain):
          return vrkContinue

        if self.node.function.name.get.mangled.isNone:
          raise newIce("unmangled identifier")

        if self.node.function.body.isSome:
          cg.enter_this_fn(self.meta.value.value, self.meta.cg_bindings_scope.get, self.meta.cg_types_scope.get)
          if driver.debug_info:
            cg.enter_di_function(some(self.meta.meta_data))

          cg.bind_var("result", cg.get_result_var)

          var i = 0
          for param in self.node.function.ty.node.type_function.params:
            if param.node.decl.name.isSome:
              if param.node.decl.name.get.node.expr_named.name.mangled.isNone:
               raise newIce("unmangled identifier")

              if not param.meta.type_sum[].as_qualified_in_span(param.meta.span).is_unit:
                let value = llvm.getParam(self.meta.value.value, cuint(i))
                param.meta.value[] = initCgValue(value)

                if driver.debug_info:
                  let
                    dbg_addr = llvm.getNamedFunction(cg.module, cstring("llvm.dbg.addr"))
                    location = llvm.valueAsMetadata(value)
                    name = param.node.decl.name.get.node.expr_named.name.short.path[^1]
                    ty = param.meta.type_sum[].as_qualified_in_span(self.meta.span).di_qualified_type_first_class(self.meta.span, cg, target)
                    variable = llvm.diBuilderCreateParameterVariable(cg.di_builder, cg.di_scope.get, name.cstring, name.len.uint, i.cuint, cg.di_file.get, self.meta.span.line.cuint, ty, 0, DiFlagZero)
                  var
                    ops = [DW_OP_stack_value]
                  let expr = llvm.diBuilderCreateExpression(cg.di_builder, addr ops[0], ops.len.cuint)
                  var
                    args = [llvm.metadataAsValue(cg.ctx, location), llvm.metadataAsValue(cg.ctx, variable), llvm.metadataAsValue(cg.ctx, expr)]
                  dump_location(cg, param)
                  discard llvm.buildCall(cg.builder, dbg_addr, addr args[0], args.len.cuint, cstring(""))
                  llvm.setCurrentDebugLocation(cg.builder, nil)

                inc i
              else:
                param.meta.value[] = initCgUnit()
              cg.bind_var(param.node.decl.name.get.node.expr_named.name.mangled.get, param.meta.value)
        else:
          cg.enter_this_declare(self.meta.value.value, self.meta.cg_bindings_scope.get, self.meta.cg_types_scope.get)

        return vrkRecurse),
      visit_stmt_if: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        pre_cg_checks(self, parent)
        var
          then: llvm.BasicBlockRef
          el: llvm.BasicBlockRef
          after: llvm.BasicBlockRef

        if self.node.stmt_if.el.isSome or self.node.stmt_if.elifs.len > 0:
          then = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring(""))
          el = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring(""))
          if self.meta.returns != fbYes and self.meta.breaks != fbYes and self.meta.continues != fbYes:
            after = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring(""))
        else:
          then = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring(""))
          after = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring(""))
          el = after

        self.node.stmt_if.cond.meta.cond_br = some((self.node.stmt_if.cond.meta.value, then, el))
        self.node.stmt_if.body.meta.in_block = some(then)
        if self.node.stmt_if.body.meta.returns != fbYes and self.node.stmt_if.body.meta.breaks != fbYes and self.node.stmt_if.body.meta.continues != fbYes:
          self.node.stmt_if.body.meta.br = some(after)
        if self.meta.returns != fbYes and self.meta.breaks != fbYes and self.meta.continues != fbYes:
          self.node.stmt_if.body.meta.after_block = some(after)

        for i, stmt in self.node.stmt_if.elifs.mpairs:
          let
            cond = el
            then = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring(""))
          if i == self.node.stmt_if.elifs.len - 1 and self.node.stmt_if.el.isNone:
            el = after
          else:
            el = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring(""))

          stmt.cond.meta.in_block = some(cond)
          stmt.cond.meta.cond_br = some((stmt.cond.meta.value, then, el))

          stmt.body.meta.in_block = some(then)
          if stmt.body.meta.returns != fbYes and stmt.body.meta.breaks != fbYes and stmt.body.meta.continues != fbYes:
            stmt.body.meta.br = some(after)
          if self.meta.returns != fbYes and self.meta.breaks != fbYes and self.meta.continues != fbYes:
            stmt.body.meta.after_block = some(after)

        if self.node.stmt_if.el.isSome:
          self.node.stmt_if.el.get.meta.in_block = some(el)
          if self.node.stmt_if.el.get.meta.returns != fbYes and self.node.stmt_if.el.get.meta.breaks != fbYes and self.node.stmt_if.el.get.meta.continues != fbYes:
            self.node.stmt_if.el.get.meta.br = some(after)
          if self.meta.returns != fbYes and self.meta.breaks != fbYes and self.meta.continues != fbYes:
            self.node.stmt_if.el.get.meta.after_block = some(after)
        
        return vrkRecurse),
      visit_stmt_while: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        pre_cg_checks(self, parent)

        cg.enter_ctx

        let
          cond = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring(""))
          then = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring(""))
          after = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring(""))

        cg.add_loop(cond, after)

        llvm.positionBuilderAtEnd(cg.builder, then)
        let early_var = cg.get_early_var
        early_var[].store_with_builder(initCgValue(llvm.constInt(llvm.intTypeInContext(cg.ctx, 8), 0, 0)), cg.builder, self.meta.span)
        llvm.positionBuilderAtEnd(cg.builder, cg.bb)

        cg.bb = cond
        discard llvm.buildBr(cg.builder, cg.bb)
        llvm.positionBuilderAtEnd(cg.builder, cg.bb)

        self.node.stmt_while.cond.meta.cond_br = some((self.node.stmt_while.cond.meta.value, then, after))

        self.node.stmt_while.body.meta.in_block = some(then)
        self.node.stmt_while.body.meta.br = some(cond)
        self.node.stmt_while.body.meta.after_block = some(after)
        
        return vrkRecurse),
      visit_stmt_dowhile: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        pre_cg_checks(self, parent)

        cg.enter_ctx

        let
          cond = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring(""))
          then = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring(""))
          after = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring(""))

        cg.add_loop(cond, after)

        llvm.positionBuilderAtEnd(cg.builder, then)
        let early_var = cg.get_early_var
        early_var[].store_with_builder(initCgValue(llvm.constInt(llvm.intTypeInContext(cg.ctx, 8), 0, 0)), cg.builder, self.meta.span)
        llvm.positionBuilderAtEnd(cg.builder, cg.bb)

        discard llvm.buildBr(cg.builder, then)

        self.node.stmt_dowhile.cond.meta.in_block = some(cond)
        self.node.stmt_dowhile.cond.meta.cond_br = some((self.node.stmt_dowhile.cond.meta.value, then, after))

        self.node.stmt_dowhile.body.meta.in_block = some(then)
        self.node.stmt_dowhile.body.meta.after_block = some(after)
        self.node.stmt_dowhile.body.meta.br = some(cond)
        
        return vrkRecurse),
      visit_stmt_for: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        pre_cg_checks(self, parent)

        cg.enter_ctx

        let
          init = if self.node.stmt_for.init.isSome: llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring("")) else: nil
          cond = if self.node.stmt_for.cond.isSome: llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring("")) else: nil
          rep = if self.node.stmt_for.rep.isSome: llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring("")) else: nil
          then = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring(""))
          after = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring(""))

        cg.add_loop(rep, after)

        llvm.positionBuilderAtEnd(cg.builder, then)
        let early_var = cg.get_early_var
        early_var[].store_with_builder(initCgValue(llvm.constInt(llvm.intTypeInContext(cg.ctx, 8), 0, 0)), cg.builder, self.meta.span)
        llvm.positionBuilderAtEnd(cg.builder, cg.bb)

        cg.bb = init
        discard llvm.buildBr(cg.builder, cg.bb)
        llvm.positionBuilderAtEnd(cg.builder, cg.bb)

        if self.node.stmt_for.init.isSome:
          if self.node.stmt_for.cond.isSome:
            self.node.stmt_for.init.get.meta.br = some(cond)
          else:
            self.node.stmt_for.init.get.meta.br = some(then)

        if self.node.stmt_for.cond.isSome:
          self.node.stmt_for.cond.get.meta.in_block = some(cond)
          self.node.stmt_for.cond.get.meta.cond_br = some((self.node.stmt_for.cond.get.meta.value, then, after))

        if self.node.stmt_for.rep.isSome:
          self.node.stmt_for.rep.get.meta.in_block = some(rep)
          if self.node.stmt_for.cond.isSome:
            self.node.stmt_for.rep.get.meta.br = some(cond)
          else:
            self.node.stmt_for.rep.get.meta.br = some(then)

        self.node.stmt_for.body.meta.in_block = some(then)
        if self.node.stmt_for.rep.isSome:
          self.node.stmt_for.body.meta.br = some(rep)
        elif self.node.stmt_for.cond.isSome:
          self.node.stmt_for.body.meta.br = some(cond)
        else:
          self.node.stmt_for.body.meta.br = some(then)
        self.node.stmt_for.body.meta.after_block = some(after)
        
        return vrkRecurse),
      visit_stmt_match: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        pre_cg_checks(self, parent)

        let ty = self.node.stmt_match.value.meta.type_sum[].deref[].as_qualified_in_span(self.meta.span)

        var
          has_el = false
          el = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring(""))
          after: llvm.BasicBlockRef
          cases = cuint(0)

        if self.meta.returns != fbYes and self.meta.breaks != fbYes and self.meta.continues != fbYes:
          after = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring(""))

        for i, case_of in self.node.stmt_match.cases.mpairs:
          let is_el = case_of.node.case_of.pat.is_else_pattern
          if is_el:
            case_of.node.case_of.pat.meta.in_block = some(el)
          else:
            case_of.node.case_of.pat.meta.in_block = some(llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring("")))

          if case_of.node.case_of.body.meta.returns != fbYes and case_of.node.case_of.body.meta.breaks != fbYes and case_of.node.case_of.body.meta.continues != fbYes:
            case_of.node.case_of.body.meta.after_block = some(after)
            case_of.node.case_of.body.meta.br = some(after)
          case_of.node.case_of.body.meta.in_block = case_of.node.case_of.pat.meta.in_block

          if is_el:
            has_el = true
            break
          else:
            inc cases

        case ty.inner.kind
        of tykSigned, tykUnsigned, tykEnum, tykTagged:
          let value = self.node.stmt_match.value.meta.value
          if not has_el:
            llvm.deleteBasicBlock(el)
            el = after
          self.node.stmt_match.value.meta.make_switch = some((value, el, after, cases))
        of tykStruct:
          raise newIce("unimplemented")
        else:
          raise newError(errkInvalidMatch, self.node.stmt_match.value.meta.span, $ty[])
        
        return vrkRecurse),
      visit_expr_compound: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.meta.returns != fbYes:
          let cont = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring(""))
          cg.bb = cont
          cg.push_bb
        pre_cg_checks(self, parent)
        cg.enter
        return vrkRecurse),
      visit_expr_parenth: some(generic_pre_visitor),
      visit_expr_fail: some(generic_pre_visitor),
      visit_expr_defer: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        self.meta.saved_block = some(cg.bb)
        cg.add_defer
        return vrkRecurse),
      visit_expr_return: some(generic_pre_visitor),
      visit_expr_continue: some(generic_pre_visitor),
      visit_expr_break: some(generic_pre_visitor),
      visit_expr_let: some(generic_pre_visitor),
      visit_expr_int: some(generic_pre_visitor),
      visit_expr_float: some(generic_pre_visitor),
      visit_expr_bool: some(generic_pre_visitor),
      visit_expr_unit: some(generic_pre_visitor),
      visit_expr_string: some(generic_pre_visitor),
      visit_expr_named: some(generic_pre_visitor),
      visit_expr_array: some(generic_pre_visitor),
      visit_expr_range_full: some(generic_pre_visitor),
      visit_expr_application: some(generic_pre_visitor),
      visit_expr_field_access: some(generic_pre_visitor),
      visit_expr_cast: some(generic_pre_visitor),
      visit_expr_sizeof: some(generic_pre_visitor),
      visit_expr_alignof: some(generic_pre_visitor),
      visit_expr_unary: some(generic_pre_visitor),
      visit_expr_binary: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        pre_cg_checks(self, parent)
        case self.node.expr_binary.op
        of hbkLogicalAnd:
          let lval = self.meta.value[].get_pointer(self.meta.span)

          let store_false = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring(""))
          let right = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring(""))
          let after = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring(""))

          llvm.positionBuilderAtEnd(cg.builder, store_false)
          discard llvm.buildBr(cg.builder, after)
          llvm.positionBuilderAtEnd(cg.builder, cg.bb)

          self.node.expr_binary.lhs.meta.cond_br = some((self.node.expr_binary.lhs.meta.value, right, store_false))

          self.node.expr_binary.rhs.meta.in_block = some(right)
          self.node.expr_binary.rhs.meta.store_at = some(self.meta.value)
          self.node.expr_binary.rhs.meta.br = some(after)
          self.node.expr_binary.rhs.meta.after_block = some(after)
        of hbkLogicalOr:
          let lval = self.meta.value[].get_pointer(self.meta.span)

          let store_true = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring(""))
          let right = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring(""))
          let after = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring(""))

          llvm.positionBuilderAtEnd(cg.builder, store_true)
          discard llvm.buildBr(cg.builder, after)
          llvm.positionBuilderAtEnd(cg.builder, cg.bb)

          self.node.expr_binary.lhs.meta.cond_br = some((self.node.expr_binary.lhs.meta.value, store_true, right))

          self.node.expr_binary.rhs.meta.in_block = some(right)
          self.node.expr_binary.rhs.meta.store_at = some(self.meta.value)
          self.node.expr_binary.rhs.meta.br = some(after)
          self.node.expr_binary.rhs.meta.after_block = some(after)
        else:
          discard
        return vrkRecurse),
      visit_expr_ternary: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        pre_cg_checks(self, parent)
        case self.node.expr_ternary.op
        of htkCond:
          let lval = self.meta.value[].get_pointer(self.meta.span)

          var
            then: llvm.BasicBlockRef
            el: BasicBlockRef
          let
            after = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring(""))

          if self.node.expr_ternary.lhs.isSome:
            then = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring(""))
          else:
            then = after
          if self.node.expr_ternary.rhs.isSome:
            el = llvm.appendBasicBlockInContext(cg.ctx, cg.function, cstring(""))
          else:
            el = after

          self.node.expr_ternary.value.meta.cond_br = some((self.node.expr_ternary.value.meta.value, then, el))

          if self.node.expr_ternary.lhs.isSome:
            self.node.expr_ternary.lhs.get.meta.in_block = some(then)
            self.node.expr_ternary.lhs.get.meta.store_at = some(self.meta.value)
            self.node.expr_ternary.lhs.get.meta.br = some(after)
            self.node.expr_ternary.lhs.get.meta.after_block = some(after)
          if self.node.expr_ternary.rhs.isSome:
            self.node.expr_ternary.rhs.get.meta.in_block = some(el)
            self.node.expr_ternary.rhs.get.meta.store_at = some(self.meta.value)
            self.node.expr_ternary.rhs.get.meta.br = some(after)
            self.node.expr_ternary.rhs.get.meta.after_block = some(after)
        return vrkRecurse),
      visit_pat_destructure: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        pre_cg_checks(self, parent)

        let
          ty = self.meta.switch_type[].as_qualified_in_span(self.meta.span)
          deref = self.meta.switch_type[].deref[].as_qualified_in_span(self.meta.span)
        case deref.inner.kind
        of tykTagged:
          var idx: int
          for i, variant in deref.inner.tagged.variants.pairs:
            if variant.name == self.node.pat_destructure.ty.node.expr_named.name.full.get:
              idx = i
              break
          cg.add_pattern((cg.pattern.value, ty, false, idx, some(self.node.pat_destructure.ty.node.expr_named.name.mangled.get)))

          var tag_size: uint
          let ts = self.meta.own_type.get[].inner.tagged.get_tagged_tag_size(cg.type_ctx)

          case ts.kind
          of sokUnknown:
            raise newError(errkInvalidType, self.meta.span, "unknown tag size")
          of sokUnsized:
            raise newError(errkInvalidType, self.meta.span, "unsized tag size")
          of sokSized:
            tag_size = ts.size

          let tag_type = llvm.intTypeInContext(cg.ctx, cuint(tag_size * 8))

          self.meta.value[] = initCgValue(llvm.constInt(tag_type, cuint(idx), 0))
        of tykStruct:
          raise newIce("unimplemented")
        else:
          raise newError(errkInvalidMatch, self.node.stmt_match.value.meta.span, $ty[])
        return vrkRecurse),
      visit_pat_bind: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        pre_cg_checks(self, parent)
        return vrkRecurse),
      visit_pat_else: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        pre_cg_checks(self, parent)
        return vrkRecurse),
      visit_case: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        pre_cg_checks(self, parent)
        cg.enter_ctx()
        return vrkRecurse),
    )
    post_visitor = HlirVisitor(
      visit_function: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.function.type_params.isSome:
          return vrkContinue

        if self.node.function.body.isSome:
          if self.node.function.body.get.meta.returns != fbYes:
            discard llvm.buildBr(cg.builder, cg.get_return_bb)

          if driver.opt_level > 0:
            llvm.runFunctionPassManager(cg.fpm, self.meta.value.value)

          if driver.debug_info:
            cg.leave_di_function
          let (bindings, types) = cg.leave_this_fn()
          self.meta.cg_bindings_scope = some(bindings)
          self.meta.cg_types_scope = some(types)
          if cg.has_bb:
            llvm.positionBuilderAtEnd(cg.builder, cg.bb)
        else:
          let (bindings, types) = cg.leave_this_declare()
          self.meta.cg_bindings_scope = some(bindings)
          self.meta.cg_types_scope = some(types)
        return vrkRecurse),
      visit_static: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.static_global.name.get.mangled.isNone:
         raise newIce("unmangled identifier")

        let global = self.meta.value.value
        if self.node.static_global.value.isSome:
          case self.node.static_global.ty.meta.own_type.get.inner.kind
          of tykSigned:
            self.meta.value[] = initCgValue(llvm.constIntCast(self.node.static_global.value.get.meta.value[].get(self.meta.span), self.node.static_global.ty.meta.type_ref, 1))
          of tykUnsigned:
            self.meta.value[] = initCgValue(llvm.constIntCast(self.node.static_global.value.get.meta.value[].get(self.meta.span), self.node.static_global.ty.meta.type_ref, 0))
          of tykFloating:
            self.meta.value[] = initCgValue(llvm.constFPCast(self.node.static_global.value.get.meta.value[].get(self.meta.span), self.node.static_global.ty.meta.type_ref))
          else:
            self.meta.value[] = self.node.static_global.value.get.meta.value[]

          llvm.setInitializer(global, self.meta.value[].get(self.meta.span))
        else:
          let undef = llvm.getUndef(self.node.static_global.ty.meta.type_ref)
          llvm.setInitializer(global, undef)
        return vrkRecurse),
      visit_stmt_while: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        cg.pop_loop
        cg.leave_ctx
        post_cg_checks(self, parent)
        return vrkRecurse),
      visit_stmt_dowhile: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        cg.pop_loop
        cg.leave_ctx
        post_cg_checks(self, parent)
        return vrkRecurse),
      visit_stmt_for: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        cg.pop_loop
        cg.leave_ctx
        post_cg_checks(self, parent)
        return vrkRecurse),
      visit_stmt_match: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if cg.switch_stack.len > 0:
          let switch = cg.switch[].load_with_builder(cg.builder)

          for i, case_of in self.node.stmt_match.cases.mpairs:
            if case_of.node.case_of.pat.is_else_pattern:
              break

            let value = case_of.node.case_of.pat.meta.value[].load_with_builder(cg.builder)
            let bb = case_of.node.case_of.body.meta.in_block.get

            llvm.addCase(switch, value, bb)

          cg.pop_switch
          cg.pop_pattern

        post_cg_checks(self, parent)
        return vrkRecurse),
      visit_expr_compound: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.expr_compound.last.isSome and
            self.node.expr_compound.last.get.meta.value[].kind != cvkUninit:
          if self.node.expr_compound.last.get.meta.value[].kind == cvkUnit:
            self.meta.value[] = initCgUnit()
          else:
            self.meta.value[] = initCgValue(self.node.expr_compound.last.get.meta.value[].load_with_builder(cg.builder))
        # this a hack, this br should never be generated, but I don't know what causes it to be generated
        let last_instr = llvm.getLastInstruction(cg.bb)
        if last_instr.isNil or llvm.isATerminatorInst(last_instr) == 0:
          discard llvm.buildBr(cg.builder, cg.get_end_bb)
        llvm.positionBuilderAtEnd(cg.builder, cg.get_end_bb)
        if not cg.bb_stack_empty:
          let early_var = cg.get_early_var[].load_with_builder(cg.builder)
          var numcases = 1
          if cg.get_continue_target.isSome:
            inc numcases
          if cg.get_break_target.isSome:
            inc numcases
          let switch = llvm.buildSwitch(cg.builder, early_var, cg.peek_bb, cuint(numcases))
          llvm.addCase(switch, llvm.constInt(llvm.intTypeInContext(cg.ctx, 8), 1, 0), cg.get_defer_bb)
          if cg.get_continue_target.isSome:
            llvm.addCase(switch, llvm.constInt(llvm.intTypeInContext(cg.ctx, 8), 2, 0), cg.get_continue_target.get)
          if cg.get_break_target.isSome:
            llvm.addCase(switch, llvm.constInt(llvm.intTypeInContext(cg.ctx, 8), 3, 0), cg.get_break_target.get)
        else:
          discard llvm.buildBr(cg.builder, cg.get_defer_bb)
        cg.bb = cg.get_end_bb
        let end_bb = cg.bb
        cg.leave()

        if self.meta.returns != fbYes:
          cg.pop_bb
          llvm.positionBuilderAtEnd(cg.builder, cg.bb)
        if self.meta.breaks == fbYes or self.meta.continues == fbYes:
          discard llvm.buildBr(cg.builder, end_bb)

        post_cg_checks(self, parent)

        return vrkRecurse),
      visit_expr_continue: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let early_var = cg.get_early_var
        early_var[].store_with_builder(initCgValue(llvm.constInt(llvm.intTypeInContext(cg.ctx, 8), 2, 0)), cg.builder, self.meta.span)
        self.meta.value[] = initCgUnit()
        post_cg_checks(self, parent)

        return vrkRecurse),
      visit_expr_break: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let early_var = cg.get_early_var
        early_var[].store_with_builder(initCgValue(llvm.constInt(llvm.intTypeInContext(cg.ctx, 8), 3, 0)), cg.builder, self.meta.span)
        self.meta.value[] = initCgUnit()
        post_cg_checks(self, parent)

        return vrkRecurse),
      visit_expr_int: some(generic_post_visitor),
      visit_expr_float: some(generic_post_visitor),
      visit_expr_bool: some(generic_post_visitor),
      visit_expr_unit: some(generic_post_visitor),
      visit_expr_string: some(generic_post_visitor),
      visit_expr_array: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.meta.is_const:
          return vrkRecurse

        let uint = cg.get_type("uint".toPath.mangled_as_type).get
        let arrayp = self.meta.value[].get_pointer(self.meta.span)
        for i, elem in self.node.expr_array.elems.pairs:
          var idx = llvm.constInt(uint, culong(i), 0)
          let p = initCgPointer(llvm.buildGEP(cg.builder, arrayp, addr idx, 1, cstring("")))
          p.store_with_builder(elem.meta.value[], cg.builder, elem.meta.span)
        post_cg_checks(self, parent)
        return vrkRecurse),
      visit_expr_field_access: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        var ty = self.node.expr_field_access.value.meta.type_sum[].as_qualified_in_span(self.meta.span)
        var ptr_depth = 0
        while ty.inner.kind == tykPointer:
          ty = ty.pointee[].as_qualified_in_span(self.meta.span)
          inc ptr_depth
        let field = self.node.expr_field_access.name
        var idx = none[int]()

        for i, f in ty.fields(cg.type_ctx):
          if f.name.isSome:
            if f.name.get == field:
              idx = some(i)
              break

        if idx.isSome:
          if llvm.getTypeKind(llvm.typeOf(self.node.expr_field_access.value.meta.value[].value)) == llvm.PointerTypeKind:
            if self.node.expr_field_access.value.meta.value[].kind == cvkValue:
              var p = self.node.expr_field_access.value.meta.value[].value
              for _ in 1 ..< ptr_depth:
                p = llvm.buildLoad(cg.builder, p, cstring(""))
              let gep = llvm.buildStructGEP(cg.builder, p, cuint(idx.get), cstring(""))
              self.meta.value[] = initCgPointer(gep)
            else:
              var p = self.node.expr_field_access.value.meta.value[].get_pointer(self.meta.span)
              for _ in 1 .. ptr_depth:
                p = llvm.buildLoad(cg.builder, p, cstring(""))
              let gep = llvm.buildStructGEP(cg.builder, p, cuint(idx.get), cstring(""))
              self.meta.value[] = initCgPointer(gep)
          else:
            let value = llvm.buildExtractValue(cg.builder, self.node.expr_field_access.value.meta.value[].load_with_builder(cg.builder), cuint(idx.get), cstring(""))
            self.meta.value[] = initCgValue(value)
        else:
          raise newError(errkNotAField, self.meta.span)
        post_cg_checks(self, parent)
        return vrkRecurse),
      visit_expr_sizeof: some(generic_post_visitor),
      visit_expr_alignof: some(generic_post_visitor),
      visit_expr_ternary: some(generic_post_visitor),
      visit_expr_parenth: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.expr_parenth.elems.len == 1:
          self.meta.value = self.node.expr_parenth.elems[0].meta.value

        post_cg_checks(self, parent)
        return vrkRecurse),
      visit_expr_fail: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        raise newError(errkFail, self.meta.span)),
      visit_expr_defer: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        cg.bb = self.meta.saved_block.get
        llvm.positionBuilderAtEnd(cg.builder, cg.bb)
        return vrkRecurse),
      visit_expr_return: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.expr_return.value.isSome and not self.node.expr_return.value.get.meta.type_sum[].as_qualified.is_unit:
          let result_var = cg.get_result_var
          result_var[].store_with_builder(self.node.expr_return.value.get.meta.value[], cg.builder, self.meta.span)
        let early_var = cg.get_early_var
        early_var[].store_with_builder(initCgValue(llvm.constInt(llvm.intTypeInContext(cg.ctx, 8), 1, 0)), cg.builder, self.meta.span)
        self.meta.value[] = initCgUnit()
        post_cg_checks(self, parent)
        return vrkRecurse),
      visit_expr_unary: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        case self.node.expr_unary.op
        of hukPostInc:
          let
            p = self.node.expr_unary.value.meta.value[].get_pointer(self.meta.span)
            v = llvm.buildLoad(cg.builder, p, cstring(""))
            t = llvm.typeOf(v)
          if llvm.getTypeKind(t) == llvm.PointerTypeKind:
            let uint = cg.get_type("uint".toPath.mangled_as_type).get
            var one = llvm.constInt(uint, 1, 0)
            let vgep = llvm.buildGEP(cg.builder, v, addr one, 1, cstring(""))
            llvm.buildStore(cg.builder, vgep, p)
            self.meta.value[] = initCgValue(v)
          else:
            let vinc = llvm.buildAdd(cg.builder, v, llvm.constInt(t, 1, 0), cstring(""))
            llvm.buildStore(cg.builder, vinc, p)
            self.meta.value[] = initCgValue(v)
        of hukPostDec:
          let
            p = self.node.expr_unary.value.meta.value[].get_pointer(self.meta.span)
            v = llvm.buildLoad(cg.builder, p, cstring(""))
            t = llvm.typeOf(v)
          if llvm.getTypeKind(t) == llvm.PointerTypeKind:
            let uint = cg.get_type("sint".toPath.mangled_as_type).get
            var mone = llvm.constInt(uint, cast[culong](clong(-1)), 1)
            let vgep = llvm.buildGEP(cg.builder, v, addr mone, 1, cstring(""))
            llvm.buildStore(cg.builder, vgep, p)
            self.meta.value[] = initCgValue(v)
          else:
            let vinc = llvm.buildSub(cg.builder, v, llvm.constInt(t, 1, 0), cstring(""))
            llvm.buildStore(cg.builder, vinc, p)
            self.meta.value[] = initCgValue(v)
        of hukPreInc:
          let
            p = self.node.expr_unary.value.meta.value[].get_pointer(self.meta.span)
            v = llvm.buildLoad(cg.builder, p, cstring(""))
            t = llvm.typeOf(v)
          if llvm.getTypeKind(t) == llvm.PointerTypeKind:
            let uint = cg.get_type("uint".toPath.mangled_as_type).get
            var one = llvm.constInt(uint, 1, 0)
            let vgep = llvm.buildGEP(cg.builder, v, addr one, 1, cstring(""))
            llvm.buildStore(cg.builder, vgep, p)
            self.meta.value[] = initCgValue(vgep)
          else:
            let vinc = llvm.buildAdd(cg.builder, v, llvm.constInt(t, 1, 0), cstring(""))
            llvm.buildStore(cg.builder, vinc, p)
            self.meta.value[] = initCgValue(vinc)
        of hukPreDec:
          let
            p = self.node.expr_unary.value.meta.value[].get_pointer(self.meta.span)
            v = llvm.buildLoad(cg.builder, p, cstring(""))
            t = llvm.typeOf(v)
          if llvm.getTypeKind(t) == llvm.PointerTypeKind:
            let uint = cg.get_type("uint".toPath.mangled_as_type).get
            var mone = llvm.constInt(uint, cast[culong](clong(-1)), 1)
            let vgep = llvm.buildGEP(cg.builder, v, addr mone, 1, cstring(""))
            llvm.buildStore(cg.builder, vgep, p)
            self.meta.value[] = initCgValue(vgep)
          else:
            let vinc = llvm.buildSub(cg.builder, v, llvm.constInt(t, 1, 0), cstring(""))
            llvm.buildStore(cg.builder, vinc, p)
            self.meta.value[] = initCgValue(vinc)
        of hukRef:
          self.meta.value[] = initCgValue(self.node.expr_unary.value.meta.value[].create_pointer(cg.function, cg.builder, cg.bb))
        of hukDeref:
          self.meta.value[] = initCgPointer(self.node.expr_unary.value.meta.value[].load_with_builder(cg.builder))
        of hukPos:
          self.meta.value[] = initCgValue(self.node.expr_unary.value.meta.value[].load_with_builder(cg.builder))
        of hukNeg:
          if self.node.expr_unary.value.meta.type_sum[].as_qualified_in_span(self.node.expr_unary.value.meta.span)[].is_floating:
            let
              v = self.node.expr_unary.value.meta.value[].load_with_builder(cg.builder)
              vneg = llvm.buildFNeg(cg.builder, v, cstring(""))
            self.meta.value[] = initCgValue(vneg)
          else:
            let
              v = self.node.expr_unary.value.meta.value[].load_with_builder(cg.builder)
              vneg = llvm.buildNeg(cg.builder, v, cstring(""))
            self.meta.value[] = initCgValue(vneg)
        of hukNot:
          let
            v = self.node.expr_unary.value.meta.value[].load_with_builder(cg.builder)
            vneg = llvm.buildNot(cg.builder, v, cstring(""))
          self.meta.value[] = initCgValue(vneg)
        of hukColon:
          raise newIce("unreachable")
        of hukRangeTo:
          let name = self.meta.type_sum[].as_qualified_in_span(self.meta.span).mangled
          var range_type = cg.get_type(name)

          let structp = self.meta.value[].get_pointer(self.meta.span)
          let endp = initCgPointer(llvm.buildStructGEP(cg.builder, structp, cuint(0), cstring("")))
          endp.store_with_builder(self.node.expr_unary.value.meta.value[], cg.builder, self.meta.span)
        of hukRangeFrom:
          let name = self.meta.type_sum[].as_qualified_in_span(self.meta.span).mangled
          var range_type = cg.get_type(name)

          let structp = self.meta.value[].get_pointer(self.meta.span)
          let startp = initCgPointer(llvm.buildStructGEP(cg.builder, structp, cuint(0), cstring("")))
          startp.store_with_builder(self.node.expr_unary.value.meta.value[], cg.builder, self.meta.span)
        post_cg_checks(self, parent)
        return vrkRecurse),
      visit_expr_binary: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        proc binary(self: ref Node, build_fn: proc (b: llvm.BuilderRef, lhs: llvm.ValueRef, rhs: llvm.ValueRef, name: cstring): llvm.ValueRef) {.closure.} =
          self.meta.value[] = initCgValue(build_fn(cg.builder, self.node.expr_binary.lhs.meta.value[].load_with_builder(cg.builder), self.node.expr_binary.rhs.meta.value[].load_with_builder(cg.builder), cstring("")))

        proc icmp(self: ref Node, cmp: llvm.IntPredicate) {.closure.} =
          self.meta.value[] = initCgValue(llvm.buildICmp(cg.builder, cmp, self.node.expr_binary.lhs.meta.value[].load_with_builder(cg.builder), self.node.expr_binary.rhs.meta.value[].load_with_builder(cg.builder), cstring("")))

        proc fcmp(self: ref Node, cmp: llvm.RealPredicate) {.closure.} =
          self.meta.value[] = initCgValue(llvm.buildFCmp(cg.builder, cmp, self.node.expr_binary.lhs.meta.value[].load_with_builder(cg.builder), self.node.expr_binary.rhs.meta.value[].load_with_builder(cg.builder), cstring("")))

        proc assign(self: ref Node, build_fn: proc (b: llvm.BuilderRef, lhs: llvm.ValueRef, rhs: llvm.ValueRef, name: cstring): llvm.ValueRef) {.closure.} =
          let value = build_fn(cg.builder, self.node.expr_binary.lhs.meta.value[].load_with_builder(cg.builder), self.node.expr_binary.rhs.meta.value[].load_with_builder(cg.builder), cstring(""))
          self.meta.value[] = initCgValue(value)
          self.node.expr_binary.lhs.meta.value[].store_with_builder(self.meta.value[], cg.builder, self.meta.span)

        case self.node.expr_binary.op
        of hbkIndex:
          case self.node.expr_binary.lhs.meta.type_sum[].as_qualified_in_span(self.node.expr_binary.lhs.meta.span).inner.kind
          of tykPointer:
            let p = self.node.expr_binary.lhs.meta.value[].load_with_builder(cg.builder)
            var idx = self.node.expr_binary.rhs.meta.value[].load_with_builder(cg.builder)
            let pidx = llvm.buildGEP(cg.builder, p, addr idx, 1, cstring(""))
            self.meta.value[] = initCgPointer(pidx)
          of tykArray:
            let rhs_ty = self.node.expr_binary.rhs.meta.type_sum[].as_qualified_in_span(self.node.expr_binary.rhs.meta.span)
            case rhs_ty.inner.kind
            of tykUnsigned:
              let arr = self.node.expr_binary.lhs.meta.value[].value
              if llvm.getTypeKind(llvm.typeOf(arr)) != llvm.PointerTypeKind:
                var idx = self.node.expr_binary.rhs.meta.value[].load_with_builder(cg.builder)
                let value = llvm.buildExtractElement(cg.builder, arr, idx, cstring(""))
                self.meta.value[] = initCgValue(value)
              else:
                let p = llvm.buildPointerCast(cg.builder, self.node.expr_binary.lhs.meta.value[].get_pointer(self.meta.span), llvm.pointerType(llvm.getElementType(self.node.expr_binary.lhs.meta.type_ref), 0), cstring(""))
                var idx = self.node.expr_binary.rhs.meta.value[].load_with_builder(cg.builder)
                let pidx = llvm.buildGEP(cg.builder, p, addr idx, 1, cstring(""))
                self.meta.value[] = initCgPointer(pidx)
            of tykStruct:
              if rhs_ty.inner.struct.name.isSome:
                if rhs_ty.inner.struct.name.get == ["__lang", "builtin", "Range"].toPath:
                  let
                    range_from_to = self.node.expr_binary.rhs.meta.value[].load_with_builder(cg.builder)
                  var
                    start_idx = llvm.buildExtractValue(cg.builder, range_from_to, 0, cstring(""))
                  let
                    end_idx = llvm.buildExtractValue(cg.builder, range_from_to, 1, cstring(""))
                    len = llvm.buildSub(cg.builder, end_idx, start_idx, cstring(""))
                    p = llvm.buildPointerCast(cg.builder, self.node.expr_binary.lhs.meta.value[].create_pointer(cg.function, cg.builder, cg.bb), llvm.pointerType(llvm.getElementType(self.node.expr_binary.lhs.meta.type_ref), 0), cstring(""))
                    pidx = llvm.buildGEP(cg.builder, p, addr start_idx, 1, cstring(""))

                    uint = cg.get_type("uint".toPath.mangled_as_type).get
                    pt = llvm.typeOf(p)

                  var fields = [uint, pt]
                  let
                    slice_t = llvm.structTypeInContext(cg.ctx, addr fields[0], cuint(fields.len), 0)
                    slice_p = self.meta.value[].get_pointer(self.meta.span)
                    lenp = llvm.buildStructGEP(cg.builder, slice_p, 0, cstring(""))
                    ptrp = llvm.buildStructGEP(cg.builder, slice_p, 1, cstring(""))
                  llvm.buildStore(cg.builder, len, lenp)
                  llvm.buildStore(cg.builder, pidx, ptrp)
                elif rhs_ty.inner.struct.name.get == ["__lang", "builtin", "RangeFrom"].toPath:
                  let
                    uint = cg.get_type("uint".toPath.mangled_as_type).get

                    range_from_to = self.node.expr_binary.rhs.meta.value[].load_with_builder(cg.builder)
                  var
                    start_idx = llvm.buildExtractValue(cg.builder, range_from_to, 0, cstring(""))
                  let
                    end_idx = llvm.constInt(uint, llvm.getArrayLength(self.node.expr_binary.lhs.meta.type_ref), 0)
                    len = llvm.buildSub(cg.builder, end_idx, start_idx, cstring(""))
                    p = llvm.buildPointerCast(cg.builder, self.node.expr_binary.lhs.meta.value[].create_pointer(cg.function, cg.builder, cg.bb), llvm.pointerType(llvm.getElementType(self.node.expr_binary.lhs.meta.type_ref), 0), cstring(""))
                    pidx = llvm.buildGEP(cg.builder, p, addr start_idx, 1, cstring(""))

                    pt = llvm.typeOf(p)

                  var fields = [uint, pt]
                  let
                    slice_t = llvm.structTypeInContext(cg.ctx, addr fields[0], cuint(fields.len), 0)
                    slice_p = self.meta.value[].get_pointer(self.meta.span)
                    lenp = llvm.buildStructGEP(cg.builder, slice_p, 0, cstring(""))
                    ptrp = llvm.buildStructGEP(cg.builder, slice_p, 1, cstring(""))
                  llvm.buildStore(cg.builder, len, lenp)
                  llvm.buildStore(cg.builder, pidx, ptrp)
                elif rhs_ty.inner.struct.name.get == ["__lang", "builtin", "RangeTo"].toPath:
                  let
                    range_from_to = self.node.expr_binary.rhs.meta.value[].load_with_builder(cg.builder)
                    end_idx = llvm.buildExtractValue(cg.builder, range_from_to, 0, cstring(""))
                    len = end_idx
                    p = llvm.buildPointerCast(cg.builder, self.node.expr_binary.lhs.meta.value[].create_pointer(cg.function, cg.builder, cg.bb), llvm.pointerType(llvm.getElementType(self.node.expr_binary.lhs.meta.type_ref), 0), cstring(""))
                    pidx = p

                    uint = cg.get_type("uint".toPath.mangled_as_type).get
                    pt = llvm.typeOf(p)

                  var fields = [uint, pt]
                  let
                    slice_t = llvm.structTypeInContext(cg.ctx, addr fields[0], cuint(fields.len), 0)
                    slice_p = self.meta.value[].get_pointer(self.meta.span)
                    lenp = llvm.buildStructGEP(cg.builder, slice_p, 0, cstring(""))
                    ptrp = llvm.buildStructGEP(cg.builder, slice_p, 1, cstring(""))
                  llvm.buildStore(cg.builder, len, lenp)
                  llvm.buildStore(cg.builder, pidx, ptrp)
                elif rhs_ty.inner.struct.name.get == ["__lang", "builtin", "RangeFull"].toPath:
                  let
                    uint = cg.get_type("uint".toPath.mangled_as_type).get
                    len = llvm.constInt(uint, llvm.getArrayLength(self.node.expr_binary.lhs.meta.type_ref), 0)
                    p = llvm.buildPointerCast(cg.builder, self.node.expr_binary.lhs.meta.value[].create_pointer(cg.function, cg.builder, cg.bb), llvm.pointerType(llvm.getElementType(self.node.expr_binary.lhs.meta.type_ref), 0), cstring(""))
                    pt = llvm.typeOf(p)

                  var fields = [uint, pt]
                  let
                    slice_t = llvm.structTypeInContext(cg.ctx, addr fields[0], cuint(fields.len), 0)
                    slice_p = self.meta.value[].get_pointer(self.meta.span)
                    lenp = llvm.buildStructGEP(cg.builder, slice_p, 0, cstring(""))
                    ptrp = llvm.buildStructGEP(cg.builder, slice_p, 1, cstring(""))
                  llvm.buildStore(cg.builder, len, lenp)
                  llvm.buildStore(cg.builder, p, ptrp)
            else:
              discard
          of tykSlice:
            let slice = self.node.expr_binary.lhs.meta.value
            var p: llvm.ValueRef
            if slice.kind == cvkValue:
              if llvm.getTypeKind(llvm.typeOf(slice.value)) == llvm.PointerTypeKind:
                p = llvm.buildLoad(
                  cg.builder,
                  llvm.buildStructGEP(
                    cg.builder,
                    self.node.expr_binary.lhs.meta.value[].get_pointer(self.meta.span),
                    1,
                    cstring(""),
                  ),
                  cstring(""),
                )
              else:
                p = llvm.buildExtractValue(
                  cg.builder,
                  self.node.expr_binary.lhs.meta.value[].get(self.meta.span),
                  1,
                  cstring(""),
                )
            else:
              p = llvm.buildLoad(
                cg.builder,
                llvm.buildStructGEP(
                  cg.builder,
                  self.node.expr_binary.lhs.meta.value[].get_pointer(self.meta.span),
                  1,
                  cstring(""),
                ),
                cstring(""),
              )
            var idx = self.node.expr_binary.rhs.meta.value[].load_with_builder(cg.builder)
            let pidx = llvm.buildGEP(cg.builder, p, addr idx, 1, cstring(""))
            self.meta.value[] = initCgPointer(pidx)
          of tykVar:
            let vararr = self.node.expr_binary.lhs.meta.value[].load_with_builder(cg.builder)
            let parr = llvm.buildStructGEP(cg.builder, vararr, 1, cstring(""))
            let elem = llvm.getElementType(llvm.getElementType(llvm.typeOf(parr)))
            let ptr_ty = llvm.pointerType(elem, 0)
            let p = llvm.buildPointerCast(cg.builder, parr, ptr_ty, cstring(""))
            var idx = self.node.expr_binary.rhs.meta.value[].load_with_builder(cg.builder)
            let pidx = llvm.buildGEP(cg.builder, p, addr idx, 1, cstring(""))
            self.meta.value[] = initCgPointer(pidx)
          else:
            raise newIce("unimplemented")
        of hbkAdd:
          if self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_floating:
            binary(self, llvm.buildFAdd)
          elif self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_pointer:
            binary(self, proc (b: llvm.BuilderRef, lhs: llvm.ValueRef, rhs: llvm.ValueRef, name: cstring): llvm.ValueRef =
              var idx = rhs
              return llvm.buildGEP(b, lhs, addr idx, 1, name))
          else:
            binary(self, llvm.buildAdd)
        of hbkSub:
          if self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_floating:
            binary(self, llvm.buildFSub)
          elif self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_pointer and
              (self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_signed or
              self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_unsigned):
            binary(self, proc (b: llvm.BuilderRef, lhs: llvm.ValueRef, rhs: llvm.ValueRef, name: cstring): llvm.ValueRef =
              var idx = llvm.buildNeg(b, rhs, name)
              return llvm.buildGEP(b, lhs, addr idx, 1, name))
          elif self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_pointer and
              self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_pointer:
            binary(self, proc (b: llvm.BuilderRef, lhs: llvm.ValueRef, rhs: llvm.ValueRef, name: cstring): llvm.ValueRef =
              let sint = cg.get_type("sint".toPath.mangled_as_type).get
              let lhs = llvm.buildPtrToInt(b, lhs, sint, name)
              let rhs = llvm.buildPtrToInt(b, rhs, sint, name)
              return llvm.buildSub(b, lhs, rhs, name))
          else:
            binary(self, llvm.buildSub)
        of hbkMul:
          if self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_floating:
            binary(self, llvm.buildFMul)
          else:
            binary(self, llvm.buildMul)
        of hbkDiv:
          if self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_floating:
            binary(self, llvm.buildFDiv)
          elif self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_unsigned:
            binary(self, llvm.buildUDiv)
          else:
            binary(self, llvm.buildSDiv)
        of hbkRem:
          if self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_floating:
            binary(self, llvm.buildFRem)
          elif self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_unsigned:
            binary(self, llvm.buildURem)
          else:
            binary(self, llvm.buildSRem)
        of hbkShl: binary(self, llvm.buildShl)
        of hbkShr: binary(self, llvm.buildAShr)
        of hbkLt:
          if self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_floating:
            fcmp(self, llvm.RealULT)
          elif self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_unsigned:
            icmp(self, llvm.IntULT)
          else:
            icmp(self, llvm.IntSLT)
        of hbkGt:
          if self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_floating:
            fcmp(self, llvm.RealUGT)
          elif self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_unsigned:
            icmp(self, llvm.IntUGT)
          else:
            icmp(self, llvm.IntSGT)
        of hbkLe:
          if self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_floating:
            fcmp(self, llvm.RealULE)
          elif self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_unsigned:
            icmp(self, llvm.IntULE)
          else:
            icmp(self, llvm.IntSLE)
        of hbkGe:
          if self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_floating:
            fcmp(self, llvm.RealUGE)
          elif self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_unsigned:
            icmp(self, llvm.IntUGE)
          else:
            icmp(self, llvm.IntSGE)
        of hbkEq:
          if self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_floating:
            fcmp(self, llvm.RealUEQ)
          else:
            icmp(self, llvm.IntEQ)
        of hbkNe:
          if self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_floating:
            fcmp(self, llvm.RealUNE)
          else:
            icmp(self, llvm.IntNE)
        of hbkBitand: binary(self, llvm.buildAnd)
        of hbkBitor: binary(self, llvm.buildOr)
        of hbkBitxor: binary(self, llvm.buildXor)
        of hbkLogicalAnd:
          let store_false = self.node.expr_binary.lhs.meta.cond_br.get.f
          llvm.positionBuilderBefore(cg.builder, llvm.getLastInstruction(store_false))
          self.meta.value[].store_with_builder(self.node.expr_binary.lhs.meta.value[], cg.builder, self.meta.span)
          llvm.positionBuilderAtEnd(cg.builder, cg.bb)
        of hbkLogicalOr:
          let store_true = self.node.expr_binary.lhs.meta.cond_br.get.t
          llvm.positionBuilderBefore(cg.builder, llvm.getLastInstruction(store_true))
          self.meta.value[].store_with_builder(self.node.expr_binary.lhs.meta.value[], cg.builder, self.meta.span)
          llvm.positionBuilderAtEnd(cg.builder, cg.bb)
        of hbkRange:
          let name = self.meta.type_sum[].as_qualified_in_span(self.meta.span).mangled
          var range_type = cg.get_type(name)
          if range_type.isNone:
            let range_generic = cg.type_ctx.get(["__lang", "builtin", "Range"].toPath).deepCopy
            range_generic.replace_param("T", self.node.expr_binary.lhs.meta.type_sum[].as_qualified_in_span(self.node.expr_binary.lhs.meta.span))
            range_type = some(range_generic.to_first_class_cg_type_with_span(self.meta.span, cg, target))
            cg.bind_type(range_generic.mangled, range_type.get)

          let structp = self.meta.value[].get_pointer(self.meta.span)
          let startp = initCgPointer(llvm.buildStructGEP(cg.builder, structp, cuint(0), cstring("")))
          let endp = initCgPointer(llvm.buildStructGEP(cg.builder, structp, cuint(1), cstring("")))
          startp.store_with_builder(self.node.expr_binary.lhs.meta.value[], cg.builder, self.meta.span)
          endp.store_with_builder(self.node.expr_binary.rhs.meta.value[], cg.builder, self.meta.span)
        of hbkAssign:
          self.node.expr_binary.lhs.meta.value[].store_with_builder(self.node.expr_binary.rhs.meta.value[], cg.builder, self.meta.span)
          self.meta.value[] = self.node.expr_binary.lhs.meta.value[]
        of hbkAssignAdd:
          if self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_floating:
            assign(self, llvm.buildFAdd)
          elif self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_pointer:
            assign(self, proc (b: llvm.BuilderRef, lhs: llvm.ValueRef, rhs: llvm.ValueRef, name: cstring): llvm.ValueRef =
              var idx = rhs
              return llvm.buildGEP(b, lhs, addr idx, 1, name))
          else:
            assign(self, llvm.buildAdd)
        of hbkAssignSub:
          if self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_floating:
            assign(self, llvm.buildFSub)
          elif self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_pointer:
            assign(self, proc (b: llvm.BuilderRef, lhs: llvm.ValueRef, rhs: llvm.ValueRef, name: cstring): llvm.ValueRef =
              var idx = llvm.buildNeg(b, rhs, name)
              return llvm.buildGEP(b, lhs, addr idx, 1, name))
          else:
            assign(self, llvm.buildSub)
        of hbkAssignMul:
          if self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_floating:
            assign(self, llvm.buildFMul)
          else:
            assign(self, llvm.buildMul)
        of hbkAssignDiv:
          if self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_floating:
            assign(self, llvm.buildFDiv)
          elif self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_unsigned:
            assign(self, llvm.buildUDiv)
          else:
            assign(self, llvm.buildSDiv)
        of hbkAssignRem:
          if self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_floating:
            assign(self, llvm.buildFRem)
          elif self.node.expr_binary.lhs.meta.type_sum[].as_qualified[].is_unsigned:
            assign(self, llvm.buildURem)
          else:
            assign(self, llvm.buildSRem)
        of hbkAssignLshift: assign(self, llvm.buildShl)
        of hbkAssignRshift: assign(self, llvm.buildAShr)
        of hbkAssignAnd: assign(self, llvm.buildAnd)
        of hbkAssignXor: assign(self, llvm.buildXor)
        of hbkAssignOr: assign(self, llvm.buildOr)
        of hbkArrow: raise newIce("unreachable")
        post_cg_checks(self, parent)
        return vrkRecurse),
      visit_expr_named: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if not self.is_type and
            not self.node.expr_named.quiet and
            (parent.node.kind != hrkDecl or parent.node.decl.name.isNone or (addr self[]) != (addr parent.node.decl.name.get[])):
          let
            name = self.node.expr_named.name.mangled.get
            variable = cg.get_var(name)

          if variable.isNone:
            raise newError(errkIceNoVariable, self.meta.span, name)

          self.meta.value[] = variable.get[]
          post_cg_checks(self, parent)
        return vrkRecurse),
      visit_expr_cast: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let value = self.node.expr_cast.value.meta.value[].load_with_builder(cg.builder)
        let to = self.node.expr_cast.ty.meta.own_type.get
        let frm = self.node.expr_cast.value.meta.type_sum[].as_qualified_in_span(self.node.expr_cast.value.meta.span)
        case frm.inner.kind
        of tykSigned:
          case to.inner.kind
          of tykSigned:
            let frm_bits = if frm.inner.signed.bits.kind == bitsCustom: frm.inner.signed.bits.bits else: target.pointer_width.uint8
            let to_bits = if to.inner.signed.bits.kind == bitsCustom: to.inner.signed.bits.bits else: target.pointer_width.uint8
            if to_bits > frm_bits:
              self.meta.value[] = initCgValue(llvm.buildSExt(cg.builder, value, self.meta.type_ref, cstring("")))
            else:
              self.meta.value[] = initCgValue(llvm.buildTrunc(cg.builder, value, self.meta.type_ref, cstring("")))
          of tykUnsigned:
            let frm_bits = if frm.inner.signed.bits.kind == bitsCustom: frm.inner.signed.bits.bits else: target.pointer_width.uint8
            let to_bits = if to.inner.unsigned.bits.kind == bitsCustom: to.inner.unsigned.bits.bits else: target.pointer_width.uint8
            if to_bits > frm_bits:
              self.meta.value[] = initCgValue(llvm.buildSExt(cg.builder, value, self.meta.type_ref, cstring("")))
            else:
              self.meta.value[] = initCgValue(llvm.buildTrunc(cg.builder, value, self.meta.type_ref, cstring("")))
          of tykFloating:
            self.meta.value[] = initCgValue(llvm.buildSIToFp(cg.builder, value, self.meta.type_ref, cstring("")))
          of tykPointer:
            if frm.inner.signed.bits.kind == bitsWord:
              self.meta.value[] = initCgValue(llvm.buildIntToPtr(cg.builder, value, self.meta.type_ref, cstring("")))
          else:
            raise newIce("invalid cast")
        of tykUnsigned:
          case to.inner.kind
          of tykSigned:
            let frm_bits = if frm.inner.unsigned.bits.kind == bitsCustom: frm.inner.unsigned.bits.bits else: target.pointer_width.uint8
            let to_bits = if to.inner.signed.bits.kind == bitsCustom: to.inner.signed.bits.bits else: target.pointer_width.uint8
            if to_bits > frm_bits:
              self.meta.value[] = initCgValue(llvm.buildZExt(cg.builder, value, self.meta.type_ref, cstring("")))
            else:
              self.meta.value[] = initCgValue(llvm.buildTrunc(cg.builder, value, self.meta.type_ref, cstring("")))
          of tykUnsigned:
            let frm_bits = if frm.inner.unsigned.bits.kind == bitsCustom: frm.inner.unsigned.bits.bits else: target.pointer_width.uint8
            let to_bits = if to.inner.unsigned.bits.kind == bitsCustom: to.inner.unsigned.bits.bits else: target.pointer_width.uint8
            if to_bits > frm_bits:
              self.meta.value[] = initCgValue(llvm.buildZExt(cg.builder, value, self.meta.type_ref, cstring("")))
            else:
              self.meta.value[] = initCgValue(llvm.buildTrunc(cg.builder, value, self.meta.type_ref, cstring("")))
          of tykEnum:
            let to = to.inner.enumeration.inner
            let frm_bits = if frm.inner.unsigned.bits.kind == bitsCustom: frm.inner.unsigned.bits.bits else: target.pointer_width.uint8
            let to_bits = if to.inner.unsigned.bits.kind == bitsCustom: to.inner.unsigned.bits.bits else: target.pointer_width.uint8
            if to_bits > frm_bits:
              self.meta.value[] = initCgValue(llvm.buildZExt(cg.builder, value, self.meta.type_ref, cstring("")))
            else:
              self.meta.value[] = initCgValue(llvm.buildTrunc(cg.builder, value, self.meta.type_ref, cstring("")))
          of tykFloating:
            self.meta.value[] = initCgValue(llvm.buildUIToFp(cg.builder, value, self.meta.type_ref, cstring("")))
          of tykPointer:
            if frm.inner.unsigned.bits.kind == bitsWord:
              self.meta.value[] = initCgValue(llvm.buildIntToPtr(cg.builder, value, self.meta.type_ref, cstring("")))
          else:
            raise newIce("invalid cast")
        of tykEnum:
          case to.inner.kind:
          of tykUnsigned:
            let frm = frm.inner.enumeration.inner
            let frm_bits = if frm.inner.unsigned.bits.kind == bitsCustom: frm.inner.unsigned.bits.bits else: target.pointer_width.uint8
            let to_bits = if to.inner.unsigned.bits.kind == bitsCustom: to.inner.unsigned.bits.bits else: target.pointer_width.uint8
            if to_bits > frm_bits:
              self.meta.value[] = initCgValue(llvm.buildZExt(cg.builder, value, self.meta.type_ref, cstring("")))
            else:
              self.meta.value[] = initCgValue(llvm.buildTrunc(cg.builder, value, self.meta.type_ref, cstring("")))
          else:
            raise newIce("invalid cast")
        of tykFloating:
          case to.inner.kind
          of tykSigned:
            self.meta.value[] = initCgValue(llvm.buildFPToSI(cg.builder, value, self.meta.type_ref, cstring("")))
          of tykUnsigned:
            self.meta.value[] = initCgValue(llvm.buildFPToUI(cg.builder, value, self.meta.type_ref, cstring("")))
          of tykFloating:
            self.meta.value[] = initCgValue(llvm.buildFPCast(cg.builder, value, self.meta.type_ref, cstring("")))
          else:
            raise newIce("invalid cast")
        of tykPointer:
          case to.inner.kind
          of tykSigned:
            if to.inner.signed.bits.kind == bitsWord:
              self.meta.value[] = initCgValue(llvm.buildPtrToInt(cg.builder, value, self.meta.type_ref, cstring("")))
          of tykUnsigned:
            if to.inner.unsigned.bits.kind == bitsWord:
              self.meta.value[] = initCgValue(llvm.buildPtrToInt(cg.builder, value, self.meta.type_ref, cstring("")))
          of tykPointer:
            self.meta.value[] = initCgValue(llvm.buildPointerCast(cg.builder, value, self.meta.type_ref, cstring("")))
          of tykFunction:
            self.meta.value[] = initCgValue(llvm.buildPointerCast(cg.builder, value, self.meta.type_ref, cstring("")))
          of tykTrait:
            let
              u8 = llvm.intTypeInContext(cg.ctx, 8)
              unit_ptr = llvm.pointerType(u8, 0)
              unitp_ptr = llvm.pointerType(unit_ptr, 0)

            let trait = self.meta.type_sum[].as_qualified_in_span(self.meta.span)
            let ptr_ty = self.node.expr_cast.value.meta.type_sum[].as_qualified_in_span(self.node.expr_cast.value.meta.span)
            let key = (trait, ptr_ty)
            let vtable = if cg.vtables.contains(key):
              llvm.constPointerCast(cg.vtables[key], unitp_ptr)
            else:
              let impls = driver.type_ctx.prod(trait, ptr_ty, self.meta.span)
              var fns = newSeq[llvm.ValueRef]()
              for impl in impls:
                let name = impl.name.mangled_with_params(impl.params.mapIt(it.ty[].as_qualified))
                let fn = cg.get_var(name).get.value
                fns.add(llvm.constPointerCast(fn, unit_ptr))
              let vtable = llvm.addGlobal(cg.module, llvm.arrayType(unit_ptr, cuint(impls.len)), cstring("vtable." & trait.inner.trait.name.path[^1] & "." & ptr_ty.pointee[].as_qualified[].name.path[^1]))
              let vtable_array = llvm.constArray(unit_ptr, addr fns[0], cuint(fns.len))
              llvm.setInitializer(vtable, vtable_array)
              cg.vtables.add(key, vtable)
              llvm.constPointerCast(vtable, unitp_ptr)
            let value = llvm.buildPointerCast(cg.builder, value, unit_ptr, cstring(""))
            let p = self.meta.value[].get_pointer(self.meta.span)
            let datap = llvm.buildStructGEP(cg.builder, p, 0, cstring(""))
            let vtablep = llvm.buildStructGEP(cg.builder, p, 1, cstring(""))
            llvm.buildStore(cg.builder, value, datap)
            llvm.buildStore(cg.builder, vtable, vtablep)
          else:
            raise newIce("invalid cast")
        of tykFunction:
          case to.inner.kind
          of tykPointer:
            self.meta.value[] = initCgValue(llvm.buildPointerCast(cg.builder, value, self.meta.type_ref, cstring("")))
          else:
            raise newIce("invalid cast")
        else:
          raise newIce("invalid cast")
        post_cg_checks(self, parent)
        return vrkRecurse),
      visit_expr_application: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.meta.own_type.isSome:
          return vrkRecurse

        if self.meta.impl.isNone or self.meta.impl.get.kind != ikCons:
          let fn = self.node.expr_application.fn

          var args = newSeq[llvm.ValueRef]()
          if self.meta.impl.isSome and self.meta.impl.get.ampvar.isSome:
            let proper = self.meta.impl.get.params.len - 1
            for arg in self.node.expr_application.args[0 ..< proper]:
              if arg.meta.value[].kind != cvkUnit:
                args.add(arg.meta.value[].load_with_builder(cg.builder))

            let
              variadic = self.node.expr_application.args.len - self.meta.impl.get.params.len + 1
              var_ptype = self.meta.impl.get.params[^1].ty[].as_qualified.to_first_class_cg_type_with_span(self.meta.span, cg, target)
              uint = cg.get_type("uint".toPath.mangled_as_type).get

            let vararr = self.meta.vararr.get
            let varlen = llvm.buildStructGEP(cg.builder, vararr, 0, cstring(""))
            llvm.buildStore(cg.builder, llvm.constInt(uint, culong(variadic), 0), varlen)
            for i in 0 ..< variadic:
              let arg = self.node.expr_application.args[proper + i]
              if arg.meta.value[].kind != cvkUnit:
                let argp = llvm.buildStructGEP(cg.builder, vararr, cuint(i + 1), cstring(""))
                llvm.buildStore(cg.builder, arg.meta.value[].load_with_builder(cg.builder), argp)
            args.add(llvm.buildPointerCast(cg.builder, vararr, var_ptype, cstring("")))
          else:
            for arg in self.node.expr_application.args:
              if arg.meta.value[].kind != cvkUnit:
                args.add(arg.meta.value[].load_with_builder(cg.builder))

          let
            lfn = fn.meta.value[].load_with_builder(cg.builder)
            argv = if args.len == 0: nil else: addr args[0]

          self.meta.value[] = initCgValue(llvm.buildCall(cg.builder, lfn, argv, cuint(args.len), cstring("")))
        else:
          let lval = self.meta.value[].get_pointer(self.meta.span)
          let struct_type = self.meta.type_sum[].as_qualified_in_span(self.meta.span)

          case struct_type.inner.kind
          of tykTagged:
            var tag_size: uint
            let ts = self.meta.type_sum[].as_qualified_in_span(self.meta.span).inner.tagged.get_tagged_tag_size(cg.type_ctx)

            case ts.kind
            of sokUnknown:
              raise newError(errkInvalidType, self.meta.span, "unknown alignment")
            of sokUnsized:
              raise newError(errkInvalidType, self.meta.span, "unsized alignment")
            of sokSized:
              tag_size = ts.size

            let tag_type = llvm.intTypeInContext(cg.ctx, cuint(tag_size * 8))

            for tag, variant in struct_type.inner.tagged.variants.pairs:
              if variant.name == self.node.expr_application.fn.node.expr_named.name.full.get:
                let p = initCgPointer(llvm.buildStructGEP(cg.builder, lval, cuint(struct_type.inner.tagged.tag_idx), cstring("")))
                let tag = initCgValue(llvm.constInt(tag_type, culong(tag), 0))
                p.store_with_builder(tag, cg.builder, self.meta.span)
                break
          else: discard

          for init in self.node.expr_application.args:
            # TODO: move this elsewhere
            if init.node.kind != hrkDecl:
              raise newError(errkMalformedHlir, init.meta.span, "a constructor argument must be a declaration")
            let field = init.node.decl.name.get.node.expr_named.name.short.path[^1]
            let value = init.node.decl.value.meta.value
            case struct_type.inner.kind
            of tykStruct:
              for i, f in struct_type.inner.struct.fields.pairs:
                if f.name.isSome:
                  if f.name.get == field:
                    let p = initCgPointer(llvm.buildStructGEP(cg.builder, lval, cuint(i), cstring("")))
                    p.store_with_builder(value[], cg.builder, init.meta.span)
                    break
            of tykUnion:
              for i, f in struct_type.inner.union.fields.pairs:
                if f.name.isSome:
                  if f.name.get == field:
                    let pty = llvm.pointerType(f.ty[].as_qualified_in_span(init.meta.span).to_first_class_cg_type_with_span(init.meta.span, cg, target), 0)
                    let p = initCgPointer(llvm.buildPointerCast(cg.builder, lval, pty, cstring("")))
                    p.store_with_builder(value[], cg.builder, init.meta.span)
                    break
            of tykTagged:
              for i, f in struct_type.inner.tagged.fields.pairs:
                if f.name.isSome:
                  if f.name.get == field:
                    let p = initCgPointer(llvm.buildStructGEP(cg.builder, lval, cuint(i), cstring("")))
                    p.store_with_builder(value[], cg.builder, init.meta.span)
                    break
              for tag, variant in struct_type.inner.tagged.variants.pairs:
                if variant.name == self.node.expr_application.fn.node.expr_named.name.full.get:
                  let
                    structp = llvm.buildPointerCast(
                      cg.builder,
                      llvm.buildStructGEP(cg.builder, lval, cuint(struct_type.inner.tagged.tag_idx + 1), cstring("")),
                      llvm.pointerType(self.node.expr_application.fn
                        .meta
                        .own_type
                        .get
                        .to_first_class_cg_type_with_span(self.node.expr_application.fn.meta.span, cg, target), 0),
                      cstring(""),
                    )
                  for i, f in variant.fields.pairs:
                    if f.name.isSome:
                      if f.name.get == field:
                        let p = initCgPointer(llvm.buildStructGEP(cg.builder, structp, cuint(i), cstring("")))
                        p.store_with_builder(value[], cg.builder, init.meta.span)
                        break
                  break
            else:
              raise newError(errkInvalidType, init.meta.span)
        post_cg_checks(self, parent)
        return vrkRecurse),
      visit_expr_let: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        if self.node.expr_let.name.get.mangled.isNone:
         raise newIce("unmangled identifier")

        if not self.meta.type_sum[].as_qualified_in_span(self.meta.span).is_unit and self.node.expr_let.value.isSome:
          self.meta.value[].store_with_builder(self.node.expr_let.value.get.meta.value[], cg.builder, self.meta.span)
        cg.bind_var(self.node.expr_let.name.get.mangled.get, self.meta.value)
        if self.meta.value.kind != cvkUnit:
          let a = self.meta.type_sum[].as_qualified_in_span(self.meta.span).get_align_of(cg.type_ctx) # in bits
          var align: uint
          case a.kind
          of sokUnknown:
            raise newError(errkInvalidType, self.meta.span, "unknown size")
          of sokUnsized:
            raise newError(errkInvalidType, self.meta.span, "unsized")
          of sokSized:
            align = a.size.uint * 8

          if driver.debug_info:
            let
              dbg_addr = llvm.getNamedFunction(cg.module, cstring("llvm.dbg.addr"))
              location = llvm.valueAsMetadata(self.meta.value.value)
              name = self.node.expr_let.name.get.short.path[^1]
              ty = self.meta.type_sum[].as_qualified_in_span(self.meta.span).di_qualified_type_first_class(self.meta.span, cg, target)
              variable = llvm.diBuilderCreateAutoVariable(cg.di_builder, cg.di_scope.get, name.cstring, name.len.uint, cg.di_file.get, self.meta.span.line.cuint, ty, 0, DiFlagZero, align.uint32)
              expr = llvm.diBuilderCreateExpression(cg.di_builder, nil, 0)
            var
              args = [llvm.metadataAsValue(cg.ctx, location), llvm.metadataAsValue(cg.ctx, variable), llvm.metadataAsValue(cg.ctx, expr)]
            discard llvm.buildCall(cg.builder, dbg_addr, addr args[0], args.len.cuint, cstring(""))
        post_cg_checks(self, parent)
        return vrkRecurse),
      visit_pat_destructure: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let
          ty = self.meta.switch_type[].as_qualified_in_span(self.meta.span)
          deref = self.meta.switch_type[].deref[].as_qualified_in_span(self.meta.span)
        case deref.inner.kind
        of tykTagged:
          cg.pop_pattern
        else:
          discard
        post_cg_checks(self, parent)
        return vrkRecurse),
      visit_pat_bind: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        case parent.node.kind
        of hrkPatDestructure:
          let field = self.node.pat_bind.name.get.short.path[^1]
          var
            ty = self.meta.switch_type[].as_qualified_in_span(self.meta.span)
            value = cg.pattern.value[].value

          var ptr_depth = 0
          while ty.inner.kind == tykPointer:
            ty = ty.pointee[].as_qualified_in_span(self.meta.span)
            inc ptr_depth

          if llvm.getTypeKind(llvm.typeOf(value)) == llvm.PointerTypeKind:
            if cg.pattern.value.kind == cvkPointer:
              for _ in 1 .. ptr_depth:
                value = llvm.buildLoad(cg.builder, value, cstring(""))
            else:
              for _ in 1 ..< ptr_depth:
                value = llvm.buildLoad(cg.builder, value, cstring(""))

          case ty.inner.kind
          of tykStruct:
            for i, f in ty.inner.struct.fields.pairs:
              if f.name.isSome:
                if f.name.get == field:
                  if ptr_depth > 0:
                    self.meta.value[] = initCgValue(llvm.buildStructGEP(cg.builder, value, cuint(i), cstring("")))
                  else:
                    self.meta.value[] = initCgPointer(llvm.buildStructGEP(cg.builder, value, cuint(i), cstring("")))
                  break
          of tykTagged:
            let
              variant = parent.node.pat_destructure.ty.node.expr_named.name.full.get
              vt = parent.node.pat_destructure.ty
            for v in ty.inner.tagged.variants:
              if v.name == variant:
                if llvm.getTypeKind(llvm.typeOf(value)) == llvm.PointerTypeKind:
                  let
                    structp = llvm.buildPointerCast(
                      cg.builder,
                      llvm.buildStructGEP(cg.builder, value, cuint(ty.inner.tagged.tag_idx + 1), cstring("")),
                      llvm.pointerType(vt
                        .meta
                        .own_type
                        .get
                        .to_first_class_cg_type_with_span(parent.node.pat_destructure.ty.meta.span, cg, target), 0),
                      cstring(""),
                    )
                  for i, f in v.fields.pairs:
                    if f.name.isSome:
                      if f.name.get == field:
                        if ptr_depth > 0:
                          self.meta.value[] = initCgValue(llvm.buildStructGEP(cg.builder, structp, cuint(i), cstring("")))
                        else:
                          self.meta.value[] = initCgPointer(llvm.buildStructGEP(cg.builder, structp, cuint(i), cstring("")))
                        break
                  break
                else:
                  let
                    structp = llvm.buildBitcast(
                      cg.builder,
                      llvm.buildExtractValue(cg.builder, value, cuint(ty.inner.tagged.tag_idx + 1), cstring("")),
                      vt
                        .meta
                        .own_type
                        .get
                        .to_first_class_cg_type_with_span(parent.node.pat_destructure.ty.meta.span, cg, target),
                      cstring(""),
                    )
                  for i, f in v.fields.pairs:
                    if f.name.isSome:
                      if f.name.get == field:
                        self.meta.value[] = initCgValue(llvm.buildExtractValue(cg.builder, structp, cuint(i), cstring("")))
                  break
          else:
            raise newError(errkInvalidType, parent.meta.span)
        of hrkPatBind:
          parent.meta.value = self.meta.value
        of hrkCase:
          self.meta.value[] = cg.pattern.value[]
        else:
          raise newError(errkMalformedHlir, self.meta.span)
        if self.node.pat_bind.pat.isNone:
          cg.bind_var(self.node.pat_bind.name.get.mangled.get, self.meta.value)
        return vrkRecurse),
      visit_pat_else: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        post_cg_checks(self, parent)
        return vrkRecurse),
      visit_case: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        cg.leave_ctx()
        post_cg_checks(self, parent)
        return vrkRecurse),
    )
  discard accept(root, pre_visitor, post_visitor)

  if driver.debug_info:
    llvm.diBuilderFinalize(cg.di_builder)

  if driver.opt_level > 0:
    llvm.runPassManager(cg.pm, cg.module)

proc code_gen*(index: ref Index, driver: ref Driver, node: ref Node, root: ref Node) =
  if root.meta.impl_scope.isSome:
    driver.type_ctx.enter_this_bounded(root.meta.impl_scope.get)
  else:
    driver.type_ctx.enter_bounded
  code_gen(index, driver, node)
  root.meta.impl_scope = some(driver.type_ctx.leave_this_bounded)

proc dump_location(cg: CgPassController, self: ref Node) =
  let scope = cg.di_scope
  if scope.isSome:
    let loc = llvm.diBuilderCreateDebugLocation(cg.ctx, self.meta.span.line.cuint, self.meta.span.col.cuint, scope.get, nil)
    let value = llvm.metadataAsValue(cg.ctx, loc)
    llvm.setCurrentDebugLocation(cg.builder, value)

proc di_qualified_type(ty: ref QualifiedType, span: Span, cg: CgPassController, target: ref Target): llvm.MetadataRef =
  if ty.meta.isSome:
    return ty.meta.get

  case ty.inner.kind
  of tykType:
    raise newError(errkInvalidType, span, "type is a meta-type")
  of tykNever:
    let name = "never"
    result = llvm.diBuilderCreateUnspecifiedType(cg.di_builder, name.cstring, name.len.uint)
  of tykUnit:
    let name = "unit"
    result = llvm.diBuilderCreateUnspecifiedType(cg.di_builder, name.cstring, name.len.uint)
  of tykBool:
    let name = "bool"
    result = llvm.diBuilderCreateBasicType(cg.di_builder, name.cstring, name.len.uint, 1, DW_ATE_boolean, DiFlagZero)
  of tykSigned:
    case ty.inner.signed.bits.kind
    of bitsWord:
      let name = ty[].human_readable_name
      result = llvm.diBuilderCreateBasicType(cg.di_builder, name.cstring, name.len.uint, target.pointer_width.uint64, DW_ATE_signed, DiFlagZero)
    of bitsCustom:
      let name = ty[].human_readable_name
      result = llvm.diBuilderCreateBasicType(cg.di_builder, name.cstring, name.len.uint, ty.inner.signed.bits.bits.uint64, DW_ATE_signed, DiFlagZero)
  of tykUnsigned:
    case ty.inner.unsigned.bits.kind
    of bitsWord:
      let name = ty[].human_readable_name
      result = llvm.diBuilderCreateBasicType(cg.di_builder, name.cstring, name.len.uint, target.pointer_width.uint64, DW_ATE_unsigned, DiFlagZero)
    of bitsCustom:
      let name = ty[].human_readable_name
      result = llvm.diBuilderCreateBasicType(cg.di_builder, name.cstring, name.len.uint, ty.inner.unsigned.bits.bits.uint64, DW_ATE_unsigned, DiFlagZero)
  of tykFloating:
    let name = ty[].human_readable_name
    case ty.inner.floating.bits.kind
    of bitsWord:
      result = llvm.diBuilderCreateBasicType(cg.di_builder, name.cstring, name.len.uint, target.pointer_width.uint64, DW_ATE_float, DiFlagZero)
    of bitsCustom:
      case ty.inner.floating.bits.bits
      of 16:
        result = llvm.diBuilderCreateBasicType(cg.di_builder, name.cstring, name.len.uint, 16, DW_ATE_float, DiFlagZero)
      of 32:
        result = llvm.diBuilderCreateBasicType(cg.di_builder, name.cstring, name.len.uint, 32, DW_ATE_float, DiFlagZero)
      of 64:
        result = llvm.diBuilderCreateBasicType(cg.di_builder, name.cstring, name.len.uint, 64, DW_ATE_float, DiFlagZero)
      of 80:
        result = llvm.diBuilderCreateBasicType(cg.di_builder, name.cstring, name.len.uint, 80, DW_ATE_float, DiFlagZero)
      of 128:
        result = llvm.diBuilderCreateBasicType(cg.di_builder, name.cstring, name.len.uint, 128, DW_ATE_float, DiFlagZero)
      else:
        raise newError(errkInvalidType, span, "invalid floating type width")
  of tykTrait:
    let name = "TraitRecord"
    let temp = llvm.diBuilderCreateUnspecifiedType(cg.di_builder, name.cstring, name.len.uint)
    ty.meta = some(temp)

    let
      unit = llvm.diBuilderCreateUnspecifiedType(cg.di_builder, "unit".cstring, "unit".len.uint)
      unit_ptr = llvm.diBuilderCreatePointerType(cg.di_builder, unit, target.pointer_width.uint64, target.pointer_width.uint32, 0, "&unit".cstring, "&unit".len.uint)
      unitp_ptr = llvm.diBuilderCreatePointerType(cg.di_builder, unit_ptr, target.pointer_width.uint64, target.pointer_width.uint32, 0, "&&unit".cstring, "&&unit".len.uint)
    var fields = [unit_ptr, unitp_ptr]
    let
      fieldv = addr fields[0]
      id = name.toPath.mangled_as_type
      size = 2'u * target.pointer_width # in bits
      align = target.pointer_width # in bits
    result = llvm.diBuilderCreateStructType(cg.di_builder, cg.di_scope.get, name.cstring, name.len.uint, cg.di_file.get, span.line.cuint, size.uint64, align.uint32, DiFlagZero, nil, fieldv, cuint(fields.len), 0, nil, id.cstring, id.len.uint)
  of tykStruct:
    let name = ty.inner.struct.name.map((name) => name.path[^1]).get("struct")
    let temp = llvm.diBuilderCreateUnspecifiedType(cg.di_builder, name.cstring, name.len.uint)
    ty.meta = some(temp)

    var fields = newSeq[llvm.MetadataRef]()
    for field in ty.inner.struct.fields:
      let qfield = field.ty[].as_qualified_in_span(span)
      let field = qfield.di_qualified_type_first_class(span, cg, target)
      fields.add(field)
    let fieldv = if fields.len == 0: nil else: addr fields[0]
    let id = ty.mangled
    let
      s = ty.get_size_of(cg.type_ctx) # in bits
      a = ty.get_align_of(cg.type_ctx) # in bits
    var size, align: uint
    case s.kind
    of sokUnknown:
      raise newError(errkInvalidType, span, "unknown size")
    of sokUnsized:
      raise newError(errkInvalidType, span, "unsized")
    of sokSized:
      size = s.size.uint * 8
    case a.kind
    of sokUnknown:
      raise newError(errkInvalidType, span, "unknown size")
    of sokUnsized:
      raise newError(errkInvalidType, span, "unsized")
    of sokSized:
      align = a.size.uint * 8
    result = llvm.diBuilderCreateStructType(cg.di_builder, cg.di_scope.get, name.cstring, name.len.uint, cg.di_file.get, span.line.cuint, size.uint64, align.uint32, DiFlagZero, nil, fieldv, cuint(fields.len), 0, nil, id.cstring, id.len.uint)
  of tykUnion:
    let name = ty.inner.union.name.map((name) => name.path[^1]).get("union")
    let temp = llvm.diBuilderCreateUnspecifiedType(cg.di_builder, name.cstring, name.len.uint)
    ty.meta = some(temp)

    var fields = newSeq[llvm.MetadataRef]()
    for field in ty.inner.union.fields:
      let qfield = field.ty[].as_qualified_in_span(span)
      let field = qfield.di_qualified_type_first_class(span, cg, target)
      fields.add(field)
    let fieldv = if fields.len == 0: nil else: addr fields[0]
    let id = ty.mangled
    let
      s = ty.get_size_of(cg.type_ctx) # in bits
      a = ty.get_align_of(cg.type_ctx) # in bits
    var size, align: uint
    case s.kind
    of sokUnknown:
      raise newError(errkInvalidType, span, "unknown size")
    of sokUnsized:
      raise newError(errkInvalidType, span, "unsized")
    of sokSized:
      size = s.size.uint * 8
    case a.kind
    of sokUnknown:
      raise newError(errkInvalidType, span, "unknown size")
    of sokUnsized:
      raise newError(errkInvalidType, span, "unsized")
    of sokSized:
      align = a.size.uint * 8
    result = llvm.diBuilderCreateUnionType(cg.di_builder, cg.di_scope.get, name.cstring, name.len.uint, cg.di_file.get, span.line.cuint, size.uint64, align.uint32, DiFlagZero, fieldv, cuint(fields.len), 0, id.cstring, id.len.uint)
  of tykEnum:
    let inner = ty
      .inner
      .enumeration
      .inner
      .di_qualified_type_first_class(span, cg, target)
    result = inner
  of tykTagged:
    let name = ty.inner.tagged.name.path[^1]
    let temp = llvm.diBuilderCreateUnspecifiedType(cg.di_builder, name.cstring, name.len.uint)
    ty.meta = some(temp)

    var struct_types = newSeq[llvm.MetadataRef]()
    for variant in ty.inner.tagged.variants:
      var fields = newSeq[llvm.MetadataRef]()
      for field in variant.fields:
        let qfield = field.ty[].as_qualified_in_span(span)
        let field = qfield.di_qualified_type_first_class(span, cg, target)
        fields.add(field)
      let fieldv = if fields.len == 0: nil else: addr fields[0]
      let name = variant.name.path[^1]
      let id = variant.name.mangled_as_type # TODO: type arguments
      let size = 0 # TODO
      let align = 0 # TODO
      let struct_type = llvm.diBuilderCreateStructType(cg.di_builder, cg.di_scope.get, name.cstring, name.len.uint, cg.di_file.get, span.line.cuint, size.uint64, align.uint32, DiFlagZero, nil, fieldv, cuint(fields.len), 0, nil, id.cstring, id.len.uint)
      struct_types.add(struct_type)

    var fields = newSeq[llvm.MetadataRef]()
    for field in ty.inner.tagged.fields:
      let qfield = field.ty[].as_qualified_in_span(span)
      let field = qfield.di_qualified_type_first_class(span, cg, target)
      fields.add(field)
    let ts = ty.inner.tagged.get_tagged_tag_size(cg.type_ctx)
    let us = ty.inner.tagged.get_tagged_union_size(cg.type_ctx)
    var tag_size, union_size: uint
    case ts.kind
    of sokUnknown:
      raise newError(errkInvalidType, span, "unknown tag size")
    of sokUnsized:
      raise newError(errkInvalidType, span, "unsized tag size")
    of sokSized:
      tag_size = ts.size.uint * 8
    case us.kind
    of sokUnknown:
      raise newError(errkInvalidType, span, "unknown tag size")
    of sokUnsized:
      raise newError(errkInvalidType, span, "unsized tag size")
    of sokSized:
      union_size = us.size.uint * 8
    let tag = llvm.diBuilderCreateBasicType(cg.di_builder, "tag".cstring, "tag".len.uint, tag_size.uint64, DW_ATE_unsigned, DiFlagZero)
    let union_name = ty.inner.tagged.name.path[^1] & ".union"
    let union_id = ty.mangled & ".union"
    # TODO: union align
    let struct_typev = if struct_types.len == 0: nil else: addr struct_types[0]
    let union_type = llvm.diBuilderCreateUnionType(cg.di_builder, cg.di_scope.get, union_name.cstring, union_name.len.uint, cg.di_file.get, span.line.cuint, union_size.uint64, 0, DiFlagZero, struct_typev, cuint(struct_types.len), 0, union_id.cstring, union_id.len.uint)
    fields.add(tag)
    fields.add(union_type)
    let fieldv = addr fields[0]
    let
      s = ty.get_size_of(cg.type_ctx) # in bits
      a = ty.get_align_of(cg.type_ctx) # in bits
    var size, align: uint
    case s.kind
    of sokUnknown:
      raise newError(errkInvalidType, span, "unknown size")
    of sokUnsized:
      raise newError(errkInvalidType, span, "unsized")
    of sokSized:
      size = s.size.uint * 8
    case a.kind
    of sokUnknown:
      raise newError(errkInvalidType, span, "unknown size")
    of sokUnsized:
      raise newError(errkInvalidType, span, "unsized")
    of sokSized:
      align = a.size.uint * 8
    let id = ty.mangled
    result = llvm.diBuilderCreateStructType(cg.di_builder, cg.di_scope.get, name.cstring, name.len.uint, cg.di_file.get, span.line.cuint, size.uint64, align.uint32, DiFlagZero, nil, fieldv, cuint(fields.len), 0, nil, id.cstring, id.len.uint)
  of tykFunction:
    var params = newSeqOfCap[llvm.MetadataRef](ty.inner.function.params.len + 1)
    params.add(ty.inner.function.result_type[].as_qualified_in_span(span).di_qualified_type_first_class(span, cg, target))
    for param in ty.inner.function.params:
      let t = param.ty[].as_qualified_in_span(span)
      if t.inner.kind != tykUnit:
        params.add(t.di_qualified_type_first_class(span, cg, target))
    let param_ptr = addr params[0]
    result = llvm.diBuilderCreateSubroutineType(cg.di_builder, cg.di_file.get, param_ptr, cuint(params.len), DiFlagZero)
  of tykPointer:
    # llvm has a complicated relationship with void pointers
    let pointee = ty.inner.point.pointee[].as_qualified_in_span(span).di_qualified_type_first_class(span, cg, target)
    let name = ty[].human_readable_name
    result = llvm.diBuilderCreatePointerType(cg.di_builder, pointee, target.pointer_width.uint64, target.pointer_width.uint32, 0, name.cstring, name.len.uint)
  of tykSlice:
    let elem = ty.inner.slice.elem[].as_qualified_in_span(span).di_qualified_type_first_class(span, cg, target)
    let ptr_name = "&" & ty.inner.slice.elem[].as_qualified_in_span(span)[].name.path[^1]
    let name = ty[].human_readable_name

    let size = llvm.diBuilderCreateBasicType(cg.di_builder, "uint".cstring, "uint".len.uint, target.pointer_width.uint64, DW_ATE_unsigned, DiFlagZero)
    let ptr_type = llvm.diBuilderCreatePointerType(cg.di_builder, elem, target.pointer_width.uint64, target.pointer_width.uint32, 0, ptr_name.cstring, ptr_name.len.uint)

    var fields = @[size, ptr_type]
    let fieldv = addr fields[0]
    let id = ty.mangled
    result = llvm.diBuilderCreateStructType(cg.di_builder, cg.di_scope.get, name.cstring, name.len.uint, cg.di_file.get, span.line.cuint, target.pointer_width.uint64 * 2, target.pointer_width.uint32, DiFlagZero, nil, fieldv, cuint(fields.len), 0, nil, id.cstring, id.len.uint)
  of tykArray:
    let elem = ty.inner.arr.elem[].as_qualified_in_span(span).di_qualified_type_first_class(span, cg, target)
    let len = ty.inner.arr.size[].as_qualified_in_span(span).inner.lit.intval.uint64
    let name = ty[].human_readable_name
    let a = ty.get_align_of(cg.type_ctx) # in bits
    var align: uint
    case a.kind
    of sokUnknown:
      raise newError(errkInvalidType, span, "unknown size")
    of sokUnsized:
      raise newError(errkInvalidType, span, "unsized")
    of sokSized:
      align = a.size.uint * 8
    var subscripts = newSeq[llvm.MetadataRef]()
    let subscriptv: ptr llvm.MetadataRef = nil
    result = llvm.diBuilderCreateArrayType(cg.di_builder, len, align.uint32, elem, subscriptv, subscripts.len.cuint)
  of tykVector:
    let elem = ty.inner.vec.elem[].as_qualified_in_span(span).di_qualified_type_first_class(span, cg, target)
    let len = ty.inner.vec.size[].as_qualified_in_span(span).inner.lit.intval.uint64
    let name = ty[].human_readable_name
    let a = ty.get_align_of(cg.type_ctx) # in bits
    var align: uint
    case a.kind
    of sokUnknown:
      raise newError(errkInvalidType, span, "unknown size")
    of sokUnsized:
      raise newError(errkInvalidType, span, "unsized")
    of sokSized:
      align = a.size.uint * 8
    var subscripts = newSeq[llvm.MetadataRef]()
    let subscriptv: ptr llvm.MetadataRef = nil
    result = llvm.diBuilderCreateVectorType(cg.di_builder, len, align.uint32, elem, subscriptv, subscripts.len.cuint)
  of tykVar:
    let elem = ty.inner.vararr.elem[].as_qualified_in_span(span).di_qualified_type_first_class(span, cg, target)
    let name = ty[].human_readable_name

    let size = llvm.diBuilderCreateBasicType(cg.di_builder, "uint".cstring, "uint".len.uint, target.pointer_width.uint64, DW_ATE_unsigned, DiFlagZero)

    var fields = @[size]
    let fieldv = addr fields[0]
    let id = ty.mangled
    let struct_type = llvm.diBuilderCreateStructType(cg.di_builder, cg.di_scope.get, name.cstring, name.len.uint, cg.di_file.get, span.line.cuint, target.pointer_width.uint64 * 2, target.pointer_width.uint32, DiFlagZero, nil, fieldv, cuint(fields.len), 0, nil, id.cstring, id.len.uint)
    result = llvm.diBuilderCreatePointerType(cg.di_builder, struct_type, target.pointer_width.uint64, target.pointer_width.uint32, 0, name.cstring, name.len.uint)
  of tykParameter:
    raise newError(errkUnspecifiedType, span, "unevaluated type parameter")
  of tykLit:
    raise newError(errkFreestandingTypeLiteral, span)
  ty.meta = some(result)

proc di_qualified_type_first_class(ty: ref QualifiedType, span: Span, cg: CgPassController, target: ref Target): llvm.MetadataRef =
  if ty.meta.isSome:
    return ty.meta.get

  case ty.inner.kind
  of tykType:
    raise newError(errkInvalidType, span, "type is a meta-type")
  of tykNever:
    let name = "never"
    result = llvm.diBuilderCreateUnspecifiedType(cg.di_builder, name.cstring, name.len.uint)
  of tykUnit:
    let name = "unit"
    result = llvm.diBuilderCreateUnspecifiedType(cg.di_builder, name.cstring, name.len.uint)
  of tykBool:
    let name = "bool"
    result = llvm.diBuilderCreateBasicType(cg.di_builder, name.cstring, name.len.uint, 1, DW_ATE_boolean, DiFlagZero)
  of tykSigned:
    case ty.inner.signed.bits.kind
    of bitsWord:
      let name = ty[].human_readable_name
      result = llvm.diBuilderCreateBasicType(cg.di_builder, name.cstring, name.len.uint, target.pointer_width.uint64, DW_ATE_signed, DiFlagZero)
    of bitsCustom:
      let name = ty[].human_readable_name
      result = llvm.diBuilderCreateBasicType(cg.di_builder, name.cstring, name.len.uint, ty.inner.signed.bits.bits.uint64, DW_ATE_signed, DiFlagZero)
  of tykUnsigned:
    case ty.inner.unsigned.bits.kind
    of bitsWord:
      let name = ty[].human_readable_name
      result = llvm.diBuilderCreateBasicType(cg.di_builder, name.cstring, name.len.uint, target.pointer_width.uint64, DW_ATE_unsigned, DiFlagZero)
    of bitsCustom:
      let name = ty[].human_readable_name
      result = llvm.diBuilderCreateBasicType(cg.di_builder, name.cstring, name.len.uint, ty.inner.unsigned.bits.bits.uint64, DW_ATE_unsigned, DiFlagZero)
  of tykFloating:
    let name = ty[].human_readable_name
    case ty.inner.floating.bits.kind
    of bitsWord:
      result = llvm.diBuilderCreateBasicType(cg.di_builder, name.cstring, name.len.uint, target.pointer_width.uint64, DW_ATE_float, DiFlagZero)
    of bitsCustom:
      case ty.inner.floating.bits.bits
      of 16:
        result = llvm.diBuilderCreateBasicType(cg.di_builder, name.cstring, name.len.uint, 16, DW_ATE_float, DiFlagZero)
      of 32:
        result = llvm.diBuilderCreateBasicType(cg.di_builder, name.cstring, name.len.uint, 32, DW_ATE_float, DiFlagZero)
      of 64:
        result = llvm.diBuilderCreateBasicType(cg.di_builder, name.cstring, name.len.uint, 64, DW_ATE_float, DiFlagZero)
      of 80:
        result = llvm.diBuilderCreateBasicType(cg.di_builder, name.cstring, name.len.uint, 80, DW_ATE_float, DiFlagZero)
      of 128:
        result = llvm.diBuilderCreateBasicType(cg.di_builder, name.cstring, name.len.uint, 128, DW_ATE_float, DiFlagZero)
      else:
        raise newError(errkInvalidType, span, "invalid floating type width")
  of tykTrait:
    let name = "TraitRecord"
    let temp = llvm.diBuilderCreateUnspecifiedType(cg.di_builder, name.cstring, name.len.uint)
    ty.meta = some(temp)

    let
      unit = llvm.diBuilderCreateUnspecifiedType(cg.di_builder, "unit".cstring, "unit".len.uint)
      unit_ptr = llvm.diBuilderCreatePointerType(cg.di_builder, unit, target.pointer_width.uint64, target.pointer_width.uint32, 0, "&unit".cstring, "&unit".len.uint)
      unitp_ptr = llvm.diBuilderCreatePointerType(cg.di_builder, unit_ptr, target.pointer_width.uint64, target.pointer_width.uint32, 0, "&&unit".cstring, "&&unit".len.uint)
    var fields = [unit_ptr, unitp_ptr]
    let
      fieldv = addr fields[0]
      id = name.toPath.mangled_as_type
      size = 2'u * target.pointer_width # in bits
      align = target.pointer_width # in bits
    result = llvm.diBuilderCreateStructType(cg.di_builder, cg.di_scope.get, name.cstring, name.len.uint, cg.di_file.get, span.line.cuint, size.uint64, align.uint32, DiFlagZero, nil, fieldv, cuint(fields.len), 0, nil, id.cstring, id.len.uint)
  of tykStruct:
    let name = ty.inner.struct.name.map((name) => name.path[^1]).get("struct")
    let temp = llvm.diBuilderCreateUnspecifiedType(cg.di_builder, name.cstring, name.len.uint)
    ty.meta = some(temp)

    var fields = newSeq[llvm.MetadataRef]()
    for field in ty.inner.struct.fields:
      let qfield = field.ty[].as_qualified_in_span(span)
      let field = qfield.di_qualified_type_first_class(span, cg, target)
      fields.add(field)
    let fieldv = if fields.len == 0: nil else: addr fields[0]
    let id = ty.mangled
    let
      s = ty.get_size_of(cg.type_ctx) # in bits
      a = ty.get_align_of(cg.type_ctx) # in bits
    var size, align: uint
    case s.kind
    of sokUnknown:
      raise newError(errkInvalidType, span, "unknown size")
    of sokUnsized:
      raise newError(errkInvalidType, span, "unsized")
    of sokSized:
      size = s.size.uint * 8
    case a.kind
    of sokUnknown:
      raise newError(errkInvalidType, span, "unknown size")
    of sokUnsized:
      raise newError(errkInvalidType, span, "unsized")
    of sokSized:
      align = a.size.uint * 8
    result = llvm.diBuilderCreateStructType(cg.di_builder, cg.di_scope.get, name.cstring, name.len.uint, cg.di_file.get, span.line.cuint, size.uint64, align.uint32, DiFlagZero, nil, fieldv, cuint(fields.len), 0, nil, id.cstring, id.len.uint)
  of tykUnion:
    let name = ty.inner.union.name.map((name) => name.path[^1]).get("union")
    let temp = llvm.diBuilderCreateUnspecifiedType(cg.di_builder, name.cstring, name.len.uint)
    ty.meta = some(temp)

    var fields = newSeq[llvm.MetadataRef]()
    for field in ty.inner.union.fields:
      let qfield = field.ty[].as_qualified_in_span(span)
      let field = qfield.di_qualified_type_first_class(span, cg, target)
      fields.add(field)
    let fieldv = if fields.len == 0: nil else: addr fields[0]
    let id = ty.mangled
    let
      s = ty.get_size_of(cg.type_ctx) # in bits
      a = ty.get_align_of(cg.type_ctx) # in bits
    var size, align: uint
    case s.kind
    of sokUnknown:
      raise newError(errkInvalidType, span, "unknown size")
    of sokUnsized:
      raise newError(errkInvalidType, span, "unsized")
    of sokSized:
      size = s.size.uint * 8
    case a.kind
    of sokUnknown:
      raise newError(errkInvalidType, span, "unknown size")
    of sokUnsized:
      raise newError(errkInvalidType, span, "unsized")
    of sokSized:
      align = a.size.uint * 8
    result = llvm.diBuilderCreateUnionType(cg.di_builder, cg.di_scope.get, name.cstring, name.len.uint, cg.di_file.get, span.line.cuint, size.uint64, align.uint32, DiFlagZero, fieldv, cuint(fields.len), 0, id.cstring, id.len.uint)
  of tykEnum:
    let inner = ty
      .inner
      .enumeration
      .inner
      .di_qualified_type_first_class(span, cg, target)
    result = inner
  of tykTagged:
    let name = ty.inner.tagged.name.path[^1]
    let temp = llvm.diBuilderCreateUnspecifiedType(cg.di_builder, name.cstring, name.len.uint)
    ty.meta = some(temp)

    var struct_types = newSeq[llvm.MetadataRef]()
    for variant in ty.inner.tagged.variants:
      var fields = newSeq[llvm.MetadataRef]()
      for field in variant.fields:
        let qfield = field.ty[].as_qualified_in_span(span)
        let field = qfield.di_qualified_type_first_class(span, cg, target)
        fields.add(field)
      let fieldv = if fields.len == 0: nil else: addr fields[0]
      let name = variant.name.path[^1]
      let id = variant.name.mangled_as_type # TODO: type arguments
      let size = 0 # TODO
      let align = 0 # TODO
      let struct_type = llvm.diBuilderCreateStructType(cg.di_builder, cg.di_scope.get, name.cstring, name.len.uint, cg.di_file.get, span.line.cuint, size.uint64, align.uint32, DiFlagZero, nil, fieldv, cuint(fields.len), 0, nil, id.cstring, id.len.uint)
      struct_types.add(struct_type)

    var fields = newSeq[llvm.MetadataRef]()
    for field in ty.inner.tagged.fields:
      let qfield = field.ty[].as_qualified_in_span(span)
      let field = qfield.di_qualified_type_first_class(span, cg, target)
      fields.add(field)
    let ts = ty.inner.tagged.get_tagged_tag_size(cg.type_ctx)
    let us = ty.inner.tagged.get_tagged_union_size(cg.type_ctx)
    var tag_size, union_size: uint
    case ts.kind
    of sokUnknown:
      raise newError(errkInvalidType, span, "unknown tag size")
    of sokUnsized:
      raise newError(errkInvalidType, span, "unsized tag size")
    of sokSized:
      tag_size = ts.size.uint * 8
    case us.kind
    of sokUnknown:
      raise newError(errkInvalidType, span, "unknown tag size")
    of sokUnsized:
      raise newError(errkInvalidType, span, "unsized tag size")
    of sokSized:
      union_size = us.size.uint * 8
    let tag = llvm.diBuilderCreateBasicType(cg.di_builder, "tag".cstring, "tag".len.uint, tag_size.uint64, DW_ATE_unsigned, DiFlagZero)
    let union_name = ty.inner.tagged.name.path[^1] & ".union"
    let union_id = ty.mangled & ".union"
    # TODO: union align
    let struct_typev = if struct_types.len == 0: nil else: addr struct_types[0]
    let union_type = llvm.diBuilderCreateUnionType(cg.di_builder, cg.di_scope.get, union_name.cstring, union_name.len.uint, cg.di_file.get, span.line.cuint, union_size.uint64, 0, DiFlagZero, struct_typev, cuint(struct_types.len), 0, union_id.cstring, union_id.len.uint)
    fields.add(tag)
    fields.add(union_type)
    let fieldv = addr fields[0]
    let
      s = ty.get_size_of(cg.type_ctx) # in bits
      a = ty.get_align_of(cg.type_ctx) # in bits
    var size, align: uint
    case s.kind
    of sokUnknown:
      raise newError(errkInvalidType, span, "unknown size")
    of sokUnsized:
      raise newError(errkInvalidType, span, "unsized")
    of sokSized:
      size = s.size.uint * 8
    case a.kind
    of sokUnknown:
      raise newError(errkInvalidType, span, "unknown size")
    of sokUnsized:
      raise newError(errkInvalidType, span, "unsized")
    of sokSized:
      align = a.size.uint * 8
    let id = ty.mangled
    result = llvm.diBuilderCreateStructType(cg.di_builder, cg.di_scope.get, name.cstring, name.len.uint, cg.di_file.get, span.line.cuint, size.uint64, align.uint32, DiFlagZero, nil, fieldv, cuint(fields.len), 0, nil, id.cstring, id.len.uint)
  of tykFunction:
    var params = newSeqOfCap[llvm.MetadataRef](ty.inner.function.params.len + 1)
    params.add(ty.inner.function.result_type[].as_qualified_in_span(span).di_qualified_type_first_class(span, cg, target))
    for param in ty.inner.function.params:
      let t = param.ty[].as_qualified_in_span(span)
      if t.inner.kind != tykUnit:
        params.add(t.di_qualified_type_first_class(span, cg, target))
    let param_ptr = addr params[0]
    let function_type = llvm.diBuilderCreateSubroutineType(cg.di_builder, cg.di_file.get, param_ptr, cuint(params.len), DiFlagZero)
    let name = "&" & ty[].human_readable_name
    result = llvm.diBuilderCreatePointerType(cg.di_builder, function_type, target.pointer_width.uint64, target.pointer_width.uint32, 0, name.cstring, name.len.uint)
  of tykPointer:
    # llvm has a complicated relationship with void pointers
    let pointee = ty.inner.point.pointee[].as_qualified_in_span(span).di_qualified_type_first_class(span, cg, target)
    let name = ty[].human_readable_name
    result = llvm.diBuilderCreatePointerType(cg.di_builder, pointee, target.pointer_width.uint64, target.pointer_width.uint32, 0, name.cstring, name.len.uint)
  of tykSlice:
    let elem = ty.inner.slice.elem[].as_qualified_in_span(span).di_qualified_type_first_class(span, cg, target)
    let ptr_name = "&" & ty.inner.slice.elem[].as_qualified_in_span(span)[].name.path[^1]
    let name = ty[].human_readable_name

    let size = llvm.diBuilderCreateBasicType(cg.di_builder, "uint".cstring, "uint".len.uint, target.pointer_width.uint64, DW_ATE_unsigned, DiFlagZero)
    let ptr_type = llvm.diBuilderCreatePointerType(cg.di_builder, elem, target.pointer_width.uint64, target.pointer_width.uint32, 0, ptr_name.cstring, ptr_name.len.uint)

    var fields = @[size, ptr_type]
    let fieldv = addr fields[0]
    let id = ty.mangled
    result = llvm.diBuilderCreateStructType(cg.di_builder, cg.di_scope.get, name.cstring, name.len.uint, cg.di_file.get, span.line.cuint, target.pointer_width.uint64 * 2, target.pointer_width.uint32, DiFlagZero, nil, fieldv, cuint(fields.len), 0, nil, id.cstring, id.len.uint)
  of tykArray:
    let elem = ty.inner.arr.elem[].as_qualified_in_span(span).di_qualified_type_first_class(span, cg, target)
    let len = ty.inner.arr.size[].as_qualified_in_span(span).inner.lit.intval.uint64
    let name = ty[].human_readable_name
    let a = ty.get_align_of(cg.type_ctx) # in bits
    var align: uint
    case a.kind
    of sokUnknown:
      raise newError(errkInvalidType, span, "unknown size")
    of sokUnsized:
      raise newError(errkInvalidType, span, "unsized")
    of sokSized:
      align = a.size.uint * 8
    var subscripts = newSeq[llvm.MetadataRef]()
    let subscriptv: ptr llvm.MetadataRef = nil
    result = llvm.diBuilderCreateArrayType(cg.di_builder, len, align.uint32, elem, subscriptv, subscripts.len.cuint)
  of tykVector:
    let elem = ty.inner.vec.elem[].as_qualified_in_span(span).di_qualified_type_first_class(span, cg, target)
    let len = ty.inner.vec.size[].as_qualified_in_span(span).inner.lit.intval.uint64
    let name = ty[].human_readable_name
    let a = ty.get_align_of(cg.type_ctx) # in bits
    var align: uint
    case a.kind
    of sokUnknown:
      raise newError(errkInvalidType, span, "unknown size")
    of sokUnsized:
      raise newError(errkInvalidType, span, "unsized")
    of sokSized:
      align = a.size.uint * 8
    var subscripts = newSeq[llvm.MetadataRef]()
    let subscriptv: ptr llvm.MetadataRef = nil
    result = llvm.diBuilderCreateVectorType(cg.di_builder, len, align.uint32, elem, subscriptv, subscripts.len.cuint)
  of tykVar:
    let elem = ty.inner.vararr.elem[].as_qualified_in_span(span).di_qualified_type_first_class(span, cg, target)
    let name = ty[].human_readable_name

    let size = llvm.diBuilderCreateBasicType(cg.di_builder, "uint".cstring, "uint".len.uint, target.pointer_width.uint64, DW_ATE_unsigned, DiFlagZero)

    var fields = @[size]
    let fieldv = addr fields[0]
    let id = ty.mangled
    let struct_type = llvm.diBuilderCreateStructType(cg.di_builder, cg.di_scope.get, name.cstring, name.len.uint, cg.di_file.get, span.line.cuint, target.pointer_width.uint64 * 2, target.pointer_width.uint32, DiFlagZero, nil, fieldv, cuint(fields.len), 0, nil, id.cstring, id.len.uint)
    result = llvm.diBuilderCreatePointerType(cg.di_builder, struct_type, target.pointer_width.uint64, target.pointer_width.uint32, 0, name.cstring, name.len.uint)
  of tykParameter:
    raise newError(errkUnspecifiedType, span, "unevaluated type parameter")
  of tykLit:
    raise newError(errkFreestandingTypeLiteral, span)
  ty.meta = some(result)
