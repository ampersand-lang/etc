import ../error
import ../span
import ../ice
import ../llvm/llvm

type
  CgVarKind* = enum
    cvkUninit
    cvkUnit
    cvkValue
    cvkPointer
  CgVar* = object
    case kind*: CgVarKind
    of cvkUninit, cvkUnit: discard
    of cvkValue, cvkPointer:
      value*: llvm.ValueRef

proc newCgVar*(): ref CgVar =
  new result
  result[] = CgVar(kind: cvkUninit)

proc initCgUnit*(): CgVar =
  result = CgVar(kind: cvkUnit)

proc initCgValue*(value: llvm.ValueRef): CgVar =
  result = CgVar(kind: cvkValue, value: value)

proc initCgPointer*(value: llvm.ValueRef): CgVar =
  result = CgVar(kind: cvkPointer, value: value)

proc kind*(value: CgVar): CgVarKind = value.kind

proc create_pointer*(value: var CgVar, fn: llvm.ValueRef, b: llvm.BuilderRef, up_bb: llvm.BasicBlockRef): llvm.ValueRef =
  case value.kind
  of cvkUninit: raise newIce("uninitialized CgVar")
  of cvkUnit: return nil
  of cvkValue:
    value.kind = cvkPointer
    let v = value.value
    let bb = llvm.getFirstBasicBlock(fn)
    let last = llvm.getLastInstruction(bb)
    llvm.positionBuilderBefore(b, last)
    value.value = llvm.buildAlloca(b, llvm.typeOf(v), cstring(""))
    llvm.positionBuilderAtEnd(b, up_bb)
    llvm.buildStore(b, v, value.value)
    return value.value
  of cvkPointer: return value.value

proc get_pointer*(value: CgVar, span: Span): llvm.ValueRef =
  case value.kind
  of cvkUninit: raise newIce("uninitialized CgVar")
  of cvkUnit: return nil
  of cvkValue: raise newError(errkNoLValue, span)
  of cvkPointer: return value.value

proc load_with_builder*(value: CgVar, b: llvm.BuilderRef): llvm.ValueRef =
  case value.kind
  of cvkUninit: raise newIce("uninitialized CgVar")
  of cvkUnit: return nil
  of cvkValue: return value.value
  of cvkPointer: return llvm.buildLoad(b, value.value, cstring(""))

proc get*(value: CgVar, span: Span): llvm.ValueRef =
  case value.kind
  of cvkUninit: raise newIce("uninitialized CgVar")
  of cvkUnit: return nil
  of cvkValue: return value.value
  of cvkPointer: raise newError(errkLValue, span)

proc store_with_builder*(dest: CgVar, value: CgVar, b: llvm.BuilderRef, span: Span) =
  case dest.kind
  of cvkUninit: raise newIce("uninitialized CgVar")
  of cvkUnit:
    case value.kind
    of cvkUnit: discard
    else: raise newError(errkInvalidType, span, "can only store a unit value in a unit variable")
  of cvkValue: raise newError(errkNoLValue, span)
  of cvkPointer:
    llvm.buildStore(b, value.load_with_builder(b), dest.value)
