import options
import sequtils
import strformat
import ice
import path
import types/types

type
  # a set of short, full and mangled names
  Names* = object
    short*: Path
    full*: Option[Path]
    mangled*: Option[string]

proc qualify*(name: var Names, module: Path) =
  if name.full.isSome:
    return
  if name.short.full:
    name.full = some[Path](name.short)
    name.full.get.full = true
  else:
    name.full = some[Path](module & name.short)

proc is_mangled*(name: Names): bool = name.mangled.isSome

proc mangle*(name: var Names) =
  if name.full.isNone:
    raise newIce("name is not fully qualified")
  if name.mangled.isSome:
    return

  var mangled_ident = "_ZN"
  for ident in name.full.get:
    mangled_ident.addInt(ident.len)
    mangled_ident.add(ident)

  mangled_ident.add("E")

  name.mangled = some[string](mangled_ident)

proc mangled*(ty: ref QualifiedType): string

proc mangled*(ty: ref QualifiedType): string =
  if ty.mangled_name.isSome:
    return ty.mangled_name.get

  result = "N"
  case ty.inner.kind
  of tykUnit, tykNever, tykBool, tykSigned, tykUnsigned, tykFloating, tykType:
    let short = $ty[]
    result.addInt(short.len)
    result.add(short)
  of tykTrait:
    for ident in ty.inner.trait.name.path:
      result.addInt(ident.len)
      result.add(ident)
  of tykStruct:
    if ty.inner.struct.name.isSome:
      for ident in ty.inner.struct.name.get.path:
        result.addInt(ident.len)
        result.add(ident)
    else:
      result.addInt("struct".len)
      result.add("struct")
      result.add("I")
      for field in ty.inner.struct.fields:
        result.add(field.ty[].as_qualified.mangled)
      result.add("E")
  of tykUnion:
    if ty.inner.union.name.isSome:
      for ident in ty.inner.union.name.get.path:
        result.addInt(ident.len)
        result.add(ident)
    else:
      result.addInt("union".len)
      result.add("union")
      result.add("I")
      for field in ty.inner.union.fields:
        result.add(field.ty[].as_qualified.mangled)
      result.add("E")
  of tykEnum:
    for ident in ty.inner.enumeration.name.path:
      result.addInt(ident.len)
      result.add(ident)
  of tykTagged:
    for ident in ty.inner.tagged.name.path:
      result.addInt(ident.len)
      result.add(ident)
  of tykFunction:
    result = "PF"
    result.add(ty.inner.function.result_type[].as_qualified.mangled)
    for param in ty.inner.function.params:
      result.add(param.ty[].as_qualified.mangled)
    if ty.inner.function.params.len == 0:
      result.add("v")
    ty.mangled_name = some(result)
    return
  of tykPointer:
    result = "P"
    result.add(ty.inner.point.pointee[].as_qualified.mangled)
    ty.mangled_name = some(result)
    return
  of tykSlice:
    result.addInt("__lang".len)
    result.add("__lang")
    result.addInt("builtin".len)
    result.add("builtin")
    result.addInt("slice".len)
    result.add("slice")
    result.add("I")
    result.add(ty.inner.slice.elem[].as_qualified.mangled)
    result.add("E")
  of tykArray:
    result.addInt("__lang".len)
    result.add("__lang")
    result.addInt("builtin".len)
    result.add("builtin")
    result.addInt("array".len)
    result.add("array")
    result.add("I")
    result.add(ty.inner.arr.elem[].as_qualified.mangled)
    result.add(ty.inner.arr.size[].as_qualified.mangled)
    result.add("E")
  of tykVector:
    result.addInt("__lang".len)
    result.add("__lang")
    result.addInt("builtin".len)
    result.add("builtin")
    result.addInt("vector".len)
    result.add("vector")
    result.add("I")
    result.add(ty.inner.vec.elem[].as_qualified.mangled)
    result.add(ty.inner.vec.size[].as_qualified.mangled)
    result.add("E")
  of tykVar:
    result.addInt("__lang".len)
    result.add("__lang")
    result.addInt("builtin".len)
    result.add("builtin")
    result.addInt("var".len)
    result.add("var")
    result.add("I")
    result.add(ty.inner.vararr.elem[].as_qualified.mangled)
    result.add("E")
  of tykParameter:
    raise newIce(fmt"unevaluated type parameter: {ty[]}")
  of tykLit:
    result.add("L")
    result.add(ty.inner.lit.ty[].as_qualified.mangled)
    result.addInt(ty.inner.lit.intval)
    result.add("E")

  if ty[].has_type_args:
    result.add("I")
    for ty in ty.type_args:
      result.add(ty.mangled)
    result.add("E")
  result.add("E")

  ty.mangled_name = some(result)

proc mangle_with_params*(name: var Names, params: openarray[ref QualifiedType]) =
  if name.full.isNone:
    raise newIce("name is not fully qualified")
  if name.mangled.isSome:
    return

  var mangled_ident = "_ZN"
  for ident in name.full.get:
    mangled_ident.addInt(ident.len)
    mangled_ident.add(ident)

  mangled_ident.add("E")

  for ty in params:
    mangled_ident.add(ty.mangled())

  if params.len == 0:
    mangled_ident.add("v")

  name.mangled = some[string](mangled_ident)

proc mangle_as_type*(name: var Names) =
  if name.full.isNone:
    raise newIce("name is not fully qualified")
  if name.mangled.isSome:
    return

  var mangled_ident = "N"
  for ident in name.full.get:
    mangled_ident.addInt(ident.len)
    mangled_ident.add(ident)

  mangled_ident.add("E")

  name.mangled = some[string](mangled_ident)

proc mangle_as_type_with_params*(name: var Names, params: openarray[ref QualifiedType]) =
  if name.full.isNone:
    raise newIce("name is not fully qualified")
  if name.mangled.isSome:
    return

  var mangled_ident = "N"
  for ident in name.full.get:
    mangled_ident.addInt(ident.len)
    mangled_ident.add(ident)

  mangled_ident.add("I")
  for ty in params:
    mangled_ident.add(ty.mangled())
  mangled_ident.add("E")

  if params.len == 0:
    mangled_ident.add("v")

  mangled_ident.add("E")

  name.mangled = some[string](mangled_ident)

proc mangled*(name: Path): string =
  var name = Names(short: name, full: some(name))
  name.mangle
  return name.mangled.get

proc mangled_with_params*(name: Path, params: openarray[ref QualifiedType]): string =
  var name = Names(short: name, full: some(name))
  name.mangle_with_params(params)
  return name.mangled.get

proc mangled_as_type*(name: Path): string =
  var name = Names(short: name, full: some(name))
  name.mangle_as_type
  return name.mangled.get

proc mangled_as_type_with_params*(name: Path, params: openarray[ref QualifiedType]): string =
  var name = Names(short: name, full: some(name))
  name.mangle_as_type_with_params(params)
  return name.mangled.get
