import tables
import options
import sequtils

import span
import path
import names
import index
import driver/driver_typedefs
import hlir/hlir
import types/types
import macros/typedefs
import macros/macros
import macros/builtin

type
  BuiltinFunctionKind = enum
    bfkComptime
    bfkIntrinsic
    bfkVar
    bfkCons
  BuiltinFunction = object
    impl: ref Impl
    case kind: BuiltinFunctionKind
    of bfkComptime:
      comptime: proc (ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue
      params: seq[string]
      noeval: bool
    of bfkIntrinsic: intrinsic: proc (cg: CgPassController, caller: ref Node, args: seq[ref Node])
    of bfkVar:
      value: MacroValue
    of bfkCons: nil
  BuiltinModule = object
    name: Path
    funcs: seq[BuiltinFunction]
  BuiltinModuleContext = object
    modules: Table[Path, BuiltinModule]

proc init_macros_module*(ctx: var BuiltinModuleContext, type_ctx: TypeContext)
proc init_macros_build_module*(ctx: var BuiltinModuleContext, type_ctx: TypeContext)
proc init_macros_pkg_module*(ctx: var BuiltinModuleContext, type_ctx: TypeContext)

proc init_modules*(type_ctx: TypeContext): BuiltinModuleContext =
  result.modules = initTable[Path, BuiltinModule]()
  init_macros_module(result, type_ctx)
  init_macros_build_module(result, type_ctx)
  init_macros_pkg_module(result, type_ctx)

proc load_module*(ctx: BuiltinModuleContext, name: Path, type_ctx: TypeContext, macro_ctx: MacroContext, cg_pass: CgPassController) =
  let module = ctx.modules[name]

  for fn in module.funcs:
    type_ctx.add_impl(fn.impl.name, fn.impl)
    case fn.kind
    of bfkComptime:
      let mvar = MacroVariable(
        kind: mvakRval,
        val: MacroValue(kind: mvkFfi, ffi: MacroFfi(name: fn.impl.name.path[^1], params: fn.params, ffi: fn.comptime, noeval: fn.noeval)),
      )
      macro_ctx.bind_var(fn.impl.name.mangled_with_params(fn.impl.params.mapIt(it.ty[].as_qualified)), mvar)
    of bfkIntrinsic:
      discard
    of bfkVar:
      let mvar = MacroVariable(
        kind: mvakRval,
        val: fn.value,
      )
      macro_ctx.bind_var(fn.impl.name.mangled, mvar)
    of bfkCons:
      discard

proc init_macros_module*(ctx: var BuiltinModuleContext, type_ctx: TypeContext) =
  ctx.modules.add(["__lang", "macros"].toPath, BuiltinModule(name: ["__lang", "macros"].toPath, funcs: newSeq[BuiltinFunction]()))

  let generic_elem = newType(Type(kind: tykParameter, param: TypeParameter(kind: pkType, name: "T"))).toQualifiedType.toTypeSumRef

  # #[lang(span)]
  # struct ::__lang::macros::Span {
  #   directory str,
  #   file str,
  #   line uint,
  #   col uint,
  # }
  let span_name = ["__lang", "macros", "Span"].toPath
  let span_fields = @[
    StructField(name: some("directory"), ty: type_ctx.get(["__lang", "builtin", "str"].toPath).toTypeSumRef),
    StructField(name: some("file"), ty: type_ctx.get(["__lang", "builtin", "str"].toPath).toTypeSumRef),
    StructField(name: some("line"), ty: type_ctx.get("uint".toPath).toTypeSumRef),
    StructField(name: some("col"), ty: type_ctx.get("uint".toPath).toTypeSumRef),
  ]
  type_ctx.types.add(span_name, newType(Type(kind: tykStruct, struct: TypeStruct(name: some(span_name), fields: span_fields))).toQualifiedType)
  let span_impl = newConsImpl(span_name, none[string](), true, false, true, type_ctx.get(span_name).toTypeSumRef)
  ctx.modules[["__lang", "macros"].toPath].funcs.add(BuiltinFunction(kind: bfkCons, impl: span_impl))

  # #[lang(node)]
  # enum ::__lang::macros::BinaryKind {
  #   INDEX,
  #   ADD,
  #   SUB,
  #   MUL,
  #   DIV,
  #   REM,
  #   SHL,
  #   SHR,
  #   LT,
  #   GT,
  #   LE,
  #   GE,
  #   EQ,
  #   NE,
  #   BITAND,
  #   BITOR,
  #   BITXOR,
  #   LOGICAL_AND,
  #   LOGICAL_OR,
  #   RANGE,
  #   ASSIGN,
  #   ASSIGN_MUL,
  #   ASSIGN_DIV,
  #   ASSIGN_REM,
  #   ASSIGN_ADD,
  #   ASSIGN_SUB,
  #   ASSIGN_LSHIFT,
  #   ASSIGN_RSHIFT,
  #   ASSIGN_AND,
  #   ASSIGN_XOR,
  #   ASSIGN_OR,
  # }
  let binary_kind_name = ["__lang", "macros", "BinaryKind"].toPath
  let binary_kind_inner = type_ctx.get("u8".toPath)
  type_ctx.types.add(binary_kind_name, newType(Type(kind: tykEnum, enumeration: TypeEnum(name: binary_kind_name, inner: binary_kind_inner))).toQualifiedType)
  let binary_kind_impl = newConsImpl(binary_kind_name, none[string](), true, false, true, type_ctx.get(binary_kind_name).toTypeSumRef)
  ctx.modules[["__lang", "macros"].toPath].funcs.add(BuiltinFunction(kind: bfkCons, impl: binary_kind_impl))

  # #[lang(node)]
  # struct ::__lang::macros::Node { }
  let node_name = ["__lang", "macros", "Node"].toPath
  let node_fields = newSeq[StructField]()
  type_ctx.types.add(node_name, newType(Type(kind: tykStruct, struct: TypeStruct(name: some(node_name), fields: node_fields))).toQualifiedType)
  let node_impl = newConsImpl(node_name, none[string](), true, false, true, type_ctx.get(node_name).toTypeSumRef)
  ctx.modules[["__lang", "macros"].toPath].funcs.add(BuiltinFunction(kind: bfkCons, impl: node_impl))

  # #[lang(typed)]
  # struct ::__lang::macros::Typed {
  #   T: type,
  #   node: ::__lang::macros::Node ,
  #   T,
  # }
  let typed_name = ["__lang", "macros", "Typed"].toPath
  let typed_fields = @[
    StructField(name: some("node"), ty: type_ctx.get(node_name).toTypeSumRef),
    StructField(name: none[string](), ty: generic_elem),
  ]
  type_ctx.types.add(typed_name, newType(Type(kind: tykStruct, struct: TypeStruct(name: some(typed_name), fields: typed_fields))).toQualifiedType)
  let typed_impl = newConsImpl(typed_name, none[string](), true, false, true, type_ctx.get(typed_name).toTypeSumRef)
  ctx.modules[["__lang", "macros"].toPath].funcs.add(BuiltinFunction(kind: bfkCons, impl: typed_impl))

  # #[lang(lang_context)]
  # struct ::__lang::macros::LangContext {
  #   call_site __lang::macros::Span,
  # }
  let lang_ctx_name = ["__lang", "macros", "LangContext"].toPath
  let lang_ctx_fields = @[
    StructField(name: some("call_site"), ty: type_ctx.get(span_name).toTypeSumRef),
    StructField(name: some("release"), ty: type_ctx.get("bool".toPath).toTypeSumRef),
  ]
  type_ctx.types.add(lang_ctx_name, newType(Type(kind: tykStruct, struct: TypeStruct(name: some(lang_ctx_name), fields: lang_ctx_fields))).toQualifiedType)
  let lang_ctx_impl = newConsImpl(lang_ctx_name, none[string](), true, false, true, type_ctx.get(lang_ctx_name).toTypeSumRef)
  ctx.modules[["__lang", "macros"].toPath].funcs.add(BuiltinFunction(kind: bfkCons, impl: lang_ctx_impl))

  let lang_ctx_var_name = ["__lang", "macros", "ctx"].toPath
  let lang_ctx_var_impl = newVarImpl(lang_ctx_var_name, none[string](), true, true, true, type_ctx.get(lang_ctx_name).toTypeSumRef)
  let
    fields = {
      "call_site": MacroValue(kind: mvkUninit),
      "release": MacroValue(kind: mvkBool, p: MacroBool(boolval: false)),
    }.toTable
    lang_ctx = MacroValue(ty: type_ctx.get(lang_ctx_name).toTypeSumRef, kind: mvkStruct, struct: MacroStruct(fields: fields))
  ctx.modules[["__lang", "macros"].toPath].funcs.add(BuiltinFunction(kind: bfkVar, impl: lang_ctx_var_impl, value: lang_ctx))

  let binary_kind_add_var_name = ["__lang", "macros", "BinaryKind", "ADD"].toPath
  let binary_kind_add_impl = newVarImpl(binary_kind_add_var_name, none[string](), true, true, true, type_ctx.get(binary_kind_name).toTypeSumRef)
  let
    binary_kind_add = MacroValue(kind: mvkEnum, enumeration: MacroEnum(name: "ADD", intval: hbkAdd.int64))
  ctx.modules[["__lang", "macros"].toPath].funcs.add(BuiltinFunction(kind: bfkVar, impl: binary_kind_add_impl, value: binary_kind_add))

  let error_name = ["__lang", "macros", "error"].toPath

  let error_str_params = @[Argument(name: some("msg"), ty: type_ctx.get(["__lang", "builtin", "str"].toPath).toTypeSumRef)]
  let error_str_impl = newFnImpl(error_name, none[string](), true, false, true, type_ctx.get("unit".toPath).toTypeSumRef, error_str_params.mapIt(it), false, true, true)
  ctx.modules[["__lang", "macros"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: error_str_impl, comptime: builtin.error, params: @["msg"]))

  let warn_name = ["__lang", "macros", "warn"].toPath

  let warn_str_params = @[Argument(name: some("msg"), ty: type_ctx.get(["__lang", "builtin", "str"].toPath).toTypeSumRef)]
  let warn_str_impl = newFnImpl(warn_name, none[string](), true, false, true, type_ctx.get("unit".toPath).toTypeSumRef, warn_str_params.mapIt(it), false, true, true)
  ctx.modules[["__lang", "macros"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: warn_str_impl, comptime: builtin.warn, params: @["msg"]))

  let debug_name = ["__lang", "macros", "debug"].toPath

  let debug_str_params = @[Argument(name: some("msg"), ty: type_ctx.get(["__lang", "builtin", "str"].toPath).toTypeSumRef)]
  let debug_str_impl = newFnImpl(debug_name, none[string](), true, false, true, type_ctx.get("unit".toPath).toTypeSumRef, debug_str_params.mapIt(it), false, true, true)
  ctx.modules[["__lang", "macros"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: debug_str_impl, comptime: builtin.debug, params: @["msg"]))

  let info_name = ["__lang", "macros", "info"].toPath

  let info_str_params = @[Argument(name: some("msg"), ty: type_ctx.get(["__lang", "builtin", "str"].toPath).toTypeSumRef)]
  let info_str_impl = newFnImpl(info_name, none[string](), true, false, true, type_ctx.get("unit".toPath).toTypeSumRef, info_str_params.mapIt(it), false, true, true)
  ctx.modules[["__lang", "macros"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: info_str_impl, comptime: builtin.info, params: @["msg"]))

  let eval_name = ["__lang", "macros", "eval"].toPath

  let eval_params = @[Argument(name: some("expr"), ty: type_ctx.get(["__lang", "macros", "Node"].toPath).toTypeSumRef)]
  let eval_impl = newFnImpl(eval_name, none[string](), true, false, true, type_ctx.get(["__lang", "macros", "Node"].toPath).toTypeSumRef, eval_params.mapIt(it), false, true, true)
  ctx.modules[["__lang", "macros"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: eval_impl, comptime: macros.eval, params: @["expr"]))

  let quote_name = ["__lang", "macros", "quote"].toPath

  let quote_params = @[Argument(name: some("body"), ty: type_ctx.get(["__lang", "macros", "Node"].toPath).toTypeSumRef)]
  let quote_impl = newFnImpl(quote_name, none[string](), true, false, true, type_ctx.get(["__lang", "macros", "Node"].toPath).toTypeSumRef, quote_params.mapIt(it), false, true, true)
  ctx.modules[["__lang", "macros"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: quote_impl, comptime: macros.quote, params: @["body"], noeval: true))

  let quasiquote_name = ["__lang", "macros", "quasiquote"].toPath

  let quasiquote_params = @[Argument(name: some("body"), ty: type_ctx.get(["__lang", "macros", "Node"].toPath).toTypeSumRef)]
  let quasiquote_impl = newFnImpl(quasiquote_name, none[string](), true, false, true, type_ctx.get(["__lang", "macros", "Node"].toPath).toTypeSumRef, quasiquote_params.mapIt(it), false, true, true)
  ctx.modules[["__lang", "macros"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: quasiquote_impl, comptime: macros.quasiquote, params: @["body"], noeval: true))

  let stringify_name = ["__lang", "macros", "stringify"].toPath

  let stringify_params = @[Argument(name: some("node"), ty: type_ctx.get(["__lang", "macros", "Node"].toPath).toTypeSumRef)]
  let stringify_impl = newFnImpl(stringify_name, none[string](), true, false, true, type_ctx.get(["__lang", "macros", "Node"].toPath).toTypeSumRef, stringify_params.mapIt(it), false, true, true)
  ctx.modules[["__lang", "macros"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: stringify_impl, comptime: builtin.stringify, params: @["node"], noeval: false))

  let new_node_expr_int_name = ["__lang", "macros", "new_node_expr_int"].toPath
  let new_node_expr_int_params = @[
    Argument(name: some("span"), ty: type_ctx.get(["__lang", "macros", "Span"].toPath).toTypeSumRef),
    Argument(name: some("value"), ty: type_ctx.get("sint".toPath).toTypeSumRef),
  ]
  let new_node_expr_int_impl = newFnImpl(new_node_expr_int_name, none[string](), true, false, true, type_ctx.get(["__lang", "macros", "Node"].toPath).toTypeSumRef, new_node_expr_int_params.mapIt(it), false, true, true)
  ctx.modules[["__lang", "macros"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: new_node_expr_int_impl, comptime: builtin.new_node_expr_int, params: @["span", "value"]))

  let new_node_expr_bool_name = ["__lang", "macros", "new_node_expr_bool"].toPath
  let new_node_expr_bool_params = @[
    Argument(name: some("span"), ty: type_ctx.get(["__lang", "macros", "Span"].toPath).toTypeSumRef),
    Argument(name: some("value"), ty: type_ctx.get("bool".toPath).toTypeSumRef),
  ]
  let new_node_expr_bool_impl = newFnImpl(new_node_expr_bool_name, none[string](), true, false, true, type_ctx.get(["__lang", "macros", "Node"].toPath).toTypeSumRef, new_node_expr_bool_params.mapIt(it), false, true, true)
  ctx.modules[["__lang", "macros"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: new_node_expr_bool_impl, comptime: builtin.new_node_expr_bool, params: @["span", "value"]))

  let new_node_expr_binary_name = ["__lang", "macros", "new_node_expr_binary"].toPath
  let new_node_expr_binary_params = @[
    Argument(name: some("span"), ty: type_ctx.get(["__lang", "macros", "Span"].toPath).toTypeSumRef),
    Argument(name: some("op"), ty: type_ctx.get(["__lang", "macros", "BinaryKind"].toPath).toTypeSumRef),
    Argument(name: some("lhs"), ty: type_ctx.get(["__lang", "macros", "Node"].toPath).toTypeSumRef),
    Argument(name: some("rhs"), ty: type_ctx.get(["__lang", "macros", "Node"].toPath).toTypeSumRef),
  ]
  let new_node_expr_binary_impl = newFnImpl(new_node_expr_binary_name, none[string](), true, false, true, type_ctx.get(["__lang", "macros", "Node"].toPath).toTypeSumRef, new_node_expr_binary_params.mapIt(it), false, true, true)
  ctx.modules[["__lang", "macros"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: new_node_expr_binary_impl, comptime: builtin.new_node_expr_binary, params: @["span", "op", "lhs", "rhs"]))

  let new_node_stmt_while_name = ["__lang", "macros", "new_node_stmt_while"].toPath
  let new_node_stmt_while_params = @[
    Argument(name: some("span"), ty: type_ctx.get(["__lang", "macros", "Span"].toPath).toTypeSumRef),
    Argument(name: some("cond"), ty: type_ctx.get(["__lang", "macros", "Node"].toPath).toTypeSumRef),
    Argument(name: some("body"), ty: type_ctx.get(["__lang", "macros", "Node"].toPath).toTypeSumRef),
  ]
  let new_node_stmt_while_impl = newFnImpl(new_node_stmt_while_name, none[string](), true, false, true, type_ctx.get(["__lang", "macros", "Node"].toPath).toTypeSumRef, new_node_stmt_while_params.mapIt(it), false, true, true)
  ctx.modules[["__lang", "macros"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: new_node_stmt_while_impl, comptime: builtin.new_node_stmt_while, params: @["span", "cond", "body"]))

proc init_macros_build_module*(ctx: var BuiltinModuleContext, type_ctx: TypeContext) =
  ctx.modules.add(["__lang", "macros", "build"].toPath, BuiltinModule(name: ["__lang", "macros", "build"].toPath, funcs: newSeq[BuiltinFunction]()))

  let toggle_flag_name = ["__lang", "macros", "build", "toggle_flag"].toPath
  let toggle_flag_params = @[
    Argument(name: some("flag"), ty: type_ctx.get(["__lang", "builtin", "str"].toPath).toTypeSumRef),
  ]
  let toggle_flag_impl = newFnImpl(toggle_flag_name, none[string](), true, false, true, type_ctx.get(["unit"].toPath).toTypeSumRef, toggle_flag_params, false, true, true)
  ctx.modules[["__lang", "macros", "build"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: toggle_flag_impl, comptime: builtin.toggle_flag, params: @["flag"]))

  let set_flag_name = ["__lang", "macros", "build", "set_flag"].toPath
  let set_flag_params = @[
    Argument(name: some("flag"), ty: type_ctx.get(["__lang", "builtin", "str"].toPath).toTypeSumRef),
    Argument(name: some("value"), ty: type_ctx.get("bool".toPath).toTypeSumRef),
  ]
  let set_flag_impl = newFnImpl(set_flag_name, none[string](), true, false, true, type_ctx.get(["unit"].toPath).toTypeSumRef, set_flag_params, false, true, true)
  ctx.modules[["__lang", "macros", "build"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: set_flag_impl, comptime: builtin.set_flag, params: @["flag", "value"]))

  let set_opt_name = ["__lang", "macros", "build", "set_opt"].toPath
  let set_opt_params = @[
    Argument(name: some("opt"), ty: type_ctx.get(["__lang", "builtin", "str"].toPath).toTypeSumRef),
    Argument(name: some("value"), ty: type_ctx.get(["__lang", "builtin", "str"].toPath).toTypeSumRef),
  ]
  let set_opt_impl = newFnImpl(set_opt_name, none[string](), true, false, true, type_ctx.get(["unit"].toPath).toTypeSumRef, set_opt_params, false, true, true)
  ctx.modules[["__lang", "macros", "build"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: set_opt_impl, comptime: builtin.set_opt, params: @["opt", "value"]))

  let unset_opt_name = ["__lang", "macros", "build", "unset_opt"].toPath
  let unset_opt_params = @[
    Argument(name: some("opt"), ty: type_ctx.get(["__lang", "builtin", "str"].toPath).toTypeSumRef),
  ]
  let unset_opt_impl = newFnImpl(unset_opt_name, none[string](), true, false, true, type_ctx.get(["unit"].toPath).toTypeSumRef, unset_opt_params, false, true, true)
  ctx.modules[["__lang", "macros", "build"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: unset_opt_impl, comptime: builtin.unset_opt, params: @["flag"]))

  let add_file_name = ["__lang", "macros", "build", "add_file"].toPath
  let add_file_params = @[
    Argument(name: some("file"), ty: type_ctx.get(["__lang", "builtin", "str"].toPath).toTypeSumRef),
  ]
  let add_file_impl = newFnImpl(add_file_name, none[string](), true, false, true, type_ctx.get(["unit"].toPath).toTypeSumRef, add_file_params, false, true, true)
  ctx.modules[["__lang", "macros", "build"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: add_file_impl, comptime: builtin.add_file, params: @["file"]))

  let add_path_name = ["__lang", "macros", "build", "add_path"].toPath
  let add_path_params = @[
    Argument(name: some("path"), ty: type_ctx.get(["__lang", "builtin", "str"].toPath).toTypeSumRef),
  ]
  let add_path_impl = newFnImpl(add_path_name, none[string](), true, false, true, type_ctx.get(["unit"].toPath).toTypeSumRef, add_path_params, false, true, true)
  ctx.modules[["__lang", "macros", "build"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: add_path_impl, comptime: builtin.add_path, params: @["path"]))

  let add_cpath_name = ["__lang", "macros", "build", "add_cpath"].toPath
  let add_cpath_params = @[
    Argument(name: some("path"), ty: type_ctx.get(["__lang", "builtin", "str"].toPath).toTypeSumRef),
  ]
  let add_cpath_impl = newFnImpl(add_cpath_name, none[string](), true, false, true, type_ctx.get(["unit"].toPath).toTypeSumRef, add_cpath_params, false, true, true)
  ctx.modules[["__lang", "macros", "build"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: add_cpath_impl, comptime: builtin.add_cpath, params: @["path"]))

  let add_lib_name = ["__lang", "macros", "build", "add_lib"].toPath
  let add_lib_params = @[
    Argument(name: some("lib"), ty: type_ctx.get(["__lang", "builtin", "str"].toPath).toTypeSumRef),
  ]
  let add_lib_impl = newFnImpl(add_lib_name, none[string](), true, false, true, type_ctx.get(["unit"].toPath).toTypeSumRef, add_lib_params, false, true, true)
  ctx.modules[["__lang", "macros", "build"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: add_lib_impl, comptime: builtin.add_lib, params: @["lib"]))

  let set_output_name = ["__lang", "macros", "build", "set_output"].toPath
  let set_output_params = @[
    Argument(name: some("output"), ty: type_ctx.get(["__lang", "builtin", "str"].toPath).toTypeSumRef),
  ]
  let set_output_impl = newFnImpl(set_output_name, none[string](), true, false, true, type_ctx.get(["unit"].toPath).toTypeSumRef, set_output_params, false, true, true)
  ctx.modules[["__lang", "macros", "build"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: set_output_impl, comptime: builtin.set_output, params: @["output"]))

  let toggle_linker_flag_name = ["__lang", "macros", "build", "toggle_linker_flag"].toPath
  let toggle_linker_flag_params = @[
    Argument(name: some("flag"), ty: type_ctx.get(["__lang", "builtin", "str"].toPath).toTypeSumRef),
  ]
  let toggle_linker_flag_impl = newFnImpl(toggle_linker_flag_name, none[string](), true, false, true, type_ctx.get(["unit"].toPath).toTypeSumRef, toggle_linker_flag_params, false, true, true)
  ctx.modules[["__lang", "macros", "build"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: toggle_linker_flag_impl, comptime: builtin.toggle_linker_flag, params: @["flag"]))

  let set_linker_flag_name = ["__lang", "macros", "build", "set_linker_flag"].toPath
  let set_linker_flag_params = @[
    Argument(name: some("flag"), ty: type_ctx.get(["__lang", "builtin", "str"].toPath).toTypeSumRef),
    Argument(name: some("value"), ty: type_ctx.get("bool".toPath).toTypeSumRef),
  ]
  let set_linker_flag_impl = newFnImpl(set_linker_flag_name, none[string](), true, false, true, type_ctx.get(["unit"].toPath).toTypeSumRef, set_linker_flag_params, false, true, true)
  ctx.modules[["__lang", "macros", "build"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: set_linker_flag_impl, comptime: builtin.set_linker_flag, params: @["flag", "value"]))

  let set_linker_opt_name = ["__lang", "macros", "build", "set_linker_opt"].toPath
  let set_linker_opt_params = @[
    Argument(name: some("opt"), ty: type_ctx.get(["__lang", "builtin", "str"].toPath).toTypeSumRef),
    Argument(name: some("value"), ty: type_ctx.get(["__lang", "builtin", "str"].toPath).toTypeSumRef),
  ]
  let set_linker_opt_impl = newFnImpl(set_linker_opt_name, none[string](), true, false, true, type_ctx.get(["unit"].toPath).toTypeSumRef, set_linker_opt_params, false, true, true)
  ctx.modules[["__lang", "macros", "build"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: set_linker_opt_impl, comptime: builtin.set_linker_opt, params: @["opt", "value"]))

  let unset_linker_opt_name = ["__lang", "macros", "build", "unset_linker_opt"].toPath
  let unset_linker_opt_params = @[
    Argument(name: some("opt"), ty: type_ctx.get(["__lang", "builtin", "str"].toPath).toTypeSumRef),
  ]
  let unset_linker_opt_impl = newFnImpl(unset_linker_opt_name, none[string](), true, false, true, type_ctx.get(["unit"].toPath).toTypeSumRef, unset_linker_opt_params, false, true, true)
  ctx.modules[["__lang", "macros", "build"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: unset_linker_opt_impl, comptime: builtin.unset_linker_opt, params: @["opt"]))

  let add_node_name = ["__lang", "macros", "build", "add_node"].toPath
  let add_node_params = @[
    Argument(name: some("file"), ty: type_ctx.get(["__lang", "builtin", "str"].toPath).toTypeSumRef),
    Argument(name: some("node"), ty: type_ctx.get(["__lang", "macros", "Node"].toPath).toTypeSumRef),
  ]
  let add_node_impl = newFnImpl(add_node_name, none[string](), true, false, true, type_ctx.get(["unit"].toPath).toTypeSumRef, add_node_params, false, true, true)
  ctx.modules[["__lang", "macros", "build"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: add_node_impl, comptime: builtin.add_node, params: @["file", "node"]))

  let get_env_name = ["__lang", "macros", "build", "get_env"].toPath
  let get_env_params = @[
    Argument(name: some("env"), ty: type_ctx.get(["__lang", "builtin", "str"].toPath).toTypeSumRef),
  ]
  let get_env_impl = newFnImpl(get_env_name, none[string](), true, false, true, type_ctx.get(["__lang", "builtin", "str"].toPath).toTypeSumRef, get_env_params, false, true, true)
  ctx.modules[["__lang", "macros", "build"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: get_env_impl, comptime: builtin.get_env, params: @["env"]))

  let set_env_name = ["__lang", "macros", "build", "set_env"].toPath
  let set_env_params = @[
    Argument(name: some("env"), ty: type_ctx.get(["__lang", "builtin", "str"].toPath).toTypeSumRef),
    Argument(name: some("value"), ty: type_ctx.get(["__lang", "builtin", "str"].toPath).toTypeSumRef),
  ]
  let set_env_impl = newFnImpl(set_env_name, none[string](), true, false, true, type_ctx.get(["unit"].toPath).toTypeSumRef, set_env_params, false, true, true)
  ctx.modules[["__lang", "macros", "build"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: set_env_impl, comptime: builtin.set_env, params: @["env", "value"]))

proc init_macros_pkg_module*(ctx: var BuiltinModuleContext, type_ctx: TypeContext) =
  ctx.modules.add(["__lang", "macros", "pkg"].toPath, BuiltinModule(name: ["__lang", "macros", "pkg"].toPath, funcs: newSeq[BuiltinFunction]()))

  let str_type = type_ctx.get(["__lang", "builtin", "str"].toPath).toTypeSumRef
  let str_slice = newType(Type(kind: tykSlice, slice: TypeSlice(elem: str_type))).toQualifiedType.toTypeSumRef

  let name_name = ["__lang", "macros", "pkg", "name"].toPath
  let name_params = @[Argument(name: some("name"), ty: str_type)]
  let name_impl = newFnImpl(name_name, none[string](), true, false, true, type_ctx.get(["unit"].toPath).toTypeSumRef, name_params, false, true, true)
  ctx.modules[["__lang", "macros", "pkg"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: name_impl, comptime: builtin.name, params: @["name"]))

  let version_name = ["__lang", "macros", "pkg", "version"].toPath
  let version_params = @[Argument(name: some("version"), ty: str_type)]
  let version_impl = newFnImpl(version_name, none[string](), true, false, true, type_ctx.get(["unit"].toPath).toTypeSumRef, version_params, false, true, true)
  ctx.modules[["__lang", "macros", "pkg"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: version_impl, comptime: builtin.version, params: @["version"]))

  let authors_name = ["__lang", "macros", "pkg", "authors"].toPath
  let authors_params = @[Argument(name: some("authors"), ty: str_slice)]
  let authors_impl = newFnImpl(authors_name, none[string](), true, false, true, type_ctx.get(["unit"].toPath).toTypeSumRef, authors_params, false, true, true)
  ctx.modules[["__lang", "macros", "pkg"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: authors_impl, comptime: builtin.authors, params: @["authors"]))

  let description_name = ["__lang", "macros", "pkg", "description"].toPath
  let description_params = @[Argument(name: some("description"), ty: str_type)]
  let description_impl = newFnImpl(description_name, none[string](), true, false, true, type_ctx.get(["unit"].toPath).toTypeSumRef, description_params, false, true, true)
  ctx.modules[["__lang", "macros", "pkg"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: description_impl, comptime: builtin.description, params: @["description"]))

  let kind_name = ["__lang", "macros", "pkg", "kind"].toPath
  let kind_params = @[Argument(name: some("kind"), ty: str_type)]
  let kind_impl = newFnImpl(kind_name, none[string](), true, false, true, type_ctx.get(["unit"].toPath).toTypeSumRef, kind_params, false, true, true)
  ctx.modules[["__lang", "macros", "pkg"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: kind_impl, comptime: builtin.kind, params: @["kind"]))

  let repository_name = ["__lang", "macros", "pkg", "repository"].toPath
  let repository_params = @[Argument(name: some("repository"), ty: str_type)]
  let repository_impl = newFnImpl(repository_name, none[string](), true, false, true, type_ctx.get(["unit"].toPath).toTypeSumRef, repository_params, false, true, true)
  ctx.modules[["__lang", "macros", "pkg"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: repository_impl, comptime: builtin.repository, params: @["repository"]))

  let dependencies_name = ["__lang", "macros", "pkg", "dependencies"].toPath
  let dependencies_params = @[Argument(name: some("dependencies"), ty: str_slice)]
  let dependencies_impl = newFnImpl(dependencies_name, none[string](), true, false, true, type_ctx.get(["unit"].toPath).toTypeSumRef, dependencies_params, false, true, true)
  ctx.modules[["__lang", "macros", "pkg"].toPath].funcs.add(BuiltinFunction(kind: bfkComptime, impl: dependencies_impl, comptime: builtin.dependencies, params: @["dependencies"]))
