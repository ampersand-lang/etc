import macros
import options
import strformat
import os
import span

type
  ErrorKind* = enum
    errkIceNoVariable
    errkUnknown
    errkFail
    errkNoInput
    errkFileNotFound
    errkUnimplemented
    errkUnexpectedCharacter
    errkUnexpectedToken
    errkNotAType
    errkUnspecifiedType
    errkAmbigousType
    errkInvalidOperation
    errkInvalidAttribute
    errkInvalidType
    errkFreestandingTypeLiteral
    errkNoImpl
    errkAmbigousImpl
    errkNoLValue
    errkLValue
    errkUnexpectedType
    errkInvalidMatch
    errkMalformedHlir
    errkNotAField
    errkNotAVariant
    errkInvalidCharacter
    errkUnrecognizedLanguage
    errkCError
    errkUnknownSize
    errkUnsizedSize
    errkUnknownAlign
    errkUnsizedAlign
    errkNotConst
    errkInvalidCapture
    errkUnrecognizedCommand
    errkNoReturn
    errkMaybeReturn
  Error* = object of Exception
    kind: ErrorKind
    span: Span
    stack_trace: string
  Errors* = object of Exception
    errors: seq[ref Error]

proc newError*(kind: ErrorKind, span: Span, msg: string): ref Error =
  var e: ref Error
  new e
  e[] = Error(kind: kind, msg: msg, span: span, stack_trace: getStackTrace())
  return e

proc newError*(kind: ErrorKind, span: Span): ref Error =
  var e: ref Error
  new e
  e[] = Error(kind: kind, msg: "", span: span, stack_trace: getStackTrace())
  return e

proc kind*(error: ref Error): ErrorKind = error.kind
proc kind*(error: Error): ErrorKind = error.kind

proc `$`*(error: Error): string =
  var kind: string
  case error.kind
  of errkIceNoVariable: kind = "ICE: variable, constant or function not found"
  of errkUnknown: kind = "unknown error"
  of errkFail: kind = "failure"
  of errkNoInput: kind = "no input"
  of errkFileNotFound: kind = "file not found"
  of errkUnimplemented: kind = "this feature is unimplemented"
  of errkUnexpectedCharacter: kind = "unexpected character"
  of errkUnexpectedToken: kind = "unexpected token"
  of errkNotAType: kind = "this node is not a type"
  of errkUnspecifiedType: kind = "type is unspecified"
  of errkAmbigousType: kind = "type is ambigous"
  of errkInvalidOperation: kind = "cannot perform operation on these expressions"
  of errkInvalidAttribute: kind = "invalid attribute"
  of errkInvalidType: kind = "invalid type"
  of errkFreestandingTypeLiteral: kind = "free-standing type literal"
  of errkNoImpl: kind = "no implementation exists for operator, function, variable or type"
  of errkAmbigousImpl: kind = "implementation of operator, function or variable is ambigous"
  of errkNoLValue: kind = "cannot store inside a value without an l-val"
  of errkLValue: kind = "value is an l-val and cannot be used as an r-val in this context"
  of errkUnexpectedType: kind = "unexpected type"
  of errkInvalidMatch: kind = "invalid match value"
  of errkMalformedHlir: kind = "malformed hlir"
  of errkNotAField: kind = "type doesn't have this field"
  of errkNotAVariant: kind = "type doesn't have this variant"
  of errkInvalidCharacter: kind = "a character must have a length of 1"
  of errkUnrecognizedLanguage: kind = "only \"C\" is allowed as an extern language"
  of errkCError: kind = "an error occured in a C header, messages were printed"
  of errkUnknownSize: kind = "type has an unknown size"
  of errkUnsizedSize: kind = "type is unsized"
  of errkUnknownAlign: kind = "type has an unknown alignment"
  of errkUnsizedAlign: kind = "type is unsized"
  of errkNotConst: kind = "value must be known at compile-time in this context"
  of errkInvalidCapture: kind = "a capture may only be a named or a reference to a named"
  of errkUnrecognizedCommand: kind = "unrecognized command"
  of errkNoReturn: kind = "function doesn't return"
  of errkMaybeReturn: kind = "function doesn't return in every branch"
  let
    print_stack_trace_str = getEnv("ETC_BACKTRACE", default = "n")
    print_stack_trace = print_stack_trace_str in ["y", "Y", "yes", "Yes", "YES", "1"]
  var stack_trace = ""
  if print_stack_trace:
    stack_trace = fmt"""

stack trace:
{error.stack_trace}"""
  if error.msg.len == 0:
    return fmt"""
error({error.kind}): {kind}: {$error.span} {stack_trace}"""
  else:
    return fmt"""
error({error.kind}): {kind}: {$error.span}: {error.msg} {stack_trace}"""

proc newErrors*(): ref Errors =
  new result
  result.errors = newSeq[ref Error]()

proc add*(errors: ref Errors, error: ref Error) =
  errors.errors.add(error)

proc add*(errors: ref Errors, error: ref Errors) =
  errors.errors.add(error.errors)

proc len*(errors: ref Errors): int = errors.errors.len

proc `$`*(errors: Errors): string =
  for error in errors.errors:
    echo error[]
