import strformat

type
  Ice = object of Exception

proc newIce*(msg: string): ref Ice =
  return newException(
    Ice,
    fmt"""


internal compiler error: {msg}
  this is a bug in the compiler, please report it along with the stack trace above

""",
  )
