import options
import tables
import sets
import strformat
import sequtils
import hashes
import ../ice
import ../error
import ../span
import ../target
import ../path
import ../bits
import ../llvm/llvm

type
  TypeContext* = ref object
    target*: ref Target
    types*: Table[Path, ref QualifiedType]
    impls*: seq[ImplScope]
    pref_list*: seq[ref QualifiedType]

  ImplScopeKind = enum
    igkScope
    igkBoundary
  ImplScope* = object
    case kind: ImplScopeKind
    of igkScope:
      impls: Table[Path, OrderedSet[ref Impl]]
    of igkBoundary: nil

  ImplKind* = enum
    ikVar
    ikFn
    ikCons
  Impl* = object
    poly_impl*: Option[ref Impl]
    name*: Path
    linkage_name*: Option[string]
    builtin*: bool
    shadow*: bool
    global*: bool
    polymorphic*: bool
    case kind: ImplKind
    of ikVar, ikCons:
      ty: ref TypeSum
    of ikFn:
      result_type: ref TypeSum
      params: seq[Argument]
      variadic*: bool
      ampvar: Option[ref TypeSum]
      nohidden*: bool
      is_macro*: bool
  Instance* = object
    impls*: OrderedSet[ref Impl]
    scores*: seq[int]
    type_args*: Option[seq[tuple[name: Option[string], ty: ref QualifiedType]]]
    found_impls*: seq[ref Impl]

  Argument* = object
    name*: Option[string]
    ty*: ref TypeSum

  ParamPtrKind* = enum
    ppkDone
    ppkTraitFn
    ppkStructField
    ppkUnionField
    ppkTaggedField
    ppkTaggedVField
    ppkFunctionResult
    ppkFunctionParam
    ppkPointee
    ppkSliceElem
    ppkArrayElem
    ppkArraySize
    ppkVarElem
  ParamPtr* = object
    case kind*: ParamPtrKind
    of ppkDone: nil
    of ppkFunctionParam:
      param_idx*: int
    of ppkTraitFn:
      fn_idx*: int
    of ppkStructField, ppkUnionField, ppkTaggedField:
      field_idx*: int
    of ppkTaggedVField:
      vfield_idx*: int
      variant_idx*: int
    of ppkFunctionResult: nil
    of ppkPointee: nil
    of ppkSliceElem: nil
    of ppkArrayElem: nil
    of ppkArraySize: nil
    of ppkVarElem: nil

  SizeofKind* = enum
    sokUnknown
    sokUnsized
    sokSized
  Sizeof* = object
    case kind*: SizeofKind
    of sokUnknown: discard
    of sokUnsized: discard
    of sokSized: size*: uint
  Alignof* = Sizeof
  TypeSum* = object
    sum*: HashSet[ref QualifiedType]
  QualifiedType* = object
    inner*: ref Type
    mangled_name*: Option[string]
    param_ptrs*: Table[string, seq[ParamPtr]]
    args*: OrderedTable[string, ref QualifiedType]
    hash*: Option[Hash]
    size*: Option[Sizeof]
    align*: Option[Alignof]
    is_eval*: bool
    is_hidden*: bool
    is_parameter*: bool
    is_return*: bool
    meta*: Option[llvm.MetadataRef]
  TypeKind* = enum
    tykUnit
    tykNever
    tykBool
    tykSigned
    tykUnsigned
    tykFloating
    tykTrait
    tykStruct
    tykUnion
    tykEnum
    tykTagged
    tykFunction
    tykPointer
    tykSlice
    tykArray
    tykVector
    tykVar
    tykParameter
    tykLit
    tykType
  Type* = object
    case kind*: TypeKind
    of tykUnit: unit*: TypeUnit
    of tykNever: never*: TypeNever
    of tykBool: p*: TypeBool
    of tykSigned: signed*: TypeSigned
    of tykUnsigned: unsigned*: TypeUnsigned
    of tykFloating: floating*: TypeFloating
    of tykTrait: trait*: TypeTrait
    of tykStruct: struct*: TypeStruct
    of tykUnion: union*: TypeStruct
    of tykEnum: enumeration*: TypeEnum
    of tykTagged: tagged*: TypeTagged
    of tykFunction: function*: TypeFunction
    of tykPointer: point*: TypePointer
    of tykSlice: slice*: TypeSlice
    of tykArray: arr*: TypeArray
    of tykVector: vec*: TypeVector
    of tykVar: vararr*: TypeVar
    of tykParameter: param*: TypeParameter
    of tykLit: lit*: TypeLiteral
    of tykType: ty*: TypeType
  TypeUnit* = object
  TypeNever* = object
  TypeBool* = object
  TypeSigned* = object
    bits*: Bits
  TypeUnsigned* = object
    bits*: Bits
  TypeFloating* = object
    bits*: Bits
  TypeTrait* = object
    name*: Path
    fns*: seq[StructField]
  TypeStruct* = object
    name*: Option[Path]
    fields*: seq[StructField]
  TypeEnum* = object
    name*: Path
    inner*: ref QualifiedType
  TypeTagged* = object
    name*: Path
    fields*: seq[StructField]
    variants*: seq[TaggedVariant]
  StructField* = object
    name*: Option[string]
    ty*: ref TypeSum
  TaggedVariant* = object
    name*: Path
    fields*: seq[StructField]
  TypeFunction* = object
    result_type*: ref TypeSum
    params*: seq[Argument]
    variadic*: bool
  TypePointer* = object
    pointee*: ref TypeSum
  TypeSlice* = object
    elem*: ref TypeSum
  TypeArray* = object
    elem*: ref TypeSum
    size*: ref TypeSum
  TypeVector* = object
    elem*: ref TypeSum
    size*: ref TypeSum
  TypeVar* = object
    elem*: ref TypeSum
  ParameterKind* = enum
    pkType
    pkInt
    pkFloat
    pkString
  TypeParameter* = object
    name*: string
    case kind*: ParameterKind
    of pkType: discard
    of pkInt, pkFloat, pkString:
      ty*: ref TypeSum
  LiteralKind* = enum
    lkInt
    lkFloat
    lkString
  TypeLiteral* = object
    ty*: ref TypeSum
    case kind*: LiteralKind
    of lkInt:
      intval*: int
    of lkFloat:
      floatval*: float
    of lkString:
      strval*: string
  TypeType* = object

proc get*(ctx: TypeContext, name: Path): ref QualifiedType
proc newVarImpl*(name: Path, linkage_name: Option[string], builtin: bool, shadow: bool, global: bool, ty: ref TypeSum): ref Impl
proc newFnImpl*(name: Path, linkage_name: Option[string], builtin: bool, shadow: bool, global: bool, result_type: ref TypeSum, params: seq[Argument], variadic: bool, nohidden: bool, is_macro: bool): ref Impl
proc newConsImpl*(name: Path, linkage_name: Option[string], builtin: bool, shadow: bool, global: bool, ty: ref TypeSum): ref Impl
proc newType*(ty: Type): ref Type
proc toQualifiedType*(ty: ref Type): ref QualifiedType
proc toTypeSum*(ty: ref QualifiedType): TypeSum
proc toTypeSumRef*(ty: ref QualifiedType): ref TypeSum
proc add_impl*(ctx: TypeContext, name: Path, impl: ref Impl)
proc as_qualified*(sum: TypeSum): ref QualifiedType
proc as_qualified_or_uninit*(sum: TypeSum): ref QualifiedType {.noSideEffect.}
proc is_qualified*(sum: TypeSum): bool {.inline.}
proc elem*(ty: ref Type): ref TypeSum
proc elem*(ty: ref QualifiedType): ref TypeSum
proc elem*(sum: TypeSum): ref TypeSum
proc pointee*(ty: ref Type): ref TypeSum
proc pointee*(ty: ref QualifiedType): ref TypeSum
proc replace_param*(ty: ref QualifiedType, param: string, arg: ref QualifiedType)
proc has_type_params*(ty: ref QualifiedType): bool {.inline.}
proc has_type_args*(ty: QualifiedType): bool {.inline.}
proc has_type_args*(sum: TypeSum): bool
proc is_monomorphized*(ty: ref QualifiedType): bool
proc type_args*(ty: ref QualifiedType): seq[ref QualifiedType]
proc type_args*(ty: QualifiedType): seq[ref QualifiedType]
proc type_args_named*(ty: ref QualifiedType): seq[(string, ref QualifiedType)]
proc type_args_named*(ty: QualifiedType): seq[(string, ref QualifiedType)]
proc enter*(ctx: var TypeContext)
proc leave*(ctx: var TypeContext)
proc kind*(impl: ref Impl): ImplKind
proc ty*(impl: ref Impl): ref TypeSum
proc result_type*(impl: ref Impl): ref TypeSum

proc is_signed(ty: Type): bool
proc is_unsigned(ty: Type): bool
proc is_floating(ty: Type): bool
proc is_var*(ty: Type): bool
proc is_var*(ty: QualifiedType): bool

proc is_type(ty: Type): bool
proc is_int(ty: Type): bool
proc is_float(ty: Type): bool
proc is_str(ty: Type): bool
proc is_parameter(ty: Type): bool
proc is_parameter*(ty: QualifiedType): bool
proc is_parameter*(ty: TypeSum): bool

proc hash*(ty: ref QualifiedType): Hash
proc hash*(sum: TypeSum): Hash
proc hash*(ty: ref QualifiedType, recurse: var HashSet[uint]): Hash
proc hash*(sum: TypeSum, recurse: var HashSet[uint]): Hash

proc `==`*(lhs: ref Impl, rhs: ref Impl): bool

proc eq*(lhs: ref QualifiedType, rhs: ref QualifiedType, recurse: var HashSet[(uint, uint)]): bool
proc eq*(lhs: ref TypeSum, rhs: ref TypeSum, recurse: var HashSet[(uint, uint)]): bool

proc `==`*(lhs: TypeParameter, rhs: TypeParameter): bool
proc `==`*(lhs: TypeLiteral, rhs: TypeLiteral): bool
proc `==`*(lhs: ref QualifiedType, rhs: ref QualifiedType): bool
proc `==`*(lhs: TypeSum, rhs: TypeSum): bool
proc `==`*(lhs: ref TypeSum, rhs: ref TypeSum): bool

proc name*(ty: QualifiedType): Path
proc `$`*(impl: Impl): string
proc `$`*(ty: QualifiedType): string
proc `$`*(sum: TypeSum): string

proc hash*(impl: ref Impl): Hash =
  var h: Hash = 0
  h = h !& hash(ty(impl)[])
  h = h !& hash(impl.name)
  result = !$h

proc hash*(impl: Option[ref Impl]): Hash =
  var h: Hash = 0
  h = h !& hash(impl.isSome)
  if impl.isSome:
    h = h !& hash(impl.get)
  result = !$h

proc hash*(param: TypeParameter): Hash =
  var h: Hash = 0
  h = h !& hash(param.name)
  result = !$h

proc hash*(lit: TypeLiteral): Hash =
  var h: Hash = 0
  h = h !& hash(lit.ty[])
  h = h !& hash(lit.kind.int)
  case lit.kind
  of lkInt:
    h = h !& hash(lit.intval)
  of lkFloat:
    h = h !& hash(lit.floatval)
  of lkString:
    h = h !& hash(lit.strval)
  result = !$h

proc hash*(ty: ref QualifiedType, recurse: var HashSet[uint]): Hash =
  # FIXME
  # disabled hash caching until it is fixed
  #if ty.hash.isNone:
  if recurse.contains(cast[uint](ty)):
    return 0
  else:
    recurse.incl(cast[uint](ty))
  var h: Hash = 0
  h = h !& ty.inner.kind.int
  case ty.inner.kind
  of tykUnit: discard
  of tykNever: discard
  of tykBool: discard
  of tykSigned: h = h !& hash(ty.inner.signed.bits)
  of tykUnsigned: h = h !& hash(ty.inner.unsigned.bits)
  of tykFloating: h = h !& hash(ty.inner.floating.bits)
  of tykTrait:
    h = h !& hash(ty.inner.trait.name)
    for _, field in ty.args:
      h = h !& hash(field, recurse)
  of tykStruct:
    if ty.inner.struct.name.isSome:
      h = h !& hash(ty.inner.struct.name.get)
    for _, field in ty.args:
      h = h !& hash(field, recurse)
  of tykUnion:
    if ty.inner.union.name.isSome:
      h = h !& hash(ty.inner.union.name.get)
    for _, field in ty.args:
      h = h !& hash(field, recurse)
  of tykEnum:
    h = h !& hash(ty.inner.enumeration.name)
    h = h !& hash(ty.inner.enumeration.inner, recurse)
  of tykTagged:
    h = h !& hash(ty.inner.tagged.name)
    for _, field in ty.args:
      h = h !& hash(field, recurse)
  of tykFunction:
    h = h !& hash(ty.inner.function.result_type[], recurse)
    for param in ty.inner.function.params:
      if param.name.isSome:
        h = h !& hash(param.name.get)
      h = h !& hash(param.ty[], recurse)
  of tykPointer:
    h = h !& hash(ty.inner.point.pointee[], recurse)
  of tykSlice:
    h = h !& hash(ty.inner.slice.elem[], recurse)
  of tykArray:
    h = h !& hash(ty.inner.arr.elem[], recurse)
  of tykVector:
    h = h !& hash(ty.inner.vec.elem[], recurse)
    h = h !& hash(ty.inner.vec.size[], recurse)
  of tykVar:
    h = h !& hash(ty.inner.vararr.elem[], recurse)
  of tykParameter:
    h = h !& hash(ty.inner.param)
  of tykLit:
    h = h !& hash(ty.inner.lit)
  of tykType:
    discard

  ty.hash = some(!$h)
  result = ty.hash.get

proc hash*(ty: ref QualifiedType): Hash =
  var recurse = initHashSet[uint]()
  result = hash(ty, recurse)

proc hash*(sum: TypeSum, recurse: var HashSet[uint]): Hash =
  if sum.is_qualified:
    return hash(sum.as_qualified, recurse)
  var h: Hash = 0
  for t in sum.sum:
    h = h !& hash(t, recurse)
  result = !$h

proc hash*(sum: TypeSum): Hash =
  var recurse = initHashSet[uint]()
  result = hash(sum, recurse)

proc impl_unary(ctx: TypeContext, name: string, result_type: ref QualifiedType, ty: ref QualifiedType) =
  var impl: ref Impl
  new impl
  impl[] = Impl(
    name: ["__lang", "builtin", name].toPath,
    linkage_name: none[string](),
    builtin: true,
    shadow: false,
    global: true,
    kind: ikFn,
    result_type: result_type.toTypeSumRef,
    params: @[Argument(name: some("value"), ty: ty.toTypeSumRef)],
    variadic: false,
    nohidden: true,
  )
  ctx.add_impl(["__lang", "builtin", name].toPath, impl)

proc impl_binary(ctx: TypeContext, name: string, result_type: ref QualifiedType, lhs: ref QualifiedType, rhs: ref QualifiedType) =
  var impl: ref Impl
  new impl
  impl[] = Impl(
    name: ["__lang", "builtin", name].toPath,
    linkage_name: none[string](),
    builtin: true,
    shadow: false,
    global: true,
    kind: ikFn,
    result_type: result_type.toTypeSumRef,
    params: @[Argument(name: some("lhs"), ty: lhs.toTypeSumRef), Argument(name: some("rhs"), ty: rhs.toTypeSumRef)],
    variadic: false,
    nohidden: true,
  )
  ctx.add_impl(["__lang", "builtin", name].toPath, impl)

proc impl_generic(ctx: TypeContext, ty: ref QualifiedType) =
  let ptr_ty = newType(Type(kind: tykPointer, point: TypePointer(pointee: ty.toTypeSumRef))).toQualifiedType
  ctx.impl_binary("__assign__", ty, ptr_ty, ty)

proc impl_bool(ctx: TypeContext, ty: ref QualifiedType) =
  let ptr_ty = newType(Type(kind: tykPointer, point: TypePointer(pointee: ty.toTypeSumRef))).toQualifiedType

  ctx.impl_unary("__ask__", ty, ty)
  ctx.impl_unary("__not__", ty, ty)
  ctx.impl_binary("__bitand__", ty, ty, ty)
  ctx.impl_binary("__bitor__", ty, ty, ty)
  ctx.impl_binary("__bitxor__", ty, ty, ty)
  ctx.impl_binary("__eq__", ty, ty, ty)
  ctx.impl_binary("__ne__", ty, ty, ty)

  ctx.impl_binary("__assign_and__", ty, ptr_ty, ty)
  ctx.impl_binary("__assign_or__", ty, ptr_ty, ty)
  ctx.impl_binary("__assign_xor__", ty, ptr_ty, ty)

proc impl_intlike(ctx: TypeContext, ty: ref QualifiedType) =
  let ptr_ty = newType(Type(kind: tykPointer, point: TypePointer(pointee: ty.toTypeSumRef))).toQualifiedType

  let
    t_bool = ctx.types["bool".toPath]
    t_range_qty = ctx.types[["__lang", "builtin", "Range"].toPath].deepCopy
    t_range_to_qty = ctx.types[["__lang", "builtin", "RangeTo"].toPath].deepCopy
    t_range_from_qty = ctx.types[["__lang", "builtin", "RangeFrom"].toPath].deepCopy
  t_range_qty.replace_param("T", ty)
  t_range_to_qty.replace_param("T", ty)
  t_range_from_qty.replace_param("T", ty)
  let
    t_range_ty = t_range_qty
    t_range_to_ty = t_range_to_qty
    t_range_from_ty = t_range_from_qty

  ctx.impl_unary("__pos__", ty, ty)
  ctx.impl_unary("__neg__", ty, ty)
  ctx.impl_unary("__not__", ty, ty)
  ctx.impl_unary("__inc__", ty, ty)
  ctx.impl_unary("__dec__", ty, ty)
  ctx.impl_unary("__range_to__", t_range_to_ty, ty)
  ctx.impl_unary("__range_from__", t_range_from_ty, ty)
  ctx.impl_binary("__add__", ty, ty, ty)
  ctx.impl_binary("__sub__", ty, ty, ty)
  ctx.impl_binary("__mul__", ty, ty, ty)
  ctx.impl_binary("__div__", ty, ty, ty)
  ctx.impl_binary("__rem__", ty, ty, ty)
  ctx.impl_binary("__bitand__", ty, ty, ty)
  ctx.impl_binary("__bitor__", ty, ty, ty)
  ctx.impl_binary("__bitxor__", ty, ty, ty)
  ctx.impl_binary("__shl__", ty, ty, ty)
  ctx.impl_binary("__shr__", ty, ty, ty)
  ctx.impl_binary("__eq__", t_bool, ty, ty)
  ctx.impl_binary("__ne__", t_bool, ty, ty)
  ctx.impl_binary("__lt__", t_bool, ty, ty)
  ctx.impl_binary("__gt__", t_bool, ty, ty)
  ctx.impl_binary("__le__", t_bool, ty, ty)
  ctx.impl_binary("__ge__", t_bool, ty, ty)
  ctx.impl_binary("__range__", t_range_ty, ty, ty)

  ctx.impl_binary("__assign_add__", ty, ptr_ty, ty)
  ctx.impl_binary("__assign_sub__", ty, ptr_ty, ty)
  ctx.impl_binary("__assign_mul__", ty, ptr_ty, ty)
  ctx.impl_binary("__assign_div__", ty, ptr_ty, ty)
  ctx.impl_binary("__assign_rem__", ty, ptr_ty, ty)
  ctx.impl_binary("__assign_and__", ty, ptr_ty, ty)
  ctx.impl_binary("__assign_or__", ty, ptr_ty, ty)
  ctx.impl_binary("__assign_xor__", ty, ptr_ty, ty)
  ctx.impl_binary("__assign_shl__", ty, ptr_ty, ty)
  ctx.impl_binary("__assign_shr__", ty, ptr_ty, ty)

proc impl_floatlike(ctx: TypeContext, ty: ref QualifiedType) =
  let ptr_ty = newType(Type(kind: tykPointer, point: TypePointer(pointee: ty.toTypeSumRef))).toQualifiedType

  let t_bool = ctx.types["bool".toPath]

  ctx.impl_unary("__pos__", ty, ty)
  ctx.impl_unary("__neg__", ty, ty)
  ctx.impl_unary("__inc__", ty, ty)
  ctx.impl_unary("__dec__", ty, ty)
  ctx.impl_binary("__add__", ty, ty, ty)
  ctx.impl_binary("__sub__", ty, ty, ty)
  ctx.impl_binary("__mul__", ty, ty, ty)
  ctx.impl_binary("__div__", ty, ty, ty)
  ctx.impl_binary("__rem__", ty, ty, ty)
  ctx.impl_binary("__eq__", t_bool, ty, ty)
  ctx.impl_binary("__ne__", t_bool, ty, ty)
  ctx.impl_binary("__lt__", t_bool, ty, ty)
  ctx.impl_binary("__gt__", t_bool, ty, ty)
  ctx.impl_binary("__le__", t_bool, ty, ty)
  ctx.impl_binary("__ge__", t_bool, ty, ty)

  ctx.impl_binary("__assign_add__", ty, ptr_ty, ty)
  ctx.impl_binary("__assign_sub__", ty, ptr_ty, ty)
  ctx.impl_binary("__assign_mul__", ty, ptr_ty, ty)
  ctx.impl_binary("__assign_div__", ty, ptr_ty, ty)
  ctx.impl_binary("__assign_rem__", ty, ptr_ty, ty)

proc impl_ptr(ctx: TypeContext, ty: ref QualifiedType) =
  let ptr_ty = newType(Type(kind: tykPointer, point: TypePointer(pointee: ty.toTypeSumRef))).toQualifiedType

  let t_bool = ctx.types["bool".toPath]
  let t_uint = ctx.types["uint".toPath]
  let t_sint = ctx.types["sint".toPath]

  let pointee = ty.pointee[].as_qualified

  ctx.impl_unary("__inc__", ty, ty)
  ctx.impl_unary("__dec__", ty, ty)
  ctx.impl_binary("__add__", ty, ty, t_uint)
  ctx.impl_binary("__sub__", ty, ty, t_uint)
  ctx.impl_binary("__sub__", t_sint, ty, ty)
  ctx.impl_binary("__add__", ty, ty, t_sint)
  ctx.impl_binary("__sub__", ty, ty, t_sint)
  ctx.impl_binary("__eq__", t_bool, ty, ty)
  ctx.impl_binary("__ne__", t_bool, ty, ty)

  ctx.impl_binary("__assign_add__", ty, ptr_ty, t_uint)
  ctx.impl_binary("__assign_add__", ty, ptr_ty, t_sint)
  ctx.impl_binary("__assign_sub__", ty, ptr_ty, t_uint)
  ctx.impl_binary("__assign_sub__", ty, ptr_ty, t_sint)

  ctx.impl_binary("__index__", pointee, ty, t_uint)

proc impl_arraylike(ctx: TypeContext, ty: ref QualifiedType) =
  let
    t_uint = ctx.types["uint".toPath]
    t_range_qty = ctx.types[["__lang", "builtin", "Range"].toPath].deepCopy
    t_range_to_qty = ctx.types[["__lang", "builtin", "RangeTo"].toPath].deepCopy
    t_range_from_qty = ctx.types[["__lang", "builtin", "RangeFrom"].toPath].deepCopy
    t_range_full = ctx.types[["__lang", "builtin", "RangeFull"].toPath].deepCopy
  t_range_qty.replace_param("T", t_uint)
  t_range_to_qty.replace_param("T", t_uint)
  t_range_from_qty.replace_param("T", t_uint)
  let
    t_range_uint = t_range_qty
    t_range_to_uint = t_range_to_qty
    t_range_from_uint = t_range_from_qty
    elem = ty.elem[].as_qualified
    slice_elem = newType(Type(kind: tykSlice, slice: TypeSlice(elem: ty.elem))).toQualifiedType

  ctx.impl_binary("__index__", slice_elem, ty, t_range_uint)
  ctx.impl_binary("__index__", slice_elem, ty, t_range_to_uint)
  ctx.impl_binary("__index__", slice_elem, ty, t_range_from_uint)
  ctx.impl_binary("__index__", slice_elem, ty, t_range_full)
  ctx.impl_binary("__index__", elem, ty, t_uint)

proc newTypeContext*(target: ref Target): TypeContext =
  new result
  result.target = target
  result.types = initTable[Path, ref QualifiedType]()
  result.impls = newSeq[ImplScope]()
  result.enter
  result.pref_list = newSeq[ref QualifiedType]()

  result.types.add("never".toPath, newType(Type(kind: tykNever, never: TypeNever())).toQualifiedType)
  result.types.add("unit".toPath, newType(Type(kind: tykUnit, unit: TypeUnit())).toQualifiedType)
  result.types.add("bool".toPath, newType(Type(kind: tykBool, p: TypeBool())).toQualifiedType)
  result.types.add("s8".toPath, newType(Type(kind: tykSigned, signed: TypeSigned(bits: 8.toBits))).toQualifiedType)
  result.types.add("s16".toPath, newType(Type(kind: tykSigned, signed: TypeSigned(bits: 16.toBits))).toQualifiedType)
  result.types.add("s32".toPath, newType(Type(kind: tykSigned, signed: TypeSigned(bits: 32.toBits))).toQualifiedType)
  result.types.add("s64".toPath, newType(Type(kind: tykSigned, signed: TypeSigned(bits: 64.toBits))).toQualifiedType)
  result.types.add("s128".toPath, newType(Type(kind: tykSigned, signed: TypeSigned(bits: 128.toBits))).toQualifiedType)
  result.types.add("sint".toPath, newType(Type(kind: tykSigned, signed: TypeSigned(bits: Bits(kind: bitsWord)))).toQualifiedType)
  result.types.add("u8".toPath, newType(Type(kind: tykUnsigned, unsigned: TypeUnsigned(bits: 8.toBits))).toQualifiedType)
  result.types.add("u16".toPath, newType(Type(kind: tykUnsigned, unsigned: TypeUnsigned(bits: 16.toBits))).toQualifiedType)
  result.types.add("u32".toPath, newType(Type(kind: tykUnsigned, unsigned: TypeUnsigned(bits: 32.toBits))).toQualifiedType)
  result.types.add("u64".toPath, newType(Type(kind: tykUnsigned, unsigned: TypeUnsigned(bits: 64.toBits))).toQualifiedType)
  result.types.add("u128".toPath, newType(Type(kind: tykUnsigned, unsigned: TypeUnsigned(bits: 128.toBits))).toQualifiedType)
  result.types.add("uint".toPath, newType(Type(kind: tykUnsigned, unsigned: TypeUnsigned(bits: Bits(kind: bitsWord)))).toQualifiedType)
  result.types.add("float16".toPath, newType(Type(kind: tykFloating, floating: TypeFloating(bits: 16.toBits))).toQualifiedType)
  result.types.add("float32".toPath, newType(Type(kind: tykFloating, floating: TypeFloating(bits: 32.toBits))).toQualifiedType)
  result.types.add("float64".toPath, newType(Type(kind: tykFloating, floating: TypeFloating(bits: 64.toBits))).toQualifiedType)
  result.types.add("float80".toPath, newType(Type(kind: tykFloating, floating: TypeFloating(bits: 80.toBits))).toQualifiedType)
  result.types.add("float128".toPath, newType(Type(kind: tykFloating, floating: TypeFloating(bits: 128.toBits))).toQualifiedType)
  result.types.add("float".toPath, newType(Type(kind: tykFloating, floating: TypeFloating(bits: Bits(kind: bitsWord)))).toQualifiedType)

  result.pref_list.add(result.types["sint".toPath])
  result.pref_list.add(result.types["s32".toPath])
  result.pref_list.add(result.types["s64".toPath])
  result.pref_list.add(result.types["s128".toPath])
  result.pref_list.add(result.types["s16".toPath])
  result.pref_list.add(result.types["s8".toPath])
  result.pref_list.add(result.types["uint".toPath])
  result.pref_list.add(result.types["u32".toPath])
  result.pref_list.add(result.types["u64".toPath])
  result.pref_list.add(result.types["u128".toPath])
  result.pref_list.add(result.types["u16".toPath])
  result.pref_list.add(result.types["u8".toPath])
  result.pref_list.add(result.types["float32".toPath])
  result.pref_list.add(result.types["float".toPath])
  result.pref_list.add(result.types["float64".toPath])
  result.pref_list.add(result.types["float128".toPath])
  result.pref_list.add(result.types["float80".toPath])
  result.pref_list.add(result.types["float16".toPath])

  let unsigned_sum = result.types["uint".toPath].toTypeSumRef

  let generic_elem = newType(Type(kind: tykParameter, param: TypeParameter(kind: pkType, name: "T"))).toQualifiedType.toTypeSumRef
  let generic_size = newType(Type(kind: tykParameter, param: TypeParameter(kind: pkInt, name: "N", ty: unsigned_sum))).toQualifiedType.toTypeSumRef

  # #[lang(pointer)]
  # struct ::__lang::builtin::pointer<T type> {}
  result.types.add(["__lang", "builtin", "pointer"].toPath, newType(Type(kind: tykPointer, point: TypePointer(pointee: generic_elem))).toQualifiedType)

  # #[lang(slice)]
  # struct ::__lang::builtin::slice<T type> {}
  result.types.add(["__lang", "builtin", "slice"].toPath, newType(Type(kind: tykSlice, slice: TypeSlice(elem: generic_elem))).toQualifiedType)

  # #[lang(array)]
  # struct ::__lang::builtin::array<T type, N uint> {}
  result.types.add(["__lang", "builtin", "array"].toPath, newType(Type(kind: tykArray, arr: TypeArray(elem: generic_elem, size: generic_size))).toQualifiedType)

  # #[lang(var)]
  # struct ::__lang::builtin::var<T type> {}
  result.types.add(["__lang", "builtin", "var"].toPath, newType(Type(kind: tykVar, vararr: TypeVar(elem: generic_elem))).toQualifiedType)

  # #[lang(string)]
  # struct ::__lang::builtin::str {
  #   len uint,
  #   ptr schar*,
  # }
  let str_name = ["__lang", "builtin", "str"].toPath
  let str_fields = @[
    StructField(name: some("len"), ty: result.types["uint".toPath].toTypeSumRef),
    StructField(name: some("ptr"), ty: newType(Type(kind: tykPointer, point: TypePointer(pointee: result.types["s8".toPath].toTypeSumRef))).toQualifiedType.toTypeSumRef),
  ]
  result.types.add(str_name, newType(Type(kind: tykStruct, struct: TypeStruct(name: some(str_name), fields: str_fields))).toQualifiedType)
  let str_impl = newConsImpl(str_name, none[string](), true, false, true, result.get(str_name).toTypeSumRef)
  result.add_impl(["__lang", "builtin", "str"].toPath, str_impl)

  # #[lang(range)]
  # struct ::__lang::builtin::Range<T type> {
  #   start T,
  #   end T,
  # }
  let range_name = ["__lang", "builtin", "Range"].toPath
  let range_args = @[("T", generic_elem[].as_qualified)]
  let range_fields = @[
    StructField(name: some("start"), ty: generic_elem),
    StructField(name: some("end"), ty: generic_elem),
  ]
  let range_type = newType(Type(kind: tykStruct, struct: TypeStruct(name: some(range_name), fields: range_fields))).toQualifiedType
  range_type.args = range_args.toOrderedTable
  result.types.add(range_name, range_type)
  let range_impl = newConsImpl(range_name, none[string](), true, false, true, result.get(range_name).toTypeSumRef)
  result.add_impl(["__lang", "builtin", "Range"].toPath, range_impl)

  # #[lang(range_to)]
  # struct ::__lang::builtin::RangeTo<T type> {
  #   end T,
  # }
  let range_to_name = ["__lang", "builtin", "RangeTo"].toPath
  let range_to_args = @[("T", generic_elem[].as_qualified)]
  let range_to_fields = @[
    StructField(name: some("end"), ty: generic_elem),
  ]
  let range_to_type = newType(Type(kind: tykStruct, struct: TypeStruct(name: some(range_to_name), fields: range_to_fields))).toQualifiedType
  range_to_type.args = range_to_args.toOrderedTable
  result.types.add(range_to_name, range_to_type)
  let range_to_impl = newConsImpl(range_to_name, none[string](), true, false, true, result.get(range_to_name).toTypeSumRef)
  result.add_impl(["__lang", "builtin", "RangeTo"].toPath, range_to_impl)

  # #[lang(range_from)]
  # struct ::__lang::builtin::RangeFrom<T type> {
  #   start T,
  # }
  let range_from_name = ["__lang", "builtin", "RangeFrom"].toPath
  let range_from_args = @[("T", generic_elem[].as_qualified)]
  let range_from_fields = @[
    StructField(name: some("start"), ty: generic_elem),
  ]
  let range_from_type = newType(Type(kind: tykStruct, struct: TypeStruct(name: some(range_from_name), fields: range_from_fields))).toQualifiedType
  range_from_type.args = range_from_args.toOrderedTable
  result.types.add(range_from_name, range_from_type)
  let range_from_impl = newConsImpl(range_from_name, none[string](), true, false, true, result.get(range_from_name).toTypeSumRef)
  result.add_impl(["__lang", "builtin", "RangeFrom"].toPath, range_from_impl)

  # #[lang(range_full)]
  # struct ::__lang::builtin::RangeFull<T type> {}
  let range_full_name = ["__lang", "builtin", "RangeFull"].toPath
  result.types.add(range_full_name, newType(Type(kind: tykStruct, struct: TypeStruct(name: some(range_full_name), fields: @[]))).toQualifiedType)
  let range_full_impl = newConsImpl(range_full_name, none[string](), true, false, true, result.get(range_full_name).toTypeSumRef)
  result.add_impl(["__lang", "builtin", "RangeFull"].toPath, range_full_impl)

  result.impl_generic(generic_elem[].as_qualified)
  result.impl_bool(result.types["bool".toPath])
  result.impl_intlike(result.types["s8".toPath])
  result.impl_intlike(result.types["s16".toPath])
  result.impl_intlike(result.types["s32".toPath])
  result.impl_intlike(result.types["s64".toPath])
  result.impl_intlike(result.types["s128".toPath])
  result.impl_intlike(result.types["sint".toPath])
  result.impl_intlike(result.types["u8".toPath])
  result.impl_intlike(result.types["u16".toPath])
  result.impl_intlike(result.types["u32".toPath])
  result.impl_intlike(result.types["u64".toPath])
  result.impl_intlike(result.types["u128".toPath])
  result.impl_intlike(result.types["uint".toPath])
  result.impl_floatlike(result.types["float16".toPath])
  result.impl_floatlike(result.types["float32".toPath])
  result.impl_floatlike(result.types["float64".toPath])
  result.impl_floatlike(result.types["float80".toPath])
  result.impl_floatlike(result.types["float128".toPath])
  result.impl_floatlike(result.types["float".toPath])

  result.impl_ptr(result.types[["__lang", "builtin", "pointer"].toPath])
  result.impl_arraylike(result.types[["__lang", "builtin", "slice"].toPath])
  result.impl_arraylike(result.types[["__lang", "builtin", "array"].toPath])
  result.impl_arraylike(result.types[["__lang", "builtin", "var"].toPath])

proc enter*(ctx: var TypeContext) =
  ctx.impls.add(ImplScope(kind: igkScope, impls: initTable[Path, OrderedSet[ref Impl]]()))

proc enter_this*(ctx: var TypeContext, scope: ImplScope) =
  ctx.impls.add(scope)

proc enter_bounded*(ctx: var TypeContext) =
  ctx.impls.add(ImplScope(kind: igkBoundary))
  ctx.enter

proc enter_this_bounded*(ctx: var TypeContext, scope: ImplScope) =
  ctx.impls.add(ImplScope(kind: igkBoundary))
  ctx.enter_this(scope)

proc leave*(ctx: var TypeContext) =
  discard ctx.impls.pop()

proc leave_this*(ctx: var TypeContext): ImplScope =
  return ctx.impls.pop()

proc leave_bounded*(ctx: var TypeContext) =
  ctx.leave
  discard ctx.impls.pop()

proc leave_this_bounded*(ctx: var TypeContext): ImplScope =
  result = ctx.leave_this
  discard ctx.impls.pop()

iterator param_ptrs_of*(ty: QualifiedType): tuple[i: int, key: string, val: ParamPtr] =
  var i = 0
  for c in ty.param_ptrs.pairs:
    for p in c[1]:
      yield (i, c[0], p)
      inc i

iterator param_ptrs_of*(sum: TypeSum): tuple[i: int, key: string, val: ParamPtr] =
  var i = 0
  for ty in sum.sum:
    for c in ty.param_ptrs.pairs:
      for p in c[1]:
        yield (i, c[0], p)
        inc i

iterator param_ptrs_keys*(ty: QualifiedType): tuple[i: int, key: string] =
  var i = 0
  for c in ty.param_ptrs.pairs:
    yield (i, c[0])
    inc i

iterator param_ptrs_keys*(sum: TypeSum): tuple[i: int, key: string] =
  var i = 0
  for ty in sum.sum:
    for c in ty.param_ptrs.pairs:
      yield (i, c[0])
      inc i

proc generate*(ty: ref QualifiedType) =
  let inner = ty.inner
  case inner.kind
  of tykTrait:
    for i, field in inner.trait.fns.pairs:
      for q in field.ty.sum:
        for p in q[].param_ptrs_of:
          if not ty.param_ptrs.contains(p[1]):
            ty.param_ptrs.add(p[1], @[])
          ty.param_ptrs[p[1]].add(ParamPtr(kind: ppkTraitFn, fn_idx: i))
  of tykStruct:
    for i, field in inner.struct.fields.pairs:
      for q in field.ty.sum:
        for p in q[].param_ptrs_of:
          if not ty.param_ptrs.contains(p[1]):
            ty.param_ptrs.add(p[1], @[])
          ty.param_ptrs[p[1]].add(ParamPtr(kind: ppkStructField, field_idx: i))
  of tykUnion:
    for i, field in inner.union.fields.pairs:
      for q in field.ty.sum:
        for p in q[].param_ptrs_of:
          if not ty.param_ptrs.contains(p[1]):
            ty.param_ptrs.add(p[1], @[])
          ty.param_ptrs[p[1]].add(ParamPtr(kind: ppkUnionField, field_idx: i))
  of tykTagged:
    for i, field in inner.tagged.fields.pairs:
      for q in field.ty.sum:
        for p in q[].param_ptrs_of:
          if not ty.param_ptrs.contains(p[1]):
            ty.param_ptrs.add(p[1], @[])
          ty.param_ptrs[p[1]].add(ParamPtr(kind: ppkTaggedField, field_idx: i))
    for i, variant in inner.tagged.variants.pairs:
      for j, field in variant.fields.pairs:
        for q in field.ty.sum:
          for p in q[].param_ptrs_of:
            if not ty.param_ptrs.contains(p[1]):
              ty.param_ptrs.add(p[1], @[])
            ty.param_ptrs[p[1]].add(ParamPtr(kind: ppkTaggedvField, variant_idx: i, vfield_idx: j))
  of tykFunction:
    for q in inner.function.result_type.sum:
      for p in q[].param_ptrs_of:
        if not ty.param_ptrs.contains(p[1]):
          ty.param_ptrs.add(p[1], @[])
        ty.param_ptrs[p[1]].add(ParamPtr(kind: ppkFunctionResult))
    for i, param in inner.function.params.pairs:
      for q in param.ty.sum:
        for p in q[].param_ptrs_of:
          if not ty.param_ptrs.contains(p[1]):
            ty.param_ptrs.add(p[1], @[])
          ty.param_ptrs[p[1]].add(ParamPtr(kind: ppkFunctionParam, param_idx: i))
  of tykPointer:
    for q in inner.point.pointee.sum:
      for p in q[].param_ptrs_of:
        if not ty.param_ptrs.contains(p[1]):
          ty.param_ptrs.add(p[1], @[])
        ty.param_ptrs[p[1]].add(ParamPtr(kind: ppkPointee))
  of tykSlice:
    for q in inner.slice.elem.sum:
      for p in q[].param_ptrs_of:
        if not ty.param_ptrs.contains(p[1]):
          ty.param_ptrs.add(p[1], @[])
        ty.param_ptrs[p[1]].add(ParamPtr(kind: ppkSliceElem))
  of tykArray:
    var q = inner.arr.elem[].as_qualified
    for p in q[].param_ptrs_of:
      if not ty.param_ptrs.contains(p[1]):
        ty.param_ptrs.add(p[1], @[])
      ty.param_ptrs[p[1]].add(ParamPtr(kind: ppkArrayElem))
    q = inner.arr.size[].as_qualified
    for p in q[].param_ptrs_of:
      if not ty.param_ptrs.contains(p[1]):
        ty.param_ptrs.add(p[1], @[])
      ty.param_ptrs[p[1]].add(ParamPtr(kind: ppkArraySize))
  of tykVector:
    for q in inner.vec.elem.sum:
      for p in q[].param_ptrs_of:
        if not ty.param_ptrs.contains(p[1]):
          ty.param_ptrs.add(p[1], @[])
        ty.param_ptrs[p[1]].add(ParamPtr(kind: ppkArrayElem))
    for q in inner.vec.size.sum:
      for p in q[].param_ptrs_of:
        if not ty.param_ptrs.contains(p[1]):
          ty.param_ptrs.add(p[1], @[])
        ty.param_ptrs[p[1]].add(ParamPtr(kind: ppkArraySize))
  of tykVar:
    for q in inner.vararr.elem.sum:
      for p in q[].param_ptrs_of:
        if not ty.param_ptrs.contains(p[1]):
          ty.param_ptrs.add(p[1], @[])
        ty.param_ptrs[p[1]].add(ParamPtr(kind: ppkVarElem))
  of tykParameter:
    ty.param_ptrs.add(inner.param.name, @[ParamPtr(kind: ppkDone)])
  else:
    discard

  ty.hash = none[Hash]()

proc regenerate*(ty: ref QualifiedType) =
  ty.param_ptrs.clear
  ty.generate

proc newType*(ty: Type): ref Type =
  new result
  result[] = ty

proc newQualifiedType*(inner: ref Type): ref QualifiedType =
  new result
  result.inner = inner
  result.args = initOrderedTable[string, ref QualifiedType]()
  result.param_ptrs = initTable[string, seq[ParamPtr]]()
  result.size = none[Sizeof]()
  result.align = none[Alignof]()
  result.is_eval = false
  result.is_hidden = false
  result.is_parameter = false
  result.is_return = false

  result.generate

proc toQualifiedType*(ty: ref Type): ref QualifiedType =
  return newQualifiedType(ty)

proc newTypeSum*(): TypeSum =
  return TypeSum(sum: initHashSet[ref QualifiedType]())

proc toTypeSum*(ty: ref QualifiedType): TypeSum =
  return TypeSum(sum: [ty].toHashSet)

proc toTypeSumRef*(ty: ref QualifiedType): ref TypeSum =
  new result
  result[] = ty.toTypeSum

proc toTypeSumRef*(ty: TypeSum): ref TypeSum =
  new result
  result[] = ty

proc is_monomorphized*(ty: ref QualifiedType, recurse: var HashSet[uint]): bool =
  let typtr = cast[uint](ty)
  if recurse.contains(typtr):
    return true
  recurse.incl(typtr)

  for param_ptr in ty[].param_ptrs_of:
    case param_ptr[2].kind
    of ppkDone:
      return not ty.inner[].is_parameter:
    of ppkTraitFn:
      for t in ty.inner.trait.fns[param_ptr[2].fn_idx].ty.sum:
        if not t.is_monomorphized(recurse):
          return false
    of ppkStructField:
      for t in ty.inner.struct.fields[param_ptr[2].field_idx].ty.sum:
        if not t.is_monomorphized(recurse):
          return false
    of ppkUnionField:
      for t in ty.inner.union.fields[param_ptr[2].field_idx].ty.sum:
        if not t.is_monomorphized(recurse):
          return false
    of ppkTaggedField:
      for t in ty.inner.tagged.fields[param_ptr[2].field_idx].ty.sum:
        if not t.is_monomorphized(recurse):
          return false
    of ppkTaggedVField:
      for t in ty.inner.tagged.variants[param_ptr[2].variant_idx].fields[param_ptr[2].vfield_idx].ty.sum:
        if not t.is_monomorphized(recurse):
          return false
    of ppkFunctionResult:
      for t in ty.inner.function.result_type.sum:
        if not t.is_monomorphized(recurse):
          return false
    of ppkFunctionParam:
      for t in ty.inner.function.params[param_ptr[2].param_idx].ty.sum:
        if not t.is_monomorphized(recurse):
          return false
    of ppkPointee:
      for t in ty.inner.point.pointee.sum:
        if not t.is_monomorphized(recurse):
          return false
    of ppkSliceElem:
      for t in ty.inner.slice.elem.sum:
        if not t.is_monomorphized(recurse):
          return false
    of ppkArrayElem:
      for t in ty.inner.arr.elem.sum:
        if not t.is_monomorphized(recurse):
          return false
    of ppkArraySize:
      for t in ty.inner.arr.size.sum:
        if not t.is_monomorphized(recurse):
          return false
    of ppkVarElem:
      for t in ty.inner.vararr.elem.sum:
        if not t.is_monomorphized(recurse):
          return false
  return true

proc is_monomorphized*(ty: ref QualifiedType): bool =
  var recurse = initHashSet[uint]()
  result = ty.is_monomorphized(recurse)

proc has_type_params*(ty: ref QualifiedType): bool {.inline.} =
  return not ty.is_monomorphized

proc has_type_params*(sum: TypeSum): bool =
  for ty in sum.sum:
    if ty.has_type_params:
      return true

proc is_simple_type*(ty: QualifiedType): bool =
  case ty.inner.kind
  of tykUnit, tykNever, tykBool, tykSigned, tykUnsigned, tykFloating, tykParameter, tykLit: return true
  else: return false

proc owns_type_args(ty: QualifiedType): bool = ty.args.len > 0
proc owns_type_args(ty: ref QualifiedType): bool = ty.args.len > 0

proc has_type_args*(ty: QualifiedType): bool {.inline.} =
  return ty.owns_type_args and ty.inner.kind != tykParameter

proc has_type_args*(sum: TypeSum): bool =
  for ty in sum.sum:
    if ty[].has_type_args:
      return true

proc is_qualified*(sum: TypeSum): bool {.inline.} =
  return sum.sum.len == 1

proc is_unspecified*(sum: TypeSum): bool {.inline.} =
  return sum.sum.len == 0

proc elem*(ty: ref Type): ref TypeSum =
  case ty.kind
  of tykPointer: return ty.point.pointee
  of tykSlice: return ty.slice.elem
  of tykArray: return ty.arr.elem
  of tykVector: return ty.vec.elem
  of tykVar: return ty.vararr.elem
  else: raise newIce("type doesn't have an element type")

proc pointee*(ty: ref Type): ref TypeSum =
  return ty.point.pointee

proc result_type*(ty: ref Type): ref TypeSum =
  return ty.function.result_type

proc params*(ty: ref Type): seq[Argument] =
  return ty.function.params

proc elem*(ty: ref QualifiedType): ref TypeSum =
  return ty.inner.elem

proc pointee*(ty: ref QualifiedType): ref TypeSum =
  return ty.inner.pointee

proc try_pointee*(ty: ref QualifiedType): Option[ref TypeSum] =
  case ty.inner.kind
  of tykPointer:
    return some(ty.inner.pointee)
  else:
    return none[ref TypeSum]()

proc try_elem*(ty: ref QualifiedType): Option[ref TypeSum] =
  case ty.inner.kind
  of tykPointer, tykSlice, tykArray, tykVector, tykVar:
    return some(ty.inner.elem)
  else:
    return none[ref TypeSum]()

proc result_type*(ty: ref QualifiedType): ref TypeSum =
  return ty.inner.result_type

proc params*(ty: ref QualifiedType): seq[Argument] =
  return ty.inner.params

proc is_unit*(ty: ref QualifiedType): bool =
  return ty.inner.kind == tykUnit

proc is_bool*(ty: ref QualifiedType): bool =
  return ty.inner.kind == tykBool

proc is_array*(ty: ref QualifiedType): bool =
  return ty.inner.kind == tykArray

proc as_qualified_or_uninit*(sum: TypeSum): ref QualifiedType {.noSideEffect.} =
  if sum.is_qualified:
    for ty in sum.sum:
      return ty

proc as_qualified*(sum: TypeSum): ref QualifiedType =
  if sum.is_qualified:
    for ty in sum.sum:
      return ty
  elif sum.is_unspecified:
    raise newIce("unspecified type")
  else:
    raise newIce(fmt"ambigous type: {sum}")

iterator items*(sum: TypeSum): ref QualifiedType =
  for ty in sum.sum:
    yield ty

proc add*(sum: var TypeSum, ty: ref QualifiedType) {.inline.} =
  sum.sum.incl(ty)

proc add*(sum: var TypeSum, ty: TypeSum) =
  for t in ty.sum:
    sum.add(t)

proc `$`*(ty: QualifiedType): string =
  case ty.inner.kind
  of tykUnit: return "unit"
  of tykNever: return "never"
  of tykBool: return "bool"
  of tykSigned:
    case ty.inner.signed.bits.kind
    of bitsWord: return "sint"
    of bitsCustom:
      case ty.inner.signed.bits.bits
      of 8: return "s8"
      of 16: return "s16"
      of 32: return "s32"
      of 64: return "s64"
      of 128: return "s128"
      else: raise newIce("invalid signed type width")
  of tykUnsigned:
    case ty.inner.unsigned.bits.kind
    of bitsWord: return "uint"
    of bitsCustom:
      case ty.inner.unsigned.bits.bits
      of 8: return "u8"
      of 16: return "u16"
      of 32: return "u32"
      of 64: return "u64"
      of 128: return "u128"
      else: raise newIce("invalid unsigned type width")
  of tykFloating:
    case ty.inner.floating.bits.kind
    of bitsWord:
      return "float"
    of bitsCustom:
      case ty.inner.floating.bits.bits
      of 16: return "float16"
      of 32: return "float32"
      of 64: return "float64"
      of 80: return "float80"
      of 128: return "float128"
      else: raise newIce("invalid floating type width")
  of tykTrait:
    result = fmt"trait {ty.inner.trait.name}"
    if ty.has_type_args:
      result.add("(")
      for i, arg in ty.type_args_named:
        if i > 0:
          result.add(", ")
        result.add(arg[0])
        result.add(": ")
        result.add($arg[1][])
      result.add(")")
  of tykStruct:
    if ty.inner.struct.name.isSome:
      result = fmt"struct {ty.inner.struct.name.get}"
      if ty.has_type_args:
        result.add("(")
        for i, arg in ty.type_args_named:
          if i > 0:
            result.add(", ")
          result.add(arg[0])
          result.add(": ")
          result.add($arg[1][])
        result.add(")")
    else:
      result = fmt"struct"
      if ty.has_type_args:
        result.add("(")
        for i, arg in ty.type_args_named:
          if i > 0:
            result.add(", ")
          result.add(arg[0])
          result.add(": ")
          result.add($arg[1][])
        result.add(")")
  of tykUnion:
    if ty.inner.union.name.isSome:
      result = fmt"union {ty.inner.union.name.get}"
      if ty.has_type_args:
        result.add("(")
        for i, arg in ty.type_args_named:
          if i > 0:
            result.add(", ")
          result.add(arg[0])
          result.add(": ")
          result.add($arg[1][])
        result.add(")")
    else:
      result = fmt"union"
      if ty.has_type_args:
        result.add("(")
        for i, arg in ty.type_args_named:
          if i > 0:
            result.add(", ")
          result.add(arg[0])
          result.add(": ")
          result.add($arg[1][])
        result.add(")")
  of tykEnum: return fmt"enum {ty.inner.enumeration.name}"
  of tykTagged:
    result = fmt"tagged {ty.inner.tagged.name}"
    if ty.has_type_args:
      result.add("(")
      for i, arg in ty.type_args_named:
        if i > 0:
          result.add(", ")
        result.add(arg[0])
        result.add(": ")
        result.add($arg[1][])
      result.add(")")
  of tykFunction:
    result.add("(")
    for i, t in ty.inner.function.params:
      if i > 0:
        result.add(", ")
      if t.name.isSome:
        result.add(t.name.get)
        result.add(": ")
      result.add($t.ty[])
    result.add(") -> ")
    result.add($ty.inner.function.result_type[])
  of tykPointer:
    return fmt"*{ty.inner.point.pointee[]}"
  of tykSlice:
    return fmt"[{ty.inner.slice.elem[]}]"
  of tykArray:
    return fmt"[{ty.inner.arr.elem[]}, {ty.inner.arr.size[]}]"
  of tykVector:
    return fmt"__lang::builtin::vector(T: {ty.inner.vec.elem[]}, N: {ty.inner.vec.size[]})"
  of tykVar:
    return fmt"..{ty.inner.vararr.elem[]}"
  of tykParameter:
    case ty.inner.param.kind
    of pkType:
      return fmt"({ty.inner.param.name}: type)"
    else:
      return fmt"({ty.inner.param.name}: {ty.inner.param.ty[]})"
  of tykLit:
    case ty.inner.lit.kind
    of lkInt:
      return fmt"({ty.inner.lit.intval}: {ty.inner.lit.ty[]})"
    of lkFloat:
      return fmt"({ty.inner.lit.floatval}: {ty.inner.lit.ty[]})"
    of lkString:
      return fmt"({ty.inner.lit.strval}: {ty.inner.lit.ty[]})"
  of tykType:
    return "type"

proc `$`*(sum: TypeSum): string =
  if sum.is_qualified:
    for ty in sum.sum:
      result = $ty[]
  elif sum.is_unspecified:
    result = "(unspecified)"
  else:
    result = "(any of "
    for i, t in sum.sum.toSeq.pairs:
      if i > 0:
        result.add(", ")
      result.add($t[])
    result.add(")")

proc name*(ty: QualifiedType): Path =
  case ty.inner.kind
  of tykUnit: return "unit".toPath
  of tykNever: return "never".toPath
  of tykBool: return "bool".toPath
  of tykSigned:
    case ty.inner.signed.bits.kind
    of bitsWord: return "sint".toPath
    of bitsCustom:
      case ty.inner.signed.bits.bits
      of 8: return "s8".toPath
      of 16: return "s16".toPath
      of 32: return "s32".toPath
      of 64: return "s64".toPath
      of 128: return "s128".toPath
      else: raise newIce("invalid signed type width")
  of tykUnsigned:
    case ty.inner.unsigned.bits.kind
    of bitsWord: return "uint".toPath
    of bitsCustom:
      case ty.inner.unsigned.bits.bits
      of 8: return "u8".toPath
      of 16: return "u16".toPath
      of 32: return "u32".toPath
      of 64: return "u64".toPath
      of 128: return "u128".toPath
      else: raise newIce("invalid unsigned type width")
  of tykFloating:
    case ty.inner.floating.bits.kind
    of bitsWord:
      return "float".toPath
    of bitsCustom:
      case ty.inner.floating.bits.bits
      of 16: return "float16".toPath
      of 32: return "float32".toPath
      of 64: return "float64".toPath
      of 80: return "float80".toPath
      of 128: return "float128".toPath
      else: raise newIce("invalid floating type width")
  of tykTrait:
    result = ty.inner.trait.name
  of tykStruct:
    if ty.inner.struct.name.isSome:
      result = ty.inner.struct.name.get
    else:
      result = "struct".toPath
  of tykUnion:
    if ty.inner.union.name.isSome:
      result = ty.inner.union.name.get
    else:
      result = "union".toPath
  of tykEnum: return ty.inner.enumeration.name
  of tykTagged:
    result = ty.inner.tagged.name
  of tykFunction:
    raise newIce("type doesn't have a name")
  of tykPointer:
    raise newIce("type doesn't have a name")
  of tykSlice:
    raise newIce("type doesn't have a name")
  of tykArray:
    raise newIce("type doesn't have a name")
  of tykVector:
    raise newIce("type doesn't have a name")
  of tykVar:
    raise newIce("type doesn't have a name")
  of tykParameter:
    return ty.inner.param.name.toPath
  of tykLit:
    raise newIce("type doesn't have a name")
  of tykType:
    return "type".toPath

proc human_readable_name*(ty: QualifiedType): string =
  case ty.inner.kind
  of tykUnit: return "unit"
  of tykNever: return "never"
  of tykBool: return "bool"
  of tykSigned:
    case ty.inner.signed.bits.kind
    of bitsWord: return "sint"
    of bitsCustom:
      case ty.inner.signed.bits.bits
      of 8: return "s8"
      of 16: return "s16"
      of 32: return "s32"
      of 64: return "s64"
      of 128: return "s128"
      else: raise newIce("invalid signed type width")
  of tykUnsigned:
    case ty.inner.unsigned.bits.kind
    of bitsWord: return "uint"

    of bitsCustom:
      case ty.inner.unsigned.bits.bits
      of 8: return "u8"
      of 16: return "u16"
      of 32: return "u32"
      of 64: return "u64"
      of 128: return "u128"
      else: raise newIce("invalid unsigned type width")
  of tykFloating:
    case ty.inner.floating.bits.kind
    of bitsWord:
      return "float"
    of bitsCustom:
      case ty.inner.floating.bits.bits
      of 16: return "float16"
      of 32: return "float32"
      of 64: return "float64"
      of 80: return "float80"
      of 128: return "float128"
      else: raise newIce("invalid floating type width")
  of tykTrait:
    result = $ty.inner.trait.name
    if ty.has_type_args:
      result.add("(")
      for i, arg in ty.type_args_named:
        if i > 0:
          result.add(", ")
        result.add(arg[0])
        result.add(": ")
        result.add($arg[1][])
      result.add(")")
  of tykStruct:
    if ty.inner.struct.name.isSome:
      result = $ty.inner.struct.name.get
      if ty.has_type_args:
        result.add("(")
        for i, arg in ty.type_args_named:
          if i > 0:
            result.add(", ")
          result.add(arg[0])
          result.add(": ")
          result.add($arg[1][])
        result.add(")")
    else:
      result = fmt"struct"
      if ty.has_type_args:
        result.add("(")
        for i, arg in ty.type_args_named:
          if i > 0:
            result.add(", ")
          result.add(arg[0])
          result.add(": ")
          result.add($arg[1][])
        result.add(")")
  of tykUnion:
    if ty.inner.union.name.isSome:
      result = $ty.inner.union.name.get
      if ty.has_type_args:
        result.add("(")
        for i, arg in ty.type_args_named:
          if i > 0:
            result.add(", ")
          result.add(arg[0])
          result.add(": ")
          result.add($arg[1][])
        result.add(")")
    else:
      result = fmt"union"
      if ty.has_type_args:
        result.add("(")
        for i, arg in ty.type_args_named:
          if i > 0:
            result.add(", ")
          result.add(arg[0])
          result.add(": ")
          result.add($arg[1][])
        result.add(")")
  of tykEnum: return $ty.inner.enumeration.name
  of tykTagged:
    result = $ty.inner.tagged.name
    if ty.has_type_args:
      result.add("(")
      for i, arg in ty.type_args_named:
        if i > 0:
          result.add(", ")
        result.add(arg[0])
        result.add(": ")
        result.add($arg[1][])
      result.add(")")
  of tykFunction:
    result.add("(")
    for i, t in ty.inner.function.params:
      if i > 0:
        result.add(", ")
      if t.name.isSome:
        result.add(t.name.get)
        result.add(": ")
      result.add($t.ty[])
    result.add(") -> ")
    result.add($ty.inner.function.result_type[])
  of tykPointer:
    return fmt"*{ty.inner.point.pointee[].as_qualified[].human_readable_name}"
  of tykSlice:
    return fmt"[{ty.inner.slice.elem[].as_qualified[].human_readable_name}]"
  of tykArray:
    return fmt"[{ty.inner.arr.elem[].as_qualified[].human_readable_name}, {ty.inner.arr.size[]}]"
  of tykVector:
    return fmt"__lang::builtin::vector(T: {ty.inner.vec.elem[].as_qualified[].human_readable_name}, N: {ty.inner.vec.size[]})"
  of tykVar:
    return fmt"..{ty.inner.vararr.elem[].as_qualified[].human_readable_name}"
  of tykParameter:
    case ty.inner.param.kind
    of pkType:
      return fmt"({ty.inner.param.name}: type)"
    else:
      return fmt"({ty.inner.param.name}: {ty.inner.param.ty[]})"
  of tykLit:
    case ty.inner.lit.kind
    of lkInt:
      return fmt"({ty.inner.lit.intval}: {ty.inner.lit.ty[]})"
    of lkFloat:
      return fmt"({ty.inner.lit.floatval}: {ty.inner.lit.ty[]})"
    of lkString:
      return fmt"({ty.inner.lit.strval}: {ty.inner.lit.ty[]})"
  of tykType:
    return "type"

proc as_qualified_in_span*(sum: TypeSum, span: Span): ref QualifiedType =
  if sum.is_qualified:
    for ty in sum.sum:
      return ty
  elif sum.is_unspecified:
    raise newError(errkUnspecifiedType, span)
  else:
    raise newError(errkAmbigousType, span, $sum)

proc `==`*(lhs: ref Impl, rhs: ref Impl): bool =
  if lhs.name != rhs.name: return false
  if lhs.kind != rhs.kind: return false
  return ty(lhs) == ty(rhs)

proc eq*(lhs: StructField, rhs: StructField, recurse: var HashSet[(uint, uint)]): bool =
  if lhs.name.isSome != rhs.name.isSome: return false
  if lhs.name.isSome and lhs.name.get != rhs.name.get: return false
  return lhs.ty.eq(rhs.ty, recurse)

proc eq*(lhs: Argument, rhs: Argument, recurse: var HashSet[(uint, uint)]): bool =
  if lhs.name.isSome != rhs.name.isSome: return false
  if lhs.name.isSome and lhs.name.get != rhs.name.get: return false
  return lhs.ty.eq(rhs.ty, recurse)

proc eq*(lhs: TaggedVariant, rhs: TaggedVariant, recurse: var HashSet[(uint, uint)]): bool =
  if lhs.name != rhs.name: return false
  if lhs.fields.len != rhs.fields.len: return false
  for i in 0 ..< lhs.fields.len:
    if not lhs.fields[i].eq(rhs.fields[i], recurse):
      return false
  return true

proc eq*(lhs: ref QualifiedType, rhs: ref QualifiedType, recurse: var HashSet[(uint, uint)]): bool =
  if recurse.contains((cast[uint](lhs), cast[uint](rhs))):
    return true
  else:
    recurse.incl((cast[uint](lhs), cast[uint](rhs)))

  case lhs.inner.kind
  of tykUnit: return rhs.inner.kind == tykUnit
  of tykNever: return rhs.inner.kind == tykNever
  of tykBool: return rhs.inner.kind == tykBool
  of tykSigned: return rhs.inner.kind == tykSigned and lhs.inner.signed.bits == rhs.inner.signed.bits
  of tykUnsigned: return rhs.inner.kind == tykUnsigned and lhs.inner.unsigned.bits == rhs.inner.unsigned.bits
  of tykFloating: return rhs.inner.kind == tykFloating and lhs.inner.floating.bits == rhs.inner.floating.bits
  of tykTrait:
    if rhs.inner.kind != tykTrait:
      return false

    if lhs.inner.trait.name != rhs.inner.trait.name:
      return false

    if lhs.args.len != rhs.args.len:
      return false

    for k, lhs in lhs.args:
      if not lhs.eq(rhs.args[k], recurse):
        return false

    return true
  of tykStruct:
    if rhs.inner.kind != tykStruct:
      return false

    if lhs.inner.struct.name != rhs.inner.struct.name:
      return false

    if lhs.args.len != rhs.args.len:
      return false

    for k, lhs in lhs.args:
      if not lhs.eq(rhs.args[k], recurse):
        return false

    return true
  of tykUnion:
    if rhs.inner.kind != tykUnion:
      return false

    if lhs.inner.union.name != rhs.inner.union.name:
      return false

    if lhs.args.len != rhs.args.len:
      return false

    for k, lhs in lhs.args:
      if not lhs.eq(rhs.args[k], recurse):
        return false

    return true
  of tykEnum: return rhs.inner.kind == tykEnum and lhs.inner.enumeration.name == rhs.inner.enumeration.name
  of tykTagged:
    if rhs.inner.kind != tykTagged:
      return false

    if lhs.inner.tagged.name != rhs.inner.tagged.name:
      return false

    if lhs.args.len != rhs.args.len:
      return false

    for k, lhs in lhs.args:
      if not lhs.eq(rhs.args[k], recurse):
        return false

    return true
  of tykFunction:
    if rhs.inner.kind != tykFunction:
      return false

    if lhs.params.len != rhs.params.len:
      return false

    if not lhs.inner.result_type.eq(rhs.inner.result_type, recurse):
      return false

    for i in 0 ..< lhs.inner.params.len:
      if not lhs.inner.params[i].ty.eq(rhs.inner.params[i].ty, recurse):
        return false

    return true
  of tykPointer: return rhs.inner.kind == tykPointer and lhs.inner.point.pointee.eq(rhs.inner.point.pointee, recurse)
  of tykSlice: return rhs.inner.kind == tykSlice and lhs.inner.slice.elem.eq(rhs.inner.slice.elem, recurse)
  of tykArray: return rhs.inner.kind == tykArray and lhs.inner.arr.elem.eq(rhs.inner.arr.elem, recurse) and lhs.inner.arr.size.eq(rhs.inner.arr.size, recurse)
  of tykVector: return rhs.inner.kind == tykVector and lhs.inner.vec.elem.eq(rhs.inner.vec.elem, recurse) and lhs.inner.vec.size.eq(rhs.inner.vec.size, recurse)
  of tykVar: return rhs.inner.kind == tykVar and lhs.inner.vararr.elem.eq(rhs.inner.vararr.elem, recurse)
  of tykParameter: return rhs.inner.kind == tykParameter and lhs.inner.param == rhs.inner.param
  of tykLit: return rhs.inner.kind == tykLit and lhs.inner.lit == rhs.inner.lit
  of tykType: return rhs.inner.kind == tykType

proc eq*(lhs: ref TypeSum, rhs: ref TypeSum, recurse: var HashSet[(uint, uint)]): bool =
  for lhs in lhs.sum:
    for rhs in rhs.sum:
      if lhs.eq(rhs, recurse):
        return true

proc `==`*(lhs: TypeParameter, rhs: TypeParameter): bool =
  if lhs.name != rhs.name:
    return false
  case lhs.kind
  of pkType: return rhs.kind == pkType
  of pkInt, pkFloat, pkString: return rhs.kind == lhs.kind and lhs.ty == rhs.ty

proc `==`*(lhs: TypeLiteral, rhs: TypeLiteral): bool =
  case lhs.kind
  of lkInt: return rhs.kind == lkInt and lhs.intval == rhs.intval
  of lkFloat: return rhs.kind == lkFloat and lhs.floatval == rhs.floatval
  of lkString: return rhs.kind == lkString and lhs.strval == rhs.strval

proc `==`*(lhs: StructField, rhs: StructField): bool =
  if lhs.name.isSome != rhs.name.isSome: return false
  if lhs.name.isSome and lhs.name.get != rhs.name.get: return false
  return lhs.ty == rhs.ty

proc `==`*(lhs: ref QualifiedType, rhs: ref QualifiedType): bool =
  var recurse = initHashSet[(uint, uint)]()
  return lhs.eq(rhs, recurse)

proc `==`*(lhs: ref TypeSum, rhs: ref TypeSum): bool =
  var recurse = initHashSet[(uint, uint)]()
  return lhs.eq(rhs, recurse)

proc `==`*(lhs: TypeSum, rhs: TypeSum): bool =
  var recurse = initHashSet[(uint, uint)]()
  for lhs in lhs.sum:
    for rhs in rhs.sum:
      if lhs.eq(rhs, recurse):
        return true

proc is_untyped_node*(t: Type): bool =
  return t.kind == tykStruct and t.struct.name.isSome and t.struct.name.get  == ["__lang", "macros", "Node"].toPath

proc is_typed_node*(t: Type): bool =
  return t.kind == tykStruct and t.struct.name.isSome and t.struct.name.get  == ["__lang", "macros", "Typed"].toPath

proc is_untyped_node*(t: QualifiedType): bool =
  return t.inner[].is_untyped_node

proc is_typed_node*(t: QualifiedType): bool =
  return t.inner[].is_typed_node

proc is_untyped_node*(t: TypeSum): bool =
  return t.is_qualified and t.as_qualified[].is_untyped_node

proc is_typed_node*(t: TypeSum): bool =
  return t.is_qualified and t.as_qualified[].is_typed_node

proc matches*(lhs: TypeParameter, rhs: TypeParameter, in_macro: bool): bool = lhs == rhs
proc matches*(lhs: TypeLiteral, rhs: TypeLiteral, in_macro: bool): bool = lhs == rhs
proc matches*(lhs: ref TypeSum, rhs: ref TypeSum, in_macro: bool, recurse: var HashSet[(uint, uint)]): bool
proc matches*(lhs: StructField, rhs: StructField, in_macro: bool, recurse: var HashSet[(uint, uint)]): bool =
  if lhs.name.isSome != rhs.name.isSome: return false
  if lhs.name.isSome and lhs.name.get != rhs.name.get: return false
  return lhs.ty.matches(rhs.ty, in_macro, recurse)

proc matches*(lhs: Argument, rhs: Argument, in_macro: bool, recurse: var HashSet[(uint, uint)]): bool =
  if lhs.name.isSome != rhs.name.isSome: return false
  if lhs.name.isSome and lhs.name.get != rhs.name.get: return false
  return lhs.ty.matches(rhs.ty, in_macro, recurse)

proc matches*(lhs: TaggedVariant, rhs: TaggedVariant, in_macro: bool, recurse: var HashSet[(uint, uint)]): bool =
  if lhs.name != rhs.name: return false
  if lhs.fields.len != rhs.fields.len: return false
  for i in 0 ..< lhs.fields.len:
    if not lhs.fields[i].matches(rhs.fields[i], in_macro, recurse):
      return false
  return true

proc matches*(lhs: ref QualifiedType, rhs: ref QualifiedType, in_macro: bool, recurse: var HashSet[(uint, uint)]): bool =
  if recurse.contains((cast[uint](lhs), cast[uint](rhs))):
    return true
  else:
    recurse.incl((cast[uint](lhs), cast[uint](rhs)))

  case rhs.inner.kind
  of tykParameter:
    case rhs.inner.param.kind
    of pkType: return lhs.inner[].is_type
    of pkInt: return lhs.inner[].is_int
    of pkFloat: return lhs.inner[].is_float
    of pkString: return lhs.inner[].is_str
  of tykStruct:
    if rhs[].is_untyped_node:
      return true
    elif rhs[].is_typed_node:
      return lhs.matches(rhs.inner.struct.fields[1].ty[].as_qualified, in_macro, recurse)
  else: discard

  case lhs.inner.kind
  of tykUnit: return rhs.inner.kind == tykUnit
  of tykNever: return true
  of tykBool: return rhs.inner.kind == tykBool
  of tykSigned: return rhs.inner.kind == tykSigned and lhs.inner.signed.bits == rhs.inner.signed.bits
  of tykUnsigned: return rhs.inner.kind == tykUnsigned and lhs.inner.unsigned.bits == rhs.inner.unsigned.bits
  of tykFloating: return rhs.inner.kind == tykFloating and lhs.inner.floating.bits == rhs.inner.floating.bits
  of tykTrait:
    if rhs.inner.kind != tykTrait:
      return false

    if lhs.inner.trait.name != rhs.inner.trait.name:
      return false

    if lhs.args.len != rhs.args.len:
      return false

    for k, lhs in lhs.args:
      if not lhs.matches(rhs.args[k], in_macro, recurse):
        return false

    return true
  of tykStruct:
    if lhs[].is_untyped_node:
      return true
    elif lhs[].is_typed_node:
      return lhs.inner.struct.fields[1].ty[].as_qualified.matches(rhs, in_macro, recurse)

    if rhs.inner.kind != tykStruct:
      return false

    if lhs.inner.struct.name != rhs.inner.struct.name:
      return false

    if lhs.args.len != rhs.args.len:
      return false

    for k, lhs in lhs.args:
      if not lhs.matches(rhs.args[k], in_macro, recurse):
        return false

    return true
  of tykUnion:
    if rhs.inner.kind != tykUnion:
      return false

    if lhs.inner.union.name != rhs.inner.union.name:
      return false

    if lhs.args.len != rhs.args.len:
      return false

    for k, lhs in lhs.args:
      if not lhs.matches(rhs.args[k], in_macro, recurse):
        return false

    return true
  of tykEnum: return rhs.inner.kind == tykEnum and lhs.inner.enumeration.name == rhs.inner.enumeration.name
  of tykTagged:
    if rhs.inner.kind != tykTagged:
      return false

    if lhs.inner.tagged.name != rhs.inner.tagged.name:
      return false

    if lhs.args.len != rhs.args.len:
      return false

    for k, lhs in lhs.args:
      if not lhs.matches(rhs.args[k], in_macro, recurse):
        return false

    return true
  of tykFunction:
    if rhs.inner.kind != tykFunction:
      return false

    if lhs.args.len != rhs.args.len:
      return false

    if not lhs.inner.result_type.matches(rhs.inner.result_type, in_macro, recurse):
      return false

    for i in 0 ..< lhs.inner.params.len:
      if not lhs.inner.params[i].ty.matches(rhs.inner.params[i].ty, in_macro, recurse):
        return false

    return true
  of tykPointer: return rhs.inner.kind == tykPointer and lhs.inner.point.pointee.matches(rhs.inner.point.pointee, in_macro, recurse)
  of tykSlice: return rhs.inner.kind == tykSlice and lhs.inner.slice.elem.matches(rhs.inner.slice.elem, in_macro, recurse)
  of tykArray: return rhs.inner.kind == tykArray and lhs.inner.arr.elem.matches(rhs.inner.arr.elem, in_macro, recurse) and lhs.inner.arr.size.matches(rhs.inner.arr.size, in_macro, recurse)
  of tykVector: return rhs.inner.kind == tykVector and lhs.inner.vec.elem.matches(rhs.inner.vec.elem, in_macro, recurse) and lhs.inner.vec.size.matches(rhs.inner.vec.size, in_macro, recurse)
  of tykVar: return rhs.inner.kind == tykVar and lhs.inner.vararr.elem.matches(rhs.inner.vararr.elem, in_macro, recurse)
  of tykParameter:
    case lhs.inner.param.kind
    of pkType: return rhs.inner[].is_type
    of pkInt: return rhs.inner[].is_int
    of pkFloat: return rhs.inner[].is_float
    of pkString: return rhs.inner[].is_str
  of tykLit: return rhs.inner.kind == tykLit and lhs.inner.lit.matches(rhs.inner.lit, in_macro)
  of tykType: return rhs.inner.kind == tykType

proc matches*(lhs: ref TypeSum, rhs: ref TypeSum, in_macro: bool, recurse: var HashSet[(uint, uint)]): bool =
  if lhs[].is_untyped_node:
    return true

  if rhs[].is_untyped_node:
    return true

  for lhs in lhs.sum:
    for rhs in rhs.sum:
      if lhs.matches(rhs, in_macro, recurse):
        return true

proc matches*(lhs: TypeParameter, rhs: TypeParameter): bool = matches(lhs, rhs, false)
proc matches*(lhs: TypeLiteral, rhs: TypeLiteral): bool = matches(lhs, rhs, false)
proc matches*(lhs: StructField, rhs: StructField): bool =
  var recurse = initHashSet[(uint, uint)]()
  result = matches(lhs, rhs, false, recurse)

proc matches*(lhs: TaggedVariant, rhs: TaggedVariant): bool =
  var recurse = initHashSet[(uint, uint)]()
  result = matches(lhs, rhs, false, recurse)

proc matches*(lhs: ref QualifiedType, rhs: ref QualifiedType): bool =
  var recurse = initHashSet[(uint, uint)]()
  result = matches(lhs, rhs, false, recurse)

proc matches*(lhs: ref TypeSum, rhs: ref TypeSum): bool =
  var recurse = initHashSet[(uint, uint)]()
  result = matches(lhs, rhs, false, recurse)

proc matches_coerce*(lhs: ref QualifiedType, rhs: ref QualifiedType): bool =
  case rhs.inner.kind
  of tykTrait: return lhs.inner.kind == tykPointer or lhs.inner.kind == tykTrait
  else: discard

  case lhs.inner.kind
  of tykSigned: return rhs.inner.kind == tykSigned or rhs.inner.kind == tykUnsigned
  of tykUnsigned: return rhs.inner.kind == tykSigned or rhs.inner.kind == tykUnsigned
  of tykFloating: return rhs.inner.kind == tykFloating
  of tykTrait: return rhs.inner.kind == tykPointer or rhs.inner.kind == tykTrait
  else:
    return false

proc matches_coerce*(lhs: ref TypeSum, rhs: ref TypeSum): bool =
  for lhs in lhs.sum:
    for rhs in rhs.sum:
      if lhs.matches_coerce(rhs):
        return true

proc toArg(field: StructField): Argument = Argument(name: field.name, ty: field.ty)

proc fields(impl: Impl): seq[Argument] =
  let ty = impl.ty[].as_qualified
  case ty.inner.kind
  of tykStruct:
    return ty.inner.struct.fields.mapIt(it.toArg)
  of tykUnion:
    return ty.inner.union.fields.mapIt(it.toArg)
  of tykTagged:
    var fields = ty.inner.tagged.fields.mapIt(it.toArg)
    for variant in ty.inner.tagged.variants:
      if variant.name == impl.name:
        fields.add(variant.fields.mapIt(it.toArg))
        return fields
  else:
    return newSeq[Argument]()

proc fields(impl: ref Impl): seq[Argument] = impl[].fields

iterator param_items*(impl: ref Impl): Argument =
  case impl.kind
  of ikCons:
    for ty in impl.fields:
      yield ty
  of ikVar:
    for ty in impl.ty.sum:
      case ty.inner.kind
      of tykFunction:
        for param in ty.inner.params:
          yield param
      else: discard
  of ikFn:
    for ty in impl.params:
      yield ty

iterator param_pairs*(impl: Impl): (int, Argument) =
  case impl.kind
  of ikCons:
    let ty = impl.ty[].as_qualified
    case ty.inner.kind
    of tykStruct:
      for i, it in ty.inner.struct.fields:
        yield (i, it.toArg)
    of tykUnion:
      for i, it in ty.inner.union.fields:
        yield (i, it.toArg)
    of tykTagged:
      var j = 0
      for i, it in ty.inner.tagged.fields:
        yield (i, it.toArg)
        inc j
      for variant in ty.inner.tagged.variants:
        if variant.name == impl.name:
          for i, it in variant.fields:
            yield (j + i, it.toArg)
          break
    else:
      discard
  of ikVar:
    for ty in impl.ty.sum:
      case ty.inner.kind
      of tykFunction:
        for i, ty in ty.inner.params.pairs:
          yield (i, ty)
      else: discard
  of ikFn:
    for i, ty in impl.params.pairs:
      yield (i, ty)

iterator param_pairs*(impl: ref Impl): (int, Argument) =
  case impl.kind
  of ikCons:
    let ty = impl.ty[].as_qualified
    case ty.inner.kind
    of tykStruct:
      for i, it in ty.inner.struct.fields:
        yield (i, it.toArg)
    of tykUnion:
      for i, it in ty.inner.union.fields:
        yield (i, it.toArg)
    of tykTagged:
      var j = 0
      for i, it in ty.inner.tagged.fields:
        yield (i, it.toArg)
        inc j
      for variant in ty.inner.tagged.variants:
        if variant.name == impl.name:
          for i, it in variant.fields:
            yield (j + i, it.toArg)
          break
    else:
      discard
  of ikVar:
    for ty in impl.ty.sum:
      case ty.inner.kind
      of tykFunction:
        for i, ty in ty.inner.params.pairs:
          yield (i, ty)
      else: discard
  of ikFn:
    for i, ty in impl.params.pairs:
      yield (i, ty)

proc replace_param*(ty: ref QualifiedType, param: string, arg: ref QualifiedType, regen: bool, recurse: var HashSet[uint]) =
  var modified = false

  let typtr = cast[uint](ty)
  if recurse.contains(typtr):
    return
  recurse.incl(typtr)

  let rec = recurse
  recurse = recurse.deepCopy

  for k, ty in ty.args:
    if ty.inner.kind == tykParameter and ty.inner.param.name == param:
      ty[] = arg[]

  recurse = rec

  for param_ptr in ty[].param_ptrs_of:
    case param_ptr[2].kind
    of ppkDone:
      if ty.inner.kind == tykParameter and ty.inner.param.name == param:
        ty[] = arg[]
        modified = true
    of ppkTraitFn:
      for ty in ty.inner.trait.fns[param_ptr[2].fn_idx].ty.sum:
        ty.replace_param(param, arg, regen, recurse)
        modified = true
    of ppkStructField:
      for ty in ty.inner.struct.fields[param_ptr[2].field_idx].ty.sum:
        ty.replace_param(param, arg, regen, recurse)
        modified = true
    of ppkUnionField:
      for ty in ty.inner.union.fields[param_ptr[2].field_idx].ty.sum:
        ty.replace_param(param, arg, regen, recurse)
        modified = true
    of ppkTaggedField:
      for ty in ty.inner.tagged.fields[param_ptr[2].field_idx].ty.sum:
        ty.replace_param(param, arg, regen, recurse)
        modified = true
    of ppkTaggedVField:
      for ty in ty.inner.tagged.variants[param_ptr[2].variant_idx].fields[param_ptr[2].vfield_idx].ty.sum:
        ty.replace_param(param, arg, regen, recurse)
        modified = true
    of ppkFunctionResult:
      for ty in ty.inner.function.result_type.sum:
        ty.replace_param(param, arg, regen, recurse)
        modified = true
    of ppkFunctionParam:
      for ty in ty.inner.function.params[param_ptr[2].param_idx].ty.sum:
        ty.replace_param(param, arg, regen, recurse)
        modified = true
    of ppkPointee:
      for ty in ty.inner.point.pointee.sum:
        ty.replace_param(param, arg, regen, recurse)
        modified = true
    of ppkSliceElem:
      for ty in ty.inner.slice.elem.sum:
        ty.replace_param(param, arg, regen, recurse)
        modified = true
    of ppkArrayElem:
      for ty in ty.inner.arr.elem.sum:
        ty.replace_param(param, arg, regen, recurse)
        modified = true
    of ppkArraySize:
      for ty in ty.inner.arr.size.sum:
        ty.replace_param(param, arg, regen, recurse)
        modified = true
    of ppkVarElem:
      for ty in ty.inner.vararr.elem.sum:
        ty.replace_param(param, arg, regen, recurse)
        modified = true

  if modified and regen:
    ty.regenerate

proc replace_param*(ty: ref QualifiedType, param: string, arg: ref QualifiedType) =
  var recurse = initHashSet[uint]()
  ty.replace_param(param, arg, true, recurse)

proc replace_params*(ty: ref QualifiedType, params: openarray[tuple[param: string, arg: ref QualifiedType]]) =
  for (param, arg) in params:
    var recurse = initHashSet[uint]()
    ty.replace_param(param, arg, false, recurse)
  ty.regenerate

proc replace_params*(sum: var TypeSum, params: openarray[tuple[param: string, arg: ref QualifiedType]]) =
  for ty in sum.sum:
    ty.replace_params(params)

proc find_params*(dest: ref TypeSum, src: ref TypeSum): seq[tuple[param: string, arg: ref QualifiedType]]

proc find_params*(dest: ref QualifiedType, src: ref QualifiedType): seq[tuple[param: string, arg: ref QualifiedType]] =
  var
    dest = dest
    src = src
  result = newSeq[tuple[param: string, arg: ref QualifiedType]]()

  if dest[].is_typed_node:
    dest = dest.inner.struct.fields[1].ty[].as_qualified

  if src[].is_typed_node:
    src = src.inner.struct.fields[1].ty[].as_qualified

  for param_ptr in dest[].param_ptrs_of:
    case param_ptr[2].kind
    of ppkDone:
      if not result.anyIt(it.param == param_ptr[1]):
        result.add((param_ptr[1], src))
    of ppkTraitFn:
      let params = dest.inner.trait.fns[param_ptr[2].fn_idx].ty.find_params(src.inner.trait.fns[param_ptr[2].fn_idx].ty)
      for param in params:
        if not result.anyIt(it.param == param.param):
          result.add(param)
    of ppkStructField:
      let params = dest.inner.struct.fields[param_ptr[2].field_idx].ty.find_params(src.inner.struct.fields[param_ptr[2].field_idx].ty)
      for param in params:
        if not result.anyIt(it.param == param.param):
          result.add(param)
    of ppkUnionField:
      let params = dest.inner.union.fields[param_ptr[2].field_idx].ty.find_params(src.inner.union.fields[param_ptr[2].field_idx].ty)
      for param in params:
        if not result.anyIt(it.param == param.param):
          result.add(param)
    of ppkTaggedField:
      let params = dest.inner.tagged.fields[param_ptr[2].field_idx].ty.find_params(src.inner.tagged.fields[param_ptr[2].field_idx].ty)
      for param in params:
        if not result.anyIt(it.param == param.param):
          result.add(param)
    of ppkTaggedVField:
      let params = dest.inner.tagged.variants[param_ptr[2].variant_idx].fields[param_ptr[2].vfield_idx].ty.find_params(src.inner.tagged.variants[param_ptr[2].variant_idx].fields[param_ptr[2].vfield_idx].ty)
      for param in params:
        if not result.anyIt(it.param == param.param):
          result.add(param)
    of ppkFunctionResult:
      let params = dest.inner.function.result_type.find_params(src.inner.function.result_type)
      for param in params:
        if not result.anyIt(it.param == param.param):
          result.add(param)
    of ppkFunctionParam:
      let params = dest.inner.function.params[param_ptr[2].param_idx].ty.find_params(src.inner.function.params[param_ptr[2].param_idx].ty)
      for param in params:
        if not result.anyIt(it.param == param.param):
          result.add(param)
    of ppkPointee:
      let params = dest.inner.point.pointee.find_params(src.inner.point.pointee)
      for param in params:
        if not result.anyIt(it.param == param.param):
          result.add(param)
    of ppkSliceElem:
      let params = dest.inner.slice.elem.find_params(src.inner.slice.elem)
      for param in params:
        if not result.anyIt(it.param == param.param):
          result.add(param)
    of ppkArrayElem:
      let params = dest.inner.arr.elem.find_params(src.inner.arr.elem)
      for param in params:
        if not result.anyIt(it.param == param.param):
          result.add(param)
    of ppkArraySize:
      let params = dest.inner.arr.size.find_params(src.inner.arr.size)
      for param in params:
        if not result.anyIt(it.param == param.param):
          result.add(param)
    of ppkVarElem:
      let params = dest.inner.vararr.elem.find_params(src.inner.vararr.elem)
      for param in params:
        if not result.anyIt(it.param == param.param):
          result.add(param)

proc find_params*(dest: ref TypeSum, src: ref TypeSum): seq[tuple[param: string, arg: ref QualifiedType]] =
  result = newSeq[tuple[param: string, arg: ref QualifiedType]]()

  for a in dest.sum:
    for b in src.sum:
      if a.matches(b):
        result.add(a.find_params(b))

proc type_arg_count*(ty: QualifiedType): int =
  return ty.args.len

proc toSeq[K, V](args: OrderedTable[K, V]): seq[(K, V)] =
  result = newSeq[(K, V)]()
  for k, v in args:
    result.add((k, v))

proc type_args*(ty: ref QualifiedType): seq[ref QualifiedType] =
  return ty.type_args_named.mapIt(it[1])

proc type_args_named*(ty: ref QualifiedType): seq[(string, ref QualifiedType)] =
  if ty.inner.kind == tykParameter:
    return @[(ty.inner.param.name, ty)]
  else:
    return ty.args.toSeq

# compatibility hack
proc type_args*(ty: QualifiedType): seq[ref QualifiedType] =
  return ty.type_args_named.mapIt(it[1])

# compatibility hack
proc type_args_named*(ty: QualifiedType): seq[(string, ref QualifiedType)] =
  if ty.inner.kind == tykParameter:
    var t: ref QualifiedType
    new t
    t[] = ty
    return @[(ty.inner.param.name, t)]
  else:
    return ty.args.toSeq

proc find_arg*(args: seq[Argument], name: string): Option[(int, Argument)] =
  for i, arg in args.pairs:
    if arg.name.isSome and arg.name.get == name:
      return some((i, arg))

proc find_arg*(args: seq[tuple[name: Option[string], ty: ref QualifiedType]], name: string): Option[(int, tuple[name: Option[string], ty: ref QualifiedType])] =
  for i, arg in args.pairs:
    if arg.name.isSome and arg.name.get == name:
      return some((i, arg))

proc monomorphize*(impl: ref Impl, src: ref TypeSum, args: seq[Argument], type_args_real: Option[seq[tuple[name: Option[string], ty: ref QualifiedType]]]): (TypeSum, seq[tuple[param: string, arg: ref QualifiedType]]) =
  var type_args = newSeq[tuple[param: string, arg: ref QualifiedType]]()
  if type_args_real.isSome:
    var
      dedup = initHashSet[string]()
      j = 0
    let ty = if impl.poly_impl.isSome:
      ty(impl.poly_impl.get)
    else:
      ty(impl)
    for param in ty[].param_ptrs_of:
      if dedup.contains(param.key):
        continue
      dedup.incl(param.key)
      let arg = find_arg(type_args_real.get, param.key)
      if arg.isSome:
        type_args.add((param.key, arg.get[1].ty))
        j = arg.get[0] + 1
      else:
        let arg = type_args_real.get[j]
        type_args.add((param.key, arg.ty))
        inc j
  else:
    for i, param in impl.param_pairs:
      if i < args.len: # this is a hack, FIXME
        type_args.add(param.ty.find_params(args[i].ty))
  if type_args.len > 0:
    result[0] = src[].deepCopy
    result[0].replace_params(type_args)
    result[1] = type_args
  else:
    result[0] = src[]
    result[1] = type_args

proc monomorphize*(impl: ref Impl, src: ref TypeSum, args: seq[Argument]): (TypeSum, seq[tuple[param: string, arg: ref QualifiedType]]) =
  return impl.monomorphize(src, args, none[seq[tuple[name: Option[string], ty: ref QualifiedType]]]())

proc newVarImpl*(name: Path, linkage_name: Option[string], builtin: bool, shadow: bool, global: bool, ty: ref TypeSum): ref Impl =
  new result
  result[] = Impl(
    kind: ikVar,
    name: name,
    linkage_name: linkage_name,
    builtin: builtin,
    shadow: shadow,
    global: global,
    ty: ty,
  )

proc newFnImpl*(name: Path, linkage_name: Option[string], builtin: bool, shadow: bool, global: bool, result_type: ref TypeSum, params: seq[Argument], variadic: bool, nohidden: bool, is_macro: bool): ref Impl =
  new result
  let ampvar = if params.len > 0 and params[^1].ty[].as_qualified[].is_var: some(params[^1].ty[].elem) else: none[ref TypeSum]()
  var polymorphic = false
  if not result_type[].as_qualified.is_monomorphized:
    polymorphic = true
  else:
    for param in params:
      if not param.ty[].as_qualified.is_monomorphized:
        polymorphic = true
        break
  result[] = Impl(
    kind: ikFn,
    name: name,
    linkage_name: linkage_name,
    builtin: builtin,
    shadow: shadow,
    global: global,
    polymorphic: polymorphic,
    result_type: result_type,
    params: params,
    variadic: variadic,
    ampvar: ampvar,
    nohidden: nohidden,
    is_macro: is_macro,
  )

proc newConsImpl*(name: Path, linkage_name: Option[string], builtin: bool, shadow: bool, global: bool, ty: ref TypeSum): ref Impl =
  new result
  var polymorphic = false
  if not ty[].as_qualified.is_monomorphized:
    polymorphic = true
  result[] = Impl(
    kind: ikCons,
    name: name,
    linkage_name: linkage_name,
    builtin: builtin,
    shadow: shadow,
    global: global,
    polymorphic: polymorphic,
    ty: ty,
  )

proc exists*(ctx: TypeContext, name: Path): bool =
  return ctx.types.contains(name)

proc get*(ctx: TypeContext, name: Path): ref QualifiedType =
  return ctx.types[name]

proc kind*(impl: ref Impl): ImplKind = impl.kind

proc ty*(impl: ref Impl): ref TypeSum =
  case impl.kind
  of ikVar, ikCons:
    return impl.ty
  of ikFn:
    let q = newType(Type(kind: tykFunction, function: TypeFunction(result_type: impl.result_type, params: impl.params))).toQualifiedType
    for t in impl.result_type.sum:
      for (k, v) in t.type_args_named:
        q.args.add(k, v)
    for param in impl.params:
      for t in param.ty.sum:
        for (k, v) in t.type_args_named:
          q.args.add(k, v)
    return q.toTypeSumRef

proc ampvar*(impl: ref Impl): Option[ref TypeSum] =
  case impl.kind
  of ikCons:
    return none[ref TypeSum]()
  of ikVar:
    for ty in impl.ty.sum:
      case ty.inner.kind
      of tykFunction:
        if ty.inner.function.params[^1].ty[].as_qualified[].is_var:
          return some(ty.inner.function.params[^1].ty[].as_qualified.elem)
      else: discard
  of ikFn:
    return impl[].ampvar

proc result_type*(impl: ref Impl): ref TypeSum =
  case impl.kind
  of ikCons:
    return impl.ty
  of ikVar:
    new result
    result[] = TypeSum(sum: initHashSet[ref QualifiedType]())
    for ty in impl.ty.sum:
      case ty.inner.kind
      of tykFunction:
        result[].add(ty.inner.result_type[])
      else: discard
  of ikFn:
    return impl.result_type

proc params*(impl: ref Impl): seq[Argument] =
  case impl.kind
  of ikCons:
    return impl.fields
  of ikVar:
    result = newSeq[Argument]()
    for ty in impl.ty.sum:
      case ty.inner.kind
      of tykFunction:
        result.add(ty.inner.params)
      else: discard
  of ikFn:
    return impl.params

proc add*(ctx: TypeContext, name: Path, ty: ref QualifiedType) =
  if ctx.types.contains(name):
    ctx.types[name] = ty
  else:
    ctx.types.add(name, ty)

proc add_impl*(ctx: TypeContext, name: Path, impl: ref Impl) =
  if not ctx.impls[^1].impls.contains(name):
    ctx.impls[^1].impls.add(name, initOrderedSet[ref Impl]())
  ctx.impls[^1].impls[name].incl(impl)

proc add_impl*(ctx: TypeContext, name: Path, impl: ref Impl, at: int) =
  if not ctx.impls[at].impls.contains(name):
    ctx.impls[at].impls.add(name, initOrderedSet[ref Impl]())
  ctx.impls[at].impls[name].incl(impl)

proc add_impl*(ctx: TypeContext, name: Path, impl: ref Impl, at: BackwardsIndex) =
  if not ctx.impls[at].impls.contains(name):
    ctx.impls[at].impls.add(name, initOrderedSet[ref Impl]())
  ctx.impls[at].impls[name].incl(impl)

proc find_impl_inner*(ctx: TypeContext, name: Path, result_type: Option[ref TypeSum], params: Option[seq[Argument]], type_args: Option[seq[tuple[name: Option[string], ty: ref QualifiedType]]], in_macro: bool, typed: bool, coerce: bool): Instance =
  result = Instance(impls: initOrderedSet[ref Impl](), scores: newSeq[int](), type_args: type_args, found_impls: newSeq[ref Impl]())
  var global_only = false
  for i in 1 .. ctx.impls.len:
    let scope = ctx.impls[^i]
    if scope.kind == igkBoundary:
      global_only = true
      continue
    let impls = scope.impls
    if name in impls:
      var shadowed = false
      let impls = impls[name]
      result.found_impls.add(impls.toSeq)
      for impl in impls:
        block loop:
          var
            impl = impl
            score = 0
          if global_only and not impl.global:
            break loop

          case impl.kind
          of ikFn:
            var
              impl_result_type = impl.result_type
              impl_params = impl.params

            if type_args.isSome:
              let
                type_args_in = type_args.get
              let type_params = ty(impl)[].as_qualified.type_args_named
              var
                type_args = newSeq[tuple[param: string, arg: ref QualifiedType]]()
                j = 0
              for (k, v) in type_params:
                block loop:
                  for i, (k2, v2) in type_args_in:
                    if k2.isSome:
                      if k == k2.get:
                        type_args.add((k2.get, v2))
                        j = i + 1
                        break loop
                  if j >= type_args_in.len:
                    break
                  type_args.add((k, type_args_in[j][1]))
                  inc j
              impl_result_type = impl_result_type.deepCopy
              impl_params = impl_params.deepCopy
              impl_result_type[].replace_params(type_args)
              for param in impl_params:
                param.ty[].replace_params(type_args)
              var impl_new: ref Impl
              new impl_new
              impl_new[] = Impl(
                poly_impl: some(impl),
                kind: ikFn,
                name: impl.name,
                linkage_name: impl.linkage_name,
                builtin: impl.builtin,
                shadow: impl.shadow,
                global: impl.global,
                polymorphic: impl.polymorphic,
                result_type: impl_result_type,
                params: impl_params,
                variadic: impl.variadic,
                ampvar: impl.ampvar,
                nohidden: impl.nohidden,
                is_macro: impl.is_macro,
              )
              impl = impl_new

            if result_type.isSome:
              if params.isSome:
                if typed:
                  if matches(impl.result_type, result_type.get):
                    score += 3
                  elif coerce and matches_coerce(impl.result_type, result_type.get):
                    score += 1
                  else:
                    break loop
              else:
                if typed:
                  if matches(ty(impl), result_type.get):
                    score += 2
                  elif coerce and matches_coerce(ty(impl), result_type.get):
                    score += 1
                  else:
                    break loop

            if params.isSome:
              if impl.variadic:
                if impl.params.len > params.get.len:
                  break loop
              elif impl.ampvar.isSome:
                if impl.params.len - 1 > params.get.len:
                  break loop
              else:
                if impl.params.len != params.get.len:
                  break loop

              var j = 0
              let endidx = if impl.ampvar.isSome: 2 else: 1
              for param in impl.params[0 ..^ endidx]:
                if param.name.isSome:
                  let arg = find_arg(params.get, param.name.get)
                  if arg.isSome:
                    if typed:
                      if matches(param.ty, arg.get[1].ty):
                        score += 3
                      elif coerce and matches_coerce(param.ty, arg.get[1].ty):
                        score += 1
                      else:
                        break loop
                    j = arg.get[0]
                    inc j
                  else:
                    if typed:
                      if matches(param.ty, params.get[j].ty):
                        score += 3
                      elif coerce and matches_coerce(param.ty, params.get[j].ty):
                        score += 1
                      else:
                        break loop
                    inc j
                else:
                  if typed:
                    if matches(param.ty, params.get[j].ty):
                      score += 3
                    elif coerce and matches_coerce(param.ty, params.get[j].ty):
                      score += 1
                    else:
                      break loop
                  inc j
              if impl.ampvar.isSome:
                let param = impl.ampvar.get
                for j in impl.params.len - endidx + 1 ..< params.get.len:
                  if typed:
                    if matches(param, params.get[j].ty):
                      score += 3
                    elif coerce and matches_coerce(param, params.get[j].ty):
                      score += 1
                    else:
                      break loop

            if impl.shadow:
              result.impls.clear
              result.scores.setLen(0)
              shadowed = true
          of ikCons:
            var
              impl_cons = impl.ty
              impl_fields = impl.fields

            if type_args.isSome:
              let
                type_args_in = type_args.get
              let type_params = ty(impl)[].as_qualified.type_args_named
              var
                type_args = newSeq[tuple[param: string, arg: ref QualifiedType]]()
                j = 0
              for (k, v) in type_params:
                block loop:
                  for i, (k2, v2) in type_args_in:
                    if k2.isSome:
                      if k == k2.get:
                        type_args.add((k2.get, v2))
                        j = i + 1
                        break loop
                  type_args.add((k, type_args_in[j][1]))
                  inc j
              impl_cons = impl_cons.deepCopy
              impl_fields = impl_fields.deepCopy
              impl_cons[].replace_params(type_args)
              for param in impl_fields:
                param.ty[].replace_params(type_args)
              var impl_new: ref Impl
              new impl_new
              impl_new[] = Impl(
                poly_impl: some(impl),
                kind: ikCons,
                name: impl.name,
                linkage_name: impl.linkage_name,
                builtin: impl.builtin,
                shadow: impl.shadow,
                global: impl.global,
                polymorphic: impl.polymorphic,
                ty: impl_cons,
              )
              impl = impl_new

            if result_type.isSome:
              if params.isSome:
                if typed:
                  if matches(impl.ty, result_type.get):
                    score += 3
                  elif coerce and matches_coerce(impl.ty, result_type.get):
                    score += 1
                  else:
                    break loop
              else:
                if typed:
                  if matches(ty(impl), result_type.get):
                    score += 2
                  elif coerce and matches_coerce(ty(impl), result_type.get):
                    score += 1
                  else:
                    break loop

            if params.isSome:
              if impl.fields.len < params.get.len:
                break loop

              var j = 0
              for param in impl.fields:
                if param.name.isSome:
                  let arg = find_arg(params.get, param.name.get)
                  if arg.isSome:
                    if typed:
                      if matches(param.ty, arg.get[1].ty):
                        score += 3
                      elif coerce and matches_coerce(param.ty, arg.get[1].ty):
                        score += 1
                      else:
                        break loop
                    j = arg.get[0]
                    inc j
                else:
                  if j >= params.get.len:
                    continue
                  if typed:
                    if matches(param.ty, params.get[j].ty):
                      score += 3
                    elif coerce and matches_coerce(param.ty, params.get[j].ty):
                      score += 1
                    else:
                      break loop
                  inc j

            if impl.shadow:
              result.impls.clear
              result.scores.setLen(0)
              shadowed = true
          of ikVar:
            if result_type.isSome:
              if typed:
                if matches(impl.ty, result_type.get):
                  score += 3
                elif coerce and matches_coerce(impl.ty, result_type.get):
                  score += 1
                else:
                  break loop

            if impl.shadow:
              result.impls.clear
              result.scores.setLen(0)
              shadowed = true

          if not result.impls.containsOrIncl(impl):
            result.scores.add(score)
      if shadowed:
        return

proc find_impl*(ctx: TypeContext, name: Path, result_type: Option[ref TypeSum], params: Option[seq[Argument]], in_macro: bool): Instance =
  result = ctx.find_impl_inner(name, result_type, params, none[seq[tuple[name: Option[string], ty: ref QualifiedType]]](), in_macro, true, true)

proc find_impl_no_coerce*(ctx: TypeContext, name: Path, result_type: Option[ref TypeSum], params: Option[seq[Argument]], in_macro: bool): Instance =
  result = ctx.find_impl_inner(name, result_type, params, none[seq[tuple[name: Option[string], ty: ref QualifiedType]]](), in_macro, true, false)

proc find_impl_with_args*(ctx: TypeContext, name: Path, result_type: Option[ref TypeSum], params: Option[seq[Argument]], type_args: seq[tuple[name: Option[string], ty: ref QualifiedType]], in_macro: bool): Instance =
  result = ctx.find_impl_inner(name, result_type, params, some(type_args), in_macro, true, true)

proc find_impl_untyped*(ctx: TypeContext, name: Path, result_type: Option[ref TypeSum], params: Option[seq[Argument]], in_macro: bool): Instance =
  result = ctx.find_impl_inner(name, result_type, params, none[seq[tuple[name: Option[string], ty: ref QualifiedType]]](), in_macro, false, true)

proc find_impl_with_args_untyped*(ctx: TypeContext, name: Path, result_type: Option[ref TypeSum], params: Option[seq[Argument]], type_args: seq[tuple[name: Option[string], ty: ref QualifiedType]], in_macro: bool): Instance =
  result = ctx.find_impl_inner(name, result_type, params, some(type_args), in_macro, false, true)

proc import_with*(ctx: TypeContext, import_name: Path) =
  var
    found = newSeq[(Path, OrderedSet[ref Impl])]()
    name = none[Path]()
  for scope in ctx.impls:
    if scope.kind == igkBoundary:
      continue
    for path, impls in scope.impls:
      if path.starts_with(import_name):
        let trim = if import_name.glob: import_name.path.len else: import_name.path.len - 1
        found.add((path.triml(trim), impls))
  for name, impl in found.items:
    var name = name
    name.full = false
    if not ctx.impls[^1].impls.contains(name):
      ctx.impls[^1].impls.add(name, impl)
    else:
      for impl in impl:
        ctx.impls[^1].impls[name].incl(impl)

proc `$`*(impl: Impl): string =
  case impl.kind
  of ikFn:
    result = if impl.is_macro: "macro " else: "fn "
  of ikCons:
    result = "type "
  of ikVar:
    result = "var "
  result.add($impl.name)
  if impl.linkage_name.isSome:
    result.add(" (a.k.a. ")
    result.add(impl.linkage_name.get)
    result.add(")")
  result.add(": ")
  case impl.kind
  of ikVar:
    result.add($impl.ty[])
  of ikCons:
    result.add($impl.ty[])
    result.add("(")
    for i, ty in impl.param_pairs:
      if i > 0:
        result.add(", ")
      if ty.name.isSome:
        result.add(ty.name.get)
        result.add(": ")
      result.add($ty.ty[])
    result.add(")")
  of ikFn:
    result.add("(")
    for i, ty in impl.params:
      if i > 0:
        result.add(", ")
      if ty.name.isSome:
        result.add(ty.name.get)
        result.add(": ")
      result.add($ty.ty[])
    if impl.params.len == 0 and impl.variadic:
      result.add("...")
    elif impl.variadic:
      result.add(", ...")
    result.add(") -> ")
    result.add($impl.result_type[])

proc get_align_of*(ty: ref QualifiedType, ctx: TypeContext): Sizeof

proc fields*(ty: ref QualifiedType, ctx: TypeContext): seq[StructField] =
  case ty.inner.kind
  of tykStruct:
    return ty.inner.struct.fields
  of tykTagged:
    var bits: uint8
    let align = ty.get_align_of(ctx)
    case align.kind
    of sokUnknown:
      raise newIce("unknown alignment")
    of sokUnsized:
      raise newIce("unsized alignment")
    of sokSized:
      bits = uint8(align.size * 8)
    var fields = ty.inner.tagged.fields
    fields.add(StructField(name: some("tag"), ty: newType(Type(kind: tykUnsigned, unsigned: TypeUnsigned(bits: bits.toBits))).toQualifiedType.toTypeSumRef))
    return fields
  of tykSlice:
    return @[
      StructField(name: some("len"), ty: newType(Type(kind: tykUnsigned, unsigned: TypeUnsigned(bits: Bits(kind: bitsWord)))).toQualifiedType.toTypeSumRef),
      StructField(name: some("ptr"), ty: newType(Type(kind: tykPointer, point: TypePointer(pointee: ty.elem))).toQualifiedType.toTypeSumRef),
    ]
  of tykVar:
    return @[
      StructField(name: some("len"), ty: newType(Type(kind: tykUnsigned, unsigned: TypeUnsigned(bits: Bits(kind: bitsWord)))).toQualifiedType.toTypeSumRef),
    ]
  of tykTrait:
    let unit_ptr = newType(Type(kind: tykPointer, point: TypePointer(pointee: newType(Type(kind: tykUnit, unit: TypeUnit())).toQualifiedType.toTypeSumRef))).toQualifiedType.toTypeSumRef
    return @[
      StructField(name: some("data"), ty: unit_ptr),
      StructField(name: some("vtable"), ty: newType(Type(kind: tykPointer, point: TypePointer(pointee: unit_ptr))).toQualifiedType.toTypeSumRef),
    ]
  else:
    return newSeq[StructField]()

proc is_type(ty: Type): bool =
  return ty.kind != tykLit and ty.kind != tykType and not (ty.kind == tykParameter and ty.param.kind != pkType)

proc is_int(ty: Type): bool =
  return ty.kind == tykLit and ty.lit.kind == lkInt or ty.kind == tykParameter and ty.param.kind != pkInt

proc is_float(ty: Type): bool =
  return ty.kind == tykLit and ty.lit.kind == lkFloat or ty.kind == tykParameter and ty.param.kind != pkFloat

proc is_str(ty: Type): bool =
  return ty.kind == tykLit and ty.lit.kind == lkString or ty.kind == tykParameter and ty.param.kind != pkString

proc is_parameter(ty: Type): bool =
  return ty.kind == tykParameter

proc is_signed(ty: Type): bool =
  return ty.kind == tykSigned

proc is_unsigned(ty: Type): bool =
  return ty.kind == tykUnsigned

proc is_floating(ty: Type): bool =
  return ty.kind == tykFloating

proc is_pointer(ty: Type): bool =
  return ty.kind == tykPointer

proc is_trait(ty: Type): bool =
  return ty.kind == tykTrait

proc is_var*(ty: Type): bool =
  return ty.kind == tykVar

proc is_signed*(ty: QualifiedType): bool =
  return ty.inner[].is_signed

proc is_unsigned*(ty: QualifiedType): bool =
  return ty.inner[].is_unsigned

proc is_floating*(ty: QualifiedType): bool =
  return ty.inner[].is_floating

proc is_pointer*(ty: QualifiedType): bool =
  return ty.inner[].is_pointer

proc is_trait*(ty: QualifiedType): bool =
  return ty.inner[].is_trait

proc is_var*(ty: QualifiedType): bool =
  return ty.inner[].is_var

proc is_parameter*(ty: QualifiedType): bool =
  return ty.inner[].is_parameter

proc is_parameter*(ty: TypeSum): bool =
  for ty in ty.sum:
    if ty.is_parameter:
      return true

proc intval*(ty: ref QualifiedType): int =
  case ty.inner.kind
  of tykLit:
    return ty.inner.lit.intval
  else:
    raise newIce("unimplemented error: non-literal.intval")

proc align_down*(size: uint, align: uint): uint =
  return size div align * align

proc align_up*(size: uint, align: uint): uint =
  if (size and (align - 1'u)) == 0'u:
    return size
  else:
    return size.align_down(align) + align

proc get_align_of*(bits: Bits, ctx: TypeContext): uint =
  case bits.kind
  of bitsWord: return ctx.target.pointer_width div 8
  of bitsCustom: return bits.bits div 8

proc get_align_of*(ty: ref Type, ctx: TypeContext): Alignof =
  case ty.kind
  of tykUnit: return Alignof(kind: sokSized, size: 0)
  of tykNever: return Alignof(kind: sokUnknown)
  of tykBool: return Alignof(kind: sokSized, size: 1)
  of tykSigned: return Alignof(kind: sokSized, size: ty.signed.bits.get_align_of(ctx))
  of tykUnsigned: return Alignof(kind: sokSized, size: ty.unsigned.bits.get_align_of(ctx))
  of tykFloating: return Alignof(kind: sokSized, size: ty.floating.bits.get_align_of(ctx))
  of tykTrait:
    return Alignof(kind: sokSized, size: ctx.target.pointer_width div 8)
  of tykStruct:
    if ty.struct.fields.len > 0:
      if ty.struct.fields[0].ty[].is_qualified:
        return ty.struct.fields[0].ty[].as_qualified.get_align_of(ctx)
      else:
        return Alignof(kind: sokUnknown)
    else:
      return Alignof(kind: sokSized, size: 1)
  of tykUnion:
    var
      max_align = 1'u
    for field in ty.union.fields:
      if field.ty[].is_qualified:
        let a = field.ty[].as_qualified.get_align_of(ctx)
        case a.kind
        of sokUnknown:
          return Alignof(kind: sokUnknown)
        of sokUnsized:
          return Alignof(kind: sokUnsized)
        of sokSized:
          max_align = max(a.size, max_align)
      else:
        return Alignof(kind: sokUnknown)
    return Alignof(kind: sokSized, size: max_align)
  of tykEnum: return ty.enumeration.inner.get_align_of(ctx)
  of tykTagged:
    if ty.tagged.fields.len != 0:
      if ty.tagged.fields[0].ty[].is_qualified:
        return ty.tagged.fields[0].ty[].as_qualified.get_align_of(ctx)
      else:
        return Alignof(kind: sokUnknown)
    var
      max_align = 1'u
    for variant in ty.tagged.variants:
      if variant.fields.len > 0:
        let a = variant.fields[0].ty[].as_qualified.get_align_of(ctx)
        case a.kind
        of sokUnknown:
          return Alignof(kind: sokUnknown)
        of sokUnsized:
          return Alignof(kind: sokUnsized)
        of sokSized:
          max_align = max(a.size, max_align)
    return Alignof(kind: sokSized, size: max_align)
  of tykFunction: return Alignof(kind: sokSized, size: ctx.target.pointer_width div 8)
  of tykPointer: return Alignof(kind: sokSized, size: ctx.target.pointer_width div 8)
  of tykSlice: return Alignof(kind: sokSized, size: ctx.target.pointer_width div 8)
  of tykArray:
    if ty.arr.elem[].is_qualified:
      return ty.arr.elem[].as_qualified.get_align_of(ctx)
    else:
      return Sizeof(kind: sokUnknown)
  of tykVector:
    if ty.vec.elem[].is_qualified:
      return ty.vec.elem[].as_qualified.get_align_of(ctx)
    else:
      return Sizeof(kind: sokUnknown)
  of tykVar: return Alignof(kind: sokSized, size: ctx.target.pointer_width div 8)
  of tykParameter: return Alignof(kind: sokUnknown)
  of tykLit: return Alignof(kind: sokSized, size: 0)
  of tykType: return Alignof(kind: sokUnsized)

proc get_align_of*(ty: ref QualifiedType, ctx: TypeContext): Sizeof =
  if ty.align.isNone:
    ty.align = some(ty.inner.get_align_of(ctx))

  return ty.align.get

proc get_size_of*(ty: ref QualifiedType, ctx: TypeContext): Sizeof

proc get_size_of*(bits: Bits, ctx: TypeContext): uint =
  case bits.kind
  of bitsWord: return ctx.target.pointer_width div 8
  of bitsCustom: return bits.bits div 8

proc get_size_of*(ty: ref Type, ctx: TypeContext): Sizeof =
  case ty.kind
  of tykUnit: return Sizeof(kind: sokSized, size: 0)
  of tykNever: return Sizeof(kind: sokUnknown)
  of tykBool: return Sizeof(kind: sokSized, size: 1)
  of tykSigned: return Sizeof(kind: sokSized, size: ty.signed.bits.get_size_of(ctx))
  of tykUnsigned: return Sizeof(kind: sokSized, size: ty.unsigned.bits.get_size_of(ctx))
  of tykFloating: return Sizeof(kind: sokSized, size: ty.floating.bits.get_size_of(ctx))
  of tykTrait:
    return Sizeof(kind: sokSized, size: 2'u * ctx.target.pointer_width div 8)
  of tykStruct:
    var
      size = 0'u
      max_align = 1'u
    for field in ty.struct.fields:
      if field.ty[].is_qualified:
        let s = field.ty[].as_qualified.get_size_of(ctx)
        case s.kind
        of sokUnknown:
          return Sizeof(kind: sokUnknown)
        of sokUnsized:
          return Sizeof(kind: sokUnsized)
        of sokSized:
          size += s.size
        let a = field.ty[].as_qualified.get_align_of(ctx)
        case a.kind
        of sokUnknown:
          return Sizeof(kind: sokUnknown)
        of sokUnsized:
          return Sizeof(kind: sokUnsized)
        of sokSized:
          size = size.align_up(a.size)
          max_align = max(a.size, max_align)
      else:
        return Sizeof(kind: sokUnknown)
    size = size.align_up(max_align)
    return Sizeof(kind: sokSized, size: size)
  of tykUnion:
    var
      size = 0'u
      max_align = 1'u
    for field in ty.union.fields:
      if field.ty[].is_qualified:
        let s = field.ty[].as_qualified.get_size_of(ctx)
        case s.kind
        of sokUnknown:
          return Sizeof(kind: sokUnknown)
        of sokUnsized:
          return Sizeof(kind: sokUnsized)
        of sokSized:
          size = max(s.size, size)
        let a = field.ty[].as_qualified.get_align_of(ctx)
        case a.kind
        of sokUnknown:
          return Sizeof(kind: sokUnknown)
        of sokUnsized:
          return Sizeof(kind: sokUnsized)
        of sokSized:
          max_align = max(a.size, max_align)
      else:
        return Sizeof(kind: sokUnknown)
    size = size.align_up(max_align)
    return Sizeof(kind: sokSized, size: size)
  of tykEnum: return ty.enumeration.inner.get_size_of(ctx)
  of tykTagged:
    var
      total_size = 0'u
      max_size = 0'u
      max_align = 1'u
    for field in ty.tagged.fields:
      if field.ty[].is_qualified:
        let s = field.ty[].as_qualified.get_size_of(ctx)
        case s.kind
        of sokUnknown:
          return Sizeof(kind: sokUnknown)
        of sokUnsized:
          return Sizeof(kind: sokUnsized)
        of sokSized:
          total_size += s.size
        let a = field.ty[].as_qualified.get_align_of(ctx)
        case a.kind
        of sokUnknown:
          return Sizeof(kind: sokUnknown)
        of sokUnsized:
          return Sizeof(kind: sokUnsized)
        of sokSized:
          total_size = total_size.align_up(a.size)
          max_align = max(a.size, max_align)
      else:
        return Sizeof(kind: sokUnknown)
    total_size = total_size.align_up(max_align)
    max_align = 1'u
    for variant in ty.tagged.variants:
      var
        size = 0'u
        align = 1'u
      for field in variant.fields:
        if field.ty[].is_qualified:
          let s = field.ty[].as_qualified.get_size_of(ctx)
          case s.kind
          of sokUnknown:
            return Sizeof(kind: sokUnknown)
          of sokUnsized:
            return Sizeof(kind: sokUnsized)
          of sokSized:
            size += s.size
          let a = field.ty[].as_qualified.get_align_of(ctx)
          case a.kind
          of sokUnknown:
            return Sizeof(kind: sokUnknown)
          of sokUnsized:
            return Sizeof(kind: sokUnsized)
          of sokSized:
            size = size.align_up(a.size)
            align = max(a.size, align)
        else:
          return Sizeof(kind: sokUnknown)
        max_align = max(align, max_align)
      size = size.align_up(max_align)
      max_size = max_size.max(size)
    max_size = max_size.align_up(max_align)
    return Sizeof(kind: sokSized, size: total_size + max_align + max_size)
  of tykFunction: return Sizeof(kind: sokSized, size: ctx.target.pointer_width div 8)
  of tykPointer: return Sizeof(kind: sokSized, size: ctx.target.pointer_width div 8)
  of tykSlice: return Sizeof(kind: sokSized, size: 2'u * (ctx.target.pointer_width div 8))
  of tykArray:
    if ty.arr.elem[].is_qualified:
      let s = ty.arr.elem[].as_qualified.get_size_of(ctx)
      case s.kind
      of sokUnknown:
        return Sizeof(kind: sokUnknown)
      of sokUnsized:
        return Sizeof(kind: sokUnsized)
      of sokSized:
        return Sizeof(kind: sokSized, size: ty.arr.size[].as_qualified.intval.uint * s.size)
    else:
      return Sizeof(kind: sokUnknown)
  of tykVector:
    if ty.vec.elem[].is_qualified:
      let s = ty.vec.elem[].as_qualified.get_size_of(ctx)
      case s.kind
      of sokUnknown:
        return Sizeof(kind: sokUnknown)
      of sokUnsized:
        return Sizeof(kind: sokUnsized)
      of sokSized:
        return Sizeof(kind: sokSized, size: ty.vec.size[].as_qualified.intval.uint * s.size)
    else:
      return Sizeof(kind: sokUnknown)
  of tykVar: return Sizeof(kind: sokSized, size: ctx.target.pointer_width div 8)
  of tykParameter: return Sizeof(kind: sokUnknown)
  of tykLit: return Sizeof(kind: sokSized, size: 0)
  of tykType: return Sizeof(kind: sokUnsized)

proc get_size_of*(ty: ref QualifiedType, ctx: TypeContext): Sizeof =
  if ty.size.isNone:
    ty.size = some(ty.inner.get_size_of(ctx))

  return ty.size.get

proc get_tagged_tag_size*(ty: TypeTagged, ctx: TypeContext): Sizeof =
  var
    max_align = 1'u
  for variant in ty.variants:
    if variant.fields.len > 0:
      let a = variant.fields[0].ty[].as_qualified.get_align_of(ctx)
      case a.kind
      of sokUnknown:
        return Alignof(kind: sokUnknown)
      of sokUnsized:
        return Alignof(kind: sokUnsized)
      of sokSized:
        max_align = max(a.size, max_align)
  return Alignof(kind: sokSized, size: max_align)

proc get_tagged_union_size*(ty: TypeTagged, ctx: TypeContext): Sizeof =
  var
    max_size = 0'u
    max_align = 1'u
  for variant in ty.variants:
    var
      size = 0'u
      align = 1'u
    for field in variant.fields:
      if field.ty[].is_qualified:
        let s = field.ty[].as_qualified.get_size_of(ctx)
        case s.kind
        of sokUnknown:
          return Sizeof(kind: sokUnknown)
        of sokUnsized:
          return Sizeof(kind: sokUnsized)
        of sokSized:
          size += s.size
        let a = field.ty[].as_qualified.get_align_of(ctx)
        case a.kind
        of sokUnknown:
          return Sizeof(kind: sokUnknown)
        of sokUnsized:
          return Sizeof(kind: sokUnsized)
        of sokSized:
          size = size.align_up(a.size)
          align = max(a.size, align)
      else:
        return Sizeof(kind: sokUnknown)
      max_align = max(align, max_align)
    size = size.align_up(max_align)
    max_size = max_size.max(size)
  return Sizeof(kind: sokSized, size: max_size)

proc eval_preferred*(ctx: TypeContext, sum: ref TypeSum): bool =
  if sum[].is_qualified:
    return true
  if sum.sum.allIt(it in ctx.pref_list):
    for pref in ctx.pref_list:
      if pref in sum.sum.mapIt(it):
        sum.sum.clear
        sum[].add(pref)
        return true

proc eval_array*(ctx: TypeContext, sum: ref TypeSum): bool =
  if sum[].is_qualified:
    return true
  if sum.sum.len == 2:
    for ty in sum.sum:
      if ty.is_array:
        sum.sum.clear
        sum[].add(ty)
        return true

proc to_type_sum_params(params: openarray[tuple[param: string, arg: ref QualifiedType]]): seq[tuple[param: string, arg: ref TypeSum]] =
  result = newSeq[tuple[param: string, arg: ref TypeSum]]()
  for (param, arg) in params:
    block loop:
      for p2 in result:
        if p2.param == param:
          p2.arg[].add(arg)
          break loop
      result.add((param, arg.toTypeSumRef))

proc eval*(ctx: TypeContext, impl: ref Impl, args: seq[Argument], type_args: Option[seq[tuple[name: Option[string], ty: ref QualifiedType]]]) =
  let params = params(impl)
  for i in 0 ..< params.len:
    if i >= args.len: # this is a hack, FIXME
      break
    let param = params[i]
    if args[i].ty[].is_qualified and args[i].ty[].as_qualified.is_monomorphized:
      continue
    if param.ty[].has_type_params:
      if type_args.isSome:
        var
          params = param.ty.find_params(args[i].ty).to_type_sum_params
          params_real = newSeq[tuple[param: string, arg: ref QualifiedType]]()
        for j in 0 ..< params.len:
          let arg = find_arg(type_args.get, params[j].param)
          if arg.isSome:
            params_real.add((params[j].param, arg.get[1].ty))
          else:
            params_real.add((params[j].param, params[j].arg[].as_qualified))
        var param = param.deepCopy
        param.ty[].replace_params(params_real)
        args[i].ty[] = param.ty[]
      else:
        discard ctx.eval_preferred(args[i].ty)
        discard ctx.eval_array(args[i].ty)
    else:
      args[i].ty[] = param.ty[]

proc eval*(ctx: TypeContext, impl: ref Impl, args: seq[Argument]) =
  ctx.eval(impl, args, none[seq[tuple[name: Option[string], ty: ref QualifiedType]]]())

proc join(sums: openarray[TypeSum]): ref TypeSum =
  new result
  result.sum = initHashSet[ref QualifiedType]()
  for sum in sums:
    for ty in sum.sum:
      result.sum.incl(ty)

proc pointee*(sum: TypeSum): ref TypeSum =
  result = newTypeSum().toTypeSumRef
  for ty in sum.sum:
    let t = ty.try_pointee
    if t.isSome:
      result[].add(t.get[])

proc elem*(sum: TypeSum): ref TypeSum =
  result = newTypeSum().toTypeSumRef
  for ty in sum.sum:
    let t = ty.try_elem
    if t.isSome:
      result[].add(t.get[])

proc deref*(sum: TypeSum): ref TypeSum =
  new result
  result.sum = initHashSet[ref QualifiedType]()
  for ty in sum.sum:
    case ty.inner.kind
    of tykPointer:
      result[].add(ty.inner.point.pointee[].deref[])
    else:
      result[].add(ty)

proc tag_idx*(tagged: TypeTagged): int =
  tagged.fields.len

proc eval_sum_in_span*(ctx: TypeContext, sum: ref TypeSum, span: Span): bool

proc eval_type_in_span(ctx: TypeContext, t: ref Type, span: Span): bool =
  case t.kind
  of tykUnit: return true
  of tykNever: return true
  of tykBool: return true
  of tykSigned: return true
  of tykUnsigned: return true
  of tykFloating: return true
  of tykTrait:
    for fn in t.trait.fns:
      if not eval_sum_in_span(ctx, fn.ty, span):
        return false
    return true
  of tykStruct:
    for field in t.struct.fields:
      if not eval_sum_in_span(ctx, field.ty, span):
        return false
    return true
  of tykUnion:
    for field in t.union.fields:
      if not eval_sum_in_span(ctx, field.ty, span):
        return false
    return true
  of tykEnum: return true
  of tykTagged:
    for field in t.tagged.fields:
      if not eval_sum_in_span(ctx, field.ty, span):
        return false

    for variant in t.tagged.variants:
      for field in variant.fields:
        if not eval_sum_in_span(ctx, field.ty, span):
          return false
    return true
  of tykFunction:
    if not eval_sum_in_span(ctx, t.function.result_type, span):
      return false
    for param in t.function.params:
      if not eval_sum_in_span(ctx, param.ty, span):
        return false
    return true
  of tykPointer: return eval_sum_in_span(ctx, t.point.pointee, span)
  of tykSlice: return eval_sum_in_span(ctx, t.slice.elem, span)
  of tykArray: return eval_sum_in_span(ctx, t.arr.elem, span)
  of tykVector: return eval_sum_in_span(ctx, t.vec.elem, span)
  of tykVar: return eval_sum_in_span(ctx, t.vararr.elem, span)
  of tykParameter: raise newError(errkUnspecifiedType, span)
  of tykLit: return true
  of tykType: return true

proc eval_qualified_type_in_span(ctx: TypeContext, t: ref QualifiedType, span: Span): bool =
  result = true
  if not t.is_eval:
    t.is_eval = true
    result = eval_type_in_span(ctx, t.inner, span)

proc try_eval_sum_in_span*(ctx: TypeContext, sum: ref TypeSum, span: Span): bool

proc eval_sum_in_span*(ctx: TypeContext, sum: ref TypeSum, span: Span): bool =
  case sum.sum.len
  of 0:
    raise newError(errkUnspecifiedType, span)
  of 1:
    let ty = sum[].as_qualified_in_span(span)
    if not eval_qualified_type_in_span(ctx, ty, span):
      return false
  else:
    if not try_eval_sum_in_span(ctx, sum, span):
      return false
    let ty = sum[].as_qualified_in_span(span)
    if not eval_qualified_type_in_span(ctx, ty, span):
      return false
  return true

proc try_eval_sum_in_span(ctx: TypeContext, sum: ref TypeSum, span: Span): bool =
  return eval_preferred(ctx, sum) or eval_array(ctx, sum)

proc restrict_sum_in_span*(ctx: TypeContext, target: ref TypeSum, restrict: ref TypeSum, span: Span): TypeSum

proc restrict(ctx: TypeContext, target: ref QualifiedType, restrict: ref QualifiedType, span: Span): Option[ref QualifiedType] =
  if restrict.inner.kind == tykParameter or restrict.inner.kind == tykNever:
    return some(target)

  if restrict == target:
    return some(target)

  case target.inner.kind
  of tykUnit: return none[ref QualifiedType]()
  of tykNever: return some(restrict)
  of tykBool: return none[ref QualifiedType]()
  of tykSigned: return none[ref QualifiedType]()
  of tykUnsigned: return none[ref QualifiedType]()
  of tykFloating: return none[ref QualifiedType]()
  of tykTrait:
    if restrict.inner.kind != tykTrait:
      return none[ref QualifiedType]()

    if restrict.inner.trait.name != target.inner.trait.name:
      return none[ref QualifiedType]()

    var fns = newSeq[StructField]()

    for i in 0 ..< target.inner.trait.fns.len:
      let t = restrict_sum_in_span(ctx, target.inner.trait.fns[i].ty, restrict.inner.trait.fns[i].ty, span).toTypeSumRef
      fns.add(StructField(name: target.inner.trait.fns[i].name, ty: t))

    let ty = newType(Type(kind: tykTrait, trait: TypeTrait(name: target.inner.trait.name, fns: fns))).toQualifiedType
    ty.param_ptrs = target.param_ptrs
    ty.args = target.args.deepCopy
    return some(ty)
  of tykStruct:
    if restrict.inner.kind != tykStruct:
      return none[ref QualifiedType]()

    if restrict.inner.struct.name != target.inner.struct.name:
      return none[ref QualifiedType]()

    var fields = newSeq[StructField]()

    for i in 0 ..< target.inner.struct.fields.len:
      let t = restrict_sum_in_span(ctx, target.inner.struct.fields[i].ty, restrict.inner.struct.fields[i].ty, span).toTypeSumRef
      fields.add(StructField(name: target.inner.struct.fields[i].name, ty: t))

    let ty = newType(Type(kind: tykStruct, struct: TypeStruct(name: target.inner.struct.name, fields: fields))).toQualifiedType
    ty.param_ptrs = target.param_ptrs
    ty.args = target.args.deepCopy
    return some(ty)
  of tykUnion:
    if restrict.inner.kind != tykUnion:
      return none[ref QualifiedType]()

    if restrict.inner.union.name != target.inner.union.name:
      return none[ref QualifiedType]()

    var fields = newSeq[StructField]()

    for i in 0 ..< target.inner.union.fields.len:
      let t = restrict_sum_in_span(ctx, target.inner.union.fields[i].ty, restrict.inner.union.fields[i].ty, span).toTypeSumRef
      fields.add(StructField(name: target.inner.union.fields[i].name, ty: t))

    let ty = newType(Type(kind: tykUnion, union: TypeStruct(name: target.inner.union.name, fields: fields))).toQualifiedType
    ty.param_ptrs = target.param_ptrs
    ty.args = target.args.deepCopy
    return some(ty)
  of tykEnum: return none[ref QualifiedType]()
  of tykTagged:
    if restrict.inner.kind != tykTagged:
      return none[ref QualifiedType]()

    if restrict.inner.tagged.name != target.inner.tagged.name:
      return none[ref QualifiedType]()

    var fields = newSeq[StructField]()
    var variants = newSeq[TaggedVariant]()

    for i in 0 ..< target.inner.tagged.fields.len:
      let t = restrict_sum_in_span(ctx, target.inner.tagged.fields[i].ty, restrict.inner.tagged.fields[i].ty, span).toTypeSumRef
      fields.add(StructField(name: target.inner.tagged.fields[i].name, ty: t))

    for i in 0 ..< target.inner.tagged.variants.len:
      var variant = newSeq[StructField]()
      let
        target_variant = target.inner.tagged.variants[i]
        restrict_variant = restrict.inner.tagged.variants[i]
      for j in 0 ..< target_variant.fields.len:
        let t = restrict_sum_in_span(ctx, target_variant.fields[j].ty, restrict_variant.fields[j].ty, span).toTypeSumRef
        variant.add(StructField(name: target_variant.fields[j].name, ty: t))
      variants.add(TaggedVariant(name: target_variant.name, fields: variant))

    let ty = newType(Type(kind: tykTagged, tagged: TypeTagged(name: target.inner.tagged.name, fields: fields, variants: variants))).toQualifiedType
    ty.param_ptrs = target.param_ptrs
    ty.args = target.args.deepCopy
    return some(ty)
  of tykFunction:
    if restrict.inner.kind != tykFunction:
      return none[ref QualifiedType]()

    if target.inner.function.params.len != restrict.inner.function.params.len:
      return none[ref QualifiedType]()

    let result_type = restrict_sum_in_span(ctx, target.inner.function.result_type, restrict.inner.function.result_type, span).toTypeSumRef

    var params = newSeq[Argument]()

    for i in 0 ..< target.inner.function.params.len:
      let t = restrict_sum_in_span(ctx, target.inner.function.params[i].ty, restrict.inner.function.params[i].ty, span).toTypeSumRef
      params.add(Argument(name: target.inner.function.params[i].name, ty: t))

    let ty = newType(Type(kind: tykFunction, function: TypeFunction(result_type: result_type, params: params))).toQualifiedType
    ty.param_ptrs = target.param_ptrs
    ty.args = target.args.deepCopy
    return some(ty)
  of tykPointer:
    if restrict.inner.kind != tykPointer:
      return none[ref QualifiedType]()

    let pointee = restrict_sum_in_span(ctx, target.inner.point.pointee, restrict.inner.point.pointee, span).toTypeSumRef

    let ty = newType(Type(kind: tykPointer, point: TypePointer(pointee: pointee))).toQualifiedType
    ty.param_ptrs = target.param_ptrs
    ty.args = target.args.deepCopy
    return some(ty)
  of tykSlice:
    if restrict.inner.kind != tykSlice:
      return none[ref QualifiedType]()

    let elem = restrict_sum_in_span(ctx, target.inner.slice.elem, restrict.inner.slice.elem, span).toTypeSumRef

    let ty = newType(Type(kind: tykSlice, slice: TypeSlice(elem: elem))).toQualifiedType
    ty.param_ptrs = target.param_ptrs
    ty.args = target.args.deepCopy
    return some(ty)
  of tykArray:
    if restrict.inner.kind != tykArray:
      return none[ref QualifiedType]()

    let elem = restrict_sum_in_span(ctx, target.inner.arr.elem, restrict.inner.arr.elem, span).toTypeSumRef

    let ty = newType(Type(kind: tykArray, arr: TypeArray(elem: elem))).toQualifiedType
    ty.param_ptrs = target.param_ptrs
    ty.args = target.args.deepCopy
    return some(ty)
  of tykVector:
    if restrict.inner.kind != tykVector:
      return none[ref QualifiedType]()

    let elem = restrict_sum_in_span(ctx, target.inner.vec.elem, restrict.inner.vec.elem, span).toTypeSumRef
    let size = restrict_sum_in_span(ctx, target.inner.vec.size, restrict.inner.vec.size, span).toTypeSumRef

    let ty = newType(Type(kind: tykVector, vec: TypeVector(elem: elem, size: size))).toQualifiedType
    ty.param_ptrs = target.param_ptrs
    ty.args = target.args.deepCopy
    return some(ty)
  of tykVar:
    if restrict.inner.kind != tykVar:
      return none[ref QualifiedType]()

    let elem = restrict_sum_in_span(ctx, target.inner.vararr.elem, restrict.inner.vararr.elem, span).toTypeSumRef

    let ty = newType(Type(kind: tykVar, vararr: TypeVar(elem: elem))).toQualifiedType
    ty.param_ptrs = target.param_ptrs
    ty.args = target.args.deepCopy
    return some(ty)
  of tykParameter: return some(restrict)
  of tykLit: return none[ref QualifiedType]()
  of tykType: return none[ref QualifiedType]()

proc shallow_restrict_sum_in_span(ctx: TypeContext, target: ref TypeSum, restrict: ref TypeSum, span: Span, coerce: bool): TypeSum =
  # using result here instead breaks the function
  # nim compiler bug?
  var res = newTypeSum()

  for target in target.sum:
    for restrict in restrict.sum:
      if target.is_parameter:
        res.add(restrict)
      elif restrict.is_parameter:
        res.add(target)
      elif target.matches(restrict):
        let t = restrict(ctx, target, restrict, span)
        if t.isSome:
          res.add(t.get)
      elif coerce and target.matches_coerce(restrict):
        res.add(target)
  return res

proc restrict_sum_in_span*(ctx: TypeContext, target: ref TypeSum, restrict: ref TypeSum, span: Span): TypeSum =
  result = shallow_restrict_sum_in_span(ctx, target, restrict, span, true)

proc restrict_sum_in_span_no_coerce*(ctx: TypeContext, target: ref TypeSum, restrict: ref TypeSum, span: Span): TypeSum =
  result = shallow_restrict_sum_in_span(ctx, target, restrict, span, false)

proc prod*(ctx: TypeContext, trait: ref QualifiedType, ptr_ty: ref QualifiedType, span: Span): seq[ref Impl] =
  result = newSeq[ref Impl]()
  for it in trait.inner.trait.fns:
    let name = it.name.get.toNonFullPath
    let fn = it.ty[].as_qualified.inner.function
    let result_type = fn.result_type
    var args = fn.params
    args[0].ty = ptr_ty.toTypeSumRef

    let instance = ctx.find_impl_no_coerce(name, some(result_type), some(args), false)
    case instance.impls.len
    of 0:
      var msg = fmt"{name}("
      for i, arg in args.pairs:
        if i > 0:
          msg.add(", ")
        msg.add($arg.ty[])
      msg.add(")")
      raise newError(errkNoImpl, span, msg)
    of 1:
      result.add(instance.impls.toSeq[0])
    else:
      var msg = fmt"{'\n'}{name}("
      for i, arg in args.pairs:
        if i > 0:
          msg.add(", ")
        msg.add($arg.ty[])
      msg.add("):\n")
      for impl in instance.impls:
        msg.add("  ")
        msg.add($impl[])
        msg.add("\n")
      raise newError(errkAmbigousImpl, span, msg)
