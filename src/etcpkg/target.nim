type
  # represents an abstract target by the width of integer types
  Target* = object
    pointer_width*: uint
    c_char_width*: uint
    c_short_width*: uint
    c_int_width*: uint
    c_long_width*: uint
    c_llong_width*: uint

proc newNativeTarget*(): ref Target =
  new result
  result.pointer_width = sizeof(int) * 8
  result.c_char_width = sizeof(cchar) * 8
  result.c_short_width = sizeof(cshort) * 8
  result.c_int_width = sizeof(cint) * 8
  result.c_long_width = sizeof(clong) * 8
  result.c_llong_width = sizeof(clonglong) * 8
