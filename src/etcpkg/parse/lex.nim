import deques
import options
import unicode
import sugar
import sequtils
import strformat
import sets
import ../span
import ../ice
import ../error

type
  Tokens* = Deque[Token]
  TokenKind* = enum
    tokUse
    tokWith
    tokTrait
    tokStruct
    tokUnion
    tokEnum
    tokTagged
    tokType
    tokExtern
    tokStatic
    tokNever
    tokUnit
    tokBool
    tokU8
    tokU16
    tokU32
    tokU64
    tokU128
    tokS8
    tokS16
    tokS32
    tokS64
    tokS128
    tokFloat16
    tokFloat32
    tokFloat64
    tokFloat80
    tokFloat128
    tokUint
    tokSint
    tokFloat
    tokLet
    tokIf
    tokElse
    tokWhile
    tokDo
    tokFor
    tokForeach
    tokIn
    tokBreak
    tokContinue
    tokReturn
    tokFail
    tokDefer
    tokMatch
    tokSizeof
    tokAlignof
    tokCast
    tokFn
    tokMacro
    tokMod
    tokDefine
    tokAnd
    tokOr

    tokDot
    tokDot2
    tokEllipsis
    tokComma
    tokSemicolon
    tokCurlyl
    tokCurlyr
    tokParenl
    tokParenr
    tokSquarel
    tokSquarer
    tokLeft
    tokRight
    tokEqarrow
    tokDasharrow
    tokInc
    tokDec
    tokAmp
    tokStar
    tokPlus
    tokDash
    tokTilde
    tokExclam
    tokSlash
    tokPercent
    tokLl
    tokRr
    tokLeq
    tokGeq
    tokEeq
    tokNeq
    tokCaret
    tokPipe
    tokQuestion
    tokColon
    tokColon2
    tokEqual
    tokStareq
    tokSlasheq
    tokPereq
    tokPluseq
    tokDasheq
    tokLleq
    tokRreq
    tokAmpeq
    tokCareteq
    tokPipeeq
    tokPound
    tokBacktick

    tokTrue
    tokFalse
    tokIdent
    tokHexadecimallit
    tokDecimallit
    tokOctallit
    tokBinarylit
    tokFloatlit
    tokCharlit
    tokStrlit

    tokEof
  Token* = object
    kind*: TokenKind
    span*: Span
  Lexer = object
    directory, file, src: string
    i, position: int
    line, col: int

const
  ident_begin = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_".toRunes.toSet
  ident_cont = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_0123456789".toRunes.toSet
  keywords = @[
    (tokUse, "use".toRunes),
    (tokWith, "with".toRunes),
    (tokTrait, "trait".toRunes),
    (tokStruct, "struct".toRunes),
    (tokUnion, "union".toRunes),
    (tokEnum, "enum".toRunes),
    (tokTagged, "tagged".toRunes),
    (tokType, "type".toRunes),
    (tokExtern, "extern".toRunes),
    (tokStatic, "static".toRunes),
    (tokUnit, "never".toRunes),
    (tokUnit, "unit".toRunes),
    (tokBool, "bool".toRunes),
    (tokU8, "u8".toRunes),
    (tokU16, "u16".toRunes),
    (tokU32, "u32".toRunes),
    (tokU64, "u64".toRunes),
    (tokU128, "u128".toRunes),
    (tokS8, "s8".toRunes),
    (tokS16, "s16".toRunes),
    (tokS32, "s32".toRunes),
    (tokS64, "s64".toRunes),
    (tokS128, "s128".toRunes),
    (tokFloat16, "float16".toRunes),
    (tokFloat32, "float32".toRunes),
    (tokFloat64, "float64".toRunes),
    (tokFloat80, "float80".toRunes),
    (tokFloat128, "float128".toRunes),
    (tokFloat, "float".toRunes),
    (tokUint, "uint".toRunes),
    (tokSint, "sint".toRunes),
    (tokLet, "let".toRunes),
    (tokIf, "if".toRunes),
    (tokElse, "else".toRunes),
    (tokWhile, "while".toRunes),
    (tokDo, "do".toRunes),
    (tokFor, "for".toRunes),
    (tokForeach, "foreach".toRunes),
    (tokIn, "in".toRunes),
    (tokBreak, "break".toRunes),
    (tokContinue, "continue".toRunes),
    (tokReturn, "return".toRunes),
    (tokFail, "fail".toRunes),
    (tokDefer, "defer".toRunes),
    (tokMatch, "match".toRunes),
    (tokSizeof, "sizeof".toRunes),
    (tokAlignof, "alignof".toRunes),
    (tokCast, "cast".toRunes),
    (tokFn, "fn".toRunes),
    (tokMacro, "macro".toRunes),
    (tokMod, "mod".toRunes),
    (tokDefine, "define".toRunes),
    (tokAnd, "and".toRunes),
    (tokOr, "or".toRunes),
    (tokFalse, "false".toRunes),
    (tokTrue, "true".toRunes),
  ]
  symbols = @[
    (tokDot, ".".toRunes),
    (tokDot2, "..".toRunes),
    (tokEllipsis, "...".toRunes),
    (tokComma, ",".toRunes),
    (tokSemicolon, ";".toRunes),
    (tokCurlyl, "{".toRunes),
    (tokCurlyr, "}".toRunes),
    (tokParenl, "(".toRunes),
    (tokParenr, ")".toRunes),
    (tokSquarel, "[".toRunes),
    (tokSquarer, "]".toRunes),
    (tokLeft, "<".toRunes),
    (tokRight, ">".toRunes),
    (tokEqarrow, "=>".toRunes),
    (tokDasharrow, "->".toRunes),
    (tokInc, "++".toRunes),
    (tokDec, "--".toRunes),
    (tokAmp, "&".toRunes),
    (tokStar, "*".toRunes),
    (tokPlus, "+".toRunes),
    (tokDash, "-".toRunes),
    (tokTilde, "~".toRunes),
    (tokExclam, "!".toRunes),
    (tokSlash, "/".toRunes),
    (tokPercent, "%".toRunes),
    (tokLl, "<<".toRunes),
    (tokRr, ">>".toRunes),
    (tokLeq, "<=".toRunes),
    (tokGeq, ">=".toRunes),
    (tokEeq, "==".toRunes),
    (tokNeq, "!=".toRunes),
    (tokCaret, "^".toRunes),
    (tokPipe, "|".toRunes),
    (tokQuestion, "?".toRunes),
    (tokColon, ":".toRunes),
    (tokColon2, "::".toRunes),
    (tokEqual, "=".toRunes),
    (tokStareq, "*=".toRunes),
    (tokSlasheq, "/=".toRunes),
    (tokPereq, "%=".toRunes),
    (tokPluseq, "+=".toRunes),
    (tokDasheq, "-=".toRunes),
    (tokLleq, "<<=".toRunes),
    (tokRreq, ">>=".toRunes),
    (tokAmpeq, "&=".toRunes),
    (tokCareteq, "^=".toRunes),
    (tokPipeeq, "|=".toRunes),
    (tokPound, "#".toRunes),
    (tokBacktick, "`".toRunes),
  ]

proc match_ident(lex: var Lexer, runes: seq[Rune]): seq[tuple[len: int, tok: Token]] =
  let
    position = lex.position
    line = lex.line
    col = lex.col
  var
    i = 0
    len = 0
  case runes[lex.i]
  of Rune('A') .. Rune('Z'), Rune('a') .. Rune('z'), Rune('_'):
    inc i
    len += runes[lex.i].size
    lex.position += runes[lex.i].size
    inc lex.i
    inc lex.col
  else:
    raise newError(errkUnexpectedCharacter, Span(directory: lex.directory, file: lex.file, src: lex.src, pos: lex.position, len: runes[lex.i - 1].size, line: lex.line, col: lex.col), "expected 'A'..'Z', 'a'..'z', '_'")
  for rune in runes[lex.i ..< runes.len]:
    case rune
    of Rune('A') .. Rune('Z'), Rune('a') .. Rune('z'), Rune('0') .. Rune('9'), Rune('_'):
      inc i
      len += rune.size
      lex.position += rune.size
      inc lex.i
      inc lex.col
    else:
      break
  return @[(i, Token(kind: tokIdent, span: Span(directory: lex.directory, file: lex.file, src: lex.src, pos: position, len: len, line: line, col: col)))]

proc match_keyword(lex: var Lexer, runes: seq[Rune], keywords: seq[tuple[tok: TokenKind, keyword: seq[Rune]]]): seq[tuple[len: int, tok: Token]] =
  let
    position = lex.position
    line = lex.line
    col = lex.col
  var
    keywords = keywords
    lex_pos = lex.position
    lex_i = lex.i
    lex_col = lex.col
    i = 0
    len = 0
  while keywords.len > 1:
    if runes[lex.i].isWhiteSpace:
      keywords.keepIf((pair) => pair.keyword.len == i)
      break
    block:
      let j = lex.i
      keywords.keepIf((pair) => pair.keyword.len > i and pair.keyword[i] == runes[j])
      for pair in keywords:
        if lex_i + pair.keyword.len <= runes.len and pair.keyword == runes[lex_i ..< lex_i + pair.keyword.len] and runes[lex_i + pair.keyword.len] notin ident_cont:
          lex.col = lex_col + pair.keyword.len
          lex.i = lex_i + pair.keyword.len
          lex.position = lex_pos + pair.keyword.len
          return @[(pair.keyword.len, Token(kind: pair.tok, span: Span(directory: lex.directory, file: lex.file, src: lex.src, pos: position, len: pair.keyword.len, line: line, col: col)))]
    inc i
    len += runes[lex.i].size
    lex.position += runes[lex.i].size
    inc lex.i
    inc lex.col
  if keywords.len == 0:
    lex.position = lex_pos
    lex.i = lex_i
    lex.col = lex_col
    return match_ident(lex, runes)
  else:
    if lex_i + keywords[0].keyword.len < runes.len and
        keywords[0].keyword == runes[lex_i ..< lex_i + keywords[0].keyword.len]:
      if runes[lex_i + keywords[0].keyword.len] in ident_cont:
        lex.position = lex_pos
        lex.i = lex_i
        lex.col = lex_col
        return match_ident(lex, runes)
      lex.col = lex_col + keywords[0].keyword.len
      lex.i = lex_i + keywords[0].keyword.len
      lex.position = lex_pos + keywords[0].keyword.len
      return @[(keywords[0].keyword.len, Token(kind: keywords[0].tok, span: Span(directory: lex.directory, file: lex.file, src: lex.src, pos: position, len: keywords[0].keyword.len, line: line, col: col)))]
    else:
      lex.position = lex_pos
      lex.i = lex_i
      lex.col = lex_col
      return match_ident(lex, runes)

proc match_symbol(lex: var Lexer, runes: seq[Rune], keywords: seq[tuple[tok: TokenKind, keyword: seq[Rune]]]): seq[tuple[len: int, tok: Token]] =
  let
    position = lex.position
    line = lex.line
    col = lex.col
  var
    keywords = keywords
    lex_pos = lex.position
    lex_i = lex.i
    lex_col = lex.col
    i = 0
    len = 0
    backup = none[tuple[tok: TokenKind, keyword: seq[Rune]]]()
  while keywords.len > 1:
    case runes[lex.i]:
    of Rune('A') .. Rune('Z'), Rune('a') .. Rune('z'), Rune('0') .. Rune('9'), Rune('_'):
      keywords.keepIf((pair) => pair.keyword.len == i)
      break
    else:
      if runes[lex.i].isWhiteSpace:
        keywords.keepIf((pair) => pair.keyword.len == i)
        break
    for j, pair in keywords.pairs:
      if pair.keyword == runes[lex_i .. lex_i + i]:
        backup = some[tuple[tok: TokenKind, keyword: seq[Rune]]](pair)
    block:
      let j = lex.i
      keywords.keepIf((pair) => i < pair.keyword.len and pair.keyword[i] == runes[j])
    inc i
    len += runes[lex.i].size
    lex.position += runes[lex.i].size
    inc lex.i
    inc lex.col
  if keywords.len == 0:
    if backup.isSome:
      lex.col = lex_col + backup.get.keyword.len
      lex.i = lex_i + backup.get.keyword.len
      lex.position = lex_pos + backup.get.keyword.len
      return @[(backup.get.keyword.len, Token(kind: backup.get.tok, span: Span(directory: lex.directory, file: lex.file, src: lex.src, pos: position, len: backup.get.keyword.len, line: line, col: col)))]
    else:
      raise newError(errkUnexpectedCharacter, Span(directory: lex.directory, file: lex.file, src: lex.src, pos: lex.position - 1, len: runes[lex.i - 1].size, line: lex.line, col: lex.col), "expected '!', '#', '%', '&', '('..'/', ':'..'?', '['..'^', '{'..'~'")
  elif keywords.len == 1:
    let kw = keywords[0]
    if kw.keyword == runes[lex_i ..< lex_i + kw.keyword.len]:
      lex.col = lex_col + backup.get.keyword.len
      lex.i = lex_i + kw.keyword.len
      lex.position = lex_pos + kw.keyword.len
      return @[(kw.keyword.len, Token(kind: kw.tok, span: Span(directory: lex.directory, file: lex.file, src: lex.src, pos: position, len: kw.keyword.len, line: line, col: col)))]
    else:
      raise newError(errkUnexpectedCharacter, Span(directory: lex.directory, file: lex.file, src: lex.src, pos: lex.position - 1, len: runes[lex.i - 1].size, line: lex.line, col: lex.col), "expected '!', '#', '%', '&', '('..'/', ':'..'?', '['..'^', '{'..'~'")
  else:
    raise newIce("ambigous symbol")

proc match_number(lex: var Lexer, runes: seq[Rune], tok: TokenKind, prefix: Option[Rune], base: int, allow_float: bool): seq[tuple[len: int, tok: Token]] =
  let
    position = lex.position
    line = lex.line
    col = lex.col
  var
    i = 0
    len = 0
    tok = tok
  if prefix.isSome:
    case runes[lex.i]
    of Rune('0'):
      inc i
      len += runes[lex.i].size
      lex.position += runes[lex.i].size
      inc lex.i
      inc lex.col
      if runes[lex.i] == prefix.get:
        inc i
        len += runes[lex.i].size
        lex.position += runes[lex.i].size
        inc lex.i
        inc lex.col
      else:
        raise newError(errkUnexpectedCharacter, Span(directory: lex.directory, file: lex.file, src: lex.src, pos: lex.position, len: runes[lex.i].size, line: lex.line, col: lex.col), fmt"expected '{prefix.get}'")
    else:
      raise newError(errkUnexpectedCharacter, Span(directory: lex.directory, file: lex.file, src: lex.src, pos: lex.position, len: runes[lex.i].size, line: lex.line, col: lex.col), "expected '0'")
  for j, rune in runes[lex.i ..< runes.len].pairs:
    case rune
    of Rune('0') .. Rune('9'), Rune('a') .. Rune('f'), Rune('A') .. Rune('F'):
      case base
      of 2:
        if rune.char notin '0' .. '1':
          raise newError(errkUnexpectedCharacter, Span(directory: lex.directory, file: lex.file, src: lex.src, pos: lex.position, len: runes[lex.i].size, line: lex.line, col: lex.col), "expected a <binary literal>")
      of 8:
        if rune.char notin '0' .. '7':
          raise newError(errkUnexpectedCharacter, Span(directory: lex.directory, file: lex.file, src: lex.src, pos: lex.position, len: runes[lex.i].size, line: lex.line, col: lex.col), "expected an <octal literal>")
      of 10:
        if rune.char notin '0' .. '9':
          raise newError(errkUnexpectedCharacter, Span(directory: lex.directory, file: lex.file, src: lex.src, pos: lex.position, len: runes[lex.i].size, line: lex.line, col: lex.col), "expected a <decimal literal>")
      of 16:
        if rune.char notin {'0' .. '9', 'a' .. 'f', 'A' .. 'F'}:
          raise newError(errkUnexpectedCharacter, Span(directory: lex.directory, file: lex.file, src: lex.src, pos: lex.position, len: runes[lex.i].size, line: lex.line, col: lex.col), "expected a <hexadecimal literal>")
      else:
        raise newIce("invalid integer base")
      inc i
      len += runes[lex.i].size
      lex.position += runes[lex.i].size
      inc lex.i
      inc lex.col
    of Rune('.'):
      if runes[lex.i + j] == Rune('.'):
        lex.i += 2
        lex.col += 2
        lex.position += 2
        return @[
          (i, Token(kind: tok, span: Span(directory: lex.directory, file: lex.file, src: lex.src, pos: position, len: len, line: line, col: col))),
          (2, Token(kind: tokDot2, span: Span(directory: lex.directory, file: lex.file, src: lex.src, pos: position + len, len: 2, line: line, col: col + len))),
        ]
      elif tok == tokFloatlit:
        raise newError(errkUnexpectedCharacter, Span(directory: lex.directory, file: lex.file, src: lex.src, pos: lex.position, len: runes[lex.i].size, line: lex.line, col: lex.col), "this <float literal> already has a decimal point")
      else:
        inc i
        len += runes[lex.i].size
        lex.position += runes[lex.i].size
        inc lex.i
        inc lex.col
        if allow_float:
          tok = tokFloatlit
        else:
          raise newError(errkUnexpectedCharacter, Span(directory: lex.directory, file: lex.file, src: lex.src, pos: lex.position, len: runes[lex.i].size, line: lex.line, col: lex.col), "expected <integer literal>")
    of Rune('G') .. Rune('Z'), Rune('g') .. Rune('z'), Rune('_'):
      raise newError(errkUnexpectedCharacter, Span(directory: lex.directory, file: lex.file, src: lex.src, pos: lex.position, len: runes[lex.i].size, line: lex.line, col: lex.col), "expected <number literal>")
    else:
      break
  return @[(i, Token(kind: tok, span: Span(directory: lex.directory, file: lex.file, src: lex.src, pos: position, len: len, line: line, col: col)))]

proc match_number(lex: var Lexer, runes: seq[Rune]): seq[tuple[len: int, tok: Token]] =
  case runes[lex.i]
  of Rune('0'):
    case runes[lex.i + 1]
    of Rune('x'):
      return match_number(lex, runes, tokHexadecimallit, some(Rune('x')), 16, false)
    of Rune('d'):
      return match_number(lex, runes, tokDecimallit, some(Rune('d')), 10, false)
    of Rune('o'):
      return match_number(lex, runes, tokOctallit, some(Rune('o')), 8, false)
    of Rune('b'):
      return match_number(lex, runes, tokBinarylit, some(Rune('b')), 2, false)
    else: return match_number(lex, runes, tokDecimallit, none[Rune](), 10, true)
  else: return match_number(lex, runes, tokDecimallit, none[Rune](), 10, true)

proc match_char_or_string(lex: var Lexer, runes: seq[Rune], seperator: Rune, tok: TokenKind): seq[tuple[len: int, tok: Token]] =
  const hex = {'0' .. '9', 'a' .. 'f', 'A' .. 'F'}

  if runes[lex.i] != seperator:
    raise newError(errkUnexpectedCharacter, Span(directory: lex.directory, file: lex.file, src: lex.src, pos: lex.position, len: runes[lex.i].size, line: lex.line, col: lex.col), fmt"expected `{seperator}`")

  let
    position = lex.position
    line = lex.line
    col = lex.col
  var
    i = 0
    len = 0

  len += runes[lex.i].size
  inc i

  lex.position += runes[lex.i].size
  inc lex.i
  inc lex.col

  while true:
    if lex.i >= runes.len:
      raise newError(errkUnexpectedCharacter, Span(directory: lex.directory, file: lex.file, src: lex.src, pos: lex.position - 1, len: runes[lex.i - 1].size, line: lex.line, col: lex.col), fmt"expected `{seperator}`")

    case runes[lex.i]
    of Rune('\\'):
      case runes[lex.i + 1]
      of Rune('e'):
        i += 2
        len += 2
        lex.i += 2
        lex.position += 2
        lex.col += 2
      of Rune('n'):
        i += 2
        len += 2
        lex.i += 2
        lex.position += 2
        lex.col += 2
      of Rune('t'):
        i += 2
        len += 2
        lex.i += 2
        lex.position += 2
        lex.col += 2
      of Rune('\\'):
        i += 2
        len += 2
        lex.i += 2
        lex.position += 2
        lex.col += 2
      of Rune('\''):
        i += 2
        len += 2
        lex.i += 2
        lex.position += 2
        lex.col += 2
      of Rune('"'):
        i += 2
        len += 2
        lex.i += 2
        lex.position += 2
        lex.col += 2
      of Rune('0') .. Rune('7'):
        i += 2
        len += 2
        lex.i += 2
        lex.position += 2
        lex.col += 2
        var j = 2
        while j > 0 and runes[lex.i].char in '0' .. '7':
          inc i
          inc len
          inc lex.i
          inc lex.position
          inc lex.col
          dec j
      of Rune('x'):
        if runes[lex.i + 2].char in hex and runes[lex.i + 3].char in hex:
          i += 4
          len += 4
          lex.i += 4
          lex.position += 4
          lex.col += 4
        else:
          raise newError(errkUnexpectedCharacter, Span(directory: lex.directory, file: lex.file, src: lex.src, pos: lex.position, len: 1 + runes[lex.i + 1].size, line: lex.line, col: lex.col), fmt"""expected one of: \e, \n, \t, \\, \', \", \ooo, \xhh, \uhhhh, \Uhhhhhhhh""")
      of Rune('u'):
        if runes[lex.i + 2].char in hex and runes[lex.i + 3].char in hex and runes[lex.i + 4].char in hex and runes[lex.i + 5].char in hex:
          i += 6
          len += 6
          lex.i += 6
          lex.position += 6
          lex.col += 6
        else:
          raise newError(errkUnexpectedCharacter, Span(directory: lex.directory, file: lex.file, src: lex.src, pos: lex.position, len: 1 + runes[lex.i + 1].size, line: lex.line, col: lex.col), fmt"""expected one of: \e, \n, \t, \\, \', \", \ooo, \xhh, \uhhhh, \Uhhhhhhhh""")
      of Rune('U'):
        if runes[lex.i + 2].char in hex and runes[lex.i + 3].char in hex and runes[lex.i + 4].char in hex and runes[lex.i + 5].char in hex and
            runes[lex.i + 6].char in hex and runes[lex.i + 7].char in hex and runes[lex.i + 8].char in hex and runes[lex.i + 9].char in hex:
          i += 10
          len += 10
          lex.i += 10
          lex.position += 10
          lex.col += 10
        else:
          raise newError(errkUnexpectedCharacter, Span(directory: lex.directory, file: lex.file, src: lex.src, pos: lex.position, len: 1 + runes[lex.i + 1].size, line: lex.line, col: lex.col), fmt"""expected one of: \e, \n, \t, \\, \', \", \ooo, \xhh, \uhhhh, \Uhhhhhhhh""")
      else:
        raise newError(errkUnexpectedCharacter, Span(directory: lex.directory, file: lex.file, src: lex.src, pos: lex.position, len: 1 + runes[lex.i + 1].size, line: lex.line, col: lex.col), fmt"""expected one of: \e, \n, \t, \\, \', \", \ooo, \xhh, \uhhhh, \Uhhhhhhhh""")
    else:
      if lex.i >= runes.len:
        raise newError(errkUnexpectedCharacter, Span(directory: lex.directory, file: lex.file, src: lex.src, pos: lex.position - 1, len: runes[lex.i - 1].size, line: lex.line, col: lex.col), fmt"expected `{seperator}`")

      if runes[lex.i] == seperator:
        len += runes[lex.i].size
        inc i
        lex.position += runes[lex.i].size
        inc lex.i
        inc lex.col
        break

      len += runes[lex.i].size
      inc i
      lex.position += runes[lex.i].size
      inc lex.i
      inc lex.col

  return @[(i, Token(kind: tok, span: Span(directory: lex.directory, file: lex.file, src: lex.src, pos: position, len: len, line: line, col: col)))]

proc match_and_add(lex: var Lexer, runes: seq[Rune], matcher: proc (lex: var Lexer, runes: seq[Rune]): seq[tuple[len: int, tok: Token]], tokens: var Deque[Token]) =
  let tok = matcher(lex, runes)
  for tok in tok:
    tokens.addLast(tok.tok)

proc lex*(directory, file, src: string): Tokens =
  result = initDeque[Token](1)
  let runes = src.toRunes
  var
    lex = Lexer(directory: directory, file: file, src: src, position: 0, i: 0, line: 1, col: 1)
  while lex.i < runes.len:
    while lex.i < runes.len and runes[lex.i].isWhiteSpace:
      if runes[lex.i] == Rune('\n'):
        inc lex.line
        lex.col = 1
      else:
        inc lex.col
      lex.position += runes[lex.i].size
      inc lex.i

    if lex.i >= runes.len:
      break

    if runes[lex.i] == Rune('/') and runes[lex.i + 1] == Rune('/'):
      lex.i += 2
      lex.position += 2
      lex.col += 2
      while true:
        if runes[lex.i] == Rune('\n'):
          inc lex.i
          inc lex.position
          inc lex.line
          lex.col = 1
          break
        inc lex.i
        lex.position += runes[lex.i].size
        inc lex.col
      continue
    elif runes[lex.i] == Rune('/') and runes[lex.i + 1] == Rune('*'):
      lex.i += 2
      lex.position += 2
      lex.col += 2
      while true:
        if runes[lex.i] == Rune('*') and runes[lex.i + 1] == Rune('/'):
          lex.i += 2
          lex.col += 2
          lex.position += 2
          break
        inc lex.i
        lex.position += runes[lex.i].size
        inc lex.col
      continue

    case runes[lex.i]
    of Rune('0') .. Rune('9'):
      match_and_add(lex, runes, match_number, result)
    of Rune('A') .. Rune('Z'), Rune('_'):
      match_and_add(lex, runes, match_ident, result)
    of Rune('a') .. Rune('z'):
      match_and_add(lex, runes, (lex: var Lexer, runes: seq[Rune]) => match_keyword(lex, runes, keywords), result)
    of Rune('!'), Rune('#'), Rune('%'), Rune('&'), Rune('(') .. Rune('/'), Rune(':') .. Rune('?'), Rune('[') .. Rune('^'), Rune('{') .. Rune('~'), Rune('`'):
      match_and_add(lex, runes, (lex: var Lexer, runes: seq[Rune]) => match_symbol(lex, runes, symbols), result)
    of Rune('\''):
      match_and_add(lex, runes, (lex: var Lexer, runes: seq[Rune]) => match_char_or_string(lex, runes, Rune('\''), tokCharlit), result)
    of Rune('"'):
      match_and_add(lex, runes, (lex: var Lexer, runes: seq[Rune]) => match_char_or_string(lex, runes, Rune('\"'), tokStrlit), result)
    else:
      raise newError(errkUnexpectedCharacter, Span(directory: lex.directory, file: lex.file, src: lex.src, pos: lex.position, len: runes[lex.i - 1].size, line: lex.line, col: lex.col), "expected 'A'..'Z', 'a'..'z', '0'..'9', '_', '\\'', '\"', '!', '#', '%', '&', '('..'/', ':'..'?', '['..'^', '{'..'~'")
  result.addLast(Token(kind: tokEof, span: Span(directory: lex.directory, file: lex.file, src: lex.src, pos: lex.position, len: 0, line: lex.line, col: lex.col)))
