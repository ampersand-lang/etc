import options
import deques
import sequtils
import sugar
import strutils
import strformat
import tables
import unicode
import ../ice
import ../error
import ../span
import ../bits
import ../path
import ../names
import lex
import ../hlir/hlir

const
  item_first = [
    tokPound, tokMod, tokUse, tokWith, tokStatic, tokDefine, tokFn, tokMacro,
  ]
  literal_first = [
    tokHexadecimallit, tokDecimallit, tokOctallit, tokBinarylit, tokFloatlit, tokTrue, tokFalse, tokColon2, tokIdent,
  ]
  expr_first = [
    # expressions
    tokHexadecimallit, tokDecimallit, tokOctallit, tokBinarylit, tokFloatlit, tokStrlit, tokCharlit, tokTrue, tokFalse,
    tokParenl, tokSquarel, tokCurlyl, tokSizeof, tokAlignof, tokCast,
    tokInc, tokDec, tokPlus, tokDash, tokAmp, tokStar, tokTilde, tokExclam, tokColon,
    tokDefer, tokFail, tokReturn, tokBreak, tokContinue, tokLet,
    tokFn,
    # types
    tokType, tokNever, tokUnit, tokBool,
    tokS8, tokS16, tokS32, tokS64, tokS128, tokSint,
    tokU8, tokU16, tokU32, tokU64, tokU128, tokUint,
    tokFloat16, tokFloat32, tokFloat64, tokFloat80, tokFloat128,
    tokTrait, tokStruct, tokUnion, tokEnum, tokTagged,
    # both
    tokColon2, tokIdent,
  ]
  stmt_first = [
    tokPound, tokMod, tokUse, tokWith, tokStatic, tokDefine, tokFn, tokMacro,
    tokHexadecimallit, tokDecimallit, tokOctallit, tokBinarylit, tokFloatlit, tokStrlit, tokCharlit, tokTrue, tokFalse,
    tokParenl, tokSquarel, tokCurlyl, tokSizeof, tokAlignof, tokCast,
    tokInc, tokDec, tokPlus, tokDash, tokAmp, tokStar, tokTilde, tokExclam, tokColon,
    tokDefer, tokFail, tokReturn, tokBreak, tokContinue, tokLet,
    tokIf, tokFor, tokForeach, tokWhile, tokDo, tokMatch,
    # types
    tokType, tokNever, tokUnit, tokBool,
    tokS8, tokS16, tokS32, tokS64, tokS128, tokSint,
    tokU8, tokU16, tokU32, tokU64, tokU128, tokUint,
    tokFloat16, tokFloat32, tokFloat64, tokFloat80, tokFloat128,
    # both
    tokColon2, tokIdent,
  ]
  pat_first = [
    tokHexadecimallit, tokDecimallit, tokOctallit, tokBinarylit, tokFloatlit, tokStrlit, tokCharlit, tokTrue, tokFalse, tokColon2, tokIdent,
    tokSquarel, tokSizeof, tokAlignof, tokCast,
    tokInc, tokDec, tokPlus, tokDash, tokAmp, tokStar, tokTilde, tokExclam,
    tokElse, tokColon,
  ]

proc parse_root(src: var Tokens): ref Node
proc parse_seq(src: var Tokens, parser: proc (src: var Tokens): ref Node, first: openarray[TokenKind], sep: Option[TokenKind], allow_empty: bool): ref Node
proc parse_with_attributes(src: var Tokens, parser: proc (src: var Tokens): ref Node): ref Node
proc parse_attribute(src: var Tokens): Attribute
proc parse_item(src: var Tokens): ref Node
proc parse_mod(src: var Tokens): ref Node
proc parse_use(src: var Tokens): ref Node
proc parse_with(src: var Tokens): ref Node
proc parse_function(src: var Tokens): ref Node
proc parse_macro(src: var Tokens): ref Node
proc parse_static(src: var Tokens): ref Node
proc parse_define(src: var Tokens): ref Node
proc parse_stmt(src: var Tokens): ref Node
proc parse_stmt_no_semi(src: var Tokens): ref Node
proc parse_stmt_no_semi_with_info(src: var Tokens): (bool, ref Node)
proc parse_expr(src: var Tokens): ref Node
proc parse_expr_str(src: var Tokens): ref Node
proc parse_primary_expr(src: var Tokens): ref Node
proc parse_arrow_expr(src: var Tokens): ref Node
proc parse_type_expr(src: var Tokens): ref Node {.inline.} = parse_arrow_expr(src)
proc parse_pat(src: var Tokens): ref Node
proc parse_decl(src: var Tokens): ref Node
proc parse_path(src: var Tokens): Path
proc parse_path_with_span(src: var Tokens): (Span, Path)
proc parse_ident(src: var Tokens): string
proc parse_ident_with_span(src: var Tokens): (Span, string)

proc parse*(src: var Tokens): ref Node =
  return parse_root(src)

proc parse_root(src: var Tokens): ref Node =
  let sequence = parse_seq(
    src,
    (src: var Tokens) => parse_with_attributes(src, parse_item),
    item_first,
    none[TokenKind](),
    true,
  )
  let tok = src.popFirst
  case tok.kind
  of tokEof: discard
  else:
    raise newError(errkUnexpectedToken, tok.span, "expected <EOF>")
  return newNode(
    sequence.meta,
    Hlir(
      kind: hrkRoot,
      root: HlirRoot(
        root: sequence,
      ),
    ),
  )

proc parse_seq(src: var Tokens, parser: proc (src: var Tokens): ref Node, first: openarray[TokenKind], sep: Option[TokenKind], allow_empty: bool): ref Node =
  var
    span = src.peekFirst.span
    sequence = newSeq[ref Node](0)
  while true:
    if src.peekFirst.kind in first:
      sequence.add(parser(src))
    else:
      if sep.isSome and src.peekFirst.kind == sep.get():
        span += src.popFirst().span
      elif sequence.len > 0:
        span += sequence[sequence.len - 1].meta.span
      else:
        span.len = 0
      break
    if sep.isSome:
      if src.peekFirst.kind == sep.get():
        src.popFirst()
      else:
        span += sequence[sequence.len - 1].meta.span
        break
  return newNode(
    newMeta(span),
    Hlir(
      kind: hrkSeq,
      sequence: HlirSeq(children: sequence),
    ),
  )

proc parse_with_attributes(src: var Tokens, parser: proc (src: var Tokens): ref Node): ref Node =
  var
    tok = src.peekFirst
    span = tok.span
    attrs = newSeq[Attribute]()
  while true:
    tok = src.peekFirst
    case tok.kind
    of tokPound:
      attrs.add(parse_attribute(src))
    else:
      break
  result = parser(src)
  result.meta.attributes = some[seq[Attribute]](attrs)

proc parse_attribute(src: var Tokens): Attribute =
  var tok = src.popFirst
  case tok.kind
  of tokPound: discard
  else: raise newError(errkUnexpectedToken, tok.span, "expected `#`")

  tok = src.popFirst
  case tok.kind
  of tokIdent:
    case tok.span.get
    of "no_mangle":
      result.kind = akNoMangle
    of "comptime":
      result.kind = akComptime
    of "test":
      result.kind = akTest
    of "test_main":
      result.kind = akTestMain
    else:
      raise newError(errkInvalidAttribute, tok.span)
  else: raise newError(errkUnexpectedToken, tok.span, "expected <path>")

proc parse_item(src: var Tokens): ref Node =
  case src.peekFirst.kind
  of tokMod:
    return parse_mod(src)
  of tokUse:
    return parse_use(src)
  of tokWith:
    return parse_with(src)
  of tokFn:
    return parse_function(src)
  of tokMacro:
    return parse_macro(src)
  of tokStatic:
    return parse_static(src)
  of tokDefine:
    return parse_define(src)
  else:
    raise newError(errkUnexpectedToken, src.peekFirst.span, "expected one of: `mod`, `use`, `with`, `fn`, `static`, `define`, `struct`, `union`, `enum`, `tagged` or `typedef`")

proc parse_mod(src: var Tokens): ref Node =
  var
    tok = src.popFirst
    span = tok.span
  case tok.kind
  of tokMod: discard
  else: raise newError(errkUnexpectedToken, tok.span, "expected `mod`")

  let path = parse_path(src)

  tok = src.popFirst
  case tok.kind
  of tokSemicolon: discard
  else: raise newError(errkUnexpectedToken, tok.span, "expected `;`")

  span += tok.span

  return newNode(
    newMeta(span),
    Hlir(
      kind: hrkMod,
      module: HlirMod(path: path),
    ),
  )

proc parse_use(src: var Tokens): ref Node =
  var
    tok = src.popFirst
    span = tok.span
  case tok.kind
  of tokUse: discard
  else: raise newError(errkUnexpectedToken, tok.span, "expected `use`")

  tok = src.peekFirst
  case tok.kind
  of tokExtern:
    src.popFirst

    let lang = parse_expr_str(src)
    let file = parse_expr_str(src)

    tok = src.popFirst
    case tok.kind
    of tokSemicolon: discard
    else: raise newError(errkUnexpectedToken, tok.span, "expected `;`")

    span += tok.span

    return newNode(
      newMeta(span),
      Hlir(
        kind: hrkUseExtern,
        use_extern: HlirUseExtern(lang: lang.node.expr_string.value, file: file.node.expr_string.value),
      ),
    )
  else:
    let path = parse_path(src)

    tok = src.popFirst
    case tok.kind
    of tokSemicolon: discard
    else: raise newError(errkUnexpectedToken, tok.span, "expected `;`")

    span += tok.span

    return newNode(
      newMeta(span),
      Hlir(
        kind: hrkUse,
        use: HlirUse(path: path),
      ),
    )

proc parse_with(src: var Tokens): ref Node =
  var
    tok = src.popFirst
    span = tok.span
  case tok.kind
  of tokWith: discard
  else: raise newError(errkUnexpectedToken, tok.span, "expected `with`")

  let path = parse_path(src)

  tok = src.popFirst
  case tok.kind
  of tokSemicolon: discard
  else: raise newError(errkUnexpectedToken, tok.span, "expected `;`")

  span += tok.span

  return newNode(
    newMeta(span),
    Hlir(
      kind: hrkWith,
      with_single: HlirWith(path: path),
    ),
  )

proc parse_function(src: var Tokens): ref Node =
  var
    tok = src.popFirst
    span = tok.span
  case tok.kind
  of tokFn: discard
  else: raise newError(errkUnexpectedToken, tok.span, "expected `fn`")

  var
    type_params: Option[seq[ref Node]]

  var name: Option[Names]
  let name_node = parse_primary_expr(src)

  case name_node.node.kind
  of hrkExprNamed:
    name_node.node.expr_named.quiet = true
    name = some(name_node.node.expr_named.name)
  of hrkExprInterp:
    name_node.node.expr_interp.quiet = true
  else:
    discard

  tok = src.popFirst
  case tok.kind
  of tokColon:
    discard
  else:
    raise newError(errkUnexpectedToken, tok.span, "expected `:`")

  let ty = parse_type_expr(src)

  tok = src.peekFirst
  case tok.kind
  of tokEqual:
    tok = src.popFirst

    let body = parse_stmt(src)

    span += body.meta.span

    return newNode(
      newMeta(span),
      Hlir(
        kind: hrkFunction,
        function: HlirFunction(
          name_node: some(name_node),
          name: name,
          type_params: type_params,
          ty: ty,
          body: some(body),
        ),
      ),
    )
  of tokSemicolon:
    tok = src.popFirst
    span += tok.span
    return newNode(
      newMeta(span),
      Hlir(
        kind: hrkFunction,
        function: HlirFunction(
          name_node: some(name_node),
          name: name,
          ty: ty,
          body: none[ref Node](),
        ),
      ),
    )
  else:
    raise newError(errkUnexpectedToken, tok.span, "expected `;` or `=`")

proc parse_macro(src: var Tokens): ref Node =
  var
    tok = src.popFirst
    span = tok.span
  case tok.kind
  of tokMacro: discard
  else: raise newError(errkUnexpectedToken, tok.span, "expected `macro`")

  var
    type_params: Option[seq[ref Node]]

  var name: Option[Names]
  let name_node = parse_primary_expr(src)

  case name_node.node.kind
  of hrkExprNamed:
    name_node.node.expr_named.quiet = true
    name = some(name_node.node.expr_named.name)
  of hrkExprInterp:
    name_node.node.expr_interp.quiet = true
  else:
    discard

  tok = src.popFirst
  case tok.kind
  of tokColon:
    discard
  else:
    raise newError(errkUnexpectedToken, tok.span, "expected `:`")

  let ty = parse_type_expr(src)

  tok = src.peekFirst
  case tok.kind
  of tokEqual:
    tok = src.popFirst

    let body = parse_stmt(src)

    span += body.meta.span

    return newNode(
      newMeta(span),
      Hlir(
        kind: hrkMacro,
        macrofn: HlirMacro(
          name_node: some(name_node),
          name: name,
          type_params: type_params,
          ty: ty,
          body: some(body),
        ),
      ),
    )
  else:
    raise newError(errkUnexpectedToken, tok.span, "expected `=`")

proc parse_static(src: var Tokens): ref Node =
  var
    tok = src.popFirst
    span = tok.span
  case tok.kind
  of tokStatic: discard
  else: raise newError(errkUnexpectedToken, tok.span, "expected `static`")

  tok = src.peekFirst
  case tok.kind
  of tokExtern:
    var name: Option[Names]
    let name_node = parse_primary_expr(src)

    case name_node.node.kind
    of hrkExprNamed:
      name_node.node.expr_named.quiet = true
      name = some(name_node.node.expr_named.name)
    of hrkExprInterp:
      name_node.node.expr_interp.quiet = true
    else:
      discard

    tok = src.popFirst
    case tok.kind
    of tokColon: discard
    else: raise newError(errkUnexpectedToken, tok.span, "expected `:`")

    let ty = parse_type_expr(src)

    tok = src.peekFirst
    case tok.kind
    of tokSemicolon:
      tok = src.popFirst
      span += tok.span
      return newNode(
        newMeta(span),
        Hlir(
          kind: hrkStaticExtern,
          static_extern: HlirStaticExtern(
            name_node: name_node,
            name: name,
            ty: ty,
          ),
        ),
      )
    else:
      raise newError(errkUnexpectedToken, tok.span, "expected `=` or `;`")
  of tokIdent:
    var name: Option[Names]
    let name_node = parse_primary_expr(src)

    case name_node.node.kind
    of hrkExprNamed:
      name_node.node.expr_named.quiet = true
      name = some(name_node.node.expr_named.name)
    of hrkExprInterp:
      name_node.node.expr_interp.quiet = true
    else:
      discard

    tok = src.popFirst
    case tok.kind
    of tokColon: discard
    else: raise newError(errkUnexpectedToken, tok.span, "expected `:`")

    let ty = parse_type_expr(src)

    tok = src.peekFirst
    case tok.kind
    of tokEqual:
      tok = src.popFirst

      let expr = parse_expr(src)

      span += expr.meta.span

      return newNode(
        newMeta(span),
        Hlir(
          kind: hrkStatic,
          static_global: HlirStatic(
            name_node: name_node,
            name: name,
            ty: ty,
            value: some(expr),
          ),
        ),
      )
    of tokSemicolon:
      tok = src.popFirst
      span += tok.span
      return newNode(
        newMeta(span),
        Hlir(
          kind: hrkStatic,
          static_global: HlirStatic(
            name_node: name_node,
            name: name,
            ty: ty,
            value: none[ref Node](),
          ),
        ),
      )
    else:
      raise newError(errkUnexpectedToken, tok.span, "expected `=` or `;`")
  else:
    raise newError(errkUnexpectedToken, tok.span, "expected <path> or `extern`")

proc parse_define(src: var Tokens): ref Node =
  var
    tok = src.popFirst
    span = tok.span
  case tok.kind
  of tokDefine: discard
  else: raise newError(errkUnexpectedToken, tok.span, "expected `define`")

  var name: Option[Names]
  let name_node = parse_primary_expr(src)

  case name_node.node.kind
  of hrkExprNamed:
    name_node.node.expr_named.quiet = true
    name = some(name_node.node.expr_named.name)
  of hrkExprInterp:
    name_node.node.expr_interp.quiet = true
  else:
    discard

  var ty: Option[ref Node]
  tok = src.peekFirst
  case tok.kind
  of tokColon:
    src.popFirst
    ty = some(parse_type_expr(src))
  else:
    discard

  tok = src.popFirst
  case tok.kind
  of tokEqual: discard
  else: raise newError(errkUnexpectedToken, tok.span, "expected `=`")

  let expr = parse_expr(src)

  tok = src.popFirst
  case tok.kind
  of tokSemicolon: discard
  else:
    raise newError(errkUnexpectedToken, tok.span, "expected `;`")

  span += tok.span

  return newNode(
    newMeta(span),
    Hlir(
      kind: hrkDefine,
      define: HlirDefine(
        name_node: name_node,
        name: name,
        ty: ty,
        value: expr,
      ),
    ),
  )

proc parse_expr_compound(src: var Tokens): ref Node =
  var
    tok = src.popFirst
    span = tok.span
  case tok.kind
  of tokCurlyl: discard
  else:
      raise newError(errkUnexpectedToken, tok.span, "expected `{`")

  var
    sequence = newSeq[ref Node]()
    last = none[ref Node]()
  while true:
    if src.peekFirst.kind in stmt_first:
      let (semi, stmt) = parse_stmt_no_semi_with_info(src)

      if semi:
        if src.peekFirst.kind == tokSemicolon:
          sequence.add(stmt)
          src.popFirst
        else:
          last = some(stmt)
          break
      else:
        sequence.add(stmt)
    else:
      break

  result = newNode(
    newMeta(span),
    Hlir(
      kind: hrkExprCompound,
      expr_compound: HlirExprCompound(elems: sequence, last: last),
    ),
  )

  tok = src.peekFirst
  case tok.kind
  of tokCurlyr:
    span += src.popFirst.span
    result.meta.span = span
  else:
      raise newError(errkUnexpectedToken, tok.span, "expected `}`")

proc parse_stmt_if(src: var Tokens): ref Node =
  var
    tok = src.popFirst
    span = tok.span
  case tok.kind
  of tokIf: discard
  else:
      raise newError(errkUnexpectedToken, tok.span, "expected `if`")

  let cond = parse_expr(src)
  let body = parse_expr_compound(src)

  span += body.meta.span

  var
    elifs = newSeq[tuple[cond: ref Node, body: ref Node]]()
    el = none[ref Node]()

  while true:
    case src[0].kind
    of tokElse:
      case src[1].kind
      of tokIf:
        tok = src.popFirst
        tok = src.popFirst

        let cond = parse_expr(src)
        let body = parse_expr_compound(src)

        span += body.meta.span
    
        elifs.add((cond, body))
      else:
        tok = src.popFirst
        el = some(parse_expr_compound(src))
        span += el.get.meta.span
        break
    else:
      break

  return newNode(
    newMeta(span),
    Hlir(
      kind: hrkStmtIf,
      stmt_if: HlirStmtIf(cond: cond, body: body, elifs: elifs, el: el),
    )
  )

proc parse_stmt_while(src: var Tokens): ref Node =
  var
    tok = src.popFirst
    span = tok.span
  case tok.kind
  of tokWhile: discard
  else:
      raise newError(errkUnexpectedToken, tok.span, "expected `while`")

  let cond = parse_expr(src)

  let body = parse_expr_compound(src)

  span += body.meta.span

  return newNode(
    newMeta(span),
    Hlir(
      kind: hrkStmtWhile,
      stmt_while: HlirStmtWhile(cond: cond, body: body),
    )
  )

proc parse_stmt_dowhile(src: var Tokens, semi: bool): ref Node =
  var
    tok = src.popFirst
    span = tok.span
  case tok.kind
  of tokDo: discard
  else:
      raise newError(errkUnexpectedToken, tok.span, "expected `do`")

  let body = parse_expr_compound(src)

  tok = src.popFirst
  case tok.kind
  of tokWhile: discard
  else:
      raise newError(errkUnexpectedToken, tok.span, "expected `while`")

  let cond = parse_expr(src)

  if semi:
    tok = src.popFirst
    case tok.kind
    of tokSemicolon: discard
    else:
        raise newError(errkUnexpectedToken, tok.span, "expected `;`")

  span += tok.span

  return newNode(
    newMeta(span),
    Hlir(
      kind: hrkStmtDoWhile,
      stmt_dowhile: HlirStmtDoWhile(body: body, cond: cond),
    )
  )

proc parse_stmt_for(src: var Tokens): ref Node =
  var
    tok = src.popFirst
    span = tok.span
  case tok.kind
  of tokFor: discard
  else:
      raise newError(errkUnexpectedToken, tok.span, "expected `for`")

  var
    init = none[ref Node]()
    cond = none[ref Node]()
    rep = none[ref Node]()

  case src.peekFirst.kind
  of tokSemicolon: discard
  else:
    init = some(parse_expr(src))

  tok = src.popFirst
  case tok.kind
  of tokSemicolon: discard
  else:
      raise newError(errkUnexpectedToken, tok.span, "expected `;`")

  case src.peekFirst.kind
  of tokSemicolon: discard
  else:
    cond = some(parse_expr(src))

  tok = src.popFirst
  case tok.kind
  of tokSemicolon: discard
  else:
      raise newError(errkUnexpectedToken, tok.span, "expected `;`")

  case src.peekFirst.kind
  of tokCurlyr: discard
  else:
    rep = some(parse_expr(src))

  let body = parse_expr_compound(src)

  span += body.meta.span

  return newNode(
    newMeta(span),
    Hlir(
      kind: hrkStmtFor,
      stmt_for: HlirStmtFor(init: init, cond: cond, rep: rep, body: body),
    )
  )

proc parse_stmt_foreach(src: var Tokens): ref Node =
  var
    tok = src.popFirst
    span = tok.span
  case tok.kind
  of tokForeach: discard
  else:
      raise newError(errkUnexpectedToken, tok.span, "expected `foreach`")

  var name: Option[Names]
  let name_node = parse_primary_expr(src)

  case name_node.node.kind
  of hrkExprNamed:
    name_node.node.expr_named.quiet = true
    name = some(name_node.node.expr_named.name)
  of hrkExprInterp:
    name_node.node.expr_interp.quiet = true
  else:
    discard

  var ty = none[ref Node]()

  case src.peekFirst.kind
  of tokIn: discard
  else:
    ty = some(parse_type_expr(src))

  tok = src.popFirst
  case tok.kind
  of tokIn: discard
  else:
    raise newError(errkUnexpectedToken, tok.span, "expected `in`")

  let iter = parse_expr(src)

  let body = parse_expr_compound(src)

  span += body.meta.span

  return newNode(
    newMeta(span),
    Hlir(
      kind: hrkStmtForeach,
      stmt_foreach: HlirStmtForeach(
        name_node: name_node,
        name: name,
        ty: ty,
        iter: iter,
        body: body,
      ),
    )
  )

proc parse_match_case(src: var Tokens): ref Node =
  let pat = parse_pat(src)

  var
    tok = src.popFirst
    span = tok.span
  case tok.kind
  of tokEqarrow: discard
  else:
      raise newError(errkUnexpectedToken, tok.span, "expected `=>`")

  let body = parse_stmt(src)

  return newNode(
    newMeta(pat.meta.span + body.meta.span),
    Hlir(
      kind: hrkCase,
      case_of: HlirCase(pat: pat, body: body),
    )
  )

proc parse_stmt_match(src: var Tokens): ref Node =
  var
    tok = src.popFirst
    span = tok.span
  case tok.kind
  of tokMatch: discard
  else:
      raise newError(errkUnexpectedToken, tok.span, "expected `match`")

  let value = parse_expr(src)

  tok = src.popFirst
  case tok.kind
  of tokCurlyl: discard
  else:
      raise newError(errkUnexpectedToken, tok.span, "expected `{`")

  let cases = parse_seq(
    src,
    parse_match_case,
    pat_first,
    none[TokenKind](),
    true,
  )

  tok = src.popFirst
  case tok.kind
  of tokCurlyr:
    span += tok.span
  else:
      raise newError(errkUnexpectedToken, tok.span, "expected `}`")

  return newNode(
    newMeta(span),
    Hlir(
      kind: hrkStmtMatch,
      stmt_match: HlirStmtMatch(value: value, cases: cases.node.sequence.children),
    )
  )

proc parse_stmt_inner(src: var Tokens, semi: bool): (bool, ref Node) =
  var tok = src.peekFirst

  case tok.kind
  of tokCurlyl:
    result = (false, parse_expr_compound(src))
  of tokIf:
    result = (false, parse_stmt_if(src))
  of tokDo:
    result = (true, parse_stmt_dowhile(src, semi))
  of tokWhile:
    result = (false, parse_stmt_while(src))
  of tokFor:
    result = (false, parse_stmt_for(src))
  of tokForeach:
    result = (false, parse_stmt_foreach(src))
  of tokMatch:
    result = (false, parse_stmt_match(src))
  of tokMod:
    result = (false, parse_mod(src))
  of tokUse:
    result = (false, parse_use(src))
  of tokWith:
    result = (false, parse_with(src))
  of tokFn:
    result = (false, parse_function(src))
  of tokMacro:
    result = (false, parse_macro(src))
  of tokStatic:
    result = (false, parse_static(src))
  of tokDefine:
    result = (false, parse_define(src))
  else:
    result = (not semi, parse_expr(src))

    var semi = semi
    if result[1].node.kind == hrkExprCommand and result[1].node.expr_command.args[^1].node.kind == hrkExprCompound:
      semi = false
      result[0] = true

    if semi:
      tok = src.peekFirst
      case tok.kind
      of tokSemicolon:
        src.popFirst()
      else:
          raise newError(errkUnexpectedToken, tok.span, "expected `;`")

proc parse_stmt_no_semi(src: var Tokens): ref Node = parse_stmt_inner(src, false)[1]
proc parse_stmt_no_semi_with_info(src: var Tokens): (bool, ref Node) = parse_stmt_inner(src, false)
proc parse_stmt(src: var Tokens): ref Node = parse_stmt_inner(src, true)[1]

proc parse_expr_function(src: var Tokens): ref Node =
  var
    tok = src.popFirst
    span = tok.span
  case tok.kind
  of tokFn: discard
  else: raise newError(errkUnexpectedToken, tok.span, "expected `fn`")

  let ty = parse_type_expr(src)

  var capture: Option[seq[ref Node]]

  tok = src.peekFirst
  if tok.kind == tokCurlyl:
    src.popFirst
    capture = some(parse_seq(src, parse_expr, expr_first, some[TokenKind](tokComma), true)
      .node
      .sequence
      .children)
    tok = src.popFirst
    if tok.kind != tokCurlyr:
      raise newError(errkUnexpectedToken, tok.span, "expected `}`")

  tok = src.peekFirst
  case tok.kind
  of tokEqual:
    tok = src.popFirst

    let body = parse_stmt_no_semi(src)

    span += body.meta.span

    return newNode(
      newMeta(span),
      Hlir(
        kind: hrkFunction,
        function: HlirFunction(
          name: none[Names](),
          ty: ty,
          capture: capture,
          is_closure: true,
          body: some(body),
        ),
      ),
    )
  else:
    span += ty.meta.span
    return newNode(
      newMeta(span),
      Hlir(
        kind: hrkFunction,
        function: HlirFunction(
          name: none[Names](),
          ty: ty,
          capture: capture,
          is_closure: true,
          body: none[ref Node](),
        ),
      ),
    )

proc parse_expr_boolean(src: var Tokens): ref Node =
  let tok = src.popFirst
  case tok.kind
  of tokTrue:
    return newNode(
      newMeta(tok.span),
      Hlir(kind: hrkExprBool, expr_bool: HlirExprBool(value: true)),
    )
  of tokFalse:
    return newNode(
      newMeta(tok.span),
      Hlir(kind: hrkExprBool, expr_bool: HlirExprBool(value: false)),
    )
  else:
    raise newError(errkUnexpectedToken, tok.span, "expected <bool literal>")

proc parse_expr_integer(src: var Tokens): ref Node =
  let tok = src.popFirst

  case tok.kind
  of tokHexadecimallit:
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkExprInt,
        expr_int: HlirExprInt(value: tok.span.get[2 ..^ 1].parseHexInt())
      ),
    )
  of tokDecimallit:
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkExprInt,
        expr_int: HlirExprInt(value: if tok.span.get.startsWith("0d"): tok.span.get[2 ..^ 1].parseInt() else: tok.span.get.parseInt())
      ),
    )
  of tokOctallit:
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkExprInt,
        expr_int: HlirExprInt(value: tok.span.get[2 ..^ 1].parseOctInt())
      ),
    )
  of tokBinarylit:
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkExprInt,
        expr_int: HlirExprInt(value: tok.span.get[2 ..^ 1].parseBinInt())
      ),
    )
  else:
    raise newError(errkUnexpectedToken, tok.span, "expected <int literal>")

  if src.peekFirst.kind == tokBacktick:
    src.popFirst
    result.node.expr_int.as_type = some(parse_primary_expr(src))

proc parse_expr_float(src: var Tokens): ref Node =
  let tok = src.popFirst

  case tok.kind
  of tokFloatlit:
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkExprFloat,
        expr_float: HlirExprFloat(value: tok.span.get.parseFloat())
      ),
    )
  else:
    raise newError(errkUnexpectedToken, tok.span, "expected <float literal>")

  if src.peekFirst.kind == tokBacktick:
    src.popFirst
    result.node.expr_float.as_type = some(parse_primary_expr(src))

proc parse_expr_str(src: var Tokens): ref Node =
  type State = enum
    stateNormal
    stateBackslash
    stateOct
    stateHex
    stateUniSmall
    stateUniBig

  let
    tok = src.popFirst
    str = tok.span.get
    runes = str.toRunes
  var
    strval = ""
    state = stateNormal
    ch = 0
    chlen = 0
    i = 1

  while i in 1 ..< str.runeLen - 1:
    let rune = runes[i]
    case state
    of stateNormal:
      case rune
      of Rune('\\'):
        state = stateBackslash
        inc i
      else:
        strval.add(rune)
        inc i
    of stateBackslash:
      case rune
      of Rune('e'):
        state = stateNormal
        strval.add('\e')
        inc i
      of Rune('n'):
        state = stateNormal
        strval.add('\n')
        inc i
      of Rune('t'):
        state = stateNormal
        strval.add('\t')
        inc i
      of Rune('\\'):
        state = stateNormal
        strval.add('\\')
        inc i
      of Rune('\''):
        state = stateNormal
        strval.add('\'')
        inc i
      of Rune('"'):
        state = stateNormal
        strval.add('"')
        inc i
      of Rune('0') .. Rune('7'):
        state = stateOct
        ch = 0
        chlen = 0
        inc i
      of Rune('x'):
        state = stateHex
        ch = 0
        chlen = 0
        inc i
      of Rune('u'):
        state = stateUniSmall
        ch = 0
        chlen = 0
        inc i
      of Rune('U'):
        state = stateUniBig
        ch = 0
        chlen = 0
        inc i
      else:
        discard # unreachable
    of stateOct:
      if chlen == 3:
        strval.add(ch.chr)
        state = stateNormal
      else:
        case rune
        of Rune('0') .. Rune('7'):
          ch *= 8
          ch += rune.int - '0'.int
          inc chlen
          inc i
        else:
          strval.add(ch.chr)
          state = stateNormal
    of stateHex:
      if chlen == 2:
        strval.add(ch.chr)
        state = stateNormal
      else:
        ch *= 16
        case rune
        of Rune('0') .. Rune('9'):
          ch += rune.int - '0'.int
        of Rune('a') .. Rune('a'):
          ch += rune.int - 'a'.int
        of Rune('A') .. Rune('F'):
          ch += rune.int - 'F'.int
        else:
          discard # unreachable
        inc chlen
        inc i
    of stateUniSmall:
      if chlen == 4:
        strval.add(ch.chr)
        state = stateNormal
      else:
        ch *= 16
        case rune
        of Rune('0') .. Rune('9'):
          ch += rune.int - '0'.int
        of Rune('a') .. Rune('a'):
          ch += rune.int - 'a'.int
        of Rune('A') .. Rune('F'):
          ch += rune.int - 'F'.int
        else:
          discard # unreachable
        inc chlen
        inc i
    of stateUniBig:
      if chlen == 8:
        strval.add(ch.chr)
        state = stateNormal
      else:
        ch *= 16
        case rune
        of Rune('0') .. Rune('9'):
          ch += rune.int - '0'.int
        of Rune('a') .. Rune('a'):
          ch += rune.int - 'a'.int
        of Rune('A') .. Rune('F'):
          ch += rune.int - 'F'.int
        else:
          discard # unreachable
        inc chlen
        inc i

  return newNode(
    newMeta(tok.span),
    Hlir(
      kind: hrkExprString,
      expr_string: HlirExprString(value: strval)
    ),
  )

proc parse_expr_char(src: var Tokens): ref Node =
  type State = enum
    stateNormal
    stateBackslash
    stateOct
    stateHex
    stateUniSmall
    stateUniBig

  let
    tok = src.popFirst
    str = tok.span.get
    runes = str.toRunes
  var
    strval = ""
    state = stateNormal
    ch = 0
    chlen = 0
    i = 1

  while i in 1 ..< str.runeLen - 1:
    let rune = runes[i]
    case state
    of stateNormal:
      case rune
      of Rune('\\'):
        state = stateBackslash
        inc i
      else:
        strval.add(rune)
        inc i
    of stateBackslash:
      case rune
      of Rune('e'):
        strval.add('\e')
        inc i
      of Rune('n'):
        strval.add('\n')
        inc i
      of Rune('t'):
        strval.add('\t')
        inc i
      of Rune('\\'):
        strval.add('\\')
        inc i
      of Rune('\''):
        strval.add('\'')
        inc i
      of Rune('"'):
        strval.add('"')
        inc i
      of Rune('0') .. Rune('7'):
        state = stateOct
        ch = 0
        chlen = 0
        inc i
      of Rune('x'):
        state = stateHex
        ch = 0
        chlen = 0
        inc i
      of Rune('u'):
        state = stateUniSmall
        ch = 0
        chlen = 0
        inc i
      of Rune('U'):
        state = stateUniBig
        ch = 0
        chlen = 0
        inc i
      else:
        discard # unreachable
    of stateOct:
      if chlen == 3:
        strval.add(ch.chr)
        state = stateNormal
      else:
        case rune
        of Rune('0') .. Rune('7'):
          ch *= 8
          ch += rune.int - '0'.int
          inc chlen
          inc i
        else:
          strval.add(ch.chr)
          state = stateNormal
    of stateHex:
      if chlen == 2:
        strval.add(ch.chr)
        state = stateNormal
      else:
        ch *= 16
        case rune
        of Rune('0') .. Rune('9'):
          ch += rune.int - '0'.int
        of Rune('a') .. Rune('a'):
          ch += rune.int - 'a'.int
        of Rune('A') .. Rune('F'):
          ch += rune.int - 'F'.int
        else:
          discard # unreachable
        inc chlen
        inc i
    of stateUniSmall:
      if chlen == 4:
        strval.add(ch.chr)
        state = stateNormal
      else:
        ch *= 16
        case rune
        of Rune('0') .. Rune('9'):
          ch += rune.int - '0'.int
        of Rune('a') .. Rune('a'):
          ch += rune.int - 'a'.int
        of Rune('A') .. Rune('F'):
          ch += rune.int - 'F'.int
        else:
          discard # unreachable
        inc chlen
        inc i
    of stateUniBig:
      if chlen == 8:
        strval.add(ch.chr)
        state = stateNormal
      else:
        ch *= 16
        case rune
        of Rune('0') .. Rune('9'):
          ch += rune.int - '0'.int
        of Rune('a') .. Rune('a'):
          ch += rune.int - 'a'.int
        of Rune('A') .. Rune('F'):
          ch += rune.int - 'F'.int
        else:
          discard # unreachable
        inc chlen
        inc i

  if strval.runeLen != 1:
    raise newError(errkInvalidCharacter, tok.span)

  return newNode(
    newMeta(tok.span),
    Hlir(
      kind: hrkExprInt,
      expr_int: HlirExprInt(value: strval.runeAt(0).int)
    ),
  )

proc parse_expr_named(src: var Tokens): ref Node =
  let path = parse_path_with_span(src)
  var type_args = none[seq[ref Node]]()

  return newNode(
    newMeta(path[0]),
    Hlir(
      kind: hrkExprNamed,
      expr_named: HlirExprNamed(name: Names(short: path[1]), type_args: type_args, quiet: false)
    ),
  )

proc parse_expr_interp(src: var Tokens): ref Node =
  var
    tok = src.popFirst
    span = tok.span
  case tok.kind
  of tokTilde: discard
  else:
    raise newError(errkUnexpectedToken, tok.span, "expected `~`")

  tok = src.popFirst
  case tok.kind
  of tokParenl:
    discard
  else:
    raise newError(errkUnexpectedToken, tok.span, "expected `(`")

  result = newNode(newMeta(span), Hlir(kind: hrkExprInterp, expr_interp: HlirExprInterp(value: parse_expr(src), quiet: false)))

  tok = src.popFirst
  case tok.kind
  of tokParenr:
    result.meta.span = span + tok.span
  else:
    raise newError(errkUnexpectedToken, tok.span, "expected `)`")

proc parse_expr_parenth(src: var Tokens): ref Node =
  var
    tok = src.popFirst
    span = tok.span
  case tok.kind
  of tokParenl: discard
  else:
    raise newError(errkUnexpectedToken, tok.span, "expected `(`")

  case src.peekFirst.kind
  of tokParenr:
    span += src.popFirst.span
    result = newNode(
      newMeta(span),
      Hlir(kind: hrkExprUnit, expr_unit: HlirExprUnit()),
    )
  else:
    result = parse_seq(
      src,
      parse_expr,
      expr_first,
      some[TokenKind](tokComma),
      true,
    )

    if result.node.sequence.children.len == 0:
      result = result.node.sequence.children[0]
    else:
      result.node = Hlir(kind: hrkExprParenth, expr_parenth: HlirExprParenth(elems: result.node.sequence.children))

    tok = src.popFirst
    case tok.kind
    of tokParenr:
      result.meta.span = span + tok.span
    else:
      raise newError(errkUnexpectedToken, tok.span, "expected `)`")

proc parse_binary_expr(src: var Tokens, parser: proc (src: var Tokens): ref Node, ops: openarray[(TokenKind, HlirBinaryKind)]): ref Node =
  result = parser(src)
  var tok = src.peekFirst
  while true:
    tok = src.peekFirst
    var found = false
    for kind, op in ops.items:
      if tok.kind == kind:
        found = true
        tok = src.popFirst
        let right = parser(src)
        result = newNode(
          newMeta(result.meta.span + right.meta.span),
          Hlir(
            kind: hrkExprBinary,
            expr_binary: HlirExprBinary(op: op, lhs: result, rhs: right),
          ),
        )
        break
    if not found:
      break

proc parse_primary_expr(src: var Tokens): ref Node =
  var tok = src.peekFirst
  case tok.kind
  of tokHexadecimallit, tokDecimallit, tokOctallit, tokBinarylit:
    return parse_expr_integer(src)
  of tokTrue, tokFalse:
    return parse_expr_boolean(src)
  of tokFloatlit:
    return parse_expr_float(src)
  of tokCharlit:
    return parse_expr_char(src)
  of tokStrlit:
    return parse_expr_str(src)
  of tokColon2, tokIdent:
    return parse_expr_named(src)
  of tokTilde:
    return parse_expr_interp(src)
  of tokParenl:
    return parse_expr_parenth(src)
  of tokFn:
    return parse_expr_function(src)
  of tokDot2:
    let span = src.popFirst.span
    return newNode(
      newMeta(span),
      Hlir(kind: hrkExprRangeFull, expr_range_full: HlirExprRangeFull()),
    )
  of tokType:
    src.popFirst()
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkTypeType,
        type_type: HlirTypeType(),
      ),
    )
  of tokNever:
    src.popFirst()
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkTypeNever,
        type_never: HlirTypeNever(),
      ),
    )
  of tokUnit:
    src.popFirst()
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkTypeUnit,
        type_unit: HlirTypeUnit(),
      ),
    )
  of tokBool:
    src.popFirst()
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkTypeBool,
        type_bool: HlirTypeBool(),
      ),
    )
  of tokS8:
    src.popFirst()
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkTypeSigned,
        type_signed: HlirTypeSigned(bits: Bits(kind: bitsCustom, bits: 8)),
      ),
    )
  of tokS16:
    src.popFirst()
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkTypeSigned,
        type_signed: HlirTypeSigned(bits: Bits(kind: bitsCustom, bits: 16)),
      ),
    )
  of tokS32:
    src.popFirst()
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkTypeSigned,
        type_signed: HlirTypeSigned(bits: Bits(kind: bitsCustom, bits: 32)),
      ),
    )
  of tokS64:
    src.popFirst()
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkTypeSigned,
        type_signed: HlirTypeSigned(bits: Bits(kind: bitsCustom, bits: 64)),
      ),
    )
  of tokS128:
    src.popFirst()
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkTypeSigned,
        type_signed: HlirTypeSigned(bits: Bits(kind: bitsCustom, bits: 128)),
      ),
    )
  of tokSint:
    src.popFirst()
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkTypeSigned,
        type_signed: HlirTypeSigned(bits: Bits(kind: bitsWord)),
      ),
    )
  of tokU8:
    src.popFirst()
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkTypeUnsigned,
        type_unsigned: HlirTypeUnsigned(bits: Bits(kind: bitsCustom, bits: 8)),
      ),
    )
  of tokU16:
    src.popFirst()
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkTypeUnsigned,
        type_unsigned: HlirTypeUnsigned(bits: Bits(kind: bitsCustom, bits: 16)),
      ),
    )
  of tokU32:
    src.popFirst()
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkTypeUnsigned,
        type_unsigned: HlirTypeUnsigned(bits: Bits(kind: bitsCustom, bits: 32)),
      ),
    )
  of tokU64:
    src.popFirst()
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkTypeUnsigned,
        type_unsigned: HlirTypeUnsigned(bits: Bits(kind: bitsCustom, bits: 64)),
      ),
    )
  of tokU128:
    src.popFirst()
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkTypeUnsigned,
        type_unsigned: HlirTypeUnsigned(bits: Bits(kind: bitsCustom, bits: 128)),
      ),
    )
  of tokUint:
    src.popFirst()
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkTypeUnsigned,
        type_unsigned: HlirTypeUnsigned(bits: Bits(kind: bitsWord)),
      ),
    )
  of tokFloat16:
    src.popFirst()
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkTypeFloating,
        type_floating: HlirTypeFloating(bits: Bits(kind: bitsCustom, bits: 16)),
      ),
    )
  of tokFloat32:
    src.popFirst()
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkTypeFloating,
        type_floating: HlirTypeFloating(bits: Bits(kind: bitsCustom, bits: 32)),
      ),
    )
  of tokFloat64:
    src.popFirst()
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkTypeFloating,
        type_floating: HlirTypeFloating(bits: Bits(kind: bitsCustom, bits: 64)),
      ),
    )
  of tokFloat80:
    src.popFirst()
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkTypeFloating,
        type_floating: HlirTypeFloating(bits: Bits(kind: bitsCustom, bits: 80)),
      ),
    )
  of tokFloat128:
    src.popFirst()
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkTypeFloating,
        type_floating: HlirTypeFloating(bits: Bits(kind: bitsCustom, bits: 128)),
      ),
    )
  of tokFloat:
    src.popFirst()
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkTypeFloating,
        type_floating: HlirTypeFloating(bits: Bits(kind: bitsWord)),
      ),
    )
  of tokTrait:
    src.popFirst()
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkExprTrait,
        expr_trait: HlirExprTrait(),
      ),
    )
  of tokStruct:
    src.popFirst()
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkExprStruct,
        expr_struct: HlirExprStruct(),
      ),
    )
  of tokUnion:
    src.popFirst()
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkExprUnion,
        expr_union: HlirExprUnion(),
      ),
    )
  of tokEnum:
    src.popFirst()
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkExprEnum,
        expr_enum: HlirExprEnum(),
      ),
    )
  of tokTagged:
    src.popFirst()
    result = newNode(
      newMeta(tok.span),
      Hlir(
        kind: hrkExprTagged,
        expr_tagged: HlirExprTagged(),
      ),
    )
  else:
    raise newError(errkUnexpectedToken, tok.span, "expected <expr> or <type>")

proc parse_postfix_expr(src: var Tokens): ref Node =
  var tok = src.peekFirst
  case tok.kind
  of tokSquarel:
    var span = src.popFirst.span
    result = parse_seq(
      src,
      parse_expr,
      expr_first,
      some[TokenKind](tokComma),
      true,
    )
    result.node = Hlir(
      kind: hrkExprArray,
      expr_array: HlirExprArray(elems: result.node.sequence.children),
    )
    tok = src.popFirst
    case tok.kind
    of tokSquarer:
      span += tok.span
    else:
      span += tok.span
      raise newError(errkUnexpectedToken, tok.span, "expected `]`")
    result.meta.span = span
  of tokCurlyl:
    result = parse_expr_compound(src)
  of tokSizeof:
    var span = src.popFirst.span

    tok = src.popFirst
    case tok.kind
    of tokParenl: discard
    else:
      raise newError(errkUnexpectedToken, tok.span, "expected `(`")

    let value = parse_expr(src)

    tok = src.popFirst
    case tok.kind
    of tokParenr:
      span += tok.span
    else:
      raise newError(errkUnexpectedToken, tok.span, "expected `)`")

    result = newNode(
      newMeta(span),
      Hlir(
        kind: hrkExprSizeof,
        expr_sizeof: HlirExprSizeof(value: value),
      )
    )
  of tokAlignof:
    var span = src.popFirst.span

    tok = src.popFirst
    case tok.kind
    of tokParenl: discard
    else:
      raise newError(errkUnexpectedToken, tok.span, "expected `(`")

    let value = parse_expr(src)

    tok = src.popFirst
    case tok.kind
    of tokParenr:
      span += tok.span
    else:
      raise newError(errkUnexpectedToken, tok.span, "expected `)`")

    result = newNode(
      newMeta(span),
      Hlir(
        kind: hrkExprAlignof,
        expr_alignof: HlirExprAlignof(value: value),
      )
    )
  of tokCast:
    var span = src.popFirst.span

    tok = src.popFirst
    case tok.kind
    of tokParenl: discard
    else:
      raise newError(errkUnexpectedToken, tok.span, "expected `(`")

    let ty = parse_type_expr(src)

    tok = src.popFirst
    case tok.kind
    of tokComma: discard
    else:
      raise newError(errkUnexpectedToken, tok.span, "expected `,`")

    let value = parse_expr(src)

    tok = src.popFirst
    case tok.kind
    of tokParenr:
      span += tok.span
    else:
      raise newError(errkUnexpectedToken, tok.span, "expected `)`")

    result = newNode(
      newMeta(span),
      Hlir(
        kind: hrkExprCast,
        expr_cast: HlirExprCast(ty: ty, value: value),
      )
    )
  else:
    result = parse_primary_expr(src)

  while true:
    tok = src.peekFirst
    case tok.kind
    of tokInc:
      src.popFirst
      result = newNode(
        newMeta(result.meta.span + tok.span),
        Hlir(
          kind: hrkExprUnary,
          expr_unary: HlirExprUnary(op: hukPostInc, value: result),
        )
      )
    of tokDec:
      src.popFirst
      result = newNode(
        newMeta(result.meta.span + tok.span),
        Hlir(
          kind: hrkExprUnary,
          expr_unary: HlirExprUnary(op: hukPostDec, value: result),
        )
      )
    of tokDot:
      src.popFirst
      let field = parse_ident_with_span(src)
      result = newNode(
        newMeta(result.meta.span + field[0]),
        Hlir(
          kind: hrkExprFieldAccess,
          expr_fieldaccess: HlirExprFieldAccess(value: result, name: field[1]),
        )
      )
    of tokColon:
      src.popFirst
      let value = parse_expr(src)
      result = newNode(
        newMeta(result.meta.span + value.meta.span),
        Hlir(
          kind: hrkDecl,
          decl: HlirDecl(name: some(result), value: value),
        )
      )
    of tokDot2:
      src.popFirst
      result = newNode(
        newMeta(result.meta.span + tok.span),
        Hlir(
          kind: hrkExprUnary,
          expr_unary: HlirExprUnary(op: hukRangeFrom, value: result),
        )
      )
    of tokParenl:
      src.popFirst

      let args = parse_seq(
        src,
        parse_expr,
        expr_first,
        some[TokenKind](tokComma),
        true,
      )

      tok = src.popFirst
      case tok.kind
      of tokParenr: discard
      else:
        raise newError(errkUnexpectedToken, tok.span, "expected `)`")

      result = newNode(
        newMeta(result.meta.span + tok.span),
        Hlir(
          kind: hrkExprApplication,
          expr_application: HlirExprApplication(fn: result, args: args.node.sequence.children),
        )
      )
    of tokSquarel:
      src.popFirst

      let index = parse_expr(src)

      tok = src.popFirst
      case tok.kind
      of tokSquarer: discard
      else:
        raise newError(errkUnexpectedToken, tok.span, "expected `]`")

      result = newNode(
        newMeta(result.meta.span + tok.span),
        Hlir(
          kind: hrkExprBinary,
          expr_binary: HlirExprBinary(op: hbkIndex, lhs: result, rhs: index),
        )
      )
    of tokExclam:
      src.popFirst

      let args = parse_seq(
        src,
        parse_expr,
        expr_first,
        some[TokenKind](tokComma),
        true,
      )

      result = newNode(
        newMeta(result.meta.span + args.meta.span),
        Hlir(
          kind: hrkExprCommand,
          expr_command: HlirExprCommand(fn: result, args: args.node.sequence.children),
        )
      )
    else:
      break

proc parse_unary_expr(src: var Tokens): ref Node =
  let ops = {
    tokInc: hukPreInc,
    tokDec: hukPreDec,
    tokPlus: hukPos,
    tokDash: hukNeg,
    tokAmp: hukRef,
    tokStar: hukDeref,
    tokExclam: hukNot,
    tokColon: hukColon,
  }
  var tok = src.peekFirst
  for kind, op in ops.items:
    if tok.kind == kind:
      tok = src.popFirst
      let value = parse_unary_expr(src)
      return newNode(
        newMeta(tok.span + value.meta.span),
        Hlir(
          kind: hrkExprUnary,
          expr_unary: HlirExprUnary(op: op, value: value),
        ),
      )

  case tok.kind
  of tokFail:
    tok = src.popFirst
    var
      value = none[ref Node]()
      span = tok.span
    if src.peekFirst.kind in expr_first:
      value = some(parse_expr(src))
      span += value.get.meta.span
    return newNode(
      newMeta(span),
      Hlir(
        kind: hrkExprFail,
        expr_fail: HlirExprFail(value: value),
      ),
    )
  of tokDefer:
    tok = src.popFirst
    let
      span = tok.span
      value = parse_expr(src)
    return newNode(
      newMeta(span + value.meta.span),
      Hlir(
        kind: hrkExprDefer,
        expr_defer: HlirExprDefer(value: value),
      ),
    )
  of tokReturn:
    tok = src.popFirst
    var
      value = none[ref Node]()
      span = tok.span
    if src.peekFirst.kind in expr_first:
      value = some(parse_expr(src))
      span += value.get.meta.span
    return newNode(
      newMeta(span),
      Hlir(
        kind: hrkExprReturn,
        expr_return: HlirExprReturn(value: value),
      ),
    )
  of tokBreak:
    tok = src.popFirst
    var
      value = none[ref Node]()
      span = tok.span
    if src.peekFirst.kind in expr_first:
      value = some(parse_expr(src))
      span += value.get.meta.span
    return newNode(
      newMeta(span),
      Hlir(
        kind: hrkExprBreak,
        expr_break: HlirExprBreak(label: value),
      ),
    )
  of tokContinue:
    tok = src.popFirst
    var
      value = none[ref Node]()
      span = tok.span
    if src.peekFirst.kind in expr_first:
      value = some(parse_expr(src))
      span += value.get.meta.span
    return newNode(
      newMeta(span),
      Hlir(
        kind: hrkExprContinue,
        expr_continue: HlirExprContinue(label: value),
      ),
    )
  of tokLet:
    tok = src.popFirst
    var
      name = none[Names]()
      value = none[ref Node]()
      ty = none[ref Node]()
      span = tok.span

    let name_node = parse_primary_expr(src)
    span += name_node.meta.span

    case name_node.node.kind
    of hrkExprNamed:
      name_node.node.expr_named.quiet = true
      name = some(name_node.node.expr_named.name)
    of hrkExprInterp:
      name_node.node.expr_interp.quiet = true
    else:
      discard

    if src.peekFirst.kind == tokColon:
      src.popFirst
      ty = some(parse_type_expr(src))
      span += ty.get.meta.span

    if src.peekFirst.kind == tokEqual:
      src.popFirst()
      value = some(parse_expr(src))
      span += value.get.meta.span

    return newNode(
      newMeta(span),
      Hlir(
        kind: hrkExprLet,
        expr_let: HlirExprLet(name_node: name_node, name: name, ty: ty, value: value),
      ),
    )
  of tokDot2:
    var
      value: ref Node
      span = tok.span
    if src[1].kind in expr_first:
      tok = src.popFirst
      value = parse_expr(src)
      span += value.meta.span
      return newNode(
        newMeta(span),
        Hlir(
          kind: hrkExprUnary,
          expr_unary: HlirExprUnary(op: hukRangeTo, value: value),
        ),
      )
    else:
      return parse_postfix_expr(src)
  else:
    return parse_postfix_expr(src)

proc parse_mul_expr(src: var Tokens): ref Node =
  return parse_binary_expr(src, parse_unary_expr, {tokStar: hbkMul, tokSlash: hbkDiv, tokPercent: hbkRem})

proc parse_add_expr(src: var Tokens): ref Node =
  return parse_binary_expr(src, parse_mul_expr, {tokPlus: hbkAdd, tokDash: hbkSub})

proc parse_shift_expr(src: var Tokens): ref Node =
  return parse_binary_expr(src, parse_add_expr, {tokLl: hbkShl, tokRr: hbkShr})

proc parse_and_expr(src: var Tokens): ref Node =
  return parse_binary_expr(src, parse_shift_expr, {tokAmp: hbkBitand})

proc parse_xor_expr(src: var Tokens): ref Node =
  return parse_binary_expr(src, parse_and_expr, {tokCaret: hbkBitxor})

proc parse_or_expr(src: var Tokens): ref Node =
  return parse_binary_expr(src, parse_xor_expr, {tokPipe: hbkBitor})

proc parse_rel_expr(src: var Tokens): ref Node =
  return parse_binary_expr(
    src,
    parse_or_expr,
    {
      tokEeq: hbkEq,
      tokNeq: hbkNe,
      tokLeft: hbkLt,
      tokRight: hbkGt,
      tokLeq: hbkLe,
      tokGeq: hbkGe,
    },
  )

proc parse_logical_and_expr(src: var Tokens): ref Node =
  return parse_binary_expr(src, parse_rel_expr, {tokAnd: hbkLogicalAnd})

proc parse_logical_or_expr(src: var Tokens): ref Node =
  return parse_binary_expr(src, parse_logical_and_expr, {tokOr: hbkLogicalOr})

proc parse_range_expr(src: var Tokens): ref Node =
  # a little hack to convert a.. => a..b
  result = parse_binary_expr(src, parse_logical_or_expr, {tokDot2: hbkRange})
  if result.node.kind == hrkExprUnary and result.node.expr_unary.op == hukRangeFrom:
    if src.peekFirst.kind in expr_first:
      let rhs = parse_logical_or_expr(src)
      result = newNode(
        newMeta(result.meta.span + rhs.meta.span),
        Hlir(kind: hrkExprBinary, expr_binary: HlirExprBinary(op: hbkRange, lhs: result.node.expr_unary.value, rhs: rhs)),
      )

proc parse_cond_expr(src: var Tokens): ref Node =
  return parse_range_expr(src)

proc parse_arrow_expr(src: var Tokens): ref Node =
  return parse_binary_expr(src, parse_cond_expr, {tokDasharrow: hbkArrow})

proc parse_assign_expr(src: var Tokens): ref Node =
  return parse_binary_expr(
    src,
    parse_arrow_expr,
    {
      tokEqual: hbkAssign,
      tokPluseq: hbkAssignAdd,
      tokDasheq: hbkAssignSub,
      tokStareq: hbkAssignMul,
      tokSlasheq: hbkAssignDiv,
      tokPereq: hbkAssignRem,
      tokLleq: hbkAssignLshift,
      tokRreq: hbkAssignRshift,
      tokAmpeq: hbkAssignAnd,
      tokCareteq: hbkAssignXor,
      tokPipeeq: hbkAssignOr,
    },
  )

proc parse_expr(src: var Tokens): ref Node =
  return parse_assign_expr(src)

proc parse_decl(src: var Tokens): ref Node =
  var
    name = some(parse_primary_expr(src))
    value: ref Node
    tok = src.peekFirst
  case tok.kind
  of tokColon:
    src.popFirst
    value = parse_type_expr(src)
  else:
    value = name.get
    name = none[ref Node]()

  let span = if name.isSome: name.get.meta.span + value.meta.span else: value.meta.span
  return newNode(
    newMeta(span),
    Hlir(
      kind: hrkDecl,
      decl: HlirDecl(name: name, value: value)
    ),
  )

proc parse_pat_else(src: var Tokens): ref Node =
  case src.peekFirst.kind
  of tokElse:
    return newNode(
      newMeta(src.popFirst.span),
      Hlir(kind: hrkPatElse, pat_else: HlirPatElse()),
    )
  else: return parse_expr(src)

proc parse_pat_bind(src: var Tokens): ref Node =
  var name: Option[Names]
  let name_node = parse_primary_expr(src)

  case name_node.node.kind
  of hrkExprNamed:
    name_node.node.expr_named.quiet = true
    name = some(name_node.node.expr_named.name)
  of hrkExprInterp:
    name_node.node.expr_interp.quiet = true
  else:
    discard

  return newNode(
    newMeta(name_node.meta.span),
    Hlir(kind: hrkPatBind, pat_bind: HlirPatBind(name_node: name_node, name: name))
  )

proc parse_pat(src: var Tokens): ref Node =
  case src.peekFirst.kind
  of tokElse: return parse_pat_else(src)
  of tokColon:
    src.popFirst
    return parse_pat_bind(src)
  else: return parse_expr(src)

proc parse_path(src: var Tokens): Path =
  return parse_path_with_span(src)[1]

proc parse_path_with_span(src: var Tokens): (Span, Path) =
  var
    full = false
    path = newSeq[string](0)
    glob = false
    tok = src.popFirst
    span = tok.span

  case tok.kind
  of tokColon2:
    full = true
    tok = src.popFirst
    case tok.kind
    of tokIdent:
      path.add(tok.span.get)
    else: raise newError(errkUnexpectedToken, tok.span, "expected <path>")
  of tokIdent:
    path.add(tok.span.get)
  else: raise newError(errkUnexpectedToken, tok.span, "expected <path> or `::`")

  while true:
    tok = src.peekFirst
    case tok.kind
    of tokColon2: src.popFirst
    else: break

    tok = src.popFirst
    case tok.kind
    of tokIdent:
      span += tok.span
      path.add(tok.span.get)
    of tokStar:
      span += tok.span
      tok = src.peekFirst
      case tok.kind
      of tokColon2:
        raise newError(errkUnexpectedToken, tok.span, "a glob cannot be followed by more identifiers")
      else: discard
      glob = true
      break
    else:
      raise newError(errkUnexpectedToken, tok.span, "expected <path> or `*`")

  return (span, Path(full: full, path: path, glob: glob))

proc parse_ident(src: var Tokens): string =
  return parse_ident_with_span(src)[1]

proc parse_ident_with_span(src: var Tokens): (Span, string) =
  let tok = src.popFirst
  case tok.kind
  of tokIdent: return (tok.span, tok.span.get)
  else: raise newError(errkUnexpectedToken, tok.span, "expected <ident>")
