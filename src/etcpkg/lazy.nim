import hlir/hlir

type
  LazyUpdate* = object
    add_node: seq[ref Node]
    add_norun: seq[ref Node]

proc newLazyUpdate*(): ref LazyUpdate =
  new result
  result.add_node = newSeq[ref Node]()
  result.add_norun = newSeq[ref Node]()

proc add*(lazy: ref LazyUpdate, node: ref Node) =
  lazy.add_node.add(node)

iterator nodes*(lazy: ref LazyUpdate): ref Node =
  while lazy.add_node.len != 0:
    yield lazy.add_node.pop

proc add_norun*(lazy: ref LazyUpdate, node: ref Node) =
  lazy.add_norun.add(node)

iterator norun*(lazy: ref LazyUpdate): ref Node =
  while lazy.add_norun.len != 0:
    yield lazy.add_norun.pop
