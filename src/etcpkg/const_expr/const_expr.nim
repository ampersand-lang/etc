import strformat
import math
import ../span
import ../error
import ../path
import ../types/types

type
  TypedExpr* = object
    span*: Span
    ty*: ref QualifiedType
    expr*: ConstExpr
  ConstExprKind* = enum
    cekUninit
    cekUnit
    cekBool
    cekUnsigned
    cekSigned
    cekFloat
    cekStr
    cekPointer
    cekArray
    cekSlice
    cekStruct
    cekUnion
    cekEnum
    cekTagged
  ConstExpr* = object
    case kind*: ConstExprKind
    of cekUninit: discard
    of cekUnit: discard
    of cekBool:
      p*: bool
    of cekSigned:
      sinteger*: int64
    of cekUnsigned:
      uinteger*: uint64
    of cekFloat:
      floating*: float64
    of cekStr:
      str*: string
    of cekPointer:
      point*: int64
    of cekArray:
      arr*: seq[TypedExpr]
    of cekSlice:
      slice*: seq[TypedExpr]
    of cekStruct:
      struct_fields*: seq[ConstExprField]
    of cekUnion:
      union_field*: ref ConstExprField
    of cekEnum:
      enum_name*: Path
      enum_value*: uint64
    of cekTagged:
      tagged_name*: Path
      tagged_fields*: seq[ConstExprField]
  ConstExprField* = object
    name*: string
    expr*: TypedExpr

proc `$`*(expr: ConstExpr): string =
  case expr.kind
  of cekUninit:
    result = "uninit"
  of cekUnit:
    result = "()"
  of cekBool:
    result = $expr.p
  of cekUnsigned:
    result = $expr.sinteger
  of cekSigned:
    result = $expr.uinteger
  of cekFloat:
    result = $expr.floating
  of cekStr:
    result = expr.str
  of cekPointer:
    result = fmt"{expr.point:#x}"
  of cekArray:
    result = "["
    for i, elem in expr.arr.pairs:
      if i > 0:
        result.add(", ")
      result.add($elem)
    result.add("]")
  of cekSlice:
    result = "["
    for i, elem in expr.slice.pairs:
      if i > 0:
        result.add(", ")
      result.add($elem)
    result.add("]")
  of cekStruct:
    result = "struct {"
    for i, field in expr.struct_fields.pairs:
      if i > 0:
        result.add(", ")
      result.add($field.name)
      result.add(": ")
      result.add($field.expr)
    result.add("}")
  of cekUnion:
    result = "union {"
    result.add($expr.union_field.name)
    result.add(": ")
    result.add($expr.union_field.expr)
    result.add("}")
  of cekEnum:
    result = fmt"enum {expr.enum_name}"
  of cekTagged:
    result = fmt"tagged {expr.tagged_name} {'{'}"
    for i, field in expr.tagged_fields.pairs:
      if i > 0:
        result.add(", ")
      result.add($field.name)
      result.add(": ")
      result.add($field.expr)
    result.add("}")

proc `$`*(expr: TypedExpr): string =
  return fmt"{expr.expr} {expr.ty[]}"

proc add*(lhs: TypedExpr, rhs: TypedExpr, span: Span): TypedExpr =
  if lhs.ty == rhs.ty:
    case lhs.expr.kind
    of cekSigned:
      return TypedExpr(span: span, ty: lhs.ty, expr: ConstExpr(kind: cekSigned, sinteger: lhs.expr.sinteger + rhs.expr.sinteger))
    of cekUnsigned:
      return TypedExpr(span: span, ty: lhs.ty, expr: ConstExpr(kind: cekUnsigned, uinteger: lhs.expr.uinteger + rhs.expr.uinteger))
    of cekFloat:
      return TypedExpr(span: span, ty: lhs.ty, expr: ConstExpr(kind: cekFloat, floating: lhs.expr.floating + rhs.expr.floating))
    else:
      raise newError(errkInvalidOperation, span)
  else:
    raise newError(errkInvalidOperation, span, "mismatching types")

proc sub*(lhs: TypedExpr, rhs: TypedExpr, span: Span): TypedExpr =
  if lhs.ty == rhs.ty:
    case lhs.expr.kind
    of cekSigned:
      return TypedExpr(span: span, ty: lhs.ty, expr: ConstExpr(kind: cekSigned, sinteger: lhs.expr.sinteger - rhs.expr.sinteger))
    of cekUnsigned:
      return TypedExpr(span: span, ty: lhs.ty, expr: ConstExpr(kind: cekUnsigned, uinteger: lhs.expr.uinteger - rhs.expr.uinteger))
    of cekFloat:
      return TypedExpr(span: span, ty: lhs.ty, expr: ConstExpr(kind: cekFloat, floating: lhs.expr.floating - rhs.expr.floating))
    else:
      raise newError(errkInvalidOperation, span)
  else:
    raise newError(errkInvalidOperation, span, "mismatching types")

proc mul*(lhs: TypedExpr, rhs: TypedExpr, span: Span): TypedExpr =
  if lhs.ty == rhs.ty:
    case lhs.expr.kind
    of cekSigned:
      return TypedExpr(span: span, ty: lhs.ty, expr: ConstExpr(kind: cekSigned, sinteger: lhs.expr.sinteger * rhs.expr.sinteger))
    of cekUnsigned:
      return TypedExpr(span: span, ty: lhs.ty, expr: ConstExpr(kind: cekUnsigned, uinteger: lhs.expr.uinteger * rhs.expr.uinteger))
    of cekFloat:
      return TypedExpr(span: span, ty: lhs.ty, expr: ConstExpr(kind: cekFloat, floating: lhs.expr.floating * rhs.expr.floating))
    else:
      raise newError(errkInvalidOperation, span)
  else:
    raise newError(errkInvalidOperation, span, "mismatching types")

proc quot*(lhs: TypedExpr, rhs: TypedExpr, span: Span): TypedExpr =
  if lhs.ty == rhs.ty:
    case lhs.expr.kind
    of cekSigned:
      return TypedExpr(span: span, ty: lhs.ty, expr: ConstExpr(kind: cekSigned, sinteger: lhs.expr.sinteger div rhs.expr.sinteger))
    of cekUnsigned:
      return TypedExpr(span: span, ty: lhs.ty, expr: ConstExpr(kind: cekUnsigned, uinteger: lhs.expr.uinteger div rhs.expr.uinteger))
    of cekFloat:
      return TypedExpr(span: span, ty: lhs.ty, expr: ConstExpr(kind: cekFloat, floating: lhs.expr.floating / rhs.expr.floating))
    else:
      raise newError(errkInvalidOperation, span)
  else:
    raise newError(errkInvalidOperation, span, "mismatching types")

proc rem*(lhs: TypedExpr, rhs: TypedExpr, span: Span): TypedExpr =
  if lhs.ty == rhs.ty:
    case lhs.expr.kind
    of cekSigned:
      return TypedExpr(span: span, ty: lhs.ty, expr: ConstExpr(kind: cekSigned, sinteger: lhs.expr.sinteger mod rhs.expr.sinteger))
    of cekUnsigned:
      return TypedExpr(span: span, ty: lhs.ty, expr: ConstExpr(kind: cekUnsigned, uinteger: lhs.expr.uinteger mod rhs.expr.uinteger))
    of cekFloat:
      return TypedExpr(span: span, ty: lhs.ty, expr: ConstExpr(kind: cekFloat, floating: lhs.expr.floating mod rhs.expr.floating))
    else:
      raise newError(errkInvalidOperation, span)
  else:
    raise newError(errkInvalidOperation, span, "mismatching types")
