import math
import tables
import options
import os
import ../error
import ../span
import ../path
import ../index
import ../types/types
import ../hlir/hlir
import typedefs

proc `-`(d: uint64): uint64 = cast[uint64](-cast[int64](d))

proc increment*(value: MacroValue): MacroValue =
  case value.kind
  of mvkUninit:
    return MacroValue(span: value.span, ty: value.ty[].toTypeSumRef, kind: mvkUninit)
  of mvkSigned:
    return MacroValue(span: value.span, ty: value.ty[].toTypeSumRef, kind: mvkSigned, signed: MacroSigned(intval: value.signed.intval + 1))
  of mvkUnsigned:
    return MacroValue(span: value.span, ty: value.ty[].toTypeSumRef, kind: mvkUnsigned, unsigned: MacroUnsigned(intval: value.unsigned.intval + 1))
  of mvkFloat:
    return MacroValue(span: value.span, ty: value.ty[].toTypeSumRef, kind: mvkFloat, floating: MacroFloat(floatval: value.floating.floatval + 1.0))
  else:
    raise newError(errkInvalidType, value.span)

proc decrement*(value: MacroValue): MacroValue =
  case value.kind
  of mvkUninit:
    return MacroValue(span: value.span, ty: value.ty[].toTypeSumRef, kind: mvkUninit)
  of mvkSigned:
    return MacroValue(span: value.span, ty: value.ty[].toTypeSumRef, kind: mvkSigned, signed: MacroSigned(intval: value.signed.intval - 1))
  of mvkUnsigned:
    return MacroValue(span: value.span, ty: value.ty[].toTypeSumRef, kind: mvkUnsigned, unsigned: MacroUnsigned(intval: value.unsigned.intval - 1))
  of mvkFloat:
    return MacroValue(span: value.span, ty: value.ty[].toTypeSumRef, kind: mvkFloat, floating: MacroFloat(floatval: value.floating.floatval - 1.0))
  else:
    raise newError(errkInvalidType, value.span)

template unary*(op: untyped) =
  proc op*(value: MacroValue): MacroValue =
    case value.kind
    of mvkUninit:
      return MacroValue(span: value.span, ty: value.ty[].toTypeSumRef, kind: mvkUninit)
    of mvkSigned:
      return MacroValue(span: value.span, ty: value.ty[].toTypeSumRef, kind: mvkSigned, signed: MacroSigned(intval: op(value.signed.intval)))
    of mvkUnsigned:
      return MacroValue(span: value.span, ty: value.ty[].toTypeSumRef, kind: mvkUnsigned, unsigned: MacroUnsigned(intval: op(value.unsigned.intval)))
    of mvkFloat:
      return MacroValue(span: value.span, ty: value.ty[].toTypeSumRef, kind: mvkFloat, floating: MacroFloat(floatval: op(value.floating.floatval)))
    else:
      raise newError(errkInvalidType, value.span)

template unary_int*(op: untyped) =
  proc op*(value: MacroValue): MacroValue =
    case value.kind
    of mvkUninit:
      return MacroValue(span: value.span, ty: value.ty[].toTypeSumRef, kind: mvkUninit)
    of mvkSigned:
      return MacroValue(span: value.span, ty: value.ty[].toTypeSumRef, kind: mvkSigned, signed: MacroSigned(intval: op(value.signed.intval)))
    of mvkUnsigned:
      return MacroValue(span: value.span, ty: value.ty[].toTypeSumRef, kind: mvkUnsigned, unsigned: MacroUnsigned(intval: op(value.unsigned.intval)))
    else:
      raise newError(errkInvalidType, value.span)

template binary*(op: untyped) =
  proc op*(lhs: MacroValue, rhs: MacroValue): MacroValue =
    case lhs.kind
    of mvkUninit:
      case rhs.kind
      of mvkUninit:
        return MacroValue(span: lhs.span, ty: lhs.ty[].toTypeSumRef, kind: mvkUninit)
      of mvkSigned:
        return MacroValue(span: lhs.span, ty: lhs.ty[].toTypeSumRef, kind: mvkUninit)
      of mvkUnsigned:
        return MacroValue(span: lhs.span, ty: lhs.ty[].toTypeSumRef, kind: mvkUninit)
      of mvkFloat:
        return MacroValue(span: lhs.span, ty: lhs.ty[].toTypeSumRef, kind: mvkUninit)
      else:
        raise newError(errkInvalidType, lhs.span)
    of mvkSigned:
      case rhs.kind
      of mvkUninit:
        return MacroValue(span: lhs.span, ty: lhs.ty[].toTypeSumRef, kind: mvkUninit)
      of mvkSigned:
        return MacroValue(span: lhs.span, ty: lhs.ty[].toTypeSumRef, kind: mvkSigned, signed: MacroSigned(intval: op(lhs.signed.intval, rhs.signed.intval)))
      else:
        raise newError(errkInvalidType, lhs.span)
    of mvkUnsigned:
      case rhs.kind
      of mvkUninit:
        return MacroValue(span: lhs.span, ty: lhs.ty[].toTypeSumRef, kind: mvkUninit)
      of mvkUnsigned:
        return MacroValue(span: lhs.span, ty: lhs.ty[].toTypeSumRef, kind: mvkUnsigned, unsigned: MacroUnsigned(intval: op(lhs.unsigned.intval, rhs.unsigned.intval)))
      else:
        raise newError(errkInvalidType, lhs.span)
    of mvkFloat:
      case rhs.kind
      of mvkUninit:
        return MacroValue(span: lhs.span, ty: lhs.ty[].toTypeSumRef, kind: mvkUninit)
      of mvkFloat:
        return MacroValue(span: lhs.span, ty: lhs.ty[].toTypeSumRef, kind: mvkFloat, floating: MacroFloat(floatval: op(lhs.floating.floatval, rhs.floating.floatval)))
      else:
        raise newError(errkInvalidType, lhs.span)
    else:
      raise newError(errkInvalidType, lhs.span)

template binary*(op: untyped, op_float: untyped, op_int: untyped) =
  proc op*(lhs: MacroValue, rhs: MacroValue): MacroValue =
    case lhs.kind
    of mvkUninit:
      case rhs.kind
      of mvkUninit:
        return MacroValue(span: lhs.span, ty: lhs.ty[].toTypeSumRef, kind: mvkUninit)
      of mvkSigned:
        return MacroValue(span: lhs.span, ty: lhs.ty[].toTypeSumRef, kind: mvkUninit)
      of mvkUnsigned:
        return MacroValue(span: lhs.span, ty: lhs.ty[].toTypeSumRef, kind: mvkUninit)
      of mvkFloat:
        return MacroValue(span: lhs.span, ty: lhs.ty[].toTypeSumRef, kind: mvkUninit)
      else:
        raise newError(errkInvalidType, lhs.span)
    of mvkSigned:
      case rhs.kind
      of mvkUninit:
        return MacroValue(span: lhs.span, ty: lhs.ty[].toTypeSumRef, kind: mvkUninit)
      of mvkSigned:
        return MacroValue(span: lhs.span, ty: lhs.ty[].toTypeSumRef, kind: mvkSigned, signed: MacroSigned(intval: op_int(lhs.signed.intval, rhs.signed.intval)))
      else:
        raise newError(errkInvalidType, lhs.span)
    of mvkUnsigned:
      case rhs.kind
      of mvkUninit:
        return MacroValue(span: lhs.span, ty: lhs.ty[].toTypeSumRef, kind: mvkUninit)
      of mvkUnsigned:
        return MacroValue(span: lhs.span, ty: lhs.ty[].toTypeSumRef, kind: mvkUnsigned, unsigned: MacroUnsigned(intval: op_int(lhs.unsigned.intval, rhs.unsigned.intval)))
      else:
        raise newError(errkInvalidType, lhs.span)
    of mvkFloat:
      case rhs.kind
      of mvkUninit:
        return MacroValue(span: lhs.span, ty: lhs.ty[].toTypeSumRef, kind: mvkUninit)
      of mvkFloat:
        return MacroValue(span: lhs.span, ty: lhs.ty[].toTypeSumRef, kind: mvkFloat, floating: MacroFloat(floatval: op_float(lhs.floating.floatval, rhs.floating.floatval)))
      else:
        raise newError(errkInvalidType, lhs.span)
    else:
      raise newError(errkInvalidType, lhs.span)

template binary_cmp*(name: untyped, op: untyped) =
  proc name*(ctx: TypeContext, lhs: MacroValue, rhs: MacroValue): MacroValue =
    case lhs.kind
    of mvkUninit:
      case rhs.kind
      of mvkUninit:
        return MacroValue(span: lhs.span, ty: ctx.get("bool".toPath).toTypeSumRef, kind: mvkUninit)
      of mvkSigned:
        return MacroValue(span: lhs.span, ty: ctx.get("bool".toPath).toTypeSumRef, kind: mvkUninit)
      of mvkUnsigned:
        return MacroValue(span: lhs.span, ty: ctx.get("bool".toPath).toTypeSumRef, kind: mvkUninit)
      of mvkFloat:
        return MacroValue(span: lhs.span, ty: ctx.get("bool".toPath).toTypeSumRef, kind: mvkUninit)
      else:
        raise newError(errkInvalidType, lhs.span)
    of mvkSigned:
      case rhs.kind
      of mvkUninit:
        return MacroValue(span: lhs.span, ty: ctx.get("bool".toPath).toTypeSumRef, kind: mvkUninit)
      of mvkSigned:
        return MacroValue(span: lhs.span, ty: ctx.get("bool".toPath).toTypeSumRef, kind: mvkBool, p: MacroBool(boolval: op(lhs.signed.intval, rhs.signed.intval)))
      else:
        raise newError(errkInvalidType, lhs.span)
    of mvkUnsigned:
      case rhs.kind
      of mvkUninit:
        return MacroValue(span: lhs.span, ty: ctx.get("bool".toPath).toTypeSumRef, kind: mvkUninit)
      of mvkUnsigned:
        return MacroValue(span: lhs.span, ty: ctx.get("bool".toPath).toTypeSumRef, kind: mvkBool, p: MacroBool(boolval: op(lhs.unsigned.intval, rhs.unsigned.intval)))
      else:
        raise newError(errkInvalidType, lhs.span)
    of mvkFloat:
      case rhs.kind
      of mvkUninit:
        return MacroValue(span: lhs.span, ty: ctx.get("bool".toPath).toTypeSumRef, kind: mvkUninit)
      of mvkFloat:
        return MacroValue(span: lhs.span, ty: ctx.get("bool".toPath).toTypeSumRef, kind: mvkBool, p: MacroBool(boolval: op(lhs.floating.floatval, rhs.floating.floatval)))
      else:
        raise newError(errkInvalidType, lhs.span)
    else:
      raise newError(errkInvalidType, lhs.span)

template binary_int*(op: untyped) =
  proc op*(lhs: MacroValue, rhs: MacroValue): MacroValue =
    case lhs.kind
    of mvkUninit:
      case rhs.kind
      of mvkUninit:
        return MacroValue(span: lhs.span, ty: lhs.ty[].toTypeSumRef, kind: mvkUninit)
      of mvkSigned:
        return MacroValue(span: lhs.span, ty: lhs.ty[].toTypeSumRef, kind: mvkUninit)
      of mvkUnsigned:
        return MacroValue(span: lhs.span, ty: lhs.ty[].toTypeSumRef, kind: mvkUninit)
      of mvkFloat:
        return MacroValue(span: lhs.span, ty: lhs.ty[].toTypeSumRef, kind: mvkUninit)
      else:
        raise newError(errkInvalidType, lhs.span)
    of mvkSigned:
      case rhs.kind
      of mvkUninit:
        return MacroValue(span: lhs.span, ty: lhs.ty[].toTypeSumRef, kind: mvkUninit)
      of mvkSigned:
        return MacroValue(span: lhs.span, ty: lhs.ty[].toTypeSumRef, kind: mvkSigned, signed: MacroSigned(intval: op(lhs.signed.intval, rhs.signed.intval)))
      else:
        raise newError(errkInvalidType, lhs.span)
    of mvkUnsigned:
      case rhs.kind
      of mvkUninit:
        return MacroValue(span: lhs.span, ty: lhs.ty[].toTypeSumRef, kind: mvkUninit)
      of mvkUnsigned:
        return MacroValue(span: lhs.span, ty: lhs.ty[].toTypeSumRef, kind: mvkUnsigned, unsigned: MacroUnsigned(intval: op(lhs.unsigned.intval, rhs.unsigned.intval)))
      else:
        raise newError(errkInvalidType, lhs.span)
    else:
      raise newError(errkInvalidType, lhs.span)

unary(`-`)
unary_int(`not`)
binary(`+`)
binary(`-`)
binary(`*`)
binary(`/`, `/`, `div`)
binary(`mod`, fmod, `mod`)
binary_cmp(eq, `==`)
binary_cmp(ne, `!=`)
binary_cmp(lt, `<`)
binary_cmp(gt, `>`)
binary_cmp(le, `<=`)
binary_cmp(ge, `>=`)
binary_int(`and`)
binary_int(`or`)
binary_int(`xor`)
binary_int(`shl`)
binary_int(`shr`)

proc macro_range*(ctx: TypeContext, startidx: MacroValue, endidx: MacroValue): MacroValue =
  return MacroValue(kind: mvkStruct, ty: ctx.get(["__lang", "builtin", "Range"].toPath).toTypeSumRef, struct: MacroStruct(fields: {"start": startidx, "end": endidx}.toTable))

proc macro_range_from*(ctx: TypeContext, startidx: MacroValue): MacroValue =
  return MacroValue(kind: mvkStruct, ty: ctx.get(["__lang", "builtin", "RangeFrom"].toPath).toTypeSumRef, struct: MacroStruct(fields: {"start": startidx}.toTable))

proc macro_range_to*(ctx: TypeContext, endidx: MacroValue): MacroValue =
  return MacroValue(kind: mvkStruct, ty: ctx.get(["__lang", "builtin", "RangeTo"].toPath).toTypeSumRef, struct: MacroStruct(fields: {"end": endidx}.toTable))

proc new_node_expr_int*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  let span = args[0].value.mspan.span
  let value = args[1].value.signed.intval
  return MacroValue(
    span: call_site,
    ty: ctx.type_ctx.get(["__lang", "macros", "Node"].toPath).toTypeSumRef,
    kind: mvkNode,
    node: MacroNode(node: newNode(newMeta(span), Hlir(kind: hrkExprInt, expr_int: HlirExprInt(value: value)))),
  )

proc new_node_expr_bool*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  let span = args[0].value.mspan.span
  let value = args[1].value.p.boolval
  return MacroValue(
    span: call_site,
    ty: ctx.type_ctx.get(["__lang", "macros", "Node"].toPath).toTypeSumRef,
    kind: mvkNode,
    node: MacroNode(node: newNode(newMeta(span), Hlir(kind: hrkExprBool, expr_bool: HlirExprBool(value: value)))),
  )

proc new_node_expr_binary*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  let span = args[0].value.mspan.span
  let op = args[1].value.enumeration.intval.HlirBinaryKind
  let lhs = args[2].value.node.node
  let rhs = args[3].value.node.node
  return MacroValue(
    span: call_site,
    ty: ctx.type_ctx.get(["__lang", "macros", "Node"].toPath).toTypeSumRef,
    kind: mvkNode,
    node: MacroNode(node: newNode(newMeta(span), Hlir(kind: hrkExprBinary, expr_binary: HlirExprBinary(op: op, lhs: lhs, rhs: rhs)))),
  )

proc new_node_stmt_while*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  let span = args[0].value.mspan.span
  let cond = args[1].value.node.node
  let body = args[2].value.node.node
  return MacroValue(
    span: call_site,
    ty: ctx.type_ctx.get(["__lang", "macros", "Node"].toPath).toTypeSumRef,
    kind: mvkNode,
    node: MacroNode(node: newNode(newMeta(span), Hlir(kind: hrkStmtWhile, stmt_while: HlirStmtWhile(cond: cond, body: body)))),
  )

proc error*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  case args[0].value.kind
  of mvkString:
    write stderr, "\e[31;1m[error]\e[0;31m:", call_site.file, ":", call_site.line, ":", call_site.col, ": "
    write stderr, args[0].value.str.strval
    writeLine stderr, "\e[0m"
    flushFile stderr
    raise newError(errkFail, call_site)
  else:
    raise newError(errkInvalidType, call_site)

proc warn*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  case args[0].value.kind
  of mvkString:
    write stderr, "\e[33;1m[warn]\e[0;33m:", call_site.file, ":", call_site.line, ":", call_site.col, ": "
    write stderr, args[0].value.str.strval
    writeLine stderr, "\e[0m"
    flushFile stderr
    return MacroValue(
      span: call_site,
      ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef,
      kind: mvkUnit,
    )
  else:
    raise newError(errkInvalidType, call_site)

proc debug*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  case args[0].value.kind
  of mvkString:
    write stderr, "\e[32;1m[debug]\e[0;32m:", call_site.file, ":", call_site.line, ":", call_site.col, ": "
    write stderr, args[0].value.str.strval
    writeLine stderr, "\e[0m"
    flushFile stderr
    return MacroValue(
      span: call_site,
      ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef,
      kind: mvkUnit,
    )
  else:
    raise newError(errkInvalidType, call_site)

proc info*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  case args[0].value.kind
  of mvkString:
    write stderr, "\e[1m[info]\e[0m:", call_site.file, ":", call_site.line, ":", call_site.col, ": "
    write stderr, args[0].value.str.strval
    writeLine stderr, ""
    flushFile stderr
    return MacroValue(
      span: call_site,
      ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef,
      kind: mvkUnit,
    )
  else:
    raise newError(errkInvalidType, call_site)

proc toggle_flag*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  let flag = case args[0].value.kind
  of mvkString:
    args[0].value.str.strval
  else:
    raise newError(errkInvalidType, call_site)
  index.toggle_flag(flag)
  return MacroValue(
    span: call_site,
    ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef,
    kind: mvkUnit,
  )

proc set_flag*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  let flag = case args[0].value.kind
  of mvkString:
    args[0].value.str.strval
  else:
    raise newError(errkInvalidType, call_site)
  let value = case args[1].value.kind
  of mvkBool:
    args[1].value.p.boolval
  else:
    raise newError(errkInvalidType, call_site)
  index.set_flag(flag, value)
  return MacroValue(
    span: call_site,
    ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef,
    kind: mvkUnit,
  )

proc set_opt*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  let opt = case args[0].value.kind
  of mvkString:
    args[0].value.str.strval
  else:
    raise newError(errkInvalidType, call_site)
  let value = case args[1].value.kind
  of mvkString:
    args[1].value.str.strval
  else:
    raise newError(errkInvalidType, call_site)
  index.set_opt(opt, value)
  return MacroValue(
    span: call_site,
    ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef,
    kind: mvkUnit,
  )

proc unset_opt*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  let opt = case args[0].value.kind
  of mvkString:
    args[0].value.str.strval
  else:
    raise newError(errkInvalidType, call_site)
  index.unset_opt(opt)
  return MacroValue(
    span: call_site,
    ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef,
    kind: mvkUnit,
  )

proc add_file*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  let file = case args[0].value.kind
  of mvkString:
    args[0].value.str.strval
  else:
    raise newError(errkInvalidType, call_site)
  index.add_file(call_site, file)
  return MacroValue(
    span: call_site,
    ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef,
    kind: mvkUnit,
  )

proc add_path*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  let dir = case args[0].value.kind
  of mvkString:
    args[0].value.str.strval
  else:
    raise newError(errkInvalidType, call_site)
  index.add_path(dir)
  return MacroValue(
    span: call_site,
    ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef,
    kind: mvkUnit,
  )

proc add_cpath*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  let dir = case args[0].value.kind
  of mvkString:
    args[0].value.str.strval
  else:
    raise newError(errkInvalidType, call_site)
  index.add_cpath(dir)
  return MacroValue(
    span: call_site,
    ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef,
    kind: mvkUnit,
  )

proc add_lib*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  let file = case args[0].value.kind
  of mvkString:
    args[0].value.str.strval
  else:
    raise newError(errkInvalidType, call_site)
  index.add_lib(file)
  return MacroValue(
    span: call_site,
    ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef,
    kind: mvkUnit,
  )

proc set_output*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  let file = case args[0].value.kind
  of mvkString:
    args[0].value.str.strval
  else:
    raise newError(errkInvalidType, call_site)
  index.set_output(file)
  return MacroValue(
    span: call_site,
    ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef,
    kind: mvkUnit,
  )

proc toggle_linker_flag*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  let flag = case args[0].value.kind
  of mvkString:
    args[0].value.str.strval
  else:
    raise newError(errkInvalidType, call_site)
  index.toggle_linker_flag(flag)
  return MacroValue(
    span: call_site,
    ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef,
    kind: mvkUnit,
  )

proc set_linker_flag*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  let flag = case args[0].value.kind
  of mvkString:
    args[0].value.str.strval
  else:
    raise newError(errkInvalidType, call_site)
  let value = case args[1].value.kind
  of mvkBool:
    args[1].value.p.boolval
  else:
    raise newError(errkInvalidType, call_site)
  index.set_linker_flag(flag, value)
  return MacroValue(
    span: call_site,
    ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef,
    kind: mvkUnit,
  )

proc set_linker_opt*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  let opt = case args[0].value.kind
  of mvkString:
    args[0].value.str.strval
  else:
    raise newError(errkInvalidType, call_site)
  let value = case args[1].value.kind
  of mvkString:
    args[1].value.str.strval
  else:
    raise newError(errkInvalidType, call_site)
  index.set_linker_opt(opt, value)
  return MacroValue(
    span: call_site,
    ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef,
    kind: mvkUnit,
  )

proc unset_linker_opt*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  let opt = case args[0].value.kind
  of mvkString:
    args[0].value.str.strval
  else:
    raise newError(errkInvalidType, call_site)
  index.unset_linker_opt(opt)
  return MacroValue(
    span: call_site,
    ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef,
    kind: mvkUnit,
  )

proc add_node*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  return MacroValue(
    span: call_site,
    ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef,
    kind: mvkUnit,
  )

proc get_env*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  let env = case args[0].value.kind
  of mvkString:
    args[0].value.str.strval
  else:
    raise newError(errkInvalidType, call_site)
  return MacroValue(
    span: call_site,
    ty: ctx.type_ctx.get(["_lang", "builtin", "str"].toPath).toTypeSumRef,
    kind: mvkString,
    str: MacroString(strval: os.getenv(env)),
  )

proc set_env*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  let env = case args[0].value.kind
  of mvkString:
    args[0].value.str.strval
  else:
    raise newError(errkInvalidType, call_site)
  let value = case args[1].value.kind
  of mvkString:
    args[1].value.str.strval
  else:
    raise newError(errkInvalidType, call_site)
  os.putenv(env, value)
  return MacroValue(
    span: call_site,
    ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef,
    kind: mvkUnit,
  )

proc name*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  let name = case args[0].value.kind
  of mvkString:
    args[0].value.str.strval
  else:
    raise newError(errkInvalidType, call_site)
  index.meta.name = some(name)
  return MacroValue(
    span: call_site,
    ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef,
    kind: mvkUnit,
  )

proc version*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  let version = case args[0].value.kind
  of mvkString:
    args[0].value.str.strval
  else:
    raise newError(errkInvalidType, call_site)
  index.meta.version = some(version)
  return MacroValue(
    span: call_site,
    ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef,
    kind: mvkUnit,
  )

proc authors*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  let authors = case args[0].value.kind
  of mvkSlice:
    var authors = newSeq[string]()
    for author in args[0].value.slice.arr[][args[0].value.slice.startidx ..< args[0].value.slice.endidx]:
      case author.kind
      of mvkString:
        authors.add(author.str.strval)
      else:
        raise newError(errkInvalidType, call_site)
    authors
  else:
    raise newError(errkInvalidType, call_site)
  index.meta.authors = some(authors)
  return MacroValue(
    span: call_site,
    ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef,
    kind: mvkUnit,
  )

proc description*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  let description = case args[0].value.kind
  of mvkString:
    args[0].value.str.strval
  else:
    raise newError(errkInvalidType, call_site)
  index.meta.description = some(description)
  return MacroValue(
    span: call_site,
    ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef,
    kind: mvkUnit,
  )

proc kind*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  let kind = case args[0].value.kind
  of mvkString:
    args[0].value.str.strval
  else:
    raise newError(errkInvalidType, call_site)
  index.meta.kind = some(kind)
  return MacroValue(
    span: call_site,
    ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef,
    kind: mvkUnit,
  )

proc repository*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  let repository = case args[0].value.kind
  of mvkString:
    args[0].value.str.strval
  else:
    raise newError(errkInvalidType, call_site)
  index.meta.repository = some(repository)
  return MacroValue(
    span: call_site,
    ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef,
    kind: mvkUnit,
  )

proc dependencies*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  let dependencies = case args[0].value.kind
  of mvkSlice:
    var dependencies = newSeq[string]()
    for dependency in args[0].value.slice.arr[][args[0].value.slice.startidx ..< args[0].value.slice.endidx]:
      case dependency.kind
      of mvkString:
        dependencies.add(dependency.str.strval)
      else:
        raise newError(errkInvalidType, call_site)
    dependencies
  else:
    raise newError(errkInvalidType, call_site)
  index.meta.dependencies = some(dependencies)
  return MacroValue(
    span: call_site,
    ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef,
    kind: mvkUnit,
  )

proc stringify*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  let str = case args[0].value.kind
  of mvkNode:
    args[0].value.node.node.meta.span.get
  else:
    raise newError(errkInvalidType, call_site)
  return MacroValue(
    span: call_site,
    ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef,
    kind: mvkNode,
    node: MacroNode(node: newNode(newMeta(call_site), Hlir(kind: hrkExprString, expr_string: HlirExprString(value: str)))),
  )
