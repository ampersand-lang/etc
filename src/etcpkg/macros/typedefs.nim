import tables
import options
import ../span
import ../path
import ../index
import ../driver/driver_typedefs
import ../types/types
import ../hlir/hlir

type
  MacroValueKind* = enum
    mvkFfi
    mvkSpan
    mvkNode
    mvkUninit
    mvkUnit
    mvkBool
    mvkSigned
    mvkUnsigned
    mvkFloat
    mvkString
    mvkStruct
    mvkUnion
    mvkEnum
    mvkTagged
    mvkPointer
    mvkArray
    mvkSlice
    mvkVar
    mvkFunction
  MacroValue* = ref object
    span*: Span
    ty*: ref TypeSum
    case kind*: MacroValueKind
    of mvkUninit: uninit*: MacroUninit
    of mvkUnit: unit*: MacroUnit
    of mvkBool: p*: MacroBool
    of mvkSigned: signed*: MacroSigned
    of mvkUnsigned: unsigned*: MacroUnsigned
    of mvkFloat: floating*: MacroFloat
    of mvkString: str*: MacroString
    of mvkStruct: struct*: MacroStruct
    of mvkUnion: union*: MacroUnion
    of mvkEnum: enumeration*: MacroEnum
    of mvkTagged: tagged*: MacroTagged
    of mvkPointer: point*: MacroPointer
    of mvkArray: arr*: MacroArray
    of mvkSlice: slice*: MacroSlice
    of mvkVar: vararr*: MacroVararr
    of mvkFunction: fn*: MacroFunction
    of mvkFfi: ffi*: MacroFfi
    of mvkSpan: mspan*: MacroSpan
    of mvkNode: node*: MacroNode
  MacroFfi* = object
    name*: string
    params*: seq[string]
    noeval*: bool
    ffi*: proc (ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue
  MacroSpan* = object
    span*: Span
  MacroNode* = object
    node*: ref Node
  MacroUninit* = object
  MacroUnit* = object
  MacroBool* = object
    boolval*: bool
  MacroSigned* = object
    intval*: int64
  MacroUnsigned* = object
    intval*: uint64
  MacroFloat* = object
    floatval*: float64
  MacroString* = object
    strval*: string
  MacroStruct* = object
    fields*: Table[string, MacroValue]
  MacroUnion* = object
    fields*: Table[string, MacroValueKind]
    data*: seq[uint8]
  MacroEnum* = object
    name*: string
    intval*: int64
  MacroTagged* = object
    name*: string
    intval*: int64
    fields*: Table[string, MacroValue]
  MacroPointer* = object
    pointee*: MacroVariable
  MacroArray* = object
    len*: int
    elems*: ref seq[MacroValue]
  MacroSlice* = object
    startidx*: int
    endidx*: int
    arr*: ref seq[MacroValue]
  MacroVararr* = object
    len*: int
    elems*: seq[MacroValue]
  MacroFunction* = object
    name*: Path
    params*: seq[tuple[name: string, ty: ref QualifiedType]]
    body*: ref Node
  MacroVariableKind* = enum
    mvakDeref
    mvakIndex
    mvakField
    mvakNamed
    mvakRval
  MacroVariable* = ref object
    val*: MacroValue # do not use if kind == mvakNamed
    case kind*: MacroVariableKind
    of mvakDeref: nil
    of mvakIndex: idx*: int
    of mvakField: field*: string
    of mvakNamed: name*: string
    of mvakRval: nil
  MacroBreak* = enum
    mbNone
    mbReturn
    mbBreak
    mbContinue
  MacroContext* = ref object
    type_ctx*: TypeContext
    bindings*: seq[Table[string, MacroVariable]]
