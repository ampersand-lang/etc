import tables
import options
import sequtils
import strformat
import ../ice
import ../error
import ../span
import ../path
import ../names
import ../bits
import ../print
import ../index
import ../types/types
import ../hlir/hlir
import ../hlir/visitor
import typedefs
import builtin

proc load*(ctx: MacroContext, val: MacroVariable): MacroValue
proc eval(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable]
proc eval*(ctx: MacroContext, index: ref Index, self: ref Node): tuple[br: MacroBreak, value: MacroVariable]
proc call*(ctx: MacroContext, index: ref Index, fn: MacroFunction, args: seq[MacroValue]): MacroValue
proc to_node*(value: MacroValue): ref Node

proc eval*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  let arg = args[0].value.node.node
  return MacroValue(
    span: call_site,
    ty: ctx.type_ctx.get(["__lang", "macros", "Node"].toPath).toTypeSumRef,
    kind: mvkNode,
    node: MacroNode(node: ctx.load(ctx.eval(index, arg).value).to_node),
  )

proc quote*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  let arg = args[0].value.node.node
  return MacroValue(
    span: call_site,
    ty: ctx.type_ctx.get(["__lang", "macros", "Node"].toPath).toTypeSumRef,
    kind: mvkNode,
    node: MacroNode(node: arg),
  )

proc quasiquote*(ctx: MacroContext, index: ref Index, call_site: Span, args: seq[tuple[name: string, value: MacroValue]]): MacroValue =
  let
    pre_visitor = HlirVisitor(
      visit_expr_interp: some(proc (self: ref Node, parent: ref Node): VisitResultKind {.closure.} =
        let quiet = self.node.expr_interp.quiet
        self[] = ctx.load(ctx.eval(index, self.node.expr_interp.value).value).node.node[]
        if self.node.kind == hrkExprNamed:
          self.node.expr_named.quiet = quiet
        return vrkContinue
      )
    )
    post_visitor = HlirVisitor()

  let arg = args[0].value.node.node.deepCopy

  discard accept(arg, pre_visitor, post_visitor)

  return MacroValue(
    span: call_site,
    ty: ctx.type_ctx.get(["__lang", "macros", "Node"].toPath).toTypeSumRef,
    kind: mvkNode,
    node: MacroNode(node: arg),
  )

proc newMacroContext*(type_ctx: TypeContext): MacroContext =
  let bindings = initTable[string, MacroVariable]()
  return MacroContext(type_ctx: type_ctx, bindings: @[bindings])

proc get_var*(ctx: MacroContext, name: string): Option[MacroVariable] =
  for i in 0 ..< ctx.bindings.len:
    let i = ctx.bindings.len - i - 1
    if ctx.bindings[i].contains(name):
      return some(ctx.bindings[i][name])

proc set_var*(ctx: MacroContext, name: string, value: MacroValue) =
  for i in 0 ..< ctx.bindings.len:
    let i = ctx.bindings.len - i - 1
    if ctx.bindings[i].contains(name):
      ctx.bindings[i][name].val = value

proc enter*(ctx: MacroContext) =
  ctx.bindings.add(initTable[string, MacroVariable]())

proc leave*(ctx: MacroContext) =
  discard ctx.bindings.pop()

proc bind_var*(ctx: MacroContext, name: string, variable: MacroVariable) =
  ctx.bindings[^1].add(name, variable)

proc to_macro_value*(span: Span): MacroValue =
  return MacroValue(kind: mvkSpan, span: span, mspan: MacroSpan(span: span))

proc to_macro_value*(node: ref Node, is_typed: bool): MacroValue =
  result = MacroValue(kind: mvkNode, span: node.meta.span, node: MacroNode(node: node))
  if is_typed:
    result = MacroValue(kind: mvkStruct, span: node.meta.span, struct: MacroStruct(fields: {"node": result}.toTable))

proc to_macro_value*(str: string, span: Span): MacroValue =
  return MacroValue(kind: mvkString, span: span, str: MacroString(strval: str))

proc to_macro_value*(intval: int, span: Span): MacroValue =
  return MacroValue(kind: mvkSigned, span: span, signed: MacroSigned(intval: intval))

proc to_macro_value*(boolval: bool, span: Span): MacroValue =
  return MacroValue(kind: mvkBool, span: span, p: MacroBool(boolval: boolval))

proc load*(ctx: MacroContext, val: MacroVariable): MacroValue =
  case val.kind
  of mvakRval:
    return val.val
  of mvakDeref:
    return ctx.load(val.val.point.pointee)
  of mvakIndex:
    case val.val.kind
    of mvkArray:
      return val.val.arr.elems[val.idx]
    of mvkSlice:
      return val.val.slice.arr[val.val.slice.startidx + val.idx]
    else:
      discard # should be unreachable
  of mvakField:
    case val.val.kind
    of mvkSpan:
      case val.field
      of "directory":
        return val.val.mspan.span.directory.to_macro_value(val.val.span)
      of "file":
        return val.val.mspan.span.file.to_macro_value(val.val.span)
      of "line":
        return val.val.mspan.span.line.to_macro_value(val.val.span)
      of "col":
        return val.val.mspan.span.col.to_macro_value(val.val.span)
    of mvkStruct:
      return val.val.struct.fields[val.field]
    else:
      raise newIce("unimplemented")
  of mvakNamed:
    return ctx.load(ctx.get_var(val.name).get)

proc assign(ctx: MacroContext, dest: MacroVariable, value: MacroValue) =
  case dest.kind
  of mvakRval:
    raise newError(errkNoLValue, dest.val.span)
  of mvakDeref:
    ctx.assign(dest.val.point.pointee, value)
  of mvakIndex:
    case dest.val.kind
    of mvkArray:
      dest.val.arr.elems[dest.idx] = value
    of mvkSlice:
      dest.val.slice.arr[dest.val.slice.startidx + dest.idx] = value
    else:
      discard # should be unreachable
  of mvakField:
    case dest.val.kind
    of mvkStruct:
      dest.val.struct.fields[dest.field] = value
    else:
      raise newIce("unimplemented")
  of mvakNamed:
    ctx.set_var(dest.name, value)

proc eval_seq(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_null(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  return (mbNone, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef, kind: mvkUnit)))

proc eval_mod(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_use(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_useextern(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_trait(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_struct(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_union(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_enum(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_enumvariant(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_tagged(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_taggedvariant(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_typedef(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_static(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_static_extern(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_define(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_function(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_macro(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_with(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_type_type(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_type_pointer(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_type_array(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_type_slice(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_type_var(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_type_function(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_type_never(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_type_unit(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_type_bool(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_type_signed(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_type_unsigned(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_type_floating(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_stmt_if(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  let cond = self.node.stmt_if.cond
  let body = self.node.stmt_if.body

  let ceval = ctx.eval(index, cond, self)
  case ceval.br
  of mbReturn:
    return ceval
  of mbBreak:
    return (mbBreak, ceval.value)
  of mbContinue:
    return (mbContinue, ceval.value)
  of mbNone:
    discard

  if ctx.load(ceval.value).p.boolval:
    let beval = ctx.eval(index, body, self)
    case beval.br
    of mbReturn: return beval
    of mbBreak: return (mbBreak, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef, kind: mvkUnit)))
    of mbContinue: return (mbContinue, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef, kind: mvkUnit)))
    of mbNone: discard
  else:
    var elif_found = false
    if self.node.stmt_if.elifs.len > 0:
      for (cond, body) in self.node.stmt_if.elifs:
        let ceval = ctx.eval(index, cond, self)
        case ceval.br
        of mbReturn:
          return ceval
        of mbBreak:
          return (mbBreak, ceval.value)
        of mbContinue:
          return (mbContinue, ceval.value)
        of mbNone:
          discard

        if ctx.load(ceval.value).p.boolval:
          elif_found = true
          let beval = ctx.eval(index, body, self)
          case beval.br
          of mbReturn: return beval
          of mbBreak: return (mbBreak, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef, kind: mvkUnit)))
          of mbContinue: return (mbContinue, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef, kind: mvkUnit)))
          of mbNone: discard
    if not elif_found and self.node.stmt_if.el.isSome:
      let beval = ctx.eval(index, self.node.stmt_if.el.get, self)
      case beval.br
      of mbReturn: return beval
      of mbBreak: return (mbBreak, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef, kind: mvkUnit)))
      of mbContinue: return (mbContinue, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef, kind: mvkUnit)))
      of mbNone: discard

  return (mbNone, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef, kind: mvkUnit)))

proc eval_stmt_while(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  let cond = self.node.stmt_while.cond
  let body = self.node.stmt_while.body
  while true:
    let ceval = ctx.eval(index, cond, self)
    case ceval.br
    of mbReturn:
      return ceval
    of mbBreak:
      return (mbBreak, ceval.value)
    of mbContinue:
      return (mbContinue, ceval.value)
    of mbNone:
      discard

    if not ctx.load(ceval.value).p.boolval:
      break

    let beval = ctx.eval(index, body, self)
    case beval.br
    of mbReturn: return beval
    of mbBreak: break
    of mbContinue: continue
    of mbNone: discard
  return (mbNone, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef, kind: mvkUnit)))

proc eval_stmt_dowhile(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_stmt_for(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_stmt_foreach(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_stmt_match(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_expr_interp(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_expr_compound(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  ctx.enter
  var eval_last = true
  result = (mbNone, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef, kind: mvkUnit)))
  for child in self.node.expr_compound.elems:
    let eval = ctx.eval(index, child, self)
    case eval.br
    of mbReturn:
      eval_last = false
      result = eval
      break
    of mbBreak:
      eval_last = false
      result = (mbBreak, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef, kind: mvkUnit)))
      break
    of mbContinue:
      eval_last = false
      result = (mbContinue, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef, kind: mvkUnit)))
      break
    of mbNone:
      discard
  if eval_last and self.node.expr_compound.last.isSome:
    let eval = ctx.eval(index, self.node.expr_compound.last.get, self)
    case eval.br
    of mbReturn:
      result = eval
    of mbBreak:
      result = (mbBreak, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef, kind: mvkUnit)))
    of mbContinue:
      result = (mbContinue, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef, kind: mvkUnit)))
    of mbNone:
      result = (mbNone, MacroVariable(kind: mvakRval, val: ctx.load(eval.value)))
  ctx.leave

proc eval_expr_parenth(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_expr_fail(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_expr_defer(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_expr_return(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  if self.node.expr_return.value.isSome:
    let eval = ctx.eval(index, self.node.expr_return.value.get, self)
    case eval.br
    of mbReturn: return eval
    of mbBreak: return (mbBreak, eval.value)
    of mbContinue: return (mbContinue, eval.value)
    of mbNone: return (mbReturn, MacroVariable(kind: mvakRval, val: ctx.load(eval.value)))
  else:
    return (mbReturn, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, ty: ctx.type_ctx.get("unit".toPath).toTypeSumRef, kind: mvkUnit)))

proc eval_expr_continue(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_expr_break(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_expr_let(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  var value = MacroValue(kind: mvkUninit, ty: self.meta.type_sum)
  if self.node.expr_let.value.isSome:
    let eval = ctx.eval(index, self.node.expr_let.value.get, self)
    case eval.br
    of mbReturn: return eval
    of mbBreak: return (mbBreak, eval.value)
    of mbContinue: return (mbContinue, eval.value)
    of mbNone: discard
    value = ctx.load(eval.value)
  ctx.bind_var(self.node.expr_let.name.get.mangled.get, MacroVariable(kind: mvakRval, val: value))
  return (mbNone, ctx.get_var(self.node.expr_let.name.get.mangled.get).get)

proc eval_expr_int(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  return (mbNone, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, ty: self.meta.type_sum, kind: mvkSigned, signed: MacroSigned(intval: self.node.expr_int.value))))

proc eval_expr_float(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_expr_bool(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  return (mbNone, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, ty: self.meta.type_sum, kind: mvkBool, p: MacroBool(boolval: self.node.expr_bool.value))))

proc eval_expr_unit(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_expr_string(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  return (mbNone, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, ty: self.meta.type_sum, kind: mvkString, str: MacroString(strval: self.node.expr_string.value))))

proc eval_expr_named(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  return (mbNone, MacroVariable(kind: mvakNamed, name: self.node.expr_named.name.mangled.get))

proc eval_expr_array(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  var arr: ref seq[MacroValue]
  new arr
  arr[] = newSeq[MacroValue]()
  for elem in self.node.expr_array.elems:
    let elem = ctx.eval(index, elem, self)
    case elem.br
    of mbReturn: return elem
    of mbBreak: return (mbBreak, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, kind: mvkUnit)))
    of mbContinue: return (mbContinue, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, kind: mvkUnit)))
    of mbNone: arr[].add(ctx.load(elem.value))
  case self.meta.type_sum[].as_qualified_in_span(self.meta.span).inner.kind
  of tykSlice:
    return (mbNone, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, kind: mvkSlice, slice: MacroSlice(startidx: 0, endidx: arr[].len, arr: arr))))
  of tykArray:
    return (mbNone, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, kind: mvkArray, arr: MacroArray(len: arr[].len, elems: arr))))
  else:
    discard # unreachable

proc eval_expr_application(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  let eval = ctx.eval(index, self.node.expr_application.fn, self)
  case eval.br
  of mbReturn: return eval
  of mbBreak: return (mbBreak, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, kind: mvkUnit)))
  of mbContinue: return (mbContinue, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, kind: mvkUnit)))
  of mbNone: discard

  let fn = ctx.load(eval.value)
  case fn.kind
  of mvkFfi:
    var args: seq[tuple[br: MacroBreak, value: MacroVariable]]
    if fn.ffi.noeval:
      args = self.node.expr_application.args.mapIt((mbNone, MacroVariable(kind: mvakRval, val: MacroValue(span: it.meta.span, kind: mvkNode, node: MacroNode(node: it)))))
    else:
      args = self.node.expr_application.args.mapIt(ctx.eval(index, it, self))
    if args.len != fn.ffi.params.len:
      raise newIce(fmt"invalid arity, expected {fn.ffi.params.len} but got {args.len}")
    var params = newSeq[tuple[name: string, value: MacroValue]]()
    for i, param in fn.ffi.params:
      case args[i].br
      of mbReturn: return args[i]
      of mbBreak: return (mbBreak, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, kind: mvkUnit)))
      of mbContinue: return (mbContinue, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, kind: mvkUnit)))
      of mbNone: discard
      params.add((param, ctx.load(args[i].value)))
    return (mbNone, MacroVariable(kind: mvakRval, val: fn.ffi.ffi(ctx, index, self.meta.span, params)))
  of mvkFunction:
    let args = self.node.expr_application.args.mapIt(ctx.eval(index, it, self))
    var params = newSeq[MacroValue]()
    for arg in args:
      case arg.br
      of mbReturn: return arg
      of mbBreak: return (mbBreak, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, kind: mvkUnit)))
      of mbContinue: return (mbContinue, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, kind: mvkUnit)))
      of mbNone: discard
      params.add(ctx.load(arg.value))
    return (mbNone, MacroVariable(kind: mvakRval, val: ctx.call(index, fn.fn, params)))
  else:
    raise newIce("unimplemented")

proc eval_expr_command(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  let eval = ctx.eval(index, self.node.expr_command.fn, self)
  case eval.br
  of mbReturn: return eval
  of mbBreak: return (mbBreak, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, kind: mvkUnit)))
  of mbContinue: return (mbContinue, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, kind: mvkUnit)))
  of mbNone: discard

  let fn = ctx.load(eval.value)
  case fn.kind
  of mvkFfi:
    var args: seq[tuple[br: MacroBreak, value: MacroVariable]]
    if fn.ffi.noeval:
      args = self.node.expr_command.args.mapIt((mbNone, MacroVariable(kind: mvakRval, val: MacroValue(span: it.meta.span, kind: mvkNode, node: MacroNode(node: it)))))
    else:
      args = self.node.expr_command.args.mapIt(ctx.eval(index, it, self))
    if args.len != fn.ffi.params.len:
      raise newIce(fmt"invalid arity, expected {fn.ffi.params.len} but got {args.len}")
    var params = newSeq[tuple[name: string, value: MacroValue]]()
    for i, param in fn.ffi.params:
      case args[i].br
      of mbReturn: return args[i]
      of mbBreak: return (mbBreak, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, kind: mvkUnit)))
      of mbContinue: return (mbContinue, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, kind: mvkUnit)))
      of mbNone: discard
      params.add((param, ctx.load(args[i].value)))
    return (mbNone, MacroVariable(kind: mvakRval, val: fn.ffi.ffi(ctx, index, self.meta.span, params)))
  of mvkFunction:
    let args = self.node.expr_command.args.mapIt(ctx.eval(index, it, self))
    var params = newSeq[MacroValue]()
    for arg in args:
      case arg.br
      of mbReturn: return arg
      of mbBreak: return (mbBreak, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, kind: mvkUnit)))
      of mbContinue: return (mbContinue, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, kind: mvkUnit)))
      of mbNone: discard
      params.add(ctx.load(arg.value))
    return (mbNone, MacroVariable(kind: mvakRval, val: ctx.call(index, fn.fn, params)))
  else:
    raise newIce("unimplemented")

proc eval_expr_fieldaccess(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  let eval = ctx.eval(index, self.node.expr_field_access.value, self)
  case eval.br
  of mbReturn: return eval
  of mbBreak: return (mbBreak, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, kind: mvkUnit)))
  of mbContinue: return (mbContinue, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, kind: mvkUnit)))
  of mbNone: discard

  let
    lhs = ctx.load(eval.value)
    field = self.node.expr_field_access.name
  case lhs.kind
  of mvkSpan:
    return (mbNone, MacroVariable(kind: mvakField, val: lhs, field: field))
  of mvkStruct:
    return (mbNone, MacroVariable(kind: mvakField, val: lhs, field: field))
  else:
    raise newIce(fmt"unimplemented: {lhs.kind}.{field}")

proc eval_expr_cast(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  let eval = ctx.eval(index, self.node.expr_cast.value, self)
  case eval.br
  of mbReturn: return eval
  of mbBreak: return (mbBreak, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, kind: mvkUnit)))
  of mbContinue: return (mbContinue, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, kind: mvkUnit)))
  of mbNone: discard

  let value = ctx.load(eval.value)
  value.ty = self.node.expr_cast.ty.to_type(ctx.type_ctx).toTypeSumRef
  return (mbNone, MacroVariable(kind: mvakRval, val: value))

proc eval_expr_sizeof(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  let
    size = self.node.expr_sizeof.value.to_type(ctx.type_ctx).get_size_of(ctx.type_ctx)
    size_real = if size.kind == sokSized: size.size else: 0
  return (mbNone, MacroVariable(kind: mvakRval, val: MacroValue(kind: mvkUnsigned, span: self.meta.span, ty: ctx.type_ctx.get("uint".toPath).toTypeSumRef, unsigned: MacroUnsigned(intval: size_real))))

proc eval_expr_alignof(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  let
    align = self.node.expr_alignof.value.to_type(ctx.type_ctx).get_align_of(ctx.type_ctx)
    align_real = if align.kind == sokSized: align.size else: 0
  return (mbNone, MacroVariable(kind: mvakRval, val: MacroValue(kind: mvkUnsigned, span: self.meta.span, ty: ctx.type_ctx.get("uint".toPath).toTypeSumRef, unsigned: MacroUnsigned(intval: align_real))))

proc eval_expr_unary(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  let eval = ctx.eval(index, self.node.expr_unary.value, self)
  case eval.br
  of mbReturn: return eval
  of mbBreak: return (mbBreak, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, kind: mvkUnit)))
  of mbContinue: return (mbContinue, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, kind: mvkUnit)))
  of mbNone: discard

  let value = eval.value
  case self.node.expr_unary.op
  of hukPostInc:
    result = (mbNone, MacroVariable(kind: mvakRval, val: ctx.load(value)))
    ctx.assign(value, ctx.load(value).increment)
  of hukPostDec:
    result = (mbNone, MacroVariable(kind: mvakRval, val: ctx.load(value)))
    ctx.assign(value, ctx.load(value).decrement)
  of hukPreInc:
    ctx.assign(value, ctx.load(value).increment)
    return (mbNone, MacroVariable(kind: mvakRval, val: ctx.load(value)))
  of hukPreDec:
    ctx.assign(value, ctx.load(value).decrement)
    return (mbNone, MacroVariable(kind: mvakRval, val: ctx.load(value)))
  of hukRef:
    return (mbNone, MacroVariable(kind: mvakRval, val: MacroValue(kind: mvkPointer, point: MacroPointer(pointee: value))))
  of hukDeref:
    return (mbNone, MacroVariable(kind: mvakDeref, val: ctx.load(value)))
  of hukPos:
    return (mbNone, MacroVariable(kind: mvakRval, val: ctx.load(value)))
  of hukNeg:
    return (mbNone, MacroVariable(kind: mvakRval, val: -ctx.load(value)))
  of hukNot:
    return (mbNone, MacroVariable(kind: mvakRval, val: not ctx.load(value)))
  of hukColon:
    raise newIce("unimplemented")
  of hukRangeFrom:
    return (mbNone, MacroVariable(kind: mvakRval, val: macro_range_from(ctx.type_ctx, ctx.load(value))))
  of hukRangeTo:
    return (mbNone, MacroVariable(kind: mvakRval, val: macro_range_to(ctx.type_ctx, ctx.load(value))))

proc eval_expr_binary(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  let leval = ctx.eval(index, self.node.expr_binary.lhs, self)
  case leval.br
  of mbReturn: return leval
  of mbBreak: return (mbBreak, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, kind: mvkUnit)))
  of mbContinue: return (mbContinue, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, kind: mvkUnit)))
  of mbNone: discard

  case self.node.expr_binary.op
  of hbkLogicalAnd:
    let lhs = ctx.load(leval.value)
    case lhs.kind
    of mvkBool:
      if lhs.p.boolval:
        let reval = ctx.eval(index, self.node.expr_binary.rhs, self)
        case reval.br
        of mbReturn: return reval
        of mbBreak: return (mbBreak, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, kind: mvkUnit)))
        of mbContinue: return (mbContinue, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, kind: mvkUnit)))
        of mbNone: discard
        return (mbNone, MacroVariable(kind: mvakRval, val: ctx.load(reval.value)))
      else:
        return (mbNone, MacroVariable(kind: mvakRval, val: lhs))
    else:
      raise newError(errkInvalidType, lhs.span)
  of hbkLogicalOr:
    let lhs = ctx.load(leval.value)
    case lhs.kind
    of mvkBool:
      if lhs.p.boolval:
        return (mbNone, MacroVariable(kind: mvakRval, val: lhs))
      else:
        let reval = ctx.eval(index, self.node.expr_binary.rhs, self)
        case reval.br
        of mbReturn: return reval
        of mbBreak: return (mbBreak, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, kind: mvkUnit)))
        of mbContinue: return (mbContinue, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, kind: mvkUnit)))
        of mbNone: discard
        return (mbNone, MacroVariable(kind: mvakRval, val: ctx.load(reval.value)))
    else:
      raise newError(errkInvalidType, lhs.span)
  else:
    discard

  let reval = ctx.eval(index, self.node.expr_binary.rhs, self)
  case reval.br
  of mbReturn: return reval
  of mbBreak: return (mbBreak, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, kind: mvkUnit)))
  of mbContinue: return (mbContinue, MacroVariable(kind: mvakRval, val: MacroValue(span: self.meta.span, kind: mvkUnit)))
  of mbNone: discard

  let lhs = leval.value
  let rhs = reval.value
  case self.node.expr_binary.op
  of hbkIndex:
    discard
  of hbkAdd:
    return (mbNone, MacroVariable(kind: mvakRval, val: ctx.load(lhs) + ctx.load(rhs)))
  of hbkSub:
    return (mbNone, MacroVariable(kind: mvakRval, val: ctx.load(lhs) - ctx.load(rhs)))
  of hbkMul:
    return (mbNone, MacroVariable(kind: mvakRval, val: ctx.load(lhs) * ctx.load(rhs)))
  of hbkDiv:
    return (mbNone, MacroVariable(kind: mvakRval, val: ctx.load(lhs) / ctx.load(rhs)))
  of hbkRem:
    return (mbNone, MacroVariable(kind: mvakRval, val: ctx.load(lhs) mod ctx.load(rhs)))
  of hbkShl:
    return (mbNone, MacroVariable(kind: mvakRval, val: ctx.load(lhs) shl ctx.load(rhs)))
  of hbkShr:
    return (mbNone, MacroVariable(kind: mvakRval, val: ctx.load(lhs) shr ctx.load(rhs)))
  of hbkLt:
    return (mbNone, MacroVariable(kind: mvakRval, val: builtin.lt(ctx.type_ctx, ctx.load(lhs), ctx.load(rhs))))
  of hbkGt:
    return (mbNone, MacroVariable(kind: mvakRval, val: builtin.gt(ctx.type_ctx, ctx.load(lhs), ctx.load(rhs))))
  of hbkLe:
    return (mbNone, MacroVariable(kind: mvakRval, val: builtin.le(ctx.type_ctx, ctx.load(lhs), ctx.load(rhs))))
  of hbkGe:
    return (mbNone, MacroVariable(kind: mvakRval, val: builtin.ge(ctx.type_ctx, ctx.load(lhs), ctx.load(rhs))))
  of hbkEq:
    return (mbNone, MacroVariable(kind: mvakRval, val: builtin.eq(ctx.type_ctx, ctx.load(lhs), ctx.load(rhs))))
  of hbkNe:
    return (mbNone, MacroVariable(kind: mvakRval, val: builtin.ne(ctx.type_ctx, ctx.load(lhs), ctx.load(rhs))))
  of hbkBitand:
    return (mbNone, MacroVariable(kind: mvakRval, val: ctx.load(lhs) and ctx.load(rhs)))
  of hbkBitor:
    return (mbNone, MacroVariable(kind: mvakRval, val: ctx.load(lhs) or ctx.load(rhs)))
  of hbkBitxor:
    return (mbNone, MacroVariable(kind: mvakRval, val: ctx.load(lhs) xor ctx.load(rhs)))
  of hbkLogicalAnd:
    discard # unreachable
  of hbkLogicalOr:
    discard # unreachable
  of hbkRange:
    return (mbNone, MacroVariable(kind: mvakRval, val: macro_range(ctx.type_ctx, ctx.load(lhs), ctx.load(rhs))))
  of hbkAssign:
    ctx.assign(lhs, ctx.load(rhs))
    return (mbNone, MacroVariable(kind: mvakRval, val: ctx.load(lhs)))
  of hbkAssignMul:
    ctx.assign(lhs, ctx.load(lhs) * ctx.load(rhs))
    return (mbNone, MacroVariable(kind: mvakRval, val: ctx.load(lhs)))
  of hbkAssignDiv:
    ctx.assign(lhs, ctx.load(lhs) / ctx.load(rhs))
    return (mbNone, MacroVariable(kind: mvakRval, val: ctx.load(lhs)))
  of hbkAssignRem:
    ctx.assign(lhs, ctx.load(lhs) mod ctx.load(rhs))
    return (mbNone, MacroVariable(kind: mvakRval, val: ctx.load(lhs)))
  of hbkAssignAdd:
    ctx.assign(lhs, ctx.load(lhs) + ctx.load(rhs))
    return (mbNone, MacroVariable(kind: mvakRval, val: ctx.load(lhs)))
  of hbkAssignSub:
    ctx.assign(lhs, ctx.load(lhs) - ctx.load(rhs))
    return (mbNone, MacroVariable(kind: mvakRval, val: ctx.load(lhs)))
  of hbkAssignLshift:
    ctx.assign(lhs, ctx.load(lhs) shl ctx.load(rhs))
    return (mbNone, MacroVariable(kind: mvakRval, val: ctx.load(lhs)))
  of hbkAssignRshift:
    ctx.assign(lhs, ctx.load(lhs) shr ctx.load(rhs))
    return (mbNone, MacroVariable(kind: mvakRval, val: ctx.load(lhs)))
  of hbkAssignAnd:
    ctx.assign(lhs, ctx.load(lhs) and ctx.load(rhs))
    return (mbNone, MacroVariable(kind: mvakRval, val: ctx.load(lhs)))
  of hbkAssignXor:
    ctx.assign(lhs, ctx.load(lhs) xor ctx.load(rhs))
    return (mbNone, MacroVariable(kind: mvakRval, val: ctx.load(lhs)))
  of hbkAssignOr:
    ctx.assign(lhs, ctx.load(lhs) or ctx.load(rhs))
    return (mbNone, MacroVariable(kind: mvakRval, val: ctx.load(lhs)))
  of hbkArrow:
    raise newIce("unimplemented")

proc eval_expr_ternary(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_expr_range_full(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_pat_bind(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_pat_destructure(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_pat_else(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_decl(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval_case(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  discard

proc eval*(ctx: MacroContext, index: ref Index, self: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  return eval(ctx, index, self, self)

proc eval(ctx: MacroContext, index: ref Index, self: ref Node, parent: ref Node): tuple[br: MacroBreak, value: MacroVariable] =
  case self.node.kind
  of hrkRoot: return eval(ctx, index, self.node.root.root, self)
  of hrkSeq: return eval_seq(ctx, index, self, parent)
  of hrkNull: return eval_null(ctx, index, self, parent)
  of hrkMod: return eval_mod(ctx, index, self, parent)
  of hrkUse: return eval_use(ctx, index, self, parent)
  of hrkUseExtern: return eval_useextern(ctx, index, self, parent)
  of hrkTrait: return eval_trait(ctx, index, self, parent)
  of hrkStruct: return eval_struct(ctx, index, self, parent)
  of hrkUnion: return eval_union(ctx, index, self, parent)
  of hrkEnum: return eval_enum(ctx, index, self, parent)
  of hrkEnumVariant: return eval_enumvariant(ctx, index, self, parent)
  of hrkTagged: return eval_tagged(ctx, index, self, parent)
  of hrkTaggedVariant: return eval_taggedvariant(ctx, index, self, parent)
  of hrkTypedef: return eval_typedef(ctx, index, self, parent)
  of hrkStatic: return eval_static(ctx, index, self, parent)
  of hrkStaticExtern: return eval_static_extern(ctx, index, self, parent)
  of hrkDefine: return eval_define(ctx, index, self, parent)
  of hrkFunction: return eval_function(ctx, index, self, parent)
  of hrkMacro: return eval_macro(ctx, index, self, parent)
  of hrkWith: return eval_with(ctx, index, self, parent)
  of hrkTypeType: return eval_type_type(ctx, index, self, parent)
  of hrkTypePointer: return eval_type_pointer(ctx, index, self, parent)
  of hrkTypeArray: return eval_type_array(ctx, index, self, parent)
  of hrkTypeSlice: return eval_type_slice(ctx, index, self, parent)
  of hrkTypeVar: return eval_type_var(ctx, index, self, parent)
  of hrkTypeFunction: return eval_type_function(ctx, index, self, parent)
  of hrkTypeNever: return eval_type_never(ctx, index, self, parent)
  of hrkTypeUnit: return eval_type_unit(ctx, index, self, parent)
  of hrkTypeBool: return eval_type_bool(ctx, index, self, parent)
  of hrkTypeSigned: return eval_type_signed(ctx, index, self, parent)
  of hrkTypeUnsigned: return eval_type_unsigned(ctx, index, self, parent)
  of hrkTypeFloating: return eval_type_floating(ctx, index, self, parent)
  of hrkStmtIf: return eval_stmt_if(ctx, index, self, parent)
  of hrkStmtWhile: return eval_stmt_while(ctx, index, self, parent)
  of hrkStmtDowhile: return eval_stmt_dowhile(ctx, index, self, parent)
  of hrkStmtFor: return eval_stmt_for(ctx, index, self, parent)
  of hrkStmtForeach: return eval_stmt_foreach(ctx, index, self, parent)
  of hrkStmtMatch: return eval_stmt_match(ctx, index, self, parent)
  of hrkExprTrait: discard
  of hrkExprStruct: discard
  of hrkExprUnion: discard
  of hrkExprEnum: discard
  of hrkExprTagged: discard
  of hrkExprInterp: return eval_expr_interp(ctx, index, self, parent)
  of hrkExprCompound: return eval_expr_compound(ctx, index, self, parent)
  of hrkExprParenth: return eval_expr_parenth(ctx, index, self, parent)
  of hrkExprFail: return eval_expr_fail(ctx, index, self, parent)
  of hrkExprDefer: return eval_expr_defer(ctx, index, self, parent)
  of hrkExprReturn: return eval_expr_return(ctx, index, self, parent)
  of hrkExprContinue: return eval_expr_continue(ctx, index, self, parent)
  of hrkExprBreak: return eval_expr_break(ctx, index, self, parent)
  of hrkExprLet: return eval_expr_let(ctx, index, self, parent)
  of hrkExprInt: return eval_expr_int(ctx, index, self, parent)
  of hrkExprFloat: return eval_expr_float(ctx, index, self, parent)
  of hrkExprBool: return eval_expr_bool(ctx, index, self, parent)
  of hrkExprUnit: return eval_expr_unit(ctx, index, self, parent)
  of hrkExprString: return eval_expr_string(ctx, index, self, parent)
  of hrkExprNamed: return eval_expr_named(ctx, index, self, parent)
  of hrkExprArray: return eval_expr_array(ctx, index, self, parent)
  of hrkExprApplication: return eval_expr_application(ctx, index, self, parent)
  of hrkExprCommand: return eval_expr_command(ctx, index, self, parent)
  of hrkExprFieldAccess: return eval_expr_fieldaccess(ctx, index, self, parent)
  of hrkExprCast: return eval_expr_cast(ctx, index, self, parent)
  of hrkExprSizeof: return eval_expr_sizeof(ctx, index, self, parent)
  of hrkExprAlignof: return eval_expr_alignof(ctx, index, self, parent)
  of hrkExprUnary: return eval_expr_unary(ctx, index, self, parent)
  of hrkExprBinary: return eval_expr_binary(ctx, index, self, parent)
  of hrkExprTernary: return eval_expr_ternary(ctx, index, self, parent)
  of hrkExprRangeFull: return eval_expr_range_full(ctx, index, self, parent)
  of hrkPatBind: return eval_pat_bind(ctx, index, self, parent)
  of hrkPatDestructure: return eval_pat_destructure(ctx, index, self, parent)
  of hrkPatElse: return eval_pat_else(ctx, index, self, parent)
  of hrkDecl: return eval_decl(ctx, index, self, parent)
  of hrkCase: return eval_case(ctx, index, self, parent)

proc call*(ctx: MacroContext, index: ref Index, fn: MacroFunction, args: seq[MacroValue]): MacroValue =
  ctx.enter
  if fn.params.len != args.len:
    raise newIce(fmt"invalid arity, expected {fn.params.len} but got {args.len}")
  for i, param in fn.params.pairs:
    let
      path = fn.name & param.name
      name = path.mangled
    ctx.bind_var(name, MacroVariable(kind: mvakRval, val: args[i]))
  result = ctx.load(ctx.eval(index, fn.body).value)
  ctx.leave

# this is a little hacky, the compiler shouldn't have to rely on a ref Node here
proc expand*(ctx: MacroContext, index: ref Index, macrofn: ref Node, args: seq[MacroValue]): MacroValue =
  ctx.enter
  for i, param in macrofn.node.macrofn.ty.node.type_function.params.pairs:
    let
      path = macrofn.node.macrofn.name.get.full.get & param.node.decl.name.get.node.expr_named.name.short
      name = path.mangled
    ctx.bind_var(name, MacroVariable(kind: mvakRval, val: args[i]))
  result = ctx.load(ctx.eval(index, macrofn.node.macrofn.body.get).value)
  ctx.leave

proc to_node*(value: MacroValue): ref Node =
  case value.kind
  of mvkFfi:
    raise newIce("cannot convert an ffi to a node")
  of mvkSpan:
    raise newIce("unimplemented")
  of mvkNode:
    raise newIce("unimplemented")
  of mvkUninit:
    raise newIce("unimplemented")
  of mvkUnit:
    raise newIce("unimplemented")
  of mvkBool:
    return newNode(newMeta(value.span), Hlir(kind: hrkExprBool, expr_bool: HlirExprBool(value: value.p.boolval)))
  of mvkSigned:
    return newNode(newMeta(value.span), Hlir(kind: hrkExprInt, expr_int: HlirExprInt(value: value.signed.intval.int)))
  of mvkUnsigned:
    return newNode(newMeta(value.span), Hlir(kind: hrkExprInt, expr_int: HlirExprInt(value: value.unsigned.intval.int)))
  of mvkFloat:
    raise newIce("unimplemented")
  of mvkString:
    raise newIce("unimplemented")
  of mvkStruct:
    raise newIce("unimplemented")
  of mvkUnion:
    raise newIce("unimplemented")
  of mvkEnum:
    raise newIce("unimplemented")
  of mvkTagged:
    raise newIce("unimplemented")
  of mvkPointer:
    raise newIce("unimplemented")
  of mvkArray:
    raise newIce("unimplemented")
  of mvkSlice:
    raise newIce("unimplemented")
  of mvkVar:
    raise newIce("unimplemented")
  of mvkFunction:
    raise newIce("unimplemented")
