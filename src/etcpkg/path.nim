import strutils
import sequtils
import hashes
import ice

type
  # a generic kind of path used for many things, like module names, tu paths, function/type names
  Path* = object
    full*: bool
    path*: seq[string]
    glob*: bool

proc toPath*(str: string): Path =
  return Path(full: true, path: @[str], glob: false)

proc toPath*(path: openarray[string]): Path =
  return Path(full: true, path: path.toSeq, glob: false)

proc toNonFullPath*(str: string): Path =
  return Path(full: false, path: @[str], glob: false)

proc toNonFullPath*(path: openarray[string]): Path =
  return Path(full: false, path: path.toSeq, glob: false)

proc starts_with*(haystack: Path, needle: Path): bool =
  if needle.path.len > haystack.path.len:
    return false
  for i in 0 ..< needle.path.len:
    if needle.path[i] != haystack.path[i]:
      return false
  return true

proc triml*(path: Path, l: int): Path =
  result = path
  for _ in 0 ..< l:
    result.path.delete(0)

proc hash*(path: Path): Hash =
  var h: Hash = 0
  h = h !& path.full.hash
  for ident in path.path:
    h = h !& ident.hash
  h = h !& path.glob.hash
  result = !$h

proc `==`*(lhs: Path, rhs: Path): bool =
  return lhs.full == rhs.full and lhs.path == rhs.path and lhs.glob == rhs.glob

proc `$`*(path: Path): string =
  if path.full:
    result = "::" & path.path.join("::")
  else:
    result = path.path.join("::")
  if path.glob:
    result.add("::*")

proc `&`*(lhs: Path, rhs: Path): Path =
  if lhs.glob:
    raise newIce("can't concatenate glob path")
  if rhs.full:
    raise newIce("can't concatenate fully-qualified paths")
  result.path = concat(lhs.path, rhs.path)
  result.full = lhs.full

proc `&`*(lhs: Path, rhs: string): Path =
  if lhs.glob:
    raise newIce("can't concatenate glob path")
  result = lhs
  result.path.add(rhs)

iterator items*(path: Path): string =
  for ident in path.path:
    yield ident

iterator mitems*(path: var Path): var string =
  for ident in path.path.mitems:
    yield ident
