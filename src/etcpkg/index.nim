import tables
import sets
import options
import sequtils
import strutils
import strformat
import os
import streams

import ice
import error
import path
import span
import parse/lex
import hlir/hlir
import llvm/llvm
import llvm/clang

type
  TranslationStage* = enum
    tsInitial
    tsLexed
    tsParsed
    tsEarlyNormalization
    tsTypeDecl
    tsTypeAnalysis
    tsConstExprPass
    tsNameMangling
    tsFlowAnalysis
    tsCodeDecl
    tsCodeGen
    tsComplete

  PackageMetadata* = object
    name*: Option[string]
    version*: Option[string]
    authors*: Option[seq[string]]
    description*: Option[string]
    kind*: Option[string]
    repository*: Option[string]
    dependencies*: Option[seq[string]]

  IndexUnit* = object
    path: string
    source: string
    ts*: TranslationStage
    tokens*: Option[Tokens]
    root*: Option[ref Node]
  Index* = object
    target*: string
    flags*: Table[string, int]
    opts*: Table[string, seq[string]]

    table: Table[string, ref IndexUnit]
    use_dir: seq[string]
    pkg_name*: Option[string]

    compile*: OrderedSet[string]

    ctable: Table[string, tuple[ts: TranslationStage, cxtu: CXTranslationUnit]]
    cxindex*: CXIndex
    c_use_dir: seq[string]
    c_cmdline: seq[string]

    meta*: PackageMetadata

proc newIndex*(): ref Index =
  new result
  result.target = "./target"
  result.flags = initTable[string, int]()
  result.opts = initTable[string, seq[string]]()

  result.table = initTable[string, ref IndexUnit]()
  result.use_dir = newSeq[string]()
  result.use_dir.add(".")
  result.use_dir.add("~/.local/lib/etc/lib".expandTilde)
  result.pkg_name = none[string]()
  result.compile = initOrderedSet[string]()

  result.ctable = initTable[string, tuple[ts: TranslationStage, cxtu: CXTranslationUnit]]()
  result.cxindex = clang.createIndex(0, 0)
  result.c_use_dir = newSeq[string]()
  result.c_use_dir.add(".")
  result.c_use_dir.add("/usr/include")
  result.c_cmdline = newSeq[string]()
  result.c_cmdline.add("-D__AMPERSAND=1")
  result.c_cmdline.add("-I/usr/include")

proc startIndex*(index: ref Index, pkg_name: Option[string], compile: OrderedSet[string]) =
  index.pkg_name = pkg_name
  index.compile = compile

proc add_include*(index: ref Index, i: string) =
  index.use_dir.add(i)

proc add_c_define*(index: ref Index, d: string) =
  index.c_cmdline.add(fmt"-D{d}")

proc add_c_include*(index: ref Index, i: string) =
  index.c_use_dir.add(i)
  index.c_cmdline.add(fmt"-I{i}")

proc compiling*(index: ref Index, unit: ref IndexUnit): bool =
  return index.compile.contains(unit.path)

proc newIndexUnit(path: string, source: string): ref IndexUnit =
  new result
  result.path = path
  result.source = source
  result.ts = tsInitial
  result.tokens = none[Tokens]()
  result.root = none[ref Node]()

proc source*(unit: ref IndexUnit): string =
  return unit.source

proc path*(unit: ref IndexUnit): string =
  return unit.path

proc load*(index: ref Index, file_name: string): ref IndexUnit =
  var full_path: string

  if file_name.isAbsolute:
    full_path = file_name
  else:
    var found = false
    for use_dir in index.use_dir:
      let path = use_dir/file_name
      full_path = path.absolutePath
      if full_path.fileExists:
        found = true
        break
    if not found:
      return nil

  full_path.normalizePath()

  if full_path in index.table:
    return index.table[full_path]
  else:
    let
      stream = newFileStream(full_path, fmRead)
      unit = newIndexUnit(full_path, stream.readAll)
    index.table.add(full_path, unit)
    return unit

proc load*(index: ref Index, path: Path, ext: string): ref IndexUnit =
  var trunc_path = path
  if index.pkg_name.isSome and path.path[0] == index.pkg_name.get:
    trunc_path.path.delete(0, 0)

  let file_name = trunc_path.path.join("/").addFileExt(ext)
  return index.load(file_name)

proc load_with_span*(index: ref Index, path: Path, ext: string, span: Span): ref IndexUnit =
  result = index.load(path, ext)
  if result.isNil:
    raise newError(errkFileNotFound, span, $path)

proc load_c_with_span*(index: ref Index, file_name: string, span: Span): tuple[ts: TranslationStage, cxtu: CXTranslationUnit] =
  var full_path: string

  if file_name.isAbsolute:
    full_path = file_name
  else:
    var found = false
    for use_dir in index.c_use_dir:
      let path = use_dir/file_name
      full_path = path.absolutePath
      if full_path.fileExists:
        found = true
        break
    if not found:
      raise newError(errkFileNotFound, span, file_name)

  full_path.normalizePath()

  if full_path in index.ctable:
    return index.ctable[full_path]
  else:
    let
      args = allocCStringArray(index.c_cmdline)
      cxtu = clang.parseTranslationUnit(index.cxindex, full_path.cstring, addr args[0], index.c_cmdline.len.cint, nil, 0, 0)
    deallocCStringArray(args)
    var is_fatal = false
    for i in 0 ..< clang.getNumDiagnostics(cxtu):
      let
        diag = clang.getDiagnostic(cxtu, i)
        msg = clang.formatDiagnostic(diag, clang.defaultDiagnosticDisplayOptions())
        str = clang.getString(msg)
      writeLine stderr, str
      clang.disposeString(msg)
      let
        severity = clang.getDiagnosticSeverity(diag);
      if severity == CXDiagnostic_Error or severity == CXDiagnostic_Fatal:
        is_fatal = true
      clang.disposeDiagnostic(diag)

    if is_fatal:
      raise newError(errkCError, span)

    let
      root = clang.getTranslationUnitCursor(cxtu)
      null = clang.getNullCursor()
    if clang.equalCursors(root, null) != 0:
      raise newIce("invalid cxtu")

    index.ctable.add(full_path, (tsInitial, cxtu))

    return (tsInitial, cxtu)

proc update_c_tu*(index: ref Index, file_name: string, ts: TranslationStage) =
  var full_path: string

  if file_name.isAbsolute:
    full_path = file_name
  else:
    var found = false
    for use_dir in index.c_use_dir:
      let path = use_dir/file_name
      full_path = path.absolutePath
      if full_path.fileExists:
        found = true
        break
    if not found:
      raise newIce("update_c_tu called on an non-existing, unloaded C file")

  full_path.normalizePath()

  let (_, cxtu) = index.ctable[full_path]
  index.ctable[full_path] = (ts, cxtu)

proc toggle_flag*(index: ref Index, flag: string) =
  if not index.flags.contains(flag):
    index.flags.add(flag, 0)
  index.flags[flag] = if index.flags[flag] > 0: 0 else: 1

proc set_flag*(index: ref Index, flag: string, value: bool) =
  if not index.flags.contains(flag):
    index.flags.add(flag, 0)
  index.flags[flag] = if value: 1 else: 0

proc set_opt*(index: ref Index, opt: string, value: string) =
  if not index.opts.contains(opt):
    index.opts.add(opt, newSeq[string]())
  index.opts[opt].add(value)

proc unset_opt*(index: ref Index, opt: string) =
  if not index.opts.contains(opt):
    index.opts.add(opt, newSeq[string]())
  index.opts[opt] = newSeq[string]()

proc add_file*(index: ref Index, span: Span, file_name: string) =
  var full_path: string
  if file_name.isAbsolute:
    full_path = file_name
  else:
    var found = false
    for use_dir in index.use_dir:
      let path = use_dir/file_name
      full_path = path.absolutePath
      if full_path.fileExists:
        found = true
        break
    if not found:
      raise newError(errkFileNotFound, span, file_name)

  full_path.normalizePath()

  index.compile.incl(full_path)

proc add_path*(index: ref Index, dir: string) =
  index.add_include(dir)

proc add_cpath*(index: ref Index, dir: string) =
  index.add_c_include(dir)

proc add_lib*(index: ref Index, lib: string) =
  index.set_opt("l", lib)

proc set_output*(index: ref Index, file_name: string) =
  index.set_opt("o", index.target/file_name)

proc toggle_linker_flag*(index: ref Index, flag: string) =
  index.set_opt("X", flag)

proc set_linker_flag*(index: ref Index, flag: string, value: bool) =
  if not index.opts.contains("X"):
    index.opts.add("X", newSeq[string]())
  if index.opts["X"].contains(flag):
    index.unset_opt("X")
  else:
    index.set_opt("X", flag)

proc set_linker_opt*(index: ref Index, opt: string, value: string) =
  index.set_opt("X", fmt"{opt}={value}")

proc unset_linker_opt*(index: ref Index, opt: string) =
  if not index.opts.contains("X"):
    index.opts.add("X", newSeq[string]())
  var idxs = newSeq[int]()
  for i, opt in index.opts["X"]:
    if opt.startsWith(fmt"{opt}="):
      idxs.add(i)
  for i in 1 .. idxs.len:
    index.opts["X"].delete(idxs[^i])
