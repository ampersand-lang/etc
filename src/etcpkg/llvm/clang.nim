import strutils
import cstrutils
import strformat
import options

import ../ice
import ../target
import ../bits
import ../path
import ../types/types

{.passL: "-lclang".}

type
  CXCursorKind* = enum
    CXCursor_UnexposedDecl                 = 1
    CXCursor_StructDecl                    = 2
    CXCursor_UnionDecl                     = 3
    CXCursor_ClassDecl                     = 4
    CXCursor_EnumDecl                      = 5
    CXCursor_FieldDecl                     = 6
    CXCursor_EnumConstantDecl              = 7
    CXCursor_FunctionDecl                  = 8
    CXCursor_VarDecl                       = 9
    CXCursor_ParmDecl                      = 10
    CXCursor_ObjCInterfaceDecl             = 11
    CXCursor_ObjCCategoryDecl              = 12
    CXCursor_ObjCProtocolDecl              = 13
    CXCursor_ObjCPropertyDecl              = 14
    CXCursor_ObjCIvarDecl                  = 15
    CXCursor_ObjCInstanceMethodDecl        = 16
    CXCursor_ObjCClassMethodDecl           = 17
    CXCursor_ObjCImplementationDecl        = 18
    CXCursor_ObjCCategoryImplDecl          = 19
    CXCursor_TypedefDecl                   = 20
    CXCursor_CXXMethod                     = 21
    CXCursor_Namespace                     = 22
    CXCursor_LinkageSpec                   = 23
    CXCursor_Constructor                   = 24
    CXCursor_Destructor                    = 25
    CXCursor_ConversionFunction            = 26
    CXCursor_TemplateTypeParameter         = 27
    CXCursor_NonTypeTemplateParameter      = 28
    CXCursor_TemplateTemplateParameter     = 29
    CXCursor_FunctionTemplate              = 30
    CXCursor_ClassTemplate                 = 31
    CXCursor_ClassTemplatePartialSpecialization = 32
    CXCursor_NamespaceAlias                = 33
    CXCursor_UsingDirective                = 34
    CXCursor_UsingDeclaration              = 35
    CXCursor_TypeAliasDecl                 = 36
    CXCursor_ObjCSynthesizeDecl            = 37
    CXCursor_ObjCDynamicDecl               = 38
    CXCursor_CXXAccessSpecifier            = 39

    CXCursor_ObjCSuperClassRef             = 40
    CXCursor_ObjCProtocolRef               = 41
    CXCursor_ObjCClassRef                  = 42
    CXCursor_TypeRef                       = 43
    CXCursor_CXXBaseSpecifier              = 44
    CXCursor_TemplateRef                   = 45
    CXCursor_NamespaceRef                  = 46
    CXCursor_MemberRef                     = 47
    CXCursor_LabelRef                      = 48

    CXCursor_OverloadedDeclRef             = 49
    CXCursor_VariableRef                   = 50

    
    CXCursor_InvalidFile                   = 70
    CXCursor_NoDeclFound                   = 71
    CXCursor_NotImplemented                = 72
    CXCursor_InvalidCode                   = 73

    CXCursor_UnexposedExpr                 = 100
    CXCursor_DeclRefExpr                   = 101
    CXCursor_MemberRefExpr                 = 102
    CXCursor_CallExpr                      = 103
    CXCursor_ObjCMessageExpr               = 104
    CXCursor_BlockExpr                     = 105
    CXCursor_IntegerLiteral                = 106
    CXCursor_FloatingLiteral               = 107
    CXCursor_ImaginaryLiteral              = 108
    CXCursor_StringLiteral                 = 109
    CXCursor_CharacterLiteral              = 110
    CXCursor_ParenExpr                     = 111
    CXCursor_UnaryOperator                 = 112
    CXCursor_ArraySubscriptExpr            = 113
    CXCursor_BinaryOperator                = 114
    CXCursor_CompoundAssignOperator        = 115
    CXCursor_ConditionalOperator           = 116
    CXCursor_CStyleCastExpr                = 117
    CXCursor_CompoundLiteralExpr           = 118
    CXCursor_InitListExpr                  = 119
    CXCursor_AddrLabelExpr                 = 120
    CXCursor_StmtExpr                      = 121
    CXCursor_GenericSelectionExpr          = 122
    CXCursor_GNUNullExpr                   = 123
    CXCursor_CXXStaticCastExpr             = 124
    CXCursor_CXXDynamicCastExpr            = 125
    CXCursor_CXXReinterpretCastExpr        = 126
    CXCursor_CXXConstCastExpr              = 127
    CXCursor_CXXFunctionalCastExpr         = 128
    CXCursor_CXXTypeidExpr                 = 129
    CXCursor_CXXBoolLiteralExpr            = 130
    CXCursor_CXXNullPtrLiteralExpr         = 131
    CXCursor_CXXThisExpr                   = 132
    CXCursor_CXXThrowExpr                  = 133
    CXCursor_CXXNewExpr                    = 134
    CXCursor_CXXDeleteExpr                 = 135
    CXCursor_UnaryExpr                     = 136
    CXCursor_ObjCStringLiteral             = 137
    CXCursor_ObjCEncodeExpr                = 138
    CXCursor_ObjCSelectorExpr              = 139
    CXCursor_ObjCProtocolExpr              = 140
    CXCursor_ObjCBridgedCastExpr           = 141
    CXCursor_PackExpansionExpr             = 142
    CXCursor_SizeOfPackExpr                = 143
    CXCursor_LambdaExpr                    = 144
    CXCursor_ObjCBoolLiteralExpr           = 145
    CXCursor_ObjCSelfExpr                  = 146
    CXCursor_OMPArraySectionExpr           = 147
    CXCursor_ObjCAvailabilityCheckExpr     = 148
    CXCursor_FixedPointLiteral             = 149
    
    CXCursor_UnexposedStmt                 = 200
    CXCursor_LabelStmt                     = 201
    CXCursor_CompoundStmt                  = 202
    CXCursor_CaseStmt                      = 203
    CXCursor_DefaultStmt                   = 204
    CXCursor_IfStmt                        = 205
    CXCursor_SwitchStmt                    = 206
    CXCursor_WhileStmt                     = 207
    CXCursor_DoStmt                        = 208
    CXCursor_ForStmt                       = 209
    CXCursor_GotoStmt                      = 210
    CXCursor_IndirectGotoStmt              = 211
    CXCursor_ContinueStmt                  = 212
    CXCursor_BreakStmt                     = 213
    CXCursor_ReturnStmt                    = 214
    CXCursor_GCCAsmStmt                    = 215
    CXCursor_ObjCAtTryStmt                 = 216
    CXCursor_ObjCAtCatchStmt               = 217
    CXCursor_ObjCAtFinallyStmt             = 218
    CXCursor_ObjCAtThrowStmt               = 219
    CXCursor_ObjCAtSynchronizedStmt        = 220
    CXCursor_ObjCAutoreleasePoolStmt       = 221
    CXCursor_ObjCForCollectionStmt         = 222
    CXCursor_CXXCatchStmt                  = 223
    CXCursor_CXXTryStmt                    = 224
    CXCursor_CXXForRangeStmt               = 225
    CXCursor_SEHTryStmt                    = 226
    CXCursor_SEHExceptStmt                 = 227
    CXCursor_SEHFinallyStmt                = 228
    CXCursor_MSAsmStmt                     = 229
    CXCursor_NullStmt                      = 230
    CXCursor_DeclStmt                      = 231
    CXCursor_OMPParallelDirective          = 232
    CXCursor_OMPSimdDirective              = 233
    CXCursor_OMPForDirective               = 234
    CXCursor_OMPSectionsDirective          = 235
    CXCursor_OMPSectionDirective           = 236
    CXCursor_OMPSingleDirective            = 237
    CXCursor_OMPParallelForDirective       = 238
    CXCursor_OMPParallelSectionsDirective  = 239
    CXCursor_OMPTaskDirective              = 240
    CXCursor_OMPMasterDirective            = 241
    CXCursor_OMPCriticalDirective          = 242
    CXCursor_OMPTaskyieldDirective         = 243
    CXCursor_OMPBarrierDirective           = 244
    CXCursor_OMPTaskwaitDirective          = 245
    CXCursor_OMPFlushDirective             = 246
    CXCursor_SEHLeaveStmt                  = 247
    CXCursor_OMPOrderedDirective           = 248
    CXCursor_OMPAtomicDirective            = 249
    CXCursor_OMPForSimdDirective           = 250
    CXCursor_OMPParallelForSimdDirective   = 251
    CXCursor_OMPTargetDirective            = 252
    CXCursor_OMPTeamsDirective             = 253
    CXCursor_OMPTaskgroupDirective         = 254
    CXCursor_OMPCancellationPointDirective = 255
    CXCursor_OMPCancelDirective            = 256
    CXCursor_OMPTargetDataDirective        = 257
    CXCursor_OMPTaskLoopDirective          = 258
    CXCursor_OMPTaskLoopSimdDirective      = 259
    CXCursor_OMPDistributeDirective        = 260
    CXCursor_OMPTargetEnterDataDirective   = 261
    CXCursor_OMPTargetExitDataDirective    = 262
    CXCursor_OMPTargetParallelDirective    = 263
    CXCursor_OMPTargetParallelForDirective = 264
    CXCursor_OMPTargetUpdateDirective      = 265
    CXCursor_OMPDistributeParallelForDirective = 266
    CXCursor_OMPDistributeParallelForSimdDirective = 267
    CXCursor_OMPDistributeSimdDirective = 268
    CXCursor_OMPTargetParallelForSimdDirective = 269
    CXCursor_OMPTargetSimdDirective = 270
    CXCursor_OMPTeamsDistributeDirective = 271
    CXCursor_OMPTeamsDistributeSimdDirective = 272
    CXCursor_OMPTeamsDistributeParallelForSimdDirective = 273
    CXCursor_OMPTeamsDistributeParallelForDirective = 274
    CXCursor_OMPTargetTeamsDirective = 275
    CXCursor_OMPTargetTeamsDistributeDirective = 276
    CXCursor_OMPTargetTeamsDistributeParallelForDirective = 277
    CXCursor_OMPTargetTeamsDistributeParallelForSimdDirective = 278
    CXCursor_OMPTargetTeamsDistributeSimdDirective = 279
    CXCursor_TranslationUnit               = 300
    
    CXCursor_UnexposedAttr                 = 400

    CXCursor_IBActionAttr                  = 401
    CXCursor_IBOutletAttr                  = 402
    CXCursor_IBOutletCollectionAttr        = 403
    CXCursor_CXXFinalAttr                  = 404
    CXCursor_CXXOverrideAttr               = 405
    CXCursor_AnnotateAttr                  = 406
    CXCursor_AsmLabelAttr                  = 407
    CXCursor_PackedAttr                    = 408
    CXCursor_PureAttr                      = 409
    CXCursor_ConstAttr                     = 410
    CXCursor_NoDuplicateAttr               = 411
    CXCursor_CUDAConstantAttr              = 412
    CXCursor_CUDADeviceAttr                = 413
    CXCursor_CUDAGlobalAttr                = 414
    CXCursor_CUDAHostAttr                  = 415
    CXCursor_CUDASharedAttr                = 416
    CXCursor_VisibilityAttr                = 417
    CXCursor_DLLExport                     = 418
    CXCursor_DLLImport                     = 419
    CXCursor_NSReturnsRetained             = 420
    CXCursor_NSReturnsNotRetained          = 421
    CXCursor_NSReturnsAutoreleased         = 422
    CXCursor_NSConsumesSelf                = 423
    CXCursor_NSConsumed                    = 424
    CXCursor_ObjCException                 = 425
    CXCursor_ObjCNSObject                  = 426
    CXCursor_ObjCIndependentClass          = 427
    CXCursor_ObjCPreciseLifetime           = 428
    CXCursor_ObjCReturnsInnerPointer       = 429
    CXCursor_ObjCRequiresSuper             = 430
    CXCursor_ObjCRootClass                 = 431
    CXCursor_ObjCSubclassingRestricted     = 432
    CXCursor_ObjCExplicitProtocolImpl      = 433
    CXCursor_ObjCDesignatedInitializer     = 434
    CXCursor_ObjCRuntimeVisible            = 435
    CXCursor_ObjCBoxable                   = 436
    CXCursor_FlagEnum                      = 437
    
    CXCursor_PreprocessingDirective        = 500
    CXCursor_MacroDefinition               = 501
    CXCursor_MacroExpansion                = 502
    CXCursor_InclusionDirective            = 503

    CXCursor_ModuleImportDecl              = 600
    CXCursor_TypeAliasTemplateDecl         = 601
    CXCursor_StaticAssert                  = 602
    CXCursor_FriendDecl                    = 603
    CXCursor_OverloadCandidate             = 700

  CXTypeKind* = enum
    CXType_Invalid = 0
    CXType_Unexposed = 1

    CXType_Void = 2
    CXType_Bool = 3
    CXType_Char_U = 4
    CXType_UChar = 5
    CXType_Char16 = 6
    CXType_Char32 = 7
    CXType_UShort = 8
    CXType_UInt = 9
    CXType_ULong = 10
    CXType_ULongLong = 11
    CXType_UInt128 = 12
    CXType_Char_S = 13
    CXType_SChar = 14
    CXType_WChar = 15
    CXType_Short = 16
    CXType_Int = 17
    CXType_Long = 18
    CXType_LongLong = 19
    CXType_Int128 = 20
    CXType_Float = 21
    CXType_Double = 22
    CXType_LongDouble = 23
    CXType_NullPtr = 24
    CXType_Overload = 25
    CXType_Dependent = 26
    CXType_ObjCId = 27
    CXType_ObjCClass = 28
    CXType_ObjCSel = 29
    CXType_Float128 = 30
    CXType_Half = 31
    CXType_Float16 = 32
    CXType_ShortAccum = 33
    CXType_Accum = 34
    CXType_LongAccum = 35
    CXType_UShortAccum = 36
    CXType_UAccum = 37
    CXType_ULongAccum = 38

    CXType_Complex = 100
    CXType_Pointer = 101
    CXType_BlockPointer = 102
    CXType_LValueReference = 103
    CXType_RValueReference = 104
    CXType_Record = 105
    CXType_Enum = 106
    CXType_Typedef = 107
    CXType_ObjCInterface = 108
    CXType_ObjCObjectPointer = 109
    CXType_FunctionNoProto = 110
    CXType_FunctionProto = 111
    CXType_ConstantArray = 112
    CXType_Vector = 113
    CXType_IncompleteArray = 114
    CXType_VariableArray = 115
    CXType_DependentSizedArray = 116
    CXType_MemberPointer = 117
    CXType_Auto = 118
    CXType_Elaborated = 119
    CXType_Pipe = 120
    CXType_OCLImage1dRO = 121
    CXType_OCLImage1dArrayRO = 122
    CXType_OCLImage1dBufferRO = 123
    CXType_OCLImage2dRO = 124
    CXType_OCLImage2dArrayRO = 125
    CXType_OCLImage2dDepthRO = 126
    CXType_OCLImage2dArrayDepthRO = 127
    CXType_OCLImage2dMSAARO = 128
    CXType_OCLImage2dArrayMSAARO = 129
    CXType_OCLImage2dMSAADepthRO = 130
    CXType_OCLImage2dArrayMSAADepthRO = 131
    CXType_OCLImage3dRO = 132
    CXType_OCLImage1dWO = 133
    CXType_OCLImage1dArrayWO = 134
    CXType_OCLImage1dBufferWO = 135
    CXType_OCLImage2dWO = 136
    CXType_OCLImage2dArrayWO = 137
    CXType_OCLImage2dDepthWO = 138
    CXType_OCLImage2dArrayDepthWO = 139
    CXType_OCLImage2dMSAAWO = 140
    CXType_OCLImage2dArrayMSAAWO = 141
    CXType_OCLImage2dMSAADepthWO = 142
    CXType_OCLImage2dArrayMSAADepthWO = 143
    CXType_OCLImage3dWO = 144
    CXType_OCLImage1dRW = 145
    CXType_OCLImage1dArrayRW = 146
    CXType_OCLImage1dBufferRW = 147
    CXType_OCLImage2dRW = 148
    CXType_OCLImage2dArrayRW = 149
    CXType_OCLImage2dDepthRW = 150
    CXType_OCLImage2dArrayDepthRW = 151
    CXType_OCLImage2dMSAARW = 152
    CXType_OCLImage2dArrayMSAARW = 153
    CXType_OCLImage2dMSAADepthRW = 154
    CXType_OCLImage2dArrayMSAADepthRW = 155
    CXType_OCLImage3dRW = 156
    CXType_OCLSampler = 157
    CXType_OCLEvent = 158
    CXType_OCLQueue = 159
    CXType_OCLReserveID = 160

    CXType_ObjCObject = 161
    CXType_ObjCTypeParam = 162
    CXType_Attributed = 163

    CXType_OCLIntelSubgroupAVCMcePayload = 164
    CXType_OCLIntelSubgroupAVCImePayload = 165
    CXType_OCLIntelSubgroupAVCRefPayload = 166
    CXType_OCLIntelSubgroupAVCSicPayload = 167
    CXType_OCLIntelSubgroupAVCMceResult = 168
    CXType_OCLIntelSubgroupAVCImeResult = 169
    CXType_OCLIntelSubgroupAVCRefResult = 170
    CXType_OCLIntelSubgroupAVCSicResult = 171
    CXType_OCLIntelSubgroupAVCImeResultSingleRefStreamout = 172
    CXType_OCLIntelSubgroupAVCImeResultDualRefStreamout = 173
    CXType_OCLIntelSubgroupAVCImeSingleRefStreamin = 174

    CXType_OCLIntelSubgroupAVCImeDualRefStreamin = 175

  CX_StorageClass* = enum
    CX_SC_Invalid
    CX_SC_None
    CX_SC_Extern
    CX_SC_Static
    CX_SC_PrivateExtern
    CX_SC_OpenCLWorkGroupLocal
    CX_SC_Auto
    CX_SC_Register

  CXTLSKind* = enum
    CXTLS_None = 0
    CXTLS_Dynamic
    CXTLS_Static

  CXDiagnosticSeverity* = enum
    CXDiagnostic_Ignored = 0
    CXDiagnostic_Note    = 1
    CXDiagnostic_Warning = 2
    CXDiagnostic_Error   = 3
    CXDiagnostic_Fatal   = 4

  CXVisitorResult* = enum
    CXVisit_Break
    CXVisit_Continue

  CXChildVisitResult* = enum
    CXChildVisit_Break
    CXChildVisit_Continue
    CXChildVisit_Recurse
  
  OpaqueIndex* = object
  OpaqueTranslationUnit* = object
  OpaqueDiagnosticSet* = object
  OpaqueDiagnostic* = object

  CXCursor* {.header: "<clang-c/Index.h>", importc: "CXCursor".} = object
    kind*: CXCursorKind
    xdata*: cint
    data*: array[0 .. 3, ptr cchar]

  CXType* {.header: "<clang-c/Index.h>", importc: "CXType".} = object
    kind*: CXTypeKind
    data*: array[0 .. 2, ptr cchar]

  CXString* {.header: "<clang-c/CXString.h>", importc: "CXString".} = object
    data*: ptr cchar
    private_flags*: cuint

  CXStringSet* {.header: "<clang-c/CXString.h>", importc: "CXString".} = object
    strings*: ptr CXString
    count*: cuint

  CXUnsavedFile* {.header: "<clang-c/Index.h>", importc: "CXUnsavedFile".} = object
    filename*: cstring
    contents*: cstring
    length*: culong

  CXClientData* = ptr cchar
  CXFieldVisitor* {.header: "<clang-c/Index.h>", importc: "CXFieldVisitor".} = proc (c: CXCursor, client_data: CXClientData): CXVisitorResult {.cdecl.}
  CXCursorVisitor* {.header: "<clang-c/Index.h>", importc: "CXCursorVisitor".} = proc (cursor: CXCursor, parent: CXCursor, client_data: CXClientData): CXChildVisitResult {.cdecl.}

  CXIndex* = ptr OpaqueIndex
  CXTranslationUnit* = ptr OpaqueTranslationUnit
  CXDiagnosticSet* = ptr OpaqueDiagnosticSet
  CXDiagnostic* = ptr OpaqueDiagnostic

proc createIndex*(excludeDeclarationsFromPCH: cint, displayDiagnostics: cint): CXIndex {.header: "<clang-c/Index.h>", importc: "clang_createIndex".}
proc cursor_getNumArguments*(c: CXCursor): cint {.header: "<clang-c/Index.h>", importc: "clang_Cursor_getNumArguments".}
proc cursor_getStorageClass*(c: CXCursor): CX_StorageClass {.header: "<clang-c/Index.h>", importc: "clang_Cursor_getStorageClass".}
proc cursor_isMacroFunctionLike*(c: CXCursor): cuint {.header: "<clang-c/Index.h>", importc: "clang_Cursor_isMacroFunctionLike".}
proc defaultDiagnosticDisplayOptions*(): cuint {.header: "<clang-c/Index.h>", importc: "clang_defaultDiagnosticDisplayOptions".}
proc disposeDiagnostic*(diags: CXDiagnostic) {.header: "<clang-c/Index.h>", importc: "clang_disposeDiagnostic".}
proc disposeString*(str: CXString) {.header: "<clang-c/CXString.h>", importc: "clang_disposeString".}
proc equalCursors*(a: CXCursor, b: CXCursor): cint {.header: "<clang-c/Index.h>", importc: "clang_equalCursors".}
proc formatDiagnostic*(diagnostic: CXDiagnostic, options: cuint): CXString {.header: "<clang-c/Index.h>", importc: "clang_formatDiagnostic".}
proc getArgType*(t: CXType, i: cuint): CXType {.header: "<clang-c/Index.h>", importc: "clang_getArgType".}
proc getArrayElementType*(t: CXType): CXType {.header: "<clang-c/Index.h>", importc: "clang_getArrayElementType".}
proc getArraySize*(t: CXType): clonglong {.header: "<clang-c/Index.h>", importc: "clang_getArraySize".}
proc getCanonicalType*(t: CXType): CXType {.header: "<clang-c/Index.h>", importc: "clang_getCanonicalType".}
proc getCString_real*(str: CXString): ptr cchar {.header: "<clang-c/CXString.h>", importc: "clang_getCString".}
proc getCursorDisplayName*(c: CXCursor): CXString {.header: "<clang-c/Index.h>", importc: "clang_getCursorDisplayName".}
proc getCursorKind*(c: CXCursor): CXCursorKind {.header: "<clang-c/Index.h>", importc: "clang_getCursorKind".}
proc getCursorLexicalParent*(c: CXCursor): CXCursor {.header: "<clang-c/Index.h>", importc: "clang_getCursorLexicalParent".}
proc getCursorSemanticParent*(c: CXCursor): CXCursor {.header: "<clang-c/Index.h>", importc: "clang_getCursorSemanticParent".}
proc getCursorSpelling*(c: CXCursor): CXString {.header: "<clang-c/Index.h>", importc: "clang_getCursorSpelling".}
proc getCursorTLSKind*(c: CXCursor): CXTLSKind {.header: "<clang-c/Index.h>", importc: "clang_getCursorTLSKind".}
proc getCursorType*(c: CXCursor): CXType {.header: "<clang-c/Index.h>", importc: "clang_getCursorType".}
proc getDiagnostic*(unit: CXTranslationUnit, index: cuint): CXDiagnostic {.header: "<clang-c/Index.h>", importc: "clang_getDiagnostic".}
proc getDiagnosticSeverity*(diag: CXDiagnostic): CXDiagnosticSeverity {.header: "<clang-c/Index.h>", importc: "clang_getDiagnosticSeverity".}
proc getElementType*(t: CXType): CXType {.header: "<clang-c/Index.h>", importc: "clang_getElementType".}
proc getEnumConstantDeclUnsignedValue*(c: CXCursor): culonglong {.header: "<clang-c/Index.h>", importc: "clang_getEnumConstantDeclUnsignedValue".}
proc getEnumDeclIntegerType*(c: CXCursor): CXType {.header: "<clang-c/Index.h>", importc: "clang_getEnumDeclIntegerType".}
proc getNullCursor*(): CXCursor {.header: "<clang-c/Index.h>", importc: "clang_getNullCursor".}
proc getNumArgTypes*(t: CXType): cint {.header: "<clang-c/Index.h>", importc: "clang_getNumArgTypes".}
proc getNumDiagnostics*(unit: CXTranslationUnit): cuint {.header: "<clang-c/Index.h>", importc: "clang_getNumDiagnostics".}
proc getNumElements*(t: CXType): cint {.header: "<clang-c/Index.h>", importc: "clang_getNumElements".}
proc getPointeeType*(t: CXType): CXType {.header: "<clang-c/Index.h>", importc: "clang_getPointeeType".}
proc getResultType*(t: CXType): CXType {.header: "<clang-c/Index.h>", importc: "clang_getResultType".}
proc getTranslationUnitCursor*(unit: CXTranslationUnit): CXCursor {.header: "<clang-c/Index.h>", importc: "clang_getTranslationUnitCursor".}
proc getTranslationUnitSpelling*(unit: CXTranslationUnit): CXString {.header: "<clang-c/Index.h>", importc: "clang_getTranslationUnitSpelling".}
proc getTypeDeclaration*(t: CXType): CXCursor {.header: "<clang-c/Index.h>", importc: "clang_getTypeDeclaration".}
proc getTypeKind*(t: CXType): CXTypeKind {.header: "<clang-c/Index.h>", importc: "clang_getTypeKind".}
proc getTypeSpelling*(t: CXType): CXString {.header: "<clang-c/Index.h>", importc: "clang_getTypeSpelling".}
proc getTypedefDeclUnderlyingType*(c: CXCursor): CXType {.header: "<clang-c/Index.h>", importc: "clang_getTypedefDeclUnderlyingType".}
proc isDeclaration*(kind: CXCursorKind): cuint {.header: "<clang-c/Index.h>", importc: "clang_isDeclaration".}
proc isFunctionTypeVariadic*(t: CXType): cuint {.header: "<clang-c/Index.h>", importc: "clang_isFunctionTypeVariadic".}
proc parseTranslationUnit*(
  idx: CXIndex,
  source_filename: cstring,
  command_line_args: ptr cstring,
  num_command_line_args: cint,
  unsaved_files: ptr CXUnsavedFile,
  num_unsaved_files: cuint,
  options: cuint,
): CXTranslationUnit {.header: "<clang-c/Index.h>", importc: "clang_parseTranslationUnit".}
proc type_visitFields*(t: CXType, visitor: CXFieldVisitor, client_data: CXClientData): cuint {.header: "<clang-c/Index.h>", importc: "clang_Type_visitFields".}
proc visitChildren*(parent: CXCursor, visitor: CXCursorVisitor, client_data: CXClientData) {.header: "<clang-c/Index.h>", importc: "clang_visitChildren".}

proc getCString*(str: CXString): cstring =
  return str.getCString_real.cstring

proc getString*(str: CXString): string =
  result.add(str.getCString)

const
  keywords = [
    "use", "with", "struct", "union", "enum",
    "tagged", "type", "extern", "static",
    "never", "unit", "bool", "u8", "u16",
    "u32", "u64", "u128", "s8", "s16",
    "s32", "s64", "s128", "float16", "float32",
    "float64", "float80", "float128", "float", "uint", "sint",
    "let", "assoc", "if", "else", "while",
    "do", "for", "foreach", "in", "break",
    "continue", "return", "fail", "match", "sizeof",
    "alignof", "cast", "fn", "macro", "mod",
    "define", "and", "or", "false", "true",
  ]

proc decorate*(name: string): string =
  result = name
  if name in keywords:
    result.add('_')

proc to_type*(t: CXType, target: ref Target, chain: seq[ptr cchar]): ref QualifiedType =
  for elem in chain:
    if t.data[0] == elem:
      return newType(Type(kind: tykUnit, unit: TypeUnit())).toQualifiedType

  case t.kind
  of CXType_Unexposed:
    return newType(Type(kind: tykUnit, unit: TypeUnit())).toQualifiedType
  of CXType_Void:
    return newType(Type(kind: tykUnit, unit: TypeUnit())).toQualifiedType
  of CXType_Bool:
    return newType(Type(kind: tykUnsigned, unsigned: TypeUnsigned(bits: Bits(kind: bitsCustom, bits: 8)))).toQualifiedType
  of CXType_Char_U:
    return newType(Type(kind: tykUnsigned, unsigned: TypeUnsigned(bits: Bits(kind: bitsCustom, bits: target.c_char_width.uint8)))).toQualifiedType
  of CXType_UChar:
    return newType(Type(kind: tykUnsigned, unsigned: TypeUnsigned(bits: Bits(kind: bitsCustom, bits: target.c_char_width.uint8)))).toQualifiedType
  of CXType_Char16:
    return newType(Type(kind: tykSigned, signed: TypeSigned(bits: Bits(kind: bitsCustom, bits: 16)))).toQualifiedType
  of CXType_Char32:
    return newType(Type(kind: tykSigned, signed: TypeSigned(bits: Bits(kind: bitsCustom, bits: 32)))).toQualifiedType
  of CXType_UShort:
    return newType(Type(kind: tykUnsigned, unsigned: TypeUnsigned(bits: Bits(kind: bitsCustom, bits: target.c_short_width.uint8)))).toQualifiedType
  of CXType_UInt:
    return newType(Type(kind: tykUnsigned, unsigned: TypeUnsigned(bits: Bits(kind: bitsCustom, bits: target.c_int_width.uint8)))).toQualifiedType
  of CXType_ULong:
    return newType(Type(kind: tykUnsigned, unsigned: TypeUnsigned(bits: Bits(kind: bitsCustom, bits: target.c_long_width.uint8)))).toQualifiedType
  of CXType_ULongLong:
    return newType(Type(kind: tykUnsigned, unsigned: TypeUnsigned(bits: Bits(kind: bitsCustom, bits: target.c_llong_width.uint8)))).toQualifiedType
  of CXType_UInt128:
    return newType(Type(kind: tykUnsigned, unsigned: TypeUnsigned(bits: Bits(kind: bitsCustom, bits: 128)))).toQualifiedType
  of CXType_Char_S:
    return newType(Type(kind: tykSigned, signed: TypeSigned(bits: Bits(kind: bitsCustom, bits: target.c_char_width.uint8)))).toQualifiedType
  of CXType_SChar:
    return newType(Type(kind: tykSigned, signed: TypeSigned(bits: Bits(kind: bitsCustom, bits: target.c_char_width.uint8)))).toQualifiedType
  of CXType_WChar:
    return newType(Type(kind: tykSigned, signed: TypeSigned(bits: Bits(kind: bitsCustom, bits: target.c_short_width.uint8)))).toQualifiedType
  of CXType_Short:
    return newType(Type(kind: tykSigned, signed: TypeSigned(bits: Bits(kind: bitsCustom, bits: target.c_short_width.uint8)))).toQualifiedType
  of CXType_Int:
    return newType(Type(kind: tykSigned, signed: TypeSigned(bits: Bits(kind: bitsCustom, bits: target.c_int_width.uint8)))).toQualifiedType
  of CXType_Long:
    return newType(Type(kind: tykSigned, signed: TypeSigned(bits: Bits(kind: bitsCustom, bits: target.c_long_width.uint8)))).toQualifiedType
  of CXType_LongLong:
    return newType(Type(kind: tykSigned, signed: TypeSigned(bits: Bits(kind: bitsCustom, bits: target.c_llong_width.uint8)))).toQualifiedType
  of CXType_Int128:
    return newType(Type(kind: tykSigned, signed: TypeSigned(bits: Bits(kind: bitsCustom, bits: 128)))).toQualifiedType
  of CXType_Float:
    return newType(Type(kind: tykFloating, floating: TypeFloating(bits: Bits(kind: bitsCustom, bits: 32)))).toQualifiedType
  of CXType_Double:
    return newType(Type(kind: tykFloating, floating: TypeFloating(bits: Bits(kind: bitsCustom, bits: 64)))).toQualifiedType
  of CXType_LongDouble:
    return newType(Type(kind: tykFloating, floating: TypeFloating(bits: Bits(kind: bitsCustom, bits: 80)))).toQualifiedType
  of CXType_Float128:
    return newType(Type(kind: tykFloating, floating: TypeFloating(bits: Bits(kind: bitsCustom, bits: 128)))).toQualifiedType
  of CXType_Half:
    return newType(Type(kind: tykFloating, floating: TypeFloating(bits: Bits(kind: bitsCustom, bits: 16)))).toQualifiedType
  of CXType_Float16:
    return newType(Type(kind: tykFloating, floating: TypeFloating(bits: Bits(kind: bitsCustom, bits: 16)))).toQualifiedType
  of CXType_Pointer:
    let pointee = clang.getPointeeType(t).to_type(target, chain)
    if clang.getPointeeType(t).kind == CXType_FunctionProto or clang.getPointeeType(t).kind == CXType_FunctionNoProto:
      return pointee
    else:
      return newType(Type(kind: tykPointer, point: TypePointer(pointee: pointee.toTypeSumRef))).toQualifiedType
  of CXType_Record:
    proc visitor(c: CXCursor, client_data: CXClientData): CXVisitorResult {.cdecl.} =
      let data = cast[ptr (ref Target, seq[ptr cchar], seq[StructField])](client_data)

      let
        name = clang.getCursorSpelling(c)
        nimname = clang.getString(name)
        cxty = clang.getCursorType(c)
        ty = cxty.to_type(data[][0], data[][1]).toTypeSumRef
      clang.disposeString(name)

      data[][2].add(StructField(name: some(nimname.decorate), ty: ty))
      return CXVisit_Continue

    var full_name = ""
    let decl = clang.getTypeDeclaration(t)
    case clang.getCursorKind(decl)
    of CXCursor_StructDecl:
      full_name = "__struct_"
    of CXCursor_UnionDecl:
      full_name = "__union_"
    else:
      raise newIce("unsupported")
    let
      name = clang.getCursorSpelling(decl)
      nimname = clang.getString(name)
    full_name.add(nimname)
    clang.disposeString(name)

    var chain = chain
    chain.add(t.data[0])

    var
      fields = newSeq[StructField]()
      data = (target, chain, fields)

    discard clang.type_visitFields(t, visitor, cast[ptr cchar](addr data))

    fields = data[2]

    let record_name = if nimname.len == 0: none[Path]() else: some(full_name.toNonFullPath)

    case clang.getCursorKind(decl)
    of CXCursor_StructDecl:
      return newType(Type(kind: tykStruct, struct: TypeStruct(name: record_name, fields: fields))).toQualifiedType
    of CXCursor_UnionDecl:
      return newType(Type(kind: tykUnion, union: TypeStruct(name: record_name, fields: fields))).toQualifiedType
    else:
      raise newIce(fmt"unsupported: {clang.getCursorKind(decl)}")
  of CXType_Enum:
    return clang.getEnumDeclIntegerType(clang.getTypeDeclaration(t)).to_type(target, chain)
  of CXType_FunctionNoProto, CXType_FunctionProto:
    let result_type = clang.getResultType(t).to_type(target, chain).toTypeSumRef
    var params = newSeqOfCap[Argument](clang.getNumArgTypes(t))
    for i in 0 ..< clang.getNumArgTypes(t).cuint:
      let param = clang.getArgType(t, i)
      params.add(Argument(ty: param.to_type(target, chain).toTypeSumRef))
    let variadic = clang.isFunctionTypeVariadic(t) != 0
    return newType(Type(kind: tykFunction, function: TypeFunction(result_type: result_type, params: params, variadic: variadic))).toQualifiedType
  of CXType_ConstantArray, CXType_IncompleteArray:
    let elem = clang.getElementType(t).to_type(target, chain)
    return newType(Type(kind: tykPointer, point: TypePointer(pointee: elem.toTypeSumRef))).toQualifiedType
  of CXType_Elaborated:
    return clang.getCanonicalType(t).to_type(target, chain)
  of CXType_Typedef:
    result = clang.getCanonicalType(t).to_type(target, chain)
    case result.inner.kind
    of tykStruct:
      if result.inner.struct.name.isNone:
        result.inner.struct.name = some(clang.getTypeSpelling(t).getString.toNonFullPath)
    of tykUnion:
      if result.inner.union.name.isNone:
        result.inner.union.name = some(clang.getTypeSpelling(t).getString.toNonFullPath)
    else:
      discard
  of CXType_Vector:
    let elem = clang.getElementType(t).to_type(target, chain)
    let count = clang.getNumElements(t).int
    let size_type = newType(Type(kind: tykUnsigned, unsigned: TypeUnsigned(bits: Bits(kind: bitsWord)))).toQualifiedType.toTypeSumRef
    let size = newType(Type(kind: tykLit, lit: TypeLiteral(ty: size_type, kind: lkInt, intval: count))).toQualifiedType.toTypeSumRef
    return newType(Type(kind: tykVector, vec: TypeVector(elem: elem.toTypeSumRef, size: size))).toQualifiedType
  else:
    raise newIce(fmt"unsupported: {t.kind}")
  
proc to_type*(t: CXType, target: ref Target): ref QualifiedType = to_type(t, target, @[])
