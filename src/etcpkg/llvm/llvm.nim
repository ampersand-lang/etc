{.passC: gorge("llvm-config --cflags").}
{.passL: gorge("llvm-config --system-libs").}
{.passL: gorge("llvm-config --ldflags").}
{.passL: gorge("llvm-config --libs").}

{.emit: """/*INCLUDESECTION*/
#include <llvm-c/Target.h>
#include <llvm-c/Core.h>
""".}

type
  OpaqueContext = object
  OpaqueModule = object
  OpaqueBuilder = object
  OpaqueBasicBlock = object
  OpaqueDiBuilder = object
  OpaqueMetadata = object
  OpaqueType = object
  OpaqueValue = object
  OpaqueModuleProvider = object
  OpaquePassManager = object
  OpaquePassRegistry = object
  OpaqueTargetMachine = object
  OpaqueTargetData = object
  OpaqueTarget = object

  Bool* = cint

  VerifierFailureAction* = enum
    AbortProcessAction
    PrintMessageAction
    ReturnStatusAction

  TypeKind* = enum
    VoidTypeKind
    HalfTypeKind
    FloatTypeKind
    DoubleTypeKind
    X86_FP80TypeKind
    FP128TypeKind
    PPC_FP128TypeKind
    LabelTypeKind
    IntegerTypeKind
    FunctionTypeKind
    StructTypeKind
    ArrayTypeKind
    PointerTypeKind
    VectorTypeKind
    MetadataTypeKind
    X86_MMXTypeKind
    TokenTypeKind

  Linkage* = enum
    ExternalLinkage
    AvailableExternallyLinkage
    LinkOnceAnyLinkage
    LinkOnceODRLinkage
    LinkOnceODRAutoHideLinkage
    WeakAnyLinkage
    WeakODRLinkage
    AppendingLinkage
    InternalLinkage
    PrivateLinkage
    DLLImportLinkage
    DLLExportLinkage
    ExternalWeakLinkage
    GhostLinkage
    CommonLinkage
    LinkerPrivateLinkage
    LinkerPrivateWeakLinkage

  ModuleFlagBehavior* = enum
    ModuleFlagBehaviorError
    ModuleFlagBehaviorWarning
    ModuleFlagBehaviorRequire
    ModuleFlagBehaviorOverride
    ModuleFlagBehaviorAppend
    ModuleFlagBehaviorAppendUnique

  IntPredicate* = enum
    IntEQ = 32
    IntNE
    IntUGT
    IntUGE
    IntULT
    IntULE
    IntSGT
    IntSGE
    IntSLT
    IntSLE
  RealPredicate* = enum
    RealPredicateFalse
    RealOEQ
    RealOGT
    RealOGE
    RealOLT
    RealOLE
    RealONE
    RealORD
    RealUNO
    RealUEQ
    RealUGT
    RealUGE
    RealULT
    RealULE
    RealUNE
    RealPredicateTrue

  TargetMachineRef* = ptr OpaqueTargetMachine
  TargetDataRef* = ptr OpaqueTargetData
  TargetRef* = ptr OpaqueTarget

  CodeGenOptLevel* = enum
    codeGenLevelNone
    codeGenLevelLess
    codeGenLevelDefault
    codeGenLevelAggressive

  RelocMode* = enum
    relocDefault
    relocStatic
    relocPIC
    relocDynamicNoPic

  CodeModel* = enum
    codeModelDefault
    codeModelJITDefault
    codeModelSmall
    codeModelKernel
    codeModelMedium
    codeModelLarge

  CodeGenFileType* = enum
    assemblyFile
    objectFile

  DWARFSourceLanguage* = enum
    DWARFSourceLanguageC89
    DWARFSourceLanguageC
    DWARFSourceLanguageAda83
    DWARFSourceLanguageC_plus_plus
    DWARFSourceLanguageCobol74
    DWARFSourceLanguageCobol85
    DWARFSourceLanguageFortran77
    DWARFSourceLanguageFortran90
    DWARFSourceLanguagePascal83
    DWARFSourceLanguageModula2
    DWARFSourceLanguageJava
    DWARFSourceLanguageC99
    DWARFSourceLanguageAda95
    DWARFSourceLanguageFortran95
    DWARFSourceLanguagePLI
    DWARFSourceLanguageObjC
    DWARFSourceLanguageObjC_plus_plus
    DWARFSourceLanguageUPC
    DWARFSourceLanguageD
    DWARFSourceLanguagePython
    DWARFSourceLanguageOpenCL
    DWARFSourceLanguageGo
    DWARFSourceLanguageModula3
    DWARFSourceLanguageHaskell
    DWARFSourceLanguageC_plus_plus_03
    DWARFSourceLanguageC_plus_plus_11
    DWARFSourceLanguageOCaml
    DWARFSourceLanguageRust
    DWARFSourceLanguageC11
    DWARFSourceLanguageSwift
    DWARFSourceLanguageJulia
    DWARFSourceLanguageDylan
    DWARFSourceLanguageC_plus_plus_14
    DWARFSourceLanguageFortran03
    DWARFSourceLanguageFortran08
    DWARFSourceLanguageRenderScript
    DWARFSourceLanguageBLISS
    DWARFSourceLanguageMips_Assembler
    DWARFSourceLanguageGOOGLE_RenderScript
    DWARFSourceLanguageBORLAND_Delph

  DWARFEmissionKind* = enum
    DWARFEmissionNone = 0
    DWARFEmissionFull
    DWARFEmissionLineTablesOnly

  DWARFTypeEncoding* = cuint

  ContextRef* = ptr OpaqueContext
  ModuleRef* = ptr OpaqueModule
  BuilderRef* = ptr OpaqueBuilder
  BasicBlockRef* = ptr OpaqueBasicBlock
  DiBuilderRef* = ptr OpaqueDiBuilder
  MetadataRef* = ptr OpaqueMetadata
  TypeRef* = ptr OpaqueType
  ValueRef* = ptr OpaqueValue
  ModuleProviderRef* = ptr OpaqueModuleProvider
  PassManagerRef* = ptr OpaquePassManager
  PassRegistryRef* = ptr OpaquePassRegistry

  DiFlags = cuint

const
  DW_ATE_address*: DWARFTypeEncoding = 0x01
  DW_ATE_boolean*: DWARFTypeEncoding = 0x02
  DW_ATE_complex_float*: DWARFTypeEncoding = 0x03
  DW_ATE_float*: DWARFTypeEncoding = 0x04
  DW_ATE_signed*: DWARFTypeEncoding = 0x05
  DW_ATE_signed_char*: DWARFTypeEncoding = 0x06
  DW_ATE_unsigned*: DWARFTypeEncoding = 0x07
  DW_ATE_unsigned_char*: DWARFTypeEncoding = 0x08
  DW_ATE_imaginary_float*: DWARFTypeEncoding = 0x09
  DW_ATE_packed_decimal*: DWARFTypeEncoding = 0x0a
  DW_ATE_numeric_string*: DWARFTypeEncoding = 0x0b
  DW_ATE_edited*: DWARFTypeEncoding = 0x0c
  DW_ATE_signed_fixed*: DWARFTypeEncoding = 0x0d
  DW_ATE_unsigned_fixed*: DWARFTypeEncoding = 0x0e
  DW_ATE_decimal_float*: DWARFTypeEncoding = 0x0f
  DW_ATE_UTF*: DWARFTypeEncoding = 0x10
  DW_ATE_lo_user*: DWARFTypeEncoding = 0x80
  DW_ATE_hi_user*: DWARFTypeEncoding = 0xff

const
  DW_OP_addr*: int64 = 0x03
  DW_OP_deref*: int64 = 0x06
  DW_OP_const1u*: int64 = 0x08
  DW_OP_const1s*: int64 = 0x09
  DW_OP_const2u*: int64 = 0x0a
  DW_OP_const2s*: int64 = 0x0b
  DW_OP_const4u*: int64 = 0x0c
  DW_OP_const4s*: int64 = 0x0d
  DW_OP_const8u*: int64 = 0x0e
  DW_OP_const8s*: int64 = 0x0f
  DW_OP_constu*: int64 = 0x10
  DW_OP_consts*: int64 = 0x11
  DW_OP_dup*: int64 = 0x12
  DW_OP_drop*: int64 = 0x13
  DW_OP_over*: int64 = 0x14
  DW_OP_pick*: int64 = 0x15
  DW_OP_swap*: int64 = 0x16
  DW_OP_rot*: int64 = 0x17
  DW_OP_xderef*: int64 = 0x18
  DW_OP_abs*: int64 = 0x19
  DW_OP_and*: int64 = 0x1a
  DW_OP_div*: int64 = 0x1b
  DW_OP_minus*: int64 = 0x1c
  DW_OP_mod*: int64 = 0x1d
  DW_OP_mul*: int64 = 0x1e
  DW_OP_neg*: int64 = 0x1f
  DW_OP_not*: int64 = 0x20
  DW_OP_or*: int64 = 0x21
  DW_OP_plus*: int64 = 0x22
  DW_OP_plus_uconst*: int64 = 0x23
  DW_OP_shl*: int64 = 0x24
  DW_OP_shr*: int64 = 0x25
  DW_OP_shra*: int64 = 0x26
  DW_OP_xor*: int64 = 0x27
  DW_OP_skip*: int64 = 0x2f
  DW_OP_bra*: int64 = 0x28
  DW_OP_eq*: int64 = 0x29
  DW_OP_ge*: int64 = 0x2a
  DW_OP_gt*: int64 = 0x2b
  DW_OP_le*: int64 = 0x2c
  DW_OP_lt*: int64 = 0x2d
  DW_OP_ne*: int64 = 0x2e
  DW_OP_lit*: int64 = 0x30
  DW_OP_lit1*: int64 = 0x31
  DW_OP_lit31*: int64 = 0x4f
  DW_OP_reg*: int64 = 0x50
  DW_OP_reg1*: int64 = 0x51
  DW_OP_reg31*: int64 = 0x6f
  DW_OP_breg*: int64 = 0x70
  DW_OP_breg1*: int64 = 0x71
  DW_OP_breg31*: int64 = 0x8f
  DW_OP_regx*: int64 = 0x90
  DW_OP_fbreg*: int64 = 0x91
  DW_OP_bregx*: int64 = 0x92
  DW_OP_piece*: int64 = 0x93
  DW_OP_deref_size*: int64 = 0x94
  DW_OP_xderef_size*: int64 = 0x95
  DW_OP_nop*: int64 = 0x96
  DW_OP_push_object_address*: int64 = 0x97
  DW_OP_call2*: int64 = 0x98
  DW_OP_call4*: int64 = 0x99
  DW_OP_call_ref*: int64 = 0x9a
  DW_OP_form_tls_address*: int64 = 0x9b
  DW_OP_call_frame_cfa*: int64 = 0x9c
  DW_OP_bit_piece*: int64 = 0x9d
  DW_OP_implicit_value*: int64 = 0x9e
  DW_OP_stack_value*: int64 = 0x9f
  DW_OP_lo_user*: int64 = 0xe0
  DW_OP_hi_user*: int64 = 0xff

const
  DiFlagZero*: DiFlags = 0
  DiFlagPrivate*: DiFlags = 1
  DiFlagProtected*: DiFlags = 2
  DiFlagPublic*: DiFlags = 3
  DiFlagFwdDecl*: DiFlags = 1 shl 2
  DiFlagAppleBlock*: DiFlags = 1 shl 3
  DiFlagBlockByrefStruct*: DiFlags = 1 shl 4
  DiFlagVirtual*: DiFlags = 1 shl 5
  DiFlagArtificial*: DiFlags = 1 shl 6
  DiFlagExplicit*: DiFlags = 1 shl 7
  DiFlagPrototyped*: DiFlags = 1 shl 8
  DiFlagObjcClassComplete*: DiFlags = 1 shl 9
  DiFlagObjectPointer*: DiFlags = 1 shl 10
  DiFlagVector*: DiFlags = 1 shl 11
  DiFlagStaticMember*: DiFlags = 1 shl 12
  DiFlagLValueReference*: DiFlags = 1 shl 13
  DiFlagRValueReference*: DiFlags = 1 shl 14
  DiFlagReserved*: DiFlags = 1 shl 15
  DiFlagSingleInheritance*: DiFlags = 1 shl 16
  DiFlagMultipleInheritance*: DiFlags = 2 shl 16
  DiFlagVirtualInheritance*: DiFlags = 3 shl 16
  DiFlagIntroducedVirtual*: DiFlags = 1 shl 18
  DiFlagBitField*: DiFlags = 1 shl 19
  DiFlagNoReturn*: DiFlags = 1 shl 20
  DiFlagMainSubprogram*: DiFlags = 1 shl 21
  DiFlagTypePassByValue*: DiFlags = 1 shl 22
  DiFlagTypePassByReference*: DiFlags = 1 shl 23
  DiFlagFixedEnum*: DiFlags = 1 shl 24
  DiFlagThunk*: DiFlags = 1 shl 25
  DiFlagTrivial*: DiFlags = 1 shl 26
  DiFlagIndirectVirtualBase*: DiFlags = (1 shl 2) or (1 shl 5)
  DiFlagAccessibility*: DiFlags = DiFlagPrivate or DiFlagProtected or DiFlagPublic
  DiFlagPtrToMemberRep*: DiFlags = DiFlagSingleInheritance or DiFlagMultipleInheritance or DiFlagVirtualInheritance

proc addCase*(switch: ValueRef, on_val: ValueRef, dest: BasicBlockRef) {.header: "<llvm-c/Core.h>", importc: "LLVMAddCase".}
proc addCFGSimplificationPass*(pm: PassManagerRef) {.header: "<llvm-c/Transforms/Scalar.h>", importc: "LLVMAddCFGSimplificationPass".}
proc addDeadStoreEliminationPass*(pm: PassManagerRef) {.header: "<llvm-c/Transforms/Scalar.h>", importc: "LLVMAddDeadStoreEliminationPass".}
proc addEarlyCSEPass*(pm: PassManagerRef) {.header: "<llvm-c/Transforms/Scalar.h>", importc: "LLVMAddEarlyCSEPass".}
proc addFunction*(m: ModuleRef, name: cstring, ty: TypeRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMAddFunction".}
proc addGlobal*(m: ModuleRef, ty: TypeRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMAddGlobal".}
proc addGlobalDCEPass*(pm: PassManagerRef) {.header: "<llvm-c/Transforms/IPO.h>", importc: "LLVMAddGlobalDCEPass".}
proc addGlobalOptimizerPass*(pm: PassManagerRef) {.header: "<llvm-c/Transforms/IPO.h>", importc: "LLVMAddGlobalOptimizerPass".}
proc addGVNPass*(pm: PassManagerRef) {.header: "<llvm-c/Transforms/Scalar.h>", importc: "LLVMAddGVNPass".}
proc addInstructionCombiningPass*(pm: PassManagerRef) {.header: "<llvm-c/Transforms/InstCombine.h>", importc: "LLVMAddInstructionCombiningPass".}
proc addLICMPass*(pm: PassManagerRef) {.header: "<llvm-c/Transforms/Scalar.h>", importc: "LLVMAddLICMPass".}
proc addLoopIdiomPass*(pm: PassManagerRef) {.header: "<llvm-c/Transforms/Scalar.h>", importc: "LLVMAddLoopIdiomPass".}
proc addLoopRerollPass*(pm: PassManagerRef) {.header: "<llvm-c/Transforms/Scalar.h>", importc: "LLVMAddLoopRerollPass".}
proc addLoopUnrollPass*(pm: PassManagerRef) {.header: "<llvm-c/Transforms/Scalar.h>", importc: "LLVMAddLoopUnrollPass".}
proc addLoopUnswitchPass*(pm: PassManagerRef) {.header: "<llvm-c/Transforms/Scalar.h>", importc: "LLVMAddLoopUnswitchPass".}
proc addLoopVectorizePass*(pm: PassManagerRef) {.header: "<llvm-c/Transforms/Vectorize.h>", importc: "LLVMAddLoopVectorizePass".}
proc addModuleFlag*(m: ModuleRef, behavior: ModuleFlagBehavior, key: cstring, keylen: uint, val: MetadataRef) {.header: "<llvm-c/Core.h>", importc: "LLVMAddModuleFlag".}
proc addPromoteMemoryToRegisterPass*(pm: PassManagerRef) {.header: "<llvm-c/Transforms/Utils.h>", importc: "LLVMAddPromoteMemoryToRegisterPass".}
proc addReassociatePass*(pm: PassManagerRef) {.header: "<llvm-c/Transforms/Scalar.h>", importc: "LLVMAddReassociatePass".}
proc addTailCallEliminationPass*(pm: PassManagerRef) {.header: "<llvm-c/Transforms/Scalar.h>", importc: "LLVMAddTailCallEliminationPass".}
proc alignOf*(ty: TypeRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMAlignOf".}
proc appendBasicBlockInContext*(c: ContextRef, fn: ValueRef, name: cstring): BasicBlockRef {.header: "<llvm-c/Core.h>", importc: "LLVMAppendBasicBlockInContext".}
proc arrayType*(elem: TypeRef, elemcount: cuint): TypeRef {.header: "<llvm-c/Core.h>", importc: "LLVMArrayType".}
proc basicBlockAsValue*(bb: BasicBlockRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBasicBlockAsValue".}
proc buildAdd*(b: BuilderRef, lhs: ValueRef, rhs: ValueRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildAdd".}
proc buildAlloca*(b: BuilderRef, ty: TypeRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildAlloca".}
proc buildAnd*(b: BuilderRef, lhs: ValueRef, rhs: ValueRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildAnd".}
proc buildAShr*(b: BuilderRef, lhs: ValueRef, rhs: ValueRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildAShr".}
proc buildBitcast*(b: BuilderRef, lhs: ValueRef, destty: TypeRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildBitCast".}
proc buildBr*(b: BuilderRef, basicblock: BasicBlockRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildBr".}
proc buildCall*(b: BuilderRef, fn: ValueRef, args: ptr ValueRef, numargs: cuint, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildCall".}
proc buildCondBr*(b: BuilderRef, cond: ValueRef, then: BasicBlockRef, el: BasicBlockRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildCondBr".}
proc buildExtractElement*(b: BuilderRef, lhs: ValueRef, idx: ValueRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildExtractElement".}
proc buildExtractValue*(b: BuilderRef, lhs: ValueRef, idx: cuint, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildExtractValue".}
proc buildFAdd*(b: BuilderRef, lhs: ValueRef, rhs: ValueRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildFAdd".}
proc buildFCmp*(b: BuilderRef, op: RealPredicate, lhs: ValueRef, rhs: ValueRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildFCmp".}
proc buildFDiv*(b: BuilderRef, lhs: ValueRef, rhs: ValueRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildFDiv".}
proc buildFMul*(b: BuilderRef, lhs: ValueRef, rhs: ValueRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildFMul".}
proc buildFNeg*(b: BuilderRef, lhs: ValueRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildFNeg".}
proc buildFPCast*(b: BuilderRef, lhs: ValueRef, destty: TypeRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildFPCast".}
proc buildFPToSI*(b: BuilderRef, lhs: ValueRef, destty: TypeRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildFPToSI".}
proc buildFPToUI*(b: BuilderRef, lhs: ValueRef, destty: TypeRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildFPToUI".}
proc buildFRem*(b: BuilderRef, lhs: ValueRef, rhs: ValueRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildFRem".}
proc buildFSub*(b: BuilderRef, lhs: ValueRef, rhs: ValueRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildFSub".}
proc buildGEP*(b: BuilderRef, point: ValueRef, indices: ptr ValueRef, numindices: cuint, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildGEP".}
proc buildICmp*(b: BuilderRef, op: IntPredicate, lhs: ValueRef, rhs: ValueRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildICmp".}
proc buildIntToPtr*(b: BuilderRef, lhs: ValueRef, destty: TypeRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildIntToPtr".}
proc buildLoad*(b: BuilderRef, lhs: ValueRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildLoad".}
proc buildLShr*(b: BuilderRef, lhs: ValueRef, rhs: ValueRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildLShr".}
proc buildMul*(b: BuilderRef, lhs: ValueRef, rhs: ValueRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildMul".}
proc buildNeg*(b: BuilderRef, lhs: ValueRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildNeg".}
proc buildNot*(b: BuilderRef, lhs: ValueRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildNot".}
proc buildOr*(b: BuilderRef, lhs: ValueRef, rhs: ValueRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildOr".}
proc buildPointerCast*(b: BuilderRef, lhs: ValueRef, destty: TypeRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildPointerCast".}
proc buildPtrToInt*(b: BuilderRef, lhs: ValueRef, destty: TypeRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildPtrToInt".}
proc buildRet*(b: BuilderRef, v: ValueRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildRet".}
proc buildRetVoid*(b: BuilderRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildRetVoid".}
proc buildSDiv*(b: BuilderRef, lhs: ValueRef, rhs: ValueRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildSDiv".}
proc buildSExt*(b: BuilderRef, lhs: ValueRef, destty: TypeRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildSExt".}
proc buildShl*(b: BuilderRef, lhs: ValueRef, rhs: ValueRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildShl".}
proc buildSIToFP*(b: BuilderRef, lhs: ValueRef, destty: TypeRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildSIToFP".}
proc buildSRem*(b: BuilderRef, lhs: ValueRef, rhs: ValueRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildSRem".}
proc buildStore*(b: BuilderRef, lhs: ValueRef, rhs: ValueRef) {.header: "<llvm-c/Core.h>", importc: "LLVMBuildStore".}
proc buildStructGEP*(b: BuilderRef, lhs: ValueRef, idx: cuint, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildStructGEP".}
proc buildSub*(b: BuilderRef, lhs: ValueRef, rhs: ValueRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildSub".}
proc buildSwitch*(b: BuilderRef, lhs: ValueRef, el: BasicBlockRef, numcases: cuint): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildSwitch".}
proc buildTrunc*(b: BuilderRef, lhs: ValueRef, destty: TypeRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildTrunc".}
proc buildUDiv*(b: BuilderRef, lhs: ValueRef, rhs: ValueRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildUDiv".}
proc buildUIToFP*(b: BuilderRef, lhs: ValueRef, destty: TypeRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildUIToFP".}
proc buildUnreachable*(b: BuilderRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildUnreachable".}
proc buildURem*(b: BuilderRef, lhs: ValueRef, rhs: ValueRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildURem".}
proc buildXor*(b: BuilderRef, lhs: ValueRef, rhs: ValueRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildXor".}
proc buildZExt*(b: BuilderRef, lhs: ValueRef, destty: TypeRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildZExt".}
proc buildZExtOrBitCast*(b: BuilderRef, lhs: ValueRef, destty: TypeRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMBuildZExtOrBitCast".}
proc constAdd*(lhs: ValueRef, rhs: ValueRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstAdd".}
proc constAnd*(lhs: ValueRef, rhs: ValueRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstAnd".}
proc constArray*(elemty: TypeRef, vals: ptr ValueRef, len: cuint): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstArray".}
proc constAShr*(lhs: ValueRef, rhs: ValueRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstAShr".}
proc constBitCast*(val: ValueRef, totype: TypeRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstBitCast".}
proc constExtractElement*(lhs: ValueRef, idx: ValueRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstExtractElement".}
proc constExtractValue*(lhs: ValueRef, idx: cuint): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstExtractValue".}
proc constFAdd*(lhs: ValueRef, rhs: ValueRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstFAdd".}
proc constFCmp*(op: RealPredicate, lhs: ValueRef, rhs: ValueRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstFCmp".}
proc constFDiv*(lhs: ValueRef, rhs: ValueRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstFDiv".}
proc constFMul*(lhs: ValueRef, rhs: ValueRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstFMul".}
proc constFNeg*(lhs: ValueRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstFNeg".}
proc constFPCast*(lhs: ValueRef, destty: TypeRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstFPCast".}
proc constFPToSI*(lhs: ValueRef, destty: TypeRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstFPToSI".}
proc constFPToUI*(lhs: ValueRef, destty: TypeRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstFPToUI".}
proc constFRem*(lhs: ValueRef, rhs: ValueRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstFRem".}
proc constFSub*(lhs: ValueRef, rhs: ValueRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstFSub".}
proc constICmp*(op: IntPredicate, lhs: ValueRef, rhs: ValueRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstICmp".}
proc constInt*(intty: TypeRef, n: culong, sign: cint): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstInt".}
proc constIntCast*(val: ValueRef, totype: TypeRef, issigned: cint): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstIntCast".}
proc constIntToPtr*(val: ValueRef, intty: TypeRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstIntToPtr".}
proc constLShr*(lhs: ValueRef, rhs: ValueRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstLShr".}
proc constMul*(lhs: ValueRef, rhs: ValueRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstMul".}
proc constNamedStruct*(structty: TypeRef, vals: ptr ValueRef, count: cuint): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstNamedStruct".}
proc constNeg*(lhs: ValueRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstNeg".}
proc constNot*(lhs: ValueRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstNot".}
proc constOr*(lhs: ValueRef, rhs: ValueRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstOr".}
proc constPointerCast*(lhs: ValueRef, destty: TypeRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstPointerCast".}
proc constPtrToInt*(lhs: ValueRef, destty: TypeRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstPtrToInt".}
proc constReal*(floatty: TypeRef, n: cdouble): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstReal".}
proc constSDiv*(lhs: ValueRef, rhs: ValueRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstSDiv".}
proc constSExt*(lhs: ValueRef, destty: TypeRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstSExt".}
proc constShl*(lhs: ValueRef, rhs: ValueRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstShl".}
proc constSIToFP*(lhs: ValueRef, destty: TypeRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstSIToFP".}
proc constSRem*(lhs: ValueRef, rhs: ValueRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstSRem".}
proc constStructInContext*(c: ContextRef, vals: ptr ValueRef, count: cuint, packed: cint): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstStructInContext".}
proc constSub*(lhs: ValueRef, rhs: ValueRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstSub".}
proc constTrunc*(lhs: ValueRef, destty: TypeRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstTrunc".}
proc constUDiv*(lhs: ValueRef, rhs: ValueRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstUDiv".}
proc constUIToFP*(lhs: ValueRef, destty: TypeRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstUIToFP".}
proc constURem*(lhs: ValueRef, rhs: ValueRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstURem".}
proc constXor*(lhs: ValueRef, rhs: ValueRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstXor".}
proc constZExt*(lhs: ValueRef, destty: TypeRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstZExt".}
proc constZExtOrBitCast*(lhs: ValueRef, destty: TypeRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMConstZExtOrBitCast".}
proc contextCreate*(): ContextRef {.header: "<llvm-c/Core.h>", importc: "LLVMContextCreate".}
proc contextDispose*(c: ContextRef) {.header: "<llvm-c/Core.h>", importc: "LLVMContextDispose".}
proc createBuilderInContext*(c: ContextRef): BuilderRef {.header: "<llvm-c/Core.h>", importc: "LLVMCreateBuilderInContext".}
proc createDiBuilder*(m: ModuleRef): DiBuilderRef {.header: "<llvm-c/DebugInfo.h>", importc: "LLVMCreateDIBuilder".}
proc createFunctionPassManager*(mp: ModuleProviderRef): PassManagerRef {.header: "<llvm-c/Core.h>", importc: "LLVMCreateFunctionPassManager".} # 
proc createModuleProviderForExistingModule*(m: ModuleRef): ModuleProviderRef {.header: "<llvm-c/Core.h>", importc: "LLVMCreateModuleProviderForExistingModule".}
proc createPassManager*(): PassManagerRef {.header: "<llvm-c/Core.h>", importc: "LLVMCreatePassManager".}
proc createTargetDataLayout*(t: TargetMachineRef): TargetDataRef {.header: "<llvm-c/Core.h>", importc: "LLVMCreateTargetDataLayout".} # XXX
proc createTargetMachine*(t: TargetRef, triple: cstring, cpu: cstring, features: cstring, level: CodeGenOptLevel, reloc: RelocMode, code_model: CodeModel): TargetMachineRef {.header: "<llvm-c/TargetMachine.h>", importc: "LLVMCreateTargetMachine".}
proc debugMetadataVersion*(): cuint {.header: "<llvm-c/DebugInfo.h>", importc: "LLVMDebugMetadataVersion".}
proc deleteBasicBlock*(bb: BasicBlockRef) {.header: "<llvm-c/Core.h>", importc: "LLVMDeleteBasicBlock".}
proc diBuilderCreateArrayType*(builder: DiBuilderRef, len: uint64, align: uint32, ty: MetadataRef, subscripts: ptr MetadataRef, num_subscripts: cuint): MetadataRef {.header: "<llvm-c/DebugInfo.h>", importc: "LLVMDIBuilderCreateArrayType".}
proc diBuilderCreateAutoVariable*(builder: DiBuilderRef, scope: MetadataRef, name: cstring, name_len: uint, file: MetadataRef, line_no: cuint, ty: MetadataRef, always_preserve: Bool, flags: DiFlags, align: uint32): MetadataRef {.header: "<llvm-c/DebugInfo.h>", importc: "LLVMDIBuilderCreateAutoVariable".}
proc diBuilderCreateBasicType*(builder: DiBuilderRef, name: cstring, name_len: uint, size: uint64, encoding: DWARFTypeEncoding, flags: DiFlags): MetadataRef {.header: "<llvm-c/DebugInfo.h>", importc: "LLVMDIBuilderCreateBasicType".}
proc diBuilderCreateCompileUnit*(builder: DiBuilderRef, lang: DWARFSourceLanguage, file_ref: MetadataRef, producer: cstring, producer_len: uint, is_optimized: Bool, flags: cstring, flags_len: uint, runtime_ver: cuint, split_name: cstring, split_name_len: cuint, kind: DWARFEmissionKind, DWOId: cuint, split_debug_nlining: Bool, debug_info_for_profiling: Bool): MetadataRef {.header: "<llvm-c/DebugInfo.h>", importc: "LLVMDIBuilderCreateCompileUnit".}
proc diBuilderCreateDebugLocation*(ctx: ContextRef, line: cuint, column: cuint, scope: MetadataRef, inlined_at: MetadataRef): MetadataRef {.header: "<llvm-c/DebugInfo.h>", importc: "LLVMDIBuilderCreateDebugLocation".}
proc diBuilderCreateExpression*(builder: DiBuilderRef, ops: ptr int64, len: uint): MetadataRef {.header: "<llvm-c/DebugInfo.h>", importc: "LLVMDIBuilderCreateExpression".}
proc diBuilderCreateFile*(builder: DiBuilderRef, filename: cstring, filename_len: uint, directory: cstring, directory_len: uint): MetadataRef {.header: "<llvm-c/DebugInfo.h>", importc: "LLVMDIBuilderCreateFile".}
proc diBuilderCreateFunction*(builder: DiBuilderRef, scope: MetadataRef, name: cstring, name_len: uint, linkage_name: cstring, linkage_name_len: uint, file: MetadataRef, line: cuint, ty: MetadataRef, local: Bool, is_definition: Bool, scope_line: cuint, flags: DiFlags, is_optimized: Bool): MetadataRef {.header: "<llvm-c/DebugInfo.h>", importc: "LLVMDIBuilderCreateFunction".}
proc diBuilderCreateParameterVariable*(builder: DiBuilderRef, scope: MetadataRef, name: cstring, name_len: uint, arg_no: cuint, file: MetadataRef, line_no: cuint, ty: MetadataRef, always_preserve: Bool, flags: DiFlags): MetadataRef {.header: "<llvm-c/DebugInfo.h>", importc: "LLVMDIBuilderCreateParameterVariable".}
proc diBuilderCreatePointerType*(builder: DiBuilderRef, pointee: MetadataRef, size: uint64, align: uint32, address_space: cuint, name: cstring, name_len: uint): MetadataRef {.header: "<llvm-c/DebugInfo.h>", importc: "LLVMDIBuilderCreatePointerType".}
proc diBuilderCreateStructType*(builder: DiBuilderRef, scope: MetadataRef, name: cstring, name_len: uint, file: MetadataRef, line_number: cuint, size: uint64, align: uint32, flags: DiFlags, derived_from: MetadataRef, elements: ptr MetadataRef, num_elements: cuint, run_time_lang: cuint, vtable: MetadataRef, unique_id: cstring, unique_id_len: uint): MetadataRef {.header: "<llvm-c/DebugInfo.h>", importc: "LLVMDIBuilderCreateStructType".}
proc diBuilderCreateSubroutineType*(builder: DiBuilderRef, file: MetadataRef, params: ptr MetadataRef, num_parameter_types: cuint, flags: DiFlags): MetadataRef {.header: "<llvm-c/DebugInfo.h>", importc: "LLVMDIBuilderCreateSubroutineType".}
proc diBuilderCreateUnionType*(builder: DiBuilderRef, scope: MetadataRef, name: cstring, name_len: uint, file: MetadataRef, line_number: cuint, size: uint64, align: uint32, flags: DiFlags, elements: ptr MetadataRef, num_elements: cuint, run_time_lang: cuint, unique_id: cstring, unique_id_len: uint): MetadataRef {.header: "<llvm-c/DebugInfo.h>", importc: "LLVMDIBuilderCreateUnionType".}
proc diBuilderCreateUnspecifiedType*(builder: DiBuilderRef, Name: cstring, name_len: uint64): MetadataRef {.header: "<llvm-c/DebugInfo.h>", importc: "LLVMDIBuilderCreateUnspecifiedType".}
proc diBuilderCreateVectorType*(builder: DiBuilderRef, len: uint64, align: uint32, ty: MetadataRef, subscripts: ptr MetadataRef, num_subscripts: cuint): MetadataRef {.header: "<llvm-c/DebugInfo.h>", importc: "LLVMDIBuilderCreateVectorType".}
proc diBuilderFinalize*(builder: DiBuilderRef) {.header: "<llvm-c/DebugInfo.h>", importc: "LLVMDIBuilderFinalize".}
proc disposeBuilder*(b: BuilderRef) {.header: "<llvm-c/Core.h>", importc: "LLVMDisposeBuilder".}
proc disposeDIBuilder*() {.header: "<llvm-c/Core.h>", importc: "LLVMDisposeDIBuilder".} # XXX
proc disposeMessage*(msg: cstring) {.header: "<llvm-c/Core.h>", importc: "LLVMDisposeMessage".}
proc disposeTargetData*(td: TargetDataRef) {.header: "<llvm-c/TargetMachine.h>", importc: "LLVMDisposeTargetData".}
proc disposeTargetMachine*(t: TargetMachineRef) {.header: "<llvm-c/TargetMachine.h>", importc: "LLVMDisposeTargetMachine".}
proc doubleTypeInContext*(c: ContextRef): TypeRef {.header: "<llvm-c/Core.h>", importc: "LLVMDoubleTypeInContext".}
proc dumpModule*(m: ModuleRef) {.header: "<llvm-c/Core.h>", importc: "LLVMDumpModule".}
proc dumpType*(m: TypeRef) {.header: "<llvm-c/Core.h>", importc: "LLVMDumpType".}
proc dumpValue*(m: ValueRef) {.header: "<llvm-c/Core.h>", importc: "LLVMDumpValue".}
proc floatTypeInContext*(c: ContextRef): TypeRef {.header: "<llvm-c/Core.h>", importc: "LLVMFloatTypeInContext".}
proc fp128TypeInContext*(c: ContextRef): TypeRef {.header: "<llvm-c/Core.h>", importc: "LLVMFP128TypeInContext".}
proc functionType*(returntype: TypeRef, paramtypes: ptr TypeRef, paramcount: cuint, vararg: cint): TypeRef {.header: "<llvm-c/Core.h>", importc: "LLVMFunctionType".}
proc getArrayLength*(ty: TypeRef): cuint {.header: "<llvm-c/Core.h>", importc: "LLVMGetArrayLength".}
proc getDefaultTargetTriple*(): cstring {.header: "<llvm-c/TargetMachine.h>", importc: "LLVMGetDefaultTargetTriple".}
proc getElementType*(ty: TypeRef): TypeRef {.header: "<llvm-c/Core.h>", importc: "LLVMGetElementType".}
proc getFirstBasicBlock*(fn: ValueRef): BasicBlockRef {.header: "<llvm-c/Core.h>", importc: "LLVMGetFirstBasicBlock".}
proc getFirstInstruction*(bb: BasicBlockRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMGetFirstInstruction".}
proc getGlobalPassRegistry*(): PassRegistryRef {.header: "<llvm-c/Core.h>", importc: "LLVMGetGlobalPassRegistry".}
proc getLastInstruction*(bb: BasicBlockRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMGetLastInstruction".}
proc getNamedFunction*(m: ModuleRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMGetNamedFunction".}
proc getNamedGlobal*(m: ModuleRef, name: cstring): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMGetNamedGlobal".}
proc getNextBasicBlock*(bb: BasicBlockRef): BasicBlockRef {.header: "<llvm-c/Core.h>", importc: "LLVMGetNextBasicBlock".}
proc getNextInstruction*(instr: ValueRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMGetNextInstruction".}
proc getParam*(fn: ValueRef, index: cuint): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMGetParam".}
proc getReturnType*(ty: TypeRef): TypeRef {.header: "<llvm-c/Core.h>", importc: "LLVMGetReturnType".}
proc getTargetFromTriple*(triple: cstring, t: ptr TargetRef, error_msg: ptr cstring): Bool {.header: "<llvm-c/TargetMachine.h>", importc: "LLVMGetTargetFromTriple".}
proc getTypeKind*(t: TypeRef): TypeKind {.header: "<llvm-c/Core.h>", importc: "LLVMGetTypeKind".}
proc getUndef*(ty: TypeRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMGetUndef".}
proc halfTypeInContext*(c: ContextRef): TypeRef {.header: "<llvm-c/Core.h>", importc: "LLVMHalfTypeInContext".}
proc initializeFunctionPassManager*(fpm: PassManagerRef) {.header: "<llvm-c/Initialization.h>", importc: "LLVMInitializeFunctionPassManager".}
proc initializeInstCombine*(pr: PassRegistryRef) {.header: "<llvm-c/Initialization.h>", importc: "LLVMInitializeInstCombine".}
proc initializeScalarOpts*(pr: PassRegistryRef) {.header: "<llvm-c/Initialization.h>", importc: "LLVMInitializeScalarOpts".}
proc initializeTransformUtils*(pr: PassRegistryRef) {.header: "<llvm-c/Initialization.h>", importc: "LLVMInitializeTransformUtils".}
proc initializeX86AsmParser*() {.header: "<llvm-c/Core.h>", importc: "LLVMInitializeX86AsmParser".} # XXX
proc initializeX86AsmPrinter*() {.header: "<llvm-c/Core.h>", importc: "LLVMInitializeX86AsmPrinter".} # XXX
proc initializeX86Disassembler*() {.header: "<llvm-c/Core.h>", importc: "LLVMInitializeX86Disassembler".} # XXX
proc initializeX86Target*() {.header: "<llvm-c/Core.h>", importc: "LLVMInitializeX86Target".} # XXX
proc initializeX86TargetInfo*() {.header: "<llvm-c/Core.h>", importc: "LLVMInitializeX86TargetInfo".} # XXX
proc initializeX86TargetMC*() {.header: "<llvm-c/Core.h>", importc: "LLVMInitializeX86TargetMC".} # XXX
proc intTypeInContext*(c: ContextRef, bits: cuint): TypeRef {.header: "<llvm-c/Core.h>", importc: "LLVMIntTypeInContext".}
proc isATerminatorInst*(inst: ValueRef): cint {.header: "<llvm-c/Core.h>", importc: "LLVMIsATerminatorInst".}
proc isOpaqueStruct*(structty: TypeRef): cint {.header: "<llvm-c/Core.h>", importc: "LLVMIsOpaqueStruct".}
proc metadataAsValue*(c: ContextRef, md: MetadataRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMMetadataAsValue".}
proc metadataTypeInContext*(c: ContextRef): TypeRef {.header: "<llvm-c/Core.h>", importc: "LLVMMetadataTypeInContext".}
proc moduleCreateWithNameInContext*(moduleid: cstring, c: ContextRef): ModuleRef {.header: "<llvm-c/Core.h>", importc: "LLVMModuleCreateWithNameInContext".}
proc pointerType*(elem: TypeRef, addressspace: cuint): TypeRef {.header: "<llvm-c/Core.h>", importc: "LLVMPointerType".}
proc positionBuilderAtEnd*(builder: BuilderRef, bb: BasicBlockRef) {.header: "<llvm-c/Core.h>", importc: "LLVMPositionBuilderAtEnd".}
proc positionBuilderBefore*(builder: BuilderRef, instr: ValueRef) {.header: "<llvm-c/Core.h>", importc: "LLVMPositionBuilderBefore".}
proc printModuleToFile*(m: ModuleRef, filename: cstring, errormessage: ptr cstring): Bool {.header: "<llvm-c/Core.h>", importc: "LLVMPrintModuleToFile".}
proc printModuleToString*(m: ModuleRef): cstring {.header: "<llvm-c/Core.h>", importc: "LLVMPrintModuleToString".}
proc runFunctionPassManager*(fpm: PassManagerRef, f: ValueRef) {.header: "<llvm-c/Core.h>", importc: "LLVMRunFunctionPassManager".}
proc runPassManager*(pm: PassManagerRef, m: ModuleRef) {.header: "<llvm-c/Core.h>", importc: "LLVMRunPassManager".}
proc setAlignment*(val: ValueRef, bytes: cuint) {.header: "<llvm-c/Core.h>", importc: "LLVMSetAlignment".}
proc setCurrentDebugLocation*(b: BuilderRef, l: ValueRef) {.header: "<llvm-c/Core.h>", importc: "LLVMSetCurrentDebugLocation".}
proc setGlobalConstant*(globalvar: ValueRef, isconst: cint) {.header: "<llvm-c/Core.h>", importc: "LLVMSetGlobalConstant".}
proc setInitializer*(globalvar: ValueRef, val: ValueRef) {.header: "<llvm-c/Core.h>", importc: "LLVMSetInitializer".}
proc setLinkage*(global: ValueRef, linkage: Linkage) {.header: "<llvm-c/Core.h>", importc: "LLVMSetLinkage".}
proc setSubprogram*(fn: ValueRef, md: MetadataRef) {.header: "<llvm-c/DebugInfo.h>", importc: "LLVMSetSubprogram".}
proc setThreadLocal*(globalvar: ValueRef, isthread: cint) {.header: "<llvm-c/Core.h>", importc: "LLVMSetThreadLocal".}
proc setValueName2*(val: ValueRef, name: cstring, namelen: uint) {.header: "<llvm-c/Core.h>", importc: "LLVMSetValueName2".}
proc setVolatile*(instr: ValueRef, isvolatile: cint) {.header: "<llvm-c/Core.h>", importc: "LLVMSetVolatile".}
proc sizeOf*(ty: TypeRef): ValueRef {.header: "<llvm-c/Core.h>", importc: "LLVMSizeOf".}
proc structCreateNamed*(c: ContextRef, name: cstring): TypeRef {.header: "<llvm-c/Core.h>", importc: "LLVMStructCreateNamed".}
proc structSetBody*(structty: TypeRef, elems: ptr TypeRef, elemcount: cuint, packed: cint) {.header: "<llvm-c/Core.h>", importc: "LLVMStructSetBody".}
proc structTypeInContext*(c: ContextRef, elems: ptr TypeRef, elemcount: cuint, packed: cint): TypeRef {.header: "<llvm-c/Core.h>", importc: "LLVMStructTypeInContext".}
proc targetMachineEmitToFile*(t: TargetMachineRef, m: ModuleRef, filename: cstring, codegen: CodeGenFileType, error_msg: ptr cstring): Bool {.header: "<llvm-c/TargetMachine.h>", importc: "LLVMTargetMachineEmitToFile".}
proc typeOf*(val: ValueRef): TypeRef {.header: "<llvm-c/Core.h>", importc: "LLVMTypeOf".}
proc valueAsMetadata*(val: ValueRef): MetadataRef {.header: "<llvm-c/Core.h>", importc: "LLVMValueAsMetadata".}
proc verifyModule*(m: ModuleRef, action: VerifierFailureAction, error_msg: ptr cstring): cint {.header: "<llvm-c/Analysis.h>", importc: "LLVMVerifyModule".}
proc vectorType*(elem: TypeRef, elemcount: cuint): TypeRef {.header: "<llvm-c/Analysis.h>", importc: "LLVMVectorType".}
proc voidTypeInContext*(c: ContextRef): TypeRef {.header: "<llvm-c/Core.h>", importc: "LLVMVoidTypeInContext".}
proc writeBitcodeToFile*(m: ModuleRef, path: cstring): cint {.header: "<llvm-c/BitWriter.h>", importc: "LLVMWriteBitcodeToFile".}
proc x86fp80TypeInContext*(c: ContextRef): TypeRef {.header: "<llvm-c/Core.h>", importc: "LLVMX86FP80TypeInContext".}

proc initNative*() =
  {.emit: """
    LLVMInitializeNativeTarget();
    LLVMInitializeNativeAsmParser();
    LLVMInitializeNativeAsmPrinter();
    LLVMInitializeNativeDisassembler();
  """.}
