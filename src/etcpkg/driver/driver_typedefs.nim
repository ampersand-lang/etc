import tables
import sets
import options

import ../ice
import ../path
import ../names
import ../target
import ../lazy
import ../index
import ../hlir/hlir
import ../types/types
import ../macros/typedefs
import ../cg/cgvar
import ../llvm/llvm
import ../llvm/clang

type
  Driver* = object
    target*: ref Target
    type_ctx*: TypeContext
    macro_ctx*: MacroContext
    lazy*: ref LazyUpdate

    pass_controllers*: array[TranslationStage, PassController]
    opt_level*: int
    debug_info*: bool
    build_mode*: bool
    install_mode*: bool
    test_mode*: bool
    release*: bool

  PassController* = ref object of RootObj
  LexPassController* = ref object of PassController
  ParsePassController* = ref object of PassController
  EnPassController* = ref object of PassController
  TdPassController* = ref object of PassController
  TaPassController* = ref object of PassController
    type_ctx*: TypeContext
    module*: Path
    in_macro_stack*: seq[bool]
    result_stack*: seq[ref TypeSum]
    function_stack*: seq[Path]
    match_stack*: seq[ref TypeSum]
    monomorphized*: Table[(Path, ref QualifiedType), ref Node]
    polymorphic_stack*: seq[Table[(Path, ref QualifiedType), ref Node]]
    parameter_stack*: seq[HashSet[string]]

    tests*: seq[Names]
    test_main_added*: bool
  CePassController* = ref object of PassController
  NmPassController* = ref object of PassController
  FaPassController* = ref object of PassController
  CdPassController* = ref object of PassController
  CgPassController* = ref object of PassController
    type_ctx*: TypeContext

    ctx*: llvm.ContextRef
    module*: llvm.ModuleRef
    builder*: llvm.BuilderRef

    mp*: llvm.ModuleProviderRef
    pr*: llvm.PassRegistryRef
    fpm*: llvm.PassManagerRef
    pm*: llvm.PassManagerRef

    di_builder*: llvm.DiBuilderRef
    di_file*: Option[llvm.MetadataRef]
    di_compile_unit*: Option[llvm.MetadataRef]
    di_function_stack*: seq[Option[llvm.MetadataRef]]

    bb_current*: Option[llvm.BasicBlockRef]
    bb_stack*: seq[seq[llvm.BasicBlockRef]]
    function_stack*: seq[llvm.ValueRef]
    pattern_stack*: seq[tuple[value: ref CgVar, ty: ref QualifiedType, is_switch: bool, variant: int, variant_name: Option[string]]]
    switch_stack*: seq[ref CgVar]
    vtables*: Table[tuple[trait: ref QualifiedType, ty: ref QualifiedType], llvm.ValueRef]
    continue_target*: seq[seq[llvm.BasicBlockRef]]
    break_target*: seq[seq[llvm.BasicBlockRef]]

    bindings*: seq[Table[string, ref CgVar]]
    types*: seq[Table[string, llvm.TypeRef]]

    result_var*: seq[ref CgVar]
    early_var*: seq[ref CgVar]
    return_bb*: seq[seq[llvm.BasicBlockRef]]
    defer_count*: seq[seq[int]]

    opt_level*: int
    debug_info*: bool
  EmptyPassController* = ref object of PassController

method init*(pass: PassController, index: ref Index, driver: ref Driver) {.base.} =
  raise newIce("using a raw PassController")

method analyze*(pass: PassController, index: ref Index, driver: ref Driver, unit: ref IndexUnit, node: ref Node) {.base.} =
  raise newIce("using a raw PassController")

method analyze_with_root*(pass: PassController, index: ref Index, driver: ref Driver, unit: ref IndexUnit, node: ref Node, root: ref Node) {.base.} =
  raise newIce("using a raw PassController")
