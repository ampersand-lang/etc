import options
import os
import sequtils
import deques

import ../ice
import ../index
import ../target
import ../lazy
import ../driver/driver_typedefs
import ../types/types
import ../macros/macros
import ../llvm/clang
import ../llvm/llvm

proc newDriver*(target: ref Target, opt_level: int, debug_info: bool, build_mode: bool, install_mode: bool, release: bool): ref Driver =
  new result
  result.target = target
  result.type_ctx = newTypeContext(target)
  result.macro_ctx = newMacroContext(result.type_ctx)
  result.lazy = newLazyUpdate()

  result.opt_level = opt_level
  result.debug_info = debug_info
  result.build_mode = build_mode
  result.install_mode = install_mode
  result.test_mode = false
  result.release = release
  
  result.pass_controllers[tsInitial] = LexPassController()
  result.pass_controllers[tsLexed] = ParsePassController()
  result.pass_controllers[tsParsed] = EnPassController()
  result.pass_controllers[tsEarlyNormalization] = TdPassController()
  result.pass_controllers[tsTypeDecl] = TaPassController()
  result.pass_controllers[tsTypeAnalysis] = CePassController()
  result.pass_controllers[tsConstExprPass] = NmPassController()
  result.pass_controllers[tsNameMangling] = FaPassController()
  result.pass_controllers[tsFlowAnalysis] = CdPassController()
  result.pass_controllers[tsCodeDecl] = CgPassController(opt_level: opt_level, debug_info: debug_info)
  result.pass_controllers[tsCodeGen] = EmptyPassController()
  result.pass_controllers[tsComplete] = EmptyPassController()

proc update*(driver: ref Driver, opt_level: int, debug_info: bool, build_mode: bool, install_mode: bool) =
  driver.opt_level = opt_level
  driver.debug_info = debug_info
  driver.build_mode = build_mode
  driver.install_mode = install_mode
  driver.pass_controllers[tsCodeDecl].CgPassController.opt_level = opt_level
  driver.pass_controllers[tsCodeDecl].CgPassController.debug_info = debug_info

proc step(driver: ref Driver, index: ref Index, unit: ref IndexUnit) =
  inc unit.ts
  driver.pass_controllers[pred(unit.ts)].analyze(index, driver, unit, unit.root.get(nil))
  for node in driver.lazy.nodes:
    for ts in tsEarlyNormalization ..< unit.ts:
      driver.pass_controllers[ts].analyze_with_root(index, driver, unit, node, unit.root.get)
    unit.root.get.node.root.root.node.sequence.children.add(node)
  for node in driver.lazy.norun:
    unit.root.get.node.root.root.node.sequence.children.add(node)

proc run_until*(driver: ref Driver, index: ref Index, unit: ref IndexUnit, ts: TranslationStage) =
  while unit.ts < ts:
    step(driver, index, unit)

proc init*(driver: ref Driver, index: ref Index) =
  for ts in tsInitial .. tsComplete:
    driver.pass_controllers[ts].init(index, driver)

proc run*(driver: ref Driver, index: ref Index, unit: ref IndexUnit): llvm.ModuleRef =
  run_until(driver, index, unit, tsComplete)
  return driver.pass_controllers[tsCodeDecl].CgPassController.module
