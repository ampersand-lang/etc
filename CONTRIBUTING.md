Contributing to Ampersand
=========================

Ampersand welcomes your contributions to help turn it into a fully-fledged
compiler.


GIT Access
----------

Ampersand and etc is developed exclusively using git and is hosted on

 * https://gitlab.com/ampersand-lang
 * https://gitlab.com/ampersand-lang/etc

Write access will remain exclusive to walterpi (aka pi\_pi3, aka Simon) for now.

Compiling the development version
---------------------------------

For build instructions see the [README](./README.md)

Patch Decisions
---------------

If you see something wrong with our code, you may immediately make a fork and
start patching, however please do also make an issue before submitting your
patch as a pull request.


Coding Style
------------

TODO


Documentation
-------------

Every significant bit of code needs to be documented in the central
documentation.  TODO


Our process
-----------

We follow a 9-9-2-2 process developed specifically for this project.  It is
an iterative process based on micro-waterfalls.  Each of the numbers refers to
the number of steps on one branch.

### Development branches

Development branches are called `dev-F`, were F is the name of the feature
spelled in lowercase kebab-case.

1. Analyze the system.
2. Plan specify the feature.
3. Develop the architecture.
4. Implement.
5. Pass unit tests.
6. Pass integration tests.
7. Pass regression tests.
8. Integrate into master.
9. End branch.

### Fix branches

Fix branches are called `fix-N`, were N is the issue number.

1. Analyze the system.
2. Specify the cause of the bug.
3. Develop a fix.
4. Implement.
5. Pass unit tests.
6. Pass integration tests.
7. Pass regression tests.
8. Integrate into master.
9. End branch.

### stable branch

1. Merge master.
2. Repeat every two weeks.

### New tests

New tests are developed on branches called `test-T`, where T is the name of the
test spelled in lowercase kebab-case.

1. Develop the test.
2. Integrate into master.
