#ifndef CLANG_H
#define CLANG_H 1

struct Foo {
    int x;
    int y;
};

typedef struct {
    int x;
} Bar;

union Val {
    int d;
    float f;
};

typedef enum Enum {
    ENUM_A,
    ENUM_B = 42,
    ENUM_C,
} Enum;

int foo(int);

#endif /* ifndef CLANG_H */
