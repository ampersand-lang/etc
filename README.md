# etc

The reference compiler for ampersand &

Important: use the `dev` branch.

# Usage

```
./etc --help
```

```
./etc [options] IN..
```

# Build instructions

0. Install nim through a package manager of choice
1. Run
```
nimble build
```
2. Done

# Dependencies

0. libLLVM version 8
1. libclang version 8.0.1 (version 8.0.1 is currently hard-coded, but will be configurable in the future)
