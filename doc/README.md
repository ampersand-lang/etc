# Technical documentation of etc

## Basic definitions

An ICE, or an internal compiler, error occurs when something unexpected by the programmer happens.  This can be
something like reaching a branch pressumed to be unreachable or simply some feature not being implemented yet.  If an
ICE occurs, it should be rewritten to be handled correctly either as an error or a valid path in the control flow.

An Error occurs when the user made an error in their program.  It can be anything ranging from passing an invalid type
to a function, to missing a semicolon on line 32.

Any other runtime failure is a bug and should be fixed to be handled as either an error or a valid path in the control
flow.

An Index is a collection of source files to be translated.  These are identified by their absolute, normalized path and
classified with a translation stage.  A translation stage shows how far in the compilation a single source file is.  Any
file in index may be either in the state "compiling" or "imported".  All compiled files will be put together into a
single translation unit, imported files will only have their definitions put into the output translation unit.  A single
index can only handle one translation unit.

A Driver is a collection of runtime variable, i.e. state.  The driver has functions defined on it that provide an API to
building & programs from source.  Every driver needs an index.  A driver consists of passes.  Every logical pass may do
one or more actual passes on the source file.  These passes may mutate the state of the driver and that of the HLIR
itself.

A HLIR, or high-level intermedediate representation, is the high-level representation of the source code.  It maps the
semantics of a source file in a nearly one-to-one ratio.  The HLIR may be interpreted and modified differently by
different logical passes.

A "lazy", or lazy updater, is a collection of HLIR nodes and instructions on how to insert them into the top-level HLIR.

A Target is a collection of information on how to emit code from an HLIR.  It includes things like the native pointer
width (in bits) and C integer types' widths (also in bits).

Bits (not to be confused with bits --- lowercase) is a unit of bitcount in a signed, unsigned, or floating type.  It may be
equal to the pointer width or be any custom bit count (different restrictions exist for signed/unsigned and floating
types).

A Span is a location in a concrete source associated with a length.  It gives information on where an HLIR node stems
from.  It includes the source file name, a reference to the source file contents, a position in the source code, a
length, a line number and a column number.

A Path is a logical unit of one or more identifiers.  etc almost never uses plain identifiers --- it almost exclusively
uses Paths to refer to variables, constants, functions and types.  An exception to this is field names of structs,
unions and tagged unions --- these use plain identifiers.  Another exception is the usage of identifiers during the
final compilation stage; during code generation only linkage names are used, which are identifiers.

A Name is a collection of a short, an optional full and an optional linkage name.  The short name is the user-provided
name.  The full name is the fully-qualified, absolute path.  The linkage name is the name that will be emitted.  All
three are created at different stages in the compilation.

A Type is a description of how an Expression should be interpreted (high-level) or a description of how bytes in memory
should be interpreted (low-level).  A Type itself is an Expression as well, it has the meta-Type `type`.

An Expression is some pattern that has a certain r-value and possibly an l-value (high-level) or a concrete
interpretation of bits (low-level).

A Pattern is a description of the value of an Expression.

A Function is a kind of Expression that can be applied with certain proper parameters (arguments) to create a new
Expression (an Application).

A Macro is a Function which substituse HLIR nodes for other HLIR nodes.  A compile time Function is a Function that
executes in a macro at compile time.

An Implementation, or Impl, is the association between a Function, it's parameters and a Path.

An Instance is an Application of an Implementation.

## Parser passes

...

## Semantical passes

...

## Code generation

...

## Type system

...
