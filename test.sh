#!/bin/sh

failure=0
for d in tests/*; do
    positive=1
    cd "$d"
    if [ "$(echo "$d" | grep -e '^.*negative$')" != '' ]; then
        positive=0
    fi
    if [ $positive == '1' ]; then
        for opt in '' '--release' '--debuginfo'; do
            if etc test $opt; then
                echo -ne "\033[32;1m[OK]\033[0m "
                echo "$0: etc test $opt ($d) passed"
            else
                echo -ne "\033[31;1m[ERR]\033[0m "
                echo "$0: etc test $opt ($d) failed"
                failure=1
            fi
        done
    else
        for opt in '' '--release' '--debuginfo'; do
            if etc test $opt; then
                echo -ne "\033[31;1m[ERR]\033[0m "
                echo "$0: etc test $opt ($d) failed"
                failure=1
            else
                echo -ne "\033[32;1m[OK]\033[0m "
                echo "$0: etc test $opt ($d) passed"
            fi
        done
    fi
    cd -
done

if [ "$failure" == '1' ]; then
    echo -e "\033[31;1m some tests failed\033[0m"
else
    echo -e "\033[32;1m all tests passed\033[0m"
fi

exit $failure
