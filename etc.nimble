# Package

version       = "0.1.0"
author        = "Szymon Walter"
description   = "The third compiler for ampersand &"
license       = "MIT"
srcDir        = "src"
installExt    = @["nim"]
bin           = @["etc"]



# Dependencies

requires "nim >= 0.20.2"

task run, "Run the app":
  exec "nimble build"
  exec "./etc"
