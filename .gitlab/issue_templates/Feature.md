# Motivation

Explain here the motivation for your feature request.

# Design

Explain how the feature fits into the current architecture of the compiler.

# Syntax

Explain the necessary syntactical changes.

# Implementation

Explain how this feature might be implemented.

# Future

Explain what future development you foresee for this feature.

# Pre-requisites

List all pre-requisites necessary to implement this feature.  If an issue
already exists for this pre-requisite, link it as #issue-no.
